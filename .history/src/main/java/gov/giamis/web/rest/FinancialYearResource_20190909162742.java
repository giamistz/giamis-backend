package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.FinancialYear;
import gov.giamis.service.FinancialYearService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.fincailCriteria;
import gov.giamis.service.fincailQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link FinancialYear}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "fincail management Resource", description = "Operations to manage fincails")
public class FinancialYearResource {

    private final Logger log = LoggerFactory.getLogger(FinancialYearResource.class);

    private static final String ENTITY_NAME = "fincail";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FinancialYearService     FinancialYearService;

    private final fincailQueryService fincailQueryService;

    public FinancialYearResource(FinancialYearService FinancialYearService, fincailQueryService fincailQueryService) {
        this.FinancialYearService = FinancialYearService;
        this.fincailQueryService = fincailQueryService;
    }

    /**
     * {@code POST  /fincails} : Create a new fincail.
     *
     * @param financialYear the fincail to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fincail, or with status {@code 400 (Bad Request)} if the fincail has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fincails")
    @ApiOperation(value = "Create a new fincail")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createfincail(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to save fincail : {}", financialYear);
        if (financialYear.getId() != null) {
            throw new BadRequestAlertException("A new fincail cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialYear result = FinancialYearService.save(financialYear);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /fincails} : Updates an existing fincail.
     *
     * @param financialYear the fincail to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fincail,
     * or with status {@code 400 (Bad Request)} if the fincail is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fincail couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fincails")
    @ApiOperation(value = "Update a single fincail")
    public CustomApiResponse updatefincail(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to update fincail : {}", financialYear);
        if (financialYear.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialYear result = FinancialYearService.save(financialYear);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /fincails} : get all the fincails.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fincails in body.
     */
    @GetMapping("/fincails")
    @ApiOperation(value = "Get all fincails with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllfincails(fincailCriteria criteria, Pageable pageable) {
        log.debug("REST request to get fincails by criteria: {}", criteria);
        Page<FinancialYear> page = fincailQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /fincails/count} : count all the fincails.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fincails/count")
    @ApiOperation(value = "Count fincails ; Optional filters can be applied")
    public CustomApiResponse countfincails(fincailCriteria criteria) {
        log.debug("REST request to count fincails by criteria: {}", criteria);
        return CustomApiResponse.ok(fincailQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fincails/:id} : get the "id" fincail.
     *
     * @param id the id of the fincail to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fincail, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fincails/{id}")
    @ApiOperation("Get a single fincail by ID")
    public CustomApiResponse getfincail(@PathVariable Long id) {
        log.debug("REST request to get fincail : {}", id);
        Optional<FinancialYear> fincail = FinancialYearService.findOne(id);
        return CustomApiResponse.ok(fincail);
    }

    /**
     * {@code DELETE  /fincails/:id} : delete the "id" fincail.
     *
     * @param id the id of the fincail to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fincails/{id}")
    @ApiOperation("Delete a single fincail by ID")
    public CustomApiResponse deletefincail(@PathVariable Long id) {
        log.debug("REST request to delete fincail : {}", id);
        FinancialYearService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
