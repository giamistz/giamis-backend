package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.FinancialYear;
import gov.giamis.service.FinancialYearService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.fCriteria;
import gov.giamis.service.fQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link FinancialYear}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "f management Resource", description = "Operations to manage fs")
public class FinancialYearResource {

    private final Logger log = LoggerFactory.getLogger(FinancialYearResource.class);

    private static final String ENTITY_NAME = "f";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FinancialYearService     FinancialYearService;

    private final fQueryService fQueryService;

    public FinancialYearResource(FinancialYearService FinancialYearService, fQueryService fQueryService) {
        this.FinancialYearService = FinancialYearService;
        this.fQueryService = fQueryService;
    }

    /**
     * {@code POST  /fs} : Create a new f.
     *
     * @param financialYear the f to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new f, or with status {@code 400 (Bad Request)} if the f has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fs")
    @ApiOperation(value = "Create a new f")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createf(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to save f : {}", financialYear);
        if (financialYear.getId() != null) {
            throw new BadRequestAlertException("A new f cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialYear result = FinancialYearService.save(financialYear);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /fs} : Updates an existing f.
     *
     * @param financialYear the f to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated f,
     * or with status {@code 400 (Bad Request)} if the f is not valid,
     * or with status {@code 500 (Internal Server Error)} if the f couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fs")
    @ApiOperation(value = "Update a single f")
    public CustomApiResponse updatef(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to update f : {}", financialYear);
        if (financialYear.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialYear result = FinancialYearService.save(financialYear);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /fs} : get all the fs.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fs in body.
     */
    @GetMapping("/fs")
    @ApiOperation(value = "Get all fs with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllfs(fCriteria criteria, Pageable pageable) {
        log.debug("REST request to get fs by criteria: {}", criteria);
        Page<FinancialYear> page = fQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /fs/count} : count all the fs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/fs/count")
    @ApiOperation(value = "Count fs ; Optional filters can be applied")
    public CustomApiResponse countfs(fCriteria criteria) {
        log.debug("REST request to count fs by criteria: {}", criteria);
        return CustomApiResponse.ok(fQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fs/:id} : get the "id" f.
     *
     * @param id the id of the f to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the f, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fs/{id}")
    @ApiOperation("Get a single f by ID")
    public CustomApiResponse getf(@PathVariable Long id) {
        log.debug("REST request to get f : {}", id);
        Optional<FinancialYear> f = FinancialYearService.findOne(id);
        return CustomApiResponse.ok(f);
    }

    /**
     * {@code DELETE  /fs/:id} : delete the "id" f.
     *
     * @param id the id of the f to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fs/{id}")
    @ApiOperation("Delete a single f by ID")
    public CustomApiResponse deletef(@PathVariable Long id) {
        log.debug("REST request to delete f : {}", id);
        FinancialYearService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
