package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.FinancialYear;
import gov.giamis.service.Service;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.PeriodCriteria;
import gov.giamis.service.FinancialYearQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link FinancialYear}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "Period management Resource", description = "Operations to manage Periods")
public class FinancialYearResource {

    private final Logger log = LoggerFactory.getLogger(FinancialYearResource.class);

    private static final String ENTITY_NAME = "period";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Service     Service;

    private final PeriodQueryService periodQueryService;

    public FinancialYearResource(Service Service, PeriodQueryService periodQueryService) {
        this.Service = Service;
        this.periodQueryService = periodQueryService;
    }

    /**
     * {@code POST  /periods} : Create a new period.
     *
     * @param financialYear the period to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new period, or with status {@code 400 (Bad Request)} if the period has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/periods")
    @ApiOperation(value = "Create a new period")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createPeriod(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to save Period : {}", financialYear);
        if (financialYear.getId() != null) {
            throw new BadRequestAlertException("A new period cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialYear result = Service.save(financialYear);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /periods} : Updates an existing period.
     *
     * @param financialYear the period to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated period,
     * or with status {@code 400 (Bad Request)} if the period is not valid,
     * or with status {@code 500 (Internal Server Error)} if the period couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/periods")
    @ApiOperation(value = "Update a single period")
    public CustomApiResponse updatePeriod(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to update Period : {}", financialYear);
        if (financialYear.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialYear result = Service.save(financialYear);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /periods} : get all the periods.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of periods in body.
     */
    @GetMapping("/periods")
    @ApiOperation(value = "Get all periods with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllPeriods(PeriodCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Periods by criteria: {}", criteria);
        Page<FinancialYear> page = periodQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /periods/count} : count all the periods.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/periods/count")
    @ApiOperation(value = "Count periods ; Optional filters can be applied")
    public CustomApiResponse countPeriods(PeriodCriteria criteria) {
        log.debug("REST request to count Periods by criteria: {}", criteria);
        return CustomApiResponse.ok(periodQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /periods/:id} : get the "id" period.
     *
     * @param id the id of the period to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the period, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/periods/{id}")
    @ApiOperation("Get a single period by ID")
    public CustomApiResponse getPeriod(@PathVariable Long id) {
        log.debug("REST request to get Period : {}", id);
        Optional<FinancialYear> period = Service.findOne(id);
        return CustomApiResponse.ok(period);
    }

    /**
     * {@code DELETE  /periods/:id} : delete the "id" period.
     *
     * @param id the id of the period to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/periods/{id}")
    @ApiOperation("Delete a single period by ID")
    public CustomApiResponse deletePeriod(@PathVariable Long id) {
        log.debug("REST request to delete Period : {}", id);
        Service.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
