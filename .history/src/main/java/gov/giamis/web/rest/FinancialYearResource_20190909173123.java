package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.FinancialYear;
import gov.giamis.service.FinancialYearService;
import gov.giamis.service.dto.FinancialYearCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.FinancialYearQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link FinancialYear}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "financialYear management Resource", description = "Operations to manage financialYears")
public class FinancialYearResource {

    private final Logger log = LoggerFactory.getLogger(FinancialYearResource.class);

    private static final String ENTITY_NAME = "financialYear";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FinancialYearService financialYearService;

    private final FinancialYearQueryService financialYearQueryService;

    public FinancialYearResource(FinancialYearService financialYearService, FinancialYearQueryService financialYearQueryService) {
        this.financialYearService = financialYearService;
        this.financialYearQueryService = financialYearQueryService;
    }

    /**
     * {@code POST  /financialYears} : Create a new financialYear.
     *
     * @param financialYear the financialYear to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new financialYear, or with status {@code 400 (Bad Request)} if the financialYear has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/financialYears")
    @ApiOperation(value = "Create a new financialYear")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createfinancialYear(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to save financialYear : {}", financialYear);
        if (financialYear.getId() != null) {
            throw new BadRequestAlertException("A new financialYear cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialYear result = financialYearService.save(financialYear);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /financialYears} : Updates an existing financialYear.
     *
     * @param financialYear the financialYear to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated financialYear,
     * or with status {@code 400 (Bad Request)} if the financialYear is not valid,
     * or with status {@code 500 (Internal Server Error)} if the financialYear couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/financialYears")
    @ApiOperation(value = "Update a single financialYear")
    public CustomApiResponse updateFinancialYear(@Valid @RequestBody FinancialYear financialYear) throws URISyntaxException {
        log.debug("REST request to update financialYear : {}", financialYear);
        if (financialYear.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialYear result = financialYearService.save(financialYear);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /financialYears} : get all the financialYears.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of financialYears in body.
     */
    @GetMapping("/financialYears")
    @ApiOperation(value = "Get all financialYears with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllFinancialYears(FinancialYearCriteria criteria, Pageable pageable) {
        log.debug("REST request to get financialYears by criteria: {}", criteria);
        Page<FinancialYear> page = financialYearQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /financialYears/count} : count all the financialYears.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/financialYears/count")
    @ApiOperation(value = "Count financialYears ; Optional filters can be applied")
    public CustomApiResponse countFinancialYears(FinancialYearCriteria criteria) {
        log.debug("REST request to count financialYears by criteria: {}", criteria);
        return CustomApiResponse.ok(financialYearQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /financialYears/:id} : get the "id" financialYear.
     *
     * @param id the id of the financialYear to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the financialYear, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/financialYears/{id}")
    @ApiOperation("Get a single financialYear by ID")
    public CustomApiResponse getFinancialYear(@PathVariable Long id) {
        log.debug("REST request to get financialYear : {}", id);
        Optional<FinancialYear> financialYear = financialYearService.findOne(id);
        return CustomApiResponse.ok(financialYear);
    }

    /**
     * {@code DELETE  /financialYears/:id} : delete the "id" financialYear.
     *
     * @param id the id of the financialYear to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/financialYears/{id}")
    @ApiOperation("Delete a single financialYear by ID")
    public CustomApiResponse financialYearfinancialYear(@PathVariable Long id) {
        log.debug("REST request to delete financialYear : {}", id);
        financialYearService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
