package gov.giamis.config;

import gov.giamis.data.OrgUnitUploadDTO;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.OrganisationUnitLevel;
import gov.giamis.repository.setup.OrganisationUnitLevelRepository;
import gov.giamis.repository.setup.OrganisationUnitRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.util.List;

@Configuration
public class OrgUnitImportConfig {

//    private final OrganisationUnitRepository orgUnitRepository;
//
//    private final OrganisationUnitLevelRepository orgUnitLevelRepository;
//
//    public OrgUnitImportConfig(OrganisationUnitRepository orgUnitRepository,
//                               OrganisationUnitLevelRepository orgUnitLevelRepository) {
//        this.orgUnitRepository = orgUnitRepository;
//        this.orgUnitLevelRepository = orgUnitLevelRepository;
//    }
//
//
//    @Bean
//    public Job orgUnitUploadJob(JobBuilderFactory jobBuilderFactory,
//                                StepBuilderFactory stepBuilderFactory,
//                                ItemReader<OrgUnitUploadDTO> orgUnitCsvFileReader,
//                                ItemProcessor<OrgUnitUploadDTO, OrganisationUnit> orgUnitUploadProcessor,
//                                ItemWriter<OrganisationUnit> orgUnitUploadWriter){
//
//        Step step = stepBuilderFactory.get("ETL-file-load")
//            .<OrgUnitUploadDTO, OrganisationUnit>chunk(100)
//            .reader(orgUnitCsvFileReader)
//            .processor(orgUnitUploadProcessor)
//            .writer(orgUnitUploadWriter)
//            .build();
//        return jobBuilderFactory.get("ETL-load")
//            .incrementer(new RunIdIncrementer())
//            .start(step)
//            .build();
//
//    }
//
//    @Bean
//    @StepScope
//    public FlatFileItemReader<OrgUnitUploadDTO> orgUnitCsvFileReader(@Value("#{jobParameters[filePath]}") String path){
//        FlatFileItemReader<OrgUnitUploadDTO> flatFileItemReader = new FlatFileItemReader<>();
//        flatFileItemReader.setResource(new FileSystemResource(path));
//        flatFileItemReader.setName("CSV-reader");
//        flatFileItemReader.setLinesToSkip(1);
//        flatFileItemReader.setLineMapper(userCsvLineMapper());
//
//        return  flatFileItemReader;
//    }
//
//    @Bean
//    public LineMapper<OrgUnitUploadDTO> userCsvLineMapper(){
//
//        DefaultLineMapper<OrgUnitUploadDTO> defaultLineMapper = new DefaultLineMapper<>();
//        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
//        delimitedLineTokenizer.setDelimiter(",");
//        delimitedLineTokenizer.setStrict(false);
//        delimitedLineTokenizer.setNames("name", "code","level","parent");
//
//        BeanWrapperFieldSetMapper<OrgUnitUploadDTO> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
//        fieldSetMapper.setTargetType(OrgUnitUploadDTO.class);
//        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
//        defaultLineMapper.setFieldSetMapper(fieldSetMapper);
//        return defaultLineMapper;
//    }
//
//    @Component
//    public class OrgUnitUploadProcessor implements ItemProcessor<OrgUnitUploadDTO, OrganisationUnit> {
//
//        @Override
//        public OrganisationUnit process(OrgUnitUploadDTO orgUnitDTO) throws Exception {
//            System.out.println(orgUnitDTO);
//            OrganisationUnit orgUnit = new OrganisationUnit();
//            orgUnit.setName(orgUnitDTO.getName());
//            orgUnit.setCode(orgUnitDTO.getCode());
//            OrganisationUnitLevel level = orgUnitLevelRepository.findByCode(orgUnitDTO.getLevel());
//            if (level == null) {
//                throw  new Exception(orgUnitDTO.getName().concat(" has invalid level; Level doest exist"));
//            }
//            orgUnit.setOrganisationUnitLevel(level);
//
//            if (orgUnitDTO.getParent() != null && !orgUnitDTO.getParent().isEmpty()) {
//                OrganisationUnit parent = orgUnitRepository.findByCode(orgUnitDTO.getParent());
//                if (parent == null) {
//                    throw new Exception(orgUnitDTO.getName().concat(" has invalid parent; Parent doest exist"));
//                }
//                orgUnit.setParentOrganisationUnit(parent);
//            }
//
//            return orgUnit;
//        }
//
//
//    }
//
//    @Component
//    public class OrgUnitUploadWriter implements ItemWriter<OrganisationUnit> {
//
//        @Override
//        public void write(List<? extends OrganisationUnit> orgUnits) throws Exception {
//            for (OrganisationUnit orgUnit:orgUnits) {
//                orgUnitRepository.save(orgUnit);
//            }
//        }
//    }


}
