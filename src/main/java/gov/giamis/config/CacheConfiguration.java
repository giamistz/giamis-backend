package gov.giamis.config;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import gov.giamis.domain.engagement.EngFindingRecommendation;
import gov.giamis.domain.auditplan.*;
import gov.giamis.domain.engagement.*;
import gov.giamis.domain.program.*;
import gov.giamis.domain.risk.*;
import gov.giamis.domain.setup.*;
import gov.giamis.repository.setup.UserRepository;
import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, UserRepository.ORGUNIT_BY_PARENTS);
            createCache(cm, UserRepository.AUTHORITIES_BY_EMAIL_CACHE);
            createCache(cm, User.class.getName());
            createCache(cm, Authority.class.getName());
            createCache(cm, User.class.getName() + ".authorities");
            createCache(cm, Role.class.getName());
            createCache(cm, Role.class.getName() + ".authorities");
            createCache(cm, Role.class.getName() + ".users");
            createCache(cm, FileResourceType.class.getName());
            createCache(cm, FileResource.class.getName());
            createCache(cm, OrganisationUnitLevel.class.getName());
            createCache(cm, OrganisationUnit.class.getName());
            createCache(cm, OrganisationUnit.class.getName() + ".orgUnitGroups");
            createCache(cm, OrganisationUnitGroupSet.class.getName());
            createCache(cm, OrganisationUnit.class.getName());
            createCache(cm, OrganisationUnitGroup.class.getName() + ".orgUnitGroupSets");
            createCache(cm, AuditableArea.class.getName());
            createCache(cm, AuditableArea.class.getName() + ".orgUnitGroups");
            createCache(cm, FinancialYear.class.getName());
            createCache(cm, Quarter.class.getName());
            createCache(cm, RiskRank.class.getName());
            createCache(cm, OrganisationUnitGroupSet.class.getName());
            createCache(cm, OrganisationUnitGroup.class.getName());
            createCache(cm, AuditableArea.class.getName() + ".organisationUnitGroups");
            createCache(cm, Target.class.getName());
            createCache(cm, RiskRegister.class.getName());
            createCache(cm, Risk.class.getName());
            createCache(cm, Risk.class.getName() + ".riskRatings");
            createCache(cm, Risk.class.getName() + ".controls");
            createCache(cm, RiskRating.class.getName());
            createCache(cm, AuditPlan.class.getName());
            createCache(cm, AuditPlanActivity.class.getName());
            createCache(cm, AuditPlanActivity.class.getName() + ".quarters");
            createCache(cm, AuditPlanActivityQuarter.class.getName());
            createCache(cm, AuditPlanActivityInput.class.getName());
            createCache(cm, StrategicAuditPlan.class.getName());
            createCache(cm, StrategicAuditPlanSchedule.class.getName());
            createCache(cm, StrategicAuditPlanScheduleDay.class.getName());
            createCache(cm, Control.class.getName());
            createCache(cm, GfsCode.class.getName());
            createCache(cm, Engagement.class.getName());
            createCache(cm, EngagementMeeting.class.getName());
            createCache(cm, MeetingAttachment.class.getName());
            createCache(cm, EngagementMember.class.getName());
            createCache(cm, EngagementObjective.class.getName());
            createCache(cm, EngagementExitMeeting.class.getName());
            createCache(cm, EngagementIntermMeeting.class.getName());
            createCache(cm, EngagementTeamMeeting.class.getName());
            createCache(cm, EngagementEntranceMeeting.class.getName());
            createCache(cm, MeetingDeliverable.class.getName());
            createCache(cm, gov.giamis.domain.setup.ControlType.class.getName());
            createCache(cm, gov.giamis.domain.engagement.EngagementResource.class.getName());
            createCache(cm, gov.giamis.domain.engagement.EngagementObjectiveRisk.class.getName());
            createCache(cm, InternalControl.class.getName());
            createCache(cm, gov.giamis.domain.risk.RiskControlMatrix.class.getName());
            createCache(cm, gov.giamis.domain.risk.RiskControlReport.class.getName());
            createCache(cm, gov.giamis.domain.engagement.EngagementProcedure.class.getName());
            createCache(cm, gov.giamis.domain.engagement.Finding.class.getName());
            createCache(cm, gov.giamis.domain.Notification.class.getName());
            createCache(cm, EngagementReport.class.getName());
            createCache(cm, MatterOfNextAudit.class.getName());
            createCache(cm, AuditProgramEngagement.class.getName());
            createCache(cm, AuditProgramObjective.class.getName());
            createCache(cm, AuditProgramRisk.class.getName());
            createCache(cm, AuditProgramControl.class.getName());
            createCache(cm, AuditProgramProcedure.class.getName());
            createCache(cm, gov.giamis.domain.HighQualification.class.getName());
            createCache(cm, gov.giamis.domain.ProfessionalQualifcation.class.getName());
            createCache(cm, EngFindingRecommendation.class.getName());
            createCache(cm, EngFindingRecommendationComment.class.getName());
            createCache(cm, gov.giamis.domain.EngagementSampling.class.getName());
            createCache(cm, gov.giamis.domain.Position.class.getName());
            createCache(cm, CommunicationLetter.class.getName());
            createCache(cm, gov.giamis.domain.Followup.class.getName());
            createCache(cm, gov.giamis.domain.Followup.class.getName() + ".engagements");
            createCache(cm, gov.giamis.domain.QuarterReport.class.getName());
            createCache(cm, gov.giamis.domain.QuarterConsolidationReport.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
