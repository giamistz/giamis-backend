package gov.giamis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Giamis.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String fileStorageBaseDir;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private String baseUrl = "http://127.0.0.1:8080";

    public String getFileStorageBaseDir() {
        return fileStorageBaseDir;
    }

    public void setFileStorageBaseDir(String fileStorageBaseDir) {
        this.fileStorageBaseDir = fileStorageBaseDir;
    }
}
