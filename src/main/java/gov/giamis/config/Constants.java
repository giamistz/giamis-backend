package gov.giamis.config;

/**
 * Application constants.
 */
public final class Constants {

	// Regex for acceptable logins
	public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

	public static final String SYSTEM_ACCOUNT = "system";
	public static final String DEFAULT_LANGUAGE = "en";
    public static final String BASE_URL = "baseUrl";
    public static final String API_VERSION = "apiVersion";
	public static final String ANONYMOUS_USER = "anonymoususer@localhost";
	public static final String API_V1 = "/api/v1";
	public static final String CREATE_SUCCESS = " Created successfully";
	public static final String ALREADY_CREATE_SUCCESS = " Cannot Create, Data already Exists";
	public static final String UPDATE_SUCCESS = " Updated successfully";
	public static final String DELETE_SUCCESS = " Deleted successfully";
	public static final String APPROVE_SUCCESS = " Approved successfully";
	public static final String EXISTS = " Already Exists";
	public static final String ENTITY_NOT_FOUND = " Specified entity not found";
    public static final Integer ACTIVE = 1;
    public static final Integer INACTIVE = 0;
    
    public static final String ASSIGN_SUCCESS = " Assigned successfully";
    
    public static final String ROLE_AUDITOR = "AUDITOR";
    
    public static final String OK ="OK";
    public static final String FA ="FA";
    public static final String USER = "user";

    public static final String ENGAGEMENT = "engagement";
    public static final String  ENGAGEMENT_MEMBER = "engagementMember";
    public static final String ENG_MEMBER_LETTER_TEMP = "mail/engagementMemberEmail";
    public static final String ENG_LETTER_TEMP = "mail/engagementEmail";
    public static final String TRAN_LETTER_TEMP = "mail/transmittalEmail";

    public static final String ROLE_CODE ="R0";
    
    public static final String WPRF ="B";
    
    public static final String SUPER_ADMINISTRATOR = "Super Administrator";
    
    public static final String FULL_IMPLEMENTED = "Full Implemented";
    public static final String PARTIAL_IMPLEMENTED = "Partial Implemented";
    public static final String NOT_IMPLEMENTED = "Not Implemented";
    public static final String ENGAGEMENT_MEMBERS = "members";

    private Constants() {
	}
}
