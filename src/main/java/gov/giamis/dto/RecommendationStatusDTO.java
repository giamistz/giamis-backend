package gov.giamis.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RecommendationStatusDTO {

    @Id
    private String status;

    private Long value;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
