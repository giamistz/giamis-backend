package gov.giamis.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class AuditableAreaQuarterValueDTO implements Serializable {

    @Id
    private String quarter;

    private Long value;

    @Id
    @Column(name = "auditable_area")
    private String auditableArea;


    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getAuditableArea() {
        return auditableArea;
    }

    public void setAuditableArea(String auditableArea) {
        this.auditableArea = auditableArea;
    }
}
