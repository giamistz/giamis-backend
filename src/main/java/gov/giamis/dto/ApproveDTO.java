package gov.giamis.dto;

import gov.giamis.domain.setup.FileResource;

import javax.validation.constraints.NotNull;

public class ApproveDTO {

    @NotNull
    private Long id;

    private Long fileResourceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(Long fileResourceId) {
        this.fileResourceId = fileResourceId;
    }
}
