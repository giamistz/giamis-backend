package gov.giamis.dto;

import javax.validation.constraints.NotNull;

public class ConfirmGoDTO {

    @NotNull
    private Long engagementId;

    @NotNull
    private boolean go;

    public Long getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(Long engagementId) {
        this.engagementId = engagementId;
    }

    public boolean isGo() {
        return go;
    }

    public void setGo(boolean go) {
        this.go = go;
    }
}
