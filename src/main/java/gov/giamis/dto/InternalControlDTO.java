package gov.giamis.dto;

import gov.giamis.domain.enumeration.ControlScore;

public class InternalControlDTO {

    private Long id;
    private String wtRef;
    private ControlScore score;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWtRef() {
        return wtRef;
    }

    public void setWtRef(String wtRef) {
        this.wtRef = wtRef;
    }

    public ControlScore getScore() {
        return score;
    }

    public void setScore(ControlScore score) {
        this.score = score;
    }
}
