package gov.giamis.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

public class WalkThroughDTO {

    @NotNull
    private Long engagementId;

    @NotNull
    private Boolean controlAdequacy;

    @NotNull
    private List<InternalControlDTO> intControls;

    public Long getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(Long engagementId) {
        this.engagementId = engagementId;
    }

    public Boolean getControlAdequacy() {
        return controlAdequacy;
    }

    public void setControlAdequacy(Boolean controlAdequacy) {
        this.controlAdequacy = controlAdequacy;
    }

    public List<InternalControlDTO> getIntControls() {
        return intControls;
    }

    public void setIntControls(List<InternalControlDTO> intControls) {
        this.intControls = intControls;
    }
}
