package gov.giamis.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class QuarterValueDTO {

    @Id
    private String quarter;

    private Long value;


    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }
}
