package gov.giamis.dto;

import gov.giamis.domain.enumeration.LetterType;

import javax.validation.constraints.NotNull;

public class ClientNotificationDTO {

    @NotNull
    private Long engagementId;
    private String email;

    @NotNull
    private LetterType type;

    public Long getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(Long engagementId) {
        this.engagementId = engagementId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LetterType getType() {
        return type;
    }

    public void setType(LetterType type) {
        this.type = type;
    }
}
