package gov.giamis.web.rest.auditplan;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;
import gov.giamis.service.auditplan.AuditPlanActivityQuarterQueryService;
import gov.giamis.service.auditplan.AuditPlanActivityQuarterService;
import gov.giamis.service.auditplan.dto.AuditPlanActivityQuarterCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditPlanActivityQuarter}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "AuditPlan Activity Quarter Resource", description = "Operations to manage AuditPlan  Activity Quarter")
public class AuditPlanActivityQuarterResource {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityQuarterResource.class);

    private static final String ENTITY_NAME = "auditPlanActivityQuarter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditPlanActivityQuarterService auditPlanActivityQuarterService;

    private final AuditPlanActivityQuarterQueryService auditPlanActivityQuarterQueryService;

    public AuditPlanActivityQuarterResource(AuditPlanActivityQuarterService auditPlanActivityQuarterService,
                                            AuditPlanActivityQuarterQueryService auditPlanActivityQuarterQueryService) {
        this.auditPlanActivityQuarterService = auditPlanActivityQuarterService;
        this.auditPlanActivityQuarterQueryService = auditPlanActivityQuarterQueryService;
    }
    /**
     * {@code POST  /audit-plan-activity-quarters} : Create a new auditPlanActivityQuarter.
     *
     * @param auditPlanActivityQuarter the auditPlanActivityQuarter to create.
     */
    @PostMapping("/audit-plan-activity-quarters")
    @ApiOperation(value = "Create AuditPlan Activity Quarter")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createAuditPlanScheduleQuarter(@Valid @RequestBody AuditPlanActivityQuarter auditPlanActivityQuarter) {
        log.debug("REST request to save AuditPlan Activity Quarter : {}", auditPlanActivityQuarter);
        if (auditPlanActivityQuarter.getId() != null) {
            throw new BadRequestAlertException("A new AuditPlan Activity Quarter already have an ID", ENTITY_NAME, "idexists");
        }
        AuditPlanActivityQuarter result = auditPlanActivityQuarterService.save(auditPlanActivityQuarter);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }
    /**
     * {@code PUT  /audit-plan-activity-quarters} : Updates an existing auditPlanActivityQuarter.
     *
     * @param auditPlanActivityQuarter the auditPlanActivityQuarter to update.
     *                                 or with status {@code 400 (Bad Request)} if the auditPlanActivityQuarter is not valid,
     *                                 or with status {@code 500 (Internal Server Error)} if the auditPlanActivityQuarter couldn't be updated.
     */
    @PutMapping("/audit-plan-activity-quarters")
    @ApiOperation(value = "Update existing AuditPlan Activity Quarter")
    public CustomApiResponse updateAuditPlanScheduleQuarter(@Valid @RequestBody AuditPlanActivityQuarter auditPlanActivityQuarter){
        log.debug("REST request to update AuditPlanActivityQuarter : {}", auditPlanActivityQuarter);

        if (auditPlanActivityQuarter.getId() == null) {
			throw new BadRequestAlertException("Updated AuditPlan Activity Quarter does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!auditPlanActivityQuarterService.findOne(auditPlanActivityQuarter.getId()).isPresent()) {
			throw new BadRequestAlertException("AuditPlan Activity Quarter with ID: " + auditPlanActivityQuarter.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		
        AuditPlanActivityQuarter result = auditPlanActivityQuarterService.save(auditPlanActivityQuarter);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }
    /**
     * {@code GET  /audit-plan-activity-quarters} : get all the auditPlanScheduleQuarters.
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     */
    @GetMapping("/audit-plan-activity-quarters")
    @ApiOperation(value = "Get all AuditPlan Activity Quarter; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllAuditPlanScheduleQuarters(AuditPlanActivityQuarterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditPlan Activity Quarters by criteria: {}", criteria);
        Page<AuditPlanActivityQuarter> page = auditPlanActivityQuarterQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }
    /**
     * {@code GET  /audit-plan-activity-quarters/count} : count all the auditPlanScheduleQuarters.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/audit-plan-activity-quarters/count")
    @ApiOperation("Count all AuditPlan Activity Quarter; Optional filters can be applied")
    public CustomApiResponse countAuditPlanScheduleQuarters(AuditPlanActivityQuarterCriteria criteria) {
        log.debug("REST request to count AuditPlan Activity Quarters by criteria: {}", criteria);
        return CustomApiResponse.ok(auditPlanActivityQuarterQueryService.countByCriteria(criteria));
    }
    /**
     * {@code GET  /audit-plan-activity-quarters/:id} : get the "id" auditPlanScheduleQuarter.
     * @param id the id of the auditPlanScheduleQuarter to retrieve.
     */
    @GetMapping("/audit-plan-activity-quarters/{id}")
    @ApiOperation("Get a single AuditPlan Activity Quarters by ID")
    public CustomApiResponse getAuditPlanScheduleQuarter(@PathVariable Long id) {
        log.debug("REST request to get AuditPlan Activity Quarter : {}", id);
        if (!auditPlanActivityQuarterService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("AuditPlan Activity Quarter with ID: " + id + " not found", ENTITY_NAME, "id does not exists");
        }
        Optional<AuditPlanActivityQuarter> auditPlanScheduleQuarter = auditPlanActivityQuarterService.findOne(id);
        return CustomApiResponse.ok(auditPlanScheduleQuarter);
    }
    /**
     * {@code DELETE  /audit-plan-activity-quarters/:id} : delete the "id" auditPlanScheduleQuarter.
     * @param id the id of the auditPlanScheduleQuarter to delete.
     */
    @DeleteMapping("/audit-plan-activity-quarters/{id}")
    @ApiOperation("Delete a single AuditPlan Activity Quarter by ID")
    public CustomApiResponse deleteAuditPlanScheduleQuarter(@PathVariable Long id) {
        log.debug("REST request to delete AuditPlan Activity Quarter : {}", id);
        if (!auditPlanActivityQuarterService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("AuditPlan Activity Quarter with ID: " + id + " not found", ENTITY_NAME, "id does not exists");
        }
        auditPlanActivityQuarterService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
