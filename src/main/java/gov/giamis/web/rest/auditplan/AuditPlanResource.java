package gov.giamis.web.rest.auditplan;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import gov.giamis.dto.ApproveDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.AuditPlan;
import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.domain.auditplan.AuditPlanActivityInput;
import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;
import gov.giamis.service.auditplan.AuditPlanActivityInputService;
import gov.giamis.service.auditplan.AuditPlanActivityQuarterService;
import gov.giamis.service.auditplan.AuditPlanActivityService;
import gov.giamis.service.auditplan.AuditPlanQueryService;
import gov.giamis.service.auditplan.AuditPlanService;
import gov.giamis.service.auditplan.dto.AuditPlanCriteria;
import gov.giamis.service.auditplan.dto.AuditPlanDTO;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link AuditPlan}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "AuditPlan Resource", description = "Operations to manage AuditPlan")
public class AuditPlanResource {

    private final Logger log = LoggerFactory.getLogger(AuditPlanResource.class);

    private static final String ENTITY_NAME = "auditPlan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditPlanService auditPlanService;

    private final AuditPlanQueryService auditPlanQueryService;

    private final AuditPlanActivityService auditPlanActivityService;
    private final AuditPlanActivityInputService auditPlanActivityInputService;
    private final AuditPlanActivityQuarterService auditPlanActivityQuarterService;

    AuditPlanDTO auditPlanDto = null;

    public AuditPlanResource(AuditPlanService auditPlanService,
    		AuditPlanQueryService auditPlanQueryService,
    		AuditPlanActivityService auditPlanActivityService,
    		 AuditPlanActivityInputService auditPlanActivityInputService,
    		 AuditPlanActivityQuarterService auditPlanActivityQuarterService) {
        this.auditPlanService = auditPlanService;
        this.auditPlanQueryService = auditPlanQueryService;
        this.auditPlanActivityService = auditPlanActivityService;
        this.auditPlanActivityInputService = auditPlanActivityInputService;
        this.auditPlanActivityQuarterService = auditPlanActivityQuarterService;
    }

    /**
     * {@code POST  /audit-plans} : Create a new auditPlan.
     *
     * @param auditPlan the auditPlan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditPlan, or with status {@code 400 (Bad Request)} if the auditPlan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-plans")
    @ApiOperation(value = "Create Audit Plan")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createAuditPlan(@Valid @RequestBody AuditPlan auditPlan) throws URISyntaxException {
        log.debug("REST request to save AuditPlan : {}", auditPlan);
        if (auditPlan.getId() != null) {
            throw new BadRequestAlertException("A new AuditPlan already have an ID", ENTITY_NAME, "id exists");
        }
        AuditPlan result = auditPlanService.save(auditPlan);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-plans} : Updates an existing auditPlan.
     *
     * @param auditPlan the auditPlan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditPlan,
     * or with status {@code 400 (Bad Request)} if the auditPlan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditPlan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-plans")
    @ApiOperation(value = "Update existing Audit Plan")
    public CustomApiResponse updateAuditPlan(@Valid @RequestBody AuditPlan auditPlan) throws URISyntaxException {
        log.debug("REST request to update AuditPlan : {}", auditPlan);

        if (auditPlan.getId() == null) {
			throw new BadRequestAlertException("Updated Audit Plan does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!auditPlanService.findOne(auditPlan.getId()).isPresent()) {
			throw new BadRequestAlertException("Audit Plan with ID: " + auditPlan.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}

        AuditPlan result = auditPlanService.save(auditPlan);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-plans} : get all the auditPlans.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditPlans in body.
     */
    @GetMapping("/audit-plans")
    @ApiOperation(value = "Get all Audit Plans; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllAuditPlans(AuditPlanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditPlans by criteria: {}", criteria);
        Page<AuditPlan> page = auditPlanQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-plans/count} : count all the auditPlans.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-plans/count")
    @ApiOperation(value = "Count all Audit Plans; Optional filters can be applied")
    public CustomApiResponse countAuditPlans(AuditPlanCriteria criteria) {
        log.debug("REST request to count AuditPlans by criteria: {}", criteria);
        return CustomApiResponse.ok(auditPlanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-plans/:id} : get the "id" auditPlan.
     *
     * @param id the id of the auditPlan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditPlan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-plans/{id}")
    @ApiOperation("Get a single Audit Plan by ID")
    public CustomApiResponse getAuditPlan(@PathVariable Long id) {
        log.debug("REST request to get AuditPlan : {}", id);

        if (!auditPlanService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Audit Plan with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

        Optional<AuditPlan> auditPlan = auditPlanService.findOne(id);
        return CustomApiResponse.ok(auditPlan);
    }

    @PutMapping("/approve-auditplan")
    @ApiOperation(value = "Approve existing AuditPlan by ID")
    public CustomApiResponse approveAuditPlanById(@RequestParam(value ="id") Long id) throws URISyntaxException {
        log.debug("REST request to approve existing AuditPlan: {}");

        if (!auditPlanService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("AuditPlan with ID Id: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        auditPlanService.updateAuditPlanById(id);

        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }

    @GetMapping("/auditplan-checklist")
    @ApiOperation(value = "Get existing AuditPlan by ID")
    public CustomApiResponse getAuditPlanStatus(@RequestParam(value ="id") Long id) throws URISyntaxException {
        log.debug("REST request to get status of AuditPlan and its children : {}");
        AuditPlanActivity auditPlanActivity = null;
        List<AuditPlanActivityInput> auditPlanActivityInput = null;
        List<AuditPlanActivityQuarter> auditPlanActivityQuarter = null;

         auditPlanDto = new AuditPlanDTO();

        if(auditPlanService.findAuditPlanById(id)!= null) {
        	AuditPlan auditPlan = auditPlanService.findAuditPlanById(id);
        	if(auditPlan.getName() != null) {
        		auditPlanDto.setHasName(true);
        	}

        	if(auditPlan.getPurpose()!= null ) {
        		auditPlanDto.setHasPurpose(true);
        	}

        	if(auditPlan.getObjective()!= null) {
        		auditPlanDto.setHasObjective(true);
        	}

        	if(auditPlan.getMethodology()!= null) {
        		auditPlanDto.setHasMethodology(true);
        	}

        	if(auditPlan.getListOfAbbreviation()!= null) {
        		auditPlanDto.setHasListofAbbreviation(true);
        	}

        	if(auditPlan.getOtherResources()!= null) {
        		auditPlanDto.setHasOtherResources(true);
        	}
        }
        if(auditPlanActivityService.findAuditPlanActivityByAuditPlanId(id)!= null) {
        	 auditPlanActivity = auditPlanActivityService.findAuditPlanActivityByAuditPlanId(id);
        	auditPlanDto.setHasAuditPlanActivity(true);
        }else {
        	auditPlanDto.setHasAuditPlanActivity(false);
        }

        if(auditPlanActivity != null) {
        	auditPlanActivityInput = auditPlanActivityInputService.findAuditPlanActivityInputByAuditPlanActivityId(auditPlanActivity.getId());
        	 boolean isAuditPlanActivityInput = auditPlanActivityInput.isEmpty();
        	if(isAuditPlanActivityInput == true) {
        		auditPlanDto.setHasAuditPlanActivityInput(false);
        	}else {
        		auditPlanDto.setHasAuditPlanActivityInput(true);
        	}

        	auditPlanActivityQuarter = auditPlanActivityQuarterService.findAuditPlanActivityQuarterByAuditPlanActivityId(auditPlanActivity.getId());
        	boolean isAuditPlanActivityQuarter = auditPlanActivityQuarter.isEmpty();
        	if(isAuditPlanActivityQuarter == true) {
        		auditPlanDto.setHasAuditPlanActivityQuarter(false);
        	}else {
        		auditPlanDto.setHasAuditPlanActivityQuarter(true);
        	}
        }
       return CustomApiResponse.ok(auditPlanDto);
    }
    /**
     * {@code DELETE  /audit-plans/:id} : delete the "id" auditPlan.
     *
     * @param id the id of the auditPlan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-plans/{id}")
    @ApiOperation("Delete  a single Audit Plan by ID")
    public CustomApiResponse deleteAuditPlan(@PathVariable Long id) {
        log.debug("REST request to delete AuditPlan : {}", id);

        if (!auditPlanService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Audit Plan with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        auditPlanService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @PostMapping("/audit-plans/approve")
    public CustomApiResponse approve(@RequestBody ApproveDTO approveDTO) {
        auditPlanService.approve(approveDTO);
        return CustomApiResponse.ok("Risk based audit plan approved successfully");
    }
}
