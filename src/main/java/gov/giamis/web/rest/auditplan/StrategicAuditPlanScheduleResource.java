package gov.giamis.web.rest.auditplan;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleQueryService;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleService;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanScheduleCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link StrategicAuditPlanSchedule}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Strategic Audit Plan Schedule Resource", description = "Operations to manage Strategic Audit Plan Schedule")
public class StrategicAuditPlanScheduleResource {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleResource.class);

    private static final String ENTITY_NAME = "strategicAuditPlanSchedule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StrategicAuditPlanScheduleService strategicAuditPlanScheduleService;

    private final StrategicAuditPlanScheduleQueryService strategicAuditPlanScheduleQueryService;

    public StrategicAuditPlanScheduleResource(StrategicAuditPlanScheduleService strategicAuditPlanScheduleService, StrategicAuditPlanScheduleQueryService strategicAuditPlanScheduleQueryService) {
        this.strategicAuditPlanScheduleService = strategicAuditPlanScheduleService;
        this.strategicAuditPlanScheduleQueryService = strategicAuditPlanScheduleQueryService;
    }

    /**
     * {@code POST  /strategic-audit-plan-schedules} : Create a new strategicAuditPlanSchedule.
     *
     * @param strategicAuditPlanSchedule the strategicAuditPlanSchedule to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new strategicAuditPlanSchedule, or with status {@code 400 (Bad Request)} if the strategicAuditPlanSchedule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/strategic-audit-plan-schedules")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create Strategic Audit Plan Schedule")
    public CustomApiResponse createStrategicPlanSchedule(@Valid @RequestBody StrategicAuditPlanSchedule strategicAuditPlanSchedule) throws URISyntaxException {
        log.debug("REST request to save StrategicAuditPlanSchedule : {}", strategicAuditPlanSchedule);
        if (strategicAuditPlanSchedule.getId() != null) {
            throw new BadRequestAlertException("A new strategicAuditPlanSchedule already have an ID", ENTITY_NAME, "id exists");
        }
        strategicAuditPlanSchedule.getScheduleDays()
            .forEach(s -> s.setStrategicAuditPlanSchedule(strategicAuditPlanSchedule));

        StrategicAuditPlanSchedule result = strategicAuditPlanScheduleService.save(strategicAuditPlanSchedule);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /strategic-audit-plan-schedules} : Updates an existing strategicAuditPlanSchedule.
     *
     * @param strategicAuditPlanSchedule the strategicAuditPlanSchedule to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated strategicAuditPlanSchedule,
     * or with status {@code 400 (Bad Request)} if the strategicAuditPlanSchedule is not valid,
     * or with status {@code 500 (Internal Server Error)} if the strategicAuditPlanSchedule couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/strategic-audit-plan-schedules")
    @ApiOperation(value = "Update existing Strategic Audit Plan Schedule")
    public CustomApiResponse updateStrategicPlanSchedule(@Valid @RequestBody StrategicAuditPlanSchedule strategicAuditPlanSchedule) throws URISyntaxException {
        log.debug("REST request to update StrategicAuditPlanSchedule : {}", strategicAuditPlanSchedule);
  
        if (strategicAuditPlanSchedule.getId() == null) {
			throw new BadRequestAlertException("Updated Strategic Audit Plan Schedule does not have an ID", ENTITY_NAME,
					"id not exists");
		}
        
        if (!strategicAuditPlanScheduleService.findOne(strategicAuditPlanSchedule.getId()).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule with ID: " + strategicAuditPlanSchedule.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}

        strategicAuditPlanSchedule.getScheduleDays()
            .forEach(s -> s.setStrategicAuditPlanSchedule(strategicAuditPlanSchedule));

        StrategicAuditPlanSchedule result = strategicAuditPlanScheduleService.save(strategicAuditPlanSchedule);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /strategic-audit-plan-schedules} : get all the strategicPlanSchedules.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of strategicPlanSchedules in body.
     */
    @GetMapping("/strategic-audit-plan-schedules")
    @ApiOperation(value = "Update existing Strategic Audit Plan Schedule")
    public CustomApiResponse getAllStrategicPlanSchedules(StrategicAuditPlanScheduleCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StrategicPlanSchedules by criteria: {}", criteria);
        Page<StrategicAuditPlanSchedule> page = strategicAuditPlanScheduleQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /strategic-audit-plan-schedules/count} : count all the strategicPlanSchedules.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/strategic-audit-plan-schedules/count")
    @ApiOperation(value = "Get all  Strategic Audit Plan Schedule; with pagination, optional filters by fields can be applied")
    public CustomApiResponse countStrategicPlanSchedules(StrategicAuditPlanScheduleCriteria criteria) {
        log.debug("REST request to count StrategicPlanSchedules by criteria: {}", criteria);
        return CustomApiResponse.ok(strategicAuditPlanScheduleQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /strategic-audit-plan-schedules/:id} : get the "id" strategicPlanSchedule.
     *
     * @param id the id of the strategicPlanSchedule to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the strategicPlanSchedule, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/strategic-audit-plan-schedules/{id}")
    @ApiOperation(value = "Get a single Strategic Audit Plan Schedule by ID")
    public CustomApiResponse getStrategicPlanSchedule(@PathVariable Long id) {
        log.debug("REST request to get StrategicAuditPlanSchedule : {}", id);
        
        if (!strategicAuditPlanScheduleService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<StrategicAuditPlanSchedule> strategicPlanSchedule = strategicAuditPlanScheduleService.findOne(id);
        return CustomApiResponse.ok(strategicPlanSchedule);
    }

    /**
     * {@code DELETE  /strategic-audit-plan-schedules/:id} : delete the "id" strategicPlanSchedule.
     *
     * @param id the id of the strategicPlanSchedule to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/strategic-audit-plan-schedules/{id}")
    @ApiOperation(value = "Delete a single Strategic Audit Plan Schedule by ID")
    public CustomApiResponse deleteStrategicPlanSchedule(@PathVariable Long id) {
        log.debug("REST request to delete StrategicAuditPlanSchedule : {}", id);
        
        if (!strategicAuditPlanScheduleService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        strategicAuditPlanScheduleService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
