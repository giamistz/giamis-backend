package gov.giamis.web.rest.auditplan;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleDayQueryService;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleDayService;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanScheduleDayCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link StrategicAuditPlanScheduleDay}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Strategic Audit Plan Schedule Days Resource", description = "Operations to manage Strategic Audit Plan Schedule Days")
public class StrategicAuditPlanScheduleDayResource {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleDayResource.class);

    private static final String ENTITY_NAME = "strategicAuditPlanScheduleDay";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StrategicAuditPlanScheduleDayService strategicAuditPlanScheduleDayService;

    private final StrategicAuditPlanScheduleDayQueryService strategicAuditPlanScheduleDayQueryService;

    public StrategicAuditPlanScheduleDayResource(StrategicAuditPlanScheduleDayService strategicAuditPlanScheduleDayService, StrategicAuditPlanScheduleDayQueryService strategicAuditPlanScheduleDayQueryService) {
        this.strategicAuditPlanScheduleDayService = strategicAuditPlanScheduleDayService;
        this.strategicAuditPlanScheduleDayQueryService = strategicAuditPlanScheduleDayQueryService;
    }

    /**
     * {@code POST  /strategic-audit-plan-schedule-days} : Create a new strategicAuditPlanScheduleDay.
     *
     * @param strategicAuditPlanScheduleDay the strategicAuditPlanScheduleDay to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new strategicAuditPlanScheduleDay, or with status {@code 400 (Bad Request)} if the strategicAuditPlanScheduleDay has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/strategic-audit-plan-schedule-days")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create Strategic Audit Plan Schedule Days")
    public CustomApiResponse createStrategicPlanScheduleDay(@Valid @RequestBody StrategicAuditPlanScheduleDay strategicAuditPlanScheduleDay) throws URISyntaxException {
        log.debug("REST request to save StrategicAuditPlanScheduleDay : {}", strategicAuditPlanScheduleDay);
        if (strategicAuditPlanScheduleDay.getId() != null) {
            throw new BadRequestAlertException("A new strategicAuditPlanScheduleDay already have an ID", ENTITY_NAME, "id exists");
        }
        StrategicAuditPlanScheduleDay result = strategicAuditPlanScheduleDayService.save(strategicAuditPlanScheduleDay);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /strategic-audit-plan-schedule-days} : Updates an existing strategicAuditPlanScheduleDay.
     *
     * @param strategicAuditPlanScheduleDay the strategicAuditPlanScheduleDay to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated strategicAuditPlanScheduleDay,
     * or with status {@code 400 (Bad Request)} if the strategicAuditPlanScheduleDay is not valid,
     * or with status {@code 500 (Internal Server Error)} if the strategicAuditPlanScheduleDay couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/strategic-audit-plan-schedule-days")
    @ApiOperation(value = "Update existing Strategic Audit Plan Schedule Days")
    public CustomApiResponse updateStrategicPlanScheduleDay(@Valid @RequestBody StrategicAuditPlanScheduleDay strategicAuditPlanScheduleDay) throws URISyntaxException {
        log.debug("REST request to update StrategicAuditPlanScheduleDay : {}", strategicAuditPlanScheduleDay);
 
        if (strategicAuditPlanScheduleDay.getId() == null) {
			throw new BadRequestAlertException("Updated Strategic Audit Plan Schedule Days does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!strategicAuditPlanScheduleDayService.findOne(strategicAuditPlanScheduleDay.getId()).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule Days with ID: " + strategicAuditPlanScheduleDay.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
        StrategicAuditPlanScheduleDay result = strategicAuditPlanScheduleDayService.save(strategicAuditPlanScheduleDay);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /strategic-audit-plan-schedule-days} : get all the strategicPlanScheduleDays.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of strategicPlanScheduleDays in body.
     */
    @GetMapping("/strategic-audit-plan-schedule-days")
    @ApiOperation(value = "Get all  Strategic Audit Plan Schedule Days; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllStrategicPlanScheduleDays(StrategicAuditPlanScheduleDayCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StrategicPlanScheduleDays by criteria: {}", criteria);
        Page<StrategicAuditPlanScheduleDay> page = strategicAuditPlanScheduleDayQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /strategic-audit-plan-schedule-days/count} : count all the strategicPlanScheduleDays.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/strategic-audit-plan-schedule-days/count")
    @ApiOperation(value = "Count all  Strategic Audit Plan Schedule Days; Optional filters can be applied")
    public CustomApiResponse countStrategicPlanScheduleDays(StrategicAuditPlanScheduleDayCriteria criteria) {
        log.debug("REST request to count StrategicPlanScheduleDays by criteria: {}", criteria);
        return CustomApiResponse.ok(strategicAuditPlanScheduleDayQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /strategic-audit-plan-schedule-days/:id} : get the "id" strategicPlanScheduleDay.
     *
     * @param id the id of the strategicPlanScheduleDay to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the strategicPlanScheduleDay, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/strategic-audit-plan-schedule-days/{id}")
    @ApiOperation(value = "Get a single Strategic Audit Plan Schedule Days by ID")
    public CustomApiResponse getStrategicPlanScheduleDay(@PathVariable Long id) {
        log.debug("REST request to get StrategicAuditPlanScheduleDay : {}", id);
        
        if (!strategicAuditPlanScheduleDayService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule Days with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<StrategicAuditPlanScheduleDay> strategicPlanScheduleDay = strategicAuditPlanScheduleDayService.findOne(id);
        return CustomApiResponse.ok(strategicPlanScheduleDay);
    }

    /**
     * {@code DELETE  /strategic-audit-plan-schedule-days/:id} : delete the "id" strategicPlanScheduleDay.
     *
     * @param id the id of the strategicPlanScheduleDay to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/strategic-audit-plan-schedule-days/{id}")
    @ApiOperation(value = "Delete a single Strategic Audit Plan Schedule Days by ID")
    public CustomApiResponse deleteStrategicPlanScheduleDay(@PathVariable Long id) {
        log.debug("REST request to delete StrategicAuditPlanScheduleDay : {}", id);
        
        if (!strategicAuditPlanScheduleDayService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan Schedule Days with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        strategicAuditPlanScheduleDayService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
