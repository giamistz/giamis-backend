package gov.giamis.web.rest.auditplan;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.AuditPlanActivityInput;
import gov.giamis.service.auditplan.AuditPlanActivityInputService;
import gov.giamis.service.auditplan.dto.AuditPlanActivityInputCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import gov.giamis.service.auditplan.AuditPlanActivityInputQueryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link AuditPlanActivityInput}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "AuditPlan Activity Input  Resource ", description = "Operations to manage AuditPlan Activity Input")
public class AuditPlanActivityInputResource {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityInputResource.class);

    private static final String ENTITY_NAME = "auditPlanActivityInput";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditPlanActivityInputService auditPlanActivityInputService;

    private final AuditPlanActivityInputQueryService auditPlanActivityInputQueryService;

    public AuditPlanActivityInputResource(AuditPlanActivityInputService auditPlanActivityInputService, AuditPlanActivityInputQueryService auditPlanActivityInputQueryService) {
        this.auditPlanActivityInputService = auditPlanActivityInputService;
        this.auditPlanActivityInputQueryService = auditPlanActivityInputQueryService;
    }

    /**
     * {@code POST  /audit-plan-activity-inputs} : Create a new auditPlanActivityInput.
     *
     * @param auditPlanActivityInput the auditPlanActivityInput to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditPlanActivityInput, or with status {@code 400 (Bad Request)} if the auditPlanActivityInput has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-plan-activity-inputs")
    @ApiOperation(value = "create  AuditPlan Activity Input")
	@ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createAuditPlanScheduleCost(@Valid @RequestBody AuditPlanActivityInput auditPlanActivityInput) throws URISyntaxException {
        log.debug("REST request to save AuditPlanActivityInput : {}", auditPlanActivityInput);
        if (auditPlanActivityInput.getId() != null) {
            throw new BadRequestAlertException("A new auditPlanActivityInput already have an ID", ENTITY_NAME, "id exists");
        }
        AuditPlanActivityInput result = auditPlanActivityInputService.save(auditPlanActivityInput);
    	return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-plan-activity-inputs} : Updates an existing auditPlanActivityInput.
     *
     * @param auditPlanActivityInput the auditPlanActivityInput to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditPlanActivityInput,
     * or with status {@code 400 (Bad Request)} if the auditPlanActivityInput is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditPlanActivityInput couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-plan-activity-inputs")
    @ApiOperation(value = "Update existing  AuditPlan Activity Input")
    public CustomApiResponse updateAuditPlanScheduleCost(@Valid @RequestBody AuditPlanActivityInput auditPlanActivityInput) throws URISyntaxException {
        log.debug("REST request to update AuditPlanActivityInput : {}", auditPlanActivityInput);
        if (auditPlanActivityInput.getId() == null) {
        	throw new BadRequestAlertException("Updated AuditPlan Activity Input does not have an ID", ENTITY_NAME,
					"id not exists");
        }

        if (!auditPlanActivityInputService.findOne(auditPlanActivityInput.getId()).isPresent()) {
			throw new BadRequestAlertException("AudiPlan Activity Input with ID: " + auditPlanActivityInput.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}

        AuditPlanActivityInput result = auditPlanActivityInputService.save(auditPlanActivityInput);
    	return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-plan-activity-inputs} : get all the auditPlanScheduleCosts.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditPlanScheduleCosts in body.
     */
    @GetMapping("/audit-plan-activity-inputs")
    @ApiOperation(value = "Get all  AuditPlan Activity Schedule Inputs; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllAuditPlanScheduleCosts(AuditPlanActivityInputCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditPlan Activity Input by criteria: {}", criteria);
        Page<AuditPlanActivityInput> page = auditPlanActivityInputQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-plan-activity-inputs/count} : count all the auditPlanScheduleCosts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-plan-activity-inputs/count")
    @ApiOperation(value = "Count all  AuditPlan Activity Inputs; Optional filters can be applied")
    public CustomApiResponse countAuditPlanScheduleCosts(AuditPlanActivityInputCriteria criteria) {
        log.debug("REST request to count AuditPlan Activity Inputs by criteria: {}", criteria);
        return CustomApiResponse.ok(auditPlanActivityInputQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-plan-activity-inputs/:id} : get the "id" auditPlanScheduleCost.
     *
     * @param id the id of the auditPlanScheduleCost to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditPlanScheduleCost, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-plan-activity-inputs/{id}")
    @ApiOperation(value = "Get a single  AuditPlan Activity Input by ID")
    public CustomApiResponse getAuditPlanScheduleCost(@PathVariable Long id) {
        log.debug("REST request to get AuditPlanActivityInput : {}", id);

        if (!auditPlanActivityInputService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("AuditPlan Activity Input with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

        Optional<AuditPlanActivityInput> auditPlanScheduleCost = auditPlanActivityInputService.findOne(id);

        return CustomApiResponse.ok(auditPlanScheduleCost);
    }

    /**
     * {@code DELETE  /audit-plan-activity-inputs/:id} : delete the "id" auditPlanScheduleCost.
     *
     * @param id the id of the auditPlanScheduleCost to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-plan-activity-inputs/{id}")
    @ApiOperation(value = "Delete a single AuditPlan Activity Input by ID")
    public CustomApiResponse deleteAuditPlanScheduleCost(@PathVariable Long id) {
        log.debug("REST request to delete AuditPlanActivityInput : {}", id);

        if (!auditPlanActivityInputService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("AuditPlan Activity Input with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        auditPlanActivityInputService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
