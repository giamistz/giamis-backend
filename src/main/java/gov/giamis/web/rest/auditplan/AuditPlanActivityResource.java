package gov.giamis.web.rest.auditplan;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.service.auditplan.AuditPlanActivityQueryService;
import gov.giamis.service.auditplan.AuditPlanActivityService;
import gov.giamis.service.auditplan.dto.AuditPlanActivityCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link AuditPlanActivity}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "AuditPlan Activities Resource", description = "Operations to manage AuditPlan Activities")
public class AuditPlanActivityResource {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityResource.class);

    private static final String ENTITY_NAME = "auditPlanActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditPlanActivityService auditPlanActivityService;

    private final AuditPlanActivityQueryService auditPlanActivityQueryService;

    public AuditPlanActivityResource(AuditPlanActivityService auditPlanActivityService, AuditPlanActivityQueryService auditPlanActivityQueryService) {
        this.auditPlanActivityService = auditPlanActivityService;
        this.auditPlanActivityQueryService = auditPlanActivityQueryService;
    }

    /**
     * {@code POST  /audit-plan-activities} : Create a new auditPlanActivity.
     *
     * @param auditPlanActivity the auditPlanActivity to create.
     */
    @PostMapping("/audit-plan-activities")
    @ApiOperation(value = "Create AuditPlan Activity")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createAuditPlanSchedule(@Valid @RequestBody AuditPlanActivity auditPlanActivity){
        log.debug("REST request to save AuditPlanActivity : {}", auditPlanActivity);
        if (auditPlanActivity.getId() != null) {
            throw new BadRequestAlertException("A new AuditPlan Activity already have an ID", ENTITY_NAME, "idexists");
        }
        auditPlanActivity.getQuarters().forEach(q -> q.setAuditPlanActivity(auditPlanActivity));
        AuditPlanActivity result = auditPlanActivityService.save(auditPlanActivity);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }
    /**
     * {@code PUT  /audit-plan-activities} : Updates an existing auditPlanActivity.
     * @param auditPlanActivity the auditPlanActivity to update.
     * or with status {@code 400 (Bad Request)} if the auditPlanActivity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditPlanActivity couldn't be updated.
     */
    @PutMapping("/audit-plan-activities")
    @ApiOperation(value = "Update existing AuditPlan Activity")
    public CustomApiResponse updateAuditPlanSchedule(@Valid @RequestBody AuditPlanActivity auditPlanActivity){
        log.debug("REST request to update AuditPlanActivity : {}", auditPlanActivity);
        if (auditPlanActivity.getId() == null) {
        	throw new BadRequestAlertException("Updated AuditPlan Activity does not have an ID", ENTITY_NAME,
					"id not exists");
        }
        if (!auditPlanActivityService.findOne(auditPlanActivity.getId()).isPresent()) {
            throw new BadRequestAlertException("AuditPlan Activity with ID: " + auditPlanActivity.getId() + " not found", ENTITY_NAME,
                "id does not exists");
        }
        auditPlanActivity.getQuarters().forEach(q -> q.setAuditPlanActivity(auditPlanActivity));
        AuditPlanActivity result = auditPlanActivityService.save(auditPlanActivity);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }
    /**
     * {@code GET  /audit-plan-activities} : get all the auditPlanSchedules.
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     */
    @GetMapping("/audit-plan-activities")
    @ApiOperation(value = "Get all AuditPlan Activities; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllAuditPlanSchedules(AuditPlanActivityCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditPlanSchedules by criteria: {}", criteria);
        Page<AuditPlanActivity> page = auditPlanActivityQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }
    /**
    * {@code GET  /audit-plan-activities/count} : count all the auditPlanSchedules.
    * @param criteria the criteria which the requested entities should match.
    */
    @GetMapping("/audit-plan-activities/count")
    @ApiOperation("Count all AuditPlan Activities; Optional filters can be applied")
    public CustomApiResponse countAuditPlanSchedules(AuditPlanActivityCriteria criteria) {
        log.debug("REST request to count Audit Plan Schedules by criteria: {}", criteria);
        return CustomApiResponse.ok(auditPlanActivityQueryService.countByCriteria(criteria));
    }
    /**
     * {@code GET  /audit-plan-activities/:id} : get the "id" auditPlanSchedule.
     * @param id the id of the auditPlanSchedule to retrieve.
     */
    @GetMapping("/audit-plan-activities/{id}")
    @ApiOperation("Get a single AuditPlan Activity by ID")
    public CustomApiResponse getAuditPlanSchedule(@PathVariable Long id) {
        log.debug("REST request to get Audit Plan Schedule : {}", id);
        if (!auditPlanActivityService.findOne(id) .isPresent()) {
            throw new BadRequestAlertException("AuditPlan Activity with ID: " + id + " not found", ENTITY_NAME, "id does not exists");
        }
        Optional<AuditPlanActivity> auditPlanSchedule = auditPlanActivityService.findOne(id);
        return CustomApiResponse.ok(auditPlanSchedule);
    }
    /**
     * {@code DELETE  /audit-plan-activities/:id} : delete the "id" auditPlanSchedule.
     * @param id the id of the auditPlanSchedule to delete.
     */
    @DeleteMapping("/audit-plan-activities/{id}")
    @ApiOperation("Delete a single AuditPlan Activity by ID")
    public CustomApiResponse deleteAuditPlanSchedule(@PathVariable Long id) {
        log.debug("REST request to delete Audit Plan Schedule : {}", id);
        if (!auditPlanActivityService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("AuditPlan Activity with ID: " + id + " not found", ENTITY_NAME, "id does not exists");
        }
        auditPlanActivityService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
