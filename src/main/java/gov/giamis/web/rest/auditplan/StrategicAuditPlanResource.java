package gov.giamis.web.rest.auditplan;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import gov.giamis.dto.ApproveDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.auditplan.StrategicAuditPlan;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.service.auditplan.StrategicAuditPlanQueryService;
import gov.giamis.service.auditplan.StrategicAuditPlanService;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanCriteria;
import gov.giamis.service.setup.UserOrganisationUnitService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link StrategicAuditPlan}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Strategic Audit Plan Resource", description = "Operations to manage Strategic Audit Plan")
public class StrategicAuditPlanResource {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanResource.class);

    private static final String ENTITY_NAME = "strategicAuditPlan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StrategicAuditPlanService strategicAuditPlanService;

    private final StrategicAuditPlanQueryService strategicAuditPlanQueryService;

    private final UserOrganisationUnitService userOrganisationUnitService;

    public StrategicAuditPlanResource(StrategicAuditPlanService strategicAuditPlanService, StrategicAuditPlanQueryService strategicAuditPlanQueryService,UserOrganisationUnitService userOrganisationUnitService) {
        this.strategicAuditPlanService = strategicAuditPlanService;
        this.strategicAuditPlanQueryService = strategicAuditPlanQueryService;
        this.userOrganisationUnitService = userOrganisationUnitService;
    }

    /**
     * {@code POST  /strategic-audit-plans} : Create a new strategicAuditPlan.
     *
     * @param strategicAuditPlan the strategicAuditPlan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new strategicAuditPlan, or with status {@code 400 (Bad Request)} if the strategicAuditPlan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/strategic-audit-plans")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create Strategic Audit Plan")
    public CustomApiResponse createStrategicPlan(@Valid @RequestBody StrategicAuditPlan strategicAuditPlan) throws URISyntaxException {
        log.debug("REST request to save StrategicAuditPlan : {}", strategicAuditPlan);
        if (strategicAuditPlan.getId() != null) {
            throw new BadRequestAlertException("A new strategic AuditPlan already have an ID", ENTITY_NAME, "id exists");
        }

        if (strategicAuditPlanService.findByName(strategicAuditPlan.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}

        if(userOrganisationUnitService.getCurrentOrganisationUnit() == null) {
        	throw new DataIntegrityViolationException("Login User does not belong to any Organisation Unit");
        }
        StrategicAuditPlan result = strategicAuditPlanService.save(strategicAuditPlan);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /strategic-audit-plans} : Updates an existing strategicAuditPlan.
     *
     * @param strategicAuditPlan the strategicAuditPlan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated strategicAuditPlan,
     * or with status {@code 400 (Bad Request)} if the strategicAuditPlan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the strategicAuditPlan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/strategic-audit-plans")
    @ApiOperation(value = "Update existing Strategic Audit Plan")
    public CustomApiResponse updateStrategicPlan(@Valid @RequestBody StrategicAuditPlan strategicAuditPlan) throws URISyntaxException {
        log.debug("REST request to update StrategicAuditPlan : {}", strategicAuditPlan);

        if (strategicAuditPlan.getId() == null) {
			throw new BadRequestAlertException("Updated Strategic Audit Plan does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!strategicAuditPlanService.findOne(strategicAuditPlan.getId()).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan with ID: " + strategicAuditPlan.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}

		if(userOrganisationUnitService.getCurrentOrganisationUnit() == null) {
        	throw new DataIntegrityViolationException("Login User does not belong to any Organisation Unit");
        }
        StrategicAuditPlan result = strategicAuditPlanService.save(strategicAuditPlan);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /strategic-audit-plans} : get all the strategicPlans.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of strategicPlans in body.
     */
    @GetMapping("/strategic-audit-plans")
    @ApiOperation(value = "Get all  Strategic Audit Plan; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllStrategicPlans(StrategicAuditPlanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StrategicPlans by criteria: {}", criteria);
        Page<StrategicAuditPlan> page = strategicAuditPlanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /strategic-audit-plans/count} : count all the strategicPlans.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/strategic-audit-plans/count")
    @ApiOperation(value = "Count all Strategic Audit Plan; Optional filters can be applied")
    public CustomApiResponse countStrategicPlans(StrategicAuditPlanCriteria criteria) {
        log.debug("REST request to count StrategicPlans by criteria: {}", criteria);
        return CustomApiResponse.ok(strategicAuditPlanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /strategic-audit-plans/:id} : get the "id" strategicPlan.
     *
     * @param id the id of the strategicPlan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the strategicPlan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/strategic-audit-plans/{id}")
    @ApiOperation(value = "Get a single Strategic Audit Plan")
    public CustomApiResponse getStrategicPlan(@PathVariable Long id) {
        log.debug("REST request to get StrategicAuditPlan : {}", id);

        if (!strategicAuditPlanService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

        Optional<StrategicAuditPlan> strategicPlan = strategicAuditPlanService.findOne(id);
        return CustomApiResponse.ok(strategicPlan);
    }

    /**
     * {@code DELETE  /strategic-audit-plans/:id} : delete the "id" strategicPlan.
     *
     * @param id the id of the strategicPlan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/strategic-audit-plans/{id}")
    @ApiOperation(value = "Delete a single Strategic Audit Plan by ID")
    public CustomApiResponse deleteStrategicPlan(@PathVariable Long id) {
        log.debug("REST request to delete StrategicAuditPlan : {}", id);

        if (!strategicAuditPlanService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Strategic Audit Plan with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        strategicAuditPlanService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @PostMapping("/strategic-audit-plans/approve")
    public CustomApiResponse approve(@RequestBody ApproveDTO approveDTO) {
        strategicAuditPlanService.approve(approveDTO);
        return CustomApiResponse.ok("Risk based strategic audit plan approved successfully");
    }
}
