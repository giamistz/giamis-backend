package gov.giamis.web.rest.risk;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.RiskRating;
import gov.giamis.service.risk.RiskRatingService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.risk.dto.RiskRatingCriteria;
import gov.giamis.service.risk.RiskRatingQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link RiskRating}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Rating Resource", description = "Operations to manage Risk Ratings")
public class RiskRatingResource {

    private final Logger log = LoggerFactory.getLogger(RiskRatingResource.class);

    private static final String ENTITY_NAME = "riskRating";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RiskRatingService riskRatingService;

    private final RiskRatingQueryService riskRatingQueryService;

    public RiskRatingResource(RiskRatingService riskRatingService, RiskRatingQueryService riskRatingQueryService) {
        this.riskRatingService = riskRatingService;
        this.riskRatingQueryService = riskRatingQueryService;
    }

    /**
     * {@code POST  /risk-ratings} : Create a new riskRating.
     *
     * @param riskRating the riskRating to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new riskRating, or with status {@code 400 (Bad Request)} if the riskRating has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/risk-ratings")
    @ApiOperation(value = "Create Risk Rating")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createRiskRating(@Valid @RequestBody RiskRating riskRating) throws URISyntaxException {
        log.debug("REST request to save RiskRating : {}", riskRating);
        if (riskRating.getId() != null) {
			throw new BadRequestAlertException("New risk rating cant have ID", ENTITY_NAME,
					"id exists");
		}
        RiskRating result = riskRatingService.save(riskRating);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /risk-ratings} : Updates an existing riskRating.
     *
     * @param riskRating the riskRating to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated riskRating,
     * or with status {@code 400 (Bad Request)} if the riskRating is not valid,
     * or with status {@code 500 (Internal Server Error)} if the riskRating couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/risk-ratings")
    @ApiOperation(value = "Update existing  Risk Rating")
    public CustomApiResponse updateRiskRating(@Valid @RequestBody RiskRating riskRating) throws URISyntaxException {
        log.debug("REST request to update RiskRating : {}", riskRating);
        if (riskRating.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RiskRating result = riskRatingService.save(riskRating);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /risk-ratings} : get all the riskRatings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of riskRatings in body.
     */
    @GetMapping("/risk-ratings")
    @ApiOperation(value = "Get all RiskRatings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllRiskRatings(RiskRatingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RiskRatings by criteria: {}", criteria);
        Page<RiskRating> page = riskRatingQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /risk-ratings/count} : count all the riskRatings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/risk-ratings/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countRiskRatings(RiskRatingCriteria criteria) {
        log.debug("REST request to count RiskRatings by criteria: {}", criteria);
        return CustomApiResponse.ok(riskRatingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /risk-ratings/:id} : get the "id" riskRating.
     *
     * @param id the id of the riskRating to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the riskRating, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/risk-ratings/{id}")
    @ApiOperation("Get a single RiskRating by ID")
    public CustomApiResponse getRiskRating(@PathVariable Long id) {
        log.debug("REST request to get RiskRating : {}", id);
        
        if (!riskRatingService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk Rating with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<RiskRating> riskRating = riskRatingService.findOne(id);
        return CustomApiResponse.ok(riskRating);
    }

    /**
     * {@code DELETE  /risk-ratings/:id} : delete the "id" riskRating.
     *
     * @param id the id of the riskRating to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/risk-ratings/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteRiskRating(@PathVariable Long id) {
        log.debug("REST request to delete RiskRating : {}", id);
        
        if (!riskRatingService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk Rating with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        riskRatingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
