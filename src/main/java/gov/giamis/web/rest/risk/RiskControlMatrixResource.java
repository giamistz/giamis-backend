package gov.giamis.web.rest.risk;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.service.dto.RiskControlMatrixCriteria;
import gov.giamis.service.risk.RiskControlMatrixQueryService;
import gov.giamis.service.risk.RiskControlMatrixService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.risk.RiskControlMatrix}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Control Matrix Resource", description = "Operations to manage Risk Control Matrix Resource")
public class
RiskControlMatrixResource {

    private final Logger log = LoggerFactory.getLogger(RiskControlMatrixResource.class);

    private static final String ENTITY_NAME = "riskControlMatrix";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RiskControlMatrixService riskControlMatrixService;

    private final RiskControlMatrixQueryService riskControlMatrixQueryService;

    public RiskControlMatrixResource(RiskControlMatrixService riskControlMatrixService, RiskControlMatrixQueryService riskControlMatrixQueryService) {
        this.riskControlMatrixService = riskControlMatrixService;
        this.riskControlMatrixQueryService = riskControlMatrixQueryService;
    }

    /**
     * {@code POST  /risk-control-matrices} : Create a new riskControlMatrix.
     *
     * @param riskControlMatrix the riskControlMatrix to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new riskControlMatrix, or with status {@code 400 (Bad Request)} if the riskControlMatrix has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/risk-control-matrices")
    @ApiOperation(value = "Create Risk Control Matrix")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createRiskControlMatrix(@Valid @RequestBody RiskControlMatrix riskControlMatrix) throws URISyntaxException {
        log.debug("REST request to save Risk Control Matrix : {}", riskControlMatrix);
        if (riskControlMatrix.getId() != null) {
            throw new BadRequestAlertException("A new Risk Control Matrix already have an ID", ENTITY_NAME, "id exists");
        }

        RiskControlMatrix result = riskControlMatrixService.save(riskControlMatrix);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /risk-control-matrices} : Updates an existing riskControlMatrix.
     *
     * @param riskControlMatrix the riskControlMatrix to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated riskControlMatrix,
     * or with status {@code 400 (Bad Request)} if the riskControlMatrix is not valid,
     * or with status {@code 500 (Internal Server Error)} if the riskControlMatrix couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/risk-control-matrices")
    @ApiOperation(value = "Update existing Risk Control Matrix")
    public CustomApiResponse updateRiskControlMatrix(@Valid @RequestBody RiskControlMatrix riskControlMatrix) throws URISyntaxException {
        log.debug("REST request to update Risk Control Matrix : {}", riskControlMatrix);
        if (riskControlMatrix.getId() == null) {
            throw new BadRequestAlertException("Updated Risk Control Matrix does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if (!riskControlMatrixService.findOne(riskControlMatrix.getId()).isPresent()) {
            throw new BadRequestAlertException("Risk Control Matrix  with ID: " + riskControlMatrix.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }

        RiskControlMatrix result = riskControlMatrixService.save(riskControlMatrix);

        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }
    /**
     * {@code GET  /risk-control-matrices} : get all the riskControlMatrices.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of riskControlMatrices in body.
     */
    @GetMapping("/risk-control-matrices")
    @ApiOperation(value = "Get all  Risk Control Matrices; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllRiskControlMatrices(RiskControlMatrixCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Risk Control Matrices by criteria: {}", criteria);
        Page<RiskControlMatrix> page = riskControlMatrixQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    @GetMapping("/risk-control-matrices/by-engagement/{engagementId}/with-procedures")
    public CustomApiResponse getWithProcedures(@PathVariable Long engagementId) {
        log.debug("REST request to get SpecificObjectives by eng: {}", engagementId);

        List<RiskControlMatrix> controls = riskControlMatrixQueryService.findByEngagementWithProcedures(engagementId);
        return CustomApiResponse.ok(controls);
    }


    /**
    * {@code GET  /risk-control-matrices/count} : count all the riskControlMatrices.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/risk-control-matrices/count")
    @ApiOperation(value = "Count all  Risk Control Matrices; Optional filters can be applied")
    public CustomApiResponse countRiskControlMatrices(RiskControlMatrixCriteria criteria) {
        log.debug("REST request to count Risk Control Matrices by criteria: {}", criteria);
        return CustomApiResponse.ok(riskControlMatrixQueryService.countByCriteria(criteria));
    }

    @GetMapping("/risk-control-matrices-status")
    @ApiOperation(value = "Count   Risk Control Matrices")
    public CustomApiResponse countRiskControlMatrix() {
        log.debug("REST request to count Risk Control Matrices ");
        Long count = riskControlMatrixService.countRiskControlMatrix();
        boolean isRiskControlMatrixFound = false;
        if(count > 0) {
        	isRiskControlMatrixFound = true;
        }
        return CustomApiResponse.ok(isRiskControlMatrixFound);
    }
    /**
     * {@code GET  /risk-control-matrices/:id} : get the "id" riskControlMatrix.
     *
     * @param id the id of the riskControlMatrix to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the riskControlMatrix, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/risk-control-matrices/{id}")
    @ApiOperation(value = "Get a single  Risk Control Matrix by ID")
    public CustomApiResponse getRiskControlMatrix(@PathVariable Long id) {
        log.debug("REST request to get Risk Control Matrix : {}", id);

        if (!riskControlMatrixService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Risk Control Matrix with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        Optional<RiskControlMatrix> riskControlMatrix = riskControlMatrixService.findOne(id);
        return CustomApiResponse.ok(riskControlMatrix);
    }

    /**
     * {@code DELETE  /risk-control-matrices/:id} : delete the "id" riskControlMatrix.
     *
     * @param id the id of the riskControlMatrix to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/risk-control-matrices/{id}")
    @ApiOperation(value = "Delete a single Risk Control Matrix by ID")
    public CustomApiResponse deleteRiskControlMatrix(@PathVariable Long id) {
        log.debug("REST request to delete Risk Control Matrix : {}", id);

        if (!riskControlMatrixService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Risk Control Matrix with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        riskControlMatrixService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
