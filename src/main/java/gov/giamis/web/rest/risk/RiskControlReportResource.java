package gov.giamis.web.rest.risk;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import gov.giamis.dto.ApproveDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.RiskControlReport;
import gov.giamis.service.dto.RiskControlReportCriteria;
import gov.giamis.service.risk.RiskControlReportQueryService;
import gov.giamis.service.risk.RiskControlReportService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.risk.RiskControlReport}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Control Report Resource", description = "Operations to manage Risk Control Report")
public class RiskControlReportResource {

    private final Logger log = LoggerFactory.getLogger(RiskControlReportResource.class);

    private static final String ENTITY_NAME = "riskControlReport";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RiskControlReportService riskControlReportService;

    private final RiskControlReportQueryService riskControlReportQueryService;

    public RiskControlReportResource(RiskControlReportService riskControlReportService, RiskControlReportQueryService riskControlReportQueryService) {
        this.riskControlReportService = riskControlReportService;
        this.riskControlReportQueryService = riskControlReportQueryService;
    }

    /**
     * {@code POST  /risk-control-reports} : Create a new riskControlReport.
     *
     * @param riskControlReport the riskControlReport to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new riskControlReport, or with status {@code 400 (Bad Request)} if the riskControlReport has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/risk-control-reports")
    @ApiOperation(value = "Create Risk Control Report")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createRiskControlReport(@Valid @RequestBody RiskControlReport riskControlReport) throws URISyntaxException {
        log.debug("REST request to save Risk Control Report : {}", riskControlReport);
        if (riskControlReport.getId() != null) {
            throw new BadRequestAlertException("A new Risk Control Report already have an ID", ENTITY_NAME, "id exists");
        }


        RiskControlReport result = riskControlReportService.save(riskControlReport);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /risk-control-reports} : Updates an existing riskControlReport.
     *
     * @param riskControlReport the riskControlReport to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated riskControlReport,
     * or with status {@code 400 (Bad Request)} if the riskControlReport is not valid,
     * or with status {@code 500 (Internal Server Error)} if the riskControlReport couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/risk-control-reports")
    @ApiOperation(value = "Update existing RiskControlReport")
    public CustomApiResponse updateRiskControlReport(@Valid @RequestBody RiskControlReport riskControlReport) throws URISyntaxException {
        log.debug("REST request to update Risk Control Report : {}", riskControlReport);
        if (riskControlReport.getId() == null) {
            throw new BadRequestAlertException("Updated Risk Control Report does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if (!riskControlReportService.findOne(riskControlReport.getId()).isPresent()) {
            throw new BadRequestAlertException("Risk Control Report  with ID: " + riskControlReport.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }

        RiskControlReport result = riskControlReportService.save(riskControlReport);

        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /risk-control-reports} : get all the riskControlReports.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of riskControlReports in body.
     */
    @GetMapping("/risk-control-reports")
    @ApiOperation(value = "Get all  Risk Control Reports; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllRiskControlReports(RiskControlReportCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Risk Control Reports by criteria: {}", criteria);
        Page<RiskControlReport> page = riskControlReportQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /risk-control-reports/count} : count all the riskControlReports.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/risk-control-reports/count")
    @ApiOperation(value = "Count all  Risk Control Reports; Optional filters can be applied")
    public CustomApiResponse countRiskControlReports(RiskControlReportCriteria criteria) {
        log.debug("REST request to count Risk Control Reports by criteria: {}", criteria);
        return CustomApiResponse.ok(riskControlReportQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /risk-control-reports/:id} : get the "id" riskControlReport.
     *
     * @param id the id of the riskControlReport to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the riskControlReport, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/risk-control-reports/{id}")
    @ApiOperation(value = "Get a single  Risk Control Report by ID")
    public CustomApiResponse getRiskControlReport(@PathVariable Long id) {
        log.debug("REST request to get Risk Control Report : {}", id);

        if (!riskControlReportService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Risk  ControlReport with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        Optional<RiskControlReport> riskControlReport = riskControlReportService.findOne(id);
        return CustomApiResponse.ok(riskControlReport);
    }

    /**
     * {@code DELETE  /risk-control-reports/:id} : delete the "id" riskControlReport.
     *
     * @param id the id of the riskControlReport to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/risk-control-reports/{id}")
    @ApiOperation(value = "Delete a single Risk Control Report by ID")
    public CustomApiResponse deleteRiskControlReport(@PathVariable Long id) {
        log.debug("REST request to delete Risk Control Report : {}", id);

        if (!riskControlReportService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Risk Control Report with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        riskControlReportService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @PostMapping("/risk-control-reports/approve")
    public CustomApiResponse approve(@RequestBody ApproveDTO approveDTO) {
        riskControlReportService.approve(approveDTO);
        return CustomApiResponse.ok("Risk control report approved successfully");
    }
}
