package gov.giamis.web.rest.risk;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.Risk;
import gov.giamis.domain.risk.RiskRating;
import gov.giamis.service.risk.RiskQueryService;
import gov.giamis.service.risk.RiskService;
import gov.giamis.service.risk.dto.RiskCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Risk}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Resource", description = "Operations to manage Risks")
public class RiskResource {

	private final Logger log = LoggerFactory.getLogger(RiskResource.class);

	private static final String ENTITY_NAME = "risk";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final RiskService riskService;

	private final RiskQueryService riskQueryService;

	public RiskResource(RiskService riskService, RiskQueryService riskQueryService) {
		this.riskService = riskService;
		this.riskQueryService = riskQueryService;
	}

	/**
	 * {@code POST  /risks} : Create a new risk.
	 *
	 * @param risk the risk to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new risk, or with status {@code 400 (Bad Request)} if the
	 *         risk has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/risks")
	@ApiOperation(value = "Create Risk")
	@ResponseStatus(HttpStatus.CREATED)
	@Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
	public CustomApiResponse createRisk(@Valid @RequestBody Risk risk) throws URISyntaxException {
		log.debug("REST request to save Risk : {}", risk);
		if (risk.getId() != null) {
			throw new BadRequestAlertException("A new risk already have an ID", ENTITY_NAME, "id exists");
		}

		if (riskService.findByCode(risk.getCode()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Code already exists");
		}
		for(RiskRating rating: risk.getRiskRatings()){
		    rating.setRisk(risk);
        }
		Risk result = riskService.save(risk);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}

	/**
	 * {@code PUT  /risks} : Updates an existing risk.
	 *
	 * @param risk the risk to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated risk, or with status {@code 400 (Bad Request)} if the
	 *         risk is not valid, or with status {@code 500 (Internal Server Error)}
	 *         if the risk couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/risks")
	@ApiOperation(value = "Update existing Risk")
	public CustomApiResponse updateRisk(@Valid @RequestBody Risk risk) throws URISyntaxException {
		log.debug("REST request to update Risk : {}", risk);
		if (risk.getId() == null) {
			throw new BadRequestAlertException("Updated Risk does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!riskService.findOne(risk.getId()).isPresent()) {
			throw new BadRequestAlertException("Risk  with ID: " + risk.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
        for(RiskRating rating: risk.getRiskRatings()){
            rating.setRisk(risk);
        }
		Risk result = riskService.save(risk);

		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /risks} : get all the risks.
	 *
	 *
	 * @param pageable the pagination information.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of risks in body.
	 */
	@GetMapping("/risks")
	@ApiOperation(value = "Get all  Risks; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllRisks(RiskCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Risks by criteria: {}", criteria);
		Page<Risk> page = riskQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /risks/count} : count all the risks.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/risks/count")
	@ApiOperation(value = "Count all  Risks; Optional filters can be applied")
    public CustomApiResponse countRisks(RiskCriteria criteria) {
        log.debug("REST request to count Risks by criteria: {}", criteria);
        return CustomApiResponse.ok(riskQueryService.countByCriteria(criteria));
    }

	/**
	 * {@code GET  /risks/:id} : get the "id" risk.
	 *
	 * @param id the id of the risk to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the risk, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/risks/{id}")
	@ApiOperation(value = "Get a single  Risk by ID")
	public CustomApiResponse getRisk(@PathVariable Long id) {
		log.debug("REST request to get Risk : {}", id);

		if (!riskService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<Risk> risk = riskService.findOne(id);
		return CustomApiResponse.ok(risk);
	}

	/**
	 * {@code DELETE  /risks/:id} : delete the "id" risk.
	 *
	 * @param id the id of the risk to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/risks/{id}")
	@ApiOperation(value = "Delete a single Risk by ID")
	public CustomApiResponse deleteRisk(@PathVariable Long id) {
		log.debug("REST request to delete Risk : {}", id);

		if (!riskService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		riskService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}

    @PutMapping("/risks/approve-all/{id}")
	@ApiOperation(value = "Approve all existing Risks by Risk Register ID")
	public CustomApiResponse approveAllRiskByRiskRegisterId(@PathVariable(value ="id") Long id) throws URISyntaxException {
		log.debug("REST request to approve all existing Risks: {}");

		if (riskService.findRiskByRiskRegisterId(id).isEmpty()) {
			throw new BadRequestAlertException("Risk with Risk Register Id: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		riskService.updateRisksByRiskRegisterId(id);

		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
	}

    @PutMapping("/risks-approve-by-id")
	@ApiOperation(value = "Approve all existing Risks by Id and Risk Register ID")
	public CustomApiResponse approveAllRiskByIdAndRiskRegisterId(@RequestParam(value ="id") Long id,@RequestParam(value ="riskRegisterId") Long riskRegisterId) throws URISyntaxException {
		log.debug("REST request to approve all existing Risks: {}");

		if (riskService.findRiskByIdAndRiskRegisterId(id, riskRegisterId).isEmpty()) {
			throw new BadRequestAlertException("Risk  with id: " +id+ " and Risk Register Id: " + riskRegisterId + " not found", ENTITY_NAME,
					"id not exists");
		}

		riskService.updateRisksByIdAndRiskRegisterId(id, riskRegisterId);

		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
	}
}
