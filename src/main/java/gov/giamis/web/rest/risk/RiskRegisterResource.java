package gov.giamis.web.rest.risk;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.RiskRegister;
import gov.giamis.dto.ApproveDTO;
import gov.giamis.service.risk.RiskRegisterService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.risk.dto.RiskRegisterCriteria;
import gov.giamis.service.risk.RiskRegisterQueryService;

import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link RiskRegister}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Register Resource", description = "Operations to manage Risk Registers")
public class RiskRegisterResource {

    private final Logger log = LoggerFactory.getLogger(RiskRegisterResource.class);

    private static final String ENTITY_NAME = "riskRegister";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RiskRegisterService riskRegisterService;

    private final RiskRegisterQueryService riskRegisterQueryService;

    public RiskRegisterResource(RiskRegisterService riskRegisterService, RiskRegisterQueryService riskRegisterQueryService) {
        this.riskRegisterService = riskRegisterService;
        this.riskRegisterQueryService = riskRegisterQueryService;
    }

    /**
     * {@code POST  /risk-registers} : Create a new riskRegister.
     *
     * @param riskRegister the riskRegister to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new riskRegister, or with status {@code 400 (Bad Request)} if the riskRegister has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/risk-registers")
    @ApiOperation(value = "create  risk register")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createRiskRegister(@Valid @RequestBody RiskRegister riskRegister) throws URISyntaxException {
        log.debug("REST request to save RiskRegister : {}", riskRegister);
        if (riskRegister.getId() != null) {
            throw new BadRequestAlertException("A new riskRegister cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (riskRegisterService.findByName(riskRegister.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Description already exists");
        }
        RiskRegister result = riskRegisterService.save(riskRegister);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /risk-registers} : Updates an existing riskRegister.
     *
     * @param riskRegister the riskRegister to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated riskRegister,
     * or with status {@code 400 (Bad Request)} if the riskRegister is not valid,
     * or with status {@code 500 (Internal Server Error)} if the riskRegister couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/risk-registers")
    public CustomApiResponse updateRiskRegister(@Valid @RequestBody RiskRegister riskRegister) throws URISyntaxException {
        log.debug("REST request to update RiskRegister : {}", riskRegister);
        if (riskRegister.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "id not exist");
        }
        if(!riskRegisterService.findOne(riskRegister.getId()).isPresent()) {
            throw new BadRequestAlertException("Risk register with ID: "+ riskRegister.getId() + "not found", ENTITY_NAME,
                "id not exist");
        }
        RiskRegister result = riskRegisterService.save(riskRegister);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /risk-registers} : get all the riskRegisters.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of riskRegisters in body.
     */
    @GetMapping("/risk-registers")
    @ApiOperation(value = "Get all risk register; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllRiskRegisters(RiskRegisterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RiskRegisters by criteria: {}", criteria);
        Page<RiskRegister> page = riskRegisterQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /risk-registers/count} : count all the riskRegisters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/risk-registers/count")
    @ApiOperation(value = "Count all risk registers; Optional filters can be applied")
    public CustomApiResponse countRiskRegisters(RiskRegisterCriteria criteria) {
        log.debug("REST request to count RiskRegisters by criteria: {}", criteria);
        return CustomApiResponse.ok(riskRegisterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /risk-registers/:id} : get the "id" riskRegister.
     *
     * @param id the id of the riskRegister to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the riskRegister, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/risk-registers/{id}")
    @ApiOperation(value = "Get a single risk register by ID")
    public CustomApiResponse getRiskRegister(@PathVariable Long id) {
        log.debug("REST request to get RiskRegister : {}", id);

        if(!riskRegisterService.findOne(id).isPresent()){
            throw new BadRequestAlertException("Risk register with ID: "+ id + "not found", ENTITY_NAME, "id not exist");
        }
        Optional<RiskRegister> riskRegister = riskRegisterService.findOne(id);
        return CustomApiResponse.ok(riskRegister);
    }

    /**
     * {@code DELETE  /risk-registers/:id} : delete the "id" riskRegister.
     *
     * @param id the id of the riskRegister to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/risk-registers/{id}")
    @ApiOperation(value = "Delete a single risk register by ID")
    public CustomApiResponse deleteRiskRegister(@PathVariable Long id) {
        log.debug("REST request to delete RiskRegister : {}", id);

        if(!riskRegisterService.findOne(id).isPresent()){
            throw new BadRequestAlertException("Risk register with ID: "+ id + "not found", ENTITY_NAME, "id not exist");
        }
        riskRegisterService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @PostMapping("/risk-registers/approve")
    public CustomApiResponse approve(@RequestBody ApproveDTO approveDTO) {
         riskRegisterService.approve(approveDTO);
        return CustomApiResponse.ok("Risk register approved successfully");
    }
}
