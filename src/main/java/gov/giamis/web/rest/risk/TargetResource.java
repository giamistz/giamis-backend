package gov.giamis.web.rest.risk;

import gov.giamis.config.Constants;
import gov.giamis.domain.risk.Target;
import gov.giamis.service.risk.TargetService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import gov.giamis.service.risk.dto.TargetCriteria;
import gov.giamis.service.risk.TargetQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Target}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Target Resource ", description = "Operations to manage Target")
public class TargetResource {

    private final Logger log = LoggerFactory.getLogger(TargetResource.class);

    private static final String ENTITY_NAME = "target";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TargetService targetService;

    private final TargetQueryService targetQueryService;

    public TargetResource(TargetService targetService, TargetQueryService targetQueryService) {
        this.targetService = targetService;
        this.targetQueryService = targetQueryService;
    }

    /**
     * {@code POST  /targets} : Create a new target.
     *
     * @param target the target to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new target, or with status {@code 400 (Bad Request)} if the target has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/targets")
	@ApiOperation(value = "create  Target")
	@ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createTarget(@Valid @RequestBody Target target) throws URISyntaxException {
        log.debug("REST request to save Target : {}", target);
        
        if (target.getId() != null) {
            throw new BadRequestAlertException("A new target cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        if (targetService.findByDescription(target.getDescription()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Description already exists");
		}
        
        Target result = targetService.save(target);    
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /targets} : Updates an existing target.
     *
     * @param target the target to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated target,
     * or with status {@code 400 (Bad Request)} if the target is not valid,
     * or with status {@code 500 (Internal Server Error)} if the target couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/targets")
    @ApiOperation(value = "Update existing  Target")
    public CustomApiResponse updateTarget(@Valid @RequestBody Target target) throws URISyntaxException {
        log.debug("REST request to update Target : {}", target);
        
        if (target.getId() == null) {
        	throw new BadRequestAlertException("Updated Target does not have an ID", ENTITY_NAME,
					"id not exists");
        }
        
        if (!targetService.findOne(target.getId()).isPresent()) {
			throw new BadRequestAlertException("Target with ID: " + target.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
        
        Target result = targetService.save(target);
    	return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /targets} : get all the targets.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of targets in body.
     */
    @GetMapping("/targets")
    @ApiOperation(value = "Get all  Targets; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllTargets(TargetCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Targets by criteria: {}", criteria);
        Page<Target> page = targetQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /targets/count} : count all the targets.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/targets/count")
    @ApiOperation(value = "Count all  Targets; Optional filters can be applied")
    public CustomApiResponse countTargets(TargetCriteria criteria) {
        log.debug("REST request to count Targets by criteria: {}", criteria);
        
		return CustomApiResponse.ok(targetQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /targets/:id} : get the "id" target.
     *
     * @param id the id of the target to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the target, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/targets/{id}")
    @ApiOperation(value = "Get a single  Target by ID")
    public CustomApiResponse getTarget(@PathVariable Long id) {
        log.debug("REST request to get Target : {}", id);
        
        if (!targetService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Target with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        
        Optional<Target> target = targetService.findOne(id);
        return CustomApiResponse.ok(target);
    }

    /**
     * {@code DELETE  /targets/:id} : delete the "id" target.
     *
     * @param id the id of the target to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/targets/{id}")
    @ApiOperation(value = "Delete a single Target by ID")
    public CustomApiResponse deleteTarget(@PathVariable Long id) {
        log.debug("REST request to delete Target : {}", id);
        
        if (!targetService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Target with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        targetService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
