package gov.giamis.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.zalando.problem.AbstractThrowableProblem;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CustomErrorResponse extends AbstractThrowableProblem {

    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomErrorResponse(String message) {
        this.message = message;
    }
}
