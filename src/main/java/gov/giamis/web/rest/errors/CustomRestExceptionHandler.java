package gov.giamis.web.rest.errors;

import io.micrometer.core.lang.NonNullApi;
import io.undertow.server.handlers.form.MultiPartParserDefinition;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@SuppressWarnings("unused")
@NonNullApi
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(
        ServletRequestBindingException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleServletRequestBindingException(ex, headers, status, request);
        ApiError apiError = new ApiError(
            status.value(),
            ex.getMessage(),
            ex.getLocalizedMessage()
        );

        return new ResponseEntity<>(apiError, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
        NoHandlerFoundException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleNoHandlerFoundException(ex, headers, status, request);

        ApiError apiError = new ApiError(
            status.value(),
            ex.getMessage(),
            ex.getRequestURL()
        );

        return new ResponseEntity<>(apiError, headers, status);
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    protected ResponseEntity<Object> handleResourceNotFound(
        ResourceNotFoundException ex,
        WebRequest request) {

        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            ex.getMessage(),
            ex.getError()
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({BadCredentialsException.class})
    protected ResponseEntity<Object> handleBadCredential(
        Exception ex,
        WebRequest request) {

        ApiError apiError = new ApiError(
            HttpStatus.UNAUTHORIZED.value(),
            "Authentication failed",
            "Invalid username or password"
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({InsufficientAuthenticationException.class})
    protected ResponseEntity<Object> handleAuthenticationRequired(
        InsufficientAuthenticationException ex,
        WebRequest request) {

        List<String> errors = new ArrayList<>();
        ApiError apiError = new ApiError(
            HttpStatus.UNAUTHORIZED.value(),
            "Authentication required",
             "Authentication required"
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({EmailAlreadyUsedException.class})
    protected ResponseEntity<Object> handleEmailAlreadyUsedException(
        EmailAlreadyUsedException ex,
        WebRequest request) {

        List<String> errors = new ArrayList<>();
        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            "Validation Error; Email already used ",
             "Validation Error; Email already used"
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InvalidDataAccessApiUsageException.class})
    protected ResponseEntity<Object> handleInvalidDataAccessException(
        InvalidDataAccessApiUsageException ex,
        WebRequest request) {

        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            ex.getMostSpecificCause().getMessage(),
            ex.getMostSpecificCause().getLocalizedMessage()
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleMethodArgumentNotValid(ex, headers, status, request);
        List<String> errors = new ArrayList<>();
        for (FieldError field: ex.getBindingResult().getFieldErrors()) {
            errors.add(field.getField().concat(" ")
                .concat(field.getDefaultMessage()));
        }
        ApiError apiError = new ApiError(
            status.value(),
            "Validation Error",
            errors
        );
        return new ResponseEntity<>(apiError, headers,status);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected  ResponseEntity<Object> handleDataIntegrityException(
        DataIntegrityViolationException ex,
        WebRequest request) {

        String error ="Data Integrity Error; "
            +StringUtils.substringBetween(
                ex.getMostSpecificCause().getMessage()
                ,"Detail:"
                ,".");

        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            error,
            error
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
        MissingServletRequestParameterException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleMissingServletRequestParameter(ex, headers, status, request);
        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            ex.getMessage(),
            ex.getLocalizedMessage()
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
        HttpRequestMethodNotSupportedException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleHttpRequestMethodNotSupported(ex, headers, status, request);

        String uri = ((ServletWebRequest)request).getRequest().getRequestURI();

        ApiError apiError = new ApiError(
            HttpStatus.METHOD_NOT_ALLOWED.value(),
            ex.getMessage(),
            uri.concat(" ").concat(ex.getLocalizedMessage())
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
        HttpMediaTypeNotSupportedException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleHttpMediaTypeNotSupported(ex, headers, status, request);

        HttpServletRequest req = ((ServletWebRequest)request).getRequest();
        ApiError apiError = new ApiError(
            HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
            ex.getMessage(),
            req.getMethod()
                .concat(" ")
                .concat(req.getRequestURI())
                .concat(" ")
                .concat(ex.getLocalizedMessage())
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(MultiPartParserDefinition.FileTooLargeException.class)
    protected  ResponseEntity<Object> handleFileTooLargeException(
        MultiPartParserDefinition.FileTooLargeException ex,
        WebRequest request) {

        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            "File too long",
            "File exceeded maximum size allowed "
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MailAuthenticationException.class})
    protected ResponseEntity<Object> handleMailAuthenticationException(
        MailAuthenticationException ex,
        WebRequest request) {

        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            ex.getMostSpecificCause().getMessage(),
            ex.getMostSpecificCause().getMessage()
            );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        HttpMessageNotReadableException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        super.handleHttpMessageNotReadable(ex, headers, status, request);

        String message = "Invalid data type in request body at "
            +StringUtils.substringBetween(
                ex.getMostSpecificCause().getMessage()
                ,"[\""
                ,"\"]");

        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            message,
            message
        );

        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
