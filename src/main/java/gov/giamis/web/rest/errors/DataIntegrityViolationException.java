package gov.giamis.web.rest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DataIntegrityViolationException extends RuntimeException {
	private static final long serialVersionUID = 8883846351805592083L;
	private String error;

    public DataIntegrityViolationException(String error) {
        super(error, null, true, false);
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
