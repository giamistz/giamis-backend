package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.ProfessionalQualifcation;
import gov.giamis.service.ProfessionalQualifcationService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.ProfessionalQualifcationCriteria;
import gov.giamis.service.ProfessionalQualifcationQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.ProfessionalQualifcation}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class ProfessionalQualifcationResource {

    private final Logger log = LoggerFactory.getLogger(ProfessionalQualifcationResource.class);

    private static final String ENTITY_NAME = "professionalQualifcation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfessionalQualifcationService professionalQualifcationService;

    private final ProfessionalQualifcationQueryService professionalQualifcationQueryService;

    public ProfessionalQualifcationResource(ProfessionalQualifcationService professionalQualifcationService, ProfessionalQualifcationQueryService professionalQualifcationQueryService) {
        this.professionalQualifcationService = professionalQualifcationService;
        this.professionalQualifcationQueryService = professionalQualifcationQueryService;
    }

    /**
     * {@code POST  /professional-qualifcations} : Create a new professionalQualifcation.
     *
     * @param professionalQualifcation the professionalQualifcation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new professionalQualifcation, or with status {@code 400 (Bad Request)} if the professionalQualifcation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/professional-qualifcations")
    public ResponseEntity<ProfessionalQualifcation> createProfessionalQualifcation(@Valid @RequestBody ProfessionalQualifcation professionalQualifcation) throws URISyntaxException {
        log.debug("REST request to save ProfessionalQualifcation : {}", professionalQualifcation);
        if (professionalQualifcation.getId() != null) {
            throw new BadRequestAlertException("A new professionalQualifcation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProfessionalQualifcation result = professionalQualifcationService.save(professionalQualifcation);
        return ResponseEntity.created(new URI("/api/professional-qualifcations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /professional-qualifcations} : Updates an existing professionalQualifcation.
     *
     * @param professionalQualifcation the professionalQualifcation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated professionalQualifcation,
     * or with status {@code 400 (Bad Request)} if the professionalQualifcation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the professionalQualifcation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/professional-qualifcations")
    public ResponseEntity<ProfessionalQualifcation> updateProfessionalQualifcation(@Valid @RequestBody ProfessionalQualifcation professionalQualifcation) throws URISyntaxException {
        log.debug("REST request to update ProfessionalQualifcation : {}", professionalQualifcation);
        if (professionalQualifcation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProfessionalQualifcation result = professionalQualifcationService.save(professionalQualifcation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, professionalQualifcation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /professional-qualifcations} : get all the professionalQualifcations.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of professionalQualifcations in body.
     */
    @GetMapping("/professional-qualifications")
    @ApiOperation(value = "Get all FinancialYears; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllProfessionalQualifcations(ProfessionalQualifcationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ProfessionalQualifcations by criteria: {}", criteria);
        Page<ProfessionalQualifcation> page = professionalQualifcationQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /professional-qualifcations/count} : count all the professionalQualifcations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/professional-qualifcations/count")
    public ResponseEntity<Long> countProfessionalQualifcations(ProfessionalQualifcationCriteria criteria) {
        log.debug("REST request to count ProfessionalQualifcations by criteria: {}", criteria);
        return ResponseEntity.ok().body(professionalQualifcationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /professional-qualifcations/:id} : get the "id" professionalQualifcation.
     *
     * @param id the id of the professionalQualifcation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the professionalQualifcation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/professional-qualifcations/{id}")
    public ResponseEntity<ProfessionalQualifcation> getProfessionalQualifcation(@PathVariable Long id) {
        log.debug("REST request to get ProfessionalQualifcation : {}", id);
        Optional<ProfessionalQualifcation> professionalQualifcation = professionalQualifcationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(professionalQualifcation);
    }

    /**
     * {@code DELETE  /professional-qualifcations/:id} : delete the "id" professionalQualifcation.
     *
     * @param id the id of the professionalQualifcation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/professional-qualifcations/{id}")
    public ResponseEntity<Void> deleteProfessionalQualifcation(@PathVariable Long id) {
        log.debug("REST request to delete ProfessionalQualifcation : {}", id);
        professionalQualifcationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
