package gov.giamis.web.rest.response;

import java.util.List;

public class CustomPage {

    private int page;
    private int size;
    private List<?> data;

    public CustomPage(int page, int size, List<?> data) {
        this.page = page;
        this.size = size;
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
