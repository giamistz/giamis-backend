package gov.giamis.web.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import gov.giamis.web.rest.errors.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomApiResponse  {

    private static final long serialVersionUID = 1L;

    private  Object data;
    private Integer status;
    private String message;
    private int page;
    private int size;
    private Long total;
    private String[] errors;
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
    public String[] getErrors() {
		return errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public CustomApiResponse errors(String message) {
        this.message = message;
        return this;
    }

    public static CustomApiResponse ok(Object data) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setData(data);
        return response;
    }

    public static CustomApiResponse ok(String message, Object data) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setData(data);
        response.setMessage(message);
        return response;
    }

    public static CustomApiResponse created(String message, Object data) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.CREATED.value());
        response.setData(data);
        response.setMessage(message);
        return response;
    }
   
    public static CustomApiResponse ok(Page page) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setData(page.getContent());
        response.setPage(page.getNumber());
        response.setTotal(page.getTotalElements());
        response.setSize(page.getSize());
        return response;
    }

    public static CustomApiResponse ok(String message) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage(message);
        return response;
    }

    public static CustomApiResponse accessDenied(String message) {
        CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setMessage(message);
        return response;
    }

    public static CustomApiResponse ok(Optional data) {
        if (data.isPresent()) {
            CustomApiResponse response = new CustomApiResponse();
            response.setStatus(HttpStatus.OK.value());
            response.setData(data.get());
            return response;
        } else {
            throw new ResourceNotFoundException("Resource not found");
        }

    }
    
    
    public static CustomApiResponse found(String message,String[] error) {
    	CustomApiResponse response = new CustomApiResponse();
        response.setStatus(HttpStatus.FOUND.value());
        response.setMessage(message);
        response.setErrors(error);
        return response;
    }
    

}
