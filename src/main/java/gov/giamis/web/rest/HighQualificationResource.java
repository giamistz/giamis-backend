package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.HighQualification;
import gov.giamis.service.HighQualificationService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.HighQualificationCriteria;
import gov.giamis.service.HighQualificationQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.HighQualification}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class HighQualificationResource {

    private final Logger log = LoggerFactory.getLogger(HighQualificationResource.class);

    private static final String ENTITY_NAME = "highQualification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HighQualificationService highQualificationService;

    private final HighQualificationQueryService highQualificationQueryService;

    public HighQualificationResource(HighQualificationService highQualificationService, HighQualificationQueryService highQualificationQueryService) {
        this.highQualificationService = highQualificationService;
        this.highQualificationQueryService = highQualificationQueryService;
    }

    /**
     * {@code POST  /high-qualifications} : Create a new highQualification.
     *
     * @param highQualification the highQualification to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new highQualification, or with status {@code 400 (Bad Request)} if the highQualification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/high-qualifications")
    public ResponseEntity<HighQualification> createHighQualification(@Valid @RequestBody HighQualification highQualification) throws URISyntaxException {
        log.debug("REST request to save HighQualification : {}", highQualification);
        if (highQualification.getId() != null) {
            throw new BadRequestAlertException("A new highQualification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HighQualification result = highQualificationService.save(highQualification);
        return ResponseEntity.created(new URI("/api/high-qualifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /high-qualifications} : Updates an existing highQualification.
     *
     * @param highQualification the highQualification to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated highQualification,
     * or with status {@code 400 (Bad Request)} if the highQualification is not valid,
     * or with status {@code 500 (Internal Server Error)} if the highQualification couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/high-qualifications")
    public ResponseEntity<HighQualification> updateHighQualification(@Valid @RequestBody HighQualification highQualification) throws URISyntaxException {
        log.debug("REST request to update HighQualification : {}", highQualification);
        if (highQualification.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HighQualification result = highQualificationService.save(highQualification);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, highQualification.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /high-qualifications} : get all the highQualifications.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of highQualifications in body.
     */
    @GetMapping("/high-qualifications")
    public CustomApiResponse getAllHighQualifications(HighQualificationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get HighQualifications by criteria: {}", criteria);
        Page<HighQualification> page = highQualificationQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /high-qualifications/count} : count all the highQualifications.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/high-qualifications/count")
    public ResponseEntity<Long> countHighQualifications(HighQualificationCriteria criteria) {
        log.debug("REST request to count HighQualifications by criteria: {}", criteria);
        return ResponseEntity.ok().body(highQualificationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /high-qualifications/:id} : get the "id" highQualification.
     *
     * @param id the id of the highQualification to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the highQualification, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/high-qualifications/{id}")
    public ResponseEntity<HighQualification> getHighQualification(@PathVariable Long id) {
        log.debug("REST request to get HighQualification : {}", id);
        Optional<HighQualification> highQualification = highQualificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(highQualification);
    }

    /**
     * {@code DELETE  /high-qualifications/:id} : delete the "id" highQualification.
     *
     * @param id the id of the highQualification to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/high-qualifications/{id}")
    public ResponseEntity<Void> deleteHighQualification(@PathVariable Long id) {
        log.debug("REST request to delete HighQualification : {}", id);
        highQualificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
