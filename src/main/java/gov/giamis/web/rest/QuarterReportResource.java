package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.QuarterReport;
import gov.giamis.service.QuarterReportService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.QuarterReportCriteria;
import gov.giamis.service.QuarterReportQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.QuarterReport}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class QuarterReportResource {

    private final Logger log = LoggerFactory.getLogger(QuarterReportResource.class);

    private static final String ENTITY_NAME = "quarterReport";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final QuarterReportService quarterReportService;

    private final QuarterReportQueryService quarterReportQueryService;

    public QuarterReportResource(QuarterReportService quarterReportService, QuarterReportQueryService quarterReportQueryService) {
        this.quarterReportService = quarterReportService;
        this.quarterReportQueryService = quarterReportQueryService;
    }

    /**
     * {@code POST  /quarter-reports} : Create a new quarterReport.
     *
     * @param quarterReport the quarterReport to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quarterReport, or with status {@code 400 (Bad Request)} if the quarterReport has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quarter-reports")
    public CustomApiResponse createQuarterReport(@Valid @RequestBody QuarterReport quarterReport) throws URISyntaxException {
        log.debug("REST request to save QuarterReport : {}", quarterReport);
        if (quarterReport.getId() != null) {
            throw new BadRequestAlertException("A new quarterReport cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuarterReport result = quarterReportService.save(quarterReport);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /quarter-reports} : Updates an existing quarterReport.
     *
     * @param quarterReport the quarterReport to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quarterReport,
     * or with status {@code 400 (Bad Request)} if the quarterReport is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quarterReport couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quarter-reports")
    public CustomApiResponse updateQuarterReport(@Valid @RequestBody QuarterReport quarterReport) throws URISyntaxException {
        log.debug("REST request to update QuarterReport : {}", quarterReport);
        if (quarterReport.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        QuarterReport result = quarterReportService.save(quarterReport);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /quarter-reports} : get all the quarterReports.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of quarterReports in body.
     */
    @GetMapping("/quarter-reports")
    public CustomApiResponse getAllQuarterReports(QuarterReportCriteria criteria, Pageable pageable) {
        log.debug("REST request to get QuarterReports by criteria: {}", criteria);
        Page<QuarterReport> page = quarterReportQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /quarter-reports/:id} : get the "id" quarterReport.
     *
     * @param id the id of the quarterReport to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quarterReport, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quarter-reports/{id}")
    public CustomApiResponse getQuarterReport(@PathVariable Long id) {
        log.debug("REST request to get QuarterReport : {}", id);
        Optional<QuarterReport> quarterReport = quarterReportService.findOne(id);
        return CustomApiResponse.ok(quarterReport);
    }

    @GetMapping("/quarter-reports/by-quarter/{quarterId}")
    public CustomApiResponse byQuarter(@PathVariable Long quarterId) {
        log.debug("REST request to get QuarterReport by quarterId : {}", quarterId);
        QuarterReport quarterReport = quarterReportService.getByQtr(quarterId);
        return CustomApiResponse.ok(quarterReport);
    }

    /**
     * {@code DELETE  /quarter-reports/:id} : delete the "id" quarterReport.
     *
     * @param id the id of the quarterReport to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quarter-reports/{id}")
    public CustomApiResponse deleteQuarterReport(@PathVariable Long id) {
        log.debug("REST request to delete QuarterReport : {}", id);
        quarterReportService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
