package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.EngagementSampling;
import gov.giamis.service.EngagementSamplingService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.EngagementSamplingCriteria;
import gov.giamis.service.EngagementSamplingQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.EngagementSampling}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class EngagementSamplingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementSamplingResource.class);

    private static final String ENTITY_NAME = "engagementSampling";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementSamplingService engagementSamplingService;

    private final EngagementSamplingQueryService engagementSamplingQueryService;

    public EngagementSamplingResource(EngagementSamplingService engagementSamplingService, EngagementSamplingQueryService engagementSamplingQueryService) {
        this.engagementSamplingService = engagementSamplingService;
        this.engagementSamplingQueryService = engagementSamplingQueryService;
    }

    /**
     * {@code POST  /engagement-samplings} : Create a new engagementSampling.
     *
     * @param engagementSampling the engagementSampling to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementSampling, or with status {@code 400 (Bad Request)} if the engagementSampling has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-samplings")
    @ApiOperation(value = "Create engagement sampling")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementSampling(@Valid @RequestBody EngagementSampling engagementSampling) throws URISyntaxException {
        log.debug("REST request to save EngagementSampling : {}", engagementSampling);
        if (engagementSampling.getId() != null) {
            throw new BadRequestAlertException("A new engagementSampling cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EngagementSampling result = engagementSamplingService.save(engagementSampling);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-samplings} : Updates an existing engagementSampling.
     *
     * @param engagementSampling the engagementSampling to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementSampling,
     * or with status {@code 400 (Bad Request)} if the engagementSampling is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementSampling couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-samplings")
    @ApiOperation(value = "Updating existing engagement sampling")
    public CustomApiResponse updateEngagementSampling(@Valid @RequestBody EngagementSampling engagementSampling) throws URISyntaxException {
        log.debug("REST request to update EngagementSampling : {}", engagementSampling);
        if (engagementSampling.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EngagementSampling result = engagementSamplingService.save(engagementSampling);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-samplings} : get all the engagementSamplings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementSamplings in body.
     */
    @GetMapping("/engagement-samplings")
    @ApiOperation(value = "Get all engagement samplings")
    public CustomApiResponse getAllEngagementSamplings(EngagementSamplingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementSamplings by criteria: {}", criteria);
        Page<EngagementSampling> page = engagementSamplingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-samplings/count} : count all the engagementSamplings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-samplings/count")
    @ApiOperation(value = "Count all engagement sampling")
    public CustomApiResponse countEngagementSamplings(EngagementSamplingCriteria criteria) {
        log.debug("REST request to count EngagementSamplings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementSamplingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-samplings/:id} : get the "id" engagementSampling.
     *
     * @param id the id of the engagementSampling to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementSampling, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-samplings/{id}")
    @ApiOperation(value = "Get single engagement sampling")
    public CustomApiResponse getEngagementSampling(@PathVariable Long id) {
        log.debug("REST request to get EngagementSampling : {}", id);
        Optional<EngagementSampling> engagementSampling = engagementSamplingService.findOne(id);
        return CustomApiResponse.ok(engagementSampling);
    }

    /**
     * {@code DELETE  /engagement-samplings/:id} : delete the "id" engagementSampling.
     *
     * @param id the id of the engagementSampling to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-samplings/{id}")
    @ApiOperation(value = "Delete single engagement samplings")
    public CustomApiResponse deleteEngagementSampling(@PathVariable Long id) {
        log.debug("REST request to delete EngagementSampling : {}", id);
        engagementSamplingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
