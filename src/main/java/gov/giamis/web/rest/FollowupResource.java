package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.Followup;
import gov.giamis.service.FollowupService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.FollowupCriteria;
import gov.giamis.service.FollowupQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.Followup}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class FollowupResource {

    private final Logger log = LoggerFactory.getLogger(FollowupResource.class);

    private static final String ENTITY_NAME = "followup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FollowupService followupService;

    private final FollowupQueryService followupQueryService;

    public FollowupResource(FollowupService followupService, FollowupQueryService followupQueryService) {
        this.followupService = followupService;
        this.followupQueryService = followupQueryService;
    }

    /**
     * {@code POST  /followups} : Create a new followup.
     *
     * @param followup the followup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new followup, or with status {@code 400 (Bad Request)} if the followup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/followups")
    public CustomApiResponse createFollowup(@Valid @RequestBody Followup followup) throws URISyntaxException {
        log.debug("REST request to save Followup : {}", followup);
        if (followup.getId() != null) {
            throw new BadRequestAlertException("A new followup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Followup result = followupService.save(followup);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /followups} : Updates an existing followup.
     *
     * @param followup the followup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated followup,
     * or with status {@code 400 (Bad Request)} if the followup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the followup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/followups")
    public CustomApiResponse updateFollowup(@Valid @RequestBody Followup followup) throws URISyntaxException {
        log.debug("REST request to update Followup : {}", followup);
        if (followup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Followup result = followupService.save(followup);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /followups} : get all the followups.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of followups in body.
     */
    @GetMapping("/followups")
    public CustomApiResponse getAllFollowups(FollowupCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Followups by criteria: {}", criteria);
        Page<Followup> page = followupQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /followups/:id} : get the "id" followup.
     *
     * @param id the id of the followup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the followup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/followups/{id}")
    public CustomApiResponse getFollowup(@PathVariable Long id) {
        log.debug("REST request to get Followup : {}", id);
        Optional<Followup> followup = followupService.findOne(id);
        return CustomApiResponse.ok(followup);
    }

    /**
     * {@code DELETE  /followups/:id} : delete the "id" followup.
     *
     * @param id the id of the followup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/followups/{id}")
    public CustomApiResponse deleteFollowup(@PathVariable Long id) {
        log.debug("REST request to delete Followup : {}", id);
        followupService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
