package gov.giamis.web.rest.setup;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.security.NoAthorization;
import gov.giamis.service.setup.FileResourceQueryService;
import gov.giamis.service.setup.FileResourceService;
import gov.giamis.service.setup.dto.FileResourceCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.errors.ResourceNotFoundException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link FileResource}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "File Resource", description = "Operations to manage File Resource")
public class FileResourceResource {

	private final Logger log = LoggerFactory.getLogger(FileResourceResource.class);

	private static final String ENTITY_NAME = "fileResource";

	private final FileResourceService fileResourceService;

	private final FileResourceQueryService fileResourceQueryService;

	public FileResourceResource(FileResourceService fileResourceService,
			FileResourceQueryService fileResourceQueryService) {
		this.fileResourceService = fileResourceService;
		this.fileResourceQueryService = fileResourceQueryService;
	}

	/**
	 * {@code POST  /file-resources} : Create a new fileResource.
	 *
	 * @param fileResource the fileResource to create.
	 * @return the {@link CustomApiResponse} with status {@code 201 (Created)} and
	 *         with body the new fileResource, or with status
	 *         {@code 400 (Bad Request)} if the fileResource has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/file-resources")
	@ApiOperation(value = "create  File Resource")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createFileResource(@Valid @RequestBody FileResource fileResource)
			throws URISyntaxException {
		log.debug("REST request to save FileResource : {}", fileResource);

		if (fileResource.getId() != null) {
			throw new BadRequestAlertException("A new File resource already have an ID",
                ENTITY_NAME, "id exists");
		}

		if (fileResourceService.findByName(fileResource.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		FileResource result = fileResourceService.save(fileResource);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}

	/**
	 * {@code PUT  /file-resources} : Updates an existing fileResource.
	 *
	 * @param fileResource the fileResource to update.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with
	 *         body the updated fileResource, or with status
	 *         {@code 400 (Bad Request)} if the fileResource is not valid, or with
	 *         status {@code 500 (Internal Server Error)} if the fileResource
	 *         couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/file-resources")
	@ApiOperation(value = "Update existing  File Resource")
	public CustomApiResponse updateFileResource(@Valid @RequestBody FileResource fileResource)
			throws URISyntaxException {

		log.debug("REST request to update FileResource : {}", fileResource);

		if (fileResource.getId() == null) {
			throw new BadRequestAlertException("Updated File Resource does not have an ID",
                ENTITY_NAME,
                "id not exists");
		}

		if (!fileResourceService.findOne(fileResource.getId()).isPresent()) {
			throw new BadRequestAlertException("File Resource with ID: " +
                fileResource.getId() + " not found", ENTITY_NAME, "id not exists");
		}
		FileResource result = fileResourceService.save(fileResource);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /file-resources} : get all the fileResources.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the
	 *         list of fileResources in body.
	 */
	@GetMapping("/file-resources")
	@ApiOperation(value = "Get all  File Resources; with pagination, " +
        "optional filters by fields can be applied")
	public CustomApiResponse getAllFileResources(FileResourceCriteria criteria, Pageable pageable) {
		log.debug("REST request to get FileResources by criteria: {}", criteria);

		Page<FileResource> page = fileResourceQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /file-resources/count} : count all the fileResources.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the
	 *         count in body.
	 */
	@GetMapping("/file-resources/count")
	@ApiOperation(value = "Count all  File Resources; Optional filters can be applied")
	public CustomApiResponse countFileResources(FileResourceCriteria criteria) {
		log.debug("REST request to count FileResources by criteria: {}", criteria);

		return CustomApiResponse.ok(fileResourceQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /file-resources/:id} : get the "id" fileResource.
	 *
	 * @param id the id of the fileResource to retrieve.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with
	 *         body the fileResource, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/file-resources/{id}")
	@ApiOperation(value = "Get a single  File Resource by ID")
	public CustomApiResponse getFileResource(@PathVariable Long id) {
		log.debug("REST request to get FileResource : {}", id);

		if (!fileResourceService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("File Resource with ID: "
                + id + " not found", ENTITY_NAME, "id not exists");
		}

		Optional<FileResource> fileResource = fileResourceService.findOne(id);
		return CustomApiResponse.ok(fileResource);
	}

	/**
	 * {@code DELETE  /file-resources/:id} : delete the "id" fileResource.
	 *
	 * @param id the id of the fileResource to delete.
	 * @return the {@link CustomApiResponse} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/file-resources/{id}")
	@ApiOperation(value = "Delete a single File Resource by ID")
	public CustomApiResponse deleteFileResource(@PathVariable Long id) {
		log.debug("REST request to delete FileResource : {}", id);

		if (!fileResourceService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("File Resource with ID: "
                + id + " not found", ENTITY_NAME, "id not exists");
		}

		fileResourceService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}

	/**
	 * {@code POST /file-resources/upload} : Upload file
	 *
	 * @param file
	 * @param name
	 * @param fileResourceTypeName
	 * @return
	 * @throws IOException
	 */
	@PostMapping(value = "/file-resources/upload")
	@ApiOperation(value = "Upload  a single File Resource by ID")
	public CustomApiResponse uploadFile(
	    @NotNull @RequestParam("file") MultipartFile file,
        @NotNull @RequestParam("name") String name,
        @RequestParam("fileResourceTypeName") String fileResourceTypeName)
        throws IOException {

		FileResource fileResource = fileResourceService.upload(file, name, fileResourceTypeName);

		return CustomApiResponse.ok("UPLOAD SUCCESSFULLY",fileResource);
	}

	/**
	 * {@code GET //file-resources/download/:id} : Download file with file resource
	 * id
	 *
	 * @param id
	 * @param response
	 * @throws IOException
	 */
	@GetMapping("/file-resources/download/{id}")
	@ApiOperation(value = "Download  a single File Resource by ID")
    @NoAthorization
	public void downloadFile(@PathVariable("id") Long id,
                             HttpServletResponse response)
        throws IOException {

		FileResource fileResource = fileResourceService.findOne(id)
				.orElseThrow(() -> new ResourceNotFoundException("No file resource found"));
		String filePath = fileResource.getPath();
		FileInputStream inputStream = new FileInputStream(filePath);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

		response.setContentType(fileResource.getContentType());
		response.setHeader("Content-disposition", "filename="
            + FilenameUtils.getName(filePath));

		IOUtils.copy(bufferedInputStream, response.getOutputStream());

		bufferedInputStream.close();
		inputStream.close();
		response.getOutputStream().flush();
	}
}
