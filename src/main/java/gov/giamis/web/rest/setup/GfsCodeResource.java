package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.GfsCode;
import gov.giamis.service.setup.GfsCodeQueryService;
import gov.giamis.service.setup.GfsCodeService;
import gov.giamis.service.setup.dto.GfsCodeCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link GfsCode}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Gfs Code Resource", description = "Operations to manage Gfs Code")
public class GfsCodeResource {

    private final Logger log = LoggerFactory.getLogger(GfsCodeResource.class);

    private static final String ENTITY_NAME = "gfsCode";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GfsCodeService gfsCodeService;

    private final GfsCodeQueryService gfsCodeQueryService;

    public GfsCodeResource(GfsCodeService gfsCodeService,
            GfsCodeQueryService gfsCodeQueryService) {
        this.gfsCodeService = gfsCodeService;
        this.gfsCodeQueryService = gfsCodeQueryService;
    }

    /**
     * {@code POST  /gfs-codes} : Create a new gfsCode.
     *
     * @param gfsCode the gfsCode to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new gfsCode, or with status {@code 400 (Bad Request)}
     *         if the gfsCode has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gfs-codes")
    @ApiOperation(value = "Create  Gfs Code")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createGfsCode(@Valid @RequestBody GfsCode gfsCode)
            throws URISyntaxException {
        log.debug("REST request to save GfsCode : {}", gfsCode);

        if (gfsCode.getId() != null) {
            throw new BadRequestAlertException("A new Gfs Code already have an ID", ENTITY_NAME, "id exists");
        }

        if (gfsCodeService.findByCode(gfsCode.getCode()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }

        GfsCode result = gfsCodeService.save(gfsCode);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /gfs-codes} : Updates an existing gfsCode.
     *
     * @param gfsCode the gfsCode to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated gfsCode, or with status {@code 400 (Bad Request)} if
     *         the gfsCode is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the gfsCode couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gfs-codes")
    @ApiOperation(value = "Update existing Gfs Code")
    public CustomApiResponse updateGfsCode(@Valid @RequestBody GfsCode gfsCode)
            throws URISyntaxException {
        log.debug("REST request to update GfsCode : {}", gfsCode);

        if (gfsCode.getId() == null) {
            throw new BadRequestAlertException("Updated Gfs Code does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if (!gfsCodeService.findOne(gfsCode.getId()).isPresent()) {
            throw new BadRequestAlertException("Gfs Code with ID: " + gfsCode.getId() + " not found",
                    ENTITY_NAME, "id not exists");
        }
        GfsCode result = gfsCodeService.save(gfsCode);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /gfs-codes} : get all the riskCategories.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of riskCategories in body.
     */
    @GetMapping("/gfs-codes")
    @ApiOperation(value = "Get all Risk Categories; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllRiskCategories(GfsCodeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RiskCategories by criteria: {}", criteria);
        Page<GfsCode> page = gfsCodeQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /gfs-codes/count} : count all the riskCategories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/gfs-codes/count")
    @ApiOperation(value = "Count all Gfs Code; Optional filters can be applied")
    public CustomApiResponse countRiskCategories(GfsCodeCriteria criteria) {
        log.debug("REST request to count RiskCategories by criteria: {}", criteria);

        return CustomApiResponse.ok(gfsCodeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /gfs-codes/:id} : get the "id" gfsCode.
     *
     * @param id the id of the gfsCode to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the gfsCode, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gfs-codes/{id}")
    @ApiOperation(value = "Get a single Gfs Code by ID")
    public CustomApiResponse getGfsCode(@PathVariable Long id) {
        log.debug("REST request to get GfsCode : {}", id);

        if (!gfsCodeService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Gfs Code with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        Optional<GfsCode> gfsCode = gfsCodeService.findOne(id);
        return CustomApiResponse.ok(gfsCode);
    }

    /**
     * {@code DELETE  /gfs-codes/:id} : delete the "id" gfsCode.
     *
     * @param id the id of the gfsCode to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gfs-codes/{id}")
    @ApiOperation("Delete a single Gfs Code by ID")
    public CustomApiResponse deleteGfsCode(@PathVariable Long id) {
        log.debug("REST request to delete GfsCode : {}", id);

        if (!gfsCodeService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Gfs Code with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        gfsCodeService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
