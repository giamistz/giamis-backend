package gov.giamis.web.rest.setup;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.service.setup.AuthorityQueryService;
import gov.giamis.service.setup.dto.AuthorityCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * AuthorityResource controller
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuthorityResource {

    private final Logger log = LoggerFactory.getLogger(AuthorityResource.class);

    private final AuthorityQueryService authorityQueryService;

    public AuthorityResource(AuthorityQueryService authorityQueryService) {
        this.authorityQueryService = authorityQueryService;
    }

    @PostMapping("/authorities")
    public CustomApiResponse create(@RequestBody Authority authority){
        throw new BadRequestAlertException("YOU CANNOT CREATE AUTHORITY","AUTHORITY","");
    }

    @DeleteMapping("/authorities/{id}")
    public CustomApiResponse delete(@PathVariable Long id){
        throw new BadRequestAlertException("YOU CANNOT DELETE AUTHORITY","AUTHORITY","");
    }

    /**
     *  Get paginated authorities by optional criteria
     *
     * @param criteria
     * @param pagination
     * @return @link
     */
    @GetMapping("/authorities")
    @ApiOperation(value = "Get all Authorities with optional criteria",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllAuthorities(AuthorityCriteria criteria, Pageable pagination) {
        log.debug("Get request to get authorities with criteria : ", criteria);
        Page<Authority> page = authorityQueryService.findByCriteria(criteria, pagination);
        return CustomApiResponse.ok(page);
    }

}
