package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.UserProfile;
import gov.giamis.service.setup.UserProfileQueryService;
import gov.giamis.service.setup.UserProfileService;
import gov.giamis.service.setup.dto.UserProfileCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link UserProfile}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "UserProfile Resource", description = "Operations to manage userProfiles")
public class UserProfileResource {

    private final Logger log = LoggerFactory.getLogger(UserProfileResource.class);

    private static final String ENTITY_NAME = "userProfile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserProfileService userProfileService;

    private final UserProfileQueryService userProfileQueryService;

    public UserProfileResource(UserProfileService userProfileService, UserProfileQueryService userProfileQueryService) {
        this.userProfileService = userProfileService;
        this.userProfileQueryService = userProfileQueryService;
    }

    /**
     * {@code POST  /user-profiles} : Create a new userProfile.
     *
     * @param userProfile the userProfile to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new userProfile, or with status {@code 400 (Bad Request)} if
     *         the userProfile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-profiles")
    @ApiOperation(value = "create  User Profile")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createUserProfile(@Valid @RequestBody UserProfile userProfile) throws URISyntaxException {
        log.debug("REST request to save UserProfile : {}", userProfile);

        if (userProfile.getId() != null) {
            throw new BadRequestAlertException("A new UserProfile already have an ID", ENTITY_NAME, "id exists");
        }

        if (userProfileService.findByCheckNumberAllIgnoreCase(userProfile.getCheckNumber()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Email or Check Number already exists");
        }
        UserProfile result = userProfileService.save(userProfile);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /user-profiles} : Updates an existing userProfile.
     *
     * @param userProfile the userProfile to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated userProfile, or with status {@code 400 (Bad Request)} if
     *         the userProfile is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the userProfile couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-profiles")
    @ApiOperation(value = "update existing UserProfile")
    public CustomApiResponse updateUserProfile(@Valid @RequestBody UserProfile userProfile) throws URISyntaxException {
        log.debug("REST request to update UserProfile : {}", userProfile);

        if (userProfile.getId() == null) {
            throw new BadRequestAlertException("Updated UserProfile does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!userProfileService.findOne(userProfile.getId()).isPresent()) {
            throw new BadRequestAlertException("UserProfile with ID: " + userProfile.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        UserProfile result = userProfileService.save(userProfile);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /user-profiles} : get all the userProfiles.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of userProfiles in body.
     */
    @GetMapping("/user-profiles")
    @ApiOperation(value = "Get all UserProfiles; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllUserProfiles(UserProfileCriteria criteria, Pageable pageable) {
        log.debug("REST request to get UserProfiles by criteria: {}", criteria);
        Page<UserProfile> page = userProfileQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /user-profiles/count} : count all the userProfiles.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/user-profiles/count")
    @ApiOperation(value = "Count all UserProfiles; Optional filters can be applied")
    public CustomApiResponse countUserProfiles(UserProfileCriteria criteria) {
        log.debug("REST request to count UserProfiles by criteria: {}", criteria);
        return CustomApiResponse.ok(userProfileQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-profiles/:id} : get the "id" userProfile.
     *
     * @param id the id of the userProfile to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the userProfile, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-profiles/{id}")
    @ApiOperation(value = "Get a single UserProfile by ID")
    public CustomApiResponse getUserProfile(@PathVariable Long id) {
        log.debug("REST request to get UserProfile : {}", id);

        if (!userProfileService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("UserProfile with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<UserProfile> userProfile = userProfileService.findOne(id);
        return CustomApiResponse.ok(userProfile);
    }

    /**
     * {@code DELETE  /user-profiles/:id} : delete the "id" userProfile.
     *
     * @param id the id of the userProfile to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-profiles/{id}")
    @ApiOperation(value = "Delete a single UserProfile by ID")
    public CustomApiResponse deleteUserProfile(@PathVariable Long id) {
        log.debug("REST request to delete UserProfile : {}", id);

        if (!userProfileService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("UserProfile with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }

        userProfileService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
