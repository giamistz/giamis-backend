package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import gov.giamis.security.NoAthorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.service.setup.FinancialYearQueryService;
import gov.giamis.service.setup.FinancialYearService;
import gov.giamis.service.setup.dto.FinancialYearCriteria;
import gov.giamis.service.util.LocalDateComparisonUtil;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link FinancialYear}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "financialYear Resource", description = "Operations to manage Financial Years")
public class FinancialYearResource {

	private final Logger log = LoggerFactory.getLogger(FinancialYearResource.class);

	private static final String ENTITY_NAME = "financialYear";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final FinancialYearService financialYearService;

	private final FinancialYearQueryService financialYearQueryService;
	
	private LocalDateComparisonUtil isValidDate = null;
	
	private LocalDate startDate,endDate = null;

	public FinancialYearResource(FinancialYearService financialYearService,
			FinancialYearQueryService financialYearQueryService) {
		this.financialYearService = financialYearService;
		this.financialYearQueryService = financialYearQueryService;
	}

	/**
	 * {@code POST  /financial-years} : Create a new financialYear.
	 *
	 * @param financialYear the financialYear to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new financialYear, or with status {@code 400 (Bad Request)}
	 *         if the financialYear has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/financial-years")
	@ApiOperation(value = "Create FinancialYear")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createfinancialYear(@Valid @RequestBody FinancialYear financialYear)
			throws URISyntaxException {
		log.debug("REST request to save financialYear : {}", financialYear);

		if (financialYear.getId() != null) {
			throw new BadRequestAlertException("A new Financial Year already have an ID", ENTITY_NAME, "id exists");
		}

		if (financialYearService.findByName(financialYear.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
        
		//Date Validation 
		startDate = LocalDate.parse(financialYear.getStartDate().toString());
        endDate = LocalDate.parse(financialYear.getEndDate().toString());
        
         isValidDate=new LocalDateComparisonUtil();
        if (isValidDate.compareTwoDates(startDate, endDate) == false) {
        	throw new DataIntegrityViolationException("Start Date "+financialYear.getStartDate()+" should not exceed End Date "+financialYear.getEndDate());
        }
        //end Date Validation
        
        FinancialYear result = financialYearService.save(financialYear);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}

	/**
	 * {@code PUT  /financial-years} : Updates an existing financialYear.
	 *
	 * @param financialYear the financialYear to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated financialYear, or with status {@code 400 (Bad Request)}
	 *         if the financialYear is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the financialYear couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/financial-years")
	@ApiOperation(value = "Update existing  FinancialYear")
	public CustomApiResponse updateFinancialYear(@Valid @RequestBody FinancialYear financialYear)
			throws URISyntaxException {
		log.debug("REST request to update financialYear : {}", financialYear);

		if (financialYear.getId() == null) {
			throw new BadRequestAlertException("Updated Financial Year does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!financialYearService.findOne(financialYear.getId()).isPresent()) {
			throw new BadRequestAlertException("Financial Year with ID: " + financialYear.getId() + " not found",
					ENTITY_NAME, "id not exists");
		}
		
		//Date Validation 
		startDate = LocalDate.parse(financialYear.getStartDate().toString());
        endDate = LocalDate.parse(financialYear.getEndDate().toString());
        
         isValidDate=new LocalDateComparisonUtil();
        if (isValidDate.compareTwoDates(startDate, endDate) == false) {
        	throw new DataIntegrityViolationException("Start Date "+financialYear.getStartDate()+" should not exceed End Date "+financialYear.getEndDate());
        }
        //end Date Validation
		FinancialYear result = financialYearService.save(financialYear);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}
  
	@PatchMapping("/financial-years/{id}")
	@ApiOperation("Update Status of the FinancialYear by ID")
	public CustomApiResponse updateFinancialYearStatus(@PathVariable Long id) {
		log.debug("REST request to update status of the financialYear : {}", id);

		if (!financialYearService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Financial Year with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		FinancialYear finYear=financialYearService.findStatusById(id);
		financialYearService.changeStatus(finYear.getStatus(),finYear);
		
		Optional<FinancialYear> financialYear = financialYearService.findOne(id);
		return CustomApiResponse.ok(financialYear);
	}

	/**
	 * {@code GET  /financial-years} : get all the financialYears.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of financialYears in body.
	 */
	@GetMapping("/financial-years")
	@ApiOperation(value = "Get all FinancialYears; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
	public CustomApiResponse getAllFinancialYears(FinancialYearCriteria criteria, Pageable pageable) {
		log.debug("REST request to get financialYears by criteria: {}", criteria);
		Page<FinancialYear> page = financialYearQueryService.findByCriteria(criteria, pageable);
		// HttpHeaders headers =
		// PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),
		// page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /financialYears/count} : count all the financialYears.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/financial-years/count")
	@ApiOperation(value = "Count all FinancialYears; Optional filters can be applied")
	public CustomApiResponse countFinancialYears(FinancialYearCriteria criteria) {
		log.debug("REST request to count financialYears by criteria: {}", criteria);
		return CustomApiResponse.ok(financialYearQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /financialYears/:id} : get the "id" financialYear.
	 *
	 * @param id the id of the financialYear to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the financialYear, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/financial-years/{id}")
	@ApiOperation("Get a single FinancialYear by ID")
	public CustomApiResponse getFinancialYear(@PathVariable Long id) {
		log.debug("REST request to get financialYear : {}", id);

		if (!financialYearService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Financial Year with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<FinancialYear> financialYear = financialYearService.findOne(id);
		return CustomApiResponse.ok(financialYear);
	}

	/**
	 * {@code DELETE  /financialYears/:id} : delete the "id" financialYear.
	 *
	 * @param id the id of the financialYear to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/financial-years/{id}")
	@ApiOperation("Delete a single FinancialYear by ID")
	public CustomApiResponse financialYear(@PathVariable Long id) {
		log.debug("REST request to delete financialYear : {}", id);

		if (!financialYearService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Financial Year with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		financialYearService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
