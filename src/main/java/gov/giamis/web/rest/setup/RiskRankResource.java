package gov.giamis.web.rest.setup;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.RiskRank;
import gov.giamis.service.setup.RiskRankQueryService;
import gov.giamis.service.setup.RiskRankService;
import gov.giamis.service.setup.dto.RiskRankCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link RiskRank}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Rank Resource", description = "Operations to manage Risk Rank")
@Validated
public class RiskRankResource {

	private final Logger log = LoggerFactory.getLogger(RiskRankResource.class);

	private static final String ENTITY_NAME = "riskRank";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final RiskRankService riskRankService;

	private final RiskRankQueryService riskRankQueryService;

	public RiskRankResource(RiskRankService riskRankService, RiskRankQueryService riskRankQueryService) {
		this.riskRankService = riskRankService;
		this.riskRankQueryService = riskRankQueryService;
	}
	/**
	 * {@code POST  /risk-ranks} : Create a new riskRank.
	 * @param riskRank the riskRank to create.
	 *         body the new riskRank, or with status {@code 400 (Bad Request)} if
	 *         the riskRank has already an ID.
	 */
	@PostMapping("/risk-ranks")
	@ApiOperation(value = "Create Risk Rank")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createRiskRank(@Valid @RequestBody RiskRank riskRank) {
		log.debug("REST request to save Risk Rank : {}", riskRank);
		if (riskRank.getId() != null) {
			throw new BadRequestAlertException("A new risk rank already have an ID", ENTITY_NAME,
					"idexists");
		}
		
		if(riskRank.getMinValue() >= riskRank.getMaxValue()) {
			throw new DataIntegrityViolationException("Minimum Value: "+riskRank.getMinValue()+" should not be equal or exceed Maximum Value: "+riskRank.getMaxValue());	
		}
		if (riskRankService.findByName(riskRank.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		RiskRank result = riskRankService.save(riskRank);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}

	/**
	 * {@code PUT  /risk-ranks} : Updates an existing riskRank.
	 *
	 * @param riskRank the riskRank to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated riskRank, or with status {@code 400 (Bad Request)} if the
	 *         riskRank is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the riskRank couldn't be
	 *         updated.
	 */
	@PutMapping("/risk-ranks")
	@ApiOperation(value = "Update existing Risk Rank")
	public CustomApiResponse updateRiskRank(@Valid @RequestBody RiskRank riskRank) {
		log.debug("REST request to update Risk Rank : {}", riskRank);
		if (riskRank.getId() == null) {
			throw new BadRequestAlertException("Updated Risk Rank does not have an ID", ENTITY_NAME, "id not exists");
		}
		if(riskRank.getMinValue() >= riskRank.getMaxValue()) {
			throw new DataIntegrityViolationException("Minimum Value: "+riskRank.getMinValue()+" should not be equal or exceed Maximum Value: "+riskRank.getMaxValue());	
		}
		if (!riskRankService.findOne(riskRank.getId()).isPresent()) {
			throw new BadRequestAlertException("Risk Rank with ID: " + riskRank.getId() + " not found", ENTITY_NAME,
					"id does not exists");
		}
		RiskRank result = riskRankService.save(riskRank);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}
	/**
	 * {@code GET  /risk-ranks} : get all the riskRanks.
	 * @param pageable the pagination information.
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of riskRanks in body.
	 */
	@GetMapping("/risk-ranks")
	@ApiOperation(value = "Get all Risk Rank; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
	public CustomApiResponse getAllRiskRanks(RiskRankCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Risk Ranks by criteria: {}", criteria);
		Page<RiskRank> page = riskRankQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}
	/**
	 * {@code GET  /risk-ranks/count} : count all the riskRanks.
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/risk-ranks/count")
	@ApiOperation("Count all Risk Rank; Optional filters can be applied")
	public CustomApiResponse countRiskRanks(RiskRankCriteria criteria) {
		log.debug("REST request to count RiskRanks by criteria: {}", criteria);
		return CustomApiResponse.ok(riskRankQueryService.countByCriteria(criteria));
	}
	/**
	 * {@code GET  /risk-ranks/:id} : get the "id" riskRank.
	 * @param id the id of the riskRank to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the riskRank, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/risk-ranks/{id}")
	@ApiOperation("Get a single Risk Rank  by ID")
	public CustomApiResponse getRiskRank(@PathVariable Long id) {
		log.debug("REST request to get Risk Rank : {}", id);
		if (!riskRankService.findOne(id) .isPresent()) {
			throw new BadRequestAlertException("Risk Rank with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}
		Optional<RiskRank> riskRank = riskRankService.findOne(id);
		return CustomApiResponse.ok(riskRank);
	}
	/**
	 * {@code DELETE  /risk-ranks/:id} : delete the "id" riskRank.
	 * @param id the id of the riskRank to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/risk-ranks/{id}")
	@ApiOperation("Delete a single Risk Rank by ID")
	public CustomApiResponse deleteRiskRank(@PathVariable Long id) {
		log.debug("REST request to delete Risk Rank : {}", id);
		if (!riskRankService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk Rank with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}
		riskRankService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
	
	@GetMapping("/risk-ranks-color")
	@ApiOperation(value = "Get color from Risk Rank", response = CustomApiResponse.class)
	public CustomApiResponse getRiskRankByColor(@Valid @RequestParam("minValue") Integer minValue,@Valid @RequestParam("maxValue") Integer maxValue) {
		log.debug("REST request to get Risk Rank by color using minValue and maxValue : {}", minValue + maxValue);
		if (minValue == null || maxValue == null) {
			throw new BadRequestAlertException("Risk Rank Minimum value or Maximum Value: should  not be null", ENTITY_NAME, "not found");
		}
		
		List<RiskRank> riskRank = riskRankService.findRiskRankByColor(minValue, maxValue);
		return CustomApiResponse.ok(riskRank);
	}
}
