package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Control;
import gov.giamis.service.setup.ControlQueryService;
import gov.giamis.service.setup.ControlService;
import gov.giamis.service.setup.dto.ControlCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Control}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Controls Resource", description = "Operations to manage Risk Controls")
public class ControlResource {

    private final Logger log = LoggerFactory.getLogger(ControlResource.class);

    private static final String ENTITY_NAME = "control";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ControlService controlService;

    private final ControlQueryService controlQueryService;

    public ControlResource(ControlService controlService, ControlQueryService controlQueryService) {
        this.controlService = controlService;
        this.controlQueryService = controlQueryService;
    }

    /**
     * {@code POST  /controls} : Create a new control.
     *
     * @param control the control to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new control, or with status {@code 400 (Bad Request)} if the control has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/controls")
    @ApiOperation(value = "Create Risk Controls")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createControl(@Valid @RequestBody Control control) throws URISyntaxException {
        log.debug("REST request to save Control : {}", control);
        if (control.getId() != null) {
            throw new BadRequestAlertException("A new control cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Control result = controlService.save(control);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /controls} : Updates an existing control.
     *
     * @param control the control to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated control,
     * or with status {@code 400 (Bad Request)} if the control is not valid,
     * or with status {@code 500 (Internal Server Error)} if the control couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/controls")
    @ApiOperation(value = "Update existing  risk control")
    public CustomApiResponse updateControl(@Valid @RequestBody Control control) throws URISyntaxException {
        log.debug("REST request to update Control : {}", control);
        if (control.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Control result = controlService.save(control);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /controls} : get all the controls.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of controls in body.
     */
    @GetMapping("/controls")
    @ApiOperation(value = "Get all risk controls")
    public CustomApiResponse getAllControls(ControlCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Controls by criteria: {}", criteria);
        Page<Control> page = controlQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /controls/count} : count all the controls.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/controls/count")
    @ApiOperation(value = "Count all controls resources")
    public CustomApiResponse countControls(ControlCriteria criteria) {
        log.debug("REST request to count Controls by criteria: {}", criteria);
        return CustomApiResponse.ok(controlQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /controls/:id} : get the "id" control.
     *
     * @param id the id of the control to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the control, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/controls/{id}")
    @ApiOperation(value = "Get single risk control by ID")
    public CustomApiResponse getControl(@PathVariable Long id) {
        log.debug("REST request to get Control : {}", id);
        Optional<Control> control = controlService.findOne(id);
        return CustomApiResponse.ok(control);
    }

    /**
     * {@code DELETE  /controls/:id} : delete the "id" control.
     *
     * @param id the id of the control to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/controls/{id}")
    public CustomApiResponse deleteControl(@PathVariable Long id) {
        log.debug("REST request to delete Control : {}", id);
        controlService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
