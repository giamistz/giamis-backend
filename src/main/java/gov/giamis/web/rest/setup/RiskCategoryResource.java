package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.RiskCategory;
import gov.giamis.service.setup.RiskCategoryQueryService;
import gov.giamis.service.setup.RiskCategoryService;
import gov.giamis.service.setup.dto.RiskCategoryCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link RiskCategory}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Risk Category Resource", description = "Operations to manage Risk Category")
public class RiskCategoryResource {

	private final Logger log = LoggerFactory.getLogger(RiskCategoryResource.class);

	private static final String ENTITY_NAME = "riskCategory";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final RiskCategoryService riskCategoryService;

	private final RiskCategoryQueryService riskCategoryQueryService;

	public RiskCategoryResource(RiskCategoryService riskCategoryService,
			RiskCategoryQueryService riskCategoryQueryService) {
		this.riskCategoryService = riskCategoryService;
		this.riskCategoryQueryService = riskCategoryQueryService;
	}

	/**
	 * {@code POST  /risk-categories} : Create a new riskCategory.
	 *
	 * @param riskCategory the riskCategory to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new riskCategory, or with status {@code 400 (Bad Request)}
	 *         if the riskCategory has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/risk-categories")
	@ApiOperation(value = "Create  Risk Category")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createRiskCategory(@Valid @RequestBody RiskCategory riskCategory)
			throws URISyntaxException {
		log.debug("REST request to save RiskCategory : {}", riskCategory);

		if (riskCategory.getId() != null) {
			throw new BadRequestAlertException("A new Risk Category already have an ID", ENTITY_NAME, "id exists");
		}

		if (riskCategoryService.findByName(riskCategory.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}

		RiskCategory result = riskCategoryService.save(riskCategory);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}

	/**
	 * {@code PUT  /risk-categories} : Updates an existing riskCategory.
	 *
	 * @param riskCategory the riskCategory to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated riskCategory, or with status {@code 400 (Bad Request)} if
	 *         the riskCategory is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the riskCategory couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/risk-categories")
	@ApiOperation(value = "Update existing Risk Category")
	public CustomApiResponse updateRiskCategory(@Valid @RequestBody RiskCategory riskCategory)
			throws URISyntaxException {
		log.debug("REST request to update RiskCategory : {}", riskCategory);

		if (riskCategory.getId() == null) {
			throw new BadRequestAlertException("Updated Risk Category does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!riskCategoryService.findOne(riskCategory.getId()).isPresent()) {
			throw new BadRequestAlertException("Risk Category with ID: " + riskCategory.getId() + " not found",
					ENTITY_NAME, "id not exists");
		}
		RiskCategory result = riskCategoryService.save(riskCategory);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /risk-categories} : get all the riskCategories.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of riskCategories in body.
	 */
	@GetMapping("/risk-categories")
	@ApiOperation(value = "Get all Risk Categories; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllRiskCategories(RiskCategoryCriteria criteria, Pageable pageable) {
		log.debug("REST request to get RiskCategories by criteria: {}", criteria);
		Page<RiskCategory> page = riskCategoryQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /risk-categories/count} : count all the riskCategories.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/risk-categories/count")
	@ApiOperation(value = "Count all Risk Category; Optional filters can be applied")
	public CustomApiResponse countRiskCategories(RiskCategoryCriteria criteria) {
		log.debug("REST request to count RiskCategories by criteria: {}", criteria);

		return CustomApiResponse.ok(riskCategoryQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /risk-categories/:id} : get the "id" riskCategory.
	 *
	 * @param id the id of the riskCategory to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the riskCategory, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/risk-categories/{id}")
	@ApiOperation(value = "Get a single Risk Category by ID")
	public CustomApiResponse getRiskCategory(@PathVariable Long id) {
		log.debug("REST request to get RiskCategory : {}", id);

		if (!riskCategoryService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk Category with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<RiskCategory> riskCategory = riskCategoryService.findOne(id);
		return CustomApiResponse.ok(riskCategory);
	}

	/**
	 * {@code DELETE  /risk-categories/:id} : delete the "id" riskCategory.
	 *
	 * @param id the id of the riskCategory to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/risk-categories/{id}")
	@ApiOperation("Delete a single Risk Category by ID")
	public CustomApiResponse deleteRiskCategory(@PathVariable Long id) {
		log.debug("REST request to delete RiskCategory : {}", id);

		if (!riskCategoryService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Risk Category with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		riskCategoryService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
