package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Quarter;
import gov.giamis.service.setup.QuarterQueryService;
import gov.giamis.service.setup.QuarterService;
import gov.giamis.service.setup.dto.QuarterCriteria;
import gov.giamis.service.util.LocalDateComparisonUtil;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Quarter}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "quarter Resource", description = "Operations to manage quarter")
public class QuarterResource {

	private final Logger log = LoggerFactory.getLogger(QuarterResource.class);

	private static final String ENTITY_NAME = "quarter";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final QuarterService quarterService;

	private final QuarterQueryService quarterQueryService;
	
    private LocalDateComparisonUtil isValidDate = null;
	
	private LocalDate startDate,endDate = null;

	public QuarterResource(QuarterService quarterService, QuarterQueryService quarterQueryService) {
		this.quarterService = quarterService;
		this.quarterQueryService = quarterQueryService;
	}

	/**
	 * {@code POST  /quarters} : Create a new quarter.
	 *
	 * @param quarter the quarter to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new quarter, or with status {@code 400 (Bad Request)} if the
	 *         quarter has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/quarters")
	@ApiOperation(value = "Create Quarter")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createQuarter(@Valid @RequestBody Quarter quarter) throws URISyntaxException {
		log.debug("REST request to save Quarter : {}", quarter);

		if (quarter.getId() != null) {
			throw new BadRequestAlertException("A new Quarter already have an ID", ENTITY_NAME, "idexists");
		}

		if (quarterService.findByName(quarter.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
        
		//Date Validation 
		startDate = LocalDate.parse(quarter.getStartDate().toString());
        endDate = LocalDate.parse(quarter.getEndDate().toString());
        
         isValidDate=new LocalDateComparisonUtil();
        if (isValidDate.compareTwoDates(startDate, endDate) == false) {
        	throw new DataIntegrityViolationException("Start Date "+quarter.getStartDate()+" should not exceed End Date "+quarter.getEndDate());
        }
        //end Date Validation
        
		Quarter result = quarterService.save(quarter);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
	}
	
	@PatchMapping("/quarters/{id}")
	@ApiOperation("Update Status of the Quarter by ID")
	public CustomApiResponse updateQuarterStatus(@PathVariable Long id) {
		log.debug("REST request to update status of the Quarter : {}", id);

		if (!quarterService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Quarter with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		Quarter quartr=quarterService.findStatusById(id);
		quarterService.changeStatus(quartr.getStatus(),quartr);
		
		Optional<Quarter> quarter = quarterService.findOne(id);
		return CustomApiResponse.ok(quarter);
	}

	/**
	 * {@code PUT  /quarters} : Updates an existing quarter.
	 *
	 * @param quarter the quarter to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated quarter, or with status {@code 400 (Bad Request)} if the
	 *         quarter is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the quarter couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/quarters")
	@ApiOperation(value = "Update existing Quarter")
	public CustomApiResponse updateQuarter(@Valid @RequestBody Quarter quarter) throws URISyntaxException {
		log.debug("REST request to update Quarter : {}", quarter);

		if (quarter.getId() == null) {
			throw new BadRequestAlertException("Updated Quarter does not have an ID", ENTITY_NAME, "id not exists");
		}

		if (!quarterService.findOne(quarter.getId()).isPresent()) {
			throw new BadRequestAlertException("Quarter with ID: " + quarter.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		
		//Date Validation 
		startDate = LocalDate.parse(quarter.getStartDate().toString());
        endDate = LocalDate.parse(quarter.getEndDate().toString());
        
         isValidDate=new LocalDateComparisonUtil();
        if (isValidDate.compareTwoDates(startDate, endDate) == false) {
        	throw new DataIntegrityViolationException("Start Date "+quarter.getStartDate()+" should not exceed End Date "+quarter.getEndDate());
        }
        //end Date Validation
		Quarter result = quarterService.save(quarter);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /quarters} : get all the quarters.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of quarters in body.
	 */
	@GetMapping("/quarters")
	@ApiOperation(value = "Get all Quarters; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
	public CustomApiResponse getAllQuarters(QuarterCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Quarters by criteria: {}", criteria);
		Page<Quarter> page = quarterQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /quarters/count} : count all the quarters.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/quarters/count")
	@ApiOperation(value = "Count all Quarters; Optional filters can be applied")
	public CustomApiResponse countQuarters(QuarterCriteria criteria) {
		log.debug("REST request to count Quarters by criteria: {}", criteria);
		return CustomApiResponse.ok(quarterQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /quarters/:id} : get the "id" quarter.
	 *
	 * @param id the id of the quarter to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the quarter, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/quarters/{id}")
	@ApiOperation("Get a single Quarter by ID")
	public CustomApiResponse getQuarter(@PathVariable Long id) {
		log.debug("REST request to get Quarter : {}", id);

		if (!quarterService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Quarter with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}

		Optional<Quarter> quarter = quarterService.findOne(id);
		return CustomApiResponse.ok(quarter);
	}

	/**
	 * {@code DELETE  /quarters/:id} : delete the "id" quarter.
	 *
	 * @param id the id of the quarter to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/quarters/{id}")
	@ApiOperation("Delete  a single Quarter by ID")
	public CustomApiResponse deleteQuarter(@PathVariable Long id) {
		log.debug("REST request to delete Quarter : {}", id);

		if (!quarterService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Quarter with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}

		quarterService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
