package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.service.setup.MenuGroupQueryService;
import gov.giamis.service.setup.MenuGroupService;
import gov.giamis.service.setup.dto.MenuGroupCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link MenuGroup}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "MenuGroup Resource", description = "Operations to manage MenuGroup")
public class MenuGroupResource {

    private final Logger log = LoggerFactory.getLogger(MenuGroupResource.class);

    private static final String ENTITY_NAME = "menuGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MenuGroupService menuGroupService;

    private final MenuGroupQueryService menuGroupQueryService;

    public MenuGroupResource(MenuGroupService menuGroupService,
            MenuGroupQueryService menuGroupQueryService) {
        this.menuGroupService = menuGroupService;
        this.menuGroupQueryService = menuGroupQueryService;
    }

    /**
     * {@code POST  /menu-groups} : Create a new menuGroup.
     *
     * @param menuGroup the menuGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new menuGroup, or with status {@code 400 (Bad Request)}
     *         if the menuGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/menu-groups")
    @ApiOperation(value = "Create  MenuGroup")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createMenuGroup(@Valid @RequestBody MenuGroup menuGroup)
            throws URISyntaxException {
        log.debug("REST request to save MenuGroup : {}", menuGroup);

        if (menuGroup.getId() != null) {
            throw new BadRequestAlertException("A new MenuGroup already have an ID", ENTITY_NAME, "id exists");
        }

        if (menuGroupService.findByName(menuGroup.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }

        MenuGroup result = menuGroupService.save(menuGroup);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /menu-groups} : Updates an existing menuGroup.
     *
     * @param menuGroup the menuGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated menuGroup, or with status {@code 400 (Bad Request)} if
     *         the menuGroup is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the menuGroup couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/menu-groups")
    @ApiOperation(value = "Update existing MenuGroup")
    public CustomApiResponse updateMenuGroup(@Valid @RequestBody MenuGroup menuGroup)
            throws URISyntaxException {
        log.debug("REST request to update MenuGroup : {}", menuGroup);

        if (menuGroup.getId() == null) {
            throw new BadRequestAlertException("Updated MenuGroup does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if (!menuGroupService.findOne(menuGroup.getId()).isPresent()) {
            throw new BadRequestAlertException("MenuGroup with ID: " + menuGroup.getId() + " not found",
                    ENTITY_NAME, "id not exists");
        }
        MenuGroup result = menuGroupService.save(menuGroup);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /menu-groups} : get all the riskCategories.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of riskCategories in body.
     */
    @GetMapping("/menu-groups")
    @ApiOperation(value = "Get all Risk Categories; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllRiskCategories(MenuGroupCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RiskCategories by criteria: {}", criteria);
        Page<MenuGroup> page = menuGroupQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /menu-groups/count} : count all the riskCategories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/menu-groups/count")
    @ApiOperation(value = "Count all MenuGroups; Optional filters can be applied")
    public CustomApiResponse countRiskCategories(MenuGroupCriteria criteria) {
        log.debug("REST request to count RiskCategories by criteria: {}", criteria);

        return CustomApiResponse.ok(menuGroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /menu-groups/:id} : get the "id" menuGroup.
     *
     * @param id the id of the menuGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the menuGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/menu-groups/{id}")
    @ApiOperation(value = "Get a single MenuGroup by ID")
    public CustomApiResponse getMenuGroup(@PathVariable Long id) {
        log.debug("REST request to get MenuGroup : {}", id);

        if (!menuGroupService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("MenuGroup with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        Optional<MenuGroup> menuGroup = menuGroupService.findOne(id);
        return CustomApiResponse.ok(menuGroup);
    }

    /**
     * {@code DELETE  /menu-groups/:id} : delete the "id" menuGroup.
     *
     * @param id the id of the menuGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/menu-groups/{id}")
    @ApiOperation("Delete a single MenuGroup by ID")
    public CustomApiResponse deleteMenuGroup(@PathVariable Long id) {
        log.debug("REST request to delete MenuGroup : {}", id);

        if (!menuGroupService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("MenuGroup with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }

        menuGroupService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
