package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Objective;
import gov.giamis.service.setup.ObjectiveQueryService;
import gov.giamis.service.setup.ObjectiveService;
import gov.giamis.service.setup.dto.ObjectiveCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Objective}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Objective Resource", description = "Operations to manage objectives")
public class ObjectiveResource {

	private final Logger log = LoggerFactory.getLogger(ObjectiveResource.class);

	private static final String ENTITY_NAME = "objective";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final ObjectiveService objectiveService;

	private final ObjectiveQueryService objectiveQueryService;

	public ObjectiveResource(ObjectiveService objectiveService, ObjectiveQueryService objectiveQueryService) {
		this.objectiveService = objectiveService;
		this.objectiveQueryService = objectiveQueryService;
	}

	/**
	 * {@code POST  /objectives} : Create a new objective.
	 *
	 * @param objective the objective to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new objective, or with status {@code 400 (Bad Request)} if
	 *         the objective has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/objectives")
	@ApiOperation(value = "create  Objective")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createObjective(@Valid @RequestBody Objective objective) throws URISyntaxException {
		log.debug("REST request to save Objective : {}", objective);

		if (objective.getId() != null) {
			throw new BadRequestAlertException("A new Objective already have an ID", ENTITY_NAME, "id exists");
		}

		if (objectiveService.findByDescription(objective.getDescription()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Description already exists");
		}
		Objective result = objectiveService.save(objective);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /objectives} : Updates an existing objective.
	 *
	 * @param objective the objective to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated objective, or with status {@code 400 (Bad Request)} if
	 *         the objective is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the objective couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/objectives")
	@ApiOperation(value = "update existing Objective")
	public CustomApiResponse updateObjective(@Valid @RequestBody Objective objective) throws URISyntaxException {
		log.debug("REST request to update Objective : {}", objective);

		if (objective.getId() == null) {
			throw new BadRequestAlertException("Updated Objective does not have an ID", ENTITY_NAME, "id not exists");
		}

		if (!objectiveService.findOne(objective.getId()).isPresent()) {
			throw new BadRequestAlertException("Objective with ID: " + objective.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		Objective result = objectiveService.save(objective);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /objectives} : get all the objectives.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of objectives in body.
	 */
	@GetMapping("/objectives")
	@ApiOperation(value = "Get all Objectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
	public CustomApiResponse getAllObjectives(ObjectiveCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Objectives by criteria: {}", criteria);
		Page<Objective> page = objectiveQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /objectives/count} : count all the objectives.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/objectives/count")
	@ApiOperation(value = "Count all Objectives; Optional filters can be applied")
	public CustomApiResponse countObjectives(ObjectiveCriteria criteria) {
		log.debug("REST request to count Objectives by criteria: {}", criteria);
		return CustomApiResponse.ok(objectiveQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /objectives/:id} : get the "id" objective.
	 *
	 * @param id the id of the objective to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the objective, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/objectives/{id}")
	@ApiOperation(value = "Get a single Objective by ID")
	public CustomApiResponse getObjective(@PathVariable Long id) {
		log.debug("REST request to get Objective : {}", id);

		if (!objectiveService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Objective with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}
		Optional<Objective> objective = objectiveService.findOne(id);
		return CustomApiResponse.ok(objective);
	}

	/**
	 * {@code DELETE  /objectives/:id} : delete the "id" objective.
	 *
	 * @param id the id of the objective to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/objectives/{id}")
	@ApiOperation(value = "Delete a single Objective by ID")
	public CustomApiResponse deleteObjective(@PathVariable Long id) {
		log.debug("REST request to delete Objective : {}", id);

		if (!objectiveService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Objective with ID: " + id + " not found", ENTITY_NAME, "id not exists");
		}

		objectiveService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
