package gov.giamis.web.rest.setup;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.OrganisationUnitLevel;
import gov.giamis.service.setup.OrganisationUnitLevelService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.setup.dto.OrganisationUnitLevelCriteria;
import gov.giamis.service.setup.OrganisationUnitLevelQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link OrganisationUnitLevel}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Organisation unit Level Resource", description = "Operations to manage Organisation unit Level")
public class OrganisationUnitLevelResource {

	private final Logger log = LoggerFactory.getLogger(OrganisationUnitLevelResource.class);

	private static final String ENTITY_NAME = "organisationUnitLevel";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final OrganisationUnitLevelService organisationUnitLevelService;

	private final OrganisationUnitLevelQueryService organisationUnitLevelQueryService;

	public OrganisationUnitLevelResource(OrganisationUnitLevelService organisationUnitLevelService,
			OrganisationUnitLevelQueryService organisationUnitLevelQueryService) {
		this.organisationUnitLevelService = organisationUnitLevelService;
		this.organisationUnitLevelQueryService = organisationUnitLevelQueryService;
	}

	/**
	 * {@code POST  /organisation-unit-levels} : Create a new organisationUnitLevel.
	 *
	 * @param organisationUnitLevel the organisationUnitLevel to create.
	 * @return the {@link CustomApiResponse} with status {@code 201 (Created)} and
	 *         with body the new organisationUnitLevel, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitLevel has already an
	 *         ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/organisation-unit-levels")
	@ApiOperation(value = "Create Organisation Unit Group Level")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createOrganisationUnitLevel(
			@Valid @RequestBody OrganisationUnitLevel organisationUnitLevel) throws URISyntaxException {
		log.debug("REST request to save OrganisationUnitLevel : {}", organisationUnitLevel);

		if (organisationUnitLevel.getId() != null) {
			throw new BadRequestAlertException("A new Organisation Unit Level already have an ID", ENTITY_NAME,
					"id exists");
		}

		if (organisationUnitLevelService.findByName(organisationUnitLevel.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		OrganisationUnitLevel result = organisationUnitLevelService.save(organisationUnitLevel);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /organisation-unit-levels} : Updates an existing
	 * organisationUnitLevel.
	 *
	 * @param organisationUnitLevel the organisationUnitLevel to update.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with
	 *         body the updated organisationUnitLevel, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitLevel is not valid,
	 *         or with status {@code 500 (Internal Server Error)} if the
	 *         organisationUnitLevel couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/organisation-unit-levels")
	@ApiOperation(value = "Update existing Organisation Unit Group Level")
	public CustomApiResponse updateOrganisationUnitLevel(
			@Valid @RequestBody OrganisationUnitLevel organisationUnitLevel) throws URISyntaxException {
		log.debug("REST request to update OrganisationUnitLevel : {}", organisationUnitLevel);
		if (organisationUnitLevel.getId() == null) {
			throw new BadRequestAlertException("Updated Organisation Unit Level does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!organisationUnitLevelService.findOne(organisationUnitLevel.getId()).isPresent()) {
			throw new BadRequestAlertException(
					"Organisation Unit Level with ID: " + organisationUnitLevel.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		OrganisationUnitLevel result = organisationUnitLevelService.save(organisationUnitLevel);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /organisation-unit-levels} : get all the organisationUnitLevels.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the
	 *         list of organisationUnitLevels in body.
	 */
	@GetMapping("/organisation-unit-levels")
	@ApiOperation(value = "Get all Organisation Unit Group Levels; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllOrganisationUnitLevels(OrganisationUnitLevelCriteria criteria, Pageable pageable) {
		log.debug("REST request to get OrganisationUnitLevels by criteria: {}", criteria);
		Page<OrganisationUnitLevel> page = organisationUnitLevelQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /organisation-unit-levels/count} : count all the
	 * organisationUnitLevels.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the
	 *         count in body.
	 */
	@GetMapping("/organisation-unit-levels/count")
	@ApiOperation(value = "Count all Organisation Unit Group Levels; Optional filters can be applied")
	public CustomApiResponse countOrganisationUnitLevels(OrganisationUnitLevelCriteria criteria) {
		log.debug("REST request to count OrganisationUnitLevels by criteria: {}", criteria);
		return CustomApiResponse.ok(organisationUnitLevelQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /organisation-unit-levels/:id} : get the "id"
	 * organisationUnitLevel.
	 *
	 * @param id the id of the organisationUnitLevel to retrieve.
	 * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with
	 *         body the organisationUnitLevel, or with status
	 *         {@code 404 (Not Found)}.
	 */
	@GetMapping("/organisation-unit-levels/{id}")
	@ApiOperation(value = "Get a single Organisation Unit Group Level by ID")
	public CustomApiResponse getOrganisationUnitLevel(@PathVariable Long id) {
		log.debug("REST request to get OrganisationUnitLevel : {}", id);
		if (!organisationUnitLevelService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Level with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		Optional<OrganisationUnitLevel> organisationUnitLevel = organisationUnitLevelService.findOne(id);
		return CustomApiResponse.ok(organisationUnitLevel);
	}

	/**
	 * {@code DELETE  /organisation-unit-levels/:id} : delete the "id"
	 * organisationUnitLevel.
	 *
	 * @param id the id of the organisationUnitLevel to delete.
	 * @return the {@link CustomApiResponse} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/organisation-unit-levels/{id}")
	@ApiOperation(value = "Delete a single Organisation Unit Group Level by ID")
	public CustomApiResponse deleteOrganisationUnitLevel(@PathVariable Long id) {
		log.debug("REST request to delete OrganisationUnitLevel : {}", id);

		if (!organisationUnitLevelService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Level with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		organisationUnitLevelService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
