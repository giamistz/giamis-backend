package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.ProfessionalTraining;
import gov.giamis.service.setup.ProfessionalTrainingQueryService;
import gov.giamis.service.setup.ProfessionalTrainingService;
import gov.giamis.service.setup.dto.ProfessionalTrainingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link ProfessionalTraining}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Professional Training Resource", description = "Operations to manage Professional Trainings")
public class ProfessionalTrainingResource {

    private final Logger log = LoggerFactory.getLogger(ProfessionalTrainingResource.class);

    private static final String ENTITY_NAME = "professionaltraining";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfessionalTrainingService professionaltrainingService;

    private final ProfessionalTrainingQueryService professionaltrainingQueryService;

    public ProfessionalTrainingResource(ProfessionalTrainingService professionaltrainingService, ProfessionalTrainingQueryService professionaltrainingQueryService) {
        this.professionaltrainingService = professionaltrainingService;
        this.professionaltrainingQueryService = professionaltrainingQueryService;
    }

    /**
     * {@code POST  /professional-trainings} : Create a new professionaltraining.
     *
     * @param professionaltraining the professionaltraining to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new professionaltraining, or with status {@code 400 (Bad Request)} if
     *         the professionaltraining has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/professional-trainings")
    @ApiOperation(value = "create  Professional Training")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createProfessionalTraining(@Valid @RequestBody ProfessionalTraining professionaltraining) throws URISyntaxException {
        log.debug("REST request to save ProfessionalTraining : {}", professionaltraining);

        if (professionaltraining.getId() != null) {
            throw new BadRequestAlertException("A new ProfessionalTraining already have an ID", ENTITY_NAME, "id exists");
        }

        if (professionaltrainingService.findByUser(professionaltraining.getUser().getId()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; User already exists");
        }
        ProfessionalTraining result = professionaltrainingService.save(professionaltraining);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /professional-trainings} : Updates an existing professionaltraining.
     *
     * @param professionaltraining the professionaltraining to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated professionaltraining, or with status {@code 400 (Bad Request)} if
     *         the professionaltraining is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the professionaltraining couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/professional-trainings")
    @ApiOperation(value = "update existing Professional Training")
    public CustomApiResponse updateProfessionalTraining(@Valid @RequestBody ProfessionalTraining professionaltraining) throws URISyntaxException {
        log.debug("REST request to update ProfessionalTraining : {}", professionaltraining);

        if (professionaltraining.getId() == null) {
            throw new BadRequestAlertException("Updated Professional Training does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!professionaltrainingService.findOne(professionaltraining.getId()).isPresent()) {
            throw new BadRequestAlertException("Professional Training with ID: " + professionaltraining.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        ProfessionalTraining result = professionaltrainingService.save(professionaltraining);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /professional-trainings} : get all the professionaltrainings.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of professionaltrainings in body.
     */
    @GetMapping("/professional-trainings")
    @ApiOperation(value = "Get all Professional Trainings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllProfessionalTrainings(ProfessionalTrainingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Professional Trainings by criteria: {}", criteria);
        Page<ProfessionalTraining> page = professionaltrainingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /professional-trainings/count} : count all the professionaltrainings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/professional-trainings/count")
    @ApiOperation(value = "Count all Professional Trainings; Optional filters can be applied")
    public CustomApiResponse countProfessionalTrainings(ProfessionalTrainingCriteria criteria) {
        log.debug("REST request to count Professional Trainings by criteria: {}", criteria);
        return CustomApiResponse.ok(professionaltrainingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /professional-trainings/:id} : get the "id" professionaltraining.
     *
     * @param id the id of the professionaltraining to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the professionaltraining, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/professional-trainings/{id}")
    @ApiOperation(value = "Get a single Professional Training by ID")
    public CustomApiResponse getProfessionalTraining(@PathVariable Long id) {
        log.debug("REST request to get ProfessionalTraining : {}", id);

        if (!professionaltrainingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ProfessionalTraining with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<ProfessionalTraining> professionaltraining = professionaltrainingService.findOne(id);
        return CustomApiResponse.ok(professionaltraining);
    }

    /**
     * {@code DELETE  /professional-trainings/:id} : delete the "id" professionaltraining.
     *
     * @param id the id of the professionaltraining to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/professional-trainings/{id}")
    @ApiOperation(value = "Delete a single Professional Training by ID")
    public CustomApiResponse deleteProfessionalTraining(@PathVariable Long id) {
        log.debug("REST request to delete Professional Training : {}", id);

        if (!professionaltrainingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Professional Training with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }

        professionaltrainingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
