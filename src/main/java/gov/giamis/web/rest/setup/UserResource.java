package gov.giamis.web.rest.setup;

import gov.giamis.config.Constants;
import gov.giamis.domain.enumeration.AuditType;
import gov.giamis.domain.setup.Role;
import gov.giamis.domain.setup.User;
import gov.giamis.domain.setup.UserProfile;
import gov.giamis.repository.setup.UserRepository;
import gov.giamis.service.MailService;
import gov.giamis.service.setup.MenuItemService;
import gov.giamis.service.setup.RoleService;
import gov.giamis.service.setup.UserService;
import gov.giamis.service.setup.dto.UserDTO;
import gov.giamis.service.util.LocalDateComparisonUtil;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.errors.EmailAlreadyUsedException;
import gov.giamis.web.rest.errors.LoginAlreadyUsedException;

import gov.giamis.web.rest.response.CustomApiResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link User} entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "Manage User Operations", description = "Resource to manage users")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;

    private final UserRepository userRepository;

    private final MailService mailService;

    private static final String ENTITY_NAME = "User";

    private final RoleService roleService;

    private final MenuItemService menuItemService;

    private LocalDateComparisonUtil isValidDate = null;

	private LocalDate dateOfBirth,appointmentDate = null;

    public UserResource(UserService userService,
    		UserRepository userRepository,
    		MailService mailService,
    		RoleService roleService,
    		MenuItemService menuItemService) {

        this.userService = userService;
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.roleService = roleService;
        this.menuItemService = menuItemService;
    }

    /**
     * {@code POST  /users}  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param userDTO the user to create.
     * @return the {@link CustomApiResponse} with status {@code 201 (Created)} and with body the new user, or with status {@code 400 (Bad Request)} if the login or email is already in use.
     * @throws BadRequestAlertException {@code 400 (Bad Request)} if the login or email is already in use.
     */
    @PostMapping("/users")
    @ApiOperation("Create User")
    @ResponseStatus( HttpStatus.CREATED)
    public CustomApiResponse createUser(@Valid @RequestBody UserDTO userDTO)  {
        log.debug("REST request to save User : {}", userDTO);
        if (userDTO.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "id exists");
        } else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            User user = userService.createUser(userDTO);

            mailService.sendCreationEmail(user);
            return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), user);
        }
    }

    /**
     * {@code PUT /users} : Updates an existing User.
     *
     * @param userDTO the user to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated user.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already in use.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already in use.
     */
    @PutMapping("/users")
    @ApiOperation("Update existing User")
    public CustomApiResponse updateUser(@Valid @RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCaseAndIdNot(
            userDTO.getEmail(),
            userDTO.getId());

        if (existingUser.isPresent()) {
            throw new EmailAlreadyUsedException();
        }
        Optional<UserDTO> updatedUser = userService.updateUser(userDTO);

        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), updatedUser);
    }

    /**
     * {@code GET /users} : get all users.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/users")
    @ApiOperation( value = "Get all Users; with pagination, optional filters by fields can be applied")
    public CustomApiResponse getAllUsers(Pageable pageable) {
        final Page<User> page = userService.getAllManagedUsers(pageable);
        return CustomApiResponse.ok(page);
    }

    @GetMapping("/user-roles/{id}")
    @ApiOperation("Get a single user by ID for Assigning Roles")
    public CustomApiResponse getRoles(@PathVariable Long id) {
        log.debug("REST request to get single User for assigning Roles : {}", id);

        if (!userService.findById(id).isPresent()) {
            throw new BadRequestAlertException("User with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<Role> roles = roleService.findAllRoles();
        return CustomApiResponse.ok(roles);
    }

    @PostMapping("/user-roles")
    @ApiOperation("Assign Roles to a Single User by user ID")
    public CustomApiResponse saveRoleToUser(@RequestBody User users) throws URISyntaxException{
    	if (!userService.findById(users.getId()).isPresent()) {
            throw new BadRequestAlertException("User with ID: " + users.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
       User user = userService.findUserById(users.getId());
       Set<Role> newRole = new HashSet<>();
       users.getRoles().forEach((role) -> {
           newRole.add(roleService.findOne(role.getId()).get());
       });
       user.setRoles(newRole);
       userService.save(user);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.ASSIGN_SUCCESS));

    }

    @GetMapping("/users-auditors")
    @ApiOperation(value = "Get all users having Auditor Role",
        response = CustomApiResponse.class)
    public CustomApiResponse getRoles() {
        log.debug("REST request to get Auditor Roles: {}");

        Role role=roleService.findByName(Constants.ROLE_AUDITOR);

        if (role == null) {
			throw new DataIntegrityViolationException("Auditors Role not found, Consult Administrator to Create Auditor role");
		}

        List<User> users =  userService.findUsersByRole(Constants.ROLE_AUDITOR);
        return CustomApiResponse.ok(users);
    }
    /**
     * {@code GET /users/:login} : get the "login" user.
     *
     * @param id the login of the user to find.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "login" user, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{id}")
    @ApiOperation("Get a single User by ID")
    public CustomApiResponse getUser(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);
        return CustomApiResponse.ok(
            userService.getUserWithAuthoritiesById(id));
    }

    /**
     * {@code DELETE /users/:login} : delete the "login" User.
     *
     * @param id the login of the user to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/users/{id}")
    @ApiOperation("Delete a single User by ID")
    public CustomApiResponse deleteUser(@PathVariable Long id) {
        log.debug("REST request to delete User: {}", id);
        userService.deleteUser(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/users-engagement-auditee/{type}")
    @ApiOperation("Get all Users by Using Engagement Auditee Planned or Special")
    public CustomApiResponse getUsersByEngagementAuditeee(@PathVariable String type) {
        log.debug("REST request all Users by Using Engagement Auditee Planned or Special: {}", type);
        List<User> results =null;

        String auditeeTypeName =String.valueOf(AuditType.PLANNED);
        if(type.toUpperCase().equals(auditeeTypeName)){
        	results = userService.getUserByEngagementPlannedAuditee();
        }else {
        	results = userService.findAll();
        }
        return CustomApiResponse.ok(results);
    }
}
