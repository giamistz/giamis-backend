package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.service.setup.OrganisationUnitQueryService;
import gov.giamis.service.setup.OrganisationUnitService;
import gov.giamis.service.setup.UserOrganisationUnitService;
import gov.giamis.service.setup.dto.OrganisationUnitCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link OrganisationUnit}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Organization unit Resource", description = "Operations to manage Organization unit")
public class OrganisationUnitResource {

	private final Logger log = LoggerFactory.getLogger(OrganisationUnitResource.class);

	private static final String ENTITY_NAME = "organisationUnit";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final OrganisationUnitService organisationUnitService;

	private final OrganisationUnitQueryService organisationUnitQueryService;

	private final UserOrganisationUnitService userOrganisationUnitService;
	
	public OrganisationUnitResource(
	    OrganisationUnitService organisationUnitService,
        OrganisationUnitQueryService organisationUnitQueryService,
        UserOrganisationUnitService userOrganisationUnitService) {

		this.organisationUnitService = organisationUnitService;
		this.organisationUnitQueryService = organisationUnitQueryService;
		this.userOrganisationUnitService = userOrganisationUnitService;
	}

	/**
	 * {@code POST  /organisation-units} : Create a new organisationUnit.
	 *
	 * @param organisationUnit the organisationUnit to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new organisationUnit, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnit has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/organisation-units")
	@ApiOperation(value = "Create Organisation Unit")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createOrganisationUnit(
	    @Valid @RequestBody OrganisationUnit organisationUnit)
			throws URISyntaxException {

		log.debug("REST request to save OrganisationUnit : {}", organisationUnit);

		if (organisationUnit.getId() != null) {
			throw new BadRequestAlertException("A new Organisation Unit already have an ID",
                ENTITY_NAME, "idexists");
		}

		OrganisationUnit result = organisationUnitService.save(organisationUnit);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /organisation-units} : Updates an existing organisationUnit.
	 *
	 * @param organisationUnit the organisationUnit to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated organisationUnit, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnit is not valid, or
	 *         with status {@code 500 (Internal Server Error)} if the
	 *         organisationUnit couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/organisation-units")
	@ApiOperation(value = "Update existing Organisation Unit")
	public CustomApiResponse updateOrganisationUnit(
	    @Valid @RequestBody OrganisationUnit organisationUnit)
			throws URISyntaxException {
		log.debug("REST request to update OrganisationUnit : {}", organisationUnit);

		if (organisationUnit.getId() == null) {
			throw new BadRequestAlertException("Updated Organisation Unit does not have an ID",
                ENTITY_NAME,
					"id not exists");
		}
		if (!organisationUnitService.findOne(organisationUnit.getId()).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit with ID: "
                + organisationUnit.getId() + " not found",
					ENTITY_NAME, "id not exists");
		}
		OrganisationUnit result = organisationUnitService.save(organisationUnit);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /organisation-units} : get all the organisationUnits.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of organisationUnits in body.
	 */
	@GetMapping("/organisation-units")
	@ApiOperation(value = "Get all Organisation Units; with pagination, " +
        "optional filters by fields can be applied")
	public CustomApiResponse getAllOrganisationUnits(
	    OrganisationUnitCriteria criteria,
        Pageable pageable) {

		log.debug("REST request to get OrganisationUnits by criteria: {}", criteria);
		Page<OrganisationUnit> page = organisationUnitQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /organisation-units/count} : count all the organisationUnits.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/organisation-units/count")
	@ApiOperation(value = "Count all Organisation Units; Optional filters can be applied")
	public CustomApiResponse countOrganisationUnits(OrganisationUnitCriteria criteria) {
		log.debug("REST request to count OrganisationUnits by criteria: {}", criteria);
		return CustomApiResponse.ok(organisationUnitQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /organisation-units/:id} : get the "id" organisationUnit.
	 *
	 * @param id the id of the organisationUnit to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the organisationUnit, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/organisation-units/{id}")
	@ApiOperation(value = "Get a single  Organisation Unit by ID")
	public CustomApiResponse getOrganisationUnit(@PathVariable Long id) {
		log.debug("REST request to get OrganisationUnit : {}", id);

		if (!organisationUnitService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		Optional<OrganisationUnit> organisationUnit = organisationUnitService.findOne(id);
		return CustomApiResponse.ok(organisationUnit);
	}

	/**
	 * {@code DELETE  /organisation-units/:id} : delete the "id" organisationUnit.
	 *
	 * @param id the id of the organisationUnit to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/organisation-units/{id}")
	@ApiOperation(value = "Delete a single Organisation Unit by ID")
	public CustomApiResponse deleteOrganisationUnit(@PathVariable Long id) {
		log.debug("REST request to delete OrganisationUnit : {}", id);

		if (!organisationUnitService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		organisationUnitService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}

    /**
     * Get user organisation unit with next children or get children org unit by parent
     * @param parentId
     * @return the {@link CustomApiResponse}
     */
	@GetMapping("/user-organisation-units")
    public CustomApiResponse getUserOrgUnit(@RequestParam(value = "parentId", required = false) Long parentId) {
	    if (parentId != null) {
	        return CustomApiResponse.ok(userOrganisationUnitService.getOrgUnitChildren(parentId));
        }
	    return CustomApiResponse.ok(userOrganisationUnitService.getCurrentUserOrgUnit());
    }

    /**
     * Get user organisation unit with next children or get children org unit by parent
     * @return the {@link CustomApiResponse}
     */
	@GetMapping("/current-user-organisation-units")
    public CustomApiResponse getCurrentUserOrgUnit() {

	    return CustomApiResponse.ok(userOrganisationUnitService.getCurrentUserOrgUnit());
    }

	@PutMapping("/set-organisation-units-user/{id}")
    @ApiOperation(value = "Update User ID  within existing Organisation Unit by Organisation Unit ID")
    public CustomApiResponse updateOrganisationUnitByUser(
        @RequestBody OrganisationUnit organisationUnit,@PathVariable Long id)
            throws URISyntaxException {
        log.debug("REST request to Update User ID  within existing Organisation Unit by Organisation Unit ID : {}", id);

        if (!organisationUnitService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Organisation Unit with ID: "
                + organisationUnit.getId() + " not found",
                    ENTITY_NAME, "id not exists");
        }
        organisationUnitService.updateUserByOrganisationUnit(organisationUnit.getUser().getId(), id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }
}
