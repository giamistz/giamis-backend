package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.ControlType;
import gov.giamis.service.setup.ControlTypeQueryService;
import gov.giamis.service.setup.ControlTypeService;
import gov.giamis.service.setup.dto.ControlTypeCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.setup.ControlType}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Control Type Resource", description = "Operations to manage Control Type")
public class ControlTypeResource {

    private final Logger log = LoggerFactory.getLogger(ControlTypeResource.class);

    private static final String ENTITY_NAME = "controlType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ControlTypeService controlTypeService;

    private final ControlTypeQueryService controlTypeQueryService;

    public ControlTypeResource(ControlTypeService controlTypeService, ControlTypeQueryService controlTypeQueryService) {
        this.controlTypeService = controlTypeService;
        this.controlTypeQueryService = controlTypeQueryService;
    }

    /**
     * {@code POST  /control-types} : Create a new controlType.
     *
     * @param controlType the controlType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new controlType, or with status {@code 400 (Bad Request)} if the controlType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/control-types")
    @ApiOperation(value = "Create Control Type")   
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createControlType(@Valid @RequestBody ControlType controlType) {
        log.debug("REST request to save Control Type : {}", controlType);
        if (controlType.getId() != null) {
            throw new BadRequestAlertException("A new Control Type already have an ID", ENTITY_NAME,
                    "id exists");
        }
        if (controlTypeService.findByName(controlType.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }
        ControlType result = controlTypeService.save(controlType);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }
    /**
     * {@code PUT  /control-types} : Updates an existing controlType.
     *
     * @param controlType the controlType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated controlType,
     * or with status {@code 400 (Bad Request)} if the controlType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the controlType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/control-types")
    @ApiOperation(value = "Update existing Control Type")
    public CustomApiResponse updateControlType(@Valid @RequestBody ControlType controlType) {
        log.debug("REST request to update Control Type : {}", controlType);
        if (controlType.getId() == null) {
            throw new BadRequestAlertException("Updated Control Type does not have an ID", ENTITY_NAME, "id not exists");
        }
        if (!controlTypeService.findOne(controlType.getId()).isPresent()) {
            throw new BadRequestAlertException("Control Type with ID: " + controlType.getId() + " not found", ENTITY_NAME,
                    "id does not exists");
        }
        ControlType result = controlTypeService.save(controlType);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /control-types} : get all the controlTypes.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of controlTypes in body.
     */
    @GetMapping("/control-types")
    @ApiOperation(value = "Get all Control Type; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllControlTypes(ControlTypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Control Types by criteria: {}", criteria);
        Page<ControlType> page = controlTypeQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /control-types/count} : count all the controlTypes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/control-types/count")
    @ApiOperation("Count all Control type; Optional filters can be applied")
    public CustomApiResponse countControlTypes(ControlTypeCriteria criteria) {
        log.debug("REST request to count ControlTypes by criteria: {}", criteria);
        return CustomApiResponse.ok(controlTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /control-types/:id} : get the "id" controlType.
     *
     * @param id the id of the controlType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the controlType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/control-types/{id}")
    @ApiOperation("Get a single Control Type  by ID")
    public CustomApiResponse getControlType(@PathVariable Long id) {
        log.debug("REST request to get Control Type : {}", id);
        if (!controlTypeService.findOne(id) .isPresent()) {
            throw new BadRequestAlertException("Control Type with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<ControlType> controlType = controlTypeService.findOne(id);
        return CustomApiResponse.ok(controlType);
    }

    /**
     * {@code DELETE  /control-types/:id} : delete the "id" controlType.
     *
     * @param id the id of the controlType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/control-types/{id}")
    @ApiOperation("Delete a single Control Type by ID")
    public CustomApiResponse deleteControlType(@PathVariable Long id) {
        log.debug("REST request to delete Control Type : {}", id);
        if (!controlTypeService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Control Type with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        controlTypeService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
