package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.OrganisationUnitGroup;
import gov.giamis.service.setup.OrganisationUnitGroupQueryService;
import gov.giamis.service.setup.OrganisationUnitGroupService;
import gov.giamis.service.setup.dto.OrganisationUnitGroupCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link OrganisationUnitGroup}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Organization unit Group Resource", description = "Operations to manage Organization unit Group")
public class OrganisationUnitGroupResource {

	private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupResource.class);

	private static final String ENTITY_NAME = "organisationUnitGroups";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final OrganisationUnitGroupService organisationUnitGroupService;

	private final OrganisationUnitGroupQueryService organisationUnitGroupQueryService;

	public OrganisationUnitGroupResource(OrganisationUnitGroupService organisationUnitGroupService,
			OrganisationUnitGroupQueryService organisationUnitGroupQueryService) {
		this.organisationUnitGroupService = organisationUnitGroupService;
		this.organisationUnitGroupQueryService = organisationUnitGroupQueryService;
	}

	/**
	 * {@code POST  /organisation-unit-groups} : Create a new organisationUnitGroup.
	 *
	 * @param organisationUnitGroup the organisationUnitGroup to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new organisationUnitGroup, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitGroup has already an
	 *         ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/organisation-unit-groups")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create Organisation Unit Group")
	public CustomApiResponse createOrganisationUnitGroup(
			@Valid @RequestBody OrganisationUnitGroup organisationUnitGroup) throws URISyntaxException {
		log.debug("REST request to save OrganisationUnitGroup : {}", organisationUnitGroup);

		if (organisationUnitGroup.getId() != null) {
			throw new BadRequestAlertException("A new Organisation Unit Group already have an ID", ENTITY_NAME,
					"id exists");
		}

		if (organisationUnitGroupService.findByName(organisationUnitGroup.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		OrganisationUnitGroup result = organisationUnitGroupService.save(organisationUnitGroup);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /organisation-unit-groups} : Updates an existing
	 * organisationUnitGroup.
	 *
	 * @param organisationUnitGroup the organisationUnitGroup to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated organisationUnitGroup, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitGroup is not valid,
	 *         or with status {@code 500 (Internal Server Error)} if the
	 *         organisationUnitGroup couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/organisation-unit-groups")
	@ApiOperation(value = "Update existing Organisation Unit Group")
	public CustomApiResponse updateOrganisationUnitGroup(
			@Valid @RequestBody OrganisationUnitGroup organisationUnitGroup) throws URISyntaxException {
		log.debug("REST request to update OrganisationUnitGroup : {}", organisationUnitGroup);

		if (organisationUnitGroup.getId() == null) {
			throw new BadRequestAlertException("Updated Organisation Unit Group does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!organisationUnitGroupService.findOne(organisationUnitGroup.getId()).isPresent()) {
			throw new BadRequestAlertException(
					"Organisation Unit Group with ID: " + organisationUnitGroup.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		OrganisationUnitGroup result = organisationUnitGroupService.save(organisationUnitGroup);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /organisation-unit-groups} : get all the organisationUnitGroups.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of organisationUnitGroups in body.
	 */
	@GetMapping("/organisation-unit-groups")
	@ApiOperation(value = "Get all Organisation Unit Groups; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllOrganisationUnitGroups(OrganisationUnitGroupCriteria criteria, Pageable pageable) {
		log.debug("REST request to get OrganisationUnitGroups by criteria: {}", criteria);
		Page<OrganisationUnitGroup> page = organisationUnitGroupQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /organisation-unit-groups/count} : count all the
	 * organisationUnitGroups.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/organisation-unit-groups/count")
	@ApiOperation(value = "count all Organisation Unit Groups; Optional filters can be applied")
	public CustomApiResponse countOrganisationUnitGroups(OrganisationUnitGroupCriteria criteria) {
		log.debug("REST request to count OrganisationUnitGroups by criteria: {}", criteria);
		return CustomApiResponse.ok(organisationUnitGroupService);
	}

	/**
	 * {@code GET  /organisation-unit-groups/:id} : get the "id"
	 * organisationUnitGroup.
	 *
	 * @param id the id of the organisationUnitGroup to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the organisationUnitGroup, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/organisation-unit-groups/{id}")
	@ApiOperation(value = "Get a single Organisation Unit Group by ID")
	public CustomApiResponse getOrganisationUnitGroup(@PathVariable Long id) {
		log.debug("REST request to get OrganisationUnitGroup : {}", id);

		if (!organisationUnitGroupService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Group with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<OrganisationUnitGroup> organisationUnitGroup = organisationUnitGroupService.findOne(id);
		return CustomApiResponse.ok(organisationUnitGroup);
	}

	/**
	 * {@code DELETE  /organisation-unit-groups/:id} : delete the "id"
	 * organisationUnitGroup.
	 *
	 * @param id the id of the organisationUnitGroup to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/organisation-unit-groups/{id}")
	@ApiOperation(value = "Delete a single Organisation Unit Group by ID")
	public CustomApiResponse deleteOrganisationUnitGroup(@PathVariable Long id) {
		log.debug("REST request to delete OrganisationUnitGroup : {}", id);

		if (!organisationUnitGroupService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Group with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		organisationUnitGroupService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
