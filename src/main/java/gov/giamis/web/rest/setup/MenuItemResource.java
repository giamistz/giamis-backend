package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.*;

import javax.validation.Valid;

import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.service.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuItem;
import gov.giamis.service.setup.AuthorityService;
import gov.giamis.service.setup.MenuItemQueryService;
import gov.giamis.service.setup.MenuItemService;
import gov.giamis.service.setup.dto.MenuItemCriteria;
import gov.giamis.service.setup.dto.RoleDTO;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link MenuItem}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "Menu Item management Resource", description = "Operations to manage menu Items")
public class MenuItemResource {

    private final Logger log = LoggerFactory.getLogger(MenuItemResource.class);

    private static final String ENTITY_NAME = "menuItem";

    private final MenuItemService menuItemService;


    private final MenuItemQueryService menuItemQueryService;

    private final AuthorityService authorityService;

    public MenuItemResource(MenuItemService menuItemService,
            MenuItemQueryService menuItemQueryService,
            AuthorityService authorityService) {
        this.menuItemService = menuItemService;
        this.menuItemQueryService = menuItemQueryService;
        this.authorityService = authorityService;
    }

    /**
     * {@code POST  /menuItems} : Create a new menuItem.
     *
     * @param menuItem the menuItem to create.
     * @return the {@link CustomApiResponse} with status {@code 201 (Created)} and with body the new menuItem, or with status {@code 400 (Bad Request)} if the menuItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/menu-items")
    @ApiOperation(value = "Create Menu Item")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createMenuItem(@Valid @RequestBody MenuItem menuItem) throws URISyntaxException {
        log.debug("REST request to save MenuItem : {}", menuItem);
        if (menuItem.getId() != null) {
            throw new BadRequestAlertException("A new menuItem already have an ID", ENTITY_NAME, "id exists");
        }

        if (menuItemService.findByName(menuItem.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }
        MenuItem result = menuItemService.save(menuItem);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /menuItems} : Updates an existing menuItem.
     *
     * @param menuItem the menuItem to update.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with body the updated menuItem,
     * or with status {@code 400 (Bad Request)} if the menuItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the menuItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/menu-items")
    @ApiOperation( value = "Update existing MenuItem")
    public CustomApiResponse updateMenuItem(@Valid @RequestBody MenuItem menuItem) throws URISyntaxException {
        log.debug("REST request to update MenuItem : {}", menuItem);

        if (menuItem.getId() == null) {
            throw new BadRequestAlertException("Updated MenuItem does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if (!menuItemService.findOne(menuItem.getId()).isPresent()) {
            throw new BadRequestAlertException("MenuItem with ID: " + menuItem.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        MenuItem result = menuItemService.save(menuItem);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /menuItems} : get all the menuItems.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the list of menuItems in body.
     */
    @GetMapping("/menu-items")
    @ApiOperation(value = "Get all MenuItems with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllMenuItems(MenuItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MenuItems by criteria: {}", criteria);
        Page<MenuItem> page = menuItemQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /menuItems/count} : count all the menuItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/menu-items/count")
    @ApiOperation("Count all menuItems ; Optional filters can be applied")
    public CustomApiResponse countMenuItems(MenuItemCriteria criteria) {
        log.debug("REST request to count MenuItems by criteria: {}", criteria);
        return CustomApiResponse.ok(menuItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /menuItems/:id} : get the "id" menuItem.
     *
     * @param id the id of the menuItem to retrieve.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with body the menuItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/menu-items/{id}")
    @ApiOperation("Get a single menuItem by ID")
    public CustomApiResponse getMenuItem(@PathVariable Long id) {
        log.debug("REST request to get MenuItem for Assigning Authorities : {}", id);

        if (!menuItemService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("MenuItem with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<MenuItem> menuItem = menuItemService.findOne(id);
        return CustomApiResponse.ok(menuItem);
    }

    @GetMapping("/menu-items-authorities")
    @ApiOperation("Get a single menuItem by ID for Assigning Authorities")
    public CustomApiResponse getMenuItemAuthorities() {
        log.debug("REST request to get MenuItem for adding Authorities");
        List<Authority> authority = authorityService.findDistinctAuthority();
        List<RoleDTO> roleDTOList = new ArrayList<RoleDTO>();
        RoleDTO roleDTO = null;

        for(Authority auth:authority) {
        	roleDTO = new RoleDTO();
        	roleDTO.setName(auth.getResource());
        	roleDTO.setAuthorities(authorityService.getAllAuthorities(auth.getResource()));
        	roleDTOList.add(roleDTO);
        }
        return CustomApiResponse.ok(roleDTOList);
    }

    @PostMapping("/menu-items-authorities")
    @ApiOperation("Assign Authorities for Single MenuItem by menuItem ID")
    public CustomApiResponse saveMenuItemAuthority(@RequestBody MenuItem menuItem) throws URISyntaxException{

        if (!menuItemService.findOne(menuItem.getId()).isPresent()) {
            throw new BadRequestAlertException("MenuItem with ID: " + menuItem.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
       MenuItem assignMenuItem = menuItemService.findMenuItemById(menuItem.getId());
       Set<Authority> newAuthority = new HashSet<>();
       menuItem.getAuthorities().forEach((authority) -> {
           newAuthority.add(authorityService.findOne(authority.getId()).get());
       });
       assignMenuItem.setAuthorities(newAuthority);
       menuItemService.save(assignMenuItem);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.ASSIGN_SUCCESS));

    }

    /**
     * {@code DELETE  /menuItems/:id} : delete the "id" menuItem.
     *
     * @param id the id of the menuItem to delete.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)}.
     */
    @DeleteMapping("/menu-items/{id}")
    @ApiOperation("Delete a single menuItem by ID")
    public CustomApiResponse deleteMenuItem(@PathVariable Long id) {
        log.debug("REST request to delete MenuItem : {}", id);

        if (!menuItemService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("MenuItem with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        menuItemService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

}
