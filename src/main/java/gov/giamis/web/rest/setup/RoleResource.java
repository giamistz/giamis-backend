package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.Role;
import gov.giamis.service.setup.AuthorityService;
import gov.giamis.service.setup.RoleQueryService;
import gov.giamis.service.setup.RoleService;
import gov.giamis.service.setup.dto.RoleCriteria;
import gov.giamis.service.setup.dto.RoleDTO;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Role}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api( value = "Role management Resource", description = "Operations to manage roles")
public class RoleResource {

    private final Logger log = LoggerFactory.getLogger(RoleResource.class);

    private static final String ENTITY_NAME = "role";

    private final RoleService roleService;


    private final RoleQueryService roleQueryService;

    private final AuthorityService authorityService;

    public RoleResource(RoleService roleService,
    		RoleQueryService roleQueryService,
    		AuthorityService authorityService) {
        this.roleService = roleService;
        this.roleQueryService = roleQueryService;
        this.authorityService = authorityService;
    }

    /**
     * {@code POST  /roles} : Create a new role.
     *
     * @param role the role to create.
     * @return the {@link CustomApiResponse} with status {@code 201 (Created)} and with body the new role, or with status {@code 400 (Bad Request)} if the role has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/roles")
    @ApiOperation(value = "Create Role")
	@ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createRole(@Valid @RequestBody Role role) throws URISyntaxException {
        log.debug("REST request to save Role : {}", role);
        if (role.getId() != null) {
            throw new BadRequestAlertException("A new role already have an ID", ENTITY_NAME, "id exists");
        }

        if (roleService.findByName(role.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
        Role result = roleService.save(role);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /roles} : Updates an existing role.
     *
     * @param role the role to update.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with body the updated role,
     * or with status {@code 400 (Bad Request)} if the role is not valid,
     * or with status {@code 500 (Internal Server Error)} if the role couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/roles")
    @ApiOperation( value = "Update existing Role")
    public CustomApiResponse updateRole(@Valid @RequestBody Role role) throws URISyntaxException {
        log.debug("REST request to update Role : {}", role);

        if (role.getId() == null) {
			throw new BadRequestAlertException("Updated Role does not have an ID", ENTITY_NAME,
					"id not exists");
		}

		if (!roleService.findOne(role.getId()).isPresent()) {
			throw new BadRequestAlertException("Role with ID: " + role.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
        Role result = roleService.save(role);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /roles} : get all the roles.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and the list of roles in body.
     */
    @GetMapping("/roles")
    @ApiOperation(value = "Get all Roles with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllRoles(RoleCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Roles by criteria: {}", criteria);
        Page<Role> page = roleQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /roles/:id} : get the "id" role.
     *
     * @param id the id of the role to retrieve.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and with body the role, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/roles/{id}")
    @ApiOperation("Get a single role by ID")
    public CustomApiResponse getRole(@PathVariable Long id) {
        log.debug("REST request to get Role for Assigning Authorities : {}", id);

        if (!roleService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Role with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<Role> role = roleService.findOne(id);
        return CustomApiResponse.ok(role);
    }

    @GetMapping("/roles-authorities")
    @ApiOperation("Get a single role by ID for Assigning Authorities")
    public CustomApiResponse getRoleAuthorities(Pageable pageable) {
        log.debug("REST request to get Role for adding Authorities");

        List<Authority> authority = authorityService.findDistinctAuthority();
        List<RoleDTO> roleDTOList = new ArrayList<RoleDTO>();
        RoleDTO roleDTO = null;

        for(Authority auth:authority) {
        	roleDTO = new RoleDTO();
        	roleDTO.setName(auth.getResource());
        	roleDTO.setAuthorities(authorityService.getAllAuthorities(auth.getResource()));
        	roleDTOList.add(roleDTO);
        }
        return CustomApiResponse.ok(roleDTOList);
    }

    @PostMapping("/roles-authorities")
    @ApiOperation("Assign Authorities for Single Role by role ID")
    public CustomApiResponse saveRoleAuthority(@RequestBody Role role) throws URISyntaxException{

    	if (!roleService.findOne(role.getId()).isPresent()) {
			throw new BadRequestAlertException("Role with ID: " + role.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
       Role assignRole = roleService.findRoleById(role.getId());
       assignRole.setAuthorities(role.getAuthorities());
       roleService.save(assignRole);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.ASSIGN_SUCCESS));

    }

    /**
     * {@code DELETE  /roles/:id} : delete the "id" role.
     *
     * @param id the id of the role to delete.
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)}.
     */
    @DeleteMapping("/roles/{id}")
    @ApiOperation("Delete a single role by ID")
    public CustomApiResponse deleteRole(@PathVariable Long id) {
        log.debug("REST request to delete Role : {}", id);

        if (!roleService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Role with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        roleService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
