package gov.giamis.web.rest.setup;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.domain.setup.MenuItem;
import gov.giamis.domain.setup.User;
import gov.giamis.repository.setup.UserRepository;
import gov.giamis.security.NoAthorization;
import gov.giamis.security.jwt.JWTFilter;
import gov.giamis.security.jwt.TokenProvider;
import gov.giamis.service.auditplan.impl.StrategicAuditPlanServiceImpl;
import gov.giamis.service.setup.*;
import gov.giamis.service.setup.dto.MenuDTO;
import gov.giamis.service.util.Utils;
import gov.giamis.web.rest.errors.ApiError;
import gov.giamis.web.rest.response.CustomApiResponse;
import gov.giamis.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Controller to authenticate user")
public class UserJWTController {

    private final Logger log = LoggerFactory.getLogger(UserJWTController.class);

    private final TokenProvider tokenProvider;

	private final AuthenticationManagerBuilder authenticationManagerBuilder;

	private final UserService userService;

	@Autowired
	private MenuGroupService menuGroupService;

	@Autowired
	private AuthorityService authorityService;

	@Autowired
	private MenuItemService menuItemService;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
    private FinancialYearService financialYearService;

	@Autowired
	public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder,
			UserService userService) {
		this.tokenProvider = tokenProvider;
		this.authenticationManagerBuilder = authenticationManagerBuilder;
		this.userService = userService;
	}

	@PostMapping("/authenticate")
	@ApiOperation(" Authenticate user and JWT")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = CustomApiResponse.class, message = "successfully login"),
			@ApiResponse(code = 401, response = ApiError.class, message = "Unauthorized") })
	@NoAthorization
	public CustomApiResponse authorize(@Valid @RequestBody LoginVM loginVM) {

		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				loginVM.getEmail(), loginVM.getPassword());

		Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();

		String jwt = tokenProvider.createToken(authentication, rememberMe);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

		User user = userService.getUserWithAuthoritiesByLogin(authentication.getName()).get();

		Set<Authority> authorities = authorityService.getAllAuthorities(user.getId());
		user.setAuthorities(authorities);

		List<Long> authIds = new ArrayList<>();
        List<String> auth = new ArrayList<>();

		authIds.add(0L);
		authorities.forEach(a -> {
		    authIds.add(a.getId());
            auth.add(a.getName());
        });

        List<MenuItem> menuItems = menuItemService.getByAuthorities(authIds);
        List<MenuItem> menuItems2 = menuItemService.getWithNoGroupByAuthorities(authIds);

        Map<String,List<Long>> groupItemIds = Utils.getMenuGroup(menuItems);

		cacheManager.getCache(UserRepository.AUTHORITIES_BY_EMAIL_CACHE).put(authentication.getName(), auth);

        List<MenuGroup> itemAsGroup = menuItems2
            .stream()
            .map(i -> new MenuGroup(i.getId(), i.getName(), i.getIcon(), i.getPath(), i.getSortOrder()))
            .collect(Collectors.toList());
        itemAsGroup.addAll(menuItemService.getWithItems(groupItemIds));
        Collections.sort(itemAsGroup, (o1, o2) -> (int) (o1.getSortOrder() - o2.getSortOrder()));
		Map<String, Object> body = new HashMap<>();
		user.setMenus(itemAsGroup);
		body.put("token", jwt);
		body.put("user", user);
		return CustomApiResponse.ok("LOGIN SUCCESSFULLY" ,body);
	}

	/**
	 * Object to return as body in JWT Authentication.
	 */
	static class JWTToken {

		private String idToken;

		JWTToken(String idToken) {
			this.idToken = idToken;
		}

		@JsonProperty("id_token")
		String getIdToken() {
			return idToken;
		}

		void setIdToken(String idToken) {
			this.idToken = idToken;
		}
	}
}
