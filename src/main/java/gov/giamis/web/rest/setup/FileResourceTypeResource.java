package gov.giamis.web.rest.setup;


import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.FileResourceType;
import gov.giamis.service.setup.FileResourceTypeQueryService;
import gov.giamis.service.setup.FileResourceTypeService;
import gov.giamis.service.setup.dto.FileResourceTypeCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link FileResourceType}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "File Resource Type Resource", description = "Operations to manage File Resource Type")
public class FileResourceTypeResource {

	private final Logger log = LoggerFactory.getLogger(FileResourceTypeResource.class);

	private static final String ENTITY_NAME = "fileResourceType";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final FileResourceTypeService fileResourceTypeService;

	private final FileResourceTypeQueryService fileResourceTypeQueryService;

	public FileResourceTypeResource(FileResourceTypeService fileResourceTypeService,
			FileResourceTypeQueryService fileResourceTypeQueryService) {
		this.fileResourceTypeService = fileResourceTypeService;
		this.fileResourceTypeQueryService = fileResourceTypeQueryService;
	}

	/**
	 * {@code POST  /file-resource-types} : Create a new fileResourceType.
	 *
	 * @param fileResourceType the fileResourceType to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new fileResourceType, or with status
	 *         {@code 400 (Bad Request)} if the fileResourceType has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/file-resource-types")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create File Resource Type")
	public CustomApiResponse createFileResourceType(@Valid @RequestBody FileResourceType fileResourceType)
			throws URISyntaxException {
		log.debug("REST request to save FileResourceType : {}", fileResourceType);

		if (fileResourceType.getId() != null) {
			throw new BadRequestAlertException("A new File Resource Type already have an ID", ENTITY_NAME, "id exists");
		}

		if (fileResourceTypeService.findByName(fileResourceType.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		FileResourceType result = fileResourceTypeService.save(fileResourceType);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /file-resource-types} : Updates an existing fileResourceType.
	 *
	 * @param fileResourceType the fileResourceType to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated fileResourceType, or with status
	 *         {@code 400 (Bad Request)} if the fileResourceType is not valid, or
	 *         with status {@code 500 (Internal Server Error)} if the
	 *         fileResourceType couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/file-resource-types")
	@ApiOperation(value = "Update existing File Resource Type")
	public CustomApiResponse updateFileResourceType(@Valid @RequestBody FileResourceType fileResourceType)
			throws URISyntaxException {
		log.debug("REST request to update FileResourceType : {}", fileResourceType);

		if (fileResourceType.getId() == null) {
			throw new BadRequestAlertException("Updated File Resource Type does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		
		if (!fileResourceTypeService.findOne(fileResourceType.getId()).isPresent()) {
			throw new BadRequestAlertException("File Resource Type with ID: " + fileResourceType.getId() + " not found",
					ENTITY_NAME, "id not exists");
		}
		FileResourceType result = fileResourceTypeService.save(fileResourceType);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /file-resource-types} : get all the fileResourceTypes.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of fileResourceTypes in body.
	 */
	@GetMapping("/file-resource-types")
	@ApiOperation(value = "Get all  File Resource Types; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllFileResourceTypes(FileResourceTypeCriteria criteria, Pageable pageable) {
		log.debug("REST request to get FileResourceTypes by criteria: {}", criteria);
		Page<FileResourceType> page = fileResourceTypeQueryService.findByCriteria(criteria, pageable);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /file-resource-types/count} : count all the fileResourceTypes.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/file-resource-types/count")
	@ApiOperation(value = "Count all  File Resource Types; Optional filters can be applied")
	public CustomApiResponse countFileResourceTypes(FileResourceTypeCriteria criteria) {
		log.debug("REST request to count FileResourceTypes by criteria: {}", criteria);
		return CustomApiResponse.ok(fileResourceTypeQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /file-resource-types/:id} : get the "id" fileResourceType.
	 *
	 * @param id the id of the fileResourceType to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the fileResourceType, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/file-resource-types/{id}")
	@ApiOperation(value = "Get a single File Resource Type by ID")
	public CustomApiResponse getFileResourceType(@PathVariable Long id) {
		log.debug("REST request to get FileResourceType : {}", id);

		if (!fileResourceTypeService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("File Resource Type with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<FileResourceType> fileResourceType = fileResourceTypeService.findOne(id);
		return CustomApiResponse.ok(fileResourceType);
	}

	/**
	 * {@code DELETE  /file-resource-types/:id} : delete the "id" fileResourceType.
	 *
	 * @param id the id of the fileResourceType to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/file-resource-types/{id}")
	@ApiOperation(value = "Delete a single File Resource Type by ID")
	public CustomApiResponse deleteFileResourceType(@PathVariable Long id) {
		log.debug("REST request to delete FileResourceType : {}", id);

		if (!fileResourceTypeService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("File Resource Type with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		fileResourceTypeService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
