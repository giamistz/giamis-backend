package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.service.setup.AuditableAreaQueryService;
import gov.giamis.service.setup.AuditableAreaService;
import gov.giamis.service.setup.dto.AuditableAreaCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link AuditableArea}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Auditable Area Resource ", description = "Operations to manage Auditable Area")
public class AuditableAreaResource {

	private final Logger log = LoggerFactory.getLogger(AuditableAreaResource.class);

	private static final String ENTITY_NAME = "auditableAreas";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final AuditableAreaService auditableAreaService;

	private final AuditableAreaQueryService auditableAreaQueryService;

	public AuditableAreaResource(AuditableAreaService auditableAreaService,
			AuditableAreaQueryService auditableAreaQueryService) {
		this.auditableAreaService = auditableAreaService;
		this.auditableAreaQueryService = auditableAreaQueryService;
	}

	/**
	 * {@code POST  /auditable-areas} : Create a new auditableArea.
	 *
	 * @param auditableArea the auditableArea to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new auditableArea, or with status {@code 400 (Bad Request)}
	 *         if the auditableArea has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/auditable-areas")
	@ApiOperation(value = "create  Auditable Area")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomApiResponse createAuditableArea(@Valid @RequestBody AuditableArea auditableArea)
			throws URISyntaxException {
		log.debug("REST request to save AuditableArea : {}", auditableArea);

		if (auditableArea.getId() != null) {
			throw new BadRequestAlertException("A new Auditable Area already have an ID", ENTITY_NAME, "id exists");
		}

		if (auditableAreaService.findByName(auditableArea.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		AuditableArea result = auditableAreaService.save(auditableArea);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /auditable-areas} : Updates an existing auditableArea.
	 *
	 * @param auditableArea the auditableArea to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated auditableArea, or with status {@code 400 (Bad Request)}
	 *         if the auditableArea is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the auditableArea couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/auditable-areas")
	@ApiOperation(value = "Update existing  Auditable Area")
	public CustomApiResponse updateAuditableArea(@Valid @RequestBody AuditableArea auditableArea)
			throws URISyntaxException {
		log.debug("REST request to update AuditableArea : {}", auditableArea);

		if (auditableArea.getId() == null) {
			throw new BadRequestAlertException("Updated Auditable Area does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!auditableAreaService.findOne(auditableArea.getId()).isPresent()) {
			throw new BadRequestAlertException("Auditable Area with ID: " + auditableArea.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}

		AuditableArea result = auditableAreaService.save(auditableArea);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /auditable-areas} : get all the auditableAreas.
	 *
	 *
	 * @param pageable the pagination information.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of auditableAreas in body.
	 */
	@GetMapping("/auditable-areas")
	@ApiOperation(value = "Get all  Auditable Areas; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllAuditableAreas(AuditableAreaCriteria criteria, Pageable pageable) {
		log.debug("REST request to get AuditableAreas by criteria: {}", criteria);
		Page<AuditableArea> page = auditableAreaQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	@GetMapping("/auditable-areas/all")
    @ApiOperation(value = "Get all auditable areas without pagination")
	public CustomApiResponse allAuditableAreas(AuditableAreaCriteria auditableAreaCriteria) {
	    log.debug("REST request to get auditable areas by criteria: {}", auditableAreaCriteria);

        List<AuditableArea> list = auditableAreaQueryService.findByCriteria(auditableAreaCriteria);
        return CustomApiResponse.ok(list);
    }

	/**
	 * {@code GET  /auditable-areas/count} : count all the auditableAreas.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/auditable-areas/count")
	@ApiOperation(value = "Count all  Auditable Areas; Optional filters can be applied")
	public CustomApiResponse countAuditableAreas(AuditableAreaCriteria criteria) {
		log.debug("REST request to count AuditableAreas by criteria: {}", criteria);
		return CustomApiResponse.ok(auditableAreaQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /auditable-areas/:id} : get the "id" auditableArea.
	 *
	 * @param id the id of the auditableArea to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the auditableArea, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/auditable-areas/{id}")
	@ApiOperation(value = "Get a single  Auditable Area by ID")
	public CustomApiResponse getAuditableArea(@PathVariable Long id) {
		log.debug("REST request to get AuditableArea : {}", id);

		if (!auditableAreaService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Auditable Area with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		Optional<AuditableArea> auditableArea = auditableAreaService.findOne(id);
		return CustomApiResponse.ok(auditableArea);
	}

	/**
	 * {@code DELETE  /auditable-areas/:id} : delete the "id" auditableArea.
	 *
	 * @param id the id of the auditableArea to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/auditable-areas/{id}")
	@ApiOperation(value = "Delete a single Auditable Area by ID")
	public CustomApiResponse deleteAuditableArea(@PathVariable Long id) {
		log.debug("REST request to delete AuditableArea : {}", id);

		if (!auditableAreaService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Auditable Area with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

		auditableAreaService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
