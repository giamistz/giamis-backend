package gov.giamis.web.rest.setup;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.OrganisationUnitGroupSet;
import gov.giamis.service.setup.OrganisationUnitGroupSetQueryService;
import gov.giamis.service.setup.OrganisationUnitGroupSetService;
import gov.giamis.service.setup.dto.OrganisationUnitGroupSetCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing
 * {@link OrganisationUnitGroupSet}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Organization unit Group Set Resource", description = "Operations to manage Organization unit Group Set")
public class OrganisationUnitGroupSetResource {

	private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupSetResource.class);

	private static final String ENTITY_NAME = "organisationUnitGroupSets";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final OrganisationUnitGroupSetService organisationUnitGroupSetService;

	private final OrganisationUnitGroupSetQueryService organisationUnitGroupSetQueryService;

	public OrganisationUnitGroupSetResource(OrganisationUnitGroupSetService organisationUnitGroupSetService,
			OrganisationUnitGroupSetQueryService organisationUnitGroupSetQueryService) {
		this.organisationUnitGroupSetService = organisationUnitGroupSetService;
		this.organisationUnitGroupSetQueryService = organisationUnitGroupSetQueryService;
	}

	/**
	 * {@code POST  /organisation-unit-group-sets} : Create a new
	 * organisationUnitGroupSet.
	 *
	 * @param organisationUnitGroupSet the organisationUnitGroupSet to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new organisationUnitGroupSet, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitGroupSet has already
	 *         an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/organisation-unit-group-sets")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create Organisation Unit Group Set")
	public CustomApiResponse createOrganisationUnitGroupSet(
			@Valid @RequestBody OrganisationUnitGroupSet organisationUnitGroupSet) throws URISyntaxException {
		log.debug("REST request to save OrganisationUnitGroupSet : {}", organisationUnitGroupSet);

		if (organisationUnitGroupSet.getId() != null) {
			throw new BadRequestAlertException("A new Organisation Unit Group Set already have an ID", ENTITY_NAME,
					"id exists");
		}

		if (organisationUnitGroupSetService.findByName(organisationUnitGroupSet.getName()) != null) {
			throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
		}
		OrganisationUnitGroupSet result = organisationUnitGroupSetService.save(organisationUnitGroupSet);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

	}

	/**
	 * {@code PUT  /organisation-unit-group-sets} : Updates an existing
	 * organisationUnitGroupSet.
	 *
	 * @param organisationUnitGroupSet the organisationUnitGroupSet to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated organisationUnitGroupSet, or with status
	 *         {@code 400 (Bad Request)} if the organisationUnitGroupSet is not
	 *         valid, or with status {@code 500 (Internal Server Error)} if the
	 *         organisationUnitGroupSet couldn't be updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/organisation-unit-group-sets")
	@ApiOperation(value = "Update existing Organisation Unit Group Set")
	public CustomApiResponse updateOrganisationUnitGroupSet(
			@Valid @RequestBody OrganisationUnitGroupSet organisationUnitGroupSet) throws URISyntaxException {
		log.debug("REST request to update OrganisationUnitGroupSet : {}", organisationUnitGroupSet);

		if (organisationUnitGroupSet.getId() == null) {
			throw new BadRequestAlertException("Updated Organisation Unit Group Set does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!organisationUnitGroupSetService.findOne(organisationUnitGroupSet.getId()).isPresent()) {
			throw new BadRequestAlertException(
					"Organisation Unit Group Set with ID: " + organisationUnitGroupSet.getId() + " not found",
					ENTITY_NAME, "id not exists");
		}
		OrganisationUnitGroupSet result = organisationUnitGroupSetService.save(organisationUnitGroupSet);
		return CustomApiResponse.created(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
	}

	/**
	 * {@code GET  /organisation-unit-group-sets} : get all the
	 * organisationUnitGroupSets.
	 *
	 * 
	 * @param pageable the pagination information.
	 * 
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of organisationUnitGroupSets in body.
	 */
	@GetMapping("/organisation-unit-group-sets")
	@ApiOperation(value = "Get all Organisation Unit Group Sets; with pagination, optional filters by fields can be applied")
	public CustomApiResponse getAllOrganisationUnitGroupSets(OrganisationUnitGroupSetCriteria criteria,
			Pageable pageable) {
		log.debug("REST request to get OrganisationUnitGroupSets by criteria: {}", criteria);
		Page<OrganisationUnitGroupSet> page = organisationUnitGroupSetQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return CustomApiResponse.ok(page);
	}

	/**
	 * {@code GET  /organisation-unit-group-sets/count} : count all the
	 * organisationUnitGroupSets.
	 *
	 * @param criteria the criteria which the requested entities should match.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
	 *         in body.
	 */
	@GetMapping("/organisation-unit-group-sets/count")
	@ApiOperation(value = "Count all Organisation Unit Group Sets; Optional filters can be applied")
	public CustomApiResponse countOrganisationUnitGroupSets(OrganisationUnitGroupSetCriteria criteria) {
		log.debug("REST request to count OrganisationUnitGroupSets by criteria: {}", criteria);
		return CustomApiResponse.ok(organisationUnitGroupSetQueryService.countByCriteria(criteria));
	}

	/**
	 * {@code GET  /organisation-unit-group-sets/:id} : get the "id"
	 * organisationUnitGroupSet.l
	 *
	 * @param id the id of the organisationUnitGroupSet to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the organisationUnitGroupSet, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/organisation-unit-group-sets/{id}")
	@ApiOperation(value = "Get a single Organisation Unit Group Set by ID")
	public CustomApiResponse getOrganisationUnitGroupSet(@PathVariable Long id) {
		log.debug("REST request to get OrganisationUnitGroupSet : {}", id);
		if (!organisationUnitGroupSetService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Group Set with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		Optional<OrganisationUnitGroupSet> organisationUnitGroupSet = organisationUnitGroupSetService.findOne(id);
		return CustomApiResponse.ok(organisationUnitGroupSet);
	}

	/**
	 * {@code DELETE  /organisation-unit-group-sets/:id} : delete the "id"
	 * organisationUnitGroupSet.
	 *
	 * @param id the id of the organisationUnitGroupSet to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/organisation-unit-group-sets/{id}")
	@ApiOperation(value = "Delete a single Organisation Unit Group Set by ID")
	public CustomApiResponse deleteOrganisationUnitGroupSet(@PathVariable Long id) {
		log.debug("REST request to delete OrganisationUnitGroupSet : {}", id);

		if (!organisationUnitGroupSetService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Organisation Unit Group Set with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
		organisationUnitGroupSetService.delete(id);
		return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
	}
}
