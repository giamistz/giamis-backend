package gov.giamis.web.rest.report;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.report.ReportContentValue;
import gov.giamis.service.report.ReportContentValueQueryService;
import gov.giamis.service.report.ReportContentValueService;
import gov.giamis.service.report.dto.ReportContentValueCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link ReportContentValue}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "ReportContentValue Resource", description = "Operations to manage Report Content Values Values")
public class ReportContentValueResource {

    private final Logger log = LoggerFactory.getLogger(ReportContentValueResource.class);

    private static final String ENTITY_NAME = "reportContentValues";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReportContentValueService reportContentValuesService;

    private final ReportContentValueQueryService reportContentValuesQueryService;

    public ReportContentValueResource(ReportContentValueService reportContentValuesService, ReportContentValueQueryService reportContentValuesQueryService) {
        this.reportContentValuesService = reportContentValuesService;
        this.reportContentValuesQueryService = reportContentValuesQueryService;
    }

    /**
     * {@code POST  /report-content-values} : Create a new reportContentValues.
     *
     * @param reportContentValues the reportContentValues to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new reportContentValues, or with status {@code 400 (Bad Request)} if
     *         the reportContentValues has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/report-content-values")
    @ApiOperation(value = "create  Report Content Values")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createReportContentValue(@Valid @RequestBody ReportContentValue reportContentValues) throws URISyntaxException {
        log.debug("REST request to save Report Content Values : {}", reportContentValues);

        if (reportContentValues.getId() != null) {
            throw new BadRequestAlertException("A new Report Content Values already have an ID", ENTITY_NAME, "id exists");
        }
        ReportContentValue result = reportContentValuesService.save(reportContentValues);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /report-content-values} : Updates an existing reportContentValues.
     *
     * @param reportContentValues the reportContentValues to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated reportContentValues, or with status {@code 400 (Bad Request)} if
     *         the reportContentValues is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the reportContentValues couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/report-content-values")
    @ApiOperation(value = "update existing Report Content Values")
    public CustomApiResponse updateReportContentValue(@Valid @RequestBody ReportContentValue reportContentValues) throws URISyntaxException {
        log.debug("REST request to update Report Content Values : {}", reportContentValues);

        if (reportContentValues.getId() == null) {
            throw new BadRequestAlertException("Updated Report Content Values does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!reportContentValuesService.findOne(reportContentValues.getId()).isPresent()) {
            throw new BadRequestAlertException("Report Content Values with ID: " + reportContentValues.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        ReportContentValue result = reportContentValuesService.save(reportContentValues);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /report-content-values} : get all the report-content-values.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of report-content-values in body.
     */
    @GetMapping("/report-content-values")
    @ApiOperation(value = "Get all report-content-values; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllReportContentValues(ReportContentValueCriteria criteria, Pageable pageable) {
        log.debug("REST request to get report-content-values by criteria: {}", criteria);
        Page<ReportContentValue> page = reportContentValuesQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /report-content-values/count} : count all the report-content-values.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/report-content-values/count")
    @ApiOperation(value = "Count all report-content-values; Optional filters can be applied")
    public CustomApiResponse countReportContentValues(ReportContentValueCriteria criteria) {
        log.debug("REST request to count report-content-values by criteria: {}", criteria);
        return CustomApiResponse.ok(reportContentValuesQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /report-content-values/:id} : get the "id" reportContentValues.
     *
     * @param id the id of the reportContentValues to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the reportContentValues, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/report-content-values/{id}")
    @ApiOperation(value = "Get a single ReportContentValue by ID")
    public CustomApiResponse getReportContentValue(@PathVariable Long id) {
        log.debug("REST request to get ReportContentValue : {}", id);

        if (!reportContentValuesService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportContentValue with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<ReportContentValue> reportContentValues = reportContentValuesService.findOne(id);
        return CustomApiResponse.ok(reportContentValues);
    }

    /**
     * {@code DELETE  /report-content-values/:id} : delete the "id" reportContentValues.
     *
     * @param id the id of the reportContentValues to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/report-content-values/{id}")
    @ApiOperation(value = "Delete a single ReportContentValue by ID")
    public CustomApiResponse deleteReportContentValue(@PathVariable Long id) {
        log.debug("REST request to delete ReportContentValue : {}", id);

        if (!reportContentValuesService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportContentValue with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }

        reportContentValuesService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
