package gov.giamis.web.rest.report;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.report.ReportContent;
import gov.giamis.service.report.ReportContentQueryService;
import gov.giamis.service.report.ReportContentService;
import gov.giamis.service.report.dto.ReportContentCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link ReportContent}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "ReportContent Resource", description = "Operations to manage Report Contents")
public class ReportContentResource {

    private final Logger log = LoggerFactory.getLogger(ReportContentResource.class);

    private static final String ENTITY_NAME = "reportContent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReportContentService reportContentService;

    private final ReportContentQueryService reportContentQueryService;

    public ReportContentResource(ReportContentService reportContentService, ReportContentQueryService reportContentQueryService) {
        this.reportContentService = reportContentService;
        this.reportContentQueryService = reportContentQueryService;
    }

    /**
     * {@code POST  /report-contents} : Create a new reportContent.
     *
     * @param reportContent the reportContent to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new reportContent, or with status {@code 400 (Bad Request)} if
     *         the reportContent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/report-contents")
    @ApiOperation(value = "create  Report Content")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createReportContent(@Valid @RequestBody ReportContent reportContent) throws URISyntaxException {
        log.debug("REST request to save Report Content : {}", reportContent);

        if (reportContent.getId() != null) {
            throw new BadRequestAlertException("A new Report Content already have an ID", ENTITY_NAME, "id exists");
        }
        ReportContent result = reportContentService.save(reportContent);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /report-contents} : Updates an existing reportContent.
     *
     * @param reportContent the reportContent to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated reportContent, or with status {@code 400 (Bad Request)} if
     *         the reportContent is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the reportContent couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/report-contents")
    @ApiOperation(value = "update existing Report Content")
    public CustomApiResponse updateReportContent(@Valid @RequestBody ReportContent reportContent) throws URISyntaxException {
        log.debug("REST request to update Report Content : {}", reportContent);

        if (reportContent.getId() == null) {
            throw new BadRequestAlertException("Updated Report Content does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!reportContentService.findOne(reportContent.getId()).isPresent()) {
            throw new BadRequestAlertException("Report Content with ID: " + reportContent.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        ReportContent result = reportContentService.save(reportContent);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /report-contents} : get all the report-contents.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of report-contents in body.
     */
    @GetMapping("/report-contents")
    @ApiOperation(value = "Get all report-contents; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllReportContents(ReportContentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get report-contents by criteria: {}", criteria);
        Page<ReportContent> page = reportContentQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /report-contents/count} : count all the report-contents.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/report-contents/count")
    @ApiOperation(value = "Count all report-contents; Optional filters can be applied")
    public CustomApiResponse countReportContents(ReportContentCriteria criteria) {
        log.debug("REST request to count report-contents by criteria: {}", criteria);
        return CustomApiResponse.ok(reportContentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /report-contents/:id} : get the "id" reportContent.
     *
     * @param id the id of the reportContent to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the reportContent, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/report-contents/{id}")
    @ApiOperation(value = "Get a single ReportContent by ID")
    public CustomApiResponse getReportContent(@PathVariable Long id) {
        log.debug("REST request to get ReportContent : {}", id);

        if (!reportContentService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportContent with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<ReportContent> reportContent = reportContentService.findOne(id);
        return CustomApiResponse.ok(reportContent);
    }

    /**
     * {@code DELETE  /report-contents/:id} : delete the "id" reportContent.
     *
     * @param id the id of the reportContent to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/report-contents/{id}")
    @ApiOperation(value = "Delete a single ReportContent by ID")
    public CustomApiResponse deleteReportContent(@PathVariable Long id) {
        log.debug("REST request to delete ReportContent : {}", id);

        if (!reportContentService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportContent with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }

        reportContentService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
