package gov.giamis.web.rest.report;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.report.ReportParam;
import gov.giamis.service.report.ReportParamQueryService;
import gov.giamis.service.report.ReportParamService;
import gov.giamis.service.report.dto.ReportParamCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link ReportParam}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "ReportParam Resource", description = "Operations to manage Report Params")
public class ReportParamResource {

    private final Logger log = LoggerFactory.getLogger(ReportParamResource.class);

    private static final String ENTITY_NAME = "reportParam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReportParamService reportParamService;

    private final ReportParamQueryService reportParamQueryService;

    public ReportParamResource(ReportParamService reportParamService, ReportParamQueryService reportParamQueryService) {
        this.reportParamService = reportParamService;
        this.reportParamQueryService = reportParamQueryService;
    }

    /**
     * {@code POST  /report-params} : Create a new reportParam.
     *
     * @param reportParam the reportParam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new reportParam, or with status {@code 400 (Bad Request)} if
     *         the reportParam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/report-params")
    @ApiOperation(value = "create  Report Param")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createReportParam(@Valid @RequestBody ReportParam reportParam) throws URISyntaxException {
        log.debug("REST request to save Report Param : {}", reportParam);

        if (reportParam.getId() != null) {
            throw new BadRequestAlertException("A new Report Param already have an ID", ENTITY_NAME, "id exists");
        }
        ReportParam result = reportParamService.save(reportParam);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /report-params} : Updates an existing reportParam.
     *
     * @param reportParam the reportParam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated reportParam, or with status {@code 400 (Bad Request)} if
     *         the reportParam is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the reportParam couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/report-params")
    @ApiOperation(value = "update existing Report Param")
    public CustomApiResponse updateReportParam(@Valid @RequestBody ReportParam reportParam) throws URISyntaxException {
        log.debug("REST request to update Report Param : {}", reportParam);

        if (reportParam.getId() == null) {
            throw new BadRequestAlertException("Updated Report Param does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!reportParamService.findOne(reportParam.getId()).isPresent()) {
            throw new BadRequestAlertException("Report Param with ID: " + reportParam.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        ReportParam result = reportParamService.save(reportParam);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /report-params} : get all the report-params.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of report-params in body.
     */
    @GetMapping("/report-params")
    @ApiOperation(value = "Get all Report Params; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllReportParams(ReportParamCriteria criteria, Pageable pageable) {
        log.debug("REST request to get report-params by criteria: {}", criteria);
        Page<ReportParam> page = reportParamQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /report-params/count} : count all the report-params.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/report-params/count")
    @ApiOperation(value = "Count all Report Params; Optional filters can be applied")
    public CustomApiResponse countReportParams(ReportParamCriteria criteria) {
        log.debug("REST request to count report-params by criteria: {}", criteria);
        return CustomApiResponse.ok(reportParamQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /report-params/:id} : get the "id" reportParam.
     *
     * @param id the id of the reportParam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the reportParam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/report-params/{id}")
    @ApiOperation(value = "Get a single ReportParam by ID")
    public CustomApiResponse getReportParam(@PathVariable Long id) {
        log.debug("REST request to get ReportParam : {}", id);

        if (!reportParamService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportParam with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<ReportParam> reportParam = reportParamService.findOne(id);
        return CustomApiResponse.ok(reportParam);
    }

    /**
     * {@code DELETE  /report-params/:id} : delete the "id" reportParam.
     *
     * @param id the id of the reportParam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/report-params/{id}")
    @ApiOperation(value = "Delete a single ReportParam by ID")
    public CustomApiResponse deleteReportParam(@PathVariable Long id) {
        log.debug("REST request to delete ReportParam : {}", id);

        if (!reportParamService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("ReportParam with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }

        reportParamService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
