package gov.giamis.web.rest.report;

import gov.giamis.config.ApplicationProperties;
import gov.giamis.config.Constants;
import gov.giamis.security.NoAthorization;
import gov.giamis.service.report.ReportExportService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(Constants.API_V1+ "/report-exports")
public class ReportExportResource {

    private final ReportExportService reportService;

    private final ApplicationProperties apProperties;

    @Autowired
    public ReportExportResource(ReportExportService reportService,
                                ApplicationProperties apProperties) {
        this.reportService = reportService;
        this.apProperties = apProperties;
    }

    @GetMapping("/print/{type}/{idName}/{id}/{format}")
    @NoAthorization
    @Transactional
    public void print(HttpServletResponse response,
                          @PathVariable("type") String type,
                          @PathVariable("id") Long id,
                          @PathVariable("format") String format,
                          @PathVariable String idName)
        throws JRException, SQLException, IOException {

        JasperReport jasperReport= reportService.getTemplate(type);

        /**
         * Set report parameters
         */
        Map<String, Object> parameters = new HashMap<>();
        File file = ResourceUtils.getFile(apProperties.getFileStorageBaseDir().concat("/report/logo.png"));

        if (file.exists()) {
            parameters.put("logo", file.getAbsolutePath());
        }
        parameters.put(idName, id);

        //Create Temporary file for report
        File tmpFile = null;
        switch (format) {
            case "pdf":
               // parameters.put()

                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
                response.setContentType("application/pdf");
                //  response.addHeader("Content-Disposition", "attachment; filename=users.pdf");
                break;
            case "xls":
                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportXls();
                response.setContentType("application/vnd.ms-excel");
                response.addHeader("Content-Disposition", "attachment; filename=users.xls");
                break;
            case "doc":
                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportDoc();
                response.setContentType("application/msword");
                response.addHeader("Content-Disposition", "attachment; filename=users.doc");
                break;
        }

        Files.copy(tmpFile.toPath(), response.getOutputStream());
        response.getOutputStream().flush();
        tmpFile.delete();
    }

    @GetMapping("/printQtr/{type}/{organisationUnitId}/{quarterId}/{format}")
    @NoAthorization
    @Transactional
    public void printQtr(HttpServletResponse response,
                          @PathVariable("type") String type,
                          @PathVariable("organisationUnitId") Long organisationUnitId,
                          @PathVariable("quarterId") Long quarterId,
                          @PathVariable("format") String format)
        throws JRException, SQLException, IOException {

        JasperReport jasperReport= reportService.getTemplate(type);

        /**
         * Set report parameters
         */
        Map<String, Object> parameters = new HashMap<>();
        File file = ResourceUtils.getFile(apProperties.getFileStorageBaseDir().concat("/report/logo.png"));

        if (file.exists()) {
            parameters.put("logo", file.getAbsolutePath());
        }
        parameters.put("organisationUnitId", organisationUnitId);
        parameters.put("quarterId", quarterId);

        //Create Temporary file for report
        File tmpFile = null;
        switch (format) {
            case "pdf":
               // parameters.put()

                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
                response.setContentType("application/pdf");
                //  response.addHeader("Content-Disposition", "attachment; filename=users.pdf");
                break;
            case "xls":
                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportXls();
                response.setContentType("application/vnd.ms-excel");
                response.addHeader("Content-Disposition", "attachment; filename=users.xls");
                break;
            case "doc":
                tmpFile = reportService.setParams(parameters).setJasperReport(jasperReport).exportDoc();
                response.setContentType("application/msword");
                response.addHeader("Content-Disposition", "attachment; filename=users.doc");
                break;
        }

        Files.copy(tmpFile.toPath(), response.getOutputStream());
        response.getOutputStream().flush();
        tmpFile.delete();
    }
}
