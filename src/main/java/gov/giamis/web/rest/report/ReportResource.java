package gov.giamis.web.rest.report;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.report.Report;
import gov.giamis.service.report.ReportQueryService;
import gov.giamis.service.report.ReportService;
import gov.giamis.service.report.dto.ReportCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Report}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Report Resource", description = "Operations to manage Reports")
public class ReportResource {

    private final Logger log = LoggerFactory.getLogger(ReportResource.class);

    private static final String ENTITY_NAME = "reports";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReportService reportsService;

    private final ReportQueryService reportsQueryService;

    public ReportResource(ReportService reportsService, ReportQueryService reportsQueryService) {
        this.reportsService = reportsService;
        this.reportsQueryService = reportsQueryService;
    }

    /**
     * {@code POST  /reports} : Create a new reports.
     *
     * @param reports the reports to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new reports, or with status {@code 400 (Bad Request)} if
     *         the reports has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reports")
    @ApiOperation(value = "create Report")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createReport(@Valid @RequestBody Report reports) throws URISyntaxException {
        log.debug("REST request to save Reports : {}", reports);

        if (reports.getId() != null) {
            throw new BadRequestAlertException("A new Report already have an ID", ENTITY_NAME, "id exists");
        }
        Report result = reportsService.save(reports);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);

    }

    /**
     * {@code PUT  /reports} : Updates an existing reports.
     *
     * @param reports the reports to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated reports, or with status {@code 400 (Bad Request)} if
     *         the reports is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the reports couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reports")
    @ApiOperation(value = "update existing Report")
    public CustomApiResponse updateReport(@Valid @RequestBody Report reports) throws URISyntaxException {
        log.debug("REST request to update Report : {}", reports);

        if (reports.getId() == null) {
            throw new BadRequestAlertException("Updated Report does not have an ID", ENTITY_NAME, "id not exists");
        }

        if (!reportsService.findOne(reports.getId()).isPresent()) {
            throw new BadRequestAlertException("Reports with ID: " + reports.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Report result = reportsService.save(reports);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /reports} : get all the reports.
     *
     * 
     * @param pageable the pagination information.
     * 
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of reports in body.
     */
    @GetMapping("/reports")
    @ApiOperation(value = "Get all reports; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllReports(ReportCriteria criteria, Pageable pageable) {
        log.debug("REST request to get all reports by criteria: {}", criteria);
        Page<Report> page = reportsQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /reports/count} : count all the reports.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count
     *         in body.
     */
    @GetMapping("/reports/count")
    @ApiOperation(value = "Count all reports; Optional filters can be applied")
    public CustomApiResponse countReports(ReportCriteria criteria) {
        log.debug("REST request to count reports by criteria: {}", criteria);
        return CustomApiResponse.ok(reportsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /reports/:id} : get the "id" reports.
     *
     * @param id the id of the reports to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the reports, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reports/{id}")
    @ApiOperation(value = "Get a single Report by ID")
    public CustomApiResponse getReport(@PathVariable Long id) {
        log.debug("REST request to get Report : {}", id);

        if (!reportsService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Report with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<Report> reports = reportsService.findOne(id);
        return CustomApiResponse.ok(reports);
    }

    /**
     * {@code DELETE  /reports/:id} : delete the "id" reports.
     *
     * @param id the id of the reports to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reports/{id}")
    @ApiOperation(value = "Delete a single Report by ID")
    public CustomApiResponse deleteReport(@PathVariable Long id) {
        log.debug("REST request to delete Report : {}", id);

        if (!reportsService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Report with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }


        reportsService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
