package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.security.NoAthorization;
import gov.giamis.service.AuthoritySyncService;
import gov.giamis.web.rest.response.CustomApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(Constants.API_V1)
public class AuthoritySyncResource {

    @Autowired
    private AuthoritySyncService authoritySyncService;

    @GetMapping("/sync-menus")
    @NoAthorization
    public CustomApiResponse syncMenu() throws IOException {
        authoritySyncService.syncMenus();
        return CustomApiResponse.ok("Menu synchronized successfully");
    }

    @GetMapping("/sync-role-authorities/{id}")
    @NoAthorization
    public CustomApiResponse syncRoleAuthorities(@PathVariable Long id) {
        authoritySyncService.syncRoleAuthorities(id);
        return CustomApiResponse.ok("Role authorities  synchronized successfully");
    }

    @GetMapping("/sync-authorities")
    @NoAthorization
    public CustomApiResponse syncAuthorities() {
        authoritySyncService.syncAuthorities();
        return CustomApiResponse.ok("Authorities synchronized successfully");
    }

}
