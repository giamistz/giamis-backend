package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.domain.QuarterConsolidationReport;
import gov.giamis.service.QuarterConsolidationReportService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.QuarterConsolidationReport}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class QuarterConsolidationReportResource {

    private final Logger log = LoggerFactory.getLogger(QuarterConsolidationReportResource.class);

    private static final String ENTITY_NAME = "quarterConsolidationReport";

    private final QuarterConsolidationReportService quarterConsolidationReportService;

    public QuarterConsolidationReportResource(QuarterConsolidationReportService quarterConsolidationReportService) {
        this.quarterConsolidationReportService = quarterConsolidationReportService;
    }

    /**
     * {@code POST  /quarter-consolidation-reports} : Create a new quarterConsolidationReport.
     *
     * @param quarterConsolidationReport the quarterConsolidationReport to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new quarterConsolidationReport, or with status {@code 400 (Bad Request)} if the quarterConsolidationReport has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/quarter-consolidation-reports")
    public CustomApiResponse createQuarterConsolidationReport(@Valid @RequestBody QuarterConsolidationReport quarterConsolidationReport) throws URISyntaxException {
        log.debug("REST request to save QuarterConsolidationReport : {}", quarterConsolidationReport);
        if (quarterConsolidationReport.getId() != null) {
            throw new BadRequestAlertException("A new quarterConsolidationReport cannot already have an ID", ENTITY_NAME, "idexists");
        }
        QuarterConsolidationReport result = quarterConsolidationReportService.save(quarterConsolidationReport);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /quarter-consolidation-reports} : Updates an existing quarterConsolidationReport.
     *
     * @param quarterConsolidationReport the quarterConsolidationReport to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated quarterConsolidationReport,
     * or with status {@code 400 (Bad Request)} if the quarterConsolidationReport is not valid,
     * or with status {@code 500 (Internal Server Error)} if the quarterConsolidationReport couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/quarter-consolidation-reports")
    public CustomApiResponse updateQuarterConsolidationReport(@Valid @RequestBody QuarterConsolidationReport quarterConsolidationReport) throws URISyntaxException {
        log.debug("REST request to update QuarterConsolidationReport : {}", quarterConsolidationReport);
        if (quarterConsolidationReport.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        QuarterConsolidationReport result = quarterConsolidationReportService.save(quarterConsolidationReport);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /quarter-consolidation-reports} : get  the quarterConsolidationReports by ou and qrt.
     *
     * @return the {@link CustomApiResponse} with status {@code 200 (OK)} and  a quarterConsolidationReports in body.
     */
    @GetMapping("/quarter-consolidation-reports/{organisationUnitId}/{quarterId}")
    public CustomApiResponse getAllQuarterConsolidationReports(
                                                               @PathVariable Long organisationUnitId,
                                                               @PathVariable Long quarterId) {
        log.debug("REST request to get QuarterConsolidationReports by ou and quarter");
        QuarterConsolidationReport report = quarterConsolidationReportService.findByOuAndQtr(organisationUnitId, quarterId);
        return CustomApiResponse.ok(report);
    }


    /**
     * {@code GET  /quarter-consolidation-reports/:id} : get the "id" quarterConsolidationReport.
     *
     * @param id the id of the quarterConsolidationReport to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the quarterConsolidationReport, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/quarter-consolidation-reports/{id}")
    public CustomApiResponse getQuarterConsolidationReport(@PathVariable Long id) {
        log.debug("REST request to get QuarterConsolidationReport : {}", id);
        Optional<QuarterConsolidationReport> quarterConsolidationReport = quarterConsolidationReportService.findOne(id);
        return CustomApiResponse.ok(quarterConsolidationReport);
    }

    /**
     * {@code DELETE  /quarter-consolidation-reports/:id} : delete the "id" quarterConsolidationReport.
     *
     * @param id the id of the quarterConsolidationReport to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/quarter-consolidation-reports/{id}")
    public CustomApiResponse deleteQuarterConsolidationReport(@PathVariable Long id) {
        log.debug("REST request to delete QuarterConsolidationReport : {}", id);
        quarterConsolidationReportService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
