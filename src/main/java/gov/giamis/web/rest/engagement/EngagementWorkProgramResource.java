package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementWorkProgram;
import gov.giamis.service.engagement.EngagementWorkProgramQueryService;
import gov.giamis.service.engagement.EngagementWorkProgramService;
import gov.giamis.service.engagement.dto.EngagementWorkProgramCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing {@link EngagementWorkProgram}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Work Program Resource", description = "Operations to manage Engagement Work Programs")
public class EngagementWorkProgramResource {

    private final Logger log = LoggerFactory.getLogger(EngagementWorkProgramResource.class);

    private static final String ENTITY_NAME = "engagementWorkProgram";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementWorkProgramService engagementWorkProgramService;
    
    private final EngagementWorkProgramQueryService engagementWorkProgramQueryService;

    public EngagementWorkProgramResource(EngagementWorkProgramService engagementWorkProgramService, 
            EngagementWorkProgramQueryService engagementWorkProgramQueryService) {
        this.engagementWorkProgramService = engagementWorkProgramService;
        this.engagementWorkProgramQueryService = engagementWorkProgramQueryService;
    }

    /**
     * {@code POST  /engagement-work-programs} : Create a new engagementWorkProgram.
     *
     * @param engagementWorkProgram the engagementWorkProgram to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementWorkProgram, or with status {@code 400 (Bad Request)} if the engagementWorkProgram has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-work-programs")
    @ApiOperation(value = "Create Engagement Work Program")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementWorkProgram(@Valid @RequestBody EngagementWorkProgram engagementWorkProgram) throws URISyntaxException {
        log.debug("REST request to save Engagement Work Program : {}", engagementWorkProgram);
        if (engagementWorkProgram.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Work Program already have an ID", ENTITY_NAME, "id exists");
        }
        
        EngagementWorkProgram result = engagementWorkProgramService.save(engagementWorkProgram);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-work-programs} : Updates an existing engagementWorkProgram.
     *
     * @param engagementWorkProgram the engagementWorkProgram to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementWorkProgram,
     * or with status {@code 400 (Bad Request)} if the engagementWorkProgram is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementWorkProgram couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-work-programs")
    @ApiOperation(value = "Update existing  Engagement Work Program")
    public CustomApiResponse updateEngagementWorkProgram(@Valid @RequestBody EngagementWorkProgram engagementWorkProgram) throws URISyntaxException {
        log.debug("REST request to update Engagement Work Program : {}", engagementWorkProgram);

        if (engagementWorkProgram.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Work Program does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (!engagementWorkProgramService.findOne(engagementWorkProgram.getId()).isPresent()) {
            throw new BadRequestAlertException("Engagement Work Program with ID: " + engagementWorkProgram.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        
        EngagementWorkProgram result = engagementWorkProgramService.save(engagementWorkProgram);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS),result);
    }

    /**
     * {@code GET  /engagement-work-programs} : get all the engagementWorkPrograms.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementWorkPrograms in body.
     */
    @GetMapping("/engagement-work-programs")
    @ApiOperation(value = "Get all EngagementWorkPrograms; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementWorkPrograms(EngagementWorkProgramCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementWorkPrograms by criteria: {}", criteria);
        Page<EngagementWorkProgram> page = engagementWorkProgramQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-work-programs/count} : count all the engagementWorkPrograms.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-work-programs/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countEngagementWorkPrograms(EngagementWorkProgramCriteria criteria) {
        log.debug("REST request to count EngagementWorkPrograms by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementWorkProgramQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-work-programs/:id} : get the "id" engagementWorkProgram.
     *
     * @param id the id of the engagementWorkProgram to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementWorkProgram, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-work-programs/{id}")
    @ApiOperation("Get a single EngagementWorkProgram by ID")
    public CustomApiResponse getEngagementWorkProgram(@PathVariable Long id) {
        log.debug("REST request to get EngagementWorkProgram : {}", id);
        
        if (!engagementWorkProgramService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Work Program with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementWorkProgram> engagementWorkProgram = engagementWorkProgramService.findOne(id);
        return CustomApiResponse.ok(engagementWorkProgram);
    }

    /**
     * {@code DELETE  /engagement-work-programs/:id} : delete the "id" engagementWorkProgram.
     *
     * @param id the id of the engagementWorkProgram to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-work-programs/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteEngagementWorkProgram(@PathVariable Long id) {
        log.debug("REST request to delete EngagementWorkProgram : {}", id);
        
        if (!engagementWorkProgramService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Work Program with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementWorkProgramService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
    
}
