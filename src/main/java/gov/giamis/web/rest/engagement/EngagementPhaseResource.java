package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementPhase;
import gov.giamis.service.engagement.EngagementPhaseQueryService;
import gov.giamis.service.engagement.EngagementPhaseService;
import gov.giamis.service.engagement.dto.EngagementPhaseCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementPhase}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Phase Resource", description = "Operations to manage Engagement Phase")
public class EngagementPhaseResource {

    private final Logger log = LoggerFactory.getLogger(EngagementPhaseResource.class);

    private static final String ENTITY_NAME = "engagementPhase";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementPhaseService engagementPhaseService;

    private final EngagementPhaseQueryService engagementPhaseQueryService;

    public EngagementPhaseResource(EngagementPhaseService engagementPhaseService, EngagementPhaseQueryService engagementPhaseQueryService) {
        this.engagementPhaseService = engagementPhaseService;
        this.engagementPhaseQueryService = engagementPhaseQueryService;
    }

    /**
     * {@code POST  /engagement-phases} : Create a new engagementPhase.
     *
     * @param engagementPhase the engagementPhase to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementPhase, or with status {@code 400 (Bad Request)} if the engagementPhase has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-phases")
    @ApiOperation(value = "Create Engagement Phase")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementPhase(@Valid @RequestBody EngagementPhase engagementPhase) throws URISyntaxException {
        log.debug("REST request to save Engagement Phase : {}", engagementPhase);
        if (engagementPhase.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Phase  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementPhase result = engagementPhaseService.save(engagementPhase);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-phases} : Updates an existing engagementPhase.
     *
     * @param engagementPhase the engagementPhase to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementPhase,
     * or with status {@code 400 (Bad Request)} if the engagementPhase is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementPhase couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-phases")
    @ApiOperation(value = "Update existing Engagement Phase")
    public CustomApiResponse updateEngagementPhase(@Valid @RequestBody EngagementPhase engagementPhase) throws URISyntaxException {
        log.debug("REST request to update EngagementPhase : {}", engagementPhase);
        if (engagementPhase.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Phase does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        EngagementPhase result = engagementPhaseService.save(engagementPhase);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-phases} : get all the engagementPhases.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementPhases in body.
     */
    @GetMapping("/engagement-phases")
    @ApiOperation(value = "Get all Engagement Phases; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementPhases(EngagementPhaseCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Phases by criteria: {}", criteria);
        Page<EngagementPhase> page = engagementPhaseQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-phases/count} : count all the engagementPhases.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-phases/count")
    @ApiOperation(value = "Count all Engagement Phases; Optional filters can be applied")
    public CustomApiResponse countEngagementPhases(EngagementPhaseCriteria criteria) {
        log.debug("REST request to count EngagementPhases by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementPhaseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-phases/:id} : get the "id" engagementPhase.
     *
     * @param id the id of the engagementPhase to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementPhase, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-phases/{id}")
    @ApiOperation("Get a single Engagement Phase by ID")
    public CustomApiResponse getEngagementPhase(@PathVariable Long id) {
        log.debug("REST request to get Engagement Phase : {}", id);
        
        if (!engagementPhaseService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Phase with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementPhase> engagementPhase = engagementPhaseService.findOne(id);
        return CustomApiResponse.ok(engagementPhase);
    }

    /**
     * {@code DELETE  /engagement-phases/:id} : delete the "id" engagementPhase.
     *
     * @param id the id of the engagementPhase to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-phases/{id}")
    @ApiOperation("Delete  a single Engagement Phase by ID")
    public CustomApiResponse deleteEngagementPhase(@PathVariable Long id) {
        log.debug("REST request to delete EngagementPhase : {}", id);
        
        if (!engagementPhaseService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Phase with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementPhaseService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
