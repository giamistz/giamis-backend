package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementObjectiveRisk;
import gov.giamis.service.dto.EngagementObjectiveRiskCriteria;
import gov.giamis.service.engagement.EngagementObjectiveRiskQueryService;
import gov.giamis.service.engagement.EngagementObjectiveRiskService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementObjectiveRisk}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Objective Risk Resource", description = "Operations to manage Engagement Objective Risk")
public class EngagementObjectiveRiskResource {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveRiskResource.class);

    private static final String ENTITY_NAME = "engagementObjectiveRisk";

    private final EngagementObjectiveRiskService engagementObjectiveRiskService;

    private final EngagementObjectiveRiskQueryService engagementObjectiveRiskQueryService;

    public EngagementObjectiveRiskResource(
        EngagementObjectiveRiskService engagementObjectiveRiskService,
        EngagementObjectiveRiskQueryService engagementObjectiveRiskQueryService) {

        this.engagementObjectiveRiskService = engagementObjectiveRiskService;
        this.engagementObjectiveRiskQueryService = engagementObjectiveRiskQueryService;
    }

    /**
     * {@code POST  /engagement-objective-risks} : Create a new engagementObjectiveRisk.
     *
     * @param engagementObjectiveRisk the engagementObjectiveRisk to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementObjectiveRisk, or with status {@code 400 (Bad Request)} if the engagementObjectiveRisk has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-objective-risks")
    @ApiOperation(value = "Create Engagement Objective Risk")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementObjectiveRisk(@Valid @RequestBody EngagementObjectiveRisk engagementObjectiveRisk) throws URISyntaxException {
        log.debug("REST request to save Engagement Objective Risk : {}", engagementObjectiveRisk);
        if (engagementObjectiveRisk.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Objective Risk already Exists", ENTITY_NAME, "id exists");
        }

        EngagementObjectiveRisk result = engagementObjectiveRiskService.save(engagementObjectiveRisk);
        
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-objective-risks} : Updates an existing engagementObjectiveRisk.
     *
     * @param engagementObjectiveRisk the engagementObjectiveRisk to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementObjectiveRisk,
     * or with status {@code 400 (Bad Request)} if the engagementObjectiveRisk is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementObjectiveRisk couldn't be updated.
     */
    @PutMapping("/engagement-objective-risks")
    @ApiOperation(value = "Update existing Engagement Objective Risk")
    public CustomApiResponse updateEngagementObjectiveRisk(@Valid @RequestBody EngagementObjectiveRisk engagementObjectiveRisk) {
        log.debug("REST request to update Engagement Objective Risk : {}", engagementObjectiveRisk);
        if (engagementObjectiveRisk.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Objective Risk does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        EngagementObjectiveRisk result = engagementObjectiveRiskService.save(engagementObjectiveRisk);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }


    @GetMapping("/engagement-objective-risks/{engagementId}")
    @ApiOperation(value = "Get all Engagement Objective Risks; with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementObjectiveRisks(@PathVariable Long engagementId) {
        log.debug("REST request to get Engagement Objective Risks by eng: {}", engagementId);
        List<EngagementObjectiveRisk> all = engagementObjectiveRiskQueryService.findByEngagement(engagementId);
        return CustomApiResponse.ok(all);
    }


    @GetMapping("/engagement-objective-risks/with-controls/{engagementId}")
    @ApiOperation(value = "Get all Engagement Objective Risks; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllWithControls(@PathVariable Long engagementId) {
        log.debug("REST request to get Engagement Objective Risks by eng: {}", engagementId);
        List<EngagementObjectiveRisk> all = engagementObjectiveRiskQueryService.findAllWithControlByEngagement(engagementId);
        return CustomApiResponse.ok(all);
    }

    /**
     * {@code GET  /engagement-objective-risks/:id} : get the "id" engagementObjectiveRisk.
     *
     * @param id the id of the engagementObjectiveRisk to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementObjectiveRisk, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-objective-risks/{id}")
    @ApiOperation("Get a single Engagement Objective Risk by ID")
    public CustomApiResponse getEngagementObjectiveRisk(@PathVariable Long id) {
        log.debug("REST request to get Engagement Objective Risk : {}", id);
        
        if (!engagementObjectiveRiskService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Objective Risk with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementObjectiveRisk> engagementObjectiveRisk = engagementObjectiveRiskService.findOne(id);
        return CustomApiResponse.ok(engagementObjectiveRisk);
    }
    /**
     * {@code DELETE  /engagement-objective-risks/:id} : delete the "id" engagementObjectiveRisk.
     *
     * @param id the id of the engagementObjectiveRisk to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-objective-risks/{id}")
    @ApiOperation("Delete  a single Engagement Objective Risk by ID")
    public CustomApiResponse deleteEngagementObjectiveRisk(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Objective Risk : {}", id);
        
        if (!engagementObjectiveRiskService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Objective Risk with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementObjectiveRiskService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
