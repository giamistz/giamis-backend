package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.MeetingDeliverable;
import gov.giamis.service.engagement.MeetingDeliverableQueryService;
import gov.giamis.service.engagement.MeetingDeliverableService;
import gov.giamis.service.engagement.dto.MeetingDeliverableCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link MeetingDeliverable}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Meeting Deliverable Resource", description = "Operations to manage Meeting Deliverables")
public class MeetingDeliverableResource {

    private final Logger log = LoggerFactory.getLogger(MeetingDeliverableResource.class);

    private static final String ENTITY_NAME = "meetingDeliverable";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetingDeliverableService meetingDeliverableService;

    private final MeetingDeliverableQueryService meetingDeliverableQueryService;

    public MeetingDeliverableResource(MeetingDeliverableService meetingDeliverableService, MeetingDeliverableQueryService meetingDeliverableQueryService) {
        this.meetingDeliverableService = meetingDeliverableService;
        this.meetingDeliverableQueryService = meetingDeliverableQueryService;
    }

    /**
     * {@code POST  /meeting-deliverables} : Create a new meetingDeliverable.
     *
     * @param meetingDeliverable the meetingDeliverable to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meetingDeliverable, or with status {@code 400 (Bad Request)} if the meetingDeliverable has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meeting-deliverables")
    @ApiOperation(value = "Create Meeting Deliverable")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createMeetingDeliverable(@Valid @RequestBody MeetingDeliverable meetingDeliverable) throws URISyntaxException {
        log.debug("REST request to save Meeting Deliverable : {}", meetingDeliverable);
        if (meetingDeliverable.getId() != null) {
            throw new BadRequestAlertException("A new meeting Deliverable already have an ID", ENTITY_NAME, "id exists");
        }
        MeetingDeliverable result = meetingDeliverableService.save(meetingDeliverable);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /meeting-deliverables} : Updates an existing meetingDeliverable.
     *
     * @param meetingDeliverable the meetingDeliverable to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetingDeliverable,
     * or with status {@code 400 (Bad Request)} if the meetingDeliverable is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meetingDeliverable couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meeting-deliverables")
    @ApiOperation(value = "Update existing  Meeting Deliverable")
    public CustomApiResponse updateMeetingDeliverable(@Valid @RequestBody MeetingDeliverable meetingDeliverable) throws URISyntaxException {
        log.debug("REST request to update Meeting Deliverable : {}", meetingDeliverable);

        if (meetingDeliverable.getId() == null) {
			throw new BadRequestAlertException("Updated Meeting Deliverable does not have an ID", ENTITY_NAME,
					"id not exists");
		}
		if (!meetingDeliverableService.findOne(meetingDeliverable.getId()).isPresent()) {
			throw new BadRequestAlertException("Meeting Deliverable with ID: " + meetingDeliverable.getId() + " not found", ENTITY_NAME,
					"id not exists");
		}
		
        MeetingDeliverable result = meetingDeliverableService.save(meetingDeliverable);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /meeting-deliverables} : get all the meetingDeliverables.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meetingDeliverables in body.
     */
    @GetMapping("/meeting-deliverables")
    @ApiOperation(value = "Get all MeetingDeliverables; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllMeetingDeliverables(MeetingDeliverableCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MeetingDeliverables by criteria: {}", criteria);
        Page<MeetingDeliverable> page = meetingDeliverableQueryService.findByCriteria(criteria, pageable);
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /meeting-deliverables/count} : count all the meetingDeliverables.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/meeting-deliverables/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countMeetingDeliverables(MeetingDeliverableCriteria criteria) {
        log.debug("REST request to count MeetingDeliverables by criteria: {}", criteria);
        return CustomApiResponse.ok(meetingDeliverableQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /meeting-deliverables/:id} : get the "id" meetingDeliverable.
     *
     * @param id the id of the meetingDeliverable to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meetingDeliverable, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meeting-deliverables/{id}")
    @ApiOperation("Get a single MeetingDeliverable by ID")
    public CustomApiResponse getMeetingDeliverable(@PathVariable Long id) {
        log.debug("REST request to get MeetingDeliverable : {}", id);
        
        if (!meetingDeliverableService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Meeting Deliverable with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<MeetingDeliverable> meetingDeliverable = meetingDeliverableService.findOne(id);
        return CustomApiResponse.ok(meetingDeliverable);
    }

    /**
     * {@code DELETE  /meeting-deliverables/:id} : delete the "id" meetingDeliverable.
     *
     * @param id the id of the meetingDeliverable to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meeting-deliverables/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteMeetingDeliverable(@PathVariable Long id) {
        log.debug("REST request to delete MeetingDeliverable : {}", id);
        
        if (!meetingDeliverableService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Meeting Deliverable with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        meetingDeliverableService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
