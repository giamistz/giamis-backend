package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.enumeration.LetterType;
import gov.giamis.service.engagement.CommunicationLetterService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.CommunicationLetterCriteria;
import gov.giamis.service.engagement.CommunicationLetterQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link CommunicationLetter}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class CommunicationLetterResource {

    private final Logger log = LoggerFactory.getLogger(CommunicationLetterResource.class);

    private static final String ENTITY_NAME = "communicationLetter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommunicationLetterService communicationLetterService;

    private final CommunicationLetterQueryService communicationLetterQueryService;

    public CommunicationLetterResource(CommunicationLetterService communicationLetterService,
                                       CommunicationLetterQueryService communicationLetterQueryService) {
        this.communicationLetterService = communicationLetterService;
        this.communicationLetterQueryService = communicationLetterQueryService;
    }


    @GetMapping("/communication-letters/{type}/{engagementId}")
    public CustomApiResponse getByType(@PathVariable LetterType type, @PathVariable Long engagementId) {
        return CustomApiResponse.ok(communicationLetterService.findByTypeAndEngagement(type, engagementId));
    }

    /**
     * {@code PUT  /communication-letters} : Updates an existing communicationLetter.
     *
     * @param communicationLetter the communicationLetter to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated communicationLetter,
     * or with status {@code 400 (Bad Request)} if the communicationLetter is not valid,
     * or with status {@code 500 (Internal Server Error)} if the communicationLetter couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/communication-letters")
    public CustomApiResponse updateCommunicationLetter(@Valid @RequestBody CommunicationLetter communicationLetter) throws URISyntaxException {
        log.debug("REST request to update CommunicationLetter : {}", communicationLetter);
        if (communicationLetter.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommunicationLetter result = communicationLetterService.save(communicationLetter);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /communication-letters} : get all the communicationLetters.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of communicationLetters in body.
     */
    @GetMapping("/communication-letters")
    public CustomApiResponse getAllCommunicationLetters(CommunicationLetterCriteria criteria) {
        log.debug("REST request to get CommunicationLetters by criteria: {}", criteria);
        List<CommunicationLetter> entityList = communicationLetterQueryService.findByCriteria(criteria);
        return CustomApiResponse.ok(entityList);
    }


}
