package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.SpecificObjective;
import gov.giamis.service.engagement.SpecificObjectiveQueryService;
import gov.giamis.service.engagement.SpecificObjectiveService;
import gov.giamis.service.engagement.dto.SpecificObjectiveCriteria;
import gov.giamis.service.engagement.dto.SpecificObjectiveDTO;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link SpecificObjective}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Specific Objective Resource", description = "Operations to manage Specific Objectives")
public class SpecificObjectiveResource {

    private final Logger log = LoggerFactory.getLogger(SpecificObjectiveResource.class);

    private static final String ENTITY_NAME = "specificObjective";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpecificObjectiveService specificObjectiveService;

    private  final SpecificObjectiveService specificObjectiveByEngagementService;

    private final SpecificObjectiveQueryService specificObjectiveQueryService;

    private SpecificObjectiveDTO specificObjectiveDTO = null;

    public SpecificObjectiveResource(SpecificObjectiveService specificObjectiveService,
    		SpecificObjectiveQueryService specificObjectiveQueryService) {
        this.specificObjectiveService = specificObjectiveService;
        this.specificObjectiveQueryService = specificObjectiveQueryService;
        this.specificObjectiveByEngagementService = specificObjectiveService;
    }

    /**
     * {@code POST  /specific-objectives} : Create a new specificObjective.
     *
     * @param specificObjective the specificObjective to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new specificObjective, or with status {@code 400 (Bad Request)} if the specificObjective has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/specific-objectives")
    @ApiOperation(value = "Create Specific Objective")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createSpecificObjective(@Valid @RequestBody SpecificObjective specificObjective) throws URISyntaxException {
        log.debug("REST request to save Specific Objective : {}", specificObjective);
        if (specificObjective.getId() != null) {
            throw new BadRequestAlertException("A new Specific Objective already have an ID", ENTITY_NAME, "id exists");
        }

        SpecificObjective result = specificObjectiveService.save(specificObjective);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    @PostMapping("/specific-objectives-editable")
    @ApiOperation(value = "Create Specific Objective")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEditableSpecificObjective(@Valid @RequestBody SpecificObjective specificObjective) throws URISyntaxException {
        log.debug("REST request to save Specific Objective : {}", specificObjective);
        if (specificObjective.getId() != null) {
            throw new BadRequestAlertException("A new Specific Objective already have an ID", ENTITY_NAME, "id exists");
        }
        specificObjective.setIsRefined(true);
        specificObjective.setRefinedDescription(specificObjective.getRefinedDescription());
        specificObjective.setDescription(null);
        SpecificObjective result = specificObjectiveService.save(specificObjective);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /specific-objectives} : Updates an existing specificObjective.
     *
     * @param specificObjective the specificObjective to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated specificObjective,
     * or with status {@code 400 (Bad Request)} if the specificObjective is not valid,
     * or with status {@code 500 (Internal Server Error)} if the specificObjective couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/specific-objectives")
    @ApiOperation(value = "Update existing  Specific Objective")
    public CustomApiResponse updateSpecificObjective(@Valid @RequestBody SpecificObjective specificObjective) throws URISyntaxException {
        log.debug("REST request to update Specific Objective : {}", specificObjective);

        if (specificObjective.getId() == null) {
            throw new BadRequestAlertException("Updated Specific Objective does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (!specificObjectiveService.findOne(specificObjective.getId()).isPresent()) {
            throw new BadRequestAlertException("Specific Objective with ID: " + specificObjective.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }

        SpecificObjective result = specificObjectiveService.save(specificObjective);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    @PutMapping("/specific-objectives/refine")
    @ApiOperation(value = "Refine  Specific Objective")
    public CustomApiResponse updateSpecificObjectiveEditable(
        @Valid @RequestBody SpecificObjective specificObjective) throws URISyntaxException {

        specificObjectiveService.refine(specificObjective);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }


    /**
     * {@code GET  /specific-objectives} : get all the specificObjectives.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of specificObjectives in body.
     */
    @GetMapping("/specific-objectives")
    @ApiOperation(value = "Get all SpecificObjectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllSpecificObjectives(SpecificObjectiveCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SpecificObjectives by criteria: {}", criteria);
        Page<SpecificObjective> page = specificObjectiveQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    @GetMapping("/specific-objectives/by-engagement/{engagementId}")
    @ApiOperation(value = "Get all SpecificObjectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse byEngagement( @PathVariable Long engagementId, Pageable pageable) {
        log.debug("REST request to get SpecificObjectives by enga: {}", engagementId);
        Page<SpecificObjective> page = specificObjectiveQueryService.findByEngagement(engagementId, pageable);
        return CustomApiResponse.ok(page);
    }

    @GetMapping("/specific-objectives/with-risks/{engagementId}")
    @ApiOperation(value = "Get all SpecificObjectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllWithRisk(@PathVariable Long engagementId) {

        List<SpecificObjective> page = specificObjectiveQueryService.findByEngagementWithRisk(engagementId);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /specific-objectives/count} : count all the specificObjectives.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/specific-objectives/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countSpecificObjectives(SpecificObjectiveCriteria criteria) {
        log.debug("REST request to count SpecificObjectives by criteria: {}", criteria);
        return CustomApiResponse.ok(specificObjectiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /specific-objectives/:id} : get the "id" specificObjective.
     *
     * @param id the id of the specificObjective to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the specificObjective, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/specific-objectives/{id}")
    @ApiOperation("Get a single SpecificObjective by ID")
    public CustomApiResponse getSpecificObjective(@PathVariable Long id) {
        log.debug("REST request to get SpecificObjective : {}", id);

        if (!specificObjectiveService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Specific Objective with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<SpecificObjective> specificObjective = specificObjectiveService.findOne(id);
        return CustomApiResponse.ok(specificObjective);
    }

    @GetMapping("/specific-objectives/refined")
    @ApiOperation("Get Specific Objectives by Is Refined True")
    public CustomApiResponse getSpecificObjectiveByTrueIsRefined() {
        log.debug("REST request to get SpecificObjective By Is Refined True");

        List<SpecificObjective> specificObjective = specificObjectiveService.findByIsRefinedTrue();
        return CustomApiResponse.ok(specificObjective);
    }
    /**
     * {@code DELETE  /specific-objectives/:id} : delete the "id" specificObjective.
     *
     * @param id the id of the specificObjective to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/specific-objectives/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteSpecificObjective(@PathVariable Long id) {
        log.debug("REST request to delete SpecificObjective : {}", id);

        if (!specificObjectiveService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Specific Objective with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        specificObjectiveService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

//    @GetMapping("/specific-objectives-editable")
//    @ApiOperation(value = "Get all SpecificObjectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
//    public CustomApiResponse getAllSpecificObjectivesEditable(SpecificObjective specObjective) {
//        log.debug("REST request to get SpecificObjectives by criteria: {}");
//        List<SpecificObjective> specificObjectiveList = specificObjectiveService.findAllSpecificObjective();
//        List<SpecificObjectiveDTO> specificObjectiveDTOList = new ArrayList<SpecificObjectiveDTO>();
//        for(SpecificObjective specificObjective:specificObjectiveList) {
//        	specificObjectiveDTO = new SpecificObjectiveDTO();
//        	specificObjectiveDTO.setId(specificObjective.getId());
//
//        	if(specificObjective.getIsRefined() == true) {
//        		specificObjectiveDTO.setName(specificObjective.getRefinedDescription());
//        	}else {
//        		specificObjectiveDTO.setName(specificObjective.getDescription());
//        	}
//        	specificObjectiveDTO.setIsRefined(specificObjective.getIsRefined());
//        	specificObjectiveDTOList.add(specificObjectiveDTO);
//        }
//
//        return CustomApiResponse.ok(specificObjectiveDTOList);
//    }

//    @GetMapping("/specific-objectives-editable/{id}")
//    @ApiOperation("Get a single Specific Objective by ID")
//    public CustomApiResponse getSpecificObjectiveEditable(@PathVariable Long id) {
//        log.debug("REST request to get SpecificObjective by ID : {}", id);
//
//        if (!specificObjectiveService.findOne(id).isPresent()) {
//            throw new BadRequestAlertException("Specific Objective with ID: " + id + " not found", ENTITY_NAME,
//                    "id not exists");
//        }
//        Optional<SpecificObjective> specificObjective = specificObjectiveService.findOne(id);
//        specificObjectiveDTO = new SpecificObjectiveDTO();
//        specificObjectiveDTO.setId(specificObjective.get().getId());
//        specificObjectiveDTO.setName(specificObjective.get().getDescription());
//        specificObjectiveDTO.setIsRefined(specificObjective.get().getIsRefined());
//        return CustomApiResponse.ok(specificObjectiveDTO);
//    }

    /**
     * Get specific objective by Engagement ID
     */
    @GetMapping("/specific-objectives-by-engagement/{id}")
    @ApiOperation("Get a single SpecificObjective by ID")
    public CustomApiResponse getSpecificObjectiveByEngagement(@PathVariable Long id) {
        log.debug("REST request to get SpecificObjectiveByEngagement : {}", id);

        if (specificObjectiveByEngagementService.findByEngagementId(id) == null) {
            throw new BadRequestAlertException("Specific Objective with Engagement ID: " + id + " not found", ENTITY_NAME,
                "id not exists");
        }
        List<SpecificObjective> specificObjective = specificObjectiveByEngagementService.findByEngagementId(id);
        return CustomApiResponse.ok(specificObjective);
    }
}
