package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementItem;
import gov.giamis.service.engagement.EngagementItemQueryService;
import gov.giamis.service.engagement.EngagementItemService;
import gov.giamis.service.engagement.EngagementProcedureService;
import gov.giamis.service.engagement.dto.EngagementItemCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementItem}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Item Resource", description = "Operations to manage Engagement Item")
public class EngagementItemResource {

    private final Logger log = LoggerFactory.getLogger(EngagementItemResource.class);

    private static final String ENTITY_NAME = "engagementItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementItemService engagementItemService;

    private final EngagementItemQueryService engagementItemQueryService;

    private final EngagementProcedureService engagementProcedureService;

    public EngagementItemResource(EngagementItemService engagementItemService,
    		EngagementItemQueryService engagementItemQueryService,
    		EngagementProcedureService engagementProcedureService) {
        this.engagementItemService = engagementItemService;
        this.engagementItemQueryService = engagementItemQueryService;
        this.engagementProcedureService = engagementProcedureService;
    }

    /**
     * {@code POST  /engagement-items} : Create a new engagementItem.
     *
     * @param engagementItem the engagementItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementItem, or with status {@code 400 (Bad Request)} if the engagementItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-items")
    @ApiOperation(value = "Create Engagement Item")
    @ResponseStatus(HttpStatus.CREATED)
	@Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementItem(@Valid @RequestBody EngagementItem engagementItem) throws URISyntaxException {
        log.debug("REST request to save Engagement Item : {}", engagementItem);
        if (engagementItem.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Item  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementItem result = engagementItemService.save(engagementItem);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-items} : Updates an existing engagementItem.
     *
     * @param engagementItem the engagementItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementItem,
     * or with status {@code 400 (Bad Request)} if the engagementItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-items")
    @ApiOperation(value = "Update existing Engagement Item")
    public CustomApiResponse updateEngagementItem(@Valid @RequestBody EngagementItem engagementItem) throws URISyntaxException {
        log.debug("REST request to update EngagementItem : {}", engagementItem);
        if (engagementItem.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Item does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        EngagementItem result = engagementItemService.save(engagementItem);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-items} : get all the engagementItems.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementItems in body.
     */
    @GetMapping("/engagement-items")
    @ApiOperation(value = "Get all Engagement Items; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementItems(EngagementItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Items by criteria: {}", criteria);
        Page<EngagementItem> page = engagementItemQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-items/count} : count all the engagementItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-items/count")
    @ApiOperation(value = "Count all Engagement Items; Optional filters can be applied")
    public CustomApiResponse countEngagementItems(EngagementItemCriteria criteria) {
        log.debug("REST request to count EngagementItems by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-items/:id} : get the "id" engagementItem.
     *
     * @param id the id of the engagementItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-items/{id}")
    @ApiOperation("Get a single Engagement Item by ID")
    public CustomApiResponse getEngagementItem(@PathVariable Long id) {
        log.debug("REST request to get Engagement Item : {}", id);

        if (!engagementItemService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Item with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementItem> engagementItem = engagementItemService.findOne(id);
        return CustomApiResponse.ok(engagementItem);
    }

    /**
     * {@code DELETE  /engagement-items/:id} : delete the "id" engagementItem.
     *
     * @param id the id of the engagementItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-items/{id}")
    @ApiOperation("Delete  a single Engagement Item by ID")
    public CustomApiResponse deleteEngagementItem(@PathVariable Long id) {
        log.debug("REST request to delete EngagementItem : {}", id);

        if (!engagementItemService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Item with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementItemService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-items/byProcedure")
    @ApiOperation("Get  Engagement Item by engagement Procedure ID")
    public CustomApiResponse getEngagementItemByEngagementProcedureId(@RequestParam Long id) {
        log.debug("REST request to get Engagement Item by Engagement Procedure ID: {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<EngagementItem> engagementItem = engagementItemService.findByEngagementProcedure(id);
        return CustomApiResponse.ok(engagementItem);
    }
}
