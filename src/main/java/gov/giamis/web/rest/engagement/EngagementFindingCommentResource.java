package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import gov.giamis.service.engagement.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementFindingComment;
import gov.giamis.service.engagement.dto.EngagementFindingCommentCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementFindingComment}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Finding Comment Resource", description = "Operations to manage Engagement Finding Comments")
public class EngagementFindingCommentResource {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingCommentResource.class);

    private static final String ENTITY_NAME = "engagementFindingComment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementFindingCommentService engagementFindingCommentService;

    private final EngagementService engagementService;

    private final EngagementFindingCommentQueryService engagementFindingCommentQueryService;

    public EngagementFindingCommentResource(
            EngagementFindingCommentService engagementFindingCommentService,
            EngagementFindingCommentQueryService engagementFindingCommentQueryService,
            EngagementService engagementService) {
        this.engagementFindingCommentService = engagementFindingCommentService;
        this.engagementFindingCommentQueryService = engagementFindingCommentQueryService;
        this.engagementService = engagementService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementFindingComment.
     *
     * @param engagementFindingComment the engagementFindingComment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementFindingComment, or with status {@code 400 (Bad Request)} if the engagementFindingComment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-finding-comments")
    @ApiOperation(value = "Create Engagement Finding Comment")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementFindingComment(@Valid @RequestBody EngagementFindingComment engagementFindingComment) throws URISyntaxException {
        log.debug("REST request to save Engagement Finding Comment : {}", engagementFindingComment);
        EngagementFindingComment result = null;
        if (engagementFindingComment.getId() != null) {
            throw new BadRequestAlertException("A new engagementFindingComment  already have an ID", ENTITY_NAME, "id exists");
        }

        result = engagementFindingCommentService.save(engagementFindingComment);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementFindingComment.
     *
     * @param engagementFindingComment the engagementFindingComment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementFindingComment,
     * or with status {@code 400 (Bad Request)} if the engagementFindingComment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementFindingComment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-finding-comments")
    @ApiOperation(value = "Update existing Engagement Finding Comment")
    public CustomApiResponse updateEngagementFindingComment(@Valid @RequestBody EngagementFindingComment engagementFindingComment) throws URISyntaxException {
        log.debug("REST request to update Engagement Finding Comment : {}", engagementFindingComment);
        if (engagementFindingComment.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Finding Comment does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        EngagementFindingComment result = engagementFindingCommentService.save(engagementFindingComment);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementFindingComments.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementFindingComments in body.
     */
    @GetMapping("/engagement-finding-comments")
    @ApiOperation(value = "Get all Engagement Finding Comments; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementFindingComments(EngagementFindingCommentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementFindingComments by criteria: {}", criteria);
        Page<EngagementFindingComment> page = engagementFindingCommentQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementFindingComments.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-finding-comments/count")
    @ApiOperation(value = "Count all Engagement Finding Comment; Optional filters can be applied")
    public CustomApiResponse countEngagementFindingComments(EngagementFindingCommentCriteria criteria) {
        log.debug("REST request to count Engagement Finding Comments by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementFindingCommentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementFindingComment.
     *
     * @param id the id of the engagementFindingComment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementFindingComment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-finding-comments/{id}")
    @ApiOperation("Get a single Engagement Finding Comment by ID")
    public CustomApiResponse getEngagementFindingComment(@PathVariable Long id) {
        log.debug("REST request to get Engagement Finding Comment : {}", id);

        if (!engagementFindingCommentService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding Comment with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementFindingComment> engagementFindingComment = engagementFindingCommentService.findOne(id);
        return CustomApiResponse.ok(engagementFindingComment);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementFindingComment.
     *
     * @param id the id of the engagementFindingComment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-finding-comments/{id}")
    @ApiOperation("Delete  a single Engagement Finding Comment by ID")
    public CustomApiResponse deleteEngagementFindingComment(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Finding Comment : {}", id);

        if (!engagementFindingCommentService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding Comment with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementFindingCommentService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }


}
