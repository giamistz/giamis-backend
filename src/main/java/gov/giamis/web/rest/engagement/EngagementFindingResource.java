package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import gov.giamis.service.engagement.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementFinding;
import gov.giamis.service.engagement.dto.EngagementFindingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementFinding}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Finding Resource", description = "Operations to manage Engagement Findings")
public class EngagementFindingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingResource.class);

    private static final String ENTITY_NAME = "engagementFinding";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementFindingService engagementFindingService;

    private final EngagementProcedureService engagementProcedureService;

    private final EngagementService engagementService;

    private final EngagementFindingQueryService engagementFindingQueryService;

    public EngagementFindingResource(
            EngagementFindingService engagementFindingService,
            EngagementFindingQueryService engagementFindingQueryService,
            EngagementProcedureService engagementProcedureService,
            EngagementService engagementService) {
        this.engagementFindingService = engagementFindingService;
        this.engagementFindingQueryService = engagementFindingQueryService;
        this.engagementProcedureService = engagementProcedureService;
        this.engagementService = engagementService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementFinding.
     *
     * @param engagementFinding the engagementFinding to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementFinding, or with status {@code 400 (Bad Request)} if the engagementFinding has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-findings")
    @ApiOperation(value = "Create Engagement Finding")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementFinding(@Valid @RequestBody EngagementFinding engagementFinding) throws URISyntaxException {
        log.debug("REST request to save Engagement Finding : {}", engagementFinding);
        EngagementFinding result = null;
        if (engagementFinding.getId() != null) {
            throw new BadRequestAlertException("A new engagementFinding  already have an ID", ENTITY_NAME, "id exists");
        }

        result = engagementFindingService.save(engagementFinding);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementFinding.
     *
     * @param engagementFinding the engagementFinding to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementFinding,
     * or with status {@code 400 (Bad Request)} if the engagementFinding is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementFinding couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-findings")
    @ApiOperation(value = "Update existing Engagement Finding")
    public CustomApiResponse updateEngagementFinding(@Valid @RequestBody EngagementFinding engagementFinding) throws URISyntaxException {
        log.debug("REST request to update Engagement Finding : {}", engagementFinding);
        if (engagementFinding.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Finding does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        EngagementFinding result = engagementFindingService.save(engagementFinding);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementFindings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementFindings in body.
     */
    @GetMapping("/engagement-findings")
    @ApiOperation(value = "Get all Engagement Findings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementFindings(EngagementFindingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementFindings by criteria: {}", criteria);
        Page<EngagementFinding> page = engagementFindingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementFindings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-findings/count")
    @ApiOperation(value = "Count all Engagement Finding; Optional filters can be applied")
    public CustomApiResponse countEngagementFindings(EngagementFindingCriteria criteria) {
        log.debug("REST request to count Engagement Findings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementFindingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementFinding.
     *
     * @param id the id of the engagementFinding to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementFinding, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-findings/{id}")
    @ApiOperation("Get a single Engagement Finding by ID")
    public CustomApiResponse getEngagementFinding(@PathVariable Long id) {
        log.debug("REST request to get Engagement Finding : {}", id);

        if (!engagementFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementFinding> engagementFinding = engagementFindingService.findOne(id);
        return CustomApiResponse.ok(engagementFinding);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementFinding.
     *
     * @param id the id of the engagementFinding to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-findings/{id}")
    @ApiOperation("Delete  a single Engagement Finding by ID")
    public CustomApiResponse deleteEngagementFinding(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Finding : {}", id);

        if (!engagementFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementFindingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-findings/byProcedure")
    @ApiOperation("Get  Engagement Finding by engagement Procedure ID")
    public CustomApiResponse getEngagementFindingByEngagementProcedure(@RequestParam Long id) {
        log.debug("Get Engagement Finding by engagement Procedure ID: {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<EngagementFinding> engagementFinding = engagementFindingService.findByEngagementProcedureId(id);
        return CustomApiResponse.ok(engagementFinding);
    }

    @GetMapping("/engagement-findings/byEngagement/{id}")
    @ApiOperation("Get  Engagement Finding by engagement ID")
    public CustomApiResponse getEngagementFindingByEngagementId(@PathVariable Long id) {
        log.debug("Get Engagement Finding by engagement ID: {}", id);

        if (!engagementService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagement with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}

        List<EngagementFinding> engagementFinding = engagementFindingService.findEngagementFindingByEngagementId(id);
        return CustomApiResponse.ok(engagementFinding);
    }

    @GetMapping("/engagement-findings/withFnYearAdnOrgUnit/{financialYearId}/{organisationUnitId}")
    @ApiOperation("Get  Engagement Finding by engagement ID")
    public CustomApiResponse engagementFindingWithFnYearAndOrgUnit(@PathVariable Long financialYearId, @PathVariable Long organisationUnitId) {
        log.debug("Get Engagement Finding by engagement ID: {}, {}", financialYearId, organisationUnitId);

        List<EngagementFinding> engagementFinding = engagementFindingService.findByFinancialYear_IdAndOrganisationUnit_Id(financialYearId, organisationUnitId);
        return CustomApiResponse.ok(engagementFinding);
    }

    @GetMapping("/engagement-findings/organisationUnit/{id}")
    @ApiOperation("Get Engagement Finding by organisation unit")
    public CustomApiResponse engagementFindingsByOrganisationUnit(@PathVariable Long id) {
        log.debug("Get Engagement Finding by organisation unit ID: {}", id);

        List<EngagementFinding> engagementFinding = engagementFindingService.findByOrganisationUnitId(id);
        return CustomApiResponse.ok(engagementFinding);
    }

    @GetMapping("/engagement-findings/previous/{engagementId}")
    @ApiOperation("Get engagement finding group by status")
    public CustomApiResponse getPrevious(@PathVariable Long engagementId) {

        return CustomApiResponse.ok(engagementFindingService
            .findPreviousGroupByStatus(engagementId));
    }

    /**
     * Count all by engagement status
     * @param engagementId = engagement id
     * @return
     */
    @GetMapping("/engagement-findings/countBy/{engagementId}")
    public CustomApiResponse countFulImplementedStatus(@PathVariable Long engagementId) {
        return CustomApiResponse.ok(engagementFindingService
        .countByEngagementFindingStatus(engagementId));
    }
}
