package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngFindingRecommendation;
import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import gov.giamis.service.engagement.EngFindingRecommendationCommentService;
import gov.giamis.service.engagement.EngFindingRecommendationService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.EngFindingReccomendCommentCriteria;
import gov.giamis.service.engagement.EngFindingRecommendationCommentQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link EngFindingRecommendationComment}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class EngFindingRecommendationCommentResource {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationCommentResource.class);

    private static final String ENTITY_NAME = "engFindingReccomendComment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngFindingRecommendationCommentService engFindingRecommendationCommentService;

    private final EngFindingRecommendationCommentQueryService engFindingRecommendationCommentQueryService;

    private final EngFindingRecommendationService engFindingRecommendationService;

    public EngFindingRecommendationCommentResource(EngFindingRecommendationCommentService engFindingRecommendationCommentService,
                                                   EngFindingRecommendationCommentQueryService engFindingRecommendationCommentQueryService,
    EngFindingRecommendationService engFindingRecommendationService) {
        this.engFindingRecommendationCommentService = engFindingRecommendationCommentService;
        this.engFindingRecommendationCommentQueryService = engFindingRecommendationCommentQueryService;
        this.engFindingRecommendationService = engFindingRecommendationService;
    }

    /**
     * {@code POST  /eng-finding-recommendation-comments} : Create a new engFindingReccomendComment.
     *
     * @param engFindingRecommendationComment the engFindingReccomendComment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engFindingReccomendComment, or with status {@code 400 (Bad Request)} if the engFindingReccomendComment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/eng-finding-recommendation-comments")
    @ApiOperation(value = "Create engagement finding recommendation")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngFindingReccomendComment(@Valid @RequestBody EngFindingRecommendationComment engFindingRecommendationComment) throws URISyntaxException {
        log.debug("REST request to save EngFindingRecommendComment : {}", engFindingRecommendationComment);
        if (engFindingRecommendationComment.getId() != null) {
            throw new BadRequestAlertException("A new engFindingReccomendComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EngFindingRecommendationComment result = engFindingRecommendationCommentService.save(engFindingRecommendationComment);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /eng-finding-recommendation-comments} : Updates an existing engFindingReccomendComment.
     *
     * @param engFindingRecommendationComment the engFindingReccomendComment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engFindingReccomendComment,
     * or with status {@code 400 (Bad Request)} if the engFindingReccomendComment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engFindingReccomendComment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/eng-finding-recommendation-comments")
    public CustomApiResponse updateEngFindingReccomendComment(@Valid @RequestBody EngFindingRecommendationComment engFindingRecommendationComment) throws URISyntaxException {
        log.debug("REST request to update EngFindingReccomendComment : {}", engFindingRecommendationComment);
        if (engFindingRecommendationComment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EngFindingRecommendationComment result = engFindingRecommendationCommentService.save(engFindingRecommendationComment);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /eng-finding-recommendation-comments} : get all the engFindingReccomendComments.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engFindingReccomendComments in body.
     */
    @GetMapping("/eng-finding-recommendation-comments")
    public CustomApiResponse getAllEngFindingReccomendComments(EngFindingReccomendCommentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngFindingReccomendComments by criteria: {}", criteria);
        Page<EngFindingRecommendationComment> page = engFindingRecommendationCommentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /eng-finding-recommendation-comments/count} : count all the engFindingReccomendComments.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/eng-finding-recommendation-comments/count")
    public CustomApiResponse countEngFindingReccomendComments(EngFindingReccomendCommentCriteria criteria) {
        log.debug("REST request to count EngFindingReccomendComments by criteria: {}", criteria);
        return CustomApiResponse.ok(engFindingRecommendationCommentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /eng-finding-recommendation-comments/:id} : get the "id" engFindingReccomendComment.
     *
     * @param id the id of the engFindingReccomendComment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engFindingReccomendComment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/eng-finding-recommendation-comments/{id}")
    public CustomApiResponse getEngFindingReccomendComment(@PathVariable Long id) {
        log.debug("REST request to get EngFindingReccomendComment : {}", id);
        if (!engFindingRecommendationCommentService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding Recommendation Comment with ID: " + id + " not found", ENTITY_NAME,
                "id not exists");
        }
        Optional<EngFindingRecommendationComment> engFindingRecommendComment = engFindingRecommendationCommentService.findOne(id);
        return CustomApiResponse.ok(engFindingRecommendComment);
    }

    /**
     * {@code DELETE  /eng-finding-recommendation-comments/:id} : delete the "id" engFindingReccomendComment.
     *
     * @param id the id of the engFindingReccomendComment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/eng-finding-recommendation-comments/{id}")
    public CustomApiResponse deleteEngFindingReccomendComment(@PathVariable Long id) {
        log.debug("REST request to delete EngFindingRecommendComment : {}", id);
        engFindingRecommendationCommentService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/eng-finding-recommendation-comments/by-recommendation/{id}")
    @ApiOperation("Get  Engagement Finding Comment By Recommendation ID")
    public CustomApiResponse getEngagementFindingByEngagementProcedure(@PathVariable Long id) {
        log.debug("Get Engagement Finding Comment By Recommendation ID: {}", id);

        if (!engFindingRecommendationService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding Recommendation with ID: " + id + " not found", ENTITY_NAME,
                "id not exists");
        }
        List<EngFindingRecommendationComment> engFindingRecommendationComments = engFindingRecommendationCommentService.findByRecommendationId(id);
        return CustomApiResponse.ok(engFindingRecommendationComments);
    }
}
