package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.MeetingAttachment;
import gov.giamis.service.engagement.MeetingAttachmentService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.engagement.dto.MeetingAttachmentCriteria;
import gov.giamis.service.engagement.MeetingAttachmentQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link MeetingAttachment}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Meeting Attachement Resource", description = "Operations to manage meeting attachement")
public class MeetingAttachmentResource {

    private final Logger log = LoggerFactory.getLogger(MeetingAttachmentResource.class);

    private static final String ENTITY_NAME = "meetingAttachment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetingAttachmentService meetingAttachmentService;

    private final MeetingAttachmentQueryService meetingAttachmentQueryService;

    public MeetingAttachmentResource(MeetingAttachmentService meetingAttachmentService, MeetingAttachmentQueryService meetingAttachmentQueryService) {
        this.meetingAttachmentService = meetingAttachmentService;
        this.meetingAttachmentQueryService = meetingAttachmentQueryService;
    }

    /**
     * {@code POST  /meeting-attachments} : Create a new meetingAttachment.
     *
     * @param meetingAttachment the meetingAttachment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meetingAttachment, or with status {@code 400 (Bad Request)} if the meetingAttachment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meeting-attachments")
    @ApiOperation(value = "Create Meeting Attachement")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createMeetingAttachment(@Valid @RequestBody MeetingAttachment meetingAttachment) throws URISyntaxException {
        log.debug("REST request to save MeetingAttachment : {}", meetingAttachment);
        if (meetingAttachment.getId() != null) {
            throw new BadRequestAlertException("A new meetingAttachment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeetingAttachment result = meetingAttachmentService.save(meetingAttachment);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /meeting-attachments} : Updates an existing meetingAttachment.
     *
     * @param meetingAttachment the meetingAttachment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetingAttachment,
     * or with status {@code 400 (Bad Request)} if the meetingAttachment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meetingAttachment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meeting-attachments")
    @ApiOperation(value = "Update existing Meeting Attachement")
    public CustomApiResponse updateMeetingAttachment(@Valid @RequestBody MeetingAttachment meetingAttachment) throws URISyntaxException {
        log.debug("REST request to update MeetingAttachment : {}", meetingAttachment);
        if (meetingAttachment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MeetingAttachment result = meetingAttachmentService.save(meetingAttachment);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /meeting-attachments} : get all the meetingAttachments.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meetingAttachments in body.
     */
    @GetMapping("/meeting-attachments")
    @ApiOperation(value = "Get all Meeting Attachement; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllMeetingAttachments(MeetingAttachmentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MeetingAttachments by criteria: {}", criteria);
        Page<MeetingAttachment> page = meetingAttachmentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /meeting-attachments/count} : count all the meetingAttachments.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/meeting-attachments/count")
    @ApiOperation(value = "Count all Meeting Attachement; Optional filters can be applied")
    public CustomApiResponse countMeetingAttachments(MeetingAttachmentCriteria criteria) {
        log.debug("REST request to count MeetingAttachments by criteria: {}", criteria);
        return CustomApiResponse.ok(meetingAttachmentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /meeting-attachments/:id} : get the "id" meetingAttachment.
     *
     * @param id the id of the meetingAttachment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meetingAttachment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meeting-attachments/{id}")
    @ApiOperation("Get a single Meeting Attachement by ID")
    public CustomApiResponse getMeetingAttachment(@PathVariable Long id) {
        log.debug("REST request to get MeetingAttachment : {}", id);
        Optional<MeetingAttachment> meetingAttachment = meetingAttachmentService.findOne(id);
        return CustomApiResponse.ok(meetingAttachment);
    }

    /**
     * {@code DELETE  /meeting-attachments/:id} : delete the "id" meetingAttachment.
     *
     * @param id the id of the meetingAttachment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meeting-attachments/{id}")
    @ApiOperation("Delete  a single Meeting Attachement ID")
    public CustomApiResponse deleteMeetingAttachment(@PathVariable Long id) {
        log.debug("REST request to delete MeetingAttachment : {}", id);
        meetingAttachmentService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
