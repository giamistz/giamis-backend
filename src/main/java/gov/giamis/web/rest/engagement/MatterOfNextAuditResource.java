package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.MatterOfNextAudit;
import gov.giamis.service.engagement.MatterOfNextAuditService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.engagement.dto.MatterOfNextAuditCriteria;
import gov.giamis.service.engagement.MatterOfNextAuditQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link MatterOfNextAudit}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class MatterOfNextAuditResource {

    private final Logger log = LoggerFactory.getLogger(MatterOfNextAuditResource.class);

    private static final String ENTITY_NAME = "matterOfNextAudit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MatterOfNextAuditService matterOfNextAuditService;

    private final MatterOfNextAuditQueryService matterOfNextAuditQueryService;

    public MatterOfNextAuditResource(MatterOfNextAuditService matterOfNextAuditService, MatterOfNextAuditQueryService matterOfNextAuditQueryService) {
        this.matterOfNextAuditService = matterOfNextAuditService;
        this.matterOfNextAuditQueryService = matterOfNextAuditQueryService;
    }

    /**
     * {@code POST  /matter-of-next-audits} : Create a new matterOfNextAudit.
     *
     * @param matterOfNextAudit the matterOfNextAudit to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new matterOfNextAudit, or with status {@code 400 (Bad Request)} if the matterOfNextAudit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/matter-of-next-audits")
    public CustomApiResponse createMatterOfNextAudit(@Valid @RequestBody MatterOfNextAudit matterOfNextAudit) throws URISyntaxException {
        log.debug("REST request to save MatterOfNextAudit : {}", matterOfNextAudit);
        if (matterOfNextAudit.getId() != null) {
            throw new BadRequestAlertException("A new matterOfNextAudit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MatterOfNextAudit result = matterOfNextAuditService.save(matterOfNextAudit);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /matter-of-next-audits} : Updates an existing matterOfNextAudit.
     *
     * @param matterOfNextAudit the matterOfNextAudit to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated matterOfNextAudit,
     * or with status {@code 400 (Bad Request)} if the matterOfNextAudit is not valid,
     * or with status {@code 500 (Internal Server Error)} if the matterOfNextAudit couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/matter-of-next-audits")
    public CustomApiResponse updateMatterOfNextAudit(@Valid @RequestBody MatterOfNextAudit matterOfNextAudit) throws URISyntaxException {
        log.debug("REST request to update MatterOfNextAudit : {}", matterOfNextAudit);
        if (matterOfNextAudit.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MatterOfNextAudit result = matterOfNextAuditService.save(matterOfNextAudit);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /matter-of-next-audits} : get all the matterOfNextAudits.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of matterOfNextAudits in body.
     */
    @GetMapping("/matter-of-next-audits")
    public CustomApiResponse getAllMatterOfNextAudits(MatterOfNextAuditCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MatterOfNextAudits by criteria: {}", criteria);
        Page<MatterOfNextAudit> page = matterOfNextAuditQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /matter-of-next-audits/count} : count all the matterOfNextAudits.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/matter-of-next-audits/count")
    public CustomApiResponse countMatterOfNextAudits(MatterOfNextAuditCriteria criteria) {
        log.debug("REST request to count MatterOfNextAudits by criteria: {}", criteria);
        return CustomApiResponse.ok(matterOfNextAuditQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /matter-of-next-audits/:id} : get the "id" matterOfNextAudit.
     *
     * @param id the id of the matterOfNextAudit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the matterOfNextAudit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/matter-of-next-audits/{id}")
    public CustomApiResponse getMatterOfNextAudit(@PathVariable Long id) {
        log.debug("REST request to get MatterOfNextAudit : {}", id);
        Optional<MatterOfNextAudit> matterOfNextAudit = matterOfNextAuditService.findOne(id);
        return CustomApiResponse.ok(matterOfNextAudit);
    }

    /**
     * {@code DELETE  /matter-of-next-audits/:id} : delete the "id" matterOfNextAudit.
     *
     * @param id the id of the matterOfNextAudit to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/matter-of-next-audits/{id}")
    public CustomApiResponse deleteMatterOfNextAudit(@PathVariable Long id) {
        log.debug("REST request to delete MatterOfNextAudit : {}", id);
        matterOfNextAuditService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
