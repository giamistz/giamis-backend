package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.engagement.EngagementPhase;
import gov.giamis.domain.enumeration.EngagementRole;
import gov.giamis.domain.setup.SimpleUser;
import gov.giamis.domain.setup.User;
import gov.giamis.dto.ClientNotificationDTO;
import gov.giamis.dto.ConfirmGoDTO;
import gov.giamis.repository.engagement.EngagementPhaseRepository;
import gov.giamis.service.engagement.EngagementMemberService;
import gov.giamis.service.setup.UserService;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.service.engagement.EngagementQueryService;
import gov.giamis.service.engagement.EngagementService;
import gov.giamis.service.engagement.dto.EngagementCriteria;
import gov.giamis.service.setup.EngagementOrganisationUnitService;
import gov.giamis.service.setup.FinancialYearService;
import gov.giamis.service.util.LocalDateComparisonUtil;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link Engagement}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Resource", description = "Operations to manage Engagement")
public class EngagementResource {

    private final Logger log = LoggerFactory.getLogger(EngagementResource.class);

    private static final String ENTITY_NAME = "engagement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementService engagementService;

    private final EngagementQueryService engagementQueryService;

    private final FinancialYearService financialYearService;

    private final EngagementOrganisationUnitService engagementOrganisationUnitService;

    private final EngagementPhaseRepository engagementPhaseRepository;

	@Autowired
    private EngagementMemberService engagementMemberService;

	@Autowired
    private UserService userService;

    public EngagementResource(EngagementService engagementService,
                              EngagementQueryService engagementQueryService,
                              FinancialYearService financialYearService,
                              EngagementOrganisationUnitService engagementOrganisationUnitService,
                              EngagementPhaseRepository engagementPhaseRepository) {
        this.engagementService = engagementService;
        this.engagementQueryService = engagementQueryService;
        this.financialYearService=financialYearService;
        this.engagementOrganisationUnitService = engagementOrganisationUnitService;
        this.engagementPhaseRepository = engagementPhaseRepository;
    }

    /**
     * {@code POST  /engagements} : Create a new engagement.
     *
     * @param engagement the engagement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagement, or with status {@code 400 (Bad Request)} if the engagement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagements")
    @ApiOperation(value = "Create Engagement")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public CustomApiResponse createEngagement(@Valid @RequestBody Engagement engagement) throws URISyntaxException {
        log.debug("REST request to save Engagement : {}", engagement);

        if (engagement.getId() != null) {
            throw new BadRequestAlertException("A new engagement already have an ID", ENTITY_NAME, "id exists");
        }
        FinancialYear financialYear=financialYearService.findCurrent();
        validateDate(engagement, financialYear);
        //end Date Validation

        engagement.setFinancialYear(financialYear);

        Engagement result = engagementService.save(engagement);
        // Save current user as part of the engagement
        User user= userService.getCurrent().get();
        EngagementMember engagementMember = new EngagementMember();
        engagementMember
            .user(new SimpleUser(user.getId()))
            .role(EngagementRole.MEMBER)
            .engagement(result);
        engagementMemberService.save(engagementMember);

        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagements} : Updates an existing engagement.
     *
     * @param engagement the engagement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagement,
     * or with status {@code 400 (Bad Request)} if the engagement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagements")
    @ApiOperation(value = "Update existing Engagement")
    public CustomApiResponse updateEngagement(@Valid @RequestBody Engagement engagement) throws URISyntaxException {
        log.debug("REST request to update Engagement : {}", engagement);

        FinancialYear financialYear=financialYearService.findCurrent();

        if (engagement.getId() == null) {
			throw new BadRequestAlertException("Updated Engagement does not have an ID", ENTITY_NAME,
					"id not exists");
		}
        validateDate(engagement, financialYear);

        engagement.setFinancialYear(financialYear);
        //end Date Validation
        Engagement result = engagementService.save(engagement);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    private void validateDate(Engagement engagement, FinancialYear financialYear) {
        if(financialYear == null) {
        	throw new DataIntegrityViolationException("Current Financial Year does not exists ");
        }

        if (engagement.getStartDate().isBefore(financialYear.getStartDate()) ||
            engagement.getStartDate().isAfter(financialYear.getEndDate())) {
            throw new DataIntegrityViolationException("Invalid Start date");
        }
        if (engagement.getEndDate().isBefore(engagement.getStartDate()) ||
            engagement.getEndDate().isAfter(financialYear.getEndDate())) {
            throw new DataIntegrityViolationException("Invalid End date");
        }
    }

    /**
     * {@code GET  /engagements} : get all the engagements.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagements in body.
     */
    @GetMapping("/engagements")
    @ApiOperation(value = "Get all Engagements; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagements(EngagementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagements by criteria: {}", criteria);

        // Get current login user
        User user = userService.getCurrent().get();
        // Create filter by current user
        LongFilter userFilter = new LongFilter();
        userFilter.setEquals(user.getId());
        // Set filter to fetch engagement by current user login
        criteria.setUserId(userFilter);
        Page<Engagement> page = engagementQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagements/count} : count all the engagements.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagements/count")
    @ApiOperation(value = "Count all Engagement; Optional filters can be applied", response = CustomApiResponse.class)
    public CustomApiResponse countEngagements(EngagementCriteria criteria) {
        log.debug("REST request to count Engagements by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagements/:id} : get the "id" engagement.
     *
     * @param id the id of the engagement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagements/{id}")
    @ApiOperation("Get a single Engagement by ID")
    public CustomApiResponse getEngagement(@PathVariable Long id) {
        log.debug("REST request to get Engagement : {}", id);

        if (!engagementService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagagement with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<Engagement> engagement = engagementService.findOne(id);
        return CustomApiResponse.ok(engagement);
    }

    /**
     * {@code DELETE  /engagements/:id} : delete the "id" engagement.
     *
     * @param id the id of the engagement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagements/{id}")
    @ApiOperation("Delete  a single Engagement by ID")
    public CustomApiResponse deleteEngagement(@PathVariable Long id) {
        log.debug("REST request to delete Engagement : {}", id);

        if (!engagementService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagagement with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        engagementService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-organisation-units")
    @ApiOperation("Get Engagement by organisation Unit ID")
    public CustomApiResponse getEngagementOrgUnit(@RequestParam(value = "parentId", required = false) Long parentId) {
	    if (parentId != null) {
	        return CustomApiResponse.ok(engagementOrganisationUnitService.getOrgUnitChildren(parentId));
        }
	    return CustomApiResponse.ok(engagementOrganisationUnitService.getCurrentEngagementOrgUnit(parentId));
    }

    @GetMapping("/get-engagement-by-fy-org/{fnID}/{orgID}")
    @ApiOperation("Get Engagement by financial year ID and organisation Unit ID")
    public CustomApiResponse getEngagementByFinancialYearAndOrgUnit(@PathVariable Long fnID, @PathVariable Long orgID) {
        if (fnID != null && orgID != null) {
            return CustomApiResponse.ok(engagementService.findByFinancialYearAndOrganisationUnit(fnID, orgID));
        }
        return CustomApiResponse.ok(engagementService.findByFinancialYearAndOrganisationUnit(fnID, orgID));
    }

    @PutMapping("/engagements/confirm-go-decision")
    public CustomApiResponse confirmConfirm(@Valid @RequestBody ConfirmGoDTO confirmGoDTO) {
         Engagement engagement = engagementService.confirmGoDecision(confirmGoDTO);
         return CustomApiResponse.ok("CONFIRMATION SUCCESSFULLY", engagement);
    }

    @PostMapping("/engagements/notify-client")
    public CustomApiResponse notifyClient(@Valid @RequestBody ClientNotificationDTO notificationDTO) {
        CommunicationLetter letter = engagementService.createEngagementLetter(notificationDTO);
        engagementService.sendNotification(letter, notificationDTO.getEmail());
        return CustomApiResponse.ok("NOTIFICATION SUCCESSFULLY", letter);
    }
}
