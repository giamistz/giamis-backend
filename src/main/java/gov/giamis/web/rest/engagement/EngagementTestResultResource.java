package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementTestResult;
import gov.giamis.service.engagement.EngagementProcedureService;
import gov.giamis.service.engagement.EngagementTestResultQueryService;
import gov.giamis.service.engagement.EngagementTestResultService;
import gov.giamis.service.engagement.dto.EngagementTestResultCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementTestResult}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Test Result Resource", description = "Operations to manage Engagement Test Result")
public class EngagementTestResultResource {

    private final Logger log = LoggerFactory.getLogger(EngagementTestResultResource.class);

    private static final String ENTITY_NAME = "engagementTestResult";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementTestResultService engagementTestResultService;

    private final EngagementTestResultQueryService engagementTestResultQueryService;

    private final EngagementProcedureService engagementProcedureService;

    public EngagementTestResultResource(EngagementTestResultService engagementTestResultService,
    		EngagementTestResultQueryService engagementTestResultQueryService,
    		EngagementProcedureService engagementProcedureService) {
        this.engagementTestResultService = engagementTestResultService;
        this.engagementTestResultQueryService = engagementTestResultQueryService;
        this.engagementProcedureService = engagementProcedureService;
    }

    /**
     * {@code POST  /engagement-test-results} : Create a new engagementTestResult.
     *
     * @param engagementTestResult the engagementTestResult to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementTestResult, or with status {@code 400 (Bad Request)} if the engagementTestResult has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-test-results")
    @ApiOperation(value = "Create Engagement Test Result")
    @ResponseStatus(HttpStatus.CREATED)
	@Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementTestResult(@Valid @RequestBody EngagementTestResult engagementTestResult) throws URISyntaxException {
        log.debug("REST request to save Engagement Test Result : {}", engagementTestResult);
        if (engagementTestResult.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Test Result  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementTestResult result = engagementTestResultService.save(engagementTestResult);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-test-results} : Updates an existing engagementTestResult.
     *
     * @param engagementTestResult the engagementTestResult to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementTestResult,
     * or with status {@code 400 (Bad Request)} if the engagementTestResult is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementTestResult couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-test-results")
    @ApiOperation(value = "Update existing Engagement Test Result")
    public CustomApiResponse updateEngagementTestResult(@Valid @RequestBody EngagementTestResult engagementTestResult) throws URISyntaxException {
        log.debug("REST request to update EngagementTestResult : {}", engagementTestResult);
        if (engagementTestResult.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Test Result does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        engagementTestResultService.updateEngagementTestResult(engagementTestResult.getEngagementItem().getId(), engagementTestResult.getWorkProgram().getId(), engagementTestResult.getEngagementProcedure().getId(), engagementTestResult.getId());
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }

    /**
     * {@code GET  /engagement-test-results} : get all the engagementTestResults.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementTestResults in body.
     */
    @GetMapping("/engagement-test-results")
    @ApiOperation(value = "Get all Engagement Test Results; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementTestResults(EngagementTestResultCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Test Results by criteria: {}", criteria);
        Page<EngagementTestResult> page = engagementTestResultQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-test-results/count} : count all the engagementTestResults.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-test-results/count")
    @ApiOperation(value = "Count all Engagement Test Results; Optional filters can be applied")
    public CustomApiResponse countEngagementTestResults(EngagementTestResultCriteria criteria) {
        log.debug("REST request to count EngagementTestResults by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementTestResultQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-test-results/:id} : get the "id" engagementTestResult.
     *
     * @param id the id of the engagementTestResult to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementTestResult, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-test-results/{id}")
    @ApiOperation("Get a single Engagement Test Result by ID")
    public CustomApiResponse getEngagementTestResult(@PathVariable Long id) {
        log.debug("REST request to get Engagement Test Result : {}", id);

        if (!engagementTestResultService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Test Result with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementTestResult> engagementTestResult = engagementTestResultService.findOne(id);
        return CustomApiResponse.ok(engagementTestResult);
    }

    /**
     * {@code DELETE  /engagement-test-results/:id} : delete the "id" engagementTestResult.
     *
     * @param id the id of the engagementTestResult to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-test-results/{id}")
    @ApiOperation("Delete  a single Engagement Test Result by ID")
    public CustomApiResponse deleteEngagementTestResult(@PathVariable Long id) {
        log.debug("REST request to delete EngagementTestResult : {}", id);

        if (!engagementTestResultService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Test Result with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementTestResultService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-test-results/byProcedure")
    @ApiOperation("Get  Engagement Test Result by engagement Procedure ID")
    public CustomApiResponse getEngagementTestResultByEngagementProcedure(@RequestParam Long id) {
        log.debug("REST request to get Engagement Test Result by Engagement Procedure ID: {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<EngagementTestResult> engagementTestResult = engagementTestResultService.findByEngagementProcedure(id);
        return CustomApiResponse.ok(engagementTestResult);
    }

    @GetMapping("/results-engagement-procedure-score")
    @ApiOperation("Get  Engagement Test Result by Score and engagement Procedure ID")
    public CustomApiResponse getEngagementTestResultByScoreEngagementProcedure(@RequestParam Long id) {
        log.debug("Get Engagement Test Result by Score and engagement Procedure ID: {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<EngagementTestResult> engagementTestResult = engagementTestResultService.findByScoreAndEngagementProcedure(id);
        return CustomApiResponse.ok(engagementTestResult);
    }

}
