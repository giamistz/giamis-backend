package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import gov.giamis.dto.WalkThroughDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.service.engagement.InternalControlQueryService;
import gov.giamis.service.engagement.InternalControlService;
import gov.giamis.service.setup.dto.InternalControlCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link InternalControl}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Internal Control Resource", description = "Operations to manage Internal Control")
public class InternalControlResource {

    private final Logger log = LoggerFactory.getLogger(InternalControlResource.class);

    private static final String ENTITY_NAME = "internalControl";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InternalControlService internalControlService;

    private final InternalControlQueryService internalControlQueryService;

    public InternalControlResource(InternalControlService internalControlService, InternalControlQueryService internalControlQueryService) {
        this.internalControlService = internalControlService;
        this.internalControlQueryService = internalControlQueryService;
    }

    /**
     * {@code POST  /internal-controls} : Create a new internalControl.
     *
     * @param internalControl the internalControl to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new internalControl, or with status {@code 400 (Bad Request)} if the internalControl has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/internal-controls")
    @ApiOperation(value = "Create Internal Control")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createInternalControl(@Valid @RequestBody InternalControl internalControl) {
        log.debug("REST request to save Internal Control : {}", internalControl);
        if (internalControl.getId() != null) {
            throw new BadRequestAlertException("A new Internal Control already have an ID", ENTITY_NAME,
                    "id exists");
        }
        InternalControl result = internalControlService.save(internalControl);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /internal-controls} : Updates an existing internalControl.
     *
     * @param internalControl the internalControl to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated internalControl,
     * or with status {@code 400 (Bad Request)} if the internalControl is not valid,
     * or with status {@code 500 (Internal Server Error)} if the internalControl couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/internal-controls")
    @ApiOperation(value = "Update existing Internal Control")
    public CustomApiResponse updateInternalControl(@Valid @RequestBody InternalControl internalControl) {
        log.debug("REST request to update Internal Control : {}", internalControl);
        if (internalControl.getId() == null) {
            throw new BadRequestAlertException("Updated Internal Control does not have an ID", ENTITY_NAME, "id not exists");
        }
        if (!internalControlService.findOne(internalControl.getId()).isPresent()) {
            throw new BadRequestAlertException("Internal Control with ID: " + internalControl.getId() + " not found", ENTITY_NAME,
                    "id does not exists");
        }
        InternalControl result = internalControlService.save(internalControl);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /internal-controls} : get all the internalControls.
     *


     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of internalControls in body.
     */
    @GetMapping("/internal-controls/by-engagement/{engagementId}")
    @ApiOperation(value = "Get all Internal Control; with pagination, optional filters by fields can be applied",
        response = CustomApiResponse.class)
    public CustomApiResponse getAllInternalControls(
        @PathVariable Long engagementId) {
        log.debug("REST request to get Internal Controls by engagementId: {}", engagementId);

        return CustomApiResponse.ok(internalControlQueryService.findByEngagement(engagementId));
    }

//    @GetMapping("/internal-controls/by-engagement/{engagementId}/with-procedures")
//    public CustomApiResponse getAllWithProcedures(@PathVariable Long engagementId) {
//        log.debug("REST request to get SpecificObjectives by eng: {}", engagementId);
//
//        List<InternalControl> controls = internalControlQueryService.findByEngagementWithProcedures(engagementId);
//        return CustomApiResponse.ok(controls);
//    }

    /**
    * {@code GET  /internal-controls/count} : count all the internalControls.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/internal-controls/count")
    @ApiOperation("Count all Internal Countrols; Optional filters can be applied")
    public CustomApiResponse countInternalControls(InternalControlCriteria criteria) {
        log.debug("REST request to count Internal Controls by criteria: {}", criteria);
        return CustomApiResponse.ok(internalControlQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /internal-controls/:id} : get the "id" internalControl.
     *
     * @param id the id of the internalControl to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the internalControl, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/internal-controls/{id}")
    @ApiOperation("Get a single Internal Control  by ID")
    public CustomApiResponse getInternalControl(@PathVariable Long id) {
        log.debug("REST request to get Internal Control : {}", id);
        if (!internalControlService.findOne(id) .isPresent()) {
            throw new BadRequestAlertException("Internal Control with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        Optional<InternalControl> internalControl = internalControlService.findOne(id);
        return CustomApiResponse.ok(internalControl);
    }

    /**
     * {@code DELETE  /internal-controls/:id} : delete the "id" internalControl.
     *
     * @param id the id of the internalControl to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/internal-controls/{id}")
    @ApiOperation("Delete a single Internal Control by ID")
    public CustomApiResponse deleteInternalControl(@PathVariable Long id) {
        log.debug("REST request to delete Internal Control : {}", id);
        if (!internalControlService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Internal Control with ID: " + id + " not found", ENTITY_NAME, "id not exists");
        }
        internalControlService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
    
    @GetMapping("/internal-controls-last-record")
    @ApiOperation("Request to get Last Record of InternalControl By Engagement ID")
    public CustomApiResponse getLastRecordByEngagementId(@RequestParam Long id) {
        log.debug("Request to get Last Record of InternalControl By Engagement ID: {}", id);
        
        if (!internalControlService.findLastRecord(id).isPresent()) {
            throw new BadRequestAlertException("Internal Control with Engagement with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<InternalControl> internalControl = internalControlService.findLastRecord(id);
        return CustomApiResponse.ok(internalControl);
    }


    @PostMapping("/internal-controls/walk-through")
    public CustomApiResponse createWalkThrough(@Valid @RequestBody WalkThroughDTO walkThrough) {
        internalControlService.createWalkThrough(walkThrough);
        return CustomApiResponse.ok("Walk through create");
    }
}
