package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngFindingRecommendation;
import gov.giamis.service.engagement.EngFindingRecommendationService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.dto.EngFindingReccomendationCriteria;
import gov.giamis.service.engagement.EngFindingRecommendationQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

import java.util.Optional;

/**
 * REST controller for managing {@link EngFindingRecommendation}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class EngFindingRecommendationResource {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationResource.class);

    private static final String ENTITY_NAME = "engFindingRecommendations";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngFindingRecommendationService engFindingRecommendationService;

    private final EngFindingRecommendationQueryService engFindingRecommendationQueryService;

    public EngFindingRecommendationResource(EngFindingRecommendationService engFindingRecommendationService, EngFindingRecommendationQueryService engFindingRecommendationQueryService) {
        this.engFindingRecommendationService = engFindingRecommendationService;
        this.engFindingRecommendationQueryService = engFindingRecommendationQueryService;
    }

    /**
     * {@code POST  /eng-finding-recommendations} : Create a new engFindingReccomendation.
     *
     * @param engFindingRecommendation the engFindingReccomendation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engFindingReccomendation, or with status {@code 400 (Bad Request)} if the engFindingReccomendation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/eng-finding-recommendations")
    @ApiOperation(value = "Create engagement finding recommendation")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngFindingReccomendation(@Valid @RequestBody EngFindingRecommendation engFindingRecommendation) throws URISyntaxException {
        log.debug("REST request to save EngFindingRecommendations : {}", engFindingRecommendation);
        if (engFindingRecommendation.getId() != null) {
            throw new BadRequestAlertException("A new engFindingReccomendation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EngFindingRecommendation result = engFindingRecommendationService.save(engFindingRecommendation);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /eng-finding-recommendations} : Updates an existing engFindingRecommendation.
     *
     * @param engFindingRecommendation the engFindingRecommendation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engFindingReccomendation,
     * or with status {@code 400 (Bad Request)} if the engFindingReccomendation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engFindingReccomendation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/eng-finding-recommendations")
    @ApiOperation("Update existing engagement finding recommendation")
    public CustomApiResponse updateEngFindingReccomendation(@Valid @RequestBody EngFindingRecommendation engFindingRecommendation) throws URISyntaxException {
        log.debug("REST request to update EngFindingRecommendation : {}", engFindingRecommendation);
        if (engFindingRecommendation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EngFindingRecommendation result = engFindingRecommendationService.save(engFindingRecommendation);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /eng-finding-recommendations} : get all the engFindingRecommendations.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engFindingRecommendations in body.
     */
    @GetMapping("/eng-finding-recommendations")
    @ApiOperation("Get all engagement finding recommendation")
    public CustomApiResponse getAllEngFindingReccomendations(EngFindingReccomendationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngFindingRecommendations by criteria: {}", criteria);
        Page<EngFindingRecommendation> page = engFindingRecommendationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /eng-finding-recommendations/count} : count all the engFindingReccomendations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/eng-finding-recommendations/count")
    @ApiOperation(value = "Count all resources of engagement finding recommendation")
    public CustomApiResponse countEngFindingReccomendations(EngFindingReccomendationCriteria criteria) {
        log.debug("REST request to count EngFindingRecommendations by criteria: {}", criteria);
        return CustomApiResponse.ok(engFindingRecommendationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /eng-finding-recommendations/:id} : get the "id" engFindingReccomendation.
     *
     * @param id the id of the engFindingRecommendation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engFindingReccomendation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/eng-finding-recommendations/{id}")
    public CustomApiResponse getEngFindingReccomendation(@PathVariable Long id) {
        log.debug("REST request to get EngFindingReccomendation : {}", id);

        if (!engFindingRecommendationService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Finding Recommendation with ID: " + id + " not found", ENTITY_NAME,
                "id not exists");
        }
        Optional<EngFindingRecommendation> engFindingRecommendation = engFindingRecommendationService.findOne(id);
        return CustomApiResponse.ok(engFindingRecommendation);
    }

    /**
     * {@code DELETE  /eng-finding-reccomendations/:id} : delete the "id" engFindingReccomendation.
     *
     * @param id the id of the engFindingReccomendation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/eng-finding-recommendations/{id}")
    public CustomApiResponse deleteEngFindingReccomendation(@PathVariable Long id) {
        log.debug("REST request to delete EngFindingReccomendation : {}", id);
        engFindingRecommendationService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
