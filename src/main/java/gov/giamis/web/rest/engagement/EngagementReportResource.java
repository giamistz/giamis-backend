package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementReport;
import gov.giamis.service.engagement.EngagementReportService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.engagement.dto.EngagementReportCriteria;
import gov.giamis.service.engagement.EngagementReportQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link EngagementReport}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class EngagementReportResource {

    private final Logger log = LoggerFactory.getLogger(EngagementReportResource.class);

    private static final String ENTITY_NAME = "engagementReport";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementReportService engagementReportService;

    private final EngagementReportQueryService engagementReportQueryService;

    public EngagementReportResource(EngagementReportService engagementReportService, EngagementReportQueryService engagementReportQueryService) {
        this.engagementReportService = engagementReportService;
        this.engagementReportQueryService = engagementReportQueryService;
    }

    /**
     * {@code POST  /engagement-reports} : Create a new engagementReport.
     *
     * @param engagementReport the engagementReport to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementReport, or with status {@code 400 (Bad Request)} if the engagementReport has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-reports")
    public CustomApiResponse createEngagementReport(@Valid @RequestBody EngagementReport engagementReport) throws URISyntaxException {
        log.debug("REST request to save EngagementReport : {}", engagementReport);
        if (engagementReport.getId() != null) {
            throw new BadRequestAlertException("A new engagementReport cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (Objects.isNull(engagementReport.getEngagement())) {
            throw new BadRequestAlertException("Invalid association value provided", ENTITY_NAME, "null");
        }
        EngagementReport result = engagementReportService.save(engagementReport);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-reports} : Updates an existing engagementReport.
     *
     * @param engagementReport the engagementReport to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementReport,
     * or with status {@code 400 (Bad Request)} if the engagementReport is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementReport couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-reports")
    public CustomApiResponse updateEngagementReport(@Valid @RequestBody EngagementReport engagementReport) throws URISyntaxException {
        log.debug("REST request to update EngagementReport : {}", engagementReport);
        if (engagementReport.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EngagementReport result = engagementReportService.save(engagementReport);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-reports} : get all the engagementReports.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementReports in body.
     */
    @GetMapping("/engagement-reports")
    public CustomApiResponse getAllEngagementReports(EngagementReportCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementReports by criteria: {}", criteria);
        Page<EngagementReport> page = engagementReportQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-reports/count} : count all the engagementReports.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-reports/count")
    public CustomApiResponse countEngagementReports(EngagementReportCriteria criteria) {
        log.debug("REST request to count EngagementReports by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementReportQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-reports/:id} : get the "id" engagementReport.
     *
     * @param id the id of the engagementReport to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementReport, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-reports/{id}")
    public CustomApiResponse getEngagementReport(@PathVariable Long id) {
        log.debug("REST request to get EngagementReport : {}", id);
        Optional<EngagementReport> engagementReport = engagementReportService.findOne(id);
        return CustomApiResponse.ok(engagementReport);
    }

    /**
     * {@code DELETE  /engagement-reports/:id} : delete the "id" engagementReport.
     *
     * @param id the id of the engagementReport to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-reports/{id}")
    public CustomApiResponse deleteEngagementReport(@PathVariable Long id) {
        log.debug("REST request to delete EngagementReport : {}", id);
        engagementReportService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
