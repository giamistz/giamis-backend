package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementEntranceMeeting;
import gov.giamis.service.engagement.EngagementEntranceMeetingQueryService;
import gov.giamis.service.engagement.EngagementEntranceMeetingService;
import gov.giamis.service.engagement.dto.EngagementEntranceMeetingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementEntranceMeeting}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Entrance Meeting Resource", description = "Operations to manage Engagement Entrance Meeting")
public class EngagementEntranceMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementEntranceMeetingResource.class);

    private static final String ENTITY_NAME = "engagementEntranceMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementEntranceMeetingService engagementEntranceMeetingService;

    private final EngagementEntranceMeetingQueryService engagementEntranceMeetingQueryService;

    public EngagementEntranceMeetingResource(
            EngagementEntranceMeetingService engagementEntranceMeetingService,
            EngagementEntranceMeetingQueryService engagementEntranceMeetingQueryService) {
        this.engagementEntranceMeetingService = engagementEntranceMeetingService;
        this.engagementEntranceMeetingQueryService = engagementEntranceMeetingQueryService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementEntranceMeeting.
     *
     * @param engagementEntranceMeeting the engagementEntranceMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementEntranceMeeting, or with status {@code 400 (Bad Request)} if the engagementEntranceMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-entrance-meetings")
    @ApiOperation(value = "Create Engagement Entrance Meeting")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementEntranceMeeting(@Valid @RequestBody EngagementEntranceMeeting engagementEntranceMeeting) throws URISyntaxException {
        log.debug("REST request to save Engagement Entrance Meeting : {}", engagementEntranceMeeting);
        EngagementEntranceMeeting result = null;
        if (engagementEntranceMeeting.getId() != null) {
            throw new BadRequestAlertException("A new engagementEntranceMeeting  already have an ID", ENTITY_NAME, "id exists");
        }

        if (engagementEntranceMeeting.getEngagement() == null) {
            throw new BadRequestAlertException("Engagement does not Exist", ENTITY_NAME, "Does not exists");
        }

        if((engagementEntranceMeetingService.countEngagementEntranceMeetingByEngagementId(engagementEntranceMeeting.getEngagement().getId()) == 0) && (engagementEntranceMeeting.getId() == null)) {
        		result = engagementEntranceMeetingService.save(engagementEntranceMeeting);

        }else {
        	result = engagementEntranceMeetingService.findByEngagementId(engagementEntranceMeeting.getEngagement().getId());
        }
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementEntranceMeeting.
     *
     * @param engagementEntranceMeeting the engagementEntranceMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementEntranceMeeting,
     * or with status {@code 400 (Bad Request)} if the engagementEntranceMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementEntranceMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-entrance-meetings")
    @ApiOperation(value = "Update existing Engagement Entrance Meeting")
    public CustomApiResponse updateEngagementEntranceMeeting(@Valid @RequestBody EngagementEntranceMeeting engagementEntranceMeeting) throws URISyntaxException {
        log.debug("REST request to update Engagement Entrance Meeting : {}", engagementEntranceMeeting);
        if (engagementEntranceMeeting.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Entrance Meeting does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        EngagementEntranceMeeting result = engagementEntranceMeetingService.save(engagementEntranceMeeting);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementEntranceMeetings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementEntranceMeetings in body.
     */
    @GetMapping("/engagement-entrance-meetings")
    @ApiOperation(value = "Get all Engagement Entrance Meetings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementEntranceMeetings(EngagementEntranceMeetingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementEntranceMeetings by criteria: {}", criteria);
        Page<EngagementEntranceMeeting> page = engagementEntranceMeetingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementEntranceMeetings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-entrance-meetings/count")
    @ApiOperation(value = "Count all Engagement Entrance Meeting; Optional filters can be applied")
    public CustomApiResponse countEngagementEntranceMeetings(EngagementEntranceMeetingCriteria criteria) {
        log.debug("REST request to count Engagement Entrance Meetings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementEntranceMeetingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementEntranceMeeting.
     *
     * @param id the id of the engagementEntranceMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementEntranceMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-entrance-meetings/{id}")
    @ApiOperation("Get a single Engagement Entrance Meeting by ID")
    public CustomApiResponse getEngagementEntranceMeeting(@PathVariable Long id) {
        log.debug("REST request to get Engagement Entrance Meeting : {}", id);

        if (!engagementEntranceMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Entrance Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementEntranceMeeting> engagementEntranceMeeting = engagementEntranceMeetingService.findOne(id);
        return CustomApiResponse.ok(engagementEntranceMeeting);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementEntranceMeeting.
     *
     * @param id the id of the engagementEntranceMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-entrance-meetings/{id}")
    @ApiOperation("Delete  a single Engagement Entrance Meeting by ID")
    public CustomApiResponse deleteEngagementEntranceMeeting(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Entrance Meeting : {}", id);

        if (!engagementEntranceMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Entrance Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementEntranceMeetingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-entrance-meeting-by-engagement-id/{id}")
    public CustomApiResponse getEngagementEntranceMeetingByEngagementId(@PathVariable Long id) {
        log.debug("REST Get Engagement entrance meeting by engagement ID : {}", id);
        EngagementEntranceMeeting engagementEntranceMeetingById = engagementEntranceMeetingService.findByEngagementId(id);
        return CustomApiResponse.ok(engagementEntranceMeetingById);
    }
}
