package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementPlanningCost;
import gov.giamis.service.engagement.EngagementPlanningCostQueryService;
import gov.giamis.service.engagement.EngagementPlanningCostService;
import gov.giamis.service.engagement.dto.EngagementPlanningCostCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementPlanningCost}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Planning Cost Resource", description = "Operations to manage Engagement Planning Costs")
public class EngagementPlanningCostResource {

    private final Logger log = LoggerFactory.getLogger(EngagementPlanningCostResource.class);

    private static final String ENTITY_NAME = "engagementPlanningCost";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementPlanningCostService engagementPlanningCostService;
    
    //private final EngagementService engagementService;
    //private final GfsCodeService gfsCodeService;

    private final EngagementPlanningCostQueryService engagementPlanningCostQueryService;
   
    public EngagementPlanningCostResource(
            EngagementPlanningCostService engagementPlanningCostService, 
            EngagementPlanningCostQueryService engagementPlanningCostQueryService) {
        this.engagementPlanningCostService = engagementPlanningCostService;
        this.engagementPlanningCostQueryService = engagementPlanningCostQueryService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementPlanningCost.
     *
     * @param engagementPlanningCost the engagementPlanningCost to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementPlanningCost, or with status {@code 400 (Bad Request)} if the engagementPlanningCost has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-planning-costs")
    @ApiOperation(value = "Create Engagement Planning Cost")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementPlanningCost(@Valid @RequestBody EngagementPlanningCost engagementPlanningCost) throws URISyntaxException {
        log.debug("REST request to save Engagement Planning Cost : {}", engagementPlanningCost);
        EngagementPlanningCost result = null;
        if (engagementPlanningCost.getId() != null) {
            throw new BadRequestAlertException("A new engagementPlanningCost  already have an ID", ENTITY_NAME, "id exists");
        }
       
        result = engagementPlanningCostService.save(engagementPlanningCost);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementPlanningCost.
     *
     * @param engagementPlanningCost the engagementPlanningCost to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementPlanningCost,
     * or with status {@code 400 (Bad Request)} if the engagementPlanningCost is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementPlanningCost couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-planning-costs")
    @ApiOperation(value = "Update existing Engagement Planning Cost")
    public CustomApiResponse updateEngagementPlanningCost(@Valid @RequestBody EngagementPlanningCost engagementPlanningCost) throws URISyntaxException {
        log.debug("REST request to update Engagement Planning Cost : {}", engagementPlanningCost);
        if (engagementPlanningCost.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Planning Cost does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        
        EngagementPlanningCost result = engagementPlanningCostService.save(engagementPlanningCost);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementPlanningCosts.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementPlanningCosts in body.
     */
    @GetMapping("/engagement-planning-costs")
    @ApiOperation(value = "Get all Engagement Planning Costs; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementPlanningCosts(EngagementPlanningCostCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementPlanningCosts by criteria: {}", criteria);
        Page<EngagementPlanningCost> page = engagementPlanningCostQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementPlanningCosts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-planning-costs/count")
    @ApiOperation(value = "Count all Engagement Planning Cost; Optional filters can be applied")
    public CustomApiResponse countEngagementPlanningCosts(EngagementPlanningCostCriteria criteria) {
        log.debug("REST request to count Engagement Planning Costs by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementPlanningCostQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementPlanningCost.
     *
     * @param id the id of the engagementPlanningCost to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementPlanningCost, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-planning-costs/{id}")
    @ApiOperation("Get a single Engagement Planning Cost by ID")
    public CustomApiResponse getEngagementPlanningCost(@PathVariable Long id) {
        log.debug("REST request to get Engagement Planning Cost : {}", id);
        
        if (!engagementPlanningCostService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Planning Cost with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementPlanningCost> engagementPlanningCost = engagementPlanningCostService.findOne(id);
        return CustomApiResponse.ok(engagementPlanningCost);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementPlanningCost.
     *
     * @param id the id of the engagementPlanningCost to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-planning-costs/{id}")
    @ApiOperation("Delete  a single Engagement Planning Cost by ID")
    public CustomApiResponse deleteEngagementPlanningCost(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Planning Cost : {}", id);
        
        if (!engagementPlanningCostService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Planning Cost with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementPlanningCostService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
    
}