package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementMeeting;
import gov.giamis.service.engagement.EngagementMeetingQueryService;
import gov.giamis.service.engagement.EngagementMeetingService;
import gov.giamis.service.engagement.dto.EngagementMeetingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementMeeting}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Meeting Resource", description = "Operations to manage Engagement Meeting")
public class EngagementMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementMeetingResource.class);

    private static final String ENTITY_NAME = "engagementMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementMeetingService engagementMeetingService;

    private final EngagementMeetingQueryService engagementMeetingQueryService;

    public EngagementMeetingResource(EngagementMeetingService engagementMeetingService, EngagementMeetingQueryService engagementMeetingQueryService) {
        this.engagementMeetingService = engagementMeetingService;
        this.engagementMeetingQueryService = engagementMeetingQueryService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementMeeting.
     *
     * @param engagementMeeting the engagementMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementMeeting, or with status {@code 400 (Bad Request)} if the engagementMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-meetings")
    @ApiOperation(value = "Create Engagement Meeting")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementMeeting(@Valid @RequestBody EngagementMeeting engagementMeeting) throws URISyntaxException {
        log.debug("REST request to save EngagementMeeting : {}", engagementMeeting);
        if (engagementMeeting.getId() != null) {
            throw new BadRequestAlertException("A new engagementMeeting  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementMeeting result = engagementMeetingService.save(engagementMeeting);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementMeeting.
     *
     * @param engagementMeeting the engagementMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementMeeting,
     * or with status {@code 400 (Bad Request)} if the engagementMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-meetings")
    @ApiOperation(value = "Update existing Engagement Meeting")
    public CustomApiResponse updateEngagementMeeting(@Valid @RequestBody EngagementMeeting engagementMeeting) throws URISyntaxException {
        log.debug("REST request to update EngagementMeeting : {}", engagementMeeting);
        if (engagementMeeting.getId() == null) {
        	throw new BadRequestAlertException("Updated Engagement Meeting does not have an ID", ENTITY_NAME,
					"id not exists");
        }
        EngagementMeeting result = engagementMeetingService.save(engagementMeeting);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementMeetings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementMeetings in body.
     */
    @GetMapping("/engagement-meetings")
    @ApiOperation(value = "Get all Engagement Meetings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementMeetings(EngagementMeetingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementMeetings by criteria: {}", criteria);
        Page<EngagementMeeting> page = engagementMeetingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementMeetings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-meetings/count")
    @ApiOperation(value = "Count all Engagement Meetings; Optional filters can be applied")
    public CustomApiResponse countEngagementMeetings(EngagementMeetingCriteria criteria) {
        log.debug("REST request to count EngagementMeetings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementMeetingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementMeeting.
     *
     * @param id the id of the engagementMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-meetings/{id}")
    @ApiOperation("Get a single Engagement Meeting by ID")
    public CustomApiResponse getEngagementMeeting(@PathVariable Long id) {
        log.debug("REST request to get EngagementMeeting : {}", id);
        
        if (!engagementMeetingService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagement Meeting with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        Optional<EngagementMeeting> engagementMeeting = engagementMeetingService.findOne(id);
        return CustomApiResponse.ok(engagementMeeting);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementMeeting.
     *
     * @param id the id of the engagementMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-meetings/{id}")
    @ApiOperation("Delete  a single Engagement Meeting by ID")
    public CustomApiResponse deleteEngagementMeeting(@PathVariable Long id) {
        log.debug("REST request to delete EngagementMeeting : {}", id);
        
        if (!engagementMeetingService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagement Meeting with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        engagementMeetingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
