package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import gov.giamis.service.engagement.EngagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementExitMeeting;
import gov.giamis.service.engagement.EngagementExitMeetingQueryService;
import gov.giamis.service.engagement.EngagementExitMeetingService;
import gov.giamis.service.engagement.dto.EngagementExitMeetingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementExitMeeting}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Exit Meeting Resource", description = "Operations to manage Engagement Exit Meeting")
public class EngagementExitMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementExitMeetingResource.class);

    private static final String ENTITY_NAME = "engagementExitMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementExitMeetingService engagementExitMeetingService;

    private final EngagementExitMeetingQueryService engagementExitMeetingQueryService;

    private final EngagementExitMeetingService engagementExitMeetingServiceByEngagementID;

    public EngagementExitMeetingResource(
        EngagementExitMeetingService engagementExitMeetingService,
        EngagementExitMeetingQueryService engagementExitMeetingQueryService, EngagementService engagementService, EngagementExitMeetingService engagementExitMeetingServiceByEngagementID) {
        this.engagementExitMeetingService = engagementExitMeetingService;
        this.engagementExitMeetingQueryService = engagementExitMeetingQueryService;
        this.engagementExitMeetingServiceByEngagementID = engagementExitMeetingServiceByEngagementID;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementExitMeeting.
     *
     * @param engagementExitMeeting the engagementExitMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementExitMeeting, or with status {@code 400 (Bad Request)} if the engagementExitMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-exit-meetings")
    @ApiOperation(value = "Create Engagement Exit Meeting")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementExitMeeting(@Valid @RequestBody EngagementExitMeeting engagementExitMeeting) throws URISyntaxException {
        log.debug("REST request to save Engagement Exit Meeting : {}", engagementExitMeeting);
        EngagementExitMeeting result = null;
        if (engagementExitMeeting.getId() != null) {
            throw new BadRequestAlertException("A new engagementExitMeeting  already have an ID", ENTITY_NAME, "id exists");
        }

        if (engagementExitMeeting.getEngagement() == null) {
            throw new BadRequestAlertException("Engagement does not Exist", ENTITY_NAME, "Does not exists");
        }

        if((engagementExitMeetingService.countEngagementExitMeetingByEngagementId(engagementExitMeeting.getEngagement().getId()) == 0) && (engagementExitMeeting.getId() == null)) {
                result = engagementExitMeetingService.save(engagementExitMeeting);

        }else {
            result = engagementExitMeetingService.findByEngagementId(engagementExitMeeting.getEngagement().getId());
        }
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementExitMeeting.
     *
     * @param engagementExitMeeting the engagementExitMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementExitMeeting,
     * or with status {@code 400 (Bad Request)} if the engagementExitMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementExitMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-exit-meetings")
    @ApiOperation(value = "Update existing Engagement Exit Meeting")
    public CustomApiResponse updateEngagementExitMeeting(@Valid @RequestBody EngagementExitMeeting engagementExitMeeting) throws URISyntaxException {
        log.debug("REST request to update Engagement Exit Meeting : {}", engagementExitMeeting);
        if (engagementExitMeeting.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Exit Meeting does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        EngagementExitMeeting result = engagementExitMeetingService.save(engagementExitMeeting);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementExitMeetings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementExitMeetings in body.
     */
    @GetMapping("/engagement-exit-meetings")
    @ApiOperation(value = "Get all Engagement Exit Meetings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementExitMeetings(EngagementExitMeetingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementExitMeetings by criteria: {}", criteria);
        Page<EngagementExitMeeting> page = engagementExitMeetingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementExitMeetings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-exit-meetings/count")
    @ApiOperation(value = "Count all Engagement Exit Meeting; Optional filters can be applied")
    public CustomApiResponse countEngagementExitMeetings(EngagementExitMeetingCriteria criteria) {
        log.debug("REST request to count Engagement Exit Meetings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementExitMeetingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementExitMeeting.
     *
     * @param id the id of the engagementExitMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementExitMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-exit-meetings/{id}")
    @ApiOperation("Get a single Engagement Exit Meeting by ID")
    public CustomApiResponse getEngagementExitMeeting(@PathVariable Long id) {
        log.debug("REST request to get Engagement Exit Meeting : {}", id);

        if (!engagementExitMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Exit Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementExitMeeting> engagementExitMeeting = engagementExitMeetingService.findOne(id);
        return CustomApiResponse.ok(engagementExitMeeting);
    }

    @GetMapping("/engagement-exit-meeting-by-engagement-id")
    public CustomApiResponse getEngagementExitMeetingByEngagementId(@Param(value = "id") Long id) {
        log.debug("REST Get Engagement exit meeting by engagement ID : {}", id);
       EngagementExitMeeting engagementExitMeetingById = engagementExitMeetingService.findByEngagementId(id);
        return CustomApiResponse.ok(engagementExitMeetingById);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementExitMeeting.
     *
     * @param id the id of the engagementExitMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-exit-meetings/{id}")
    @ApiOperation("Delete  a single Engagement Exit Meeting by ID")
    public CustomApiResponse deleteEngagementExitMeeting(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Exit Meeting : {}", id);

        if (!engagementExitMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Exit Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementExitMeetingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
