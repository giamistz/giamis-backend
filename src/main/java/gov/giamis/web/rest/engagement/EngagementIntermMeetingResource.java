package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementIntermMeeting;
import gov.giamis.domain.engagement.EngagementIntermMember;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.service.engagement.EngagementIntermMeetingQueryService;
import gov.giamis.service.engagement.EngagementIntermMeetingService;
import gov.giamis.service.engagement.dto.EngagementIntermMeetingCriteria;
import gov.giamis.service.setup.FinancialYearService;
import gov.giamis.service.setup.UserOrganisationUnitService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementIntermMeeting}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Interm Meeting Resource", description = "Operations to manage Engagement Interm Meeting")
public class EngagementIntermMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementIntermMeetingResource.class);

    private static final String ENTITY_NAME = "engagementIntermMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementIntermMeetingService engagementIntermMeetingService;

    private final EngagementIntermMeetingQueryService engagementIntermMeetingQueryService;

   private final FinancialYearService financialYearService;

    FinancialYear financialYear = null;

    private final UserOrganisationUnitService userOrganisationUnitService;

    public EngagementIntermMeetingResource(
    		EngagementIntermMeetingService engagementIntermMeetingService,
    		EngagementIntermMeetingQueryService engagementIntermMeetingQueryService,
    		FinancialYearService financialYearService,
    		UserOrganisationUnitService userOrganisationUnitService) {
        this.engagementIntermMeetingService = engagementIntermMeetingService;
        this.engagementIntermMeetingQueryService = engagementIntermMeetingQueryService;
        this.financialYearService = financialYearService;
        this.userOrganisationUnitService = userOrganisationUnitService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementIntermMeeting.
     *
     * @param engagementIntermMeeting the engagementIntermMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementIntermMeeting, or with status {@code 400 (Bad Request)} if the engagementIntermMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-interm-meetings")
    @ApiOperation(value = "Create Engagement interm Meeting")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementIntermMeeting(@Valid @RequestBody EngagementIntermMeeting engagementIntermMeeting) throws URISyntaxException {
        log.debug("REST request to save Engagement Interm Meeting : {}", engagementIntermMeeting);
        financialYear=financialYearService.findByStatus(Constants.ACTIVE);
        if (engagementIntermMeeting.getId() != null) {
            throw new BadRequestAlertException("A new engagementIntermMeeting  already have an ID", ENTITY_NAME, "id exists");
        }

        if(financialYear == null) {
        	throw new DataIntegrityViolationException("Current Financial Year does not exists ");
        }

        if(userOrganisationUnitService.getCurrentOrganisationUnit() == null) {
        	throw new DataIntegrityViolationException("Login User does not belong to any Organisation Unit");
        }

        for(EngagementIntermMember engagementIntermMember: engagementIntermMeeting.getEngagementIntermMember()){
            engagementIntermMember.setEngagementIntermMeeting(engagementIntermMeeting);
        }
        EngagementIntermMeeting result = engagementIntermMeetingService.save(engagementIntermMeeting);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementIntermMeeting.
     *
     * @param engagementIntermMeeting the engagementIntermMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementIntermMeeting,
     * or with status {@code 400 (Bad Request)} if the engagementIntermMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementIntermMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-interm-meetings")
    @ApiOperation(value = "Update existing Engagement Interm Meeting")
    public CustomApiResponse updateEngagementIntermMeeting(@Valid @RequestBody EngagementIntermMeeting engagementIntermMeeting) throws URISyntaxException {
        log.debug("REST request to update Engagement Interm Meeting : {}", engagementIntermMeeting);
        financialYear=financialYearService.findByStatus(Constants.ACTIVE);
        if (engagementIntermMeeting.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Interm Meeting does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        if(financialYear == null) {
        	throw new DataIntegrityViolationException("Current Financial Year does not exists ");
        }

        if(userOrganisationUnitService.getCurrentOrganisationUnit() == null) {
        	throw new DataIntegrityViolationException("Login User does not belong to any Organisation Unit");
        }

        for(EngagementIntermMember engagementIntermMember: engagementIntermMeeting.getEngagementIntermMember()){
            engagementIntermMember.setEngagementIntermMeeting(engagementIntermMeeting);
        }
        EngagementIntermMeeting result = engagementIntermMeetingService.save(engagementIntermMeeting);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementIntermMeetings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementIntermMeetings in body.
     */
    @GetMapping("/engagement-interm-meetings")
    @ApiOperation(value = "Get all Engagement Interm Meetings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementIntermMeetings(EngagementIntermMeetingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementIntermMeetings by criteria: {}", criteria);
        Page<EngagementIntermMeeting> page = engagementIntermMeetingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-meetings/count} : count all the engagementIntermMeetings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-interm-meetings/count")
    @ApiOperation(value = "Count all Engagement Interm Meeting; Optional filters can be applied")
    public CustomApiResponse countEngagementIntermMeetings(EngagementIntermMeetingCriteria criteria) {
        log.debug("REST request to count Engagement Interm Meetings by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementIntermMeetingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementIntermMeeting.
     *
     * @param id the id of the engagementIntermMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementIntermMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-interm-meetings/{id}")
    @ApiOperation("Get a single Engagement Interm Meeting by ID")
    public CustomApiResponse getEngagementIntermMeeting(@PathVariable Long id) {
        log.debug("REST request to get Engagement Interm Meeting : {}", id);

        if (!engagementIntermMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Interm Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementIntermMeeting> engagementIntermMeeting = engagementIntermMeetingService.findOne(id);
        return CustomApiResponse.ok(engagementIntermMeeting);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementIntermMeeting.
     *
     * @param id the id of the engagementIntermMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-interm-meetings/{id}")
    @ApiOperation("Delete  a single Engagement Interm Meeting by ID")
    public CustomApiResponse deleteEngagementIntermMeeting(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Interm Meeting : {}", id);

        if (!engagementIntermMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Interm Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementIntermMeetingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
