package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementObjective;
import gov.giamis.service.engagement.EngagementObjectiveQueryService;
import gov.giamis.service.engagement.EngagementObjectiveService;
import gov.giamis.service.engagement.dto.EngagementObjectiveCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementObjective}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Objective Resource", description = "Operations to manage Engagement Objective")
public class EngagementObjectiveResource {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveResource.class);

    private static final String ENTITY_NAME = "engagementObjective";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementObjectiveService engagementObjectiveService;

    private final EngagementObjectiveQueryService engagementObjectiveQueryService;

    public EngagementObjectiveResource(EngagementObjectiveService engagementObjectiveService, EngagementObjectiveQueryService engagementObjectiveQueryService) {
        this.engagementObjectiveService = engagementObjectiveService;
        this.engagementObjectiveQueryService = engagementObjectiveQueryService;
    }

    /**
     * {@code POST  /engagement-objectives} : Create a new engagementObjective.
     *
     * @param engagementObjective the engagementObjective to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementObjective, or with status {@code 400 (Bad Request)} if the engagementObjective has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-objectives")
    @ApiOperation(value = "Create Engagement Objective")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementObjective(@Valid @RequestBody EngagementObjective engagementObjective) throws URISyntaxException {
        log.debug("REST request to save EngagementObjective : {}", engagementObjective);
        if (engagementObjective.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Objective already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementObjective result = engagementObjectiveService.save(engagementObjective);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-objectives} : Updates an existing engagementObjective.
     *
     * @param engagementObjective the engagementObjective to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementObjective,
     * or with status {@code 400 (Bad Request)} if the engagementObjective is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementObjective couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-objectives")
    @ApiOperation(value = "Update existing Engagement Objective")
    public CustomApiResponse updateEngagementObjective(@Valid @RequestBody EngagementObjective engagementObjective) throws URISyntaxException {
        log.debug("REST request to update Engagement Objective : {}", engagementObjective);
        if (engagementObjective.getId() == null) {
        	throw new BadRequestAlertException("Updated Engagement Objective does not have an ID", ENTITY_NAME,
					"id not exists");
        }
        
        EngagementObjective result = engagementObjectiveService.save(engagementObjective);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-objectives} : get all the engagementObjectives.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementObjectives in body.
     */
    @GetMapping("/engagement-objectives")
    @ApiOperation(value = "Get all Engagement Objectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementObjectives(EngagementObjectiveCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Objectives by criteria: {}", criteria);
        Page<EngagementObjective> page = engagementObjectiveQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    @GetMapping("/engagement-objectives/with-risks")
    @ApiOperation(value = "Get all Engagement Objectives; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getEngagementObjectivesWithRisks(EngagementObjectiveCriteria criteria) {
        log.debug("REST request to get Engagement Objectives by criteria: {}", criteria);
        List<EngagementObjective> objectives = engagementObjectiveQueryService.findAllWithRiskByCriteria(criteria);
        return CustomApiResponse.ok(objectives);
    }

    /**
     * {@code GET  /engagement-objectives/count} : count all the engagementObjectives.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/engagement-objectives/count")
    @ApiOperation(value = "Count all Engagement Objectives; Optional filters can be applied")
    public CustomApiResponse countEngagementObjectives(EngagementObjectiveCriteria criteria) {
        log.debug("REST request to count Engagement Objectives by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementObjectiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-objectives/:id} : get the "id" engagementObjective.
     *
     * @param id the id of the engagementObjective to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementObjective, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-objectives/{id}")
    @ApiOperation("Get a single Engagement Objective by ID")
    public CustomApiResponse getEngagementObjective(@PathVariable Long id) {
        log.debug("REST request to get EngagementObjective : {}", id);
        
        if (!engagementObjectiveService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagagement Objective with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        
        Optional<EngagementObjective> engagementObjective = engagementObjectiveService.findOne(id);
        return CustomApiResponse.ok(engagementObjective);
    }

    /**
     * {@code DELETE  /engagement-objectives/:id} : delete the "id" engagementObjective.
     *
     * @param id the id of the engagementObjective to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-objectives/{id}")
    @ApiOperation("Delete  a single Engagement Objective by ID")
    public CustomApiResponse deleteEngagementObjective(@PathVariable Long id) {
        log.debug("REST request to delete EngagementObjective : {}", id);
        
        if (!engagementObjectiveService.findOne(id).isPresent()) {
			throw new BadRequestAlertException("Engagagement Objective with ID: " + id + " not found", ENTITY_NAME,
					"id not exists");
		}
        engagementObjectiveService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
