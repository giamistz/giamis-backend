package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.CategoryOfFinding;
import gov.giamis.service.engagement.CategoryOfFindingQueryService;
import gov.giamis.service.engagement.CategoryOfFindingService;
import gov.giamis.service.engagement.dto.CategoryOfFindingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link CategoryOfFinding}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Category of Finding Resource", description = "Operations to manage Category of Findings")
public class CategoryOfFindingResource {

    private final Logger log = LoggerFactory.getLogger(CategoryOfFindingResource.class);

    private static final String ENTITY_NAME = "categoryOfFinding";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryOfFindingService categoryOfFindingService;

    private final CategoryOfFindingQueryService categoryOfFindingQueryService;

    public CategoryOfFindingResource(CategoryOfFindingService categoryOfFindingService, 
            CategoryOfFindingQueryService categoryOfFindingQueryService) {
        this.categoryOfFindingService = categoryOfFindingService;
        this.categoryOfFindingQueryService = categoryOfFindingQueryService;
    }

    /**
     * {@code POST  /category-of-findings} : Create a new categoryOfFinding.
     *
     * @param categoryOfFinding the categoryOfFinding to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categoryOfFinding, or with status {@code 400 (Bad Request)} if the categoryOfFinding has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/category-of-findings")
    @ApiOperation(value = "Create Category of Finding")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createCategoryOfFinding(@Valid @RequestBody CategoryOfFinding categoryOfFinding) throws URISyntaxException {
        log.debug("REST request to save Category of Finding : {}", categoryOfFinding);
        if (categoryOfFinding.getId() != null) {
            throw new BadRequestAlertException("A new Category of Finding already have an ID", ENTITY_NAME, "id exists");
        }

        if (categoryOfFindingService.findByName(categoryOfFinding.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }
        
        CategoryOfFinding result = categoryOfFindingService.save(categoryOfFinding);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /category-of-findings} : Updates an existing categoryOfFinding.
     *
     * @param categoryOfFinding the categoryOfFinding to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categoryOfFinding,
     * or with status {@code 400 (Bad Request)} if the categoryOfFinding is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categoryOfFinding couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/category-of-findings")
    @ApiOperation(value = "Update existing  Category of Finding")
    public CustomApiResponse updateCategoryOfFinding(@Valid @RequestBody CategoryOfFinding categoryOfFinding) throws URISyntaxException {
        log.debug("REST request to update Category of Finding : {}", categoryOfFinding);

        if (categoryOfFinding.getId() == null) {
            throw new BadRequestAlertException("Updated Category of Finding does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (!categoryOfFindingService.findOne(categoryOfFinding.getId()).isPresent()) {
            throw new BadRequestAlertException("Category of Finding with ID: " + categoryOfFinding.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        
        CategoryOfFinding result = categoryOfFindingService.save(categoryOfFinding);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /category-of-findings} : get all the categoryOfFindings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categoryOfFindings in body.
     */
    @GetMapping("/category-of-findings")
    @ApiOperation(value = "Get all CategoryOfFindings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllCategoryOfFindings(CategoryOfFindingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CategoryOfFindings by criteria: {}", criteria);
        Page<CategoryOfFinding> page = categoryOfFindingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /category-of-findings/count} : count all the categoryOfFindings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/category-of-findings/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countCategoryOfFindings(CategoryOfFindingCriteria criteria) {
        log.debug("REST request to count CategoryOfFindings by criteria: {}", criteria);
        return CustomApiResponse.ok(categoryOfFindingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /category-of-findings/:id} : get the "id" categoryOfFinding.
     *
     * @param id the id of the categoryOfFinding to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categoryOfFinding, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/category-of-findings/{id}")
    @ApiOperation("Get a single CategoryOfFinding by ID")
    public CustomApiResponse getCategoryOfFinding(@PathVariable Long id) {
        log.debug("REST request to get CategoryOfFinding : {}", id);
        
        if (!categoryOfFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Category of Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<CategoryOfFinding> categoryOfFinding = categoryOfFindingService.findOne(id);
        return CustomApiResponse.ok(categoryOfFinding);
    }

    /**
     * {@code DELETE  /category-of-findings/:id} : delete the "id" categoryOfFinding.
     *
     * @param id the id of the categoryOfFinding to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/category-of-findings/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteCategoryOfFinding(@PathVariable Long id) {
        log.debug("REST request to delete CategoryOfFinding : {}", id);
        
        if (!categoryOfFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Category of Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        categoryOfFindingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
    
}
