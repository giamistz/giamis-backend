package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementResource;
import gov.giamis.service.dto.EngagementResourceCriteria;
import gov.giamis.service.engagement.EngagementResourceQueryService;
import gov.giamis.service.engagement.EngagementResourceService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementResource}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Resource Resource", description = "Operations to manage Engagement Resource")
public class EngagementResourceResource {

    private final Logger log = LoggerFactory.getLogger(EngagementResourceResource.class);

    private static final String ENTITY_NAME = "engagementResource";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementResourceService engagementResourceService;

    private final EngagementResourceQueryService engagementResourceQueryService;

    public EngagementResourceResource(EngagementResourceService engagementResourceService, EngagementResourceQueryService engagementResourceQueryService) {
        this.engagementResourceService = engagementResourceService;
        this.engagementResourceQueryService = engagementResourceQueryService;
    }

    /**
     * {@code POST  /engagement-resources} : Create a new engagementResource.
     *
     * @param engagementResource the engagementResource to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementResource, or with status {@code 400 (Bad Request)} if the engagementResource has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-resources")
    @ApiOperation(value = "Create Engagement Resource")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementResource(@Valid @RequestBody EngagementResource engagementResource) throws URISyntaxException {
        log.debug("REST request to save Engagement Resource : {}", engagementResource);
        if (engagementResource.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Resource  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementResource result = engagementResourceService.save(engagementResource);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-resources} : Updates an existing engagementResource.
     *
     * @param engagementResource the engagementResource to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementResource,
     * or with status {@code 400 (Bad Request)} if the engagementResource is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementResource couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-resources")
    @ApiOperation(value = "Update existing Engagement Resource")
    public CustomApiResponse updateEngagementResource(@Valid @RequestBody EngagementResource engagementResource) throws URISyntaxException {
        log.debug("REST request to update Engagement Resource : {}", engagementResource);
        if (engagementResource.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Resource does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        EngagementResource result = engagementResourceService.save(engagementResource);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-resources} : get all the engagementResources.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementResources in body.
     */
    @GetMapping("/engagement-resources")
    @ApiOperation(value = "Get all Engagement Resources; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementResources(EngagementResourceCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Resources by criteria: {}", criteria);
        Page<EngagementResource> page = engagementResourceQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-resources/count} : count all the engagementResources.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-resources/count")
    @ApiOperation(value = "Count all Engagement Resources; Optional filters can be applied")
    public CustomApiResponse countEngagementResources(EngagementResourceCriteria criteria) {
        log.debug("REST request to count Engagement Resources by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementResourceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-resources/:id} : get the "id" engagementResource.
     *
     * @param id the id of the engagementResource to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementResource, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-resources/{id}")
    @ApiOperation("Get a single Engagement Resource by ID")
    public CustomApiResponse getEngagementResource(@PathVariable Long id) {
        log.debug("REST request to get Engagement Resource : {}", id);
        
        if (!engagementResourceService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Resource with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementResource> engagementResource = engagementResourceService.findOne(id);
        return CustomApiResponse.ok(engagementResource);
    }

    /**
     * {@code DELETE  /engagement-resources/:id} : delete the "id" engagementResource.
     *
     * @param id the id of the engagementResource to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-resources/{id}")
    @ApiOperation("Delete  a single Engagement Resource by ID")
    public CustomApiResponse deleteEngagementResource(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Resource : {}", id);
        
        if (!engagementResourceService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Resource with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementResourceService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
