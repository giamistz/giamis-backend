package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import gov.giamis.domain.engagement.EngagementTeamMeetingMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementTeamMeeting;
import gov.giamis.domain.engagement.MeetingAgenda;
import gov.giamis.domain.engagement.MeetingDeliverable;
import gov.giamis.service.engagement.EngagementTeamMeetingQueryService;
import gov.giamis.service.engagement.EngagementTeamMeetingService;
import gov.giamis.service.engagement.dto.EngagementTeamMeetingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link EngagementTeamMeeting}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Team Meeting Resource", description = "Operations to manage Engagement Team Meeting")
public class EngagementTeamMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EngagementTeamMeetingResource.class);

    private static final String ENTITY_NAME = "engagementTeamMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementTeamMeetingService engagementTeamMeetingService;

    private final EngagementTeamMeetingQueryService engagementTeamMeetingQueryService;

    public EngagementTeamMeetingResource(
            EngagementTeamMeetingService engagementTeamMeetingService,
            EngagementTeamMeetingQueryService engagementTeamMeetingQueryService) {
        this.engagementTeamMeetingService = engagementTeamMeetingService;
        this.engagementTeamMeetingQueryService = engagementTeamMeetingQueryService;
    }

    /**
     * {@code POST  /engagement-meetings} : Create a new engagementTeamMeeting.
     *
     * @param engagementTeamMeeting the engagementTeamMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementTeamMeeting, or with status {@code 400 (Bad Request)} if the engagementTeamMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-team-meetings")
    @ApiOperation(value = "Create Engagement Team Meeting")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackOn = {Exception.class, DataIntegrityViolationException.class, RuntimeException.class, ConstraintViolationException.class})
    public CustomApiResponse createEngagementTeamMeeting(@Valid @RequestBody EngagementTeamMeeting engagementTeamMeeting) throws URISyntaxException {
        log.debug("REST request to save Engagement Team Meeting : {}", engagementTeamMeeting);
        EngagementTeamMeeting result = null;
        if (engagementTeamMeeting.getId() != null) {
            throw new BadRequestAlertException("A new engagementTeamMeeting  already have an ID", ENTITY_NAME, "id exists");
        }

        if (engagementTeamMeeting.getEngagement() == null) {
            throw new BadRequestAlertException("Engagement does not Exist", ENTITY_NAME, "Does not exists");
        }

        if((engagementTeamMeetingService.countEngagementTeamMeetingByEngagementId(engagementTeamMeeting.getEngagement().getId()) == 0) && (engagementTeamMeeting.getId() == null)) {
        		result = engagementTeamMeetingService.save(engagementTeamMeeting);

        }else {
        	result = engagementTeamMeetingService.findByEngagementId(engagementTeamMeeting.getEngagement().getId());
        }
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-meetings} : Updates an existing engagementTeamMeeting.
     *
     * @param engagementTeamMeeting the engagementTeamMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementTeamMeeting,
     * or with status {@code 400 (Bad Request)} if the engagementTeamMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementTeamMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-team-meetings")
    @ApiOperation(value = "Update existing Engagement Team Meeting")
    public CustomApiResponse updateEngagementTeamMeeting(@Valid @RequestBody EngagementTeamMeeting engagementTeamMeeting) throws URISyntaxException {
        log.debug("REST request to update Engagement Team Meeting : {}", engagementTeamMeeting);
        if (engagementTeamMeeting.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Team Meeting does not have an ID", ENTITY_NAME,
                    "id not exists");
        }

        EngagementTeamMeeting result = engagementTeamMeetingService.save(engagementTeamMeeting);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-meetings} : get all the engagementTeamMeetings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementTeamMeetings in body.
     */
    @GetMapping("/engagement-team-meetings")
    @ApiOperation(value = "Get all Engagement Team Meetings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementTeamMeetings(EngagementTeamMeetingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementTeamMeetings by criteria: {}", criteria);
        Page<EngagementTeamMeeting> page = engagementTeamMeetingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /engagement-meetings/:id} : get the "id" engagementTeamMeeting.
     *
     * @param id the id of the engagementTeamMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementTeamMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-team-meetings/{id}")
    @ApiOperation("Get a single Engagement Team Meeting by ID")
    public CustomApiResponse getEngagementTeamMeeting(@PathVariable Long id) {
        log.debug("REST request to get Engagement Team Meeting : {}", id);

        if (!engagementTeamMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Team Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementTeamMeeting> engagementTeamMeeting = engagementTeamMeetingService.findOne(id);
        return CustomApiResponse.ok(engagementTeamMeeting);
    }

    /**
     * {@code DELETE  /engagement-meetings/:id} : delete the "id" engagementTeamMeeting.
     *
     * @param id the id of the engagementTeamMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-team-meetings/{id}")
    @ApiOperation("Delete  a single Engagement Team Meeting by ID")
    public CustomApiResponse deleteEngagementTeamMeeting(@PathVariable Long id) {
        log.debug("REST request to delete Engagement Team Meeting : {}", id);

        if (!engagementTeamMeetingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Team Meeting with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementTeamMeetingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-team-meeting-by-engagement-id/{id}")
    public CustomApiResponse getEngagementTeamMeetingByEngagementId(@PathVariable Long id) {
        log.debug("REST Get Engagement team meeting by engagement ID : {}", id);
        EngagementTeamMeeting engagementTeamMeetingById = engagementTeamMeetingService.findByEngagementId(id);
        return CustomApiResponse.ok(engagementTeamMeetingById);
    }

    @GetMapping("/engagement-team-meetings/{id}/deliverables")
    public CustomApiResponse getDeliverable(@PathVariable Long id) {
        return CustomApiResponse.ok( engagementTeamMeetingService.getDeliverables(id));
    }

    @PostMapping("/engagement-team-meetings/deliverables")
    public CustomApiResponse createDeliverable(@RequestBody MeetingDeliverable deliverable) {
        engagementTeamMeetingService.saveDeliverable(deliverable);
        return CustomApiResponse.ok("Deliverable".concat(Constants.CREATE_SUCCESS));
    }

    @PutMapping("/engagement-team-meetings/deliverables")
    public CustomApiResponse UpdateDeliverable(@RequestBody MeetingDeliverable deliverable) {
        engagementTeamMeetingService.saveDeliverable(deliverable);
        return CustomApiResponse.ok("Deliverable".concat(Constants.UPDATE_SUCCESS));
    }

    @DeleteMapping("/engagement-team-meetings/deliverables/{id}")
    public CustomApiResponse DeleteDeliverable(@PathVariable Long id) {
        engagementTeamMeetingService.deleteDeliverable(id);
        return CustomApiResponse.ok("Deliverable".concat(Constants.UPDATE_SUCCESS));
    }

    @GetMapping("/engagement-team-meetings/{id}/agendas")
    public CustomApiResponse getAgenda(@PathVariable Long id) {
        return CustomApiResponse.ok(engagementTeamMeetingService.getAgendas(id));
    }

    @PostMapping("/engagement-team-meetings/agendas")
    public CustomApiResponse createAgenda(@RequestBody MeetingAgenda agenda) {
        engagementTeamMeetingService.saveAgenda(agenda);
        return CustomApiResponse.ok("MeetingAgenda ".concat(Constants.CREATE_SUCCESS));
    }

    @PutMapping("/engagement-team-meetings/agendas")
    public CustomApiResponse updateAgenda(@RequestBody MeetingAgenda agenda) {
        engagementTeamMeetingService.saveAgenda(agenda);
        return CustomApiResponse.ok("MeetingAgenda ".concat(Constants.CREATE_SUCCESS));
    }

    @DeleteMapping("/engagement-team-meetings/agendas/{id}")
    public CustomApiResponse deleteAgenda(@PathVariable Long id) {
        engagementTeamMeetingService.deleteAgenda(id);
        return CustomApiResponse.ok("MeetingAgenda ".concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-team-meetings/{id}/members")
    public CustomApiResponse getMember(@PathVariable Long id) {
        return CustomApiResponse.ok(engagementTeamMeetingService.getMembers(id));
    }

    @PostMapping("/engagement-team-meetings/members")
    public CustomApiResponse createMember(@RequestBody EngagementTeamMeetingMember member) {
        engagementTeamMeetingService.saveMember(member);
        return CustomApiResponse.ok("Meeting member ".concat(Constants.CREATE_SUCCESS));
    }

    @PutMapping("/engagement-team-meetings/member")
    public CustomApiResponse updateMember(@RequestBody EngagementTeamMeetingMember member) {
        engagementTeamMeetingService.saveMember(member);
        return CustomApiResponse.ok("Meeting member ".concat(Constants.CREATE_SUCCESS));
    }

    @DeleteMapping("/engagement-team-meetings/members/{id}")
    public CustomApiResponse deleteMember(@PathVariable Long id) {
        engagementTeamMeetingService.deleteMember(id);
        return CustomApiResponse.ok("Meeting member ".concat(Constants.DELETE_SUCCESS));
    }


}
