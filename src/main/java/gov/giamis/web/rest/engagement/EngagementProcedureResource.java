package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.service.dto.EngagementProcedureCriteria;
import gov.giamis.service.engagement.EngagementProcedureQueryService;
import gov.giamis.service.engagement.EngagementProcedureService;
import gov.giamis.service.engagement.SpecificObjectiveService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.EngagementProcedure}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Procedure Resource", description = "Operations to manage Engagement Procedure")
public class EngagementProcedureResource {

    private final Logger log = LoggerFactory.getLogger(EngagementProcedureResource.class);

    private static final String ENTITY_NAME = "engagementProcedure";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementProcedureService engagementProcedureService;

    private final SpecificObjectiveService specificObjectiveService;

    private final EngagementProcedureQueryService engagementProcedureQueryService;

    public EngagementProcedureResource(EngagementProcedureService engagementProcedureService,
    		EngagementProcedureQueryService engagementProcedureQueryService,
    		SpecificObjectiveService specificObjectiveService) {
        this.engagementProcedureService = engagementProcedureService;
        this.engagementProcedureQueryService = engagementProcedureQueryService;
        this.specificObjectiveService = specificObjectiveService;
    }

    /**
     * {@code POST  /engagement-procedures} : Create a new engagementProcedure.
     *
     * @param engagementProcedure the engagementProcedure to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementProcedure, or with status {@code 400 (Bad Request)} if the engagementProcedure has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-procedures")
    @ApiOperation(value = "Create Engagement Procedure")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementProcedure(@Valid @RequestBody EngagementProcedure engagementProcedure) throws URISyntaxException {
        log.debug("REST request to save Engagement Procedure : {}", engagementProcedure);
        if (engagementProcedure.getId() != null) {
            throw new BadRequestAlertException("A new Engagement Procedure  already have an ID", ENTITY_NAME, "id exists");
        }
        EngagementProcedure result = engagementProcedureService.save(engagementProcedure);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-procedures} : Updates an existing engagementProcedure.
     *
     * @param engagementProcedure the engagementProcedure to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementProcedure,
     * or with status {@code 400 (Bad Request)} if the engagementProcedure is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementProcedure couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-procedures")
    @ApiOperation(value = "Update existing Engagement Procedure")
    public CustomApiResponse updateEngagementProcedure(@Valid @RequestBody EngagementProcedure engagementProcedure) throws URISyntaxException {
        log.debug("REST request to update EngagementProcedure : {}", engagementProcedure);
        if (engagementProcedure.getId() == null) {
            throw new BadRequestAlertException("Updated Engagement Procedure does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (engagementProcedure.getWprf() == null) {
            engagementProcedure.setWprf(
                engagementProcedureService.findOne(engagementProcedure.getId()).get().getWprf());
        }
        engagementProcedureService.update(engagementProcedure);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }

    /**
     * {@code GET  /engagement-procedures} : get all the engagementProcedures.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementProcedures in body.
     */
    @GetMapping("/engagement-procedures")
    @ApiOperation(value = "Get all Engagement Procedures; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementProcedures(EngagementProcedureCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Engagement Procedures by criteria: {}", criteria);
        Page<EngagementProcedure> page = engagementProcedureQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /engagement-procedures/count} : count all the engagementProcedures.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/engagement-procedures/count")
    @ApiOperation(value = "Count all Engagement Procedures; Optional filters can be applied")
    public CustomApiResponse countEngagementProcedures(EngagementProcedureCriteria criteria) {
        log.debug("REST request to count EngagementProcedures by criteria: {}", criteria);
        return CustomApiResponse.ok(engagementProcedureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /engagement-procedures/:id} : get the "id" engagementProcedure.
     *
     * @param id the id of the engagementProcedure to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementProcedure, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-procedures/{id}")
    @ApiOperation("Get a single Engagement Procedure by ID")
    public CustomApiResponse getEngagementProcedure(@PathVariable Long id) {
        log.debug("REST request to get Engagement Procedure : {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<EngagementProcedure> engagementProcedure = engagementProcedureService.findOne(id);
        return CustomApiResponse.ok(engagementProcedure);
    }

    /**
     * {@code DELETE  /engagement-procedures/:id} : delete the "id" engagementProcedure.
     *
     * @param id the id of the engagementProcedure to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-procedures/{id}")
    @ApiOperation("Delete  a single Engagement Procedure by ID")
    public CustomApiResponse deleteEngagementProcedure(@PathVariable Long id) {
        log.debug("REST request to delete EngagementProcedure : {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementProcedureService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-procedures/by-objective/{id}")
    @ApiOperation("Get  Engagement Procedure by Specific Objective ID")
    public CustomApiResponse getEngagementProcedureBySpecificObjectiveId(@PathVariable Long id) {
        log.debug("REST request to get Engagement Procedure : {}", id);

        if (!specificObjectiveService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Specific Objective with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<EngagementProcedure> engagementProcedure = engagementProcedureService.findBySpecificObjectiveId(id);
        return CustomApiResponse.ok(engagementProcedure);
    }
}
