package gov.giamis.web.rest.engagement;

import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import gov.giamis.service.dto.FindingCriteria;
import gov.giamis.service.engagement.FindingQueryService;
import gov.giamis.service.engagement.FindingService;
import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.Finding;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link gov.giamis.domain.engagement.Finding}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Finding Resource", description = "Operations to manage Finding Resource")
public class FindingResource {

    private final Logger log = LoggerFactory.getLogger(FindingResource.class);

    private static final String ENTITY_NAME = "finding";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FindingService findingService;

    private final FindingQueryService findingQueryService;

    public FindingResource(FindingService findingService, FindingQueryService findingQueryService) {
        this.findingService = findingService;
        this.findingQueryService = findingQueryService;
    }

    /**
     * {@code POST  /findings} : Create a new finding.
     *
     * @param finding the finding to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new finding, or with status {@code 400 (Bad Request)} if the finding has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/findings")
    @ApiOperation(value = "Create Finding")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createFinding(@Valid @RequestBody Finding finding) throws URISyntaxException {
        log.debug("REST request to save Finding : {}", finding);
        if (finding.getId() != null) {
            throw new BadRequestAlertException("A new Finding  already have an ID", ENTITY_NAME, "id exists");
        }
        Finding result = findingService.save(finding);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /findings} : Updates an existing finding.
     *
     * @param finding the finding to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated finding,
     * or with status {@code 400 (Bad Request)} if the finding is not valid,
     * or with status {@code 500 (Internal Server Error)} if the finding couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/findings")
    @ApiOperation(value = "Update existing Finding")
    public CustomApiResponse updateFinding(@Valid @RequestBody Finding finding) throws URISyntaxException {
        log.debug("REST request to update Finding : {}", finding);
        if (finding.getId() == null) {
            throw new BadRequestAlertException("Updated Finding does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        Finding result = findingService.save(finding);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /findings} : get all the findings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of findings in body.
     */
    @GetMapping("/findings")
    @ApiOperation(value = "Get all Findings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllFindings(FindingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Findings by criteria: {}", criteria);
        Page<Finding> page = findingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /findings/count} : count all the findings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/findings/count")
    @ApiOperation(value = "Count all Findings; Optional filters can be applied")
    public CustomApiResponse countFindings(FindingCriteria criteria) {
        log.debug("REST request to count Findings by criteria: {}", criteria);
        return CustomApiResponse.ok(findingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /findings/:id} : get the "id" finding.
     *
     * @param id the id of the finding to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the finding, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/findings/{id}")
    @ApiOperation("Get a single Finding by ID")
    public CustomApiResponse getFinding(@PathVariable Long id) {
        log.debug("REST request to get Finding : {}", id);
        
        if (!findingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<Finding> finding = findingService.findOne(id);
        return CustomApiResponse.ok(finding);
    }

    /**
     * {@code DELETE  /findings/:id} : delete the "id" finding.
     *
     * @param id the id of the finding to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/findings/{id}")
    @ApiOperation("Delete  a single Finding by ID")
    public CustomApiResponse deleteFinding(@PathVariable Long id) {
        log.debug("REST request to delete Finding : {}", id);
        
        if (!findingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        findingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
