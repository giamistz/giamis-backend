package gov.giamis.web.rest.engagement;

import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.SubCategoryOfFinding;
import gov.giamis.service.engagement.SubCategoryOfFindingQueryService;
import gov.giamis.service.engagement.SubCategoryOfFindingService;
import gov.giamis.service.engagement.dto.SubCategoryOfFindingCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * REST controller for managing {@link SubCategoryOfFinding}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Sub Category of Finding Resource", description = "Operations to manage Sub Category of Findings")
public class SubCategoryOfFindingResource {

    private final Logger log = LoggerFactory.getLogger(SubCategoryOfFindingResource.class);

    private static final String ENTITY_NAME = "subCategoryOfFinding";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubCategoryOfFindingService subCategoryOfFindingService;

    private final SubCategoryOfFindingQueryService subCategoryOfFindingQueryService;

    public SubCategoryOfFindingResource(SubCategoryOfFindingService subCategoryOfFindingService, 
            SubCategoryOfFindingQueryService subCategoryOfFindingQueryService) {
        this.subCategoryOfFindingService = subCategoryOfFindingService;
        this.subCategoryOfFindingQueryService = subCategoryOfFindingQueryService;
    }

    /**
     * {@code POST  /subcategory-of-findings} : Create a new subCategoryOfFinding.
     *
     * @param subCategoryOfFinding the subCategoryOfFinding to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subCategoryOfFinding, or with status {@code 400 (Bad Request)} if the subCategoryOfFinding has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/subcategory-of-findings")
    @ApiOperation(value = "Create Sub Category of Finding")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createSubCategoryOfFinding(@Valid @RequestBody SubCategoryOfFinding subCategoryOfFinding) throws URISyntaxException {
        log.debug("REST request to save Sub Category of Finding : {}", subCategoryOfFinding);
        if (subCategoryOfFinding.getId() != null) {
            throw new BadRequestAlertException("A new Sub Category of Finding already have an ID", ENTITY_NAME, "id exists");
        }

        if (subCategoryOfFindingService.findByName(subCategoryOfFinding.getName()) != null) {
            throw new DataIntegrityViolationException("Duplicate Entry; Name already exists");
        }
        
        SubCategoryOfFinding result = subCategoryOfFindingService.save(subCategoryOfFinding);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /subcategory-of-findings} : Updates an existing subCategoryOfFinding.
     *
     * @param subCategoryOfFinding the subCategoryOfFinding to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subCategoryOfFinding,
     * or with status {@code 400 (Bad Request)} if the subCategoryOfFinding is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subCategoryOfFinding couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/subcategory-of-findings")
    @ApiOperation(value = "Update existing  Sub Category of Finding")
    public CustomApiResponse updateSubCategoryOfFinding(@Valid @RequestBody SubCategoryOfFinding subCategoryOfFinding) throws URISyntaxException {
        log.debug("REST request to update Sub Category of Finding : {}", subCategoryOfFinding);

        if (subCategoryOfFinding.getId() == null) {
            throw new BadRequestAlertException("Updated Sub Category of Finding does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (!subCategoryOfFindingService.findOne(subCategoryOfFinding.getId()).isPresent()) {
            throw new BadRequestAlertException("Sub Category of Finding with ID: " + subCategoryOfFinding.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }
        
        SubCategoryOfFinding result = subCategoryOfFindingService.save(subCategoryOfFinding);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /subcategory-of-findings} : get all the subCategoryOfFindings.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subCategoryOfFindings in body.
     */
    @GetMapping("/subcategory-of-findings")
    @ApiOperation(value = "Get all SubCategoryOfFindings; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllSubCategoryOfFindings(SubCategoryOfFindingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SubCategoryOfFindings by criteria: {}", criteria);
        Page<SubCategoryOfFinding> page = subCategoryOfFindingQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /subcategory-of-findings/count} : count all the subCategoryOfFindings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/subcategory-of-findings/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countSubCategoryOfFindings(SubCategoryOfFindingCriteria criteria) {
        log.debug("REST request to count SubCategoryOfFindings by criteria: {}", criteria);
        return CustomApiResponse.ok(subCategoryOfFindingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /subcategory-of-findings/:id} : get the "id" subCategoryOfFinding.
     *
     * @param id the id of the subCategoryOfFinding to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subCategoryOfFinding, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/subcategory-of-findings/{id}")
    @ApiOperation("Get a single SubCategoryOfFinding by ID")
    public CustomApiResponse getSubCategoryOfFinding(@PathVariable Long id) {
        log.debug("REST request to get SubCategoryOfFinding : {}", id);
        
        if (!subCategoryOfFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Sub Category of Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<SubCategoryOfFinding> subCategoryOfFinding = subCategoryOfFindingService.findOne(id);
        return CustomApiResponse.ok(subCategoryOfFinding);
    }

    /**
     * {@code DELETE  /subcategory-of-findings/:id} : delete the "id" subCategoryOfFinding.
     *
     * @param id the id of the subCategoryOfFinding to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/subcategory-of-findings/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteSubCategoryOfFinding(@PathVariable Long id) {
        log.debug("REST request to delete SubCategoryOfFinding : {}", id);
        
        if (!subCategoryOfFindingService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Sub Category of Finding with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        subCategoryOfFindingService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
    
}
