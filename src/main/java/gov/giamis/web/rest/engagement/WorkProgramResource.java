package gov.giamis.web.rest.engagement;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.WorkProgram;
import gov.giamis.service.engagement.EngagementProcedureService;
import gov.giamis.service.engagement.WorkProgramQueryService;
import gov.giamis.service.engagement.WorkProgramService;
import gov.giamis.service.engagement.dto.WorkProgramCriteria;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.errors.DataIntegrityViolationException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link WorkProgram}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Work Program Resource", description = "Operations to manage Work Programs")
public class WorkProgramResource {

    private final Logger log = LoggerFactory.getLogger(WorkProgramResource.class);

    private static final String ENTITY_NAME = "workProgram";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkProgramService workProgramService;

    private final EngagementProcedureService engagementProcedureService;

    private final WorkProgramQueryService workProgramQueryService;

    public WorkProgramResource(WorkProgramService workProgramService,
    		WorkProgramQueryService workProgramQueryService,
    		EngagementProcedureService engagementProcedureService) {
        this.workProgramService = workProgramService;
        this.workProgramQueryService = workProgramQueryService;
        this.engagementProcedureService = engagementProcedureService;
    }

    /**
     * {@code POST  /work-programs} : Create a new workProgram.
     *
     * @param workProgram the workProgram to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workProgram, or with status {@code 400 (Bad Request)} if the workProgram has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/work-programs")
    @ApiOperation(value = "Create Work Program")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createWorkProgram(@Valid @RequestBody WorkProgram workProgram) throws URISyntaxException {
        log.debug("REST request to save Work Program : {}", workProgram);
        if (workProgram.getId() != null) {
            throw new BadRequestAlertException("A new Work Program already have an ID", ENTITY_NAME, "id exists");
        }

//        if (workProgramService.findByCode(workProgram.getCode()) != null) {
//			throw new DataIntegrityViolationException("Duplicate Entry; Code already exists");
//		}
        WorkProgram result = workProgramService.save(workProgram);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /work-programs} : Updates an existing workProgram.
     *
     * @param workProgram the workProgram to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workProgram,
     * or with status {@code 400 (Bad Request)} if the workProgram is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workProgram couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/work-programs")
    @ApiOperation(value = "Update existing  Work Program")
    public CustomApiResponse updateWorkProgram(@Valid @RequestBody WorkProgram workProgram) throws URISyntaxException {
        log.debug("REST request to update Work Program : {}", workProgram);

        if (workProgram.getId() == null) {
            throw new BadRequestAlertException("Updated Work Program does not have an ID", ENTITY_NAME,
                    "id not exists");
        }
        if (!workProgramService.findOne(workProgram.getId()).isPresent()) {
            throw new BadRequestAlertException("Work Program with ID: " + workProgram.getId() + " not found", ENTITY_NAME,
                    "id not exists");
        }

        workProgramService.updateWorkProgram(workProgram.getName(),workProgram.getId());
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS));
    }

    /**
     * {@code GET  /work-programs} : get all the workPrograms.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workPrograms in body.
     */
    @GetMapping("/work-programs")
    @ApiOperation(value = "Get all WorkPrograms; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllWorkPrograms(WorkProgramCriteria criteria, Pageable pageable) {
        log.debug("REST request to get WorkPrograms by criteria: {}", criteria);
        Page<WorkProgram> page = workProgramQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /work-programs/count} : count all the workPrograms.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/work-programs/count")
    @ApiOperation(value = "Count all risk ratings; Optional filters can be applied")
    public CustomApiResponse countWorkPrograms(WorkProgramCriteria criteria) {
        log.debug("REST request to count WorkPrograms by criteria: {}", criteria);
        return CustomApiResponse.ok(workProgramQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /work-programs/:id} : get the "id" workProgram.
     *
     * @param id the id of the workProgram to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workProgram, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/work-programs/{id}")
    @ApiOperation("Get a single WorkProgram by ID")
    public CustomApiResponse getWorkProgram(@PathVariable Long id) {
        log.debug("REST request to get WorkProgram : {}", id);

        if (!workProgramService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Work Program with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        Optional<WorkProgram> workProgram = workProgramService.findOne(id);
        return CustomApiResponse.ok(workProgram);
    }

    /**
     * {@code DELETE  /work-programs/:id} : delete the "id" workProgram.
     *
     * @param id the id of the workProgram to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/work-programs/{id}")
    @ApiOperation("Deleting a single risk rating")
    public CustomApiResponse deleteWorkProgram(@PathVariable Long id) {
        log.debug("REST request to delete WorkProgram : {}", id);

        if (!workProgramService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Work Program with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        workProgramService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/work-programs/byProcedure")
    @ApiOperation("Get  Work Program by engagement Procedure ID")
    public CustomApiResponse getWorkProgramByEngagementProcedure(@RequestParam Long id) {
        log.debug("REST request to get Work Program by Engagement Procedure ID : {}", id);

        if (!engagementProcedureService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Procedure with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        List<WorkProgram> workProgram = workProgramService.findByEngagementProcedure(id);
        return CustomApiResponse.ok(workProgram);
    }
}
