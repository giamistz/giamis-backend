package gov.giamis.web.rest.engagement;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.security.NoAthorization;
import gov.giamis.service.engagement.EngagementMemberQueryService;
import gov.giamis.service.engagement.EngagementMemberService;
import gov.giamis.service.engagement.dto.EngagementMemberCriteria;
import gov.giamis.service.report.EngagementTemplateService;
import gov.giamis.service.setup.FileResourceService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.jasperreports.engine.JRException;

/**
 * REST controller for managing {@link EngagementMember}.
 */
@RestController
@RequestMapping(Constants.API_V1)
@Api(value = "Engagement Member Resource", description = "Operations to manage Engagement Member")
public class EngagementMemberResource {

    private final Logger log = LoggerFactory.getLogger(EngagementMemberResource.class);

    private static final String ENTITY_NAME = "engagementMember";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EngagementMemberService engagementMemberService;

    private final EngagementMemberQueryService engagementMemberQueryService;

    @Autowired
    private  JHipsterProperties jHipsterProperties;

    @Autowired
    private EngagementTemplateService engagementTemplateService;


    @Autowired
    private FileResourceService fileResourceService;


    public EngagementMemberResource(
        EngagementMemberService engagementMemberService,
        EngagementMemberQueryService engagementMemberQueryService) {
        this.engagementMemberService = engagementMemberService;
        this.engagementMemberQueryService = engagementMemberQueryService;
    }

    /**
     * {@code POST  /engagement-members} : Create a new engagementMember.
     *
     * @param engagementMember the engagementMember to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new engagementMember, or with status {@code 400 (Bad Request)} if the engagementMember has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/engagement-members")
    @ApiOperation(value = "Create Engagement Member")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomApiResponse createEngagementMember(@Valid @RequestBody EngagementMember engagementMember) {
        log.debug("REST request to save EngagementMember : {}", engagementMember);
        if (engagementMember.getId() != null) {
            throw new BadRequestAlertException("A new engagementMember cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EngagementMember result = engagementMemberService.save(engagementMember);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /engagement-members} : Updates an existing engagementMember.
     *
     * @param engagementMember the engagementMember to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated engagementMember,
     * or with status {@code 400 (Bad Request)} if the engagementMember is not valid,
     * or with status {@code 500 (Internal Server Error)} if the engagementMember couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/engagement-members")
    @ApiOperation(value = "Update existing Engagement Member")
    public CustomApiResponse updateEngagementMember(@Valid @RequestBody EngagementMember engagementMember) throws URISyntaxException {
        log.debug("REST request to update EngagementMember : {}", engagementMember);
        if (engagementMember.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EngagementMember result = engagementMemberService.save(engagementMember);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /engagement-members} : get all the engagementMembers.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of engagementMembers in body.
     */
    @GetMapping("/engagement-members")
    @ApiOperation(value = "Get all Engagement Member; with pagination, optional filters by fields can be applied", response = CustomApiResponse.class)
    public CustomApiResponse getAllEngagementMembers(EngagementMemberCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EngagementMembers by criteria: {}", criteria);
        Page<EngagementMember> page = engagementMemberQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
     * {@code GET  /engagement-members/:id} : get the "id" engagementMember.
     *
     * @param id the id of the engagementMember to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the engagementMember, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/engagement-members/{id}")
    @ApiOperation("Get a single Engagement Member by ID")
    public CustomApiResponse getEngagementMember(@PathVariable Long id) {
        log.debug("REST request to get EngagementMember : {}", id);
        Optional<EngagementMember> engagementMember = engagementMemberService.findOne(id);
        return CustomApiResponse.ok(engagementMember);
    }

    /**
     * {@code DELETE  /engagement-members/:id} : delete the "id" engagementMember.
     *
     * @param id the id of the engagementMember to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/engagement-members/{id}")
    @ApiOperation("Delete  a single Engagement Member by ID")
    public CustomApiResponse deleteEngagementMember(@PathVariable Long id) {
        log.debug("REST request to delete EngagementMember : {}", id);
        engagementMemberService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    @GetMapping("/engagement-members/{id}/create-letter")
    public CustomApiResponse createLetter(@PathVariable Long id) {
       EngagementMember member = engagementMemberService.findOne(id).get();
       FileResource letter = engagementMemberService.createLetter(member);
       return CustomApiResponse.ok("INVITATION LETTER".concat(Constants.CREATE_SUCCESS), letter);
    }

    @GetMapping("/engagement-members/{id}/notify")
    public CustomApiResponse notify(@PathVariable Long id) {
       EngagementMember member = engagementMemberService.findOne(id).get();
       engagementMemberService.sendInvitationLetter(member);
       return CustomApiResponse.ok("INVITATION LETTER WILL BE SENT");
    }

    @GetMapping("/engagement-members/notify-all/{engagementId}")
    public CustomApiResponse notifyAll(@PathVariable Long engagementId) {
        if (!engagementMemberService.hasTeamLead(engagementId)) {
            throw new BadRequestAlertException("NO TEAM LEAD FOR ENGAGEMENT", "ENGAGEMENT_MEMBER", "");
        }
        List<EngagementMember> members = engagementMemberQueryService.findAllByEngagement(engagementId);
        members.forEach(m -> {
            engagementMemberService.createLetter(m);
            engagementMemberService.sendInvitationLetter(m);
        });
       return CustomApiResponse.ok("INVITATION LETTER SENT");
    }

    @GetMapping("/engagement-members/nda/{id}")
    @NoAthorization
    public String getNdaForm(@PathVariable Long id) {
        EngagementMember member = engagementMemberService.findOne(id).get();
        return engagementTemplateService.getNdaForm(member);
    }

    @GetMapping("/engagement-members/nda/agree/{id}")
    @NoAthorization
    public ModelAndView agreeNda(@PathVariable("id") Long id, ModelMap model) throws SQLException, IOException, JRException {
        EngagementMember member = engagementMemberService.findOne(id).get();
        if (member.isAcceptedNda() != null && member.isAcceptedNda()) {
            return new ModelAndView("redirect:/");
        }

        member.setAcceptedNda(true);
        member.setAcceptanceDate(LocalDate.now());
        File nda = engagementTemplateService.createNda(member.getId());
        FileResource fileResource = fileResourceService.upload(
            nda,
            "nda_"+member.getUser().getEmail(),
            "engagement"+member.getEngagement().getId());
        member.setNdaFileResource(fileResource);
        engagementMemberService.save(member);

        model.addAttribute("nda", "SUCCESSFULLY YOU WILL BE REDIRECTED");
        return new ModelAndView("redirect:"+jHipsterProperties
            .getMail()
            .getBaseUrl(), model);
    }

    @GetMapping("/engagement-members/nda/decline/{id}")
    @NoAthorization
    public String declineNda(@PathVariable("id") Long id){
        engagementMemberService.delete(id);
        return "Success Decline";
    }

    @PutMapping("/approve-team-members")
    @ApiOperation("Approve a single Engagement Member by ID")
    public CustomApiResponse deleteEngagementProcedure(@RequestParam Long id) {
        log.debug("REST request to approve EngagementMember by ID : {}", id);

        if (!engagementMemberService.findOne(id).isPresent()) {
            throw new BadRequestAlertException("Engagement Member with ID: " + id + " not found", ENTITY_NAME,
                    "id not exists");
        }
        engagementMemberService.approveEngagementMember(LocalDate.now(), id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.APPROVE_SUCCESS));
    }
}
