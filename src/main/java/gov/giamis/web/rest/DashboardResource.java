package gov.giamis.web.rest;

import gov.giamis.config.Constants;
import gov.giamis.dto.AuditableAreaQuarterValueDTO;
import gov.giamis.dto.QuarterValueDTO;
import gov.giamis.security.NoAthorization;
import gov.giamis.service.DashboardService;
import gov.giamis.web.rest.response.CustomApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(Constants.API_V1)
public class DashboardResource {

    private final DashboardService service;

    public DashboardResource(DashboardService service) {
        this.service = service;
    }


    @GetMapping("dashboard/quarter/engagements/by-year/{financialYearId}")
    @NoAthorization
    public CustomApiResponse engagementByQuarter(@PathVariable Long financialYearId) {
        return CustomApiResponse.ok(service.engByUserOrgUnitByFnByQtr(financialYearId));
    }

    @GetMapping("dashboard/auditable-area/quarter/engagements/by-year/{financialYearId}")
    @NoAthorization
    public CustomApiResponse getByAreaByQrt(@PathVariable Long financialYearId) {
        return CustomApiResponse.ok(service.getByAreaByQrt(financialYearId));
    }

    @GetMapping("dashboard/quarter/recommendation/by-year/{financialYearId}")
    @NoAthorization
    public CustomApiResponse getRecommByQrt(@PathVariable Long financialYearId) {
        return CustomApiResponse.ok(service.recommByStatus(financialYearId));
    }

    @GetMapping("dashboard/auditors")
    @NoAthorization
    public CustomApiResponse auditors() {
        return CustomApiResponse.ok(service.auditors());
    }
}
