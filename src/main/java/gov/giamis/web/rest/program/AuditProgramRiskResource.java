package gov.giamis.web.rest.program;

import gov.giamis.config.Constants;
import gov.giamis.domain.program.AuditProgramRisk;
import gov.giamis.service.program.AuditProgramRiskService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.program.dto.AuditProgramRiskCriteria;
import gov.giamis.service.program.AuditProgramRiskQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditProgramRisk}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuditProgramRiskResource {

    private final Logger log = LoggerFactory.getLogger(AuditProgramRiskResource.class);

    private static final String ENTITY_NAME = "auditProgramRisk";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditProgramRiskService auditProgramRiskService;

    private final AuditProgramRiskQueryService auditProgramRiskQueryService;

    public AuditProgramRiskResource(AuditProgramRiskService auditProgramRiskService, AuditProgramRiskQueryService auditProgramRiskQueryService) {
        this.auditProgramRiskService = auditProgramRiskService;
        this.auditProgramRiskQueryService = auditProgramRiskQueryService;
    }

    /**
     * {@code POST  /audit-program-risks} : Create a new auditProgramRisk.
     *
     * @param auditProgramRisk the auditProgramRisk to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditProgramRisk, or with status {@code 400 (Bad Request)} if the auditProgramRisk has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-program-risks")
    public CustomApiResponse createAuditProgramRisk(@Valid @RequestBody AuditProgramRisk auditProgramRisk) throws URISyntaxException {
        log.debug("REST request to save AuditProgramRisk : {}", auditProgramRisk);
        if (auditProgramRisk.getId() != null) {
            throw new BadRequestAlertException("A new auditProgramRisk cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditProgramRisk result = auditProgramRiskService.save(auditProgramRisk);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-program-risks} : Updates an existing auditProgramRisk.
     *
     * @param auditProgramRisk the auditProgramRisk to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditProgramRisk,
     * or with status {@code 400 (Bad Request)} if the auditProgramRisk is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditProgramRisk couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-program-risks")
    public CustomApiResponse updateAuditProgramRisk(@Valid @RequestBody AuditProgramRisk auditProgramRisk) throws URISyntaxException {
        log.debug("REST request to update AuditProgramRisk : {}", auditProgramRisk);
        if (auditProgramRisk.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditProgramRisk result = auditProgramRiskService.save(auditProgramRisk);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-program-risks} : get all the auditProgramRisks.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditProgramRisks in body.
     */
    @GetMapping("/audit-program-risks")
    public CustomApiResponse getAllAuditProgramRisks(AuditProgramRiskCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditProgramRisks by criteria: {}", criteria);
        Page<AuditProgramRisk> page = auditProgramRiskQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-program-risks/count} : count all the auditProgramRisks.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-program-risks/count")
    public CustomApiResponse countAuditProgramRisks(AuditProgramRiskCriteria criteria) {
        log.debug("REST request to count AuditProgramRisks by criteria: {}", criteria);
        return CustomApiResponse.ok(auditProgramRiskQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-program-risks/:id} : get the "id" auditProgramRisk.
     *
     * @param id the id of the auditProgramRisk to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditProgramRisk, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-program-risks/{id}")
    public CustomApiResponse getAuditProgramRisk(@PathVariable Long id) {
        log.debug("REST request to get AuditProgramRisk : {}", id);
        Optional<AuditProgramRisk> auditProgramRisk = auditProgramRiskService.findOne(id);
        return CustomApiResponse.ok(auditProgramRisk);
    }

    /**
     * {@code DELETE  /audit-program-risks/:id} : delete the "id" auditProgramRisk.
     *
     * @param id the id of the auditProgramRisk to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-program-risks/{id}")
    public CustomApiResponse deleteAuditProgramRisk(@PathVariable Long id) {
        log.debug("REST request to delete AuditProgramRisk : {}", id);
        auditProgramRiskService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
