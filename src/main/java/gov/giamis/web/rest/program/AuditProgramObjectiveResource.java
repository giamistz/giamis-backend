package gov.giamis.web.rest.program;

import gov.giamis.config.Constants;
import gov.giamis.domain.program.AuditProgramObjective;
import gov.giamis.service.program.AuditProgramObjectiveService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.program.dto.AuditProgramObjectiveCriteria;
import gov.giamis.service.program.AuditProgramObjectiveQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditProgramObjective}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuditProgramObjectiveResource {

    private final Logger log = LoggerFactory.getLogger(AuditProgramObjectiveResource.class);

    private static final String ENTITY_NAME = "auditProgramObjective";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditProgramObjectiveService auditProgramObjectiveService;

    private final AuditProgramObjectiveQueryService auditProgramObjectiveQueryService;

    public AuditProgramObjectiveResource(AuditProgramObjectiveService auditProgramObjectiveService, AuditProgramObjectiveQueryService auditProgramObjectiveQueryService) {
        this.auditProgramObjectiveService = auditProgramObjectiveService;
        this.auditProgramObjectiveQueryService = auditProgramObjectiveQueryService;
    }

    /**
     * {@code POST  /audit-program-objectives} : Create a new auditProgramObjective.
     *
     * @param auditProgramObjective the auditProgramObjective to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditProgramObjective, or with status {@code 400 (Bad Request)} if the auditProgramObjective has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-program-objectives")
    public CustomApiResponse createAuditProgramObjective(@Valid @RequestBody AuditProgramObjective auditProgramObjective) throws URISyntaxException {
        log.debug("REST request to save AuditProgramObjective : {}", auditProgramObjective);
        if (auditProgramObjective.getId() != null) {
            throw new BadRequestAlertException("A new auditProgramObjective cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditProgramObjective result = auditProgramObjectiveService.save(auditProgramObjective);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-program-objectives} : Updates an existing auditProgramObjective.
     *
     * @param auditProgramObjective the auditProgramObjective to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditProgramObjective,
     * or with status {@code 400 (Bad Request)} if the auditProgramObjective is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditProgramObjective couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-program-objectives")
    public CustomApiResponse updateAuditProgramObjective(@Valid @RequestBody AuditProgramObjective auditProgramObjective) throws URISyntaxException {
        log.debug("REST request to update AuditProgramObjective : {}", auditProgramObjective);
        if (auditProgramObjective.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditProgramObjective result = auditProgramObjectiveService.save(auditProgramObjective);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-program-objectives} : get all the auditProgramObjectives.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditProgramObjectives in body.
     */
    @GetMapping("/audit-program-objectives")
    public CustomApiResponse getAllAuditProgramObjectives(AuditProgramObjectiveCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditProgramObjectives by criteria: {}", criteria);
        Page<AuditProgramObjective> page = auditProgramObjectiveQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-program-objectives/count} : count all the auditProgramObjectives.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-program-objectives/count")
    public CustomApiResponse countAuditProgramObjectives(AuditProgramObjectiveCriteria criteria) {
        log.debug("REST request to count AuditProgramObjectives by criteria: {}", criteria);
        return CustomApiResponse.ok(auditProgramObjectiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-program-objectives/:id} : get the "id" auditProgramObjective.
     *
     * @param id the id of the auditProgramObjective to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditProgramObjective, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-program-objectives/{id}")
    public CustomApiResponse getAuditProgramObjective(@PathVariable Long id) {
        log.debug("REST request to get AuditProgramObjective : {}", id);
        Optional<AuditProgramObjective> auditProgramObjective = auditProgramObjectiveService.findOne(id);
        return CustomApiResponse.ok(auditProgramObjective);
    }

    /**
     * {@code DELETE  /audit-program-objectives/:id} : delete the "id" auditProgramObjective.
     *
     * @param id the id of the auditProgramObjective to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-program-objectives/{id}")
    public CustomApiResponse deleteAuditProgramObjective(@PathVariable Long id) {
        log.debug("REST request to delete AuditProgramObjective : {}", id);
        auditProgramObjectiveService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    /**
     * Get all audit program objective by engagement Id
     * @param engagementId = audit program engagement Id
     * @return
     */
    @GetMapping("/audit-program-objectives/byEngagementId/{engagementId}")
    public CustomApiResponse getByAuditEngagementId(@PathVariable Long engagementId) {
        return CustomApiResponse.ok(auditProgramObjectiveService
            .findObjectivesByAuditProgramEngagementId(engagementId));
    }
}
