package gov.giamis.web.rest.program;

import gov.giamis.config.Constants;
import gov.giamis.domain.program.AuditProgramProcedure;
import gov.giamis.service.program.AuditProgramProcedureService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.program.dto.AuditProgramProcedureCriteria;
import gov.giamis.service.program.AuditProgramProcedureQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditProgramProcedure}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuditProgramProcedureResource {

    private final Logger log = LoggerFactory.getLogger(AuditProgramProcedureResource.class);

    private static final String ENTITY_NAME = "auditProgramProcedure";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditProgramProcedureService auditProgramProcedureService;

    private final AuditProgramProcedureQueryService auditProgramProcedureQueryService;

    public AuditProgramProcedureResource(AuditProgramProcedureService auditProgramProcedureService, AuditProgramProcedureQueryService auditProgramProcedureQueryService) {
        this.auditProgramProcedureService = auditProgramProcedureService;
        this.auditProgramProcedureQueryService = auditProgramProcedureQueryService;
    }

    /**
     * {@code POST  /audit-program-procedures} : Create a new auditProgramProcedure.
     *
     * @param auditProgramProcedure the auditProgramProcedure to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditProgramProcedure, or with status {@code 400 (Bad Request)} if the auditProgramProcedure has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-program-procedures")
    public CustomApiResponse createAuditProgramProcedure(@Valid @RequestBody AuditProgramProcedure auditProgramProcedure) throws URISyntaxException {
        log.debug("REST request to save AuditProgramProcedure : {}", auditProgramProcedure);
        if (auditProgramProcedure.getId() != null) {
            throw new BadRequestAlertException("A new auditProgramProcedure cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditProgramProcedure result = auditProgramProcedureService.save(auditProgramProcedure);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-program-procedures} : Updates an existing auditProgramProcedure.
     *
     * @param auditProgramProcedure the auditProgramProcedure to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditProgramProcedure,
     * or with status {@code 400 (Bad Request)} if the auditProgramProcedure is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditProgramProcedure couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-program-procedures")
    public CustomApiResponse updateAuditProgramProcedure(@Valid @RequestBody AuditProgramProcedure auditProgramProcedure) throws URISyntaxException {
        log.debug("REST request to update AuditProgramProcedure : {}", auditProgramProcedure);
        if (auditProgramProcedure.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditProgramProcedure result = auditProgramProcedureService.save(auditProgramProcedure);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-program-procedures} : get all the auditProgramProcedures.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditProgramProcedures in body.
     */
    @GetMapping("/audit-program-procedures")
    public CustomApiResponse getAllAuditProgramProcedures(AuditProgramProcedureCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditProgramProcedures by criteria: {}", criteria);
        Page<AuditProgramProcedure> page = auditProgramProcedureQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-program-procedures/count} : count all the auditProgramProcedures.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-program-procedures/count")
    public CustomApiResponse countAuditProgramProcedures(AuditProgramProcedureCriteria criteria) {
        log.debug("REST request to count AuditProgramProcedures by criteria: {}", criteria);
        return CustomApiResponse.ok(auditProgramProcedureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-program-procedures/:id} : get the "id" auditProgramProcedure.
     *
     * @param id the id of the auditProgramProcedure to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditProgramProcedure, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-program-procedures/{id}")
    public CustomApiResponse getAuditProgramProcedure(@PathVariable Long id) {
        log.debug("REST request to get AuditProgramProcedure : {}", id);
        Optional<AuditProgramProcedure> auditProgramProcedure = auditProgramProcedureService.findOne(id);
        return CustomApiResponse.ok(auditProgramProcedure);
    }

    /**
     * {@code DELETE  /audit-program-procedures/:id} : delete the "id" auditProgramProcedure.
     *
     * @param id the id of the auditProgramProcedure to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-program-procedures/{id}")
    public CustomApiResponse deleteAuditProgramProcedure(@PathVariable Long id) {
        log.debug("REST request to delete AuditProgramProcedure : {}", id);
        auditProgramProcedureService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
