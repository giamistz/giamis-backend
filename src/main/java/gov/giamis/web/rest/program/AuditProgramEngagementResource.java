package gov.giamis.web.rest.program;

import gov.giamis.config.Constants;
import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.service.program.AuditProgramEngagementService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.program.dto.AuditProgramEngagementCriteria;
import gov.giamis.service.program.AuditProgramEngagementQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditProgramEngagement}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuditProgramEngagementResource {

    private final Logger log = LoggerFactory.getLogger(AuditProgramEngagementResource.class);

    private static final String ENTITY_NAME = "auditProgramEngagement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditProgramEngagementService auditProgramEngagementService;

    private final AuditProgramEngagementQueryService auditProgramEngagementQueryService;

    public AuditProgramEngagementResource(AuditProgramEngagementService auditProgramEngagementService, AuditProgramEngagementQueryService auditProgramEngagementQueryService) {
        this.auditProgramEngagementService = auditProgramEngagementService;
        this.auditProgramEngagementQueryService = auditProgramEngagementQueryService;
    }

    /**
     * {@code POST  /audit-program-engagements} : Create a new auditProgramEngagement.
     *
     * @param auditProgramEngagement the auditProgramEngagement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditProgramEngagement, or with status {@code 400 (Bad Request)} if the auditProgramEngagement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-program-engagements")
    public CustomApiResponse createAuditProgramEngagement(@Valid @RequestBody AuditProgramEngagement auditProgramEngagement) throws URISyntaxException {
        log.debug("REST request to save AuditProgramEngagement : {}", auditProgramEngagement);
        if (auditProgramEngagement.getId() != null) {
            throw new BadRequestAlertException("A new auditProgramEngagement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditProgramEngagement result = auditProgramEngagementService.save(auditProgramEngagement);
        return CustomApiResponse.created(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-program-engagements} : Updates an existing auditProgramEngagement.
     *
     * @param auditProgramEngagement the auditProgramEngagement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditProgramEngagement,
     * or with status {@code 400 (Bad Request)} if the auditProgramEngagement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditProgramEngagement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-program-engagements")
    public CustomApiResponse updateAuditProgramEngagement(@Valid @RequestBody AuditProgramEngagement auditProgramEngagement) throws URISyntaxException {
        log.debug("REST request to update AuditProgramEngagement : {}", auditProgramEngagement);
        if (auditProgramEngagement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditProgramEngagement result = auditProgramEngagementService.save(auditProgramEngagement);
        return CustomApiResponse.ok(result);
    }

    /**
     * {@code GET  /audit-program-engagements} : get all the auditProgramEngagements.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditProgramEngagements in body.
     */
    @GetMapping("/audit-program-engagements")
    public CustomApiResponse getAllAuditProgramEngagements(AuditProgramEngagementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditProgramEngagements by criteria: {}", criteria);
        Page<AuditProgramEngagement> page = auditProgramEngagementQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-program-engagements/count} : count all the auditProgramEngagements.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-program-engagements/count")
    public CustomApiResponse countAuditProgramEngagements(AuditProgramEngagementCriteria criteria) {
        log.debug("REST request to count AuditProgramEngagements by criteria: {}", criteria);
        return CustomApiResponse.ok(auditProgramEngagementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-program-engagements/:id} : get the "id" auditProgramEngagement.
     *
     * @param id the id of the auditProgramEngagement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditProgramEngagement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-program-engagements/{id}")
    public CustomApiResponse getAuditProgramEngagement(@PathVariable Long id) {
        log.debug("REST request to get AuditProgramEngagement : {}", id);
        Optional<AuditProgramEngagement> auditProgramEngagement = auditProgramEngagementService.findOne(id);
        return CustomApiResponse.ok(auditProgramEngagement);
    }

    /**
     * {@code DELETE  /audit-program-engagements/:id} : delete the "id" auditProgramEngagement.
     *
     * @param id the id of the auditProgramEngagement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-program-engagements/{id}")
    public CustomApiResponse deleteAuditProgramEngagement(@PathVariable Long id) {
        log.debug("REST request to delete AuditProgramEngagement : {}", id);
        auditProgramEngagementService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }
}
