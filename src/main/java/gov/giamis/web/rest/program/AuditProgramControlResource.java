package gov.giamis.web.rest.program;

import gov.giamis.config.Constants;
import gov.giamis.domain.program.AuditProgramControl;
import gov.giamis.service.program.AuditProgramControlService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import gov.giamis.service.program.dto.AuditProgramControlCriteria;
import gov.giamis.service.program.AuditProgramControlQueryService;

import gov.giamis.web.rest.response.CustomApiResponse;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AuditProgramControl}.
 */
@RestController
@RequestMapping(Constants.API_V1)
public class AuditProgramControlResource {

    private final Logger log = LoggerFactory.getLogger(AuditProgramControlResource.class);

    private static final String ENTITY_NAME = "auditProgramControl";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuditProgramControlService auditProgramControlService;

    private final AuditProgramControlQueryService auditProgramControlQueryService;

    public AuditProgramControlResource(AuditProgramControlService auditProgramControlService, AuditProgramControlQueryService auditProgramControlQueryService) {
        this.auditProgramControlService = auditProgramControlService;
        this.auditProgramControlQueryService = auditProgramControlQueryService;
    }

    /**
     * {@code POST  /audit-program-controls} : Create a new auditProgramControl.
     *
     * @param auditProgramControl the auditProgramControl to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new auditProgramControl, or with status {@code 400 (Bad Request)} if the auditProgramControl has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audit-program-controls")
    public CustomApiResponse createAuditProgramControl(@Valid @RequestBody AuditProgramControl auditProgramControl) throws URISyntaxException {
        log.debug("REST request to save AuditProgramControl : {}", auditProgramControl);
        if (auditProgramControl.getId() != null) {
            throw new BadRequestAlertException("A new auditProgramControl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditProgramControl result = auditProgramControlService.save(auditProgramControl);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.CREATE_SUCCESS), result);
    }

    /**
     * {@code PUT  /audit-program-controls} : Updates an existing auditProgramControl.
     *
     * @param auditProgramControl the auditProgramControl to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated auditProgramControl,
     * or with status {@code 400 (Bad Request)} if the auditProgramControl is not valid,
     * or with status {@code 500 (Internal Server Error)} if the auditProgramControl couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audit-program-controls")
    public CustomApiResponse updateAuditProgramControl(@Valid @RequestBody AuditProgramControl auditProgramControl) throws URISyntaxException {
        log.debug("REST request to update AuditProgramControl : {}", auditProgramControl);
        if (auditProgramControl.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditProgramControl result = auditProgramControlService.save(auditProgramControl);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.UPDATE_SUCCESS), result);
    }

    /**
     * {@code GET  /audit-program-controls} : get all the auditProgramControls.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of auditProgramControls in body.
     */
    @GetMapping("/audit-program-controls")
    public CustomApiResponse getAllAuditProgramControls(AuditProgramControlCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditProgramControls by criteria: {}", criteria);
        Page<AuditProgramControl> page = auditProgramControlQueryService.findByCriteria(criteria, pageable);
        return CustomApiResponse.ok(page);
    }

    /**
    * {@code GET  /audit-program-controls/count} : count all the auditProgramControls.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/audit-program-controls/count")
    public CustomApiResponse countAuditProgramControls(AuditProgramControlCriteria criteria) {
        log.debug("REST request to count AuditProgramControls by criteria: {}", criteria);
        return CustomApiResponse.ok(auditProgramControlQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /audit-program-controls/:id} : get the "id" auditProgramControl.
     *
     * @param id the id of the auditProgramControl to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the auditProgramControl, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audit-program-controls/{id}")
    public CustomApiResponse getAuditProgramControl(@PathVariable Long id) {
        log.debug("REST request to get AuditProgramControl : {}", id);
        Optional<AuditProgramControl> auditProgramControl = auditProgramControlService.findOne(id);
        return CustomApiResponse.ok(auditProgramControl);
    }

    /**
     * {@code DELETE  /audit-program-controls/:id} : delete the "id" auditProgramControl.
     *
     * @param id the id of the auditProgramControl to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audit-program-controls/{id}")
    public CustomApiResponse deleteAuditProgramControl(@PathVariable Long id) {
        log.debug("REST request to delete AuditProgramControl : {}", id);
        auditProgramControlService.delete(id);
        return CustomApiResponse.ok(ENTITY_NAME.concat(Constants.DELETE_SUCCESS));
    }

    /**
     * Get all audit program procedure by engagement Id
     * @param engagementId = audit program engagement Id
     * @return
     */
    @GetMapping("/audit-program-controls/controlsByEngagementId/{engagementId}")
    public CustomApiResponse getControlsByAuditEngagementId(@PathVariable Long engagementId) {
        return CustomApiResponse.ok(auditProgramControlService
            .findControlsByAuditProgramEngagementId(engagementId));
    }
}
