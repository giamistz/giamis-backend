package gov.giamis.web.rest.upload;

import gov.giamis.config.Constants;
import gov.giamis.web.rest.response.CustomApiResponse;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Paths;

@RestController
@RequestMapping(Constants.API_V1)
public class DataImportResource {

//    private final Job orgUnitUploadJob;
//
//    private final JobLauncher jobLauncher;
//
//    public DataImportResource(Job orgUnitUploadJob, JobLauncher jobLauncher) {
//        this.orgUnitUploadJob = orgUnitUploadJob;
//        this.jobLauncher = jobLauncher;
//    }
//
//    @PostMapping("/import-org-units")
//    public CustomApiResponse importOrgUnit(@RequestParam("file") MultipartFile file)
//        throws IOException,
//        JobParametersInvalidException,
//        JobExecutionAlreadyRunningException,
//        JobRestartException,
//        JobInstanceAlreadyCompleteException {
//
//      //  File tmpFile = Files.createTempFile("giamis-org-unit",".xls").toFile();
//        String path = "/tmp/giamis-org-unit".concat(String.valueOf(System.currentTimeMillis()).concat(".csv"));
//        file.transferTo(Paths.get(path));
//
//        JobExecution jobExecution = jobLauncher.run(orgUnitUploadJob, new JobParametersBuilder()
//            .addString("filePath", path)
//            .addLong("time", System.currentTimeMillis())
//            .toJobParameters());
//
//        return CustomApiResponse.ok(jobExecution.getStatus());
//    }
}
