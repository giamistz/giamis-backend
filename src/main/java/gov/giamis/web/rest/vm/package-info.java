/**
 * View Models used by Spring MVC REST controllers.
 */
package gov.giamis.web.rest.vm;
