/**
 * Data Access Objects used by WebSocket services.
 */
package gov.giamis.web.websocket.dto;
