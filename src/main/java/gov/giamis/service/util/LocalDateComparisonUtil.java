package gov.giamis.service.util;

import java.time.LocalDate;

public class LocalDateComparisonUtil {
	
	public LocalDateComparisonUtil() {
		
	}
	
	public Boolean compareTwoDates(LocalDate startDate,LocalDate endDate) {
        if ((startDate.isBefore(endDate)) || (startDate.isEqual(endDate))) {
        	return true;
        }
        return false;
	}
	

}
