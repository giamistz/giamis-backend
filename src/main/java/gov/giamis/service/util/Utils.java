package gov.giamis.service.util;

import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.domain.setup.MenuItem;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.*;
import java.util.List;

@Component
public class Utils {

	public static String capitalizeWords(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}

	public static Map<String,List<Long>> getMenuGroup(List<MenuItem> menuItems) {

	    Map<String, List<Long>> groupItemIds = new HashMap<>();
        List<Long> menuGroupIds = new ArrayList<>();
        List<Long> menuItemsIds = new ArrayList<>();
        menuGroupIds.add(0L);
        menuItemsIds.add(0L);

        menuItems.forEach(m -> {
            menuItemsIds.add(m.getId());
            if (m.getMenuGroup() != null) {
                getGroup(m.getMenuGroup(), menuGroupIds);
            }
        });
        groupItemIds.put("groupIds", menuGroupIds);
        groupItemIds.put("itemIds", menuItemsIds);
        return groupItemIds;
    }

    private static void getGroup(MenuGroup group, List<Long> collect) {
	    collect.add(group.getId());
	    if (group.getParentMenuGroup() != null) {
	        getGroup(group.getParentMenuGroup(), collect);
        }
    }


   

}
