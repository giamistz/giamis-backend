package gov.giamis.service;

import gov.giamis.domain.EngagementSampling;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementSampling}.
 */
public interface EngagementSamplingService {

    /**
     * Save a engagementSampling.
     *
     * @param engagementSampling the entity to save.
     * @return the persisted entity.
     */
    EngagementSampling save(EngagementSampling engagementSampling);

    /**
     * Get all the engagementSamplings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementSampling> findAll(Pageable pageable);


    /**
     * Get the "id" engagementSampling.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementSampling> findOne(Long id);

    /**
     * Delete the "id" engagementSampling.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
