package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.OrganisationUnit_;
import gov.giamis.domain.setup.Quarter_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.QuarterConsolidationReport;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.QuarterConsolidationReportRepository;
import gov.giamis.service.dto.QuarterConsolidationReportCriteria;

/**
 * Service for executing complex queries for {@link QuarterConsolidationReport} entities in the database.
 * The main input is a {@link QuarterConsolidationReportCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuarterConsolidationReport} or a {@link Page} of {@link QuarterConsolidationReport} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuarterConsolidationReportQueryService extends QueryService<QuarterConsolidationReport> {

    private final Logger log = LoggerFactory.getLogger(QuarterConsolidationReportQueryService.class);

    private final QuarterConsolidationReportRepository quarterConsolidationReportRepository;

    public QuarterConsolidationReportQueryService(QuarterConsolidationReportRepository quarterConsolidationReportRepository) {
        this.quarterConsolidationReportRepository = quarterConsolidationReportRepository;
    }

    /**
     * Return a {@link List} of {@link QuarterConsolidationReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuarterConsolidationReport> findByCriteria(QuarterConsolidationReportCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<QuarterConsolidationReport> specification = createSpecification(criteria);
        return quarterConsolidationReportRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link QuarterConsolidationReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuarterConsolidationReport> findByCriteria(QuarterConsolidationReportCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<QuarterConsolidationReport> specification = createSpecification(criteria);
        return quarterConsolidationReportRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuarterConsolidationReportCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<QuarterConsolidationReport> specification = createSpecification(criteria);
        return quarterConsolidationReportRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<QuarterConsolidationReport> createSpecification(QuarterConsolidationReportCriteria criteria) {
        Specification<QuarterConsolidationReport> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), QuarterConsolidationReport_.id));
            }
            if (criteria.getOrganisationId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationId(),
                    root -> root.join(QuarterConsolidationReport_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getQuarterId() != null) {
                specification = specification.and(buildSpecification(criteria.getQuarterId(),
                    root -> root.join(QuarterConsolidationReport_.quarter, JoinType.LEFT).get(Quarter_.id)));
            }
        }
        return specification;
    }
}
