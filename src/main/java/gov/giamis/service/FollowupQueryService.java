package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.Followup;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.FollowupRepository;
import gov.giamis.service.dto.FollowupCriteria;

/**
 * Service for executing complex queries for {@link Followup} entities in the database.
 * The main input is a {@link FollowupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Followup} or a {@link Page} of {@link Followup} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FollowupQueryService extends QueryService<Followup> {

    private final Logger log = LoggerFactory.getLogger(FollowupQueryService.class);

    private final FollowupRepository followupRepository;

    public FollowupQueryService(FollowupRepository followupRepository) {
        this.followupRepository = followupRepository;
    }

    /**
     * Return a {@link List} of {@link Followup} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Followup> findByCriteria(FollowupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Followup> specification = createSpecification(criteria);
        return followupRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Followup} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Followup> findByCriteria(FollowupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Followup> specification = createSpecification(criteria);
        return followupRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FollowupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Followup> specification = createSpecification(criteria);
        return followupRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Followup> createSpecification(FollowupCriteria criteria) {
        Specification<Followup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Followup_.id));
            }
            if (criteria.getIsOpen() != null) {
                specification = specification.and(buildSpecification(criteria.getIsOpen(), Followup_.isOpen));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Followup_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Followup_.endDate));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getFinancialYearId(),
                    root -> root.join(Followup_.financialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(Followup_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(Followup_.engagements, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }
}
