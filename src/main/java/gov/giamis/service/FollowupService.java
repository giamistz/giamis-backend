package gov.giamis.service;

import gov.giamis.domain.Followup;
import gov.giamis.repository.FollowupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Followup}.
 */
@Service
@Transactional
public class FollowupService {

    private final Logger log = LoggerFactory.getLogger(FollowupService.class);

    private final FollowupRepository followupRepository;

    public FollowupService(FollowupRepository followupRepository) {
        this.followupRepository = followupRepository;
    }

    /**
     * Save a followup.
     *
     * @param followup the entity to save.
     * @return the persisted entity.
     */
    public Followup save(Followup followup) {
        log.debug("Request to save Followup : {}", followup);
        return followupRepository.save(followup);
    }

    /**
     * Get all the followups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Followup> findAll(Pageable pageable) {
        log.debug("Request to get all Followups");
        return followupRepository.findAll(pageable);
    }

    /**
     * Get all the followups with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Followup> findAllWithEagerRelationships(Pageable pageable) {
        return followupRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one followup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Followup> findOne(Long id) {
        log.debug("Request to get Followup : {}", id);
        return followupRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the followup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Followup : {}", id);
        followupRepository.deleteById(id);
    }
}
