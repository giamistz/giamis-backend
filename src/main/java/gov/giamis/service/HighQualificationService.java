package gov.giamis.service;

import gov.giamis.domain.HighQualification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link HighQualification}.
 */
public interface HighQualificationService {

    /**
     * Save a highQualification.
     *
     * @param highQualification the entity to save.
     * @return the persisted entity.
     */
    HighQualification save(HighQualification highQualification);

    /**
     * Get all the highQualifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HighQualification> findAll(Pageable pageable);


    /**
     * Get the "id" highQualification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HighQualification> findOne(Long id);

    /**
     * Delete the "id" highQualification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
