package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.ProfessionalQualifcation;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.ProfessionalQualifcationRepository;
import gov.giamis.service.dto.ProfessionalQualifcationCriteria;

/**
 * Service for executing complex queries for {@link ProfessionalQualifcation} entities in the database.
 * The main input is a {@link ProfessionalQualifcationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProfessionalQualifcation} or a {@link Page} of {@link ProfessionalQualifcation} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProfessionalQualifcationQueryService extends QueryService<ProfessionalQualifcation> {

    private final Logger log = LoggerFactory.getLogger(ProfessionalQualifcationQueryService.class);

    private final ProfessionalQualifcationRepository professionalQualifcationRepository;

    public ProfessionalQualifcationQueryService(ProfessionalQualifcationRepository professionalQualifcationRepository) {
        this.professionalQualifcationRepository = professionalQualifcationRepository;
    }

    /**
     * Return a {@link List} of {@link ProfessionalQualifcation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProfessionalQualifcation> findByCriteria(ProfessionalQualifcationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProfessionalQualifcation> specification = createSpecification(criteria);
        return professionalQualifcationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ProfessionalQualifcation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProfessionalQualifcation> findByCriteria(ProfessionalQualifcationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProfessionalQualifcation> specification = createSpecification(criteria);
        return professionalQualifcationRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProfessionalQualifcationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProfessionalQualifcation> specification = createSpecification(criteria);
        return professionalQualifcationRepository.count(specification);
    }

    /**
     * Function to convert {@link ProfessionalQualifcationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProfessionalQualifcation> createSpecification(ProfessionalQualifcationCriteria criteria) {
        Specification<ProfessionalQualifcation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ProfessionalQualifcation_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ProfessionalQualifcation_.name));
            }
        }
        return specification;
    }
}
