package gov.giamis.service;

import gov.giamis.domain.setup.User;
import gov.giamis.dto.AuditableAreaQuarterValueDTO;
import gov.giamis.dto.OrgUnitAuditorDTO;
import gov.giamis.dto.QuarterValueDTO;
import gov.giamis.dto.RecommendationStatusDTO;
import gov.giamis.repository.DashBoardRepository;
import gov.giamis.service.setup.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DashboardService {

    private final DashBoardRepository dashBoardRepository;

    private final UserService userService;

    private List<Long> orgUnitIds = new ArrayList<>();


    public DashboardService(DashBoardRepository dashBoardRepository,
                            UserService userService) {
        this.dashBoardRepository = dashBoardRepository;
        this.userService = userService;

    }


    public List<QuarterValueDTO> engByUserOrgUnitByFnByQtr(Long fyId) {
        User user = this.userService.getUserWithAuthorities().get();
        if (user.getOrganisationUnit() != null) {
            orgUnitIds = this.userService.userOrgUnits(user.getOrganisationUnit().getId());
        }
        return dashBoardRepository.engagementByQuarter(fyId, orgUnitIds);
    }

    public List<AuditableAreaQuarterValueDTO> getByAreaByQrt(Long fyId) {
        User user = this.userService.getUserWithAuthorities().get();
        if (user.getOrganisationUnit() != null) {
            orgUnitIds = this.userService.userOrgUnits(user.getOrganisationUnit().getId());
        }
        return dashBoardRepository.getByAreaByQrt(fyId, orgUnitIds);
    }

    public List<RecommendationStatusDTO> recommByStatus(Long fyId) {
            User user = this.userService.getUserWithAuthorities().get();
            if (user.getOrganisationUnit() != null) {
                orgUnitIds = this.userService.userOrgUnits(user.getOrganisationUnit().getId());
            }
            return dashBoardRepository.recommByStatus(fyId, orgUnitIds);
    }

    public List<OrgUnitAuditorDTO> auditors() {
            User user = this.userService.getUserWithAuthorities().get();
            if (user.getOrganisationUnit() != null) {
                orgUnitIds = this.userService.userOrgUnits(user.getOrganisationUnit().getId());
            }
            return dashBoardRepository.auditors(orgUnitIds);
    }

}
