package gov.giamis.service.risk.impl;

import java.util.Optional;

import gov.giamis.domain.risk.Risk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.risk.RiskRating;
import gov.giamis.repository.risk.RiskRatingRepository;
import gov.giamis.service.risk.RiskRatingService;

/**
 * Service Implementation for managing {@link RiskRating}.
 */
@Service
@Transactional
public class RiskRatingServiceImpl implements RiskRatingService {

    private final Logger log = LoggerFactory.getLogger(RiskRatingServiceImpl.class);

    private final RiskRatingRepository riskRatingRepository;

    @Autowired
    private CacheManager cacheManager;

    public RiskRatingServiceImpl(RiskRatingRepository riskRatingRepository) {
        this.riskRatingRepository = riskRatingRepository;
    }

    /**
     * Save a riskRating.
     *
     * @param riskRating the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RiskRating save(RiskRating riskRating) {
        log.debug("Request to save RiskRating : {}", riskRating);
        cacheManager.getCache(Risk.class.getName() +".riskRatings").clear();
        cacheManager.getCache(RiskRating.class.getName()).clear();
        return riskRatingRepository.save(riskRating);
    }

    /**
     * Get all the riskRatings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RiskRating> findAll(Pageable pageable) {
        log.debug("Request to get all RiskRatings");
        return riskRatingRepository.findAll(pageable);
    }


    /**
     * Get one riskRating by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RiskRating> findOne(Long id) {
        log.debug("Request to get RiskRating : {}", id);
        return riskRatingRepository.findById(id);
    }

    /**
     * Delete the riskRating by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RiskRating : {}", id);
        riskRatingRepository.deleteById(id);
    }

}
