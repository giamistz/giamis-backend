package gov.giamis.service.risk.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.risk.Risk;
import gov.giamis.repository.risk.RiskRepository;
import gov.giamis.service.risk.RiskService;

/**
 * Service Implementation for managing {@link Risk}.
 */
@Service
@Transactional
public class RiskServiceImpl implements RiskService {

    private final Logger log = LoggerFactory.getLogger(RiskServiceImpl.class);
    
    @Autowired
    private final RiskRepository riskRepository;
    
    
   
    public RiskServiceImpl(RiskRepository riskRepository) {
        this.riskRepository = riskRepository;
    }

    /**
     * Save a risk.
     *
     * @param risk the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Risk save(Risk risk) {
        log.debug("Request to save Risk : {}", risk);
        if (risk.getId() == null) {
            risk.setCode(getNextCode(risk));
        }
        return riskRepository.save(risk);
    }

    /**
     * Get all the risks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Risk> findAll(Pageable pageable) {
        log.debug("Request to get all Risks");
        return riskRepository.findAll(pageable);
    }


    /**
     * Get one risk by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Risk> findOne(Long id) {
        log.debug("Request to get Risk : {}", id);
        return riskRepository.findById(id);
    }

    /**
     * Delete the risk by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Risk : {}", id);
        riskRepository.deleteById(id);
    }
    
    @Override
	public Risk findByCode(String code) {
		return riskRepository.findByCodeIgnoreCase(code);
	}

	@Override
	public List<Risk> findRiskByOrganisationUnitId(Long id) {
		return riskRepository.findByOrganisationUnit_Id(id);
	}

	@Override
	public List<Risk> findRiskByIdAndOrganisationUnitId(Long id,Long organisationUnitId) {
		return riskRepository.findByIdAndOrganisationUnit_Id(id, organisationUnitId);
	}

	@Override
	public void updateRisksByOrganisationUnitId(Long organisationUnitId) {
		 riskRepository.updateRiskByOrganisationUnitId(organisationUnitId);
	}

	@Override
	public void updateRisksByIdAndOrganisationUnitId(Long id, Long organisationUnitId) {
		 riskRepository.updateRiskByIdAndOrganisationUnitId(id, organisationUnitId);
	}

	@Override
	public List<Risk> findRiskByRiskRegisterId(Long id) {
		// TODO Auto-generated method stub
		return riskRepository.findByRiskRegister_Id(id);
	}
  
	@Override
	public void updateRisksByRiskRegisterId(Long id) {
		riskRepository.updateRiskByRiskRegisterId(id);
	}
	
	@Override
	public void updateRisksByIdAndRiskRegisterId(Long id, Long riskRegisterId) {
		 riskRepository.updateRiskByIdAndRiskRegisterId(id, riskRegisterId);
	}

    @Override
    public String getNextCode(Risk risk) {
        String maxCode = riskRepository.maxByRegisterId(risk.getRiskRegister().getId(), risk.getTarget().getId());
        String code;
        if (maxCode == null || !maxCode.contains("R-".concat(risk.getTarget().getCode()))) {
            code ="R-".concat(risk.getTarget().getCode()).concat("01");
        } else {
            String last = maxCode.replace("R-".concat(risk.getTarget().getCode()),"");
            int lastInt = Integer.parseInt(last);
            lastInt = lastInt + 1;
            if (lastInt < 10) {
                code = "R-".concat(risk.getTarget().getCode()).concat("0").concat(String.valueOf(lastInt));
            } else {
                code = "R-".concat(risk.getTarget().getCode()).concat(String.valueOf(lastInt));
            }
        }
        return code;
    }

    @Override
	public List<Risk> findRiskByIdAndRiskRegisterId(Long id, Long riskRegisterId) {
		return riskRepository.findByIdAndRiskRegister_Id(id, riskRegisterId);
	}
}
