package gov.giamis.service.risk.impl;

import gov.giamis.service.risk.TargetService;
import gov.giamis.domain.risk.Target;
import gov.giamis.repository.risk.TargetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Target}.
 */
@Service
@Transactional
public class TargetServiceImpl implements TargetService {

    private final Logger log = LoggerFactory.getLogger(TargetServiceImpl.class);

    private final TargetRepository targetRepository;

    public TargetServiceImpl(TargetRepository targetRepository) {
        this.targetRepository = targetRepository;
    }

    /**
     * Save a target.
     *
     * @param target the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Target save(Target target) {
        log.debug("Request to save Target : {}", target);
        return targetRepository.save(target);
    }

    /**
     * Get all the targets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Target> findAll(Pageable pageable) {
        log.debug("Request to get all Targets");
        return targetRepository.findAll(pageable);
    }


    /**
     * Get one target by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Target> findOne(Long id) {
        log.debug("Request to get Target : {}", id);
        return targetRepository.findById(id);
    }

    /**
     * Delete the target by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Target : {}", id);
        targetRepository.deleteById(id);
    }

    @Override
    public Target findByDescription(String description) {
        return targetRepository.findByDescriptionIgnoreCase(description);
    }
}
