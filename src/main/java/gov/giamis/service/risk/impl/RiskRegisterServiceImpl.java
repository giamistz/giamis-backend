package gov.giamis.service.risk.impl;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.User;
import gov.giamis.dto.ApproveDTO;
import gov.giamis.repository.risk.RiskRepository;
import gov.giamis.repository.setup.FileResourceRepository;
import gov.giamis.service.risk.RiskRegisterService;
import gov.giamis.domain.risk.RiskRegister;
import gov.giamis.repository.risk.RiskRegisterRepository;
import gov.giamis.service.setup.UserOrganisationUnitService;
import gov.giamis.service.setup.UserService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RiskRegister}.
 */
@Service
@Transactional
public class RiskRegisterServiceImpl implements RiskRegisterService {

    private final Logger log = LoggerFactory.getLogger(RiskRegisterServiceImpl.class);

    private final RiskRegisterRepository riskRegisterRepository;

    private  final UserOrganisationUnitService userOrganisationUnitService;

    private final RiskRepository riskRepository;

    private final UserService userService;

    private final FileResourceRepository fileResourceRepository;

    public RiskRegisterServiceImpl(
        RiskRegisterRepository riskRegisterRepository,
        UserOrganisationUnitService userOrganisationUnitService,
        RiskRepository riskRepository, UserService userService,
        FileResourceRepository fileResourceRepository) {
        this.riskRegisterRepository = riskRegisterRepository;
        this.userOrganisationUnitService = userOrganisationUnitService;
        this.riskRepository = riskRepository;
        this.userService = userService;
        this.fileResourceRepository = fileResourceRepository;
    }

    /**
     * Save a riskRegister.
     *
     * @param riskRegister the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RiskRegister save(RiskRegister riskRegister) {
        log.debug("Request to save RiskRegister : {}", riskRegister);
        OrganisationUnit userOrgUnit = userOrganisationUnitService.getCurrentOrganisationUnit();
        if (userOrgUnit == null) {
            throw new BadRequestAlertException("Current user has no organisation unit",
                "Organisation Unit","orgUnitNotExist");
        }
        if (!userOrgUnit.getOrganisationUnitLevel().getRiskRegisterLevel()) {
            throw new BadRequestAlertException("User organisation unit is not risk register level",
                "Organisation Unit","notRiskRegisterLevel");
        }
        riskRegister.setOrganisationUnit(userOrgUnit);
        return riskRegisterRepository.save(riskRegister);
    }

    /**
     * Get all the riskRegisters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RiskRegister> findAll(Pageable pageable) {
        log.debug("Request to get all RiskRegisters");
        return riskRegisterRepository.findAll(pageable);
    }


    /**
     * Get one riskRegister by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RiskRegister> findOne(Long id) {
        log.debug("Request to get RiskRegister : {}", id);
        return riskRegisterRepository.findById(id);
    }

    /**
     * Delete the riskRegister by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RiskRegister : {}", id);
        riskRegisterRepository.deleteById(id);
    }

    @Override
    public RiskRegister findByName(String name) {
        return riskRegisterRepository.findByNameIgnoreCase(name);
    }

    @Override
    @Transactional
    public void approve(ApproveDTO approveDTO) {

        riskRepository.updateRiskByRiskRegisterId(approveDTO.getId());
        User user = userService.getCurrent().get();
        RiskRegister register = riskRegisterRepository.getOne(approveDTO.getId());
        FileResource fileResource = fileResourceRepository.getOne(approveDTO.getFileResourceId());

        register.setFileResource(fileResource);
        register.setApproved(true);
        register.setApprovedDate(LocalDate.now());
        register.setApprovedBy(user.getFirstName().concat(" ").concat(user.getLastName()));
        riskRegisterRepository.save(register);
        fileResourceRepository.setAssigned(fileResource.getId(), true);
    }
}
