package gov.giamis.service.risk.impl;

import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.repository.risk.RiskControlMatrixRepository;
import gov.giamis.service.risk.RiskControlMatrixService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RiskControlMatrix}.
 */
@Service
@Transactional
public class RiskControlMatrixServiceImpl implements RiskControlMatrixService {

    private final Logger log = LoggerFactory.getLogger(RiskControlMatrixServiceImpl.class);

    private final RiskControlMatrixRepository riskControlMatrixRepository;

    public RiskControlMatrixServiceImpl(RiskControlMatrixRepository riskControlMatrixRepository) {
        this.riskControlMatrixRepository = riskControlMatrixRepository;
    }

    /**
     * Save a riskControlMatrix.
     *
     * @param riskControlMatrix the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RiskControlMatrix save(RiskControlMatrix riskControlMatrix) {
        log.debug("Request to save RiskControlMatrix : {}", riskControlMatrix);
        return riskControlMatrixRepository.save(riskControlMatrix);
    }

    /**
     * Get all the riskControlMatrices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RiskControlMatrix> findAll(Pageable pageable) {
        log.debug("Request to get all RiskControlMatrices");
        return riskControlMatrixRepository.findAll(pageable);
    }


    /**
     * Get one riskControlMatrix by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RiskControlMatrix> findOne(Long id) {
        log.debug("Request to get RiskControlMatrix : {}", id);
        return riskControlMatrixRepository.findById(id);
    }

    /**
     * Delete the riskControlMatrix by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RiskControlMatrix : {}", id);
        riskControlMatrixRepository.deleteById(id);
    }

	@Override
	public Long countRiskControlMatrix() {
		log.debug("Request count RiskControlMatrix");
		return riskControlMatrixRepository.countRiskControlMatrix();
	}
}
