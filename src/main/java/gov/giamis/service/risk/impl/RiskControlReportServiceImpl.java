package gov.giamis.service.risk.impl;

import gov.giamis.domain.risk.RiskControlReport;
import gov.giamis.domain.setup.User;
import gov.giamis.dto.ApproveDTO;
import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.repository.risk.RiskControlReportRepository;
import gov.giamis.service.risk.RiskControlReportService;

import gov.giamis.service.setup.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RiskControlReport}.
 */
@Service
@Transactional
public class RiskControlReportServiceImpl implements RiskControlReportService {

    private final Logger log = LoggerFactory.getLogger(RiskControlReportServiceImpl.class);

    private final RiskControlReportRepository riskControlReportRepository;

    private final EngagementRepository engagementRepository;

    private final UserService userService;

    public RiskControlReportServiceImpl(RiskControlReportRepository riskControlReportRepository,
                                        EngagementRepository engagementRepository,
                                        UserService userService) {
        this.riskControlReportRepository = riskControlReportRepository;
        this.engagementRepository = engagementRepository;
        this.userService = userService;
    }

    /**
     * Save a riskControlReport.
     *
     * @param riskControlReport the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RiskControlReport save(RiskControlReport riskControlReport) {
        log.debug("Request to save RiskControlReport : {}", riskControlReport);
        Long engagementId = riskControlReport.getEngagement().getId();
        engagementRepository.findById(engagementId).ifPresent(riskControlReport::engagement);
        return riskControlReportRepository.save(riskControlReport);
    }

    /**
     * Get all the riskControlReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RiskControlReport> findAll(Pageable pageable) {
        log.debug("Request to get all RiskControlReports");
        return riskControlReportRepository.findAll(pageable);
    }


    /**
     * Get one riskControlReport by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RiskControlReport> findOne(Long id) {
        log.debug("Request to get RiskControlReport : {}", id);
        return riskControlReportRepository.findById(id);
    }

    /**
     * Delete the riskControlReport by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RiskControlReport : {}", id);
        riskControlReportRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void approve(ApproveDTO approveDTO) {

        riskControlReportRepository.updateRiskControlReportById(approveDTO.getId());
        User user = userService.getCurrent().get();
        RiskControlReport riskControlReport = riskControlReportRepository.getOne(approveDTO.getId());

        riskControlReport.setApproved(true);
        riskControlReport.setApprovedDate(LocalDate.now());
        riskControlReport.setApprovedBy(user.getFirstName().concat(" ").concat(user.getLastName()));
        riskControlReportRepository.save(riskControlReport);
    }
}
