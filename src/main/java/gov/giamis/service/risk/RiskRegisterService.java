package gov.giamis.service.risk;

import gov.giamis.domain.risk.RiskRegister;

import gov.giamis.dto.ApproveDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link RiskRegister}.
 */
public interface RiskRegisterService {

    /**
     * Save a riskRegister.
     *
     * @param riskRegister the entity to save.
     * @return the persisted entity.
     */
    RiskRegister save(RiskRegister riskRegister);

    /**
     * Get all the riskRegisters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RiskRegister> findAll(Pageable pageable);


    /**
     * Get the "id" riskRegister.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RiskRegister> findOne(Long id);

    /**
     * Delete the "id" riskRegister.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    RiskRegister findByName(String name);

    void approve(ApproveDTO approveDTO);
}
