package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.risk.RiskRating_;
import gov.giamis.domain.risk.Risk_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.risk.RiskRating;
import gov.giamis.repository.risk.RiskRatingRepository;
import gov.giamis.service.risk.dto.RiskRatingCriteria;

/**
 * Service for executing complex queries for {@link RiskRating} entities in the database.
 * The main input is a {@link RiskRatingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskRating} or a {@link Page} of {@link RiskRating} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskRatingQueryService extends QueryService<RiskRating> {

    private final Logger log = LoggerFactory.getLogger(RiskRatingQueryService.class);

    private final RiskRatingRepository riskRatingRepository;

    public RiskRatingQueryService(RiskRatingRepository riskRatingRepository) {
        this.riskRatingRepository = riskRatingRepository;
    }

    /**
     * Return a {@link List} of {@link RiskRating} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskRating> findByCriteria(RiskRatingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskRating> specification = createSpecification(criteria);
        return riskRatingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskRating} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskRating> findByCriteria(RiskRatingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskRating> specification = createSpecification(criteria);
        return riskRatingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskRatingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskRating> specification = createSpecification(criteria);
        return riskRatingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskRating> createSpecification(RiskRatingCriteria criteria) {
        Specification<RiskRating> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), RiskRating_.id));
            }
            if (criteria.getImpact() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getImpact(), RiskRating_.impact));
            }
            if (criteria.getLikelihood() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getLikelihood(), RiskRating_.likelihood));
            }
            if (criteria.getSource() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getSource(), RiskRating_.source));
            }
            if (criteria.getRiskId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getRiskId(),
                    root -> root.join(RiskRating_.risk, JoinType.LEFT)
                        .get(Risk_.id)));
            }
        }
        return specification;
    }
}
