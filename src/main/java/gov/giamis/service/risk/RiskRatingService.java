package gov.giamis.service.risk;

import gov.giamis.domain.risk.RiskRating;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link RiskRating}.
 */
public interface RiskRatingService {

    /**
     * Save a riskRating.
     *
     * @param riskRating the entity to save.
     * @return the persisted entity.
     */
    RiskRating save(RiskRating riskRating);

    /**
     * Get all the riskRatings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RiskRating> findAll(Pageable pageable);


    /**
     * Get the "id" riskRating.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RiskRating> findOne(Long id);

    /**
     * Delete the "id" riskRating.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
