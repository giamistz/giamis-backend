package gov.giamis.service.risk;

import gov.giamis.domain.risk.Target;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Target}.
 */
public interface TargetService {

    /**
     * Save a target.
     *
     * @param target the entity to save.
     * @return the persisted entity.
     */
    Target save(Target target);

    /**
     * Get all the targets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Target> findAll(Pageable pageable);


    /**
     * Get the "id" target.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Target> findOne(Long id);

    /**
     * Delete the "id" target.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    Target findByDescription(String description);
}
