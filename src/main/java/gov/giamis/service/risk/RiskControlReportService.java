package gov.giamis.service.risk;

import gov.giamis.dto.ApproveDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.risk.RiskControlReport;

import java.util.Optional;

/**
 * Service Interface for managing {@link RiskControlReport}.
 */
public interface RiskControlReportService {

    /**
     * Save a riskControlReport.
     *
     * @param riskControlReport the entity to save.
     * @return the persisted entity.
     */
    RiskControlReport save(RiskControlReport riskControlReport);

    /**
     * Get all the riskControlReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RiskControlReport> findAll(Pageable pageable);


    /**
     * Get the "id" riskControlReport.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RiskControlReport> findOne(Long id);

    /**
     * Delete the "id" riskControlReport.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void approve(ApproveDTO approveDTO);
}
