package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.risk.Target_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.Objective_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.risk.Target;
import gov.giamis.repository.risk.TargetRepository;
import gov.giamis.service.risk.dto.TargetCriteria;

/**
 * Service for executing complex queries for {@link Target} entities in the database.
 * The main input is a {@link TargetCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Target} or a {@link Page} of {@link Target} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TargetQueryService extends QueryService<Target> {

    private final Logger log = LoggerFactory.getLogger(TargetQueryService.class);

    private final TargetRepository targetRepository;

    public TargetQueryService(TargetRepository targetRepository) {
        this.targetRepository = targetRepository;
    }

    /**
     * Return a {@link List} of {@link Target} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Target> findByCriteria(TargetCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Target> specification = createSpecification(criteria);
        return targetRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Target} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Target> findByCriteria(TargetCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Target> specification = createSpecification(criteria);
        return targetRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TargetCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Target> specification = createSpecification(criteria);
        return targetRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Target> createSpecification(TargetCriteria criteria) {
        Specification<Target> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Target_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Target_.code));
            }
            if (criteria.getObjectiveId() != null) {
                specification = specification.and(buildSpecification(criteria.getObjectiveId(),
                    root -> root.join(Target_.objective, JoinType.LEFT).get(Objective_.id)));
            }
            if (criteria.getStartFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getStartFinancialYearId(),
                    root -> root.join(Target_.startFinancialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
            if (criteria.getEndFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getEndFinancialYearId(),
                    root -> root.join(Target_.endFinancialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(Target_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
        }
        return specification;
    }
}
