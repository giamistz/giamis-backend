package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.domain.engagement.EngagementObjectiveRisk_;
import gov.giamis.domain.engagement.InternalControl_;
import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.domain.risk.RiskControlMatrix_;
import gov.giamis.repository.risk.RiskControlMatrixRepository;
import gov.giamis.service.dto.RiskControlMatrixCriteria;

/**
 * Service for executing complex queries for {@link RiskControlMatrix} entities in the database.
 * The main input is a {@link RiskControlMatrixCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskControlMatrix} or a {@link Page} of {@link RiskControlMatrix} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskControlMatrixQueryService extends QueryService<RiskControlMatrix> {

    private final Logger log = LoggerFactory.getLogger(RiskControlMatrixQueryService.class);

    private final RiskControlMatrixRepository riskControlMatrixRepository;

    public RiskControlMatrixQueryService(RiskControlMatrixRepository riskControlMatrixRepository) {
        this.riskControlMatrixRepository = riskControlMatrixRepository;
    }

    /**
     * Return a {@link List} of {@link RiskControlMatrix} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskControlMatrix> findByCriteria(RiskControlMatrixCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskControlMatrix> specification = createSpecification(criteria);
        return riskControlMatrixRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskControlMatrix} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskControlMatrix> findByCriteria(RiskControlMatrixCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskControlMatrix> specification = createSpecification(criteria);
        return riskControlMatrixRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskControlMatrixCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskControlMatrix> specification = createSpecification(criteria);
        return riskControlMatrixRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskControlMatrix> createSpecification(RiskControlMatrixCriteria criteria) {
        Specification<RiskControlMatrix> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RiskControlMatrix_.id));
            }
            if (criteria.getIsKeyControl() != null) {
                specification = specification.and(buildSpecification(criteria.getIsKeyControl(), RiskControlMatrix_.isKeyControl));
            }
            if (criteria.getEngagementObjectiveRiskId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementObjectiveRiskId(),
                    root -> root.join(RiskControlMatrix_.engagementObjectiveRisk, JoinType.LEFT).get(EngagementObjectiveRisk_.id)));
            }
            if (criteria.getControlStepId() != null) {
                specification = specification.and(buildSpecification(criteria.getControlStepId(),
                    root -> root.join(RiskControlMatrix_.internalControl, JoinType.LEFT).get(InternalControl_.id)));
            }
        }
        return specification;
    }

    public List<RiskControlMatrix> findByEngagementWithProcedures(Long engagementId) {
        return riskControlMatrixRepository.findAllWithProcedures(engagementId);
    }
}
