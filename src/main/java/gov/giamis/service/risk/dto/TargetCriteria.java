package gov.giamis.service.risk.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.risk.Target;
import gov.giamis.web.rest.risk.TargetResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link Target} entity. This class is used
 * in {@link TargetResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /targets?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TargetCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private LongFilter objectiveId;

    private LongFilter startFinancialYearId;

    private LongFilter endFinancialYearId;

    private LongFilter organisationUnitId;

    public TargetCriteria(){
    }

    public TargetCriteria(TargetCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.objectiveId = other.objectiveId == null ? null : other.objectiveId.copy();
        this.startFinancialYearId = other.startFinancialYearId == null ? null : other.startFinancialYearId.copy();
        this.endFinancialYearId = other.endFinancialYearId == null ? null : other.endFinancialYearId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
    }

    @Override
    public TargetCriteria copy() {
        return new TargetCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public LongFilter getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(LongFilter objectiveId) {
        this.objectiveId = objectiveId;
    }

    public LongFilter getStartFinancialYearId() {
        return startFinancialYearId;
    }

    public void setStartFinancialYearId(LongFilter startFinancialYearId) {
        this.startFinancialYearId = startFinancialYearId;
    }

    public LongFilter getEndFinancialYearId() {
        return endFinancialYearId;
    }

    public void setEndFinancialYearId(LongFilter endFinancialYearId) {
        this.endFinancialYearId = endFinancialYearId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TargetCriteria that = (TargetCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(objectiveId, that.objectiveId) &&
            Objects.equals(startFinancialYearId, that.startFinancialYearId) &&
            Objects.equals(endFinancialYearId, that.endFinancialYearId) &&
            Objects.equals(organisationUnitId, that.organisationUnitId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        objectiveId,
        startFinancialYearId,
        endFinancialYearId,
        organisationUnitId
        );
    }

    @Override
    public String toString() {
        return "TargetCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (objectiveId != null ? "objectiveId=" + objectiveId + ", " : "") +
                (startFinancialYearId != null ? "startFinancialYearId=" + startFinancialYearId + ", " : "") +
                (endFinancialYearId != null ? "endFinancialYearId=" + endFinancialYearId + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
            "}";
    }

}
