package gov.giamis.service.risk.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.risk.RiskRegister;
import gov.giamis.web.rest.risk.RiskRegisterResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link RiskRegister} entity. This class is used
 * in {@link RiskRegisterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /risk-registers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RiskRegisterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter financialYearId;

    private LongFilter organisationUnitId;

    public RiskRegisterCriteria(){
    }

    public RiskRegisterCriteria(RiskRegisterCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
    }

    @Override
    public RiskRegisterCriteria copy() {
        return new RiskRegisterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(LongFilter financialYearId) {
        this.financialYearId = financialYearId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RiskRegisterCriteria that = (RiskRegisterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(financialYearId, that.financialYearId) &&
            Objects.equals(organisationUnitId, that.organisationUnitId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        financialYearId,
        organisationUnitId
        );
    }

    @Override
    public String toString() {
        return "RiskRegisterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (financialYearId != null ? "financialYearId=" + financialYearId + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
            "}";
    }

}
