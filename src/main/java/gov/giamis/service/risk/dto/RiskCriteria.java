package gov.giamis.service.risk.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.risk.Risk;
import gov.giamis.web.rest.risk.RiskResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.RiskSource;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link Risk} entity. This class is used
 * in {@link RiskResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /risks?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RiskCriteria implements Serializable, Criteria {
    /**
     * Class for filtering RiskSource
     */
    public static class RiskSourceFilter extends Filter<RiskSource> {

        public RiskSourceFilter() {
        }

        public RiskSourceFilter(RiskSourceFilter filter) {
            super(filter);
        }

        @Override
        public RiskSourceFilter copy() {
            return new RiskSourceFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private RiskSourceFilter source;

    private BooleanFilter completed;

    private LongFilter riskRatingId;

    private LongFilter controlId;

    private LongFilter riskRegisterId;

    private LongFilter targetId;

    private LongFilter organisationUnitId;

    private LongFilter financialYearId;

    private LongFilter riskCategoryId;

    public RiskCriteria(){
    }

    public RiskCriteria(RiskCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.source = other.source == null ? null : other.source.copy();
        this.completed = other.completed == null ? null : other.completed.copy();
        this.riskRatingId = other.riskRatingId == null ? null : other.riskRatingId.copy();
        this.controlId = other.controlId == null ? null : other.controlId.copy();
        this.riskRegisterId = other.riskRegisterId == null ? null : other.riskRegisterId.copy();
        this.targetId = other.targetId == null ? null : other.targetId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.riskCategoryId = other.riskCategoryId == null ? null : other.riskCategoryId.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
    }

    @Override
    public RiskCriteria copy() {
        return new RiskCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public RiskSourceFilter getSource() {
        return source;
    }

    public void setSource(RiskSourceFilter source) {
        this.source = source;
    }

    public BooleanFilter getCompleted() {
        return completed;
    }

    public void setCompleted(BooleanFilter completed) {
        this.completed = completed;
    }

    public LongFilter getRiskRatingId() {
        return riskRatingId;
    }

    public void setRiskRatingId(LongFilter riskRatingId) {
        this.riskRatingId = riskRatingId;
    }

    public LongFilter getControlId() {
        return controlId;
    }

    public void setControlId(LongFilter controlId) {
        this.controlId = controlId;
    }

    public LongFilter getRiskRegisterId() {
        return riskRegisterId;
    }

    public void setRiskRegisterId(LongFilter riskRegisterId) {
        this.riskRegisterId = riskRegisterId;
    }

    public LongFilter getTargetId() {
        return targetId;
    }

    public void setTargetId(LongFilter targetId) {
        this.targetId = targetId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getRiskCategoryId() {
        return riskCategoryId;
    }

    public void setRiskCategoryId(LongFilter riskCategoryId) {
        this.riskCategoryId = riskCategoryId;
    }

    public LongFilter getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(LongFilter financialYearId) {
        this.financialYearId = financialYearId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RiskCriteria that = (RiskCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(source, that.source) &&
            Objects.equals(completed, that.completed) &&
            Objects.equals(riskRatingId, that.riskRatingId) &&
            Objects.equals(controlId, that.controlId) &&
            Objects.equals(riskRegisterId, that.riskRegisterId) &&
            Objects.equals(targetId, that.targetId) &&
            Objects.equals(organisationUnitId, that.organisationUnitId) &&
            Objects.equals(riskCategoryId, that.riskCategoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        source,
        completed,
            riskRatingId,
            controlId,
        riskRegisterId,
        targetId,
        organisationUnitId,
        riskCategoryId
        );
    }

    @Override
    public String toString() {
        return "RiskCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (source != null ? "source=" + source + ", " : "") +
                (completed != null ? "completed=" + completed + ", " : "") +
                (riskRatingId != null ? "riskRatingId=" + riskRatingId + ", " : "") +
                (controlId != null ? "controlId=" + controlId + ", " : "") +
                (riskRegisterId != null ? "riskRegisterId=" + riskRegisterId + ", " : "") +
                (targetId != null ? "targetId=" + targetId + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
                (riskCategoryId != null ? "riskCategoryId=" + riskCategoryId + ", " : "") +
            "}";
    }

}
