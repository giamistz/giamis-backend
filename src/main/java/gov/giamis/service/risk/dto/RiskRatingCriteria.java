package gov.giamis.service.risk.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.risk.RiskRating;
import gov.giamis.web.rest.risk.RiskRatingResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.RiskSource;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link RiskRating} entity. This class is used
 * in {@link RiskRatingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RiskRatingCriteria implements Serializable, Criteria {
    /**
     * Class for filtering RiskSource
     */
    public static class RiskSourceFilter extends Filter<RiskSource> {

        public RiskSourceFilter() {
        }

        public RiskSourceFilter(RiskSourceFilter filter) {
            super(filter);
        }

        @Override
        public RiskSourceFilter copy() {
            return new RiskSourceFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter impact;

    private IntegerFilter likelihood;

    private RiskSourceFilter source;

    private LongFilter riskId;

    public RiskRatingCriteria(){
    }

    public RiskRatingCriteria(RiskRatingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.impact = other.impact == null ? null : other.impact.copy();
        this.likelihood = other.likelihood == null ? null : other.likelihood.copy();
        this.source = other.source == null ? null : other.source.copy();
        this.riskId = other.riskId == null ? null : other.riskId.copy();
    }

    @Override
    public RiskRatingCriteria copy() {
        return new RiskRatingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getImpact() {
        return impact;
    }

    public void setImpact(IntegerFilter impact) {
        this.impact = impact;
    }

    public IntegerFilter getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(IntegerFilter likelihood) {
        this.likelihood = likelihood;
    }

    public RiskSourceFilter getSource() {
        return source;
    }

    public void setSource(RiskSourceFilter source) {
        this.source = source;
    }

    public LongFilter getRiskId() {
        return riskId;
    }

    public void setRiskId(LongFilter riskId) {
        this.riskId = riskId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RiskRatingCriteria that = (RiskRatingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(impact, that.impact) &&
            Objects.equals(likelihood, that.likelihood) &&
            Objects.equals(source, that.source) &&
            Objects.equals(riskId, that.riskId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        impact,
        likelihood,
        source,
        riskId
        );
    }

    @Override
    public String toString() {
        return "RiskRatingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (impact != null ? "impact=" + impact + ", " : "") +
                (likelihood != null ? "likelihood=" + likelihood + ", " : "") +
                (source != null ? "source=" + source + ", " : "") +
                (riskId != null ? "riskId=" + riskId + ", " : "") +
            "}";
    }

}
