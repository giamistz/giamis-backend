package gov.giamis.service.risk;

import gov.giamis.domain.risk.Risk;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Risk}.
 */
public interface RiskService {

    /**
     * Save a risk.
     *
     * @param risk the entity to save.
     * @return the persisted entity.
     */
    Risk save(Risk risk);

    /**
     * Get all the risks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Risk> findAll(Pageable pageable);


    /**
     * Get the "id" risk.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Risk> findOne(Long id);

    /**
     * Delete the "id" risk.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    Risk findByCode(String code);
    
    void updateRisksByOrganisationUnitId(Long organisationUnitId);
    
    void updateRisksByIdAndOrganisationUnitId(Long id,Long organisationUnitId);
    
    List<Risk> findRiskByOrganisationUnitId(Long id);
    
    List<Risk> findRiskByIdAndOrganisationUnitId(Long id,Long organisationUnitId);
    
    List<Risk> findRiskByRiskRegisterId(Long id);

	void updateRisksByRiskRegisterId(Long id);
	
	List<Risk> findRiskByIdAndRiskRegisterId(Long id,Long riskRegisterId);

	void updateRisksByIdAndRiskRegisterId(Long id, Long riskRegisterId);

	String getNextCode(Risk risk);
}
