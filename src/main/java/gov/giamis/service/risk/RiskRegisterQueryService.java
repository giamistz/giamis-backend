package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.risk.RiskRegister_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.risk.RiskRegister;
import gov.giamis.repository.risk.RiskRegisterRepository;
import gov.giamis.service.risk.dto.RiskRegisterCriteria;

/**
 * Service for executing complex queries for {@link RiskRegister} entities in the database.
 * The main input is a {@link RiskRegisterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskRegister} or a {@link Page} of {@link RiskRegister} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskRegisterQueryService extends QueryService<RiskRegister> {

    private final Logger log = LoggerFactory.getLogger(RiskRegisterQueryService.class);

    private final RiskRegisterRepository riskRegisterRepository;

    public RiskRegisterQueryService(RiskRegisterRepository riskRegisterRepository) {
        this.riskRegisterRepository = riskRegisterRepository;
    }

    /**
     * Return a {@link List} of {@link RiskRegister} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskRegister> findByCriteria(RiskRegisterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskRegister> specification = createSpecification(criteria);
        return riskRegisterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskRegister} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskRegister> findByCriteria(RiskRegisterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskRegister> specification = createSpecification(criteria);
        return riskRegisterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskRegisterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskRegister> specification = createSpecification(criteria);
        return riskRegisterRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskRegister> createSpecification(RiskRegisterCriteria criteria) {
        Specification<RiskRegister> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RiskRegister_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), RiskRegister_.name));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getFinancialYearId(),
                    root -> root.join(RiskRegister_.financialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(RiskRegister_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
        }
        return specification;
    }
}
