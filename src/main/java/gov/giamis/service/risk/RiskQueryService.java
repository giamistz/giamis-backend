package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.risk.*;
import gov.giamis.domain.setup.Control_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;
import gov.giamis.domain.setup.RiskCategory_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.risk.RiskRepository;
import gov.giamis.service.risk.dto.RiskCriteria;

/**
 * Service for executing complex queries for {@link Risk} entities in the database.
 * The main input is a {@link RiskCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Risk} or a {@link Page} of {@link Risk} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskQueryService extends QueryService<Risk> {

    private final Logger log = LoggerFactory.getLogger(RiskQueryService.class);

    private final RiskRepository riskRepository;

    public RiskQueryService(RiskRepository riskRepository) {
        this.riskRepository = riskRepository;
    }

    /**
     * Return a {@link List} of {@link Risk} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Risk> findByCriteria(RiskCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Risk> specification = createSpecification(criteria);
        return riskRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Risk} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Risk> findByCriteria(RiskCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Risk> specification = createSpecification(criteria);
        return riskRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Risk> specification = createSpecification(criteria);
        return riskRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Risk> createSpecification(RiskCriteria criteria) {
        Specification<Risk> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), Risk_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), Risk_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), Risk_.name));
            }
            if (criteria.getSource() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getSource(), Risk_.source));
            }
            if (criteria.getCompleted() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getCompleted(), Risk_.completed));
            }
            if (criteria.getRiskRatingId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getRiskRatingId(),
                    root -> root.join(Risk_.riskRatings, JoinType.LEFT)
                        .get(RiskRating_.id)));
            }
            
            if (criteria.getControlId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getControlId(),
                    root -> root.join(Risk_.controls, JoinType.LEFT)
                        .get(Control_.id)));
            }
            if (criteria.getRiskRegisterId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getRiskRegisterId(),
                    root -> root.join(Risk_.riskRegister, JoinType.LEFT)
                        .get(RiskRegister_.id)));
            }
            if (criteria.getTargetId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getTargetId(),
                    root -> root.join(Risk_.target, JoinType.LEFT)
                        .get(Target_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitId(),
                    root -> root.join(Risk_.organisationUnit, JoinType.INNER)
                        .get(OrganisationUnit_.id)));
            }
            if (criteria.getRiskCategoryId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getRiskCategoryId(),
                    root -> root.join(Risk_.riskCategory, JoinType.LEFT)
                        .get(RiskCategory_.id)));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getFinancialYearId(),
                    root -> root.join(Risk_.riskRegister, JoinType.INNER)
                        .join(RiskRegister_.financialYear, JoinType.INNER)
                        .get(FinancialYear_.id)));
            }
            
        }
        return specification;
    }
}
