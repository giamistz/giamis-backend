package gov.giamis.service.risk;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.risk.RiskControlReport;
import gov.giamis.domain.risk.RiskControlReport_;
import gov.giamis.repository.risk.RiskControlReportRepository;
import gov.giamis.service.dto.RiskControlReportCriteria;

/**
 * Service for executing complex queries for {@link RiskControlReport} entities in the database.
 * The main input is a {@link RiskControlReportCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskControlReport} or a {@link Page} of {@link RiskControlReport} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskControlReportQueryService extends QueryService<RiskControlReport> {

    private final Logger log = LoggerFactory.getLogger(RiskControlReportQueryService.class);

    private final RiskControlReportRepository riskControlReportRepository;

    public RiskControlReportQueryService(RiskControlReportRepository riskControlReportRepository) {
        this.riskControlReportRepository = riskControlReportRepository;
    }

    /**
     * Return a {@link List} of {@link RiskControlReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskControlReport> findByCriteria(RiskControlReportCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskControlReport> specification = createSpecification(criteria);
        return riskControlReportRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskControlReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskControlReport> findByCriteria(RiskControlReportCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskControlReport> specification = createSpecification(criteria);
        return riskControlReportRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskControlReportCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskControlReport> specification = createSpecification(criteria);
        return riskControlReportRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskControlReport> createSpecification(RiskControlReportCriteria criteria) {
        Specification<RiskControlReport> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RiskControlReport_.id));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(RiskControlReport_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }
}
