package gov.giamis.service.risk;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.risk.RiskControlMatrix;

import java.util.Optional;

/**
 * Service Interface for managing {@link RiskControlMatrix}.
 */
public interface RiskControlMatrixService {

    /**
     * Save a riskControlMatrix.
     *
     * @param riskControlMatrix the entity to save.
     * @return the persisted entity.
     */
    RiskControlMatrix save(RiskControlMatrix riskControlMatrix);

    /**
     * Get all the riskControlMatrices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RiskControlMatrix> findAll(Pageable pageable);


    /**
     * Get the "id" riskControlMatrix.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RiskControlMatrix> findOne(Long id);

    /**
     * Delete the "id" riskControlMatrix.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    Long countRiskControlMatrix();
}
