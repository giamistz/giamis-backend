package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.MeetingDeliverable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MeetingDeliverable}.
 */
public interface MeetingDeliverableService {

    /**
     * Save a MeetingDeliverable.
     *
     * @param MeetingDeliverable the entity to save.
     * @return the persisted entity.
     */
    MeetingDeliverable save(MeetingDeliverable meetingDeliverable);

    /**
     * Get all the MeetingDeliverables.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MeetingDeliverable> findAll(Pageable pageable);


    /**
     * Get the "id" MeetingDeliverable.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MeetingDeliverable> findOne(Long id);

    /**
     * Delete the "id" MeetingDeliverable.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
