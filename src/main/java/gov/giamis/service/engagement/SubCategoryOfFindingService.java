package gov.giamis.service.engagement;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.SubCategoryOfFinding;
import java.util.Optional;

/**
 * Service Interface for managing {@link SubCategoryOfFinding}.
 */
public interface SubCategoryOfFindingService {

    /**
     * Save a subCategoryOfFinding.
     *
     * @param subCategoryOfFinding the entity to save.
     * @return the persisted entity.
     */
    SubCategoryOfFinding save(SubCategoryOfFinding subCategoryOfFinding);

    /**
     * Get all the subCategoryOfFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubCategoryOfFinding> findAll(Pageable pageable);


    /**
     * Get the "id" subCategoryOfFinding.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SubCategoryOfFinding> findOne(Long id);

    /**
     * Delete the "id" subCategoryOfFinding.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    SubCategoryOfFinding findByName(String name);
}
