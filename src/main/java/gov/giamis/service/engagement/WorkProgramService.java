package gov.giamis.service.engagement;


import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.dto.InternalControlDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.WorkProgram;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link WorkProgram}.
 */
public interface WorkProgramService {

    /**
     * Save a workProgram.
     *
     * @param workProgram the entity to save.
     * @return the persisted entity.
     */
    WorkProgram save(WorkProgram workProgram);

    /**
     * Get all the workPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WorkProgram> findAll(Pageable pageable);


    /**
     * Get the "id" workProgram.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WorkProgram> findOne(Long id);

    /**
     * Delete the "id" workProgram.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    WorkProgram findByCode(String code);
    
    List<WorkProgram> findByEngagementProcedure(Long id);
    
    void updateWorkProgram(String name,Long id);

    WorkProgram getOrCreate(String name, EngagementProcedure procedure);
}
