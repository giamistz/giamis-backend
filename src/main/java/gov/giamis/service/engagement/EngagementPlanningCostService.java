package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementPlanningCost;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementPlanningCost}.
 */
public interface EngagementPlanningCostService {

    /**
     * Save a engagementPlanningCost.
     *
     * @param engagementPlanningCost the entity to save.
     * @return the persisted entity.
     */
    EngagementPlanningCost save(EngagementPlanningCost engagementPlanningCost);

    /**
     * Get all the engagementPlanningCosts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementPlanningCost> findAll(Pageable pageable);


    /**
     * Get the "id" engagementPlanningCost.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementPlanningCost> findOne(Long id);

    /**
     * Delete the "id" engagementPlanningCost.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    EngagementPlanningCost findByEngagementId(Long id);

    EngagementPlanningCost findByGfsCodeId(Long id);
}