package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementObjectiveRisk;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementObjectiveRisk}.
 */
public interface EngagementObjectiveRiskService {

    /**
     * Save a engagementObjectiveRisk.
     *
     * @param engagementObjectiveRisk the entity to save.
     * @return the persisted entity.
     */
    EngagementObjectiveRisk save(EngagementObjectiveRisk engagementObjectiveRisk);

    /**
     * Get all the engagementObjectiveRisks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementObjectiveRisk> findAll(Pageable pageable);


    /**
     * Get the "id" engagementObjectiveRisk.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementObjectiveRisk> findOne(Long id);

    /**
     * Delete the "id" engagementObjectiveRisk.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

}
