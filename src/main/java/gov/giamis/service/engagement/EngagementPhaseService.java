package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementPhase;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementPhase}.
 */
public interface EngagementPhaseService {

    /**
     * Save a engagementPhase.
     *
     * @param engagementPhase the entity to save.
     * @return the persisted entity.
     */
    EngagementPhase save(EngagementPhase engagementPhase);

    /**
     * Get all the engagementPhases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementPhase> findAll(Pageable pageable);


    /**
     * Get the "id" engagementPhase.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementPhase> findOne(Long id);

    /**
     * Delete the "id" engagementPhase.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    EngagementPhase findByName(String name);
}
