
package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import gov.giamis.repository.engagement.SpecificObjectiveRepository;
import gov.giamis.service.engagement.dto.SpecificObjectiveCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link SpecificObjective} entities in the database.
 * The main input is a {@link SpecificObjectiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpecificObjective} or a {@link Page} of {@link SpecificObjective} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpecificObjectiveQueryService extends QueryService<SpecificObjective> {

    private final Logger log = LoggerFactory.getLogger(SpecificObjectiveQueryService.class);

    private final SpecificObjectiveRepository specificObjectiveRepository;

    public SpecificObjectiveQueryService(SpecificObjectiveRepository specificObjectiveRepository) {
        this.specificObjectiveRepository = specificObjectiveRepository;
    }

    /**
     * Return a {@link List} of {@link SpecificObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpecificObjective> findByCriteria(SpecificObjectiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SpecificObjective> specification = createSpecification(criteria);
        return specificObjectiveRepository.findAll(specification);
    }

    public Page<SpecificObjective> findByEngagement(Long engagementId, Pageable pageable) {
        return specificObjectiveRepository.findByEngagement_Id(engagementId, pageable);
    }

    @Transactional(readOnly = true)
    public List<SpecificObjective> findByEngagementWithRisk(Long engagementId) {
        log.debug("find by criteria : eng {}", engagementId);
        return specificObjectiveRepository.findAllWithRisks(engagementId);
    }

    /**
     * Return a {@link Page} of {@link SpecificObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpecificObjective> findByCriteria(SpecificObjectiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SpecificObjective> specification = createSpecification(criteria);
        return specificObjectiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpecificObjectiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SpecificObjective> specification = createSpecification(criteria);
        return specificObjectiveRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<SpecificObjective> createSpecification(SpecificObjectiveCriteria criteria) {
        Specification<SpecificObjective> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SpecificObjective_.id));
            }
            if (criteria.getIsRefined() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRefined(), SpecificObjective_.isRefined));
            }
            
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getEngagementId(),
                    root -> root.join(SpecificObjective_.engagement, JoinType.LEFT)
                        .get(Engagement_.id)));
            }
        }
        return specification;
    }


}
