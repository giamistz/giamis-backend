package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementIntermMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementIntermMeeting}.
 */
public interface EngagementIntermMeetingService {

    /**
     * Save a engagementIntermMeeting.
     *
     * @param engagementIntermMeeting the entity to save.
     * @return the persisted entity.
     */
    EngagementIntermMeeting save(EngagementIntermMeeting engagementIntermMeeting);

    /**
     * Get all the engagementIntermMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementIntermMeeting> findAll(Pageable pageable);


    /**
     * Get the "id" engagementIntermMeeting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementIntermMeeting> findOne(Long id);

    /**
     * Delete the "id" engagementIntermMeeting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
