package gov.giamis.service.engagement;

import gov.giamis.dto.WalkThroughDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.InternalControl;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link InternalControl}.
 */
public interface InternalControlService {

    /**
     * Save a internalControl.
     *
     * @param internalControl the entity to save.
     * @return the persisted entity.
     */
    InternalControl save(InternalControl internalControl);

    /**
     * Get all the internalControls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<InternalControl> findAll(Pageable pageable);


    /**
     * Get the "id" internalControl.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<InternalControl> findOne(Long id);

    /**
     * Delete the "id" internalControl.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    Optional<InternalControl> findLastRecord(Long id);

    void createWalkThrough(WalkThroughDTO walkThrough);
}
