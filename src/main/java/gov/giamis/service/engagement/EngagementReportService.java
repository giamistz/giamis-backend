package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementReport;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementReport}.
 */
public interface EngagementReportService {

    /**
     * Save a engagementReport.
     *
     * @param engagementReport the entity to save.
     * @return the persisted entity.
     */
    EngagementReport save(EngagementReport engagementReport);

    /**
     * Get all the engagementReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementReport> findAll(Pageable pageable);


    /**
     * Get the "id" engagementReport.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementReport> findOne(Long id);

    /**
     * Delete the "id" engagementReport.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
