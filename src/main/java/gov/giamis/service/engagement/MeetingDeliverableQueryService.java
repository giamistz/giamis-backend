package gov.giamis.service.engagement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.MeetingDeliverable;
import gov.giamis.domain.engagement.MeetingDeliverable_;
import gov.giamis.repository.engagement.MeetingDeliverableRepository;
import gov.giamis.service.engagement.dto.MeetingDeliverableCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MeetingDeliverable} entities in the database.
 * The main input is a {@link MeetingDeliverableCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MeetingDeliverable} or a {@link Page} of {@link MeetingDeliverable} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MeetingDeliverableQueryService extends QueryService<MeetingDeliverable> {

    private final Logger log = LoggerFactory.getLogger(MeetingDeliverableQueryService.class);

    private final MeetingDeliverableRepository meetingDeliverableRepository;

    public MeetingDeliverableQueryService(MeetingDeliverableRepository meetingDeliverableRepository) {
        this.meetingDeliverableRepository = meetingDeliverableRepository;
    }

    /**
     * Return a {@link List} of {@link MeetingDeliverable} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MeetingDeliverable> findByCriteria(MeetingDeliverableCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MeetingDeliverable> specification = createSpecification(criteria);
        return meetingDeliverableRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MeetingDeliverable} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MeetingDeliverable> findByCriteria(MeetingDeliverableCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MeetingDeliverable> specification = createSpecification(criteria);
        return meetingDeliverableRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MeetingDeliverableCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MeetingDeliverable> specification = createSpecification(criteria);
        return meetingDeliverableRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<MeetingDeliverable> createSpecification(MeetingDeliverableCriteria criteria) {
        Specification<MeetingDeliverable> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MeetingDeliverable_.id));
            }
            if (criteria.getDeliverable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeliverable(), MeetingDeliverable_.deliverable));
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(),MeetingDeliverable_.date));
            }

        }
        return specification;
    }
}
