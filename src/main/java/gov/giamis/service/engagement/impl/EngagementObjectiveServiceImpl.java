package gov.giamis.service.engagement.impl;

import gov.giamis.service.engagement.EngagementObjectiveService;
import gov.giamis.domain.engagement.EngagementObjective;
import gov.giamis.repository.engagement.EngagementObjectiveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementObjective}.
 */
@Service
@Transactional
public class EngagementObjectiveServiceImpl implements EngagementObjectiveService {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveServiceImpl.class);

    private final EngagementObjectiveRepository engagementObjectiveRepository;

    public EngagementObjectiveServiceImpl(EngagementObjectiveRepository engagementObjectiveRepository) {
        this.engagementObjectiveRepository = engagementObjectiveRepository;
    }

    /**
     * Save a engagementObjective.
     *
     * @param engagementObjective the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementObjective save(EngagementObjective engagementObjective) {
        log.debug("Request to save EngagementObjective : {}", engagementObjective);
        return engagementObjectiveRepository.save(engagementObjective);
    }

    /**
     * Get all the engagementObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementObjective> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementObjectives");
        return engagementObjectiveRepository.findAll(pageable);
    }


    /**
     * Get one engagementObjective by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementObjective> findOne(Long id) {
        log.debug("Request to get EngagementObjective : {}", id);
        return engagementObjectiveRepository.findById(id);
    }

    /**
     * Delete the engagementObjective by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementObjective : {}", id);
        engagementObjectiveRepository.deleteById(id);
    }
}
