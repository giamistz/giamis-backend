package gov.giamis.service.engagement.impl;

import java.util.List;
import java.util.Optional;

import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.dto.InternalControlDTO;
import gov.giamis.repository.risk.RiskControlMatrixRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.repository.engagement.EngagementProcedureRepository;
import gov.giamis.service.engagement.EngagementProcedureService;

/**
 * Service Implementation for managing {@link EngagementProcedure}.
 */
@Service
@Transactional
public class EngagementProcedureServiceImpl implements EngagementProcedureService {

    private final Logger log = LoggerFactory.getLogger(EngagementProcedureServiceImpl.class);

    private final EngagementProcedureRepository engagementProcedureRepository;

    private final RiskControlMatrixRepository matrixRepository;
   

    public EngagementProcedureServiceImpl(EngagementProcedureRepository engagementProcedureRepository,
                                          RiskControlMatrixRepository matrixRepository) {
        this.engagementProcedureRepository = engagementProcedureRepository;
        this.matrixRepository = matrixRepository;
    }

    /**
     * Save a engagementProcedure.
     *
     * @param engagementProcedure the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementProcedure save(EngagementProcedure engagementProcedure) {
        log.debug("Request to save EngagementProcedure : {}", engagementProcedure);
        if (engagementProcedure.getId() == null || engagementProcedure.getCode() == null) {
            Long matrixId = engagementProcedure.getRiskControlMatrix().getId();
            RiskControlMatrix matrix = matrixRepository.getOne(matrixId);
            long count = engagementProcedureRepository
                .countBySpecificObjective(matrix
                    .getEngagementObjectiveRisk()
                    .getSpecificObjective()
                    .getId());
            count = ++count;
            String code = matrix
                .getEngagementObjectiveRisk()
                .getSpecificObjective().getId()
                .toString();

            String objCode = matrix.getEngagementObjectiveRisk().getSpecificObjective().getCode();
            objCode = objCode == null ? "0" : objCode;
            String wprF = "B".concat(objCode).concat("-").concat(String.valueOf(count));

            code = code.concat(".").concat(String.valueOf(count));

            engagementProcedure.setWprf(wprF);
            engagementProcedure.setCode(code.concat(".").concat(String.valueOf(count)));
        }
        return engagementProcedureRepository.save(engagementProcedure);
    }

    /**
     * Get all the engagementProcedures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementProcedure> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementProcedures");
        return engagementProcedureRepository.findAll(pageable);
    }


    /**
     * Get one engagementProcedure by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementProcedure> findOne(Long id) {
        log.debug("Request to get EngagementProcedure : {}", id);
        return engagementProcedureRepository.findById(id);
    }

    /**
     * Delete the engagementProcedure by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementProcedure : {}", id);
        engagementProcedureRepository.deleteById(id);
    }

	@Override
	public List<EngagementProcedure> findBySpecificObjectiveId(Long id) {
		 log.debug("Request to get EngagementProcedure by Specific Objective ID : {}", id);
		return engagementProcedureRepository.findBySpecificObjective(id);
	}

	@Override
	public void update(EngagementProcedure engagementProcedure) {
		 log.debug("Request to update EngagementProcedure by ID : {}", engagementProcedure);
		engagementProcedureRepository.save(engagementProcedure);
		
	}

    @Override
    public EngagementProcedure getOrCreate(InternalControlDTO c) {
        String name = "Walk through ".concat(c.getWtRef());
        EngagementProcedure existing = engagementProcedureRepository
            .findFirstByInternalControl_IdAndName(c.getId(), name);
        if (existing != null) {
            return existing;
        }
        return engagementProcedureRepository.save(new EngagementProcedure(new InternalControl(c.getId()), name, true));
    }


}
