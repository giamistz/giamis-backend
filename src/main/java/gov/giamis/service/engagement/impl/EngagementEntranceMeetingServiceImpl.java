package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementEntranceMeeting;
import gov.giamis.repository.engagement.EngagementEntranceMeetingRepository;
import gov.giamis.service.engagement.EngagementEntranceMeetingService;

/**
 * Service Implementation for managing {@link EngagementEntranceMeeting}.
 */
@Service
@Transactional
public class EngagementEntranceMeetingServiceImpl implements EngagementEntranceMeetingService {

    private final Logger log = LoggerFactory.getLogger(EngagementEntranceMeetingServiceImpl.class);

    private final EngagementEntranceMeetingRepository engagementEntranceMeetingRepository;
    
    public EngagementEntranceMeetingServiceImpl(
            EngagementEntranceMeetingRepository engagementEntranceMeetingRepository) {
        this.engagementEntranceMeetingRepository = engagementEntranceMeetingRepository;
    }

    /**
     * Save a engagementEntranceMeeting.
     *
     * @param engagementEntranceMeeting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementEntranceMeeting save(EngagementEntranceMeeting engagementEntranceMeeting) {
        log.debug("Request to save Engagement Entrance Meeting : {}", engagementEntranceMeeting);
        return engagementEntranceMeetingRepository.save(engagementEntranceMeeting);
    }

    /**
     * Get all the engagementEntranceMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementEntranceMeeting> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Entrance Meetings");
        return engagementEntranceMeetingRepository.findAll(pageable);
    }


    /**
     * Get one engagementEntranceMeeting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementEntranceMeeting> findOne(Long id) {
        log.debug("Request to get Engagement Entrance Meeting : {}", id);
        return engagementEntranceMeetingRepository.findById(id);
    }

    /**
     * Delete the engagementEntranceMeeting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Entrance Meeting : {}", id);
        engagementEntranceMeetingRepository.deleteById(id);
    }
    
    @Override
	public EngagementEntranceMeeting findByEngagementId(Long id) {
		log.debug("Request to get Engagement Entrance Meeting by Engagement ID : {}", id);
		return engagementEntranceMeetingRepository.findByEngagement_Id(id);
	}

	@Override
	public Long countEngagementEntranceMeetingByEngagementId(Long id) {
		log.debug("Request to count Engagement Entrance Meeting by Engagement ID : {}", id);
		return engagementEntranceMeetingRepository.countEngagementEntranceMeetingByEngagementId(id);
	}
}
