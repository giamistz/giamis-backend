package gov.giamis.service.engagement.impl;

import java.util.List;
import java.util.Optional;

import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.dto.InternalControlDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.domain.engagement.WorkProgram;
import gov.giamis.repository.engagement.EngagementProcedureRepository;
import gov.giamis.repository.engagement.WorkProgramRepository;
import gov.giamis.service.engagement.WorkProgramService;

/**
 * Service Implementation for managing {@link WorkProgram}.
 */
@Service
@Transactional
public class WorkProgramServiceImpl implements WorkProgramService {

    private final Logger log = LoggerFactory.getLogger(WorkProgramServiceImpl.class);

    private final WorkProgramRepository workProgramRepository;

    private final EngagementProcedureRepository engagementProcedureRepository;

    public WorkProgramServiceImpl(WorkProgramRepository workProgramRepository,
    		EngagementProcedureRepository engagementProcedureRepository) {
        this.workProgramRepository = workProgramRepository;
        this.engagementProcedureRepository = engagementProcedureRepository;
    }

    /**
     * Save a workProgram.
     *
     * @param workProgram the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WorkProgram save(WorkProgram workProgram) {
        log.debug("Request to save Work Program : {}", workProgram);

       EngagementProcedure engagementProcedure = engagementProcedureRepository.findFirstById(workProgram.getEngagementProcedure().getId());
        Long countWorkProgram = workProgramRepository.countWorkProgramByEngagementProcedure(workProgram.getEngagementProcedure().getId())+1;;

        String newGeneratedCode = engagementProcedure.getCode().toString()+"."+countWorkProgram.toString();
        workProgram.setCode(newGeneratedCode);
        return workProgramRepository.save(workProgram);
    }

    /**
     * Get all the workPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WorkProgram> findAll(Pageable pageable) {
        log.debug("Request to get all Work Programs");
        return workProgramRepository.findAll(pageable);
    }


    /**
     * Get one workProgram by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WorkProgram> findOne(Long id) {
        log.debug("Request to get Work Program : {}", id);
        return workProgramRepository.findById(id);
    }

    /**
     * Delete the workProgram by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Work Program : {}", id);
        workProgramRepository.deleteById(id);
    }

	@Override
	public WorkProgram findByCode(String code) {
		log.debug("Request to get Work Program By code : {}", code);
		return workProgramRepository.findByCodeIgnoreCase(code);
	}

	@Override
	public List<WorkProgram> findByEngagementProcedure(Long id) {
		 log.debug("Request to get Work Program by Engagement Procedure ID : {}", id);
		return workProgramRepository.findByEngagementProcedure_Id(id);
	}

	@Override
	public void updateWorkProgram(String name, Long id) {
		 log.debug("Request to update Work Program by ID : {}", id);
		 workProgramRepository.updateWorkProgram(name, id);
	}

    @Override
    public WorkProgram getOrCreate(String name, EngagementProcedure procedure) {
        WorkProgram existing = workProgramRepository.findFirstByEngagementProcedure_IdAndName(procedure.getId(), name);
        if (existing != null) {
            return existing;
        }
        return workProgramRepository.save(new WorkProgram(procedure, name));
    }


}
