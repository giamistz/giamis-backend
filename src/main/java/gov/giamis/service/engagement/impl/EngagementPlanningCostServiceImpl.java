package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementPlanningCost;
import gov.giamis.repository.engagement.EngagementPlanningCostRepository;
import gov.giamis.service.engagement.EngagementPlanningCostService;

/**
 * Service Implementation for managing {@link EngagementPlanningCost}.
 */
@Service
@Transactional
public class EngagementPlanningCostServiceImpl implements EngagementPlanningCostService {

    private final Logger log = LoggerFactory.getLogger(EngagementPlanningCostServiceImpl.class);

    private final EngagementPlanningCostRepository engagementPlanningCostRepository;
    

    public EngagementPlanningCostServiceImpl(
            EngagementPlanningCostRepository engagementPlanningCostRepository) {
        this.engagementPlanningCostRepository = engagementPlanningCostRepository;
    }

    /**
     * Save a engagementPlanningCost.
     *
     * @param engagementPlanningCost the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementPlanningCost save(EngagementPlanningCost engagementPlanningCost) {
        log.debug("Request to save Engagement Planning Cost : {}", engagementPlanningCost);
        return engagementPlanningCostRepository.save(engagementPlanningCost);
    }

    /**
     * Get all the engagementPlanningCosts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementPlanningCost> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Planning Costs");
        return engagementPlanningCostRepository.findAll(pageable);
    }


    /**
     * Get one engagementPlanningCost by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementPlanningCost> findOne(Long id) {
        log.debug("Request to get Engagement Planning Cost : {}", id);
        return engagementPlanningCostRepository.findById(id);
    }

    /**
     * Delete the engagementPlanningCost by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Planning Cost : {}", id);
        engagementPlanningCostRepository.deleteById(id);
    }

    @Override
    public EngagementPlanningCost findByEngagementId(Long id) {
        log.debug("Request to get Engagement Planning Cost by Engagement ID : {}", id);
        return engagementPlanningCostRepository.findByEngagement_id(id);
    }

	@Override
	public EngagementPlanningCost findByGfsCodeId(Long id) {
		 log.debug("Request to get Engagement Planning Cost by GfsCode ID : {}", id);
	        return engagementPlanningCostRepository.findByGfsCode_id(id);
	}

}

