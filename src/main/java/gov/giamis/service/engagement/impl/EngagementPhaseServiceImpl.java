package gov.giamis.service.engagement.impl;

import gov.giamis.domain.engagement.EngagementPhase;
import gov.giamis.repository.engagement.EngagementPhaseRepository;
import gov.giamis.service.engagement.EngagementPhaseService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementPhase}.
 */
@Service
@Transactional
public class EngagementPhaseServiceImpl implements EngagementPhaseService {

    private final Logger log = LoggerFactory.getLogger(EngagementPhaseServiceImpl.class);

    private final EngagementPhaseRepository engagementPhaseRepository;

    public EngagementPhaseServiceImpl(EngagementPhaseRepository engagementPhaseRepository) {
        this.engagementPhaseRepository = engagementPhaseRepository;
    }

    /**
     * Save a engagementPhase.
     *
     * @param engagementPhase the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementPhase save(EngagementPhase engagementPhase) {
        log.debug("Request to save Engagement Phase : {}", engagementPhase);
        engagementPhase.setName(engagementPhase.getName().toUpperCase());
        return engagementPhaseRepository.save(engagementPhase);
    }

    /**
     * Get all the engagementPhases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementPhase> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Phases");
        return engagementPhaseRepository.findAll(pageable);
    }


    /**
     * Get one engagementPhase by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementPhase> findOne(Long id) {
        log.debug("Request to get Engagement Phase : {}", id);
        return engagementPhaseRepository.findById(id);
    }

    /**
     * Delete the engagementPhase by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Phase : {}", id);
        engagementPhaseRepository.deleteById(id);
    }

	@Override
	public EngagementPhase findByName(String name) {
		log.debug("Request to get Engagement Phase by name : {}", name);
        return engagementPhaseRepository.findByNameIgnoreCase(name);
	}
}
