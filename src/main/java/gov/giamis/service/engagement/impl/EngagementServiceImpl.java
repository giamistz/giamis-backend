package gov.giamis.service.engagement.impl;

import gov.giamis.domain.Notification;
import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.enumeration.LetterType;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.dto.ClientNotificationDTO;
import gov.giamis.dto.ConfirmGoDTO;
import gov.giamis.repository.NotificationRepository;
import gov.giamis.repository.engagement.EngagementMemberRepository;
import gov.giamis.service.MailService;
import gov.giamis.service.engagement.CommunicationLetterService;
import gov.giamis.service.engagement.EngagementService;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.repository.engagement.EngagementRepository;

import gov.giamis.service.report.EngagementTemplateService;
import gov.giamis.service.setup.FileResourceService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Engagement}.
 */
@Service
@Transactional
public class EngagementServiceImpl implements EngagementService {

    private final Logger log = LoggerFactory.getLogger(EngagementServiceImpl.class);

    private final EngagementRepository engagementRepository;

    private final EngagementTemplateService templateService;

    private final FileResourceService fileResourceService;

    private final NotificationRepository notificationRepository;

    private final MailService mailService;

    private final EngagementMemberRepository memberRepository;

    private final CommunicationLetterService letterService;


    public EngagementServiceImpl(EngagementRepository engagementRepository,
                                 EngagementTemplateService engagementTemplateService,
                                 FileResourceService fileResourceService,
                                 NotificationRepository notificationRepository,
                                 MailService mailService,
                                 EngagementMemberRepository memberRepository,
                                 CommunicationLetterService letterService) {
        this.engagementRepository = engagementRepository;
        this.templateService = engagementTemplateService;
        this.fileResourceService = fileResourceService;
        this.notificationRepository = notificationRepository;
        this.mailService = mailService;
        this.memberRepository = memberRepository;
        this.letterService = letterService;
    }

    /**
     * Save a engagement.
     *
     * @param engagement the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Engagement save(Engagement engagement) {
        log.debug("Request to save Engagement : {}", engagement);
        return engagementRepository.save(engagement);
    }

    /**
     * Get all the engagements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Engagement> findAll(Pageable pageable) {
        log.debug("Request to get all Engagements");
        return engagementRepository.findAll(pageable);
    }


    /**
     * Get one engagement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Engagement> findOne(Long id) {
        log.debug("Request to get Engagement : {}", id);
        return engagementRepository.findById(id);
    }

    /**
     * Delete the engagement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement : {}", id);
        engagementRepository.deleteById(id);
    }

    /**
     * Get all engagements by financial year  and Organisation Unit
     * * @param financialYearId = financialYearId
     * * @param organisationUnitId = organisationUnitId
     */

    @Override
    public List<Engagement> findByFinancialYearAndOrganisationUnit(Long financialYearId, Long organisationUnitId) {
        log.debug("Request to get all engagement by financial year and organisation unit : {}", financialYearId, organisationUnitId);
        return engagementRepository.findByFinancialYear_IdAndOrganisationUnit_Id(financialYearId, organisationUnitId);
    }

    @Override
    public Engagement confirmGoDecision(ConfirmGoDTO confirmGoDTO) {
        Engagement engagement = engagementRepository.getOne(confirmGoDTO.getEngagementId());
         engagement.setGo(confirmGoDTO.isGo());
         return engagementRepository.save(engagement);
    }

    @Override
    public CommunicationLetter createEngagementLetter(ClientNotificationDTO notificationDTO) {

        CommunicationLetter comLetter = letterService.findByTypeAndEngagement(
            notificationDTO.getType(),
            notificationDTO.getEngagementId());
        try {
            File letter = templateService.createClientLetter(comLetter);
            FileResource fileResource = fileResourceService.upload(letter,
                comLetter.getSubject(),
                "engagement"+notificationDTO.getEngagementId());
            comLetter.setFileResource(fileResource);
            letterService.save(comLetter);
            fileResourceService.setAssigned(fileResource.getId(), true);

        } catch (Exception e) {
            throw new BadRequestAlertException("Could not create letter","engagement","engagement");
        }
        return comLetter;
    }

    @Async
    @Override
    public void sendNotification(CommunicationLetter letter, String email) {
        Notification notification = notificationRepository.save(new Notification(
            email,
            letter.getBody(),
            "Engagement Notification",
            letter.getFileResource()
        ));
        mailService.sendNotificationEmail(notification);
    }

}
