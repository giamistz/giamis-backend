package gov.giamis.service.engagement.impl;

import java.util.List;
import java.util.Optional;

import gov.giamis.domain.engagement.EngagementTeamMeetingMember;
import gov.giamis.domain.engagement.MeetingAgenda;
import gov.giamis.domain.engagement.MeetingDeliverable;
import gov.giamis.repository.engagement.EngTeamMeetingMemberRepository;
import gov.giamis.repository.engagement.MeetingDeliverableRepository;
import gov.giamis.repository.engagement.TeamMeetingAgendaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementTeamMeeting;
import gov.giamis.repository.engagement.EngagementTeamMeetingRepository;
import gov.giamis.service.engagement.EngagementTeamMeetingService;

/**
 * Service Implementation for managing {@link EngagementTeamMeeting}.
 */
@Service
@Transactional
public class EngagementTeamMeetingServiceImpl implements EngagementTeamMeetingService {

    private final Logger log = LoggerFactory.getLogger(EngagementTeamMeetingServiceImpl.class);

    private final EngagementTeamMeetingRepository engagementTeamMeetingRepository;

    private final TeamMeetingAgendaRepository agendaRepository;

    private final MeetingDeliverableRepository deliverableRepository;

    private final EngTeamMeetingMemberRepository memberRepository;
    

    public EngagementTeamMeetingServiceImpl(
        EngagementTeamMeetingRepository engagementTeamMeetingRepository,
        TeamMeetingAgendaRepository agendaRepository,
        MeetingDeliverableRepository deliverableRepository,
        EngTeamMeetingMemberRepository memberRepository) {
        this.engagementTeamMeetingRepository = engagementTeamMeetingRepository;
        this.agendaRepository = agendaRepository;
        this.deliverableRepository = deliverableRepository;
        this.memberRepository = memberRepository;
    }

    /**
     * Save a engagementTeamMeeting.
     *
     * @param engagementTeamMeeting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementTeamMeeting save(EngagementTeamMeeting engagementTeamMeeting) {
        log.debug("Request to save Engagement Team Meeting : {}", engagementTeamMeeting);
        return engagementTeamMeetingRepository.save(engagementTeamMeeting);
    }

    /**
     * Get all the engagementTeamMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementTeamMeeting> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Team Meetings");
        return engagementTeamMeetingRepository.findAll(pageable);
    }


    /**
     * Get one engagementTeamMeeting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementTeamMeeting> findOne(Long id) {
        log.debug("Request to get Engagement Team Meeting : {}", id);
        return engagementTeamMeetingRepository.findById(id);
    }

    /**
     * Delete the engagementTeamMeeting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Team Meeting : {}", id);
        engagementTeamMeetingRepository.deleteById(id);
    }

	@Override
	public EngagementTeamMeeting findByEngagementId(Long id) {
		log.debug("Request to get Engagement Team Meeting by Engagement ID : {}", id);
		return engagementTeamMeetingRepository.findByEngagement_Id(id);
	}

	@Override
	public Long countEngagementTeamMeetingByEngagementId(Long id) {
		log.debug("Request to count Engagement Team Meeting by Engagement ID : {}", id);
		return engagementTeamMeetingRepository.countEngagementTeamMeetingByEngagementId(id);
	}

    @Override
    public List<MeetingDeliverable> getDeliverables(Long teamMeetingId) {
        return deliverableRepository.findByEngagementTeamMeeting_Id(teamMeetingId);
    }

    @Override
    public List<MeetingAgenda> getAgendas(Long teamMeetingId) {
        return agendaRepository.findByEngagementTeamMeeting_Id(teamMeetingId);
    }

    @Override
    public List<EngagementTeamMeetingMember> getMembers(Long teamMeetingId) {
        return memberRepository.findByEngagementMeetingId(teamMeetingId);
    }

    @Override
    public MeetingDeliverable saveDeliverable(MeetingDeliverable deliverable) {
        return deliverableRepository.save(deliverable);
    }

    @Override
    public MeetingAgenda saveAgenda(MeetingAgenda agenda) {
        return agendaRepository.save(agenda);
    }

    @Override
    public EngagementTeamMeetingMember saveMember(EngagementTeamMeetingMember member) {
        return memberRepository.save(member);
    }

    @Override
    public void deleteDeliverable(Long id) {
       deliverableRepository.deleteById(id);
    }

    @Override
    public void deleteAgenda(Long id) {
        agendaRepository.deleteById(id);
    }

    @Override
    public void deleteMember(Long id) {
       memberRepository.deleteById(id);
    }
}
