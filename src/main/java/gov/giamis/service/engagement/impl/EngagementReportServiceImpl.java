package gov.giamis.service.engagement.impl;

import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.service.engagement.EngagementReportService;
import gov.giamis.domain.engagement.EngagementReport;
import gov.giamis.repository.engagement.EngagementReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementReport}.
 */
@Service
@Transactional
public class EngagementReportServiceImpl implements EngagementReportService {

    private final Logger log = LoggerFactory.getLogger(EngagementReportServiceImpl.class);

    private final EngagementReportRepository engagementReportRepository;

    private final EngagementRepository engagementRepository;

    public EngagementReportServiceImpl(EngagementReportRepository engagementReportRepository, EngagementRepository engagementRepository) {
        this.engagementReportRepository = engagementReportRepository;
        this.engagementRepository = engagementRepository;
    }

    /**
     * Save a engagementReport.
     *
     * @param engagementReport the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementReport save(EngagementReport engagementReport) {
        log.debug("Request to save EngagementReport : {}", engagementReport);
        Long engagementId = engagementReport.getEngagement().getId();
        engagementRepository.findById(engagementId).ifPresent(engagementReport::engagement);
        return engagementReportRepository.save(engagementReport);
    }

    /**
     * Get all the engagementReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementReport> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementReports");
        return engagementReportRepository.findAll(pageable);
    }


    /**
     * Get one engagementReport by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementReport> findOne(Long id) {
        log.debug("Request to get EngagementReport : {}", id);
        return engagementReportRepository.findById(id);
    }

    /**
     * Delete the engagementReport by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementReport : {}", id);
        engagementReportRepository.deleteById(id);
    }
}
