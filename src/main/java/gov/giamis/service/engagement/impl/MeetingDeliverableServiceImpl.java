package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.MeetingDeliverable;
import gov.giamis.repository.engagement.MeetingDeliverableRepository;
import gov.giamis.service.engagement.MeetingDeliverableService;

/**
 * Service Implementation for managing {@link MeetingDeliverable}.
 */
@Service
@Transactional
public class MeetingDeliverableServiceImpl implements MeetingDeliverableService {

    private final Logger log = LoggerFactory.getLogger(MeetingDeliverableServiceImpl.class);

    private final MeetingDeliverableRepository meetingDeliverableRepository;

    public MeetingDeliverableServiceImpl(MeetingDeliverableRepository meetingDeliverableRepository) {
        this.meetingDeliverableRepository = meetingDeliverableRepository;
    }

    /**
     * Save a meetingDeliverable.
     *
     * @param meetingDeliverable the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MeetingDeliverable save(MeetingDeliverable meetingDeliverable) {
        log.debug("Request to save Meeting Deliverable : {}", meetingDeliverable);
        return meetingDeliverableRepository.save(meetingDeliverable);
    }

    /**
     * Get all the meetingDeliverables.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MeetingDeliverable> findAll(Pageable pageable) {
        log.debug("Request to get all Meeting Deliverables");
        return meetingDeliverableRepository.findAll(pageable);
    }


    /**
     * Get one meetingDeliverable by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MeetingDeliverable> findOne(Long id) {
        log.debug("Request to get Meeting Deliverable : {}", id);
        return meetingDeliverableRepository.findById(id);
    }

    /**
     * Delete the meetingDeliverable by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Meeting Deliverable : {}", id);
        meetingDeliverableRepository.deleteById(id);
    }


}
