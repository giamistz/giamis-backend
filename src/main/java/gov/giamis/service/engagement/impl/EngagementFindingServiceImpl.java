package gov.giamis.service.engagement.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.repository.engagement.EngagementProcedureRepository;

import gov.giamis.service.engagement.EngagementService;
import gov.giamis.service.engagement.dto.EngagementFindingDTO;
import gov.giamis.service.setup.FinancialYearService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementFinding;
import gov.giamis.repository.engagement.EngagementFindingRepository;
import gov.giamis.service.engagement.EngagementFindingService;

import static java.util.stream.Collectors.*;

/**
 * Service Implementation for managing {@link EngagementFinding}.
 */
@Service
@Transactional
public class EngagementFindingServiceImpl implements EngagementFindingService {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingServiceImpl.class);

    private final EngagementFindingRepository engagementFindingRepository;

    private final EngagementProcedureRepository engagementProcedureRepository;

    private final FinancialYearService financialYearService;

    private final EngagementService engagementService;

    @Autowired
    public EngagementFindingServiceImpl(
        EngagementFindingRepository engagementFindingRepository,
        EngagementProcedureRepository engagementProcedureRepository,
        FinancialYearService financialYearService,
        EngagementService engagementService) {
        this.engagementFindingRepository = engagementFindingRepository;
        this.engagementProcedureRepository = engagementProcedureRepository;
        this.financialYearService = financialYearService;
        this.engagementService = engagementService;
    }

    /**
     * Save a engagementFinding.
     *
     * @param engagementFinding the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementFinding save(EngagementFinding engagementFinding) {
        log.debug("Request to save Engagement Finding : {}", engagementFinding);
        return engagementFindingRepository.save(engagementFinding);
    }

    /**
     * Get all the engagementFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementFinding> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Findings");
        return engagementFindingRepository.findAll(pageable);
    }


    /**
     * Get one engagementFinding by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementFinding> findOne(Long id) {
        log.debug("Request to get Engagement Finding : {}", id);
        return engagementFindingRepository.findById(id);
    }

    /**
     * Delete the engagementFinding by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Finding : {}", id);
        engagementFindingRepository.deleteById(id);
    }

	@Override
	public List<EngagementFinding> findByEngagementProcedureId(Long id) {
		log.debug("Request to get Engagement Finding by Engagement Procedure ID : {}", id);
		return engagementFindingRepository.findByEngagementProcedure_Id(id);
	}

    @Override
    public void initiateFinding(List<EngagementFinding> engagementFindings) {
        // TODO clean findings by work done if a work done has not results which is not okay

        engagementFindings.forEach(f -> {
            EngagementFinding exist = engagementFindingRepository.findByWorkProgram_Id(f.getWorkProgram().getId());
            if (exist == null) {
                List<EngagementProcedure> procedures = new ArrayList<>();
                // Get procedure by objective
                EngagementProcedure procedure = engagementProcedureRepository
                    .findFirstById(f.getEngagementProcedure().getId());
                if (procedure.getWalkThrough()) {
                    procedures = engagementProcedureRepository
                        .findByInternalControl_Id(procedure.getInternalControl().getId());
                } else {
                    procedures = engagementProcedureRepository
                        .findByRiskControlMatrix_Id(procedure.getRiskControlMatrix().getId());
                }

                // Count finding by procedures
                Long count = engagementFindingRepository.countByEngagementProcedureIn(procedures);
                // Increment finding
                count = count + 1;
                String code = "FA-"+count;
                f.setFiveAttribute(code);
                //Create five attribute
                save(f);
            }
        });
    }

	@Override
	public List<EngagementFinding> findEngagementFindingByEngagementId(Long id) {
		  log.debug("Request to get Engagement Finding by Engagement ID: {}", id);
		return engagementFindingRepository.findEngagementFindingByEngagementId(id);
	}

    @Override
    public List<EngagementFinding> findByFinancialYear_IdAndOrganisationUnit_Id(Long financialYear, Long organisationUnit) {
        log.debug("Request to get Engagement Finding by Financiqal Year ID and Organisation Unit: {}, {}", financialYear, organisationUnit);
        return engagementFindingRepository.findByFinancialYear_IdAndOrganisationUnit_Id(financialYear, organisationUnit);
    }

    @Override
    public List<EngagementFinding> findByOrganisationUnitId(Long id) {
        log.debug("Request to get Engagement Finding by organisation unit id {}", id);
        return engagementFindingRepository.findByOrganisationUnitId(id);
    }

    /**
     *  Calculate previous finding group by status
     *
     * @param engagementId = current financial year

     * @return
     */
    @Override
    public Map<String, List<EngagementFindingDTO>> findPreviousGroupByStatus(Long engagementId) {

        Engagement en = engagementService.findOne(engagementId).get();

        List<EngagementFinding> previous = engagementFindingRepository
            .getPreviousFinding(
                en.getStartDate(),
                en.getOrganisationUnit().getId(),
                en.getAuditableArea().getId());

        return previous
            .stream()
            .map(f -> new EngagementFindingDTO(
                f.getImplementStatus(),
                f.getId(),
                f.getDescription(),
                f.getCause()))
            .collect(groupingBy(EngagementFindingDTO::getStatus));
    }
    public Long countByEngagementFindingStatus(Long engagementId) {
        return engagementFindingRepository.countByImplementStatusAndEngagement(engagementId);
    }

}

