package gov.giamis.service.engagement.impl;

import gov.giamis.service.engagement.MeetingAttachmentService;
import gov.giamis.domain.engagement.MeetingAttachment;
import gov.giamis.repository.engagement.MeetingAttachmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MeetingAttachment}.
 */
@Service
@Transactional
public class MeetingAttachmentServiceImpl implements MeetingAttachmentService {

    private final Logger log = LoggerFactory.getLogger(MeetingAttachmentServiceImpl.class);

    private final MeetingAttachmentRepository meetingAttachmentRepository;

    public MeetingAttachmentServiceImpl(MeetingAttachmentRepository meetingAttachmentRepository) {
        this.meetingAttachmentRepository = meetingAttachmentRepository;
    }

    /**
     * Save a meetingAttachment.
     *
     * @param meetingAttachment the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MeetingAttachment save(MeetingAttachment meetingAttachment) {
        log.debug("Request to save MeetingAttachment : {}", meetingAttachment);
        return meetingAttachmentRepository.save(meetingAttachment);
    }

    /**
     * Get all the meetingAttachments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MeetingAttachment> findAll(Pageable pageable) {
        log.debug("Request to get all MeetingAttachments");
        return meetingAttachmentRepository.findAll(pageable);
    }


    /**
     * Get one meetingAttachment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MeetingAttachment> findOne(Long id) {
        log.debug("Request to get MeetingAttachment : {}", id);
        return meetingAttachmentRepository.findById(id);
    }

    /**
     * Delete the meetingAttachment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MeetingAttachment : {}", id);
        meetingAttachmentRepository.deleteById(id);
    }
}
