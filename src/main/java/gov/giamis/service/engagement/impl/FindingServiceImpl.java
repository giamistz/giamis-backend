package gov.giamis.service.engagement.impl;

import gov.giamis.domain.engagement.Finding;
import gov.giamis.repository.engagement.FindingRepository;
import gov.giamis.service.engagement.FindingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Finding}.
 */
@Service
@Transactional
public class FindingServiceImpl implements FindingService {

    private final Logger log = LoggerFactory.getLogger(FindingServiceImpl.class);

    private final FindingRepository findingRepository;

    public FindingServiceImpl(FindingRepository findingRepository) {
        this.findingRepository = findingRepository;
    }

    /**
     * Save a finding.
     *
     * @param finding the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Finding save(Finding finding) {
        log.debug("Request to save Finding : {}", finding);
        return findingRepository.save(finding);
    }

    /**
     * Get all the findings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Finding> findAll(Pageable pageable) {
        log.debug("Request to get all Findings");
        return findingRepository.findAll(pageable);
    }


    /**
     * Get one finding by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Finding> findOne(Long id) {
        log.debug("Request to get Finding : {}", id);
        return findingRepository.findById(id);
    }

    /**
     * Delete the finding by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Finding : {}", id);
        findingRepository.deleteById(id);
    }
}
