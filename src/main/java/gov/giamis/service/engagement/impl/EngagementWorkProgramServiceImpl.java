package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementWorkProgram;
import gov.giamis.repository.engagement.EngagementWorkProgramRepository;
import gov.giamis.service.engagement.EngagementWorkProgramService;

/**
 * Service Implementation for managing {@link EngagementWorkProgram}.
 */
@Service
@Transactional
public class EngagementWorkProgramServiceImpl implements EngagementWorkProgramService {

    private final Logger log = LoggerFactory.getLogger(EngagementWorkProgramServiceImpl.class);

    private final EngagementWorkProgramRepository engagementWorkProgramRepository;
   
    public EngagementWorkProgramServiceImpl(EngagementWorkProgramRepository engagementWorkProgramRepository) {
        this.engagementWorkProgramRepository = engagementWorkProgramRepository;
    }

    /**
     * Save a engagementWorkProgram.
     *
     * @param engagementWorkProgram the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementWorkProgram save(EngagementWorkProgram engagementWorkProgram) {
        log.debug("Request to save Engagement Work Program : {}", engagementWorkProgram);
        return engagementWorkProgramRepository.save(engagementWorkProgram);
    }

    /**
     * Get all the engagementWorkPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementWorkProgram> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Work Programs");
        return engagementWorkProgramRepository.findAll(pageable);
    }


    /**
     * Get one engagementWorkProgram by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementWorkProgram> findOne(Long id) {
        log.debug("Request to get Engagement Work Program : {}", id);
        return engagementWorkProgramRepository.findById(id);
    }

    /**
     * Delete the engagementWorkProgram by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Work Program : {}", id);
        engagementWorkProgramRepository.deleteById(id);
    }

}
