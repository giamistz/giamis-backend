package gov.giamis.service.engagement.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.SpecificObjective;
import gov.giamis.repository.engagement.SpecificObjectiveRepository;
import gov.giamis.service.engagement.SpecificObjectiveService;

/**
 * Service Implementation for managing {@link SpecificObjective}.
 */
@Service
@Transactional
public class SpecificObjectiveServiceImpl implements SpecificObjectiveService {

    private final Logger log = LoggerFactory.getLogger(SpecificObjectiveServiceImpl.class);

    private final SpecificObjectiveRepository specificObjectiveRepository;
   

    public SpecificObjectiveServiceImpl(SpecificObjectiveRepository specificObjectiveRepository) {
        this.specificObjectiveRepository = specificObjectiveRepository;
    }
    /**
     * Save a specificObjective.
     *
     * @param specificObjective the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SpecificObjective save(SpecificObjective specificObjective) {
        log.debug("Request to save Specific Objective : {}", specificObjective);
        return specificObjectiveRepository.save(specificObjective);
    }

    /**
     * Get all the specificObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SpecificObjective> findAll(Pageable pageable) {
        log.debug("Request to get all Specific Objectives");
        return specificObjectiveRepository.findAll(pageable);
    }


    /**
     * Get one specificObjective by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SpecificObjective> findOne(Long id) {
        log.debug("Request to get Specific Objective : {}", id);
        return specificObjectiveRepository.findById(id);
    }

    /**
     * Delete the specificObjective by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Specific Objective : {}", id);
        specificObjectiveRepository.deleteById(id);
    }
	@Override
	public List<SpecificObjective> findAllSpecificObjective() {
		log.debug("Request to get all Specific Objective changed and un changed ");
		return specificObjectiveRepository.findAllSpecificObjective();
	}
	@Override
	public void refine(SpecificObjective objective) {
		log.debug("Request to Update Specific Objective by ID ");
		String code = objective.getCode();
		if (code == null) {
            long count = specificObjectiveRepository
                .countByEngagement_IdAndIsRefinedTrue(objective.getEngagement().getId());
            count = ++count;
            code = String.valueOf(count);
        }
		if (objective.getId() == null) {
		    SpecificObjective newRefined = new SpecificObjective(objective.getEngagement(), objective.getRefinedDescription(), code);
		    specificObjectiveRepository.save(newRefined);
        } else {
            specificObjectiveRepository.refine(objective.getRefinedDescription(), code, objective.getId());
        }

	}
	@Override
	public List<SpecificObjective> findByIsRefinedTrue() {
		log.debug("Request to get Specific Objective by Is Refined True ");
		return   specificObjectiveRepository.findByIsRefinedTrue();
	}
	@Override
	public List<SpecificObjective> findByEngagementId(Long id) {
		log.debug("Request to get Specific Objective by Engagement ID ");
		return   specificObjectiveRepository.findByEngagement_Id(id);
	}

}
