package gov.giamis.service.engagement.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementFinding;
import gov.giamis.domain.engagement.EngagementItem;
import gov.giamis.repository.engagement.EngagementItemRepository;
import gov.giamis.service.engagement.EngagementFindingService;
import gov.giamis.service.engagement.EngagementItemService;

/**
 * Service Implementation for managing {@link EngagementItem}.
 */
@Service
@Transactional
public class EngagementItemServiceImpl implements EngagementItemService {

    private final Logger log = LoggerFactory.getLogger(EngagementItemServiceImpl.class);

    private final EngagementItemRepository engagementItemRepository;

    private final EngagementFindingService engagementFindingService;

    public EngagementItemServiceImpl(EngagementItemRepository engagementItemRepository,
                                     EngagementFindingService engagementFindingService) {
        this.engagementItemRepository = engagementItemRepository;

        this.engagementFindingService = engagementFindingService;
    }

    /**
     * Save a engagementItem.
     *
     * @param engagementItem the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public EngagementItem save(EngagementItem engagementItem) {
        log.debug("Request to save Engagement Item : {}", engagementItem);
        List<EngagementFinding> findings = new ArrayList<>();
        engagementItem.getEngagementTestResult().forEach(r -> {
            r.setEngagementItem(engagementItem);
            if (!r.getOk()) {
                findings.add(new EngagementFinding(engagementItem.getEngagementProcedure(), r.getWorkProgram()));
            }
        });

        EngagementItem result=  engagementItemRepository.save(engagementItem);
        engagementFindingService.initiateFinding(findings);
        return  result;

    }

    /**
     * Get all the engagementItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementItem> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Items");
        return engagementItemRepository.findAll(pageable);
    }


    /**
     * Get one engagementItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementItem> findOne(Long id) {
        log.debug("Request to get Engagement Item : {}", id);
        return engagementItemRepository.findById(id);
    }

    /**
     * Delete the engagementItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Item : {}", id);
        engagementItemRepository.deleteById(id);
    }

	@Override
	public List<EngagementItem> findByEngagementProcedure(Long id) {
		 log.debug("Request to get Engagement Item by Engagement Procedure ID : {}", id);
		return engagementItemRepository.findByEngagementProcedure_Id(id);
	}


}
