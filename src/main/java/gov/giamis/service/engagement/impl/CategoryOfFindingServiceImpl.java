package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.CategoryOfFinding;
import gov.giamis.repository.engagement.CategoryOfFindingRepository;
import gov.giamis.service.engagement.CategoryOfFindingService;

/**
 * Service Implementation for managing {@link CategoryOfFinding}.
 */
@Service
@Transactional
public class CategoryOfFindingServiceImpl implements CategoryOfFindingService {

    private final Logger log = LoggerFactory.getLogger(CategoryOfFindingServiceImpl.class);

    private final CategoryOfFindingRepository categoryOfFindingRepository;
   

    public CategoryOfFindingServiceImpl(CategoryOfFindingRepository categoryOfFindingRepository) {
        this.categoryOfFindingRepository = categoryOfFindingRepository;
    }
    /**
     * Save a categoryOfFinding.
     *
     * @param categoryOfFinding the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CategoryOfFinding save(CategoryOfFinding categoryOfFinding) {
        log.debug("Request to save Category of Finding : {}", categoryOfFinding);
        return categoryOfFindingRepository.save(categoryOfFinding);
    }

    /**
     * Get all the categoryOfFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CategoryOfFinding> findAll(Pageable pageable) {
        log.debug("Request to get all Category of Findings");
        return categoryOfFindingRepository.findAll(pageable);
    }


    /**
     * Get one categoryOfFinding by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryOfFinding> findOne(Long id) {
        log.debug("Request to get Category of Finding : {}", id);
        return categoryOfFindingRepository.findById(id);
    }

    /**
     * Delete the categoryOfFinding by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category of Finding : {}", id);
        categoryOfFindingRepository.deleteById(id);
    }
    
    @Override
	public CategoryOfFinding findByName(String name) {
		log.debug("Request to get Category of Finding By name : {}", name);
		return categoryOfFindingRepository.findByNameIgnoreCase(name);
	}


}