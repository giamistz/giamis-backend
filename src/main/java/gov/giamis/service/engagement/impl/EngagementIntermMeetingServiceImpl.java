package gov.giamis.service.engagement.impl;

import gov.giamis.service.engagement.EngagementIntermMeetingService;
import gov.giamis.service.setup.UserOrganisationUnitService;
import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementIntermMeeting;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.repository.engagement.EngagementIntermMeetingRepository;
import gov.giamis.repository.setup.FinancialYearRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementIntermMeeting}.
 */
@Service
@Transactional
public class EngagementIntermMeetingServiceImpl implements EngagementIntermMeetingService {

    private final Logger log = LoggerFactory.getLogger(EngagementIntermMeetingServiceImpl.class);

    private final EngagementIntermMeetingRepository engagementIntermMeetingRepository;
    
    private final FinancialYearRepository financialYearRepository;
    
    private final UserOrganisationUnitService userOrganisationUnitService;

    public EngagementIntermMeetingServiceImpl(
    		EngagementIntermMeetingRepository engagementIntermMeetingRepository,
    		FinancialYearRepository financialYearRepository,
    		UserOrganisationUnitService userOrganisationUnitService) {
        this.engagementIntermMeetingRepository = engagementIntermMeetingRepository;
        this.financialYearRepository = financialYearRepository;
        this.userOrganisationUnitService = userOrganisationUnitService;
    }

    /**
     * Save a engagementIntermMeeting.
     *
     * @param engagementIntermMeeting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementIntermMeeting save(EngagementIntermMeeting engagementIntermMeeting) {
        log.debug("Request to save EngagementIntermMeeting : {}", engagementIntermMeeting);
        FinancialYear financialYear=financialYearRepository.findByStatus(Constants.ACTIVE);
        engagementIntermMeeting.setFinancialYear(financialYear);
        
        engagementIntermMeeting.setOrganisationUnit(userOrganisationUnitService.getCurrentOrganisationUnit());
        return engagementIntermMeetingRepository.save(engagementIntermMeeting);
    }

    /**
     * Get all the engagementIntermMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementIntermMeeting> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementIntermMeetings");
        return engagementIntermMeetingRepository.findAll(pageable);
    }


    /**
     * Get one engagementIntermMeeting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementIntermMeeting> findOne(Long id) {
        log.debug("Request to get EngagementIntermMeeting : {}", id);
        return engagementIntermMeetingRepository.findById(id);
    }

    /**
     * Delete the engagementIntermMeeting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementIntermMeeting : {}", id);
        engagementIntermMeetingRepository.deleteById(id);
    }
}
