package gov.giamis.service.engagement.impl;
import java.util.List;
import java.util.Optional;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementFindingComment;
import gov.giamis.repository.engagement.EngagementFindingCommentRepository;
import gov.giamis.service.engagement.EngagementFindingCommentService;

/**
 * Service Implementation for managing {@link EngagementFindingComment}.
 */
@Service
@Transactional
public class EngagementFindingCommentServiceImpl implements EngagementFindingCommentService {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingCommentServiceImpl.class);

    private final EngagementFindingCommentRepository engagementFindingCommentCommentRepository;

    @Autowired
    public EngagementFindingCommentServiceImpl(
        EngagementFindingCommentRepository engagementFindingCommentCommentRepository) {
        this.engagementFindingCommentCommentRepository = engagementFindingCommentCommentRepository;
    }

    /**
     * Save a engagementFindingComment.
     *
     * @param engagementFindingComment the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementFindingComment save(EngagementFindingComment engagementFindingComment) {
        log.debug("Request to save Engagement Finding Comment : {}", engagementFindingComment);
        return engagementFindingCommentCommentRepository.save(engagementFindingComment);
    }

    /**
     * Get all the engagementFindingComments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementFindingComment> findAll(Pageable pageable) {
        log.debug("Request to get all Engagement Finding Comments");
        return engagementFindingCommentCommentRepository.findAll(pageable);
    }


    /**
     * Get one engagementFindingComment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementFindingComment> findOne(Long id) {
        log.debug("Request to get Engagement Finding Comment : {}", id);
        return engagementFindingCommentCommentRepository.findById(id);
    }

    /**
     * Delete the engagementFindingComment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Engagement Finding Comment : {}", id);
        engagementFindingCommentCommentRepository.deleteById(id);
    }

}

