package gov.giamis.service.engagement.impl;

import gov.giamis.service.engagement.MatterOfNextAuditService;
import gov.giamis.domain.engagement.MatterOfNextAudit;
import gov.giamis.repository.engagement.MatterOfNextAuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MatterOfNextAudit}.
 */
@Service
@Transactional
public class MatterOfNextAuditServiceImpl implements MatterOfNextAuditService {

    private final Logger log = LoggerFactory.getLogger(MatterOfNextAuditServiceImpl.class);

    private final MatterOfNextAuditRepository matterOfNextAuditRepository;

    public MatterOfNextAuditServiceImpl(MatterOfNextAuditRepository matterOfNextAuditRepository) {
        this.matterOfNextAuditRepository = matterOfNextAuditRepository;
    }

    /**
     * Save a matterOfNextAudit.
     *
     * @param matterOfNextAudit the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MatterOfNextAudit save(MatterOfNextAudit matterOfNextAudit) {
        log.debug("Request to save MatterOfNextAudit : {}", matterOfNextAudit);
        return matterOfNextAuditRepository.save(matterOfNextAudit);
    }

    /**
     * Get all the matterOfNextAudits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MatterOfNextAudit> findAll(Pageable pageable) {
        log.debug("Request to get all MatterOfNextAudits");
        return matterOfNextAuditRepository.findAll(pageable);
    }


    /**
     * Get one matterOfNextAudit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MatterOfNextAudit> findOne(Long id) {
        log.debug("Request to get MatterOfNextAudit : {}", id);
        return matterOfNextAuditRepository.findById(id);
    }

    /**
     * Delete the matterOfNextAudit by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MatterOfNextAudit : {}", id);
        matterOfNextAuditRepository.deleteById(id);
    }
}
