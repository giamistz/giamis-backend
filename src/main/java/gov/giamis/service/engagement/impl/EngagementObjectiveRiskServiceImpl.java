package gov.giamis.service.engagement.impl;

import gov.giamis.domain.engagement.EngagementObjectiveRisk;
import gov.giamis.repository.engagement.EngagementObjectiveRiskRepository;
import gov.giamis.service.engagement.EngagementObjectiveRiskService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementObjectiveRisk}.
 */
@Service
@Transactional
public class EngagementObjectiveRiskServiceImpl implements EngagementObjectiveRiskService {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveRiskServiceImpl.class);

    private final EngagementObjectiveRiskRepository engagementObjectiveRiskRepository;

    public EngagementObjectiveRiskServiceImpl(EngagementObjectiveRiskRepository engagementObjectiveRiskRepository) {
        this.engagementObjectiveRiskRepository = engagementObjectiveRiskRepository;
    }

    /**
     * Save a engagementObjectiveRisk.
     *
     * @param engagementObjectiveRisk the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementObjectiveRisk save(EngagementObjectiveRisk engagementObjectiveRisk) {
        log.debug("Request to save EngagementObjectiveRisk : {}", engagementObjectiveRisk);
        return engagementObjectiveRiskRepository.save(engagementObjectiveRisk);
    }

    /**
     * Get all the engagementObjectiveRisks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementObjectiveRisk> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementObjectiveRisks");
        return engagementObjectiveRiskRepository.findAll(pageable);
    }


    /**
     * Get one engagementObjectiveRisk by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementObjectiveRisk> findOne(Long id) {
        log.debug("Request to get EngagementObjectiveRisk : {}", id);
        return engagementObjectiveRiskRepository.findById(id);
    }

    /**
     * Delete the engagementObjectiveRisk by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementObjectiveRisk : {}", id);
        engagementObjectiveRiskRepository.deleteById(id);
    }


}
