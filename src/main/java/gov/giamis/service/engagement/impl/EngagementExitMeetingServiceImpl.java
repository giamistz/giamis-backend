package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementExitMeeting;
import gov.giamis.repository.engagement.EngagementExitMeetingRepository;
import gov.giamis.service.engagement.EngagementExitMeetingService;
import gov.giamis.service.setup.UserOrganisationUnitService;

/**
 * Service Implementation for managing {@link EngagementExitMeeting}.
 */
@Service
@Transactional
public class EngagementExitMeetingServiceImpl implements EngagementExitMeetingService {

    private final Logger log = LoggerFactory.getLogger(EngagementExitMeetingServiceImpl.class);

    private final EngagementExitMeetingRepository engagementExitMeetingRepository;

    private final UserOrganisationUnitService userOrganisationUnitService;

    public EngagementExitMeetingServiceImpl(
    		EngagementExitMeetingRepository engagementExitMeetingRepository,
    		UserOrganisationUnitService userOrganisationUnitService) {
        this.engagementExitMeetingRepository = engagementExitMeetingRepository;
        this.userOrganisationUnitService = userOrganisationUnitService;
    }

    /**
     * Save a engagementExitMeeting.
     *
     * @param engagementExitMeeting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementExitMeeting save(EngagementExitMeeting engagementExitMeeting) {
        log.debug("Request to save EngagementExitMeeting : {}", engagementExitMeeting);
        return engagementExitMeetingRepository.save(engagementExitMeeting);
    }

    /**
     * Get all the engagementExitMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementExitMeeting> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementExitMeetings");
        return engagementExitMeetingRepository.findAll(pageable);
    }


    /**
     * Get one engagementExitMeeting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementExitMeeting> findOne(Long id) {
        log.debug("Request to get EngagementExitMeeting : {}", id);
        return engagementExitMeetingRepository.findById(id);
    }

    /**
     * Delete the engagementExitMeeting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementExitMeeting : {}", id);
        engagementExitMeetingRepository.deleteById(id);
    }

	@Override
	public EngagementExitMeeting findByEngagementId(Long id) {
		log.debug("Request to get EngagementExitMeeting by Engagement ID : {}", id);
        return engagementExitMeetingRepository.findByEngagement_Id(id);
	}

	@Override
	public Long countEngagementExitMeetingByEngagementId(Long id) {
		log.debug("Request to count EngagementExitMeeting by Engagement ID : {}", id);
		return engagementExitMeetingRepository.countEngagementExitMeetingByEngagementId(id);
	}
}
