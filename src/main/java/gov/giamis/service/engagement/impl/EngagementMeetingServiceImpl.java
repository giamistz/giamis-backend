package gov.giamis.service.engagement.impl;

import gov.giamis.service.engagement.EngagementMeetingService;
import gov.giamis.domain.engagement.EngagementMeeting;
import gov.giamis.repository.engagement.EngagementMeetingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementMeeting}.
 */
@Service
@Transactional
public class EngagementMeetingServiceImpl implements EngagementMeetingService {

    private final Logger log = LoggerFactory.getLogger(EngagementMeetingServiceImpl.class);

    private final EngagementMeetingRepository engagementMeetingRepository;

    public EngagementMeetingServiceImpl(EngagementMeetingRepository engagementMeetingRepository) {
        this.engagementMeetingRepository = engagementMeetingRepository;
    }

    /**
     * Save a engagementMeeting.
     *
     * @param engagementMeeting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementMeeting save(EngagementMeeting engagementMeeting) {
        log.debug("Request to save EngagementMeeting : {}", engagementMeeting);
        return engagementMeetingRepository.save(engagementMeeting);
    }

    /**
     * Get all the engagementMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementMeeting> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementMeetings");
        return engagementMeetingRepository.findAll(pageable);
    }


    /**
     * Get one engagementMeeting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementMeeting> findOne(Long id) {
        log.debug("Request to get EngagementMeeting : {}", id);
        return engagementMeetingRepository.findById(id);
    }

    /**
     * Delete the engagementMeeting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementMeeting : {}", id);
        engagementMeetingRepository.deleteById(id);
    }
}
