package gov.giamis.service.engagement.impl;

import gov.giamis.domain.Notification;
import gov.giamis.domain.enumeration.EngagementRole;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.repository.NotificationRepository;
import gov.giamis.service.MailService;
import gov.giamis.service.engagement.EngagementMemberService;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.repository.engagement.EngagementMemberRepository;
import gov.giamis.service.report.EngagementTemplateService;
import gov.giamis.service.setup.FileResourceService;
import gov.giamis.web.rest.errors.BadRequestAlertException;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementMember}.
 */
@Service
@Transactional
public class EngagementMemberServiceImpl implements EngagementMemberService {

    private final Logger log = LoggerFactory.getLogger(EngagementMemberServiceImpl.class);

    private final EngagementMemberRepository engagementMemberRepository;

    private final FileResourceService fileResourceService;

    private final EngagementTemplateService engagementTemplateService;

    private final NotificationRepository notificationRepository;

    private final MailService mailService;

    @Autowired
    public EngagementMemberServiceImpl(
        EngagementMemberRepository engagementMemberRepository,
        FileResourceService fileResourceService,
        EngagementTemplateService engagementTemplateService,
        NotificationRepository notificationRepository,
        MailService mailService) {

        this.engagementMemberRepository = engagementMemberRepository;
        this.fileResourceService = fileResourceService;
        this.engagementTemplateService = engagementTemplateService;
        this.notificationRepository = notificationRepository;
        this.mailService = mailService;
    }

    /**
     * Save a engagementMember.
     *
     * @param engagementMember the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public EngagementMember save(EngagementMember engagementMember) {
        log.debug("Request to save EngagementMember : {}", engagementMember);
        return engagementMemberRepository.save(engagementMember);
    }

    /**
     * Get all the engagementMembers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementMember> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementMembers");
        return engagementMemberRepository.findAll(pageable);
    }


    /**
     * Get one engagementMember by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementMember> findOne(Long id) {
        log.debug("Request to get EngagementMember : {}", id);
        return engagementMemberRepository.findById(id);
    }

    /**
     * Delete the engagementMember by id.
     *
     * @param id the id of the entity.
     */
    @Override
    @Transactional
    public void delete(Long id) {
        log.debug("Request to delete EngagementMember : {}", id);
        EngagementMember member = engagementMemberRepository.findById(id).get();
        if (member.getLetterFileResource() != null) {
            notificationRepository.deleteByFileResource(member.getLetterFileResource());
            fileResourceService.delete(member.getLetterFileResource().getId());
        }
        if (member.getNdaFileResource() != null) {
            notificationRepository.deleteByFileResource(member.getNdaFileResource());
            fileResourceService.delete(member.getNdaFileResource().getId());
        }
        engagementMemberRepository.deleteById(id);
    }

    /**
     * @param member = engagement Member
     */
    @Override
    @Async
    public void sendInvitationLetter(EngagementMember member) {

        Notification notification = notificationRepository.save(new Notification(
            member.getUser().getEmail(),
            engagementTemplateService.getEngInvitationLetterBody(member),
            "Invitation to form Engagement Team",
            member.getLetterFileResource()
        ));
        mailService.sendNotificationEmail(notification);

    }

	@Override
	public void approveEngagementMember(LocalDate acceptanceDate, Long id) {
		log.debug("Request to approve EngagementMember by ID : {}", id);
         engagementMemberRepository.approveEngagementMember(acceptanceDate, id);
	}

	@Override
	public FileResource createLetter(EngagementMember member) {
        /**
         * Create a Invitation letter
         */
        FileResource fileResource;
        try {
            File letter = engagementTemplateService.createEngagementMemberLetter(member.getId());

             fileResource= fileResourceService.upload(
                letter,
                "letter_"+member.getUser().getEmail(),
                "engagement"+ member.getEngagement().getId());
             member.setLetterFileResource(fileResource);
             save(member);

             //letter.delete();
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestAlertException("COULD NOT CREATE INVITATION LETTER", "MEMBER","");
        }
        return fileResource;
    }

    @Override
    public boolean hasTeamLead(Long engagementId) {
        int teamLead = engagementMemberRepository.countByEngagement_IdAndRole(engagementId, EngagementRole.TEAMLEAD);
        return teamLead > 0;
    }
}
