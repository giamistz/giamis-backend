package gov.giamis.service.engagement.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.SubCategoryOfFinding;
import gov.giamis.repository.engagement.SubCategoryOfFindingRepository;
import gov.giamis.service.engagement.SubCategoryOfFindingService;

/**
 * Service Implementation for managing {@link SubCategoryOfFinding}.
 */
@Service
@Transactional
public class SubCategoryOfFindingServiceImpl implements SubCategoryOfFindingService {

    private final Logger log = LoggerFactory.getLogger(SubCategoryOfFindingServiceImpl.class);

    private final SubCategoryOfFindingRepository subCategoryOfFindingRepository;
   

    public SubCategoryOfFindingServiceImpl(SubCategoryOfFindingRepository subCategoryOfFindingRepository) {
        this.subCategoryOfFindingRepository = subCategoryOfFindingRepository;
    }
    /**
     * Save a subCategoryOfFinding.
     *
     * @param subCategoryOfFinding the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SubCategoryOfFinding save(SubCategoryOfFinding subCategoryOfFinding) {
        log.debug("Request to save Sub Category of Finding : {}", subCategoryOfFinding);
        return subCategoryOfFindingRepository.save(subCategoryOfFinding);
    }

    /**
     * Get all the subCategoryOfFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubCategoryOfFinding> findAll(Pageable pageable) {
        log.debug("Request to get all Sub Category of Findings");
        return subCategoryOfFindingRepository.findAll(pageable);
    }


    /**
     * Get one subCategoryOfFinding by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubCategoryOfFinding> findOne(Long id) {
        log.debug("Request to get Sub Category of Finding : {}", id);
        return subCategoryOfFindingRepository.findById(id);
    }

    /**
     * Delete the subCategoryOfFinding by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sub Category of Finding : {}", id);
        subCategoryOfFindingRepository.deleteById(id);
    }

    @Override
	public SubCategoryOfFinding findByName(String name) {
		log.debug("Request to get Sub Category of Finding By name : {}", name);
		return subCategoryOfFindingRepository.findByNameIgnoreCase(name);
	}

}