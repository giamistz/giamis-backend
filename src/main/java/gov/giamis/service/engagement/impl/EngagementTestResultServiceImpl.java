package gov.giamis.service.engagement.impl;

import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.EngagementTestResult;
import gov.giamis.repository.engagement.EngagementFindingRepository;
import gov.giamis.repository.engagement.EngagementItemRepository;
import gov.giamis.repository.engagement.EngagementScoreRepository;
import gov.giamis.repository.engagement.EngagementTestResultRepository;
import gov.giamis.service.engagement.EngagementTestResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementTestResult}.
 */
@Service
@Transactional
public class EngagementTestResultServiceImpl implements EngagementTestResultService {

    private final Logger log = LoggerFactory.getLogger(EngagementTestResultServiceImpl.class);

    private final EngagementTestResultRepository engagementTestResultRepository;
    
    private final EngagementItemRepository engagementItemRepository;
    
    private final EngagementFindingRepository engagementFindingRepository;
    
    private final EngagementScoreRepository engagementScoreRepository;
   
    public EngagementTestResultServiceImpl(EngagementTestResultRepository engagementTestResultRepository,
    		EngagementFindingRepository engagementFindingRepository,
    		EngagementItemRepository engagementItemRepository,
    		EngagementScoreRepository engagementScoreRepository) {
        this.engagementTestResultRepository = engagementTestResultRepository;
        this.engagementFindingRepository = engagementFindingRepository;
        this.engagementItemRepository = engagementItemRepository;
        this.engagementScoreRepository = engagementScoreRepository;
    }

    /**
     * Save a engagementTestResult.
     *
     * @param engagementTestResult the entity to save.
     * @return the persisted entity.
     */
    @SuppressWarnings("null")
	@Override
    public EngagementTestResult save(EngagementTestResult engagementTestResult) {
        log.debug("Request to save EngagementTest Result : {}", engagementTestResult);
        return engagementTestResultRepository.save(engagementTestResult);
    }

    /**
     * Get all the engagementTestResults.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementTestResult> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementTestResults");
        return engagementTestResultRepository.findAll(pageable);
    }


    /**
     * Get one engagementTestResult by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementTestResult> findOne(Long id) {
        log.debug("Request to get EngagementTestResult : {}", id);
        return engagementTestResultRepository.findById(id);
    }

    /**
     * Delete the engagementTestResult by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementTestResult : {}", id);
        engagementTestResultRepository.deleteById(id);
    }

	@Override
	public List<EngagementTestResult> findByEngagementProcedure(Long id) {
		 log.debug("Request to get EngagementTestResult by Engagement Procedure ID : {}", id);
		return engagementTestResultRepository.findByEngagementProcedure_Id(id);
	}

	@Override
	public List<EngagementTestResult> findByScoreAndEngagementProcedure(Long id) {
		 log.debug("Request to get EngagementTestResult by score equals Ok And Engagement Procedure ID : {}", id);
		return engagementTestResultRepository.findByIsOkAndEngagementProcedure_Id(true,id);
	}



	@Override
	public void updateEngagementTestResult(Long item, Long workProgram, Long procedure,Long id) {
		log.debug("Request to update EngagementTestResult by  ID : {}", id);
		engagementTestResultRepository.updateEngagementTestResult(item, workProgram, procedure, id);
	}
}
