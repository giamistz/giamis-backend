package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementObjective;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementObjective}.
 */
public interface EngagementObjectiveService {

    /**
     * Save a engagementObjective.
     *
     * @param engagementObjective the entity to save.
     * @return the persisted entity.
     */
    EngagementObjective save(EngagementObjective engagementObjective);

    /**
     * Get all the engagementObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementObjective> findAll(Pageable pageable);


    /**
     * Get the "id" engagementObjective.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementObjective> findOne(Long id);

    /**
     * Delete the "id" engagementObjective.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
}
