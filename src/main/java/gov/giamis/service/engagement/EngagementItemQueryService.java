package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementItem;
import gov.giamis.domain.engagement.EngagementItem_;
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.repository.engagement.EngagementItemRepository;
import gov.giamis.service.engagement.dto.EngagementItemCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementItem} entities in the database.
 * The main input is a {@link EngagementItemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementItem} or a {@link Page} of {@link EngagementItem} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementItemQueryService extends QueryService<EngagementItem> {

    private final Logger log = LoggerFactory.getLogger(EngagementItemQueryService.class);

    private final EngagementItemRepository engagementProcedureRepository;

    public EngagementItemQueryService(EngagementItemRepository engagementProcedureRepository) {
        this.engagementProcedureRepository = engagementProcedureRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementItem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementItem> findByCriteria(EngagementItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementItem> specification = createSpecification(criteria);
        return engagementProcedureRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementItem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementItem> findByCriteria(EngagementItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementItem> specification = createSpecification(criteria);
        return engagementProcedureRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementItemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementItem> specification = createSpecification(criteria);
        return engagementProcedureRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementItem> createSpecification(EngagementItemCriteria criteria) {
        Specification<EngagementItem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementItem_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), EngagementItem_.name));
            }

            if (criteria.getEngagementProcedureId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementProcedureId(),
                    root -> root.join(EngagementItem_.engagementProcedure, JoinType.LEFT).get(EngagementProcedure_.id)));
            }
            
        }
        return specification;
    }
}
