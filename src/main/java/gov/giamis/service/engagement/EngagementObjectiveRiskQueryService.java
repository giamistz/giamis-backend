package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.Engagement_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementObjectiveRisk;
import gov.giamis.domain.engagement.EngagementObjectiveRisk_;
import gov.giamis.domain.engagement.SpecificObjective_;
import gov.giamis.domain.risk.Risk_;
import gov.giamis.repository.engagement.EngagementObjectiveRiskRepository;
import gov.giamis.service.dto.EngagementObjectiveRiskCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementObjectiveRisk} entities in the database.
 * The main input is a {@link EngagementObjectiveRiskCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementObjectiveRisk} or a {@link Page} of {@link EngagementObjectiveRisk} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementObjectiveRiskQueryService extends QueryService<EngagementObjectiveRisk> {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveRiskQueryService.class);

    private final EngagementObjectiveRiskRepository engagementObjectiveRiskRepository;

    public EngagementObjectiveRiskQueryService(EngagementObjectiveRiskRepository engagementObjectiveRiskRepository) {
        this.engagementObjectiveRiskRepository = engagementObjectiveRiskRepository;
    }

    public List<EngagementObjectiveRisk> findByEngagement(Long engagementId) {
        return engagementObjectiveRiskRepository.findByEngagement(engagementId);
    }

    @Transactional(readOnly = true)
    public List<EngagementObjectiveRisk> findAllWithControlByEngagement(Long engagementId) {
        log.debug("find by eng : {}", engagementId);
        return engagementObjectiveRiskRepository.findAllWithControl(engagementId);
    }

}
