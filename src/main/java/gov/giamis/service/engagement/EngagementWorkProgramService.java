package gov.giamis.service.engagement;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementWorkProgram;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementWorkProgram}.
 */
public interface EngagementWorkProgramService {

    /**
     * Save a engagementWorkProgram.
     *
     * @param engagementWorkProgram the entity to save.
     * @return the persisted entity.
     */
    EngagementWorkProgram save(EngagementWorkProgram engagementWorkProgram);

    /**
     * Get all the engagementWorkPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementWorkProgram> findAll(Pageable pageable);


    /**
     * Get the "id" engagementWorkProgram.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementWorkProgram> findOne(Long id);

    /**
     * Delete the "id" engagementWorkProgram.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
