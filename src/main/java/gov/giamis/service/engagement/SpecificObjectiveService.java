package gov.giamis.service.engagement;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.SpecificObjective;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link SpecificObjective}.
 */
public interface SpecificObjectiveService {

    /**
     * Save a specificObjective.
     *
     * @param specificObjective the entity to save.
     * @return the persisted entity.
     */
    SpecificObjective save(SpecificObjective specificObjective);

    /**
     * Get all the specificObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SpecificObjective> findAll(Pageable pageable);


    /**
     * Get the "id" specificObjective.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SpecificObjective> findOne(Long id);

    /**
     * Delete the "id" specificObjective.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<SpecificObjective> findAllSpecificObjective();
    
    void refine(SpecificObjective objective);
    
    List<SpecificObjective> findByIsRefinedTrue();
    
    List<SpecificObjective> findByEngagementId(Long id);
    
}
