package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.setup.FileResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementMember}.
 */
public interface EngagementMemberService {

    /**
     * Save a engagementMember.
     *
     * @param engagementMember the entity to save.
     * @return the persisted entity.
     */
    EngagementMember save(EngagementMember engagementMember);

    /**
     * Get all the engagementMembers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementMember> findAll(Pageable pageable);


    /**
     * Get the "id" engagementMember.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementMember> findOne(Long id);

    /**
     * Delete the "id" engagementMember.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     *
     */
    void sendInvitationLetter(EngagementMember engagementMember);
    
    void approveEngagementMember(LocalDate acceptanceDate,Long id);

    FileResource createLetter(EngagementMember member);

    boolean hasTeamLead(Long engagementId);
}
