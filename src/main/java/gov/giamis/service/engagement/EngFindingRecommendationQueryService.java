package gov.giamis.service.engagement;

import java.util.List;
;
import gov.giamis.domain.engagement.EngFindingRecommendation_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.EngFindingRecommendation;
import gov.giamis.repository.engagement.EngFindingReccomendationRepository;
import gov.giamis.service.dto.EngFindingReccomendationCriteria;

/**
 * Service for executing complex queries for {@link EngFindingRecommendation} entities in the database.
 * The main input is a {@link EngFindingReccomendationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngFindingRecommendation} or a {@link Page} of {@link EngFindingRecommendation} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngFindingRecommendationQueryService extends QueryService<EngFindingRecommendation> {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationQueryService.class);

    private final EngFindingReccomendationRepository engFindingReccomendationRepository;

    public EngFindingRecommendationQueryService(EngFindingReccomendationRepository engFindingReccomendationRepository) {
        this.engFindingReccomendationRepository = engFindingReccomendationRepository;
    }

    /**
     * Return a {@link List} of {@link EngFindingRecommendation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngFindingRecommendation> findByCriteria(EngFindingReccomendationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngFindingRecommendation> specification = createSpecification(criteria);
        return engFindingReccomendationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngFindingRecommendation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngFindingRecommendation> findByCriteria(EngFindingReccomendationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngFindingRecommendation> specification = createSpecification(criteria);
        return engFindingReccomendationRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngFindingReccomendationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngFindingRecommendation> specification = createSpecification(criteria);
        return engFindingReccomendationRepository.count(specification);
    }

    /**
     * Function to convert {@link EngFindingReccomendationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EngFindingRecommendation> createSpecification(EngFindingReccomendationCriteria criteria) {
        Specification<EngFindingRecommendation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), EngFindingRecommendation_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), EngFindingRecommendation_.description));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), EngFindingRecommendation_.status));
            }
        }
        return specification;
    }
}
