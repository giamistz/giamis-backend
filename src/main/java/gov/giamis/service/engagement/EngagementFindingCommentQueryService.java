package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.SimpleUser_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementFindingComment;
import gov.giamis.domain.engagement.EngagementFindingComment_;
import gov.giamis.domain.engagement.EngagementFinding_;
import gov.giamis.domain.setup.User_;
import gov.giamis.repository.engagement.EngagementFindingCommentRepository;
import gov.giamis.service.engagement.dto.EngagementFindingCommentCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementFindingComment} entities in the database.
 * The main input is a {@link EngagementFindingCommentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementFindingComment} or a {@link Page} of {@link EngagementFindingComment} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementFindingCommentQueryService extends QueryService<EngagementFindingComment> {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingCommentQueryService.class);

    private final EngagementFindingCommentRepository engagementFindingCommentRepository;

    public EngagementFindingCommentQueryService(EngagementFindingCommentRepository engagementFindingCommentRepository) {
        this.engagementFindingCommentRepository = engagementFindingCommentRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementFindingComment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementFindingComment> findByCriteria(EngagementFindingCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementFindingComment> specification = createSpecification(criteria);
        return engagementFindingCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementFindingComment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementFindingComment> findByCriteria(EngagementFindingCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementFindingComment> specification = createSpecification(criteria);
        return engagementFindingCommentRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementFindingCommentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementFindingComment> specification = createSpecification(criteria);
        return engagementFindingCommentRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementFindingComment> createSpecification(EngagementFindingCommentCriteria criteria) {
        Specification<EngagementFindingComment> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementFindingComment_.id));
            }
       
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(EngagementFindingComment_.user, JoinType.LEFT).get(SimpleUser_.id)));
            }
            
            if (criteria.getEngagementFindingId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementFindingId(),
                    root -> root.join(EngagementFindingComment_.engagementFinding, JoinType.LEFT).get(EngagementFinding_.id)));
            }
            
        }
        return specification;
    }
}
