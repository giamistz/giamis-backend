package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementExitMeeting;
import gov.giamis.domain.engagement.EngagementExitMeeting_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.repository.engagement.EngagementExitMeetingRepository;
import gov.giamis.service.engagement.dto.EngagementExitMeetingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementExitMeeting} entities in the database.
 * The main input is a {@link EngagementExitMeetingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementExitMeeting} or a {@link Page} of {@link EngagementExitMeeting} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementExitMeetingQueryService extends QueryService<EngagementExitMeeting> {

    private final Logger log = LoggerFactory.getLogger(EngagementExitMeetingQueryService.class);

    private final EngagementExitMeetingRepository engagementExitMeetingRepository;

    public EngagementExitMeetingQueryService(EngagementExitMeetingRepository engagementExitMeetingRepository) {
        this.engagementExitMeetingRepository = engagementExitMeetingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementExitMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementExitMeeting> findByCriteria(EngagementExitMeetingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementExitMeeting> specification = createSpecification(criteria);
        return engagementExitMeetingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementExitMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementExitMeeting> findByCriteria(EngagementExitMeetingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementExitMeeting> specification = createSpecification(criteria);
        return engagementExitMeetingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementExitMeetingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementExitMeeting> specification = createSpecification(criteria);
        return engagementExitMeetingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementExitMeeting> createSpecification(EngagementExitMeetingCriteria criteria) {
        Specification<EngagementExitMeeting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementExitMeeting_.id));
            }
            if (criteria.getMeetingDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMeetingDate(), EngagementExitMeeting_.meetingDate));
            }
            if (criteria.getVenue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVenue(), EngagementExitMeeting_.venue));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementExitMeeting_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }
}
