package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.engagement.MatterOfNextAudit_;
import gov.giamis.domain.setup.AuditableArea_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.MatterOfNextAudit;
import gov.giamis.repository.engagement.MatterOfNextAuditRepository;
import gov.giamis.service.engagement.dto.MatterOfNextAuditCriteria;

/**
 * Service for executing complex queries for {@link MatterOfNextAudit} entities in the database.
 * The main input is a {@link MatterOfNextAuditCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MatterOfNextAudit} or a {@link Page} of {@link MatterOfNextAudit} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MatterOfNextAuditQueryService extends QueryService<MatterOfNextAudit> {

    private final Logger log = LoggerFactory.getLogger(MatterOfNextAuditQueryService.class);

    private final MatterOfNextAuditRepository matterOfNextAuditRepository;

    public MatterOfNextAuditQueryService(MatterOfNextAuditRepository matterOfNextAuditRepository) {
        this.matterOfNextAuditRepository = matterOfNextAuditRepository;
    }

    /**
     * Return a {@link List} of {@link MatterOfNextAudit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MatterOfNextAudit> findByCriteria(MatterOfNextAuditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MatterOfNextAudit> specification = createSpecification(criteria);
        return matterOfNextAuditRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MatterOfNextAudit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MatterOfNextAudit> findByCriteria(MatterOfNextAuditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MatterOfNextAudit> specification = createSpecification(criteria);
        return matterOfNextAuditRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MatterOfNextAuditCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MatterOfNextAudit> specification = createSpecification(criteria);
        return matterOfNextAuditRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<MatterOfNextAudit> createSpecification(MatterOfNextAuditCriteria criteria) {
        Specification<MatterOfNextAudit> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MatterOfNextAudit_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), MatterOfNextAudit_.name));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(MatterOfNextAudit_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getAuditeeId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditeeId(),
                    root -> root.join(MatterOfNextAudit_.auditee, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(MatterOfNextAudit_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            if (criteria.getAuditableAreaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditableAreaId(),
                    root -> root.join(MatterOfNextAudit_.auditableArea, JoinType.LEFT).get(AuditableArea_.id)));
            }
        }
        return specification;
    }
}
