package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.CommunicationLetter_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.FileResource_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.repository.engagement.CommunicationLetterRepository;
import gov.giamis.service.dto.CommunicationLetterCriteria;

/**
 * Service for executing complex queries for {@link CommunicationLetter} entities in the database.
 * The main input is a {@link CommunicationLetterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommunicationLetter} or a {@link Page} of {@link CommunicationLetter} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommunicationLetterQueryService extends QueryService<CommunicationLetter> {

    private final Logger log = LoggerFactory.getLogger(CommunicationLetterQueryService.class);

    private final CommunicationLetterRepository communicationLetterRepository;

    public CommunicationLetterQueryService(CommunicationLetterRepository communicationLetterRepository) {
        this.communicationLetterRepository = communicationLetterRepository;
    }

    /**
     * Return a {@link List} of {@link CommunicationLetter} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommunicationLetter> findByCriteria(CommunicationLetterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommunicationLetter> specification = createSpecification(criteria);
        return communicationLetterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CommunicationLetter} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommunicationLetter> findByCriteria(CommunicationLetterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommunicationLetter> specification = createSpecification(criteria);
        return communicationLetterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommunicationLetterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommunicationLetter> specification = createSpecification(criteria);
        return communicationLetterRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<CommunicationLetter> createSpecification(CommunicationLetterCriteria criteria) {
        Specification<CommunicationLetter> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CommunicationLetter_.id));
            }
            if (criteria.getSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubject(), CommunicationLetter_.subject));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), CommunicationLetter_.type));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(CommunicationLetter_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            if (criteria.getFileResourceId() != null) {
                specification = specification.and(buildSpecification(criteria.getFileResourceId(),
                    root -> root.join(CommunicationLetter_.fileResource, JoinType.LEFT).get(FileResource_.id)));
            }
        }
        return specification;
    }
}
