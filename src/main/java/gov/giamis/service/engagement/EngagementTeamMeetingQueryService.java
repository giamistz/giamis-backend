package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementTeamMeeting;
import gov.giamis.domain.engagement.EngagementTeamMeeting_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.repository.engagement.EngagementTeamMeetingRepository;
import gov.giamis.service.engagement.dto.EngagementTeamMeetingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementTeamMeeting} entities in the database.
 * The main input is a {@link EngagementTeamMeetingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementTeamMeeting} or a {@link Page} of {@link EngagementTeamMeeting} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementTeamMeetingQueryService extends QueryService<EngagementTeamMeeting> {

    private final Logger log = LoggerFactory.getLogger(EngagementTeamMeetingQueryService.class);

    private final EngagementTeamMeetingRepository engagementTeamMeetingRepository;

    public EngagementTeamMeetingQueryService(EngagementTeamMeetingRepository engagementTeamMeetingRepository) {
        this.engagementTeamMeetingRepository = engagementTeamMeetingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementTeamMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementTeamMeeting> findByCriteria(EngagementTeamMeetingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementTeamMeeting> specification = createSpecification(criteria);
        return engagementTeamMeetingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementTeamMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementTeamMeeting> findByCriteria(EngagementTeamMeetingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementTeamMeeting> specification = createSpecification(criteria);
        return engagementTeamMeetingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementTeamMeetingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementTeamMeeting> specification = createSpecification(criteria);
        return engagementTeamMeetingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementTeamMeeting> createSpecification(EngagementTeamMeetingCriteria criteria) {
        Specification<EngagementTeamMeeting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementTeamMeeting_.id));
            }
            if (criteria.getMeetingDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMeetingDate(), EngagementTeamMeeting_.meetingDate));
            }
            if (criteria.getVenue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVenue(), EngagementTeamMeeting_.venue));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementTeamMeeting_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            
        }
        return specification;
    }
}
