package gov.giamis.service.engagement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementPhase;
import gov.giamis.domain.engagement.EngagementPhase_;
import gov.giamis.repository.engagement.EngagementPhaseRepository;
import gov.giamis.service.engagement.dto.EngagementPhaseCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementPhase} entities in the database.
 * The main input is a {@link EngagementPhaseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementPhase} or a {@link Page} of {@link EngagementPhase} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementPhaseQueryService extends QueryService<EngagementPhase> {

    private final Logger log = LoggerFactory.getLogger(EngagementPhaseQueryService.class);

    private final EngagementPhaseRepository engagementPhaseRepository;

    public EngagementPhaseQueryService(EngagementPhaseRepository engagementPhaseRepository) {
        this.engagementPhaseRepository = engagementPhaseRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementPhase} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementPhase> findByCriteria(EngagementPhaseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementPhase> specification = createSpecification(criteria);
        return engagementPhaseRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementPhase} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementPhase> findByCriteria(EngagementPhaseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementPhase> specification = createSpecification(criteria);
        return engagementPhaseRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementPhaseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementPhase> specification = createSpecification(criteria);
        return engagementPhaseRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementPhase> createSpecification(EngagementPhaseCriteria criteria) {
        Specification<EngagementPhase> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementPhase_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), EngagementPhase_.name));
            }
        }
        return specification;
    }
}
