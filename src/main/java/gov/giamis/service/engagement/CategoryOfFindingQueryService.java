
package gov.giamis.service.engagement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.CategoryOfFinding;
import gov.giamis.domain.engagement.CategoryOfFinding_;
import gov.giamis.repository.engagement.CategoryOfFindingRepository;
import gov.giamis.service.engagement.dto.CategoryOfFindingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CategoryOfFinding} entities in the database.
 * The main input is a {@link CategoryOfFindingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CategoryOfFinding} or a {@link Page} of {@link CategoryOfFinding} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CategoryOfFindingQueryService extends QueryService<CategoryOfFinding> {

    private final Logger log = LoggerFactory.getLogger(CategoryOfFindingQueryService.class);

    private final CategoryOfFindingRepository categoryOfFindingRepository;

    public CategoryOfFindingQueryService(CategoryOfFindingRepository categoryOfFindingRepository) {
        this.categoryOfFindingRepository = categoryOfFindingRepository;
    }

    /**
     * Return a {@link List} of {@link CategoryOfFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CategoryOfFinding> findByCriteria(CategoryOfFindingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CategoryOfFinding> specification = createSpecification(criteria);
        return categoryOfFindingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CategoryOfFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CategoryOfFinding> findByCriteria(CategoryOfFindingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CategoryOfFinding> specification = createSpecification(criteria);
        return categoryOfFindingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CategoryOfFindingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CategoryOfFinding> specification = createSpecification(criteria);
        return categoryOfFindingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<CategoryOfFinding> createSpecification(CategoryOfFindingCriteria criteria) {
        Specification<CategoryOfFinding> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CategoryOfFinding_.id));
            }
        }
        return specification;
    }
}
