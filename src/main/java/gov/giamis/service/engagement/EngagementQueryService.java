package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementMember_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.Engagement;
import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.service.engagement.dto.EngagementCriteria;

/**
 * Service for executing complex queries for {@link Engagement} entities in the database.
 * The main input is a {@link EngagementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Engagement} or a {@link Page} of {@link Engagement} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementQueryService extends QueryService<Engagement> {

    private final Logger log = LoggerFactory.getLogger(EngagementQueryService.class);

    private final EngagementRepository engagementRepository;

    public EngagementQueryService(EngagementRepository engagementRepository) {
        this.engagementRepository = engagementRepository;
    }

    /**
     * Return a {@link List} of {@link Engagement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Engagement> findByCriteria(EngagementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Engagement> specification = createSpecification(criteria);
        return engagementRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Engagement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Engagement> findByCriteria(EngagementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Engagement> specification = createSpecification(criteria);
        return engagementRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Engagement> specification = createSpecification(criteria);
        return engagementRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Engagement> createSpecification(EngagementCriteria criteria) {
        Specification<Engagement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Engagement_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Engagement_.process));
            }
            if (criteria.getAuditType() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditType(), Engagement_.auditType));
            }
            if (criteria.getScope() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScope(), Engagement_.scope));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Engagement_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Engagement_.endDate));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getFinancialYearId(),
                    root -> root.join(Engagement_.financialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(Engagement_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getParentEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getParentEngagementId(),
                    root -> root.join(Engagement_.parentEngagement, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getAuditableAreaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditableAreaId(),
                    root -> root.join(Engagement_.auditableArea, JoinType.LEFT).get(AuditableArea_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Engagement_.engagementMembers, JoinType.LEFT)
                        .join(EngagementMember_.user, JoinType.LEFT).get(SimpleUser_.id)));
            }
        }
        return specification;
    }
}
