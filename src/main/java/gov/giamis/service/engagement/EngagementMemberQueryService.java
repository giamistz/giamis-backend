package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementMember_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.SimpleUser_;
import gov.giamis.domain.setup.User_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.repository.engagement.EngagementMemberRepository;
import gov.giamis.service.engagement.dto.EngagementMemberCriteria;

/**
 * Service for executing complex queries for {@link EngagementMember} entities in the database.
 * The main input is a {@link EngagementMemberCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementMember} or a {@link Page} of {@link EngagementMember} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementMemberQueryService extends QueryService<EngagementMember> {

    private final Logger log = LoggerFactory.getLogger(EngagementMemberQueryService.class);

    private final EngagementMemberRepository engagementMemberRepository;

    public EngagementMemberQueryService(EngagementMemberRepository engagementMemberRepository) {
        this.engagementMemberRepository = engagementMemberRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementMember} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementMember> findByCriteria(EngagementMemberCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementMember> specification = createSpecification(criteria);
        return engagementMemberRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementMember} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementMember> findByCriteria(EngagementMemberCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementMember> specification = createSpecification(criteria);
        return engagementMemberRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementMemberCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementMember> specification = createSpecification(criteria);
        return engagementMemberRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementMember> createSpecification(EngagementMemberCriteria criteria) {
        Specification<EngagementMember> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementMember_.id));
            }
            if (criteria.getRole() != null) {
                specification = specification.and(buildSpecification(criteria.getRole(), EngagementMember_.role));
            }
            if (criteria.getAcceptedNda() != null) {
                specification = specification.and(buildSpecification(criteria.getAcceptedNda(), EngagementMember_.acceptedNda));
            }
            if (criteria.getAcceptanceDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAcceptanceDate(), EngagementMember_.acceptanceDate));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementMember_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(EngagementMember_.user, JoinType.LEFT).get(SimpleUser_.id)));
            }
        }
        return specification;
    }

    public List<EngagementMember> findAllByEngagement(Long engagementId) {
        return engagementMemberRepository.findByEngagement_Id(engagementId);
    }
}
