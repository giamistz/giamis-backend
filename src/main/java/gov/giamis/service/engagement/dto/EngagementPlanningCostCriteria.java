  
package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementPlanningCost;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link EngagementPlanningCost} entity. This class is used
 * in {@link EngagementPlanningCostResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-meetings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementPlanningCostCriteria implements Serializable, Criteria {


    private static final long serialVersionUID = 1L;

    private LongFilter id;
    
    private LongFilter gfsCodeId;
    
    private LongFilter engagementId;

    public EngagementPlanningCostCriteria(){
    }
    
    public LongFilter getId() {
		return id;
	}


	public void setId(LongFilter id) {
		this.id = id;
	}


	public LongFilter getGfsCodeId() {
		return gfsCodeId;
	}


	public void setGfsCodeId(LongFilter gfsCodeId) {
		this.gfsCodeId = gfsCodeId;
	}


	public LongFilter getEngagementId() {
		return engagementId;
	}


	public void setEngagementId(LongFilter engagementId) {
		this.engagementId = engagementId;
	}


	public EngagementPlanningCostCriteria(EngagementPlanningCostCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.gfsCodeId = other.gfsCodeId == null ? null : other.gfsCodeId.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
    }
    
    @Override
    public EngagementPlanningCostCriteria copy() {
        return new EngagementPlanningCostCriteria(this);
    }
       @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final EngagementPlanningCostCriteria that = (EngagementPlanningCostCriteria) o;
            return
                Objects.equals(id, that.id) &&
                Objects.equals(gfsCodeId, that.gfsCodeId)&&
                Objects.equals(engagementId, that.engagementId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(
            id,
            gfsCodeId,
            engagementId
            );
        }

		@Override
		public String toString() {
			return "EngagementPlanningCostCriteria [id=" + id + ", gfsCodeId=" + gfsCodeId + ", engagementId="
					+ engagementId + "]";
		}
}
