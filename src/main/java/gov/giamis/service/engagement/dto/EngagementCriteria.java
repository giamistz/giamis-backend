package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementPhase;
import gov.giamis.web.rest.engagement.EngagementResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.AuditType;
import gov.giamis.domain.enumeration.EngagementPhaseStatus;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link Engagement} entity. This class is used
 * in {@link EngagementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementCriteria implements Serializable, Criteria {
    /**
     * Class for filtering AuditType
     */
    public static class AuditTypeFilter extends Filter<AuditType> {

        public AuditTypeFilter() {
        }

        public AuditTypeFilter(AuditTypeFilter filter) {
            super(filter);
        }

        @Override
        public AuditTypeFilter copy() {
            return new AuditTypeFilter(this);
        }

    }

    /**
     * Class for filtering EngagementPhase
     */
    public static class EngagementPhaseFilter extends Filter<EngagementPhase> {

        public EngagementPhaseFilter() {
        }

        public EngagementPhaseFilter(EngagementPhaseFilter filter) {
            super(filter);
        }

        @Override
        public EngagementPhaseFilter copy() {
            return new EngagementPhaseFilter(this);
        }

    }

    /**
     * Class for filtering EngagementPhaseStatus
     */
    public static class EngagementPhaseStatusFilter extends Filter<EngagementPhaseStatus> {

        public EngagementPhaseStatusFilter() {
        }

        public EngagementPhaseStatusFilter(EngagementPhaseStatusFilter filter) {
            super(filter);
        }

        @Override
        public EngagementPhaseStatusFilter copy() {
            return new EngagementPhaseStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private AuditTypeFilter auditType;

    private StringFilter scope;

    private LocalDateFilter startDate;

    private LocalDateFilter endDate;

    private EngagementPhaseFilter phase;

    private EngagementPhaseStatusFilter phaseStatus;

    private LongFilter financialYearId;

    private LongFilter organisationUnitId;

    private LongFilter parentEngagementId;

    private LongFilter auditableAreaId;

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    private LongFilter userId;

    public EngagementCriteria() {
    }

    public EngagementCriteria(EngagementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.auditType = other.auditType == null ? null : other.auditType.copy();
        this.scope = other.scope == null ? null : other.scope.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.phase = other.phase == null ? null : other.phase.copy();
        this.phaseStatus = other.phaseStatus == null ? null : other.phaseStatus.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.parentEngagementId = other.parentEngagementId == null ? null : other.parentEngagementId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.auditableAreaId = other.auditableAreaId == null ? null : other.auditableAreaId.copy();

    }

    @Override
    public EngagementCriteria copy() {
        return new EngagementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public AuditTypeFilter getAuditType() {
        return auditType;
    }

    public void setAuditType(AuditTypeFilter auditType) {
        this.auditType = auditType;
    }

    public StringFilter getScope() {
        return scope;
    }

    public void setScope(StringFilter scope) {
        this.scope = scope;
    }

    public LocalDateFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateFilter startDate) {
        this.startDate = startDate;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public EngagementPhaseFilter getPhase() {
        return phase;
    }

    public void setPhase(EngagementPhaseFilter phase) {
        this.phase = phase;
    }

    public EngagementPhaseStatusFilter getPhaseStatus() {
        return phaseStatus;
    }

    public void setPhaseStatus(EngagementPhaseStatusFilter phaseStatus) {
        this.phaseStatus = phaseStatus;
    }

    public LongFilter getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(LongFilter financialYearId) {
        this.financialYearId = financialYearId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getParentEngagementId() {
        return parentEngagementId;
    }

    public void setParentEngagementId(LongFilter parentEngagementId) {
        this.parentEngagementId = parentEngagementId;
    }

    public LongFilter getAuditableAreaId() {
        return auditableAreaId;
    }

    public void setAuditableAreaId(LongFilter auditableAreaId) {
        this.auditableAreaId = auditableAreaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementCriteria that = (EngagementCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(auditType, that.auditType) &&
                Objects.equals(scope, that.scope) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(phase, that.phase) &&
                Objects.equals(phaseStatus, that.phaseStatus) &&
                Objects.equals(financialYearId, that.financialYearId) &&
                Objects.equals(organisationUnitId, that.organisationUnitId) &&
                Objects.equals(parentEngagementId, that.parentEngagementId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            auditType,
            scope,
            startDate,
            endDate,
            phase,
            phaseStatus,
            financialYearId,
            organisationUnitId,
            parentEngagementId
        );
    }

}
