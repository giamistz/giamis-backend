package gov.giamis.service.engagement.dto;

public class SpecificObjectiveDTO {

	public SpecificObjectiveDTO() {

	}

	public SpecificObjectiveDTO(String name) {
		super();
		this.name = name;
	}

	private Long id;
	
	private String name;
	
	private Boolean isRefined;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsRefined() {
		return isRefined;
	}

	public void setIsRefined(Boolean isRefined) {
		this.isRefined = isRefined;
	}
}