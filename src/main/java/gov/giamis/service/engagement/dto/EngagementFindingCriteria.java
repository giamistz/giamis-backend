  
package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementFinding;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngagementFinding} entity. This class is used
 * in {@link EngagementFindingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-meetings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementFindingCriteria implements Serializable, Criteria {


    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter fiveAttribute;
    
    private LongFilter workProgramId;
    
    private LongFilter engagementProcedureId;

    public EngagementFindingCriteria(){
    }
    
    
    public LongFilter getId() {
		return id;
	}


	public void setId(LongFilter id) {
		this.id = id;
	}


	public StringFilter getFiveAttribute() {
		return fiveAttribute;
	}


	public void setFiveAttribute(StringFilter fiveAttribute) {
		this.fiveAttribute = fiveAttribute;
	}


	public LongFilter getWorkProgramId() {
		return workProgramId;
	}


	public void setWorkProgramId(LongFilter workProgramId) {
		this.workProgramId = workProgramId;
	}

	public LongFilter getEngagementProcedureId() {
		return engagementProcedureId;
	}


	public void setEngagementProcedureId(LongFilter engagementProcedureId) {
		this.engagementProcedureId = engagementProcedureId;
	}


	public EngagementFindingCriteria(EngagementFindingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.fiveAttribute = other.fiveAttribute == null ? null : other.fiveAttribute.copy();
        this.workProgramId = other.workProgramId == null ? null : other.workProgramId.copy();
        this.engagementProcedureId = other.engagementProcedureId == null ? null : other.engagementProcedureId.copy();
    }
    
    @Override
    public EngagementFindingCriteria copy() {
        return new EngagementFindingCriteria(this);
    }
	   @Override
	    public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        final EngagementFindingCriteria that = (EngagementFindingCriteria) o;
	        return
	            Objects.equals(id, that.id) &&
	            Objects.equals(fiveAttribute, that.fiveAttribute) &&
	            Objects.equals(workProgramId, that.workProgramId)&&
	            Objects.equals(engagementProcedureId, that.engagementProcedureId);
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(
	        id,
	        fiveAttribute,
	        workProgramId,
	        engagementProcedureId
	        );
	    }

		@Override
		public String toString() {
			return "EngagementFindingCriteria [id=" + id + ", fiveAttribute=" + fiveAttribute + ", workProgramId="
					+ workProgramId + ", engagementProcedureId=" + engagementProcedureId + "]";
		}
}
