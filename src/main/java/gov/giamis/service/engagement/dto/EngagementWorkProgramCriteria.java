package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementWorkProgram;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link EngagementWorkProgram} entity. This class is used in
 * {@link EngagementWorkProgramRexxxxxxx} to receive all the possible filtering options
 * from the Http GET request parameters. For example the following could be a
 * valid request:
 * {@dateCompleted /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class EngagementWorkProgramCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter datePlanned;

    private LocalDateFilter dateCompleted;

    private LongFilter specificObjective;
    
    private LongFilter engagementProcedure;

    public EngagementWorkProgramCriteria() {
    }

    public LongFilter getId() {
		return id;
	}


	public void setId(LongFilter id) {
		this.id = id;
	}


	public LocalDateFilter getDatePlanned() {
		return datePlanned;
	}


	public void setDatePlanned(LocalDateFilter datePlanned) {
		this.datePlanned = datePlanned;
	}


	public LocalDateFilter getDateCompleted() {
		return dateCompleted;
	}


	public void setDateCompleted(LocalDateFilter dateCompleted) {
		this.dateCompleted = dateCompleted;
	}


	public LongFilter getSpecificObjective() {
		return specificObjective;
	}


	public void setSpecificObjective(LongFilter specificObjective) {
		this.specificObjective = specificObjective;
	}


	public LongFilter getEngagementProcedure() {
		return engagementProcedure;
	}


	public void setEngagementProcedure(LongFilter engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}


	public EngagementWorkProgramCriteria(EngagementWorkProgramCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.datePlanned = other.datePlanned == null ? null : other.datePlanned.copy();
        this.dateCompleted = other.dateCompleted == null ? null : other.dateCompleted.copy();
        this.specificObjective = other.specificObjective == null ? null : other.specificObjective.copy();
        this.engagementProcedure = other.engagementProcedure == null ? null : other.engagementProcedure.copy();
    }

    @Override
    public EngagementWorkProgramCriteria copy() {
        return new EngagementWorkProgramCriteria(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementWorkProgramCriteria that = (EngagementWorkProgramCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(datePlanned, that.datePlanned) && Objects.equals(dateCompleted, that.dateCompleted)
                && Objects.equals(specificObjective, that.specificObjective)
                        && Objects.equals(engagementProcedure, that.engagementProcedure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datePlanned, dateCompleted, specificObjective,engagementProcedure);
    }

}
