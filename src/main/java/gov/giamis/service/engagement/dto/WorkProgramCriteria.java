package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.WorkProgram;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link WorkProgram} entity. This class is used in
 * {@link WorkProgramRexxxxxxx} to receive all the possible filtering options
 * from the Http GET request parameters. For example the following could be a
 * valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class WorkProgramCriteria implements Serializable, Criteria {

	private static final long serialVersionUID = 1L;

	private LongFilter id;

	private StringFilter name;

	private StringFilter code;

	private LongFilter engagementProcedureId;

	public WorkProgramCriteria() {
	}

	public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getName() {
		return name;
	}

	public void setName(StringFilter name) {
		this.name = name;
	}

	public StringFilter getCode() {
		return code;
	}

	public void setCode(StringFilter code) {
		this.code = code;
	}
	
	

	public LongFilter getEngagementProcedureId() {
		return engagementProcedureId;
	}

	public void setEngagementProcedureId(LongFilter engagementProcedureId) {
		this.engagementProcedureId = engagementProcedureId;
	}

	public WorkProgramCriteria(WorkProgramCriteria other) {
		this.id = other.id == null ? null : other.id.copy();
		this.name = other.name == null ? null : other.name.copy();
		this.code = other.code == null ? null : other.code.copy();
		this.engagementProcedureId = other.engagementProcedureId == null ? null : other.engagementProcedureId.copy();
	}

	@Override
	public WorkProgramCriteria copy() {
		return new WorkProgramCriteria(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final WorkProgramCriteria that = (WorkProgramCriteria) o;
		return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(code, that.code)
				&& Objects.equals(engagementProcedureId, that.engagementProcedureId);

	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, code, engagementProcedureId);
	}

	@Override
	public String toString() {
		return "WorkProgramCriteria [id=" + id + ", name=" + name + ", code=" + code + ", engagementProcedureId="
				+ engagementProcedureId + "]";
	}
}
