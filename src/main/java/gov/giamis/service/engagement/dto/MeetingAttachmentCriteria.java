package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.MeetingAttachment;
import gov.giamis.web.rest.engagement.MeetingAttachmentResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link MeetingAttachment} entity. This class is used
 * in {@link MeetingAttachmentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /meeting-attachments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MeetingAttachmentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter engagementMeetingId;

    private LongFilter fileResourceId;

    public MeetingAttachmentCriteria(){
    }

    public MeetingAttachmentCriteria(MeetingAttachmentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.engagementMeetingId = other.engagementMeetingId == null ? null : other.engagementMeetingId.copy();
        this.fileResourceId = other.fileResourceId == null ? null : other.fileResourceId.copy();
    }

    @Override
    public MeetingAttachmentCriteria copy() {
        return new MeetingAttachmentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getEngagementMeetingId() {
        return engagementMeetingId;
    }

    public void setEngagementMeetingId(LongFilter engagementMeetingId) {
        this.engagementMeetingId = engagementMeetingId;
    }

    public LongFilter getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(LongFilter fileResourceId) {
        this.fileResourceId = fileResourceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MeetingAttachmentCriteria that = (MeetingAttachmentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(engagementMeetingId, that.engagementMeetingId) &&
            Objects.equals(fileResourceId, that.fileResourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        engagementMeetingId,
        fileResourceId
        );
    }

    @Override
    public String toString() {
        return "MeetingAttachmentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (engagementMeetingId != null ? "engagementMeetingId=" + engagementMeetingId + ", " : "") +
                (fileResourceId != null ? "fileResourceId=" + fileResourceId + ", " : "") +
            "}";
    }

}
