package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementItem;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngagementItem} entity. This class is used
 * in {@link EngagementItemResource} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /engagement-objectives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class EngagementItemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private DoubleFilter amount;

    private LongFilter engagementProcedureId;

    public EngagementItemCriteria() {
    }

    public EngagementItemCriteria(EngagementItemCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.engagementProcedureId = other.engagementProcedureId == null ? null : other.engagementProcedureId.copy();
    }

    @Override
    public EngagementItemCriteria copy() {
        return new EngagementItemCriteria(this);
    }
  
    public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getName() {
		return name;
	}

	public void setName(StringFilter name) {
		this.name = name;
	}

	public DoubleFilter getAmount() {
		return amount;
	}

	public void setAmount(DoubleFilter amount) {
		this.amount = amount;
	}

	public LongFilter getEngagementProcedureId() {
		return engagementProcedureId;
	}

	public void setEngagementProcedureId(LongFilter engagementProcedureId) {
		this.engagementProcedureId = engagementProcedureId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementItemCriteria that = (EngagementItemCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(amount, that.amount)
                && Objects.equals(engagementProcedureId, that.engagementProcedureId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, amount, engagementProcedureId);
    }

	@Override
	public String toString() {
		return "EngagementItemCriteria [id=" + id + ", name=" + name + ", amount=" + amount + ", engagementProcedureId="
				+ engagementProcedureId + "]";
	}
}