package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.MatterOfNextAudit;
import gov.giamis.web.rest.engagement.MatterOfNextAuditResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link MatterOfNextAudit} entity. This class is used
 * in {@link MatterOfNextAuditResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /matter-of-next-audits?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MatterOfNextAuditCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter organisationUnitId;

    private LongFilter auditeeId;

    private LongFilter engagementId;

    private LongFilter auditableAreaId;

    public MatterOfNextAuditCriteria(){
    }

    public MatterOfNextAuditCriteria(MatterOfNextAuditCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.auditeeId = other.auditeeId == null ? null : other.auditeeId.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.auditableAreaId = other.auditableAreaId == null ? null : other.auditableAreaId.copy();
    }

    @Override
    public MatterOfNextAuditCriteria copy() {
        return new MatterOfNextAuditCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getAuditeeId() {
        return auditeeId;
    }

    public void setAuditeeId(LongFilter auditeeId) {
        this.auditeeId = auditeeId;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    public LongFilter getAuditableAreaId() {
        return auditableAreaId;
    }

    public void setAuditableAreaId(LongFilter auditableAreaId) {
        this.auditableAreaId = auditableAreaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MatterOfNextAuditCriteria that = (MatterOfNextAuditCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(organisationUnitId, that.organisationUnitId) &&
            Objects.equals(auditeeId, that.auditeeId) &&
            Objects.equals(engagementId, that.engagementId) &&
            Objects.equals(auditableAreaId, that.auditableAreaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        organisationUnitId,
        auditeeId,
        engagementId,
        auditableAreaId
        );
    }

    @Override
    public String toString() {
        return "MatterOfNextAuditCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
                (auditeeId != null ? "auditeeId=" + auditeeId + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
                (auditableAreaId != null ? "auditableAreaId=" + auditableAreaId + ", " : "") +
            "}";
    }

}
