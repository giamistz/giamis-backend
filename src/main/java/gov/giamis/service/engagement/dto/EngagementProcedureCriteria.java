package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.web.rest.engagement.EngagementProcedureResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngagementProcedure} entity. This class is used
 * in {@link EngagementProcedureResource} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /engagement-objectives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class EngagementProcedureCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private BooleanFilter approved;

    private LongFilter specificObjectiveId;

    public EngagementProcedureCriteria() {
    }

    public EngagementProcedureCriteria(EngagementProcedureCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.approved = other.approved == null ? null : other.approved.copy();
        this.specificObjectiveId = other.specificObjectiveId == null ? null : other.specificObjectiveId.copy();
    }

    @Override
    public EngagementProcedureCriteria copy() {
        return new EngagementProcedureCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public BooleanFilter getApproved() {
        return approved;
    }

    public void setApproved(BooleanFilter approved) {
        this.approved = approved;
    }

    public LongFilter getSpecificObjectiveId() {
		return specificObjectiveId;
	}

	public void setSpecificObjectiveId(LongFilter specificObjectiveId) {
		this.specificObjectiveId = specificObjectiveId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementProcedureCriteria that = (EngagementProcedureCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(approved, that.approved)
                && Objects.equals(specificObjectiveId, that.specificObjectiveId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, approved, specificObjectiveId);
    }

	@Override
	public String toString() {
		return "EngagementProcedureCriteria [id=" + id + ", name=" + name + ", approved=" + approved
				+ ", specificObjectiveId=" + specificObjectiveId + "]";
	}
}