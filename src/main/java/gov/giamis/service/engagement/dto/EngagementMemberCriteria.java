package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.web.rest.engagement.EngagementMemberResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.EngagementRole;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link EngagementMember} entity. This class is used
 * in {@link EngagementMemberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-members?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementMemberCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EngagementRole
     */
    public static class EngagementRoleFilter extends Filter<EngagementRole> {

        public EngagementRoleFilter() {
        }

        public EngagementRoleFilter(EngagementRoleFilter filter) {
            super(filter);
        }

        @Override
        public EngagementRoleFilter copy() {
            return new EngagementRoleFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private EngagementRoleFilter role;

    private BooleanFilter acceptedNda;

    private LocalDateFilter acceptanceDate;

    private LongFilter engagementId;

    private LongFilter userId;

    public EngagementMemberCriteria(){
    }

    public EngagementMemberCriteria(EngagementMemberCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.role = other.role == null ? null : other.role.copy();
        this.acceptedNda = other.acceptedNda == null ? null : other.acceptedNda.copy();
        this.acceptanceDate = other.acceptanceDate == null ? null : other.acceptanceDate.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public EngagementMemberCriteria copy() {
        return new EngagementMemberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public EngagementRoleFilter getRole() {
        return role;
    }

    public void setRole(EngagementRoleFilter role) {
        this.role = role;
    }

    public BooleanFilter getAcceptedNda() {
        return acceptedNda;
    }

    public void setAcceptedNda(BooleanFilter acceptedNda) {
        this.acceptedNda = acceptedNda;
    }

    public LocalDateFilter getAcceptanceDate() {
        return acceptanceDate;
    }

    public void setAcceptanceDate(LocalDateFilter acceptanceDate) {
        this.acceptanceDate = acceptanceDate;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementMemberCriteria that = (EngagementMemberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(role, that.role) &&
            Objects.equals(acceptedNda, that.acceptedNda) &&
            Objects.equals(acceptanceDate, that.acceptanceDate) &&
            Objects.equals(engagementId, that.engagementId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        role,
        acceptedNda,
        acceptanceDate,
        engagementId,
        userId
        );
    }

    @Override
    public String toString() {
        return "EngagementMemberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (role != null ? "role=" + role + ", " : "") +
                (acceptedNda != null ? "acceptedNda=" + acceptedNda + ", " : "") +
                (acceptanceDate != null ? "acceptanceDate=" + acceptanceDate + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
