package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementReport;
import gov.giamis.web.rest.engagement.EngagementReportResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.EngagementReportStatus;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link EngagementReport} entity. This class is used
 * in {@link EngagementReportResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-reports?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementReportCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EngagementReportStatus
     */
    public static class EngagementReportStatusFilter extends Filter<EngagementReportStatus> {

        public EngagementReportStatusFilter() {
        }

        public EngagementReportStatusFilter(EngagementReportStatusFilter filter) {
            super(filter);
        }

        @Override
        public EngagementReportStatusFilter copy() {
            return new EngagementReportStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private EngagementReportStatusFilter status;

    private LongFilter engagementId;

    public EngagementReportCriteria(){
    }

    public EngagementReportCriteria(EngagementReportCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
    }

    @Override
    public EngagementReportCriteria copy() {
        return new EngagementReportCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public EngagementReportStatusFilter getStatus() {
        return status;
    }

    public void setStatus(EngagementReportStatusFilter status) {
        this.status = status;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementReportCriteria that = (EngagementReportCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(status, that.status) &&
            Objects.equals(engagementId, that.engagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        status,
        engagementId
        );
    }

    @Override
    public String toString() {
        return "EngagementReportCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
            "}";
    }

}
