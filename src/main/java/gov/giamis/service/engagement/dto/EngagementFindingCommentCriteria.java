  
package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementFindingComment;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngagementFindingComment} entity. This class is used
 * in {@link EngagementFindingCommentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-meetings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementFindingCommentCriteria implements Serializable, Criteria {


    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter comments;
    
    private LongFilter userId;
    
    private LongFilter engagementFindingId;

    private LongFilter fileResourceId;

    public EngagementFindingCommentCriteria(){
    }
    
    
	public LongFilter getId() {
		return id;
	}


	public void setId(LongFilter id) {
		this.id = id;
	}


	public StringFilter getComments() {
		return comments;
	}


	public void setComments(StringFilter comments) {
		this.comments = comments;
	}


	public LongFilter getUserId() {
		return userId;
	}


	public void setUserId(LongFilter userId) {
		this.userId = userId;
	}


	public LongFilter getEngagementFindingId() {
		return engagementFindingId;
	}


	public void setEngagementFindingId(LongFilter engagementFindingId) {
		this.engagementFindingId = engagementFindingId;
	}


	public LongFilter getFileResourceId() {
		return fileResourceId;
	}


	public void setFileResourceId(LongFilter fileResourceId) {
		this.fileResourceId = fileResourceId;
	}


	public EngagementFindingCommentCriteria(EngagementFindingCommentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.engagementFindingId = other.engagementFindingId == null ? null : other.engagementFindingId.copy();
        this.fileResourceId = other.fileResourceId == null ? null : other.fileResourceId.copy();
    }
    
    @Override
    public EngagementFindingCommentCriteria copy() {
        return new EngagementFindingCommentCriteria(this);
    }
	   @Override
	    public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        final EngagementFindingCommentCriteria that = (EngagementFindingCommentCriteria) o;
	        return
	            Objects.equals(id, that.id) &&
	            Objects.equals(comments, that.comments) &&
	            Objects.equals(userId, that.userId)&&
	            Objects.equals(engagementFindingId, that.engagementFindingId) &&
	            Objects.equals(fileResourceId, that.fileResourceId);
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(
	        id,
	        comments,
	        userId,
	        engagementFindingId,
	        fileResourceId
	        );
	    }

}
