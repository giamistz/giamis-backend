package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementPhase;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngagementPhase} entity. This class is used
 * in {@link EngagementPhaseRexxxxxxx} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class EngagementPhaseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;


    public EngagementPhaseCriteria() {
    }

    public EngagementPhaseCriteria(EngagementPhaseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
    }

    @Override
    public EngagementPhaseCriteria copy() {
        return new EngagementPhaseCriteria(this);
    }
    
    public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getName() {
		return name;
	}

	public void setName(StringFilter name) {
		this.name = name;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementPhaseCriteria that = (EngagementPhaseCriteria) o;
        return Objects.equals(id, that.id) 
        && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

	@Override
	public String toString() {
		return "EngagementPhaseCriteria [id=" + id + ", name=" + name + "]";
	}
}
