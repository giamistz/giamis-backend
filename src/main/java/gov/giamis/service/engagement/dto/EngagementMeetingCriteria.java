package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementMeeting;
import gov.giamis.web.rest.engagement.EngagementMeetingResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.MeetingType;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link EngagementMeeting} entity. This class is used
 * in {@link EngagementMeetingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-meetings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementMeetingCriteria implements Serializable, Criteria {
    /**
     * Class for filtering MeetingType
     */
    public static class MeetingTypeFilter extends Filter<MeetingType> {

        public MeetingTypeFilter() {
        }

        public MeetingTypeFilter(MeetingTypeFilter filter) {
            super(filter);
        }

        @Override
        public MeetingTypeFilter copy() {
            return new MeetingTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private MeetingTypeFilter meetingType;

    private LocalDateFilter meetingDate;

    private StringFilter venue;

    private LongFilter engagementId;

    public EngagementMeetingCriteria(){
    }

    public EngagementMeetingCriteria(EngagementMeetingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.meetingType = other.meetingType == null ? null : other.meetingType.copy();
        this.meetingDate = other.meetingDate == null ? null : other.meetingDate.copy();
        this.venue = other.venue == null ? null : other.venue.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
    }

    @Override
    public EngagementMeetingCriteria copy() {
        return new EngagementMeetingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public MeetingTypeFilter getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(MeetingTypeFilter meetingType) {
        this.meetingType = meetingType;
    }

    public LocalDateFilter getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(LocalDateFilter meetingDate) {
        this.meetingDate = meetingDate;
    }

    public StringFilter getVenue() {
        return venue;
    }

    public void setVenue(StringFilter venue) {
        this.venue = venue;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementMeetingCriteria that = (EngagementMeetingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(meetingType, that.meetingType) &&
            Objects.equals(meetingDate, that.meetingDate) &&
            Objects.equals(venue, that.venue) &&
            Objects.equals(engagementId, that.engagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        meetingType,
        meetingDate,
        venue,
        engagementId
        );
    }

    @Override
    public String toString() {
        return "EngagementMeetingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (meetingType != null ? "meetingType=" + meetingType + ", " : "") +
                (meetingDate != null ? "meetingDate=" + meetingDate + ", " : "") +
                (venue != null ? "venue=" + venue + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
            "}";
    }

}
