package gov.giamis.service.engagement.dto;

public class EngagementFindingDTO {
    private Long id;
    private String description;
    private String cause;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public EngagementFindingDTO() {}

    public EngagementFindingDTO(String status, Long id, String description, String cause) {
        this.id = id;
        this.description = description;
        this.cause = cause;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
