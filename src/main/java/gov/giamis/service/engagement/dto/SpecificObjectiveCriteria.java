package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.SpecificObjective;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link SpecificObjective} entity. This class is used
 * in {@link SpecificObjectiveRexxxxxxx} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class SpecificObjectiveCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private LongFilter engagementId;


    private BooleanFilter isRefined;

    public SpecificObjectiveCriteria() {
    }

    public SpecificObjectiveCriteria(SpecificObjectiveCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.isRefined = other.isRefined == null ? null : other.isRefined.copy();
    }

    @Override
    public SpecificObjectiveCriteria copy() {
        return new SpecificObjectiveCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public BooleanFilter getIsRefined() {
        return isRefined;
    }

    public void setIsRefined(BooleanFilter isRefined) {
        this.isRefined = isRefined;
    }


    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpecificObjectiveCriteria that = (SpecificObjectiveCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(description, that.description)
                 && Objects.equals(engagementId, that.engagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, engagementId);
    }

    @Override
    public String toString() {
        return "SpecificObjectiveCriteria{" + (id != null ? "id=" + id + ", " : "")
                + (description != null ? "description=" + description + ", " : "")
                + (engagementId != null ? "engagementId=" + engagementId + ", " : "") + "}";
    }

}
