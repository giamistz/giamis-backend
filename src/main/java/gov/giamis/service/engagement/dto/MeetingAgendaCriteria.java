package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.MeetingAgenda;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link MeetingAgenda} entity. This class is used
 * in {@link MeetingAgendaRexxxxxxx} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class MeetingAgendaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter agenda;

    private LongFilter engagementTeamMeeting;

    public MeetingAgendaCriteria() {
    }

    public MeetingAgendaCriteria(MeetingAgendaCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.agenda = other.agenda == null ? null : other.agenda.copy();
        this.engagementTeamMeeting = other.engagementTeamMeeting == null ? null : other.engagementTeamMeeting.copy();
    }

    @Override
    public MeetingAgendaCriteria copy() {
        return new MeetingAgendaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAgenda() {
        return agenda;
    }

    public void setAgenda(StringFilter agenda) {
        this.agenda = agenda;
    }

    public LongFilter getEngagementTeamMeetingId() {
        return engagementTeamMeeting;
    }

    public void setEngagementTeamMeetingId(LongFilter engagementTeamMeeting) {
        this.engagementTeamMeeting = engagementTeamMeeting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MeetingAgendaCriteria that = (MeetingAgendaCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(agenda, that.agenda)
                 && Objects.equals(engagementTeamMeeting, that.engagementTeamMeeting);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, agenda, engagementTeamMeeting);
    }

    @Override
    public String toString() {
        return "MeetingAgendaCriteria{" + (id != null ? "id=" + id + ", " : "")
                + (agenda != null ? "agenda=" + agenda + ", " : "")
                + (engagementTeamMeeting != null ? "engagementTeamMeeting=" + engagementTeamMeeting + ", " : "") + "}";
    }

}
