package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.MeetingDeliverable;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link MeetingDeliverable} entity. This class is used
 * in {@link MeetingDeliverableRexxxxxxx} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /risk-ratings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class MeetingDeliverableCriteria implements Serializable, Criteria {

	private static final long serialVersionUID = 1L;

	private LongFilter id;

	private StringFilter deliverable;

	private LocalDateFilter date;

	private LongFilter engagementTeamMeeting;

	public MeetingDeliverableCriteria() {
	}

	public MeetingDeliverableCriteria(MeetingDeliverableCriteria other) {
		this.id = other.id == null ? null : other.id.copy();
		this.deliverable = other.deliverable == null ? null : other.deliverable.copy();
		this.date = other.date == null ? null : other.date.copy();
		this.engagementTeamMeeting = other.engagementTeamMeeting == null ? null : other.engagementTeamMeeting.copy();
	}

	@Override
	public MeetingDeliverableCriteria copy() {
		return new MeetingDeliverableCriteria(this);
	}

	public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getDeliverable() {
		return deliverable;
	}

	public void setDeliverable(StringFilter deliverable) {
		this.deliverable = deliverable;
	}

	public LocalDateFilter getDate() {
		return date;
	}

	public void setDate(LocalDateFilter date) {
		this.date = date;
	}

	public LongFilter getEngagementTeamMeetingId() {
		return engagementTeamMeeting;
	}

	public void setEngagementTeamMeetingId(LongFilter engagementTeamMeeting) {
		this.engagementTeamMeeting = engagementTeamMeeting;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final MeetingDeliverableCriteria that = (MeetingDeliverableCriteria) o;
		return Objects.equals(id, that.id) && Objects.equals(deliverable, that.deliverable)
				&& Objects.equals(date, that.date) && Objects.equals(engagementTeamMeeting, that.engagementTeamMeeting);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, deliverable, date, engagementTeamMeeting);
	}

	@Override
	public String toString() {
		return "MeetingDeliverableCriteria{" + (id != null ? "id=" + id + ", " : "")
				+ (deliverable != null ? "deliverable=" + deliverable + ", " : "")
				+ (date != null ? "date=" + date + ", " : "")
				+ (engagementTeamMeeting != null ? "engagementTeamMeeting=" + engagementTeamMeeting + ", " : "") + "}";
	}

}
