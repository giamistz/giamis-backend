package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementObjective;
import gov.giamis.web.rest.engagement.EngagementObjectiveResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link EngagementObjective} entity. This class is used
 * in {@link EngagementObjectiveResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-objectives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementObjectiveCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;


    public EngagementObjectiveCriteria(){
    }

    public EngagementObjectiveCriteria(EngagementObjectiveCriteria other){
        this.id = other.id == null ? null : other.id.copy();
    }

    @Override
    public EngagementObjectiveCriteria copy() {
        return new EngagementObjectiveCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementObjectiveCriteria that = (EngagementObjectiveCriteria) o;
        return
            Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id
        );
    }

    @Override
    public String toString() {
        return "EngagementObjectiveCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
            "}";
    }

}
