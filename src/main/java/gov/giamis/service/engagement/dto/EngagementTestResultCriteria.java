package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementTestResult;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link EngagementTestResult} entity. This class is used
 * in {@link EngagementTestResultResource} to receive all the possible filtering
 * options from the Http GET request parameters. For example the following could
 * be a valid request:
 * {@code /engagement-objectives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific
 * {@link Filter} class are used, we need to use fix type specific filters.
 */
public class EngagementTestResultCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter engagementItemId;

    private LongFilter workProgramId;

    private LongFilter engagementProcedureId;

    private LongFilter engagementObjectiveId;

    public EngagementTestResultCriteria() {
    }

    public EngagementTestResultCriteria(EngagementTestResultCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.engagementItemId = other.engagementItemId == null ? null : other.engagementItemId.copy();
        this.workProgramId = other.workProgramId == null ? null : other.workProgramId.copy();
        this.engagementProcedureId = other.engagementProcedureId == null ? null : other.engagementProcedureId.copy();
        this.engagementObjectiveId = other.engagementObjectiveId == null ? null : other.engagementObjectiveId.copy();
    }

    @Override
    public EngagementTestResultCriteria copy() {
        return new EngagementTestResultCriteria(this);
    }
  
    public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public LongFilter getEngagementItemId() {
		return engagementItemId;
	}

	public void setEngagementItemId(LongFilter engagementItemId) {
		this.engagementItemId = engagementItemId;
	}

	public LongFilter getWorkProgramId() {
		return workProgramId;
	}

	public void setWorkProgramId(LongFilter workProgramId) {
		this.workProgramId = workProgramId;
	}

	public LongFilter getEngagementObjectiveId() {
		return engagementObjectiveId;
	}

	public void setEngagementObjectiveId(LongFilter engagementObjectiveId) {
		this.engagementObjectiveId = engagementObjectiveId;
	}

	public LongFilter getEngagementProcedureId() {
        return engagementProcedureId;
    }

    public void setEngagementProcedureId(LongFilter engagementProcedureId) {
        this.engagementProcedureId = engagementProcedureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementTestResultCriteria that = (EngagementTestResultCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(engagementItemId, that.engagementItemId) && Objects.equals(workProgramId, that.workProgramId)
                && Objects.equals(engagementProcedureId, that.engagementProcedureId)
                && Objects.equals(engagementObjectiveId, that.engagementObjectiveId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, engagementItemId, workProgramId, engagementProcedureId,engagementObjectiveId);
    }

	@Override
	public String toString() {
		return "EngagementTestResultCriteria [id=" + id + ", engagementItemId=" + engagementItemId + ", workProgramId="
				+ workProgramId + ", engagementProcedureId=" + engagementProcedureId + ", engagementObjectiveId="
				+ engagementObjectiveId + "]";
	}

}