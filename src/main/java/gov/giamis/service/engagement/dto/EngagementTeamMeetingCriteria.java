package gov.giamis.service.engagement.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngagementTeamMeeting;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link EngagementTeamMeeting} entity. This class is used
 * in {@link EngagementTeamMeetingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-meetings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementTeamMeetingCriteria implements Serializable, Criteria {


    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter meetingDate;

    private StringFilter venue;

    private LongFilter engagementId;
    
    private LongFilter organisationUnitId;
    
    private LongFilter financialYearId;
    
    private LongFilter engagementObjectiveId;

    public EngagementTeamMeetingCriteria(){
    }

    public EngagementTeamMeetingCriteria(EngagementTeamMeetingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.meetingDate = other.meetingDate == null ? null : other.meetingDate.copy();
        this.venue = other.venue == null ? null : other.venue.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
        this.engagementObjectiveId = other.engagementObjectiveId == null ? null : other.engagementObjectiveId.copy();
    }
    
    @Override
    public EngagementTeamMeetingCriteria copy() {
        return new EngagementTeamMeetingCriteria(this);
    }

	public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public LocalDateFilter getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(LocalDateFilter meetingDate) {
		this.meetingDate = meetingDate;
	}

	public StringFilter getVenue() {
		return venue;
	}

	public void setVenue(StringFilter venue) {
		this.venue = venue;
	}

	public LongFilter getEngagementId() {
		return engagementId;
	}

	public void setEngagementId(LongFilter engagementId) {
		this.engagementId = engagementId;
	}

	public LongFilter getOrganisationUnitId() {
		return organisationUnitId;
	}

	public void setOrganisationUnitId(LongFilter organisationUnitId) {
		this.organisationUnitId = organisationUnitId;
	}

	public LongFilter getFinancialYearId() {
		return financialYearId;
	}

	public void setFinancialYearId(LongFilter financialYearId) {
		this.financialYearId = financialYearId;
	}

	public LongFilter getEngagementObjectiveId() {
		return engagementObjectiveId;
	}

	public void setEngagementObjectiveId(LongFilter engagementObjectiveId) {
		this.engagementObjectiveId = engagementObjectiveId;
	}
	
	   @Override
	    public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        final EngagementTeamMeetingCriteria that = (EngagementTeamMeetingCriteria) o;
	        return
	            Objects.equals(id, that.id) &&
	            Objects.equals(meetingDate, that.meetingDate) &&
	            Objects.equals(venue, that.venue) &&
	            Objects.equals(engagementId, that.engagementId)&&
	            Objects.equals(organisationUnitId, that.organisationUnitId)&&
	            Objects.equals(financialYearId, that.financialYearId)&&
	            Objects.equals(engagementObjectiveId, that.engagementObjectiveId);
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(
	        id,
	        meetingDate,
	        venue,
	        engagementId,
	        organisationUnitId,
	        financialYearId,
	        engagementObjectiveId
	        );
	    }

		@Override
		public String toString() {
			return "EngagementTeamMeetingCriteria [id=" + id + ", meetingDate=" + meetingDate + ", venue=" + venue
					+ ", engagementId=" + engagementId + ", organisationUnitId=" + organisationUnitId
					+ ", financialYearId=" + financialYearId + ", engagementObjectiveId=" + engagementObjectiveId + "]";
		}
}
