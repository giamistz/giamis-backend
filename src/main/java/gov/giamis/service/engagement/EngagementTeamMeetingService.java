package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementTeamMeeting;

import gov.giamis.domain.engagement.EngagementTeamMeetingMember;
import gov.giamis.domain.engagement.MeetingAgenda;
import gov.giamis.domain.engagement.MeetingDeliverable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementTeamMeeting}.
 */
public interface EngagementTeamMeetingService {

    /**
     * Save a engagementTeamMeeting.
     *
     * @param engagementTeamMeeting the entity to save.
     * @return the persisted entity.
     */
    EngagementTeamMeeting save(EngagementTeamMeeting engagementTeamMeeting);

    /**
     * Get all the engagementTeamMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementTeamMeeting> findAll(Pageable pageable);


    /**
     * Get the "id" engagementTeamMeeting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementTeamMeeting> findOne(Long id);

    /**
     * Delete the "id" engagementTeamMeeting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    EngagementTeamMeeting findByEngagementId(Long id);
    
    Long countEngagementTeamMeetingByEngagementId(Long id);

    List<MeetingDeliverable> getDeliverables(Long teamMeetingId);

    List<MeetingAgenda> getAgendas(Long teamMeetingId);

    List<EngagementTeamMeetingMember> getMembers(Long teamMeetingId);

    MeetingDeliverable saveDeliverable(MeetingDeliverable deliverable);

    MeetingAgenda saveAgenda(MeetingAgenda agenda);

    EngagementTeamMeetingMember saveMember(EngagementTeamMeetingMember member);

    void deleteDeliverable(Long id);

    void deleteAgenda(Long id);

    void deleteMember(Long id);
}
