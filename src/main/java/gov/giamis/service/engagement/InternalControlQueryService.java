package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.ControlType_;
import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.domain.engagement.InternalControl_;
import gov.giamis.repository.engagement.InternalControlRepository;
import gov.giamis.service.setup.dto.InternalControlCriteria;

/**
 * Service for executing complex queries for {@link InternalControl} entities in the database.
 * The main input is a {@link InternalControlCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InternalControl} or a {@link Page} of {@link InternalControl} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InternalControlQueryService extends QueryService<InternalControl> {

    private final Logger log = LoggerFactory.getLogger(InternalControlQueryService.class);

    private final InternalControlRepository internalControlRepository;

    public InternalControlQueryService(InternalControlRepository internalControlRepository) {
        this.internalControlRepository = internalControlRepository;
    }

    /**
     * Return a {@link List} of {@link InternalControl} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InternalControl> findByCriteria(InternalControlCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<InternalControl> specification = createSpecification(criteria);
        return internalControlRepository.findAll(specification);
    }

    public List<InternalControl> findByEngagement(Long engagementId) {
        return internalControlRepository.findByEngagement_Id(engagementId);
    }

//    public List<InternalControl> findByEngagementWithProcedures(Long engagementId) {
//        return internalControlRepository.findAllWithProcedures(engagementId);
//    }

    /**
     * Return a {@link Page} of {@link InternalControl} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InternalControl> findByCriteria(InternalControlCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<InternalControl> specification = createSpecification(criteria);
        return internalControlRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(InternalControlCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<InternalControl> specification = createSpecification(criteria);
        return internalControlRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<InternalControl> createSpecification(InternalControlCriteria criteria) {
        Specification<InternalControl> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), InternalControl_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), InternalControl_.name));
            }
            if (criteria.getStepNumber() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStepNumber(), InternalControl_.stepNumber));
            }
            if (criteria.getCriteria() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCriteria(), InternalControl_.criteria));
            }
            if (criteria.getScore() != null) {
                specification = specification.and(buildSpecification(criteria.getScore(), InternalControl_.score));
            }
            if (criteria.getInternalControlTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getInternalControlTypeId(),
                    root -> root.join(InternalControl_.internalControlType, JoinType.LEFT).get(ControlType_.id)));
            }
            if (criteria.getAreaOfResponsibility() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getAreaOfResponsibility(), InternalControl_.areaOfResponsibility));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(InternalControl_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }


}
