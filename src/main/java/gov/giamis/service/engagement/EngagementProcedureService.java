package gov.giamis.service.engagement;

import gov.giamis.dto.InternalControlDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementProcedure;
import org.springframework.data.jpa.repository.query.Procedure;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementProcedure}.
 */
public interface EngagementProcedureService {

    /**
     * Save a engagementProcedure.
     *
     * @param engagementProcedure the entity to save.
     * @return the persisted entity.
     */
    EngagementProcedure save(EngagementProcedure engagementProcedure);

    /**
     * Get all the engagementProcedures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementProcedure> findAll(Pageable pageable);


    /**
     * Get the "id" engagementProcedure.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementProcedure> findOne(Long id);

    /**
     * Delete the "id" engagementProcedure.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<EngagementProcedure> findBySpecificObjectiveId(Long id);
    
    void update(EngagementProcedure engagementProcedure);

    EngagementProcedure getOrCreate(InternalControlDTO c);
}
