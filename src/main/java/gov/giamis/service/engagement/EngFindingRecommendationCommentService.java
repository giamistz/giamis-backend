package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngFindingRecommendationComment}.
 */
public interface EngFindingRecommendationCommentService {

    /**
     * Save a engFindingReccomendComment.
     *
     * @param engFindingRecommendationComment the entity to save.
     * @return the persisted entity.
     */
    EngFindingRecommendationComment save(EngFindingRecommendationComment engFindingRecommendationComment);

    /**
     * Get all the engFindingReccomendComments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngFindingRecommendationComment> findAll(Pageable pageable);


    /**
     * Get the "id" engFindingReccomendComment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngFindingRecommendationComment> findOne(Long id);

    /**
     * Delete the "id" engFindingReccomendComment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<EngFindingRecommendationComment> findByRecommendationId(Long id);
}
