package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementMeeting_;
import gov.giamis.domain.engagement.Engagement_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.EngagementMeeting;
import gov.giamis.repository.engagement.EngagementMeetingRepository;
import gov.giamis.service.engagement.dto.EngagementMeetingCriteria;

/**
 * Service for executing complex queries for {@link EngagementMeeting} entities in the database.
 * The main input is a {@link EngagementMeetingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementMeeting} or a {@link Page} of {@link EngagementMeeting} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementMeetingQueryService extends QueryService<EngagementMeeting> {

    private final Logger log = LoggerFactory.getLogger(EngagementMeetingQueryService.class);

    private final EngagementMeetingRepository engagementMeetingRepository;

    public EngagementMeetingQueryService(EngagementMeetingRepository engagementMeetingRepository) {
        this.engagementMeetingRepository = engagementMeetingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementMeeting> findByCriteria(EngagementMeetingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementMeeting> specification = createSpecification(criteria);
        return engagementMeetingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementMeeting> findByCriteria(EngagementMeetingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementMeeting> specification = createSpecification(criteria);
        return engagementMeetingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementMeetingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementMeeting> specification = createSpecification(criteria);
        return engagementMeetingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementMeeting> createSpecification(EngagementMeetingCriteria criteria) {
        Specification<EngagementMeeting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementMeeting_.id));
            }
            if (criteria.getMeetingType() != null) {
                specification = specification.and(buildSpecification(criteria.getMeetingType(), EngagementMeeting_.meetingType));
            }
            if (criteria.getMeetingDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMeetingDate(), EngagementMeeting_.meetingDate));
            }
            if (criteria.getVenue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVenue(), EngagementMeeting_.venue));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementMeeting_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }
}
