package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementItem;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementItem}.
 */
public interface EngagementItemService {

    /**
     * Save a engagementItem.
     *
     * @param engagementItem the entity to save.
     * @return the persisted entity.
     */
    EngagementItem save(EngagementItem engagementItem);

    /**
     * Get all the engagementItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementItem> findAll(Pageable pageable);


    /**
     * Get the "id" engagementItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementItem> findOne(Long id);

    /**
     * Delete the "id" engagementItem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<EngagementItem> findByEngagementProcedure(Long id);
    
}
