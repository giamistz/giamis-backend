package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.MatterOfNextAudit;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MatterOfNextAudit}.
 */
public interface MatterOfNextAuditService {

    /**
     * Save a matterOfNextAudit.
     *
     * @param matterOfNextAudit the entity to save.
     * @return the persisted entity.
     */
    MatterOfNextAudit save(MatterOfNextAudit matterOfNextAudit);

    /**
     * Get all the matterOfNextAudits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MatterOfNextAudit> findAll(Pageable pageable);


    /**
     * Get the "id" matterOfNextAudit.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MatterOfNextAudit> findOne(Long id);

    /**
     * Delete the "id" matterOfNextAudit.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
