package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementEntranceMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementEntranceMeeting}.
 */
public interface EngagementEntranceMeetingService {

    /**
     * Save a engagementEntranceMeeting.
     *
     * @param engagementEntranceMeeting the entity to save.
     * @return the persisted entity.
     */
    EngagementEntranceMeeting save(EngagementEntranceMeeting engagementEntranceMeeting);

    /**
     * Get all the engagementEntranceMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementEntranceMeeting> findAll(Pageable pageable);


    /**
     * Get the "id" engagementEntranceMeeting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementEntranceMeeting> findOne(Long id);

    /**
     * Delete the "id" engagementEntranceMeeting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    EngagementEntranceMeeting findByEngagementId(Long id);
    
    Long countEngagementEntranceMeetingByEngagementId(Long id);
}
