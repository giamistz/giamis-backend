package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementFindingComment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementFindingComment}.
 */
public interface EngagementFindingCommentService {

    /**
     * Save a engagementFindingComment.
     *
     * @param engagementFindingComment the entity to save.
     * @return the persisted entity.
     */
    EngagementFindingComment save(EngagementFindingComment engagementFindingComment);

    /**
     * Get all the engagementFindingComments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementFindingComment> findAll(Pageable pageable);


    /**
     * Get the "id" engagementFindingComment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementFindingComment> findOne(Long id);

    /**
     * Delete the "id" engagementFindingComment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
}
