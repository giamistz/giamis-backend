package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementFinding;
import gov.giamis.domain.engagement.EngagementFinding_;
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.domain.engagement.WorkProgram_;
import gov.giamis.repository.engagement.EngagementFindingRepository;
import gov.giamis.service.engagement.dto.EngagementFindingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementFinding} entities in the database.
 * The main input is a {@link EngagementFindingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementFinding} or a {@link Page} of {@link EngagementFinding} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementFindingQueryService extends QueryService<EngagementFinding> {

    private final Logger log = LoggerFactory.getLogger(EngagementFindingQueryService.class);

    private final EngagementFindingRepository engagementFindingRepository;

    public EngagementFindingQueryService(EngagementFindingRepository engagementFindingRepository) {
        this.engagementFindingRepository = engagementFindingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementFinding> findByCriteria(EngagementFindingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementFinding> specification = createSpecification(criteria);
        return engagementFindingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementFinding> findByCriteria(EngagementFindingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementFinding> specification = createSpecification(criteria);
        return engagementFindingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementFindingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementFinding> specification = createSpecification(criteria);
        return engagementFindingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementFinding> createSpecification(EngagementFindingCriteria criteria) {
        Specification<EngagementFinding> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementFinding_.id));
            }
       
            if (criteria.getEngagementProcedureId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementProcedureId(),
                    root -> root.join(EngagementFinding_.engagementProcedure, JoinType.LEFT).get(EngagementProcedure_.id)));
            }
            
            if (criteria.getWorkProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkProgramId(),
                    root -> root.join(EngagementFinding_.workProgram, JoinType.LEFT).get(WorkProgram_.id)));
            }
            
        }
        return specification;
    }
}
