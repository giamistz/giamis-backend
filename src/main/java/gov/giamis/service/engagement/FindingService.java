package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.Finding;

import java.util.Optional;

/**
 * Service Interface for managing {@link Finding}.
 */
public interface FindingService {

    /**
     * Save a finding.
     *
     * @param finding the entity to save.
     * @return the persisted entity.
     */
    Finding save(Finding finding);

    /**
     * Get all the findings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Finding> findAll(Pageable pageable);


    /**
     * Get the "id" finding.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Finding> findOne(Long id);

    /**
     * Delete the "id" finding.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
