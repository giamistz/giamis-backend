package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngFindingRecommendation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngFindingRecommendation}.
 */
public interface EngFindingRecommendationService {

    /**
     * Save a engFindingReccomendation.
     *
     * @param engFindingRecommendation the entity to save.
     * @return the persisted entity.
     */
    EngFindingRecommendation save(EngFindingRecommendation engFindingRecommendation);

    /**
     * Get all the engFindingReccomendations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngFindingRecommendation> findAll(Pageable pageable);


    /**
     * Get the "id" engFindingReccomendation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngFindingRecommendation> findOne(Long id);

    /**
     * Delete the "id" engFindingReccomendation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
