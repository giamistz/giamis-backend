package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementReport_;
import gov.giamis.domain.engagement.Engagement_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.EngagementReport;
import gov.giamis.repository.engagement.EngagementReportRepository;
import gov.giamis.service.engagement.dto.EngagementReportCriteria;

/**
 * Service for executing complex queries for {@link EngagementReport} entities in the database.
 * The main input is a {@link EngagementReportCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementReport} or a {@link Page} of {@link EngagementReport} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementReportQueryService extends QueryService<EngagementReport> {

    private final Logger log = LoggerFactory.getLogger(EngagementReportQueryService.class);

    private final EngagementReportRepository engagementReportRepository;

    public EngagementReportQueryService(EngagementReportRepository engagementReportRepository) {
        this.engagementReportRepository = engagementReportRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementReport> findByCriteria(EngagementReportCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementReport> specification = createSpecification(criteria);
        return engagementReportRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementReport> findByCriteria(EngagementReportCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementReport> specification = createSpecification(criteria);
        return engagementReportRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementReportCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementReport> specification = createSpecification(criteria);
        return engagementReportRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementReport> createSpecification(EngagementReportCriteria criteria) {
        Specification<EngagementReport> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementReport_.id));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), EngagementReport_.status));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementReport_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
        }
        return specification;
    }
}
