package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.domain.engagement.EngagementResource;
import gov.giamis.domain.engagement.EngagementResource_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.FileResource_;
import gov.giamis.repository.engagement.EngagementResourceRepository;
import gov.giamis.service.dto.EngagementResourceCriteria;

/**
 * Service for executing complex queries for {@link EngagementResource} entities in the database.
 * The main input is a {@link EngagementResourceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementResource} or a {@link Page} of {@link EngagementResource} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementResourceQueryService extends QueryService<EngagementResource> {

    private final Logger log = LoggerFactory.getLogger(EngagementResourceQueryService.class);

    private final EngagementResourceRepository engagementResourceRepository;

    public EngagementResourceQueryService(EngagementResourceRepository engagementResourceRepository) {
        this.engagementResourceRepository = engagementResourceRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementResource} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementResource> findByCriteria(EngagementResourceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementResource> specification = createSpecification(criteria);
        return engagementResourceRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementResource} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementResource> findByCriteria(EngagementResourceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementResource> specification = createSpecification(criteria);
        return engagementResourceRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementResourceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementResource> specification = createSpecification(criteria);
        return engagementResourceRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementResource> createSpecification(EngagementResourceCriteria criteria) {
        Specification<EngagementResource> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementResource_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), EngagementResource_.name));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementResource_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            if (criteria.getFileResourceId() != null) {
                specification = specification.and(buildSpecification(criteria.getFileResourceId(),
                    root -> root.join(EngagementResource_.fileResource, JoinType.LEFT).get(FileResource_.id)));
            }
        }
        return specification;
    }
}
