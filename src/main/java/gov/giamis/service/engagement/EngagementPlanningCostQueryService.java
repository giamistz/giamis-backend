package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementPlanningCost;
import gov.giamis.domain.engagement.EngagementPlanningCost_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.GfsCode_;
import gov.giamis.repository.engagement.EngagementPlanningCostRepository;
import gov.giamis.service.engagement.dto.EngagementPlanningCostCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementPlanningCost} entities in the database.
 * The main input is a {@link EngagementPlanningCostCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementPlanningCost} or a {@link Page} of {@link EngagementPlanningCost} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementPlanningCostQueryService extends QueryService<EngagementPlanningCost> {

    private final Logger log = LoggerFactory.getLogger(EngagementPlanningCostQueryService.class);

    private final EngagementPlanningCostRepository engagementPlanningCostRepository;

    public EngagementPlanningCostQueryService(EngagementPlanningCostRepository engagementPlanningCostRepository) {
        this.engagementPlanningCostRepository = engagementPlanningCostRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementPlanningCost} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementPlanningCost> findByCriteria(EngagementPlanningCostCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementPlanningCost> specification = createSpecification(criteria);
        return engagementPlanningCostRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementPlanningCost} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementPlanningCost> findByCriteria(EngagementPlanningCostCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementPlanningCost> specification = createSpecification(criteria);
        return engagementPlanningCostRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementPlanningCostCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementPlanningCost> specification = createSpecification(criteria);
        return engagementPlanningCostRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementPlanningCost> createSpecification(EngagementPlanningCostCriteria criteria) {
        Specification<EngagementPlanningCost> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementPlanningCost_.id));
            }
       
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementPlanningCost_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            
            if (criteria.getGfsCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getGfsCodeId(),
                    root -> root.join(EngagementPlanningCost_.gfsCode, JoinType.LEFT).get(GfsCode_.id)));
            }
            
        }
        return specification;
    }
}
