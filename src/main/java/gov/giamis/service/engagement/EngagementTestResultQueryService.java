package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementItem_;
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.domain.engagement.EngagementTestResult;
import gov.giamis.domain.engagement.EngagementTestResult_;
import gov.giamis.domain.engagement.WorkProgram_;
import gov.giamis.repository.engagement.EngagementTestResultRepository;
import gov.giamis.service.engagement.dto.EngagementTestResultCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementTestResult} entities in the database.
 * The main input is a {@link EngagementTestResultCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementTestResult} or a {@link Page} of {@link EngagementTestResult} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementTestResultQueryService extends QueryService<EngagementTestResult> {

    private final Logger log = LoggerFactory.getLogger(EngagementTestResultQueryService.class);

    private final EngagementTestResultRepository engagementTestResultRepository;

    public EngagementTestResultQueryService(EngagementTestResultRepository engagementTestResultRepository) {
        this.engagementTestResultRepository = engagementTestResultRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementTestResult} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementTestResult> findByCriteria(EngagementTestResultCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementTestResult> specification = createSpecification(criteria);
        return engagementTestResultRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementTestResult} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementTestResult> findByCriteria(EngagementTestResultCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementTestResult> specification = createSpecification(criteria);
        return engagementTestResultRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementTestResultCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementTestResult> specification = createSpecification(criteria);
        return engagementTestResultRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementTestResult> createSpecification(EngagementTestResultCriteria criteria) {
        Specification<EngagementTestResult> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementTestResult_.id));
            }

            if (criteria.getEngagementProcedureId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementProcedureId(),
                    root -> root.join(EngagementTestResult_.engagementProcedure, JoinType.LEFT).get(EngagementProcedure_.id)));
            }
            
            if (criteria.getEngagementItemId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementItemId(),
                    root -> root.join(EngagementTestResult_.engagementItem, JoinType.LEFT).get(EngagementItem_.id)));
            }
            
            if (criteria.getWorkProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkProgramId(),
                    root -> root.join(EngagementTestResult_.workProgram, JoinType.LEFT).get(WorkProgram_.id)));
            }
            
        }
        return specification;
    }
}
