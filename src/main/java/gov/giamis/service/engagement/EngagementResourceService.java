package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.EngagementResource;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementResource}.
 */
public interface EngagementResourceService {

    /**
     * Save a engagementResource.
     *
     * @param engagementResource the entity to save.
     * @return the persisted entity.
     */
    EngagementResource save(EngagementResource engagementResource);

    /**
     * Get all the engagementResources.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementResource> findAll(Pageable pageable);


    /**
     * Get the "id" engagementResource.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementResource> findOne(Long id);

    /**
     * Delete the "id" engagementResource.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
