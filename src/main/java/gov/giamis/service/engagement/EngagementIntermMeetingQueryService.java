package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementIntermMeeting_;
import gov.giamis.domain.engagement.EngagementMember_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.engagement.EngagementExitMeeting_;
import gov.giamis.domain.engagement.EngagementIntermMeeting;
import gov.giamis.repository.engagement.EngagementIntermMeetingRepository;
import gov.giamis.service.engagement.dto.EngagementIntermMeetingCriteria;

/**
 * Service for executing complex queries for {@link EngagementIntermMeeting} entities in the database.
 * The main input is a {@link EngagementIntermMeetingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementIntermMeeting} or a {@link Page} of {@link EngagementIntermMeeting} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementIntermMeetingQueryService extends QueryService<EngagementIntermMeeting> {

    private final Logger log = LoggerFactory.getLogger(EngagementIntermMeetingQueryService.class);

    private final EngagementIntermMeetingRepository engagementIntermMeetingRepository;

    public EngagementIntermMeetingQueryService(EngagementIntermMeetingRepository engagementIntermMeetingRepository) {
        this.engagementIntermMeetingRepository = engagementIntermMeetingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementIntermMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementIntermMeeting> findByCriteria(EngagementIntermMeetingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementIntermMeeting> specification = createSpecification(criteria);
        return engagementIntermMeetingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementIntermMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementIntermMeeting> findByCriteria(EngagementIntermMeetingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementIntermMeeting> specification = createSpecification(criteria);
        return engagementIntermMeetingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementIntermMeetingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementIntermMeeting> specification = createSpecification(criteria);
        return engagementIntermMeetingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementIntermMeeting> createSpecification(EngagementIntermMeetingCriteria criteria) {
        Specification<EngagementIntermMeeting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementIntermMeeting_.id));
            }
            if (criteria.getMeetingDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMeetingDate(), EngagementIntermMeeting_.meetingDate));
            }
            if (criteria.getVenue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVenue(), EngagementIntermMeeting_.venue));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementIntermMeeting_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(EngagementIntermMeeting_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(criteria.getFinancialYearId(),
                    root -> root.join(EngagementIntermMeeting_.financialYear, JoinType.LEFT).get(FinancialYear_.id)));
            }
        }
        return specification;
    }
}
