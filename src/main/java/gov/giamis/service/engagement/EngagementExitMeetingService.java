package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementExitMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementExitMeeting}.
 */
public interface EngagementExitMeetingService {

    /**
     * Save a engagementExitMeeting.
     *
     * @param engagementExitMeeting the entity to save.
     * @return the persisted entity.
     */
    EngagementExitMeeting save(EngagementExitMeeting engagementExitMeeting);

    /**
     * Get all the engagementExitMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementExitMeeting> findAll(Pageable pageable);


    /**
     * Get the "id" engagementExitMeeting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementExitMeeting> findOne(Long id);

    /**
     * Delete the "id" engagementExitMeeting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

   EngagementExitMeeting findByEngagementId(Long id);

    Long countEngagementExitMeetingByEngagementId(Long id);
   
}
