package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.domain.engagement.EngagementWorkProgram;
import gov.giamis.domain.engagement.EngagementWorkProgram_;
import gov.giamis.domain.engagement.SpecificObjective_;
import gov.giamis.repository.engagement.EngagementWorkProgramRepository;
import gov.giamis.service.engagement.dto.EngagementWorkProgramCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementWorkProgram} entities in the database.
 * The main input is a {@link EngagementWorkProgramCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementWorkProgram} or a {@link Page} of {@link EngagementWorkProgram} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementWorkProgramQueryService extends QueryService<EngagementWorkProgram> {

    private final Logger log = LoggerFactory.getLogger(EngagementWorkProgramQueryService.class);

    private final EngagementWorkProgramRepository engagementWorkProgramRepository;

    public EngagementWorkProgramQueryService(EngagementWorkProgramRepository engagementWorkProgramRepository) {
        this.engagementWorkProgramRepository = engagementWorkProgramRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementWorkProgram} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementWorkProgram> findByCriteria(EngagementWorkProgramCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementWorkProgram> specification = createSpecification(criteria);
        return engagementWorkProgramRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementWorkProgram} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementWorkProgram> findByCriteria(EngagementWorkProgramCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementWorkProgram> specification = createSpecification(criteria);
        return engagementWorkProgramRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementWorkProgramCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementWorkProgram> specification = createSpecification(criteria);
        return engagementWorkProgramRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementWorkProgram> createSpecification(EngagementWorkProgramCriteria criteria) {
        Specification<EngagementWorkProgram> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementWorkProgram_.id));
            }
            if (criteria.getDatePlanned() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatePlanned(), EngagementWorkProgram_.datePlanned));
            }
            if (criteria.getDateCompleted()!= null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCompleted(),EngagementWorkProgram_.dateCompleted));
            }
            
            if (criteria.getSpecificObjective() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getSpecificObjective(),
                    root -> root.join(EngagementWorkProgram_.specificObjective, JoinType.LEFT)
                        .get(SpecificObjective_.id)));
            }
            
            if (criteria.getEngagementProcedure() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getEngagementProcedure(),
                    root -> root.join(EngagementWorkProgram_.engagementProcedure, JoinType.LEFT)
                        .get(EngagementProcedure_.id)));
            }
        }
        return specification;
    }
}
