package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementFinding;

import gov.giamis.service.engagement.dto.EngagementFindingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementFinding}.
 */
public interface EngagementFindingService {

    /**
     * Save a engagementFinding.
     *
     * @param engagementFinding the entity to save.
     * @return the persisted entity.
     */
    EngagementFinding save(EngagementFinding engagementFinding);

    /**
     * Get all the engagementFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementFinding> findAll(Pageable pageable);


    /**
     * Get the "id" engagementFinding.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementFinding> findOne(Long id);

    /**
     * Delete the "id" engagementFinding.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<EngagementFinding> findByEngagementProcedureId(Long id);

    void initiateFinding(List<EngagementFinding> engagementFindings);

    List<EngagementFinding> findEngagementFindingByEngagementId(Long id);

    List<EngagementFinding> findByFinancialYear_IdAndOrganisationUnit_Id(Long financialYear, Long organisationUnit);

    Map<String, List<EngagementFindingDTO>> findPreviousGroupByStatus(Long engagementId);

    Long countByEngagementFindingStatus(Long engagementId);

    List<EngagementFinding> findByOrganisationUnitId(Long id);
}
