package gov.giamis.service.engagement;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.engagement.CategoryOfFinding;
import java.util.Optional;

/**
 * Service Interface for managing {@link CategoryOfFinding}.
 */
public interface CategoryOfFindingService {

    /**
     * Save a categoryOfFinding.
     *
     * @param categoryOfFinding the entity to save.
     * @return the persisted entity.
     */
    CategoryOfFinding save(CategoryOfFinding categoryOfFinding);

    /**
     * Get all the categoryOfFindings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CategoryOfFinding> findAll(Pageable pageable);


    /**
     * Get the "id" categoryOfFinding.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CategoryOfFinding> findOne(Long id);

    /**
     * Delete the "id" categoryOfFinding.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    CategoryOfFinding findByName(String name);
    
}
