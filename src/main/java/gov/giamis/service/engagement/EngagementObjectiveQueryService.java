package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementObjective_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.domain.setup.Objective_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.EngagementObjective;
import gov.giamis.repository.engagement.EngagementObjectiveRepository;
import gov.giamis.service.engagement.dto.EngagementObjectiveCriteria;

/**
 * Service for executing complex queries for {@link EngagementObjective} entities in the database.
 * The main input is a {@link EngagementObjectiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementObjective} or a {@link Page} of {@link EngagementObjective} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementObjectiveQueryService extends QueryService<EngagementObjective> {

    private final Logger log = LoggerFactory.getLogger(EngagementObjectiveQueryService.class);

    private final EngagementObjectiveRepository engagementObjectiveRepository;

    public EngagementObjectiveQueryService(EngagementObjectiveRepository engagementObjectiveRepository) {
        this.engagementObjectiveRepository = engagementObjectiveRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementObjective> findByCriteria(EngagementObjectiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementObjective> specification = createSpecification(criteria);
        return engagementObjectiveRepository.findAll(specification);
    }

    @Transactional(readOnly = true)
    public List<EngagementObjective> findAllWithRiskByCriteria(EngagementObjectiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementObjective> specification = createSpecification(criteria);
        return engagementObjectiveRepository.findAllWithRisks(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementObjective> findByCriteria(EngagementObjectiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementObjective> specification = createSpecification(criteria);
        return engagementObjectiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementObjectiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementObjective> specification = createSpecification(criteria);
        return engagementObjectiveRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementObjective> createSpecification(EngagementObjectiveCriteria criteria) {
        Specification<EngagementObjective> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementObjective_.id));
            }
        }
        return specification;
    }
}
