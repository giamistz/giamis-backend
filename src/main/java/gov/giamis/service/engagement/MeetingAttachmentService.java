package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.MeetingAttachment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MeetingAttachment}.
 */
public interface MeetingAttachmentService {

    /**
     * Save a meetingAttachment.
     *
     * @param meetingAttachment the entity to save.
     * @return the persisted entity.
     */
    MeetingAttachment save(MeetingAttachment meetingAttachment);

    /**
     * Get all the meetingAttachments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MeetingAttachment> findAll(Pageable pageable);


    /**
     * Get the "id" meetingAttachment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MeetingAttachment> findOne(Long id);

    /**
     * Delete the "id" meetingAttachment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
