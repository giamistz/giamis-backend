package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.EngagementEntranceMeeting;
import gov.giamis.domain.engagement.EngagementEntranceMeeting_;
import gov.giamis.domain.engagement.Engagement_;
import gov.giamis.repository.engagement.EngagementEntranceMeetingRepository;
import gov.giamis.service.engagement.dto.EngagementEntranceMeetingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link EngagementEntranceMeeting} entities in the database.
 * The main input is a {@link EngagementEntranceMeetingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementEntranceMeeting} or a {@link Page} of {@link EngagementEntranceMeeting} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementEntranceMeetingQueryService extends QueryService<EngagementEntranceMeeting> {

    private final Logger log = LoggerFactory.getLogger(EngagementEntranceMeetingQueryService.class);

    private final EngagementEntranceMeetingRepository engagementEntranceMeetingRepository;

    public EngagementEntranceMeetingQueryService(EngagementEntranceMeetingRepository engagementEntranceMeetingRepository) {
        this.engagementEntranceMeetingRepository = engagementEntranceMeetingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementEntranceMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementEntranceMeeting> findByCriteria(EngagementEntranceMeetingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementEntranceMeeting> specification = createSpecification(criteria);
        return engagementEntranceMeetingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementEntranceMeeting} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementEntranceMeeting> findByCriteria(EngagementEntranceMeetingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementEntranceMeeting> specification = createSpecification(criteria);
        return engagementEntranceMeetingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementEntranceMeetingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementEntranceMeeting> specification = createSpecification(criteria);
        return engagementEntranceMeetingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementEntranceMeeting> createSpecification(EngagementEntranceMeetingCriteria criteria) {
        Specification<EngagementEntranceMeeting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementEntranceMeeting_.id));
            }
            if (criteria.getMeetingDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMeetingDate(), EngagementEntranceMeeting_.meetingDate));
            }
            if (criteria.getVenue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVenue(), EngagementEntranceMeeting_.venue));
            }
            if (criteria.getEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementId(),
                    root -> root.join(EngagementEntranceMeeting_.engagement, JoinType.LEFT).get(Engagement_.id)));
            }
            
        }
        return specification;
    }
}
