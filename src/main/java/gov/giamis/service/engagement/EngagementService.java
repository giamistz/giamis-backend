package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.engagement.Engagement;

import gov.giamis.dto.ClientNotificationDTO;
import gov.giamis.dto.ConfirmGoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Engagement}.
 */
public interface EngagementService {

    /**
     * Save a engagement.
     *
     * @param engagement the entity to save.
     * @return the persisted entity.
     */
    Engagement save(Engagement engagement);

    /**
     * Get all the engagements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Engagement> findAll(Pageable pageable);


    /**
     * Get the "id" engagement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Engagement> findOne(Long id);

    /**
     * Delete the "id" engagement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<Engagement> findByFinancialYearAndOrganisationUnit(Long financialYearId, Long organisationUnitId);

    Engagement confirmGoDecision(ConfirmGoDTO confirmGoDTO);

    CommunicationLetter createEngagementLetter(ClientNotificationDTO notificationDTO);

    void sendNotification(CommunicationLetter letter, String email);
}
