package gov.giamis.service.engagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import gov.giamis.domain.engagement.EngagementTestResult;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementTestResult}.
 */
public interface EngagementTestResultService {

    /**
     * Save a engagementTestResult.
     *
     * @param engagementTestResult the entity to save.
     * @return the persisted entity.
     */
    EngagementTestResult save(EngagementTestResult engagementTestResult);

    /**
     * Get all the engagementTestResults.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementTestResult> findAll(Pageable pageable);


    /**
     * Get the "id" engagementTestResult.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementTestResult> findOne(Long id);

    /**
     * Delete the "id" engagementTestResult.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<EngagementTestResult> findByEngagementProcedure(Long id);
    
    List<EngagementTestResult> findByScoreAndEngagementProcedure(Long id);
   
    void updateEngagementTestResult(Long item,Long workProgram,Long procedure,Long id);
}
