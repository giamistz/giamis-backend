package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.domain.engagement.Finding;
import gov.giamis.domain.engagement.Finding_;
import gov.giamis.repository.engagement.FindingRepository;
import gov.giamis.service.dto.FindingCriteria;

/**
 * Service for executing complex queries for {@link Finding} entities in the database.
 * The main input is a {@link FindingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Finding} or a {@link Page} of {@link Finding} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FindingQueryService extends QueryService<Finding> {

    private final Logger log = LoggerFactory.getLogger(FindingQueryService.class);

    private final FindingRepository findingRepository;

    public FindingQueryService(FindingRepository findingRepository) {
        this.findingRepository = findingRepository;
    }

    /**
     * Return a {@link List} of {@link Finding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Finding> findByCriteria(FindingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Finding> specification = createSpecification(criteria);
        return findingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Finding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Finding> findByCriteria(FindingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Finding> specification = createSpecification(criteria);
        return findingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FindingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Finding> specification = createSpecification(criteria);
        return findingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Finding> createSpecification(FindingCriteria criteria) {
        Specification<Finding> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Finding_.id));
            }
            if (criteria.getCondition() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCondition(), Finding_.condition));
            }
            if (criteria.getNumberOfError() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfError(), Finding_.numberOfError));
            }
            if (criteria.getTotalValueAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalValueAmount(), Finding_.totalValueAmount));
            }
            if (criteria.getImpactOnAssurance() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImpactOnAssurance(), Finding_.impactOnAssurance));
            }
            if (criteria.getActionPlanCategory() != null) {
                specification = specification.and(buildSpecification(criteria.getActionPlanCategory(), Finding_.actionPlanCategory));
            }
            if (criteria.getProcedureId() != null) {
                specification = specification.and(buildSpecification(criteria.getProcedureId(),
                    root -> root.join(Finding_.procedure, JoinType.LEFT).get(EngagementProcedure_.id)));
            }
        }
        return specification;
    }
}
