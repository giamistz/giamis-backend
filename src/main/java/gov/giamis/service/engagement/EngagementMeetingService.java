package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link EngagementMeeting}.
 */
public interface EngagementMeetingService {

    /**
     * Save a engagementMeeting.
     *
     * @param engagementMeeting the entity to save.
     * @return the persisted entity.
     */
    EngagementMeeting save(EngagementMeeting engagementMeeting);

    /**
     * Get all the engagementMeetings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EngagementMeeting> findAll(Pageable pageable);


    /**
     * Get the "id" engagementMeeting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EngagementMeeting> findOne(Long id);

    /**
     * Delete the "id" engagementMeeting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
   
}
