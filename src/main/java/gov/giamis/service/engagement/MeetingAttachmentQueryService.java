package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngagementMeeting_;
import gov.giamis.domain.engagement.MeetingAttachment_;
import gov.giamis.domain.setup.FileResource_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.engagement.MeetingAttachment;
import gov.giamis.repository.engagement.MeetingAttachmentRepository;
import gov.giamis.service.engagement.dto.MeetingAttachmentCriteria;

/**
 * Service for executing complex queries for {@link MeetingAttachment} entities in the database.
 * The main input is a {@link MeetingAttachmentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MeetingAttachment} or a {@link Page} of {@link MeetingAttachment} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MeetingAttachmentQueryService extends QueryService<MeetingAttachment> {

    private final Logger log = LoggerFactory.getLogger(MeetingAttachmentQueryService.class);

    private final MeetingAttachmentRepository meetingAttachmentRepository;

    public MeetingAttachmentQueryService(MeetingAttachmentRepository meetingAttachmentRepository) {
        this.meetingAttachmentRepository = meetingAttachmentRepository;
    }

    /**
     * Return a {@link List} of {@link MeetingAttachment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MeetingAttachment> findByCriteria(MeetingAttachmentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MeetingAttachment> specification = createSpecification(criteria);
        return meetingAttachmentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MeetingAttachment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MeetingAttachment> findByCriteria(MeetingAttachmentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MeetingAttachment> specification = createSpecification(criteria);
        return meetingAttachmentRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MeetingAttachmentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MeetingAttachment> specification = createSpecification(criteria);
        return meetingAttachmentRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<MeetingAttachment> createSpecification(MeetingAttachmentCriteria criteria) {
        Specification<MeetingAttachment> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MeetingAttachment_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), MeetingAttachment_.name));
            }
            if (criteria.getEngagementMeetingId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngagementMeetingId(),
                    root -> root.join(MeetingAttachment_.engagementMeeting, JoinType.LEFT).get(EngagementMeeting_.id)));
            }
            if (criteria.getFileResourceId() != null) {
                specification = specification.and(buildSpecification(criteria.getFileResourceId(),
                    root -> root.join(MeetingAttachment_.fileResource, JoinType.LEFT).get(FileResource_.id)));
            }
        }
        return specification;
    }
}
