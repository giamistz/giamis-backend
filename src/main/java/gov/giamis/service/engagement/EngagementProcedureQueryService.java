package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.domain.engagement.EngagementProcedure_;
import gov.giamis.domain.engagement.InternalControl_;
import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.domain.risk.RiskControlMatrix_;
import gov.giamis.domain.risk.RiskControlReport_;
import gov.giamis.repository.engagement.EngagementProcedureRepository;
import gov.giamis.service.dto.EngagementProcedureCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link EngagementProcedure} entities in the database.
 * The main input is a {@link EngagementProcedureCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementProcedure} or a {@link Page} of {@link EngagementProcedure} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementProcedureQueryService extends QueryService<EngagementProcedure> {

    private final Logger log = LoggerFactory.getLogger(EngagementProcedureQueryService.class);

    private final EngagementProcedureRepository engagementProcedureRepository;

    public EngagementProcedureQueryService(EngagementProcedureRepository engagementProcedureRepository) {
        this.engagementProcedureRepository = engagementProcedureRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementProcedure} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementProcedure> findByCriteria(EngagementProcedureCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementProcedure> specification = createSpecification(criteria);
        return engagementProcedureRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementProcedure} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementProcedure> findByCriteria(EngagementProcedureCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementProcedure> specification = createSpecification(criteria);
        return engagementProcedureRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementProcedureCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementProcedure> specification = createSpecification(criteria);
        return engagementProcedureRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<EngagementProcedure> createSpecification(EngagementProcedureCriteria criteria) {
        Specification<EngagementProcedure> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EngagementProcedure_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), EngagementProcedure_.name));
            }
        
            if (criteria.getRiskControlMatrixId() != null) {
                specification = specification.and(buildSpecification(criteria.getRiskControlMatrixId(),
                    root -> root.join(EngagementProcedure_.riskControlMatrix, JoinType.LEFT).get(RiskControlMatrix_.id)));
            }
        }
        return specification;
    }
}
