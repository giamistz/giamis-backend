package gov.giamis.service.engagement;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import gov.giamis.domain.engagement.EngFindingRecommendationComment_;
import gov.giamis.domain.engagement.EngFindingRecommendation_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.engagement.EngFindingReccomendCommentRepository;
import gov.giamis.service.dto.EngFindingReccomendCommentCriteria;

/**
 * Service for executing complex queries for {@link EngFindingRecommendationComment} entities in the database.
 * The main input is a {@link EngFindingReccomendCommentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngFindingRecommendationComment} or a {@link Page} of {@link EngFindingRecommendationComment} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngFindingRecommendationCommentQueryService extends QueryService<EngFindingRecommendationComment> {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationCommentQueryService.class);

    private final EngFindingReccomendCommentRepository engFindingReccomendCommentRepository;

    public EngFindingRecommendationCommentQueryService(EngFindingReccomendCommentRepository engFindingReccomendCommentRepository) {
        this.engFindingReccomendCommentRepository = engFindingReccomendCommentRepository;
    }

    /**
     * Return a {@link List} of {@link EngFindingRecommendationComment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngFindingRecommendationComment> findByCriteria(EngFindingReccomendCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngFindingRecommendationComment> specification = createSpecification(criteria);
        return engFindingReccomendCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngFindingRecommendationComment} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngFindingRecommendationComment> findByCriteria(EngFindingReccomendCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngFindingRecommendationComment> specification = createSpecification(criteria);
        return engFindingReccomendCommentRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngFindingReccomendCommentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngFindingRecommendationComment> specification = createSpecification(criteria);
        return engFindingReccomendCommentRepository.count(specification);
    }

    /**
     * Function to convert {@link EngFindingReccomendCommentCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EngFindingRecommendationComment> createSpecification(EngFindingReccomendCommentCriteria criteria) {
        Specification<EngFindingRecommendationComment> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), EngFindingRecommendationComment_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), EngFindingRecommendationComment_.description));
            }
            if (criteria.getUser() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUser(), EngFindingRecommendationComment_.name));
            }
            if (criteria.getEngFindingReccomendationId() != null) {
                specification = specification.and(buildSpecification(criteria.getEngFindingReccomendationId(),
                    root -> root.join(EngFindingRecommendationComment_.engFindingRecommendation, JoinType.LEFT).get(EngFindingRecommendation_.id)));
            }
        }
        return specification;
    }
}
