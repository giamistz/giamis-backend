
package gov.giamis.service.engagement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.engagement.SubCategoryOfFinding;
import gov.giamis.domain.engagement.SubCategoryOfFinding_;
import gov.giamis.repository.engagement.SubCategoryOfFindingRepository;
import gov.giamis.service.engagement.dto.SubCategoryOfFindingCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link SubCategoryOfFinding} entities in the database.
 * The main input is a {@link SubCategoryOfFindingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SubCategoryOfFinding} or a {@link Page} of {@link SubCategoryOfFinding} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SubCategoryOfFindingQueryService extends QueryService<SubCategoryOfFinding> {

    private final Logger log = LoggerFactory.getLogger(SubCategoryOfFindingQueryService.class);

    private final SubCategoryOfFindingRepository subCategoryOfFindingRepository;

    public SubCategoryOfFindingQueryService(SubCategoryOfFindingRepository subCategoryOfFindingRepository) {
        this.subCategoryOfFindingRepository = subCategoryOfFindingRepository;
    }

    /**
     * Return a {@link List} of {@link SubCategoryOfFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SubCategoryOfFinding> findByCriteria(SubCategoryOfFindingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SubCategoryOfFinding> specification = createSpecification(criteria);
        return subCategoryOfFindingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link SubCategoryOfFinding} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SubCategoryOfFinding> findByCriteria(SubCategoryOfFindingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SubCategoryOfFinding> specification = createSpecification(criteria);
        return subCategoryOfFindingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SubCategoryOfFindingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SubCategoryOfFinding> specification = createSpecification(criteria);
        return subCategoryOfFindingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<SubCategoryOfFinding> createSpecification(SubCategoryOfFindingCriteria criteria) {
        Specification<SubCategoryOfFinding> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SubCategoryOfFinding_.id));
            }
        }
        return specification;
    }
}
