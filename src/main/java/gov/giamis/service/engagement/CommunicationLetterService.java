package gov.giamis.service.engagement;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.enumeration.LetterType;
import gov.giamis.repository.engagement.CommunicationLetterRepository;
import gov.giamis.repository.engagement.EngagementMemberRepository;
import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.service.report.EngagementTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link CommunicationLetter}.
 */
@Service
@Transactional
public class CommunicationLetterService {

    private final Logger log = LoggerFactory.getLogger(CommunicationLetterService.class);

    private final CommunicationLetterRepository communicationLetterRepository;

    private final EngagementTemplateService templateService;

    private final EngagementRepository engagementRepository;

    private final EngagementMemberRepository memberRepository;

    public CommunicationLetterService(CommunicationLetterRepository communicationLetterRepository,
                                      EngagementTemplateService templateService,
                                      EngagementRepository engagementRepository,
                                      EngagementMemberRepository memberRepository) {
        this.communicationLetterRepository = communicationLetterRepository;
        this.templateService = templateService;
        this.engagementRepository = engagementRepository;
        this.memberRepository = memberRepository;
    }

    /**
     * Save a communicationLetter.
     *
     * @param communicationLetter the entity to save.
     * @return the persisted entity.
     */
    public CommunicationLetter save(CommunicationLetter communicationLetter) {
        log.debug("Request to save CommunicationLetter : {}", communicationLetter);
        return communicationLetterRepository.save(communicationLetter);
    }

    /**
     * Get all the communicationLetters.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CommunicationLetter> findAll() {
        log.debug("Request to get all CommunicationLetters");
        return communicationLetterRepository.findAll();
    }


    /**
     * Get one communicationLetter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommunicationLetter> findOne(Long id) {
        log.debug("Request to get CommunicationLetter : {}", id);
        return communicationLetterRepository.findById(id);
    }

    /**
     * Delete the communicationLetter by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CommunicationLetter : {}", id);
        communicationLetterRepository.deleteById(id);
    }

    public CommunicationLetter findByTypeAndEngagement(LetterType type, Long engagementId) {
        Optional<CommunicationLetter> letter = communicationLetterRepository
            .findByTypeAndEngagement_Id(type, engagementId);
        if (letter.isPresent()) {
            return letter.get();
        } else {
            String body;
            String subject;
            CommunicationLetter newLetter = null;
            Engagement e = engagementRepository.getOne(engagementId);
            switch (type) {
                case ENGAGEMENT_LETTER:
                    List<EngagementMember> m = memberRepository.findByEngagement_Id(engagementId);
                    body = templateService.getEngagementLetterBody(e, m);
                    subject = "ENGAGEMENT LETTER";
                    newLetter = communicationLetterRepository.save(new CommunicationLetter(type, subject, body, e));
                    break;
                case TRANSMITTAL_LETTER:
                    body= templateService.getTransmittalLetterBody(e);
                    subject = "TRANSMITTAL LETTER";
                    newLetter = communicationLetterRepository.save(new CommunicationLetter(type, subject, body, e));
                    break;
            }
            return newLetter;
        }
    }
}
