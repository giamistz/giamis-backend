package gov.giamis.service;

import gov.giamis.domain.QuarterReport;
import gov.giamis.repository.QuarterReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link QuarterReport}.
 */
@Service
@Transactional
public class QuarterReportService {

    private final Logger log = LoggerFactory.getLogger(QuarterReportService.class);

    private final QuarterReportRepository quarterReportRepository;

    public QuarterReportService(QuarterReportRepository quarterReportRepository) {
        this.quarterReportRepository = quarterReportRepository;
    }

    /**
     * Save a quarterReport.
     *
     * @param quarterReport the entity to save.
     * @return the persisted entity.
     */
    public QuarterReport save(QuarterReport quarterReport) {
        log.debug("Request to save QuarterReport : {}", quarterReport);
        return quarterReportRepository.save(quarterReport);
    }

    public QuarterReport getByQtr(Long quarterId) {
        log.debug("Request to save QuarterReport : {}", quarterId);
        return quarterReportRepository.findFirstByQuarter_Id(quarterId);
    }

    /**
     * Get all the quarterReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<QuarterReport> findAll(Pageable pageable) {
        log.debug("Request to get all QuarterReports");
        return quarterReportRepository.findAll(pageable);
    }


    /**
     * Get one quarterReport by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<QuarterReport> findOne(Long id) {
        log.debug("Request to get QuarterReport : {}", id);
        return quarterReportRepository.findById(id);
    }

    /**
     * Delete the quarterReport by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete QuarterReport : {}", id);
        quarterReportRepository.deleteById(id);
    }
}
