package gov.giamis.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.domain.setup.MenuItem;
import gov.giamis.domain.setup.Role;
import gov.giamis.dto.MenuDTO;
import gov.giamis.repository.setup.AuthorityRepository;
import gov.giamis.repository.setup.MenuGroupRepository;
import gov.giamis.repository.setup.MenuItemRepository;
import gov.giamis.repository.setup.RoleRepository;
import gov.giamis.security.NoAthorization;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

@Service
public class AuthoritySyncService {

    private final RequestMappingInfoHandlerMapping requestMappingHandlerMapping;

    private final AuthorityRepository authorityRepository;

    private final RoleRepository roleRepository;

    private final MenuGroupRepository menuGroupRepository;

    private final MenuItemRepository menuItemRepository;

    private long order = 0;

    public AuthoritySyncService(RequestMappingInfoHandlerMapping requestMappingHandlerMapping,
                                AuthorityRepository authorityRepository,
                                RoleRepository roleRepository,
                                MenuGroupRepository menuGroupRepository,
                                MenuItemRepository menuItemRepository) {

        this.requestMappingHandlerMapping = requestMappingHandlerMapping;
        this.authorityRepository = authorityRepository;
        this.roleRepository = roleRepository;
        this.menuGroupRepository = menuGroupRepository;
        this.menuItemRepository = menuItemRepository;
    }

    public void syncAuthorities() {

        requestMappingHandlerMapping.getHandlerMethods().forEach(new BiConsumer<RequestMappingInfo, HandlerMethod>() {
            @Override
            public void accept(RequestMappingInfo requestMappingInfo, HandlerMethod handlerMethod) {


                NoAthorization noAthorization = handlerMethod.getMethodAnnotation(NoAthorization.class);

                if(requestMappingInfo.getMethodsCondition().getMethods().size() != 0 &&
                    requestMappingInfo.getPatternsCondition().getPatterns().size() == 1 &&
                    Iterables.get(requestMappingInfo.getPatternsCondition().getPatterns(),0).contains("api") &&
                    noAthorization == null
                ) {
                    String resourceName = handlerMethod.getBean().toString().replace("Resource","");
                    String actionName = null;
                    Set<RequestMethod> method = requestMappingInfo.getMethodsCondition().getMethods();
                    if (method.contains(RequestMethod.GET)) {
                        actionName = "Read";
                    } else if (
                        method.contains(RequestMethod.PUT) ||
                        method.contains(RequestMethod.POST) ||
                        method.contains(RequestMethod.PATCH)) {
                        actionName = "Write";
                    } else if (method.contains(RequestMethod.DELETE)) {
                        actionName = "Delete";
                    }
                    if (actionName != null) {
                        String authorityName = actionName.toUpperCase().concat("_").concat(resourceName.toUpperCase());

                        if(!authorityRepository.findByName(authorityName).isPresent()) {
                            Authority authority = new Authority();
                            authority.setName(authorityName);
                            authority.setResource(StringUtils.capitalize(resourceName.replaceAll("\\d+", "").replaceAll("(.)([A-Z])", "$1 $2")));
                            authority.setAction(actionName.toUpperCase());
                            authority.setDisplayName(actionName);
                            authorityRepository.save(authority);
                        }
                    }
                }

            }
        });
    }

    public void syncRoleAuthorities(Long id) {
        Role role = roleRepository.findRoleById(id);

        if(role != null && !authorityRepository.findAuthorityOrderByName().isEmpty()) {
            role.setAuthorities(authorityRepository.findAllAuthorities());
            roleRepository.save(role);
        }
    }

    @Transactional
    public void syncMenus() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<MenuDTO> menuList = objectMapper.readValue(getClass().getResourceAsStream("/routes.json"),
            new TypeReference<List<MenuDTO>>(){});

        menuList.forEach(m -> {
            this.order = this.order + 1;
            MenuGroup menuGroup = saveItem(m, null);
            if (m.getChildren() !=null && m.getChildren().size() > 0) {
                saveChildren(m.getChildren(), menuGroup);
            }
        });
    }

    private void saveChildren(List<MenuDTO> menus, MenuGroup menuGroup) {
        menus.forEach(m -> {
            this.order = this.order + 1;
            MenuGroup menuGroup1 =saveItem(m, menuGroup);
            if (m.getChildren() !=null && m.getChildren().size() > 0) {
                saveChildren(m.getChildren(), menuGroup1);
            }
        });
    }

    private MenuGroup saveItem(MenuDTO m, MenuGroup parentMenuGroup) {
        MenuGroup menuGroup2 = null;
        if (m.getPath().equals("")) {
            MenuGroup existing = menuGroupRepository.findByNameIgnoreCase(m.getName());
            if (existing == null) {
                menuGroup2 = menuGroupRepository.save(new MenuGroup(m.getName(), m.getIcon(), parentMenuGroup, this.order));
            } else {
                existing.setSortOrder(this.order);
                menuGroup2 = menuGroupRepository.save(existing);
            }
        } else {
            MenuItem existingItem = menuItemRepository.findByNameIgnoreCase(m.getName());
            if (existingItem == null) {
                menuItemRepository.save(new MenuItem(m.getName(), m.getPath(), m.getIcon(), parentMenuGroup, this.order));
                menuGroup2 = parentMenuGroup;
            } else {
                existingItem.setSortOrder(this.order);
                menuItemRepository.save(existingItem);
            }
        }
        return menuGroup2;
    }

}
