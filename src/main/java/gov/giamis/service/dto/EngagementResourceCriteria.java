package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.engagement.EngagementResource} entity. This class is used
 * in {@link gov.giamis.web.rest.engagement.EngagementResourceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-resources?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementResourceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter engagementId;

    private LongFilter fileResourceId;

    public EngagementResourceCriteria(){
    }

    public EngagementResourceCriteria(EngagementResourceCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.fileResourceId = other.fileResourceId == null ? null : other.fileResourceId.copy();
    }

    @Override
    public EngagementResourceCriteria copy() {
        return new EngagementResourceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    public LongFilter getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(LongFilter fileResourceId) {
        this.fileResourceId = fileResourceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementResourceCriteria that = (EngagementResourceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(engagementId, that.engagementId) &&
            Objects.equals(fileResourceId, that.fileResourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        engagementId,
        fileResourceId
        );
    }

    @Override
    public String toString() {
        return "EngagementResourceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
                (fileResourceId != null ? "fileResourceId=" + fileResourceId + ", " : "") +
            "}";
    }

}
