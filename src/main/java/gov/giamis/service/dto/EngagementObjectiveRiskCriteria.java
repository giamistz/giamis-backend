package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.engagement.EngagementObjectiveRisk} entity. This class is used
 * in {@link gov.giamis.web.rest.engagement.EngagementObjectiveRiskResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-objective-risks?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementObjectiveRiskCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter impact;

    private IntegerFilter likelihood;

    private LongFilter engagementObjectiveId;

    private LongFilter riskId;

    private LongFilter engagementId;

    public EngagementObjectiveRiskCriteria(){
    }

    public EngagementObjectiveRiskCriteria(EngagementObjectiveRiskCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.impact = other.impact == null ? null : other.impact.copy();
        this.likelihood = other.likelihood == null ? null : other.likelihood.copy();
        this.engagementObjectiveId = other.engagementObjectiveId == null ? null : other.engagementObjectiveId.copy();
        this.riskId = other.riskId == null ? null : other.riskId.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
    }

    @Override
    public EngagementObjectiveRiskCriteria copy() {
        return new EngagementObjectiveRiskCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getImpact() {
        return impact;
    }

    public void setImpact(IntegerFilter impact) {
        this.impact = impact;
    }

    public IntegerFilter getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(IntegerFilter likelihood) {
        this.likelihood = likelihood;
    }

    public LongFilter getEngagementObjectiveId() {
        return engagementObjectiveId;
    }

    public void setEngagementObjectiveId(LongFilter engagementObjectiveId) {
        this.engagementObjectiveId = engagementObjectiveId;
    }

    public LongFilter getRiskId() {
        return riskId;
    }

    public void setRiskId(LongFilter riskId) {
        this.riskId = riskId;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementObjectiveRiskCriteria that = (EngagementObjectiveRiskCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(impact, that.impact) &&
            Objects.equals(likelihood, that.likelihood) &&
            Objects.equals(engagementObjectiveId, that.engagementObjectiveId) &&
            Objects.equals(riskId, that.riskId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        impact,
        likelihood,
        engagementObjectiveId,
        riskId
        );
    }

    @Override
    public String toString() {
        return "EngagementObjectiveRiskCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (impact != null ? "impact=" + impact + ", " : "") +
                (likelihood != null ? "likelihood=" + likelihood + ", " : "") +
                (engagementObjectiveId != null ? "engagementObjectiveId=" + engagementObjectiveId + ", " : "") +
                (riskId != null ? "riskId=" + riskId + ", " : "") +
            "}";
    }

}
