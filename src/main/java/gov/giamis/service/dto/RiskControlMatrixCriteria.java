package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.risk.RiskControlMatrix} entity. This class is used
 * in {@link gov.giamis.web.rest.risk.RiskControlMatrixResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /risk-control-matrices?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RiskControlMatrixCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter isKeyControl;

    private LongFilter engagementObjectiveRiskId;

    private LongFilter controlStepId;

    public RiskControlMatrixCriteria(){
    }

    public RiskControlMatrixCriteria(RiskControlMatrixCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.isKeyControl = other.isKeyControl == null ? null : other.isKeyControl.copy();
        this.engagementObjectiveRiskId = other.engagementObjectiveRiskId == null ? null : other.engagementObjectiveRiskId.copy();
        this.controlStepId = other.controlStepId == null ? null : other.controlStepId.copy();
    }

    @Override
    public RiskControlMatrixCriteria copy() {
        return new RiskControlMatrixCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getIsKeyControl() {
        return isKeyControl;
    }

    public void setIsKeyControl(BooleanFilter isKeyControl) {
        this.isKeyControl = isKeyControl;
    }

    public LongFilter getEngagementObjectiveRiskId() {
        return engagementObjectiveRiskId;
    }

    public void setEngagementObjectiveRiskId(LongFilter engagementObjectiveRiskId) {
        this.engagementObjectiveRiskId = engagementObjectiveRiskId;
    }

    public LongFilter getControlStepId() {
        return controlStepId;
    }

    public void setControlStepId(LongFilter controlStepId) {
        this.controlStepId = controlStepId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RiskControlMatrixCriteria that = (RiskControlMatrixCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(isKeyControl, that.isKeyControl) &&
            Objects.equals(engagementObjectiveRiskId, that.engagementObjectiveRiskId) &&
            Objects.equals(controlStepId, that.controlStepId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        isKeyControl,
        engagementObjectiveRiskId,
        controlStepId
        );
    }

    @Override
    public String toString() {
        return "RiskControlMatrixCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (isKeyControl != null ? "isKeyControl=" + isKeyControl + ", " : "") +
                (engagementObjectiveRiskId != null ? "engagementObjectiveRiskId=" + engagementObjectiveRiskId + ", " : "") +
                (controlStepId != null ? "controlStepId=" + controlStepId + ", " : "") +
            "}";
    }

}
