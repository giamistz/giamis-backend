package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.engagement.EngagementProcedure} entity. This class is used
 * in {@link gov.giamis.web.rest.engagement.EngagementProcedureResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-procedures?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementProcedureCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LocalDateFilter datePlaned;

    private LocalDateFilter dateCompleted;

    private BooleanFilter approved;

    private StringFilter population;

    private StringFilter sample;

    private LongFilter riskControlMatrixId;

    public EngagementProcedureCriteria(){
    }

    public EngagementProcedureCriteria(EngagementProcedureCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.datePlaned = other.datePlaned == null ? null : other.datePlaned.copy();
        this.dateCompleted = other.dateCompleted == null ? null : other.dateCompleted.copy();
        this.approved = other.approved == null ? null : other.approved.copy();
        this.population = other.population == null ? null : other.population.copy();
        this.sample = other.sample == null ? null : other.sample.copy();
        this.riskControlMatrixId = other.riskControlMatrixId == null ? null : other.riskControlMatrixId.copy();
    }

    @Override
    public EngagementProcedureCriteria copy() {
        return new EngagementProcedureCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getDatePlaned() {
        return datePlaned;
    }

    public void setDatePlaned(LocalDateFilter datePlaned) {
        this.datePlaned = datePlaned;
    }

    public LocalDateFilter getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDateFilter dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public BooleanFilter getApproved() {
        return approved;
    }

    public void setApproved(BooleanFilter approved) {
        this.approved = approved;
    }

    public StringFilter getPopulation() {
        return population;
    }

    public void setPopulation(StringFilter population) {
        this.population = population;
    }

    public StringFilter getSample() {
        return sample;
    }

    public void setSample(StringFilter sample) {
        this.sample = sample;
    }

    public LongFilter getRiskControlMatrixId() {
		return riskControlMatrixId;
	}

	public void setRiskControlMatrixId(LongFilter riskControlMatrixId) {
		this.riskControlMatrixId = riskControlMatrixId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementProcedureCriteria that = (EngagementProcedureCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(datePlaned, that.datePlaned) &&
            Objects.equals(dateCompleted, that.dateCompleted) &&
            Objects.equals(approved, that.approved) &&
            Objects.equals(population, that.population) &&
            Objects.equals(sample, that.sample) &&
            Objects.equals(riskControlMatrixId, that.riskControlMatrixId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        datePlaned,
        dateCompleted,
        approved,
        population,
        sample,
            riskControlMatrixId
        );
    }

	@Override
	public String toString() {
		return "EngagementProcedureCriteria [id=" + id + ", name=" + name + ", datePlaned=" + datePlaned
				+ ", dateCompleted=" + dateCompleted + ", approved=" + approved + ", population=" + population
				+ ", sample=" + sample + ", riskControlMatrixId=" + riskControlMatrixId + "]";
	}
}
