package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.web.rest.engagement.CommunicationLetterResource;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.LetterType;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link CommunicationLetter} entity. This class is used
 * in {@link CommunicationLetterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /communication-letters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CommunicationLetterCriteria implements Serializable, Criteria {
    /**
     * Class for filtering LetterType
     */
    public static class LetterTypeFilter extends Filter<LetterType> {

        public LetterTypeFilter() {
        }

        public LetterTypeFilter(LetterTypeFilter filter) {
            super(filter);
        }

        @Override
        public LetterTypeFilter copy() {
            return new LetterTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter subject;

    private LetterTypeFilter type;

    private LongFilter engagementId;

    private LongFilter fileResourceId;

    public CommunicationLetterCriteria(){
    }

    public CommunicationLetterCriteria(CommunicationLetterCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.subject = other.subject == null ? null : other.subject.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
        this.fileResourceId = other.fileResourceId == null ? null : other.fileResourceId.copy();
    }

    @Override
    public CommunicationLetterCriteria copy() {
        return new CommunicationLetterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSubject() {
        return subject;
    }

    public void setSubject(StringFilter subject) {
        this.subject = subject;
    }

    public LetterTypeFilter getType() {
        return type;
    }

    public void setType(LetterTypeFilter type) {
        this.type = type;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }

    public LongFilter getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(LongFilter fileResourceId) {
        this.fileResourceId = fileResourceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommunicationLetterCriteria that = (CommunicationLetterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(subject, that.subject) &&
            Objects.equals(type, that.type) &&
            Objects.equals(engagementId, that.engagementId) &&
            Objects.equals(fileResourceId, that.fileResourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        subject,
        type,
        engagementId,
        fileResourceId
        );
    }

    @Override
    public String toString() {
        return "CommunicationLetterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (subject != null ? "subject=" + subject + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
                (fileResourceId != null ? "fileResourceId=" + fileResourceId + ", " : "") +
            "}";
    }

}
