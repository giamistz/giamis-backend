package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import gov.giamis.domain.enumeration.ActionPlanCategory;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.engagement.Finding} entity. This class is used
 * in {@link gov.giamis.web.rest.engagement.FindingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /findings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FindingCriteria implements Serializable, Criteria {
    /**
     * Class for filtering ActionPlanCategory
     */
    public static class ActionPlanCategoryFilter extends Filter<ActionPlanCategory> {

        public ActionPlanCategoryFilter() {
        }

        public ActionPlanCategoryFilter(ActionPlanCategoryFilter filter) {
            super(filter);
        }

        @Override
        public ActionPlanCategoryFilter copy() {
            return new ActionPlanCategoryFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter condition;

    private IntegerFilter numberOfError;

    private DoubleFilter totalValueAmount;

    private StringFilter impactOnAssurance;

    private ActionPlanCategoryFilter actionPlanCategory;

    private LongFilter procedureId;

    public FindingCriteria(){
    }

    public FindingCriteria(FindingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.condition = other.condition == null ? null : other.condition.copy();
        this.numberOfError = other.numberOfError == null ? null : other.numberOfError.copy();
        this.totalValueAmount = other.totalValueAmount == null ? null : other.totalValueAmount.copy();
        this.impactOnAssurance = other.impactOnAssurance == null ? null : other.impactOnAssurance.copy();
        this.actionPlanCategory = other.actionPlanCategory == null ? null : other.actionPlanCategory.copy();
        this.procedureId = other.procedureId == null ? null : other.procedureId.copy();
    }

    @Override
    public FindingCriteria copy() {
        return new FindingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCondition() {
        return condition;
    }

    public void setCondition(StringFilter condition) {
        this.condition = condition;
    }

    public IntegerFilter getNumberOfError() {
        return numberOfError;
    }

    public void setNumberOfError(IntegerFilter numberOfError) {
        this.numberOfError = numberOfError;
    }

    public DoubleFilter getTotalValueAmount() {
        return totalValueAmount;
    }

    public void setTotalValueAmount(DoubleFilter totalValueAmount) {
        this.totalValueAmount = totalValueAmount;
    }

    public StringFilter getImpactOnAssurance() {
        return impactOnAssurance;
    }

    public void setImpactOnAssurance(StringFilter impactOnAssurance) {
        this.impactOnAssurance = impactOnAssurance;
    }

    public ActionPlanCategoryFilter getActionPlanCategory() {
        return actionPlanCategory;
    }

    public void setActionPlanCategory(ActionPlanCategoryFilter actionPlanCategory) {
        this.actionPlanCategory = actionPlanCategory;
    }

    public LongFilter getProcedureId() {
        return procedureId;
    }

    public void setProcedureId(LongFilter procedureId) {
        this.procedureId = procedureId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FindingCriteria that = (FindingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(condition, that.condition) &&
            Objects.equals(numberOfError, that.numberOfError) &&
            Objects.equals(totalValueAmount, that.totalValueAmount) &&
            Objects.equals(impactOnAssurance, that.impactOnAssurance) &&
            Objects.equals(actionPlanCategory, that.actionPlanCategory) &&
            Objects.equals(procedureId, that.procedureId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        condition,
        numberOfError,
        totalValueAmount,
        impactOnAssurance,
        actionPlanCategory,
        procedureId
        );
    }

    @Override
    public String toString() {
        return "FindingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (condition != null ? "condition=" + condition + ", " : "") +
                (numberOfError != null ? "numberOfError=" + numberOfError + ", " : "") +
                (totalValueAmount != null ? "totalValueAmount=" + totalValueAmount + ", " : "") +
                (impactOnAssurance != null ? "impactOnAssurance=" + impactOnAssurance + ", " : "") +
                (actionPlanCategory != null ? "actionPlanCategory=" + actionPlanCategory + ", " : "") +
                (procedureId != null ? "procedureId=" + procedureId + ", " : "") +
            "}";
    }

}
