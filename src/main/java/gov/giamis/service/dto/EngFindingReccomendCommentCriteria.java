package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import gov.giamis.web.rest.engagement.EngFindingRecommendationCommentResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link EngFindingRecommendationComment} entity. This class is used
 * in {@link EngFindingRecommendationCommentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /eng-finding-reccomend-comments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngFindingReccomendCommentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private StringFilter user;

    private LongFilter engFindingReccomendationId;

    public EngFindingReccomendCommentCriteria(){
    }

    public EngFindingReccomendCommentCriteria(EngFindingReccomendCommentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.user = other.user == null ? null : other.user.copy();
        this.engFindingReccomendationId = other.engFindingReccomendationId == null ? null : other.engFindingReccomendationId.copy();
    }

    @Override
    public EngFindingReccomendCommentCriteria copy() {
        return new EngFindingReccomendCommentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getUser() {
        return user;
    }

    public void setUser(StringFilter user) {
        this.user = user;
    }

    public LongFilter getEngFindingReccomendationId() {
        return engFindingReccomendationId;
    }

    public void setEngFindingReccomendationId(LongFilter engFindingReccomendationId) {
        this.engFindingReccomendationId = engFindingReccomendationId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngFindingReccomendCommentCriteria that = (EngFindingReccomendCommentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(user, that.user) &&
            Objects.equals(engFindingReccomendationId, that.engFindingReccomendationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        description,
        user,
        engFindingReccomendationId
        );
    }

    @Override
    public String toString() {
        return "EngFindingReccomendCommentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (user != null ? "user=" + user + ", " : "") +
                (engFindingReccomendationId != null ? "engFindingReccomendationId=" + engFindingReccomendationId + ", " : "") +
            "}";
    }

}
