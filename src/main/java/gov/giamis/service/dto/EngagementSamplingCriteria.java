package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.EngagementSampling} entity. This class is used
 * in {@link gov.giamis.web.rest.EngagementSamplingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /engagement-samplings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EngagementSamplingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter populationSize;

    private IntegerFilter sampleSize;

    private StringFilter remarks;

    private BooleanFilter samplingType;

    public EngagementSamplingCriteria(){
    }

    public EngagementSamplingCriteria(EngagementSamplingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.populationSize = other.populationSize == null ? null : other.populationSize.copy();
        this.sampleSize = other.sampleSize == null ? null : other.sampleSize.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.samplingType = other.samplingType == null ? null : other.samplingType.copy();
    }

    @Override
    public EngagementSamplingCriteria copy() {
        return new EngagementSamplingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(IntegerFilter populationSize) {
        this.populationSize = populationSize;
    }

    public IntegerFilter getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(IntegerFilter sampleSize) {
        this.sampleSize = sampleSize;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public BooleanFilter getSamplingType() {
        return samplingType;
    }

    public void setSamplingType(BooleanFilter samplingType) {
        this.samplingType = samplingType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EngagementSamplingCriteria that = (EngagementSamplingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(populationSize, that.populationSize) &&
            Objects.equals(sampleSize, that.sampleSize) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(samplingType, that.samplingType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        populationSize,
        sampleSize,
        remarks,
        samplingType
        );
    }

    @Override
    public String toString() {
        return "EngagementSamplingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (populationSize != null ? "populationSize=" + populationSize + ", " : "") +
                (sampleSize != null ? "sampleSize=" + sampleSize + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (samplingType != null ? "samplingType=" + samplingType + ", " : "") +
            "}";
    }

}
