package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.QuarterConsolidationReport} entity. This class is used
 * in {@link gov.giamis.web.rest.QuarterConsolidationReportResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quarter-consolidation-reports?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuarterConsolidationReportCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter organisationId;

    private LongFilter quarterId;

    public QuarterConsolidationReportCriteria(){
    }

    public QuarterConsolidationReportCriteria(QuarterConsolidationReportCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.organisationId = other.organisationId == null ? null : other.organisationId.copy();
        this.quarterId = other.quarterId == null ? null : other.quarterId.copy();
    }

    @Override
    public QuarterConsolidationReportCriteria copy() {
        return new QuarterConsolidationReportCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(LongFilter organisationId) {
        this.organisationId = organisationId;
    }

    public LongFilter getQuarterId() {
        return quarterId;
    }

    public void setQuarterId(LongFilter quarterId) {
        this.quarterId = quarterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuarterConsolidationReportCriteria that = (QuarterConsolidationReportCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(organisationId, that.organisationId) &&
            Objects.equals(quarterId, that.quarterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        organisationId,
        quarterId
        );
    }

    @Override
    public String toString() {
        return "QuarterConsolidationReportCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (organisationId != null ? "organisationId=" + organisationId + ", " : "") +
                (quarterId != null ? "quarterId=" + quarterId + ", " : "") +
            "}";
    }

}
