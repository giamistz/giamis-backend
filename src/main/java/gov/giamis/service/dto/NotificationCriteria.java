package gov.giamis.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.Notification} entity. This class is used
 * in {@link gov.giamis.web.rest.NotificationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notifications?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NotificationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter email;

    private StringFilter subject;

    private BooleanFilter isSent;

    private BooleanFilter isRead;

    private LongFilter fileResourceId;

    public NotificationCriteria(){
    }

    public NotificationCriteria(NotificationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.subject = other.subject == null ? null : other.subject.copy();
        this.isSent = other.isSent == null ? null : other.isSent.copy();
        this.isRead = other.isRead == null ? null : other.isRead.copy();
        this.fileResourceId = other.fileResourceId == null ? null : other.fileResourceId.copy();
    }

    @Override
    public NotificationCriteria copy() {
        return new NotificationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getSubject() {
        return subject;
    }

    public void setSubject(StringFilter subject) {
        this.subject = subject;
    }

    public BooleanFilter getIsSent() {
        return isSent;
    }

    public void setIsSent(BooleanFilter isSent) {
        this.isSent = isSent;
    }

    public BooleanFilter getIsRead() {
        return isRead;
    }

    public void setIsRead(BooleanFilter isRead) {
        this.isRead = isRead;
    }

    public LongFilter getFileResourceId() {
        return fileResourceId;
    }

    public void setFileResourceId(LongFilter fileResourceId) {
        this.fileResourceId = fileResourceId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotificationCriteria that = (NotificationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(email, that.email) &&
            Objects.equals(subject, that.subject) &&
            Objects.equals(isSent, that.isSent) &&
            Objects.equals(isRead, that.isRead) &&
            Objects.equals(fileResourceId, that.fileResourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        email,
        subject,
        isSent,
        isRead,
        fileResourceId
        );
    }

    @Override
    public String toString() {
        return "NotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (subject != null ? "subject=" + subject + ", " : "") +
                (isSent != null ? "isSent=" + isSent + ", " : "") +
                (isRead != null ? "isRead=" + isRead + ", " : "") +
                (fileResourceId != null ? "fileResourceId=" + fileResourceId + ", " : "") +
            "}";
    }

}
