package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link AuditPlanActivity}.
 */
public interface AuditPlanActivityService {

    /**
     * Save a auditPlanActivity.
     *
     * @param auditPlanActivity the entity to save.
     * @return the persisted entity.
     */
    AuditPlanActivity save(AuditPlanActivity auditPlanActivity);

    /**
     * Get all the auditPlanSchedules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditPlanActivity> findAll(Pageable pageable);


    /**
     * Get the "id" auditPlanSchedule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditPlanActivity> findOne(Long id);

    /**
     * Delete the "id" auditPlanSchedule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    AuditPlanActivity findAuditPlanActivityByAuditPlanId(Long id);
   
}
