package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditPlanActivityQuarter}.
 */
public interface AuditPlanActivityQuarterService {

    /**
     * Save a auditPlanActivityQuarter.
     *
     * @param auditPlanActivityQuarter the entity to save.
     * @return the persisted entity.
     */
    AuditPlanActivityQuarter save(AuditPlanActivityQuarter auditPlanActivityQuarter);

    /**
     * Get all the auditPlanScheduleQuarters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditPlanActivityQuarter> findAll(Pageable pageable);


    /**
     * Get the "id" auditPlanScheduleQuarter.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditPlanActivityQuarter> findOne(Long id);

    /**
     * Delete the "id" auditPlanScheduleQuarter.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    List<AuditPlanActivityQuarter> findAuditPlanActivityQuarterByAuditPlanActivityId(Long id);
}
