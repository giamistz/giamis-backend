package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.AuditPlan;

import gov.giamis.dto.ApproveDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link AuditPlan}.
 */
public interface AuditPlanService {

    /**
     * Save a auditPlan.
     *
     * @param auditPlan the entity to save.
     * @return the persisted entity.
     */
    AuditPlan save(AuditPlan auditPlan);

    /**
     * Get all the auditPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditPlan> findAll(Pageable pageable);


    /**
     * Get the "id" auditPlan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditPlan> findOne(Long id);

    /**
     * Delete the "id" auditPlan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void updateAuditPlanById(Long id);

    AuditPlan findAuditPlanById(Long id);

    AuditPlan findByOrganisationUnit(Long id);

    void approve(ApproveDTO approveDTO);
}
