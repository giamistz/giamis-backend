package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;
import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay_;
import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanScheduleDayCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.StrategicAuditPlanScheduleDayRepository;

/**
 * Service for executing complex queries for {@link StrategicAuditPlanScheduleDay} entities in the database.
 * The main input is a {@link StrategicAuditPlanScheduleDayCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StrategicAuditPlanScheduleDay} or a {@link Page} of {@link StrategicAuditPlanScheduleDay} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StrategicAuditPlanScheduleDayQueryService extends QueryService<StrategicAuditPlanScheduleDay> {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleDayQueryService.class);

    private final StrategicAuditPlanScheduleDayRepository strategicAuditPlanScheduleDayRepository;

    public StrategicAuditPlanScheduleDayQueryService(StrategicAuditPlanScheduleDayRepository strategicAuditPlanScheduleDayRepository) {
        this.strategicAuditPlanScheduleDayRepository = strategicAuditPlanScheduleDayRepository;
    }

    /**
     * Return a {@link List} of {@link StrategicAuditPlanScheduleDay} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StrategicAuditPlanScheduleDay> findByCriteria(StrategicAuditPlanScheduleDayCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<StrategicAuditPlanScheduleDay> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleDayRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link StrategicAuditPlanScheduleDay} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlanScheduleDay> findByCriteria(StrategicAuditPlanScheduleDayCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<StrategicAuditPlanScheduleDay> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleDayRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StrategicAuditPlanScheduleDayCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<StrategicAuditPlanScheduleDay> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleDayRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<StrategicAuditPlanScheduleDay> createSpecification(StrategicAuditPlanScheduleDayCriteria criteria) {
        Specification<StrategicAuditPlanScheduleDay> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), StrategicAuditPlanScheduleDay_.id));
            }
            if (criteria.getTotalDays() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getTotalDays(), StrategicAuditPlanScheduleDay_.totalDays));
            }
            if (criteria.getStrategicPlanScheduleId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getStrategicPlanScheduleId(),
                    root -> root.join(StrategicAuditPlanScheduleDay_.strategicAuditPlanSchedule, JoinType.LEFT)
                        .get(StrategicAuditPlanSchedule_.id)));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getFinancialYearId(),
                    root -> root.join(StrategicAuditPlanScheduleDay_.financialYear, JoinType.LEFT)
                        .get(FinancialYear_.id)));
            }
        }
        return specification;
    }
}
