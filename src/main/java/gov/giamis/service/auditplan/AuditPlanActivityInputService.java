package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivityInput;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditPlanActivityInput}.
 */
public interface AuditPlanActivityInputService {

    /**
     * Save a auditPlanActivityInput.
     *
     * @param auditPlanActivityInput the entity to save.
     * @return the persisted entity.
     */
    AuditPlanActivityInput save(AuditPlanActivityInput auditPlanActivityInput);

    /**
     * Get all the auditPlanScheduleCosts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditPlanActivityInput> findAll(Pageable pageable);


    /**
     * Get the "id" auditPlanScheduleCost.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditPlanActivityInput> findOne(Long id);

    /**
     * Delete the "id" auditPlanScheduleCost.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<AuditPlanActivityInput> findAuditPlanActivityInputByAuditPlanActivityId(Long id);
}
