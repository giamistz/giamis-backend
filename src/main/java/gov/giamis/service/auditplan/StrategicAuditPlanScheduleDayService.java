package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link StrategicAuditPlanScheduleDay}.
 */
public interface StrategicAuditPlanScheduleDayService {

    /**
     * Save a strategicAuditPlanScheduleDay.
     *
     * @param strategicAuditPlanScheduleDay the entity to save.
     * @return the persisted entity.
     */
    StrategicAuditPlanScheduleDay save(StrategicAuditPlanScheduleDay strategicAuditPlanScheduleDay);

    /**
     * Get all the strategicPlanScheduleDays.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StrategicAuditPlanScheduleDay> findAll(Pageable pageable);


    /**
     * Get the "id" strategicPlanScheduleDay.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StrategicAuditPlanScheduleDay> findOne(Long id);

    /**
     * Delete the "id" strategicPlanScheduleDay.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
