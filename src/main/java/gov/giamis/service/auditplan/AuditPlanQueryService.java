package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.AuditPlan_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.auditplan.AuditPlan;
import gov.giamis.repository.auditplan.AuditPlanRepository;
import gov.giamis.service.auditplan.dto.AuditPlanCriteria;

/**
 * Service for executing complex queries for {@link AuditPlan} entities in the database.
 * The main input is a {@link AuditPlanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditPlan} or a {@link Page} of {@link AuditPlan} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditPlanQueryService extends QueryService<AuditPlan> {

    private final Logger log = LoggerFactory.getLogger(AuditPlanQueryService.class);

    private final AuditPlanRepository auditPlanRepository;

    public AuditPlanQueryService(AuditPlanRepository auditPlanRepository) {
        this.auditPlanRepository = auditPlanRepository;
    }

    /**
     * Return a {@link List} of {@link AuditPlan} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditPlan> findByCriteria(AuditPlanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditPlan> specification = createSpecification(criteria);
        return auditPlanRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditPlan} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditPlan> findByCriteria(AuditPlanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditPlan> specification = createSpecification(criteria);
        return auditPlanRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditPlanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditPlan> specification = createSpecification(criteria);
        return auditPlanRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditPlan> createSpecification(AuditPlanCriteria criteria) {
        Specification<AuditPlan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditPlan_.id));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitId(),
                    root -> root.join(AuditPlan_.organisationUnit, JoinType.LEFT)
                        .get(OrganisationUnit_.id)));
            }
            if (criteria.getFinancialYearId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getFinancialYearId(),
                    root -> root.join(AuditPlan_.financialYear, JoinType.LEFT)
                        .get(FinancialYear_.id)));
            }
        }
        return specification;
    }
}
