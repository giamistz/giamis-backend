package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.AuditPlanActivityInput;
import gov.giamis.web.rest.auditplan.AuditPlanActivityInputResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link AuditPlanActivityInput} entity. This class is used
 * in {@link AuditPlanActivityInputResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-plan-schedule-costs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditPlanActivityInputCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter auditPlanActivityId;

    public AuditPlanActivityInputCriteria(){
    }

    public AuditPlanActivityInputCriteria(AuditPlanActivityInputCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.auditPlanActivityId = other.auditPlanActivityId == null ? null : other.auditPlanActivityId.copy();
    }

    @Override
    public AuditPlanActivityInputCriteria copy() {
        return new AuditPlanActivityInputCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAuditPlanActivityId() {
        return auditPlanActivityId;
    }

    public void setAuditPlanActivityId(LongFilter auditPlanActivityId) {
        this.auditPlanActivityId = auditPlanActivityId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditPlanActivityInputCriteria that = (AuditPlanActivityInputCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(auditPlanActivityId, that.auditPlanActivityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
            auditPlanActivityId
        );
    }

    @Override
    public String toString() {
        return "AuditPlanActivityInputCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (auditPlanActivityId != null ? "auditPlanActivityId=" + auditPlanActivityId + ", " : "") +
            "}";
    }

}
