package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.AuditPlan;
import gov.giamis.web.rest.auditplan.AuditPlanResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditPlan} entity. This class is used
 * in {@link AuditPlanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-plans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditPlanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter organisationUnitId;

    private LongFilter financialYearId;

    public AuditPlanCriteria(){
    }

    public AuditPlanCriteria(AuditPlanCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
    }

    @Override
    public AuditPlanCriteria copy() {
        return new AuditPlanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(LongFilter financialYearId) {
        this.financialYearId = financialYearId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditPlanCriteria that = (AuditPlanCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(organisationUnitId, that.organisationUnitId) &&
            Objects.equals(financialYearId, that.financialYearId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        organisationUnitId,
        financialYearId
        );
    }

    @Override
    public String toString() {
        return "AuditPlanCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
                (financialYearId != null ? "financialYearId=" + financialYearId + ", " : "") +
            "}";
    }

}
