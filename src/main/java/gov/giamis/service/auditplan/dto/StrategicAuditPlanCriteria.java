package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.StrategicAuditPlan;
import gov.giamis.web.rest.auditplan.StrategicAuditPlanResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link StrategicAuditPlan} entity. This class is used
 * in {@link StrategicAuditPlanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /strategic-plans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StrategicAuditPlanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter organisationUnitId;

    private LongFilter startFinancialYearId;

    private LongFilter endFinancialYearId;

    public StrategicAuditPlanCriteria(){
    }

    public StrategicAuditPlanCriteria(StrategicAuditPlanCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.startFinancialYearId = other.startFinancialYearId == null ? null : other.startFinancialYearId.copy();
        this.endFinancialYearId = other.endFinancialYearId == null ? null : other.endFinancialYearId.copy();
    }

    @Override
    public StrategicAuditPlanCriteria copy() {
        return new StrategicAuditPlanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getStartFinancialYearId() {
        return startFinancialYearId;
    }

    public void setStartFinancialYearId(LongFilter startFinancialYearId) {
        this.startFinancialYearId = startFinancialYearId;
    }

    public LongFilter getEndFinancialYearId() {
        return endFinancialYearId;
    }

    public void setEndFinancialYearId(LongFilter endFinancialYearId) {
        this.endFinancialYearId = endFinancialYearId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StrategicAuditPlanCriteria that = (StrategicAuditPlanCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(organisationUnitId, that.organisationUnitId) &&
            Objects.equals(startFinancialYearId, that.startFinancialYearId) &&
            Objects.equals(endFinancialYearId, that.endFinancialYearId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        organisationUnitId,
        startFinancialYearId,
        endFinancialYearId
        );
    }

    @Override
    public String toString() {
        return "StrategicAuditPlanCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
                (startFinancialYearId != null ? "startFinancialYearId=" + startFinancialYearId + ", " : "") +
                (endFinancialYearId != null ? "endFinancialYearId=" + endFinancialYearId + ", " : "") +
            "}";
    }

}
