package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;
import gov.giamis.web.rest.auditplan.StrategicAuditPlanScheduleDayResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link StrategicAuditPlanScheduleDay} entity. This class is used
 * in {@link StrategicAuditPlanScheduleDayResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /strategic-plan-schedule-days?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StrategicAuditPlanScheduleDayCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter totalDays;

    private LongFilter strategicPlanScheduleId;

    private LongFilter financialYearId;

    public StrategicAuditPlanScheduleDayCriteria(){
    }

    public StrategicAuditPlanScheduleDayCriteria(StrategicAuditPlanScheduleDayCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.totalDays = other.totalDays == null ? null : other.totalDays.copy();
        this.strategicPlanScheduleId = other.strategicPlanScheduleId == null ? null : other.strategicPlanScheduleId.copy();
        this.financialYearId = other.financialYearId == null ? null : other.financialYearId.copy();
    }

    @Override
    public StrategicAuditPlanScheduleDayCriteria copy() {
        return new StrategicAuditPlanScheduleDayCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(IntegerFilter totalDays) {
        this.totalDays = totalDays;
    }

    public LongFilter getStrategicPlanScheduleId() {
        return strategicPlanScheduleId;
    }

    public void setStrategicPlanScheduleId(LongFilter strategicPlanScheduleId) {
        this.strategicPlanScheduleId = strategicPlanScheduleId;
    }

    public LongFilter getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(LongFilter financialYearId) {
        this.financialYearId = financialYearId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StrategicAuditPlanScheduleDayCriteria that = (StrategicAuditPlanScheduleDayCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(totalDays, that.totalDays) &&
            Objects.equals(strategicPlanScheduleId, that.strategicPlanScheduleId) &&
            Objects.equals(financialYearId, that.financialYearId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        totalDays,
        strategicPlanScheduleId,
        financialYearId
        );
    }

    @Override
    public String toString() {
        return "StrategicAuditPlanScheduleDayCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (totalDays != null ? "totalDays=" + totalDays + ", " : "") +
                (strategicPlanScheduleId != null ? "strategicPlanScheduleId=" + strategicPlanScheduleId + ", " : "") +
                (financialYearId != null ? "financialYearId=" + financialYearId + ", " : "") +
            "}";
    }

}
