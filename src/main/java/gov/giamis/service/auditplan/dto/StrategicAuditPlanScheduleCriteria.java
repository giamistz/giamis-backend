package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;
import gov.giamis.web.rest.auditplan.StrategicAuditPlanScheduleResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link StrategicAuditPlanSchedule} entity. This class is used
 * in {@link StrategicAuditPlanScheduleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /strategic-plan-schedules?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StrategicAuditPlanScheduleCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter strategicPlanId;

    private LongFilter organisationUnitId;

    private LongFilter auditableAreaId;

    private LongFilter riskId;

    public StrategicAuditPlanScheduleCriteria(){
    }

    public StrategicAuditPlanScheduleCriteria(StrategicAuditPlanScheduleCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.strategicPlanId = other.strategicPlanId == null ? null : other.strategicPlanId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
        this.auditableAreaId = other.auditableAreaId == null ? null : other.auditableAreaId.copy();
        this.riskId = other.riskId == null ? null : other.riskId.copy();
    }

    @Override
    public StrategicAuditPlanScheduleCriteria copy() {
        return new StrategicAuditPlanScheduleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getStrategicPlanId() {
        return strategicPlanId;
    }

    public void setStrategicPlanId(LongFilter strategicPlanId) {
        this.strategicPlanId = strategicPlanId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }

    public LongFilter getAuditableAreaId() {
        return auditableAreaId;
    }

    public void setAuditableAreaId(LongFilter auditableAreaId) {
        this.auditableAreaId = auditableAreaId;
    }

    public LongFilter getRiskId() {
        return riskId;
    }

    public void setRiskId(LongFilter riskId) {
        this.riskId = riskId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StrategicAuditPlanScheduleCriteria that = (StrategicAuditPlanScheduleCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(strategicPlanId, that.strategicPlanId) &&
            Objects.equals(organisationUnitId, that.organisationUnitId) &&
            Objects.equals(auditableAreaId, that.auditableAreaId) &&
            Objects.equals(riskId, that.riskId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        strategicPlanId,
        organisationUnitId,
        auditableAreaId,
        riskId
        );
    }

    @Override
    public String toString() {
        return "StrategicAuditPlanScheduleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (strategicPlanId != null ? "strategicPlanId=" + strategicPlanId + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
                (auditableAreaId != null ? "auditableAreaId=" + auditableAreaId + ", " : "") +
                (riskId != null ? "riskId=" + riskId + ", " : "") +
            "}";
    }

}
