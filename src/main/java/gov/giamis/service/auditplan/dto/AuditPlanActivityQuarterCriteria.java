package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;
import gov.giamis.web.rest.auditplan.AuditPlanActivityQuarterResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link AuditPlanActivityQuarter} entity. This class is used
 * in {@link AuditPlanActivityQuarterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-plan-schedule-quarters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditPlanActivityQuarterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter auditPlanScheduleId;

    private LongFilter quarterId;

    public AuditPlanActivityQuarterCriteria(){
    }

    public AuditPlanActivityQuarterCriteria(AuditPlanActivityQuarterCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.auditPlanScheduleId = other.auditPlanScheduleId == null ? null : other.auditPlanScheduleId.copy();
        this.quarterId = other.quarterId == null ? null : other.quarterId.copy();
    }

    @Override
    public AuditPlanActivityQuarterCriteria copy() {
        return new AuditPlanActivityQuarterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAuditPlanScheduleId() {
        return auditPlanScheduleId;
    }

    public void setAuditPlanScheduleId(LongFilter auditPlanScheduleId) {
        this.auditPlanScheduleId = auditPlanScheduleId;
    }

    public LongFilter getQuarterId() {
        return quarterId;
    }

    public void setQuarterId(LongFilter quarterId) {
        this.quarterId = quarterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditPlanActivityQuarterCriteria that = (AuditPlanActivityQuarterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(auditPlanScheduleId, that.auditPlanScheduleId) &&
            Objects.equals(quarterId, that.quarterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        auditPlanScheduleId,
        quarterId
        );
    }

    @Override
    public String toString() {
        return "AuditPlanActivityQuarterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (auditPlanScheduleId != null ? "auditPlanScheduleId=" + auditPlanScheduleId + ", " : "") +
                (quarterId != null ? "quarterId=" + quarterId + ", " : "") +
            "}";
    }

}
