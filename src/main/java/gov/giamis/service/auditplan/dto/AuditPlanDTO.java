package gov.giamis.service.auditplan.dto;

public class AuditPlanDTO {

	private boolean hasName = false;
	private boolean hasPurpose = false;
	private boolean hasObjective = false;
	private boolean hasMethodology = false;
	private boolean hasListofAbbreviation = false;
	private boolean hasOtherResources = false;
	private boolean hasAuditPlanActivity = false;
	private boolean hasAuditPlanActivityInput = false;
	private boolean hasAuditPlanActivityQuarter = false;

	public AuditPlanDTO() {

	}

	public AuditPlanDTO(boolean hasName, boolean hasPurpose, boolean hasObjective, boolean hasMethodology,
			boolean hasListofAbbreviation, boolean hasOtherResources, boolean hasAuditPlanActivity,
			boolean hasAuditPlanActivityInput, boolean hasAuditPlanActivityQuarter) {
		super();
		this.hasName = hasName;
		this.hasPurpose = hasPurpose;
		this.hasObjective = hasObjective;
		this.hasMethodology = hasMethodology;
		this.hasListofAbbreviation = hasListofAbbreviation;
		this.hasOtherResources = hasOtherResources;
		this.hasAuditPlanActivity = hasAuditPlanActivity;
		this.hasAuditPlanActivityInput = hasAuditPlanActivityInput;
		this.hasAuditPlanActivityQuarter = hasAuditPlanActivityQuarter;
	}

	public boolean isHasName() {
		return hasName;
	}

	public boolean isHasPurpose() {
		return hasPurpose;
	}

	public boolean isHasObjective() {
		return hasObjective;
	}

	public boolean isHasMethodology() {
		return hasMethodology;
	}

	public boolean isHasListofAbbreviation() {
		return hasListofAbbreviation;
	}

	public boolean isHasOtherResources() {
		return hasOtherResources;
	}

	public boolean isHasAuditPlanActivity() {
		return hasAuditPlanActivity;
	}

	public boolean isHasAuditPlanActivityInput() {
		return hasAuditPlanActivityInput;
	}

	public boolean isHasAuditPlanActivityQuarter() {
		return hasAuditPlanActivityQuarter;
	}

	public boolean hasName() {
		return hasName;
	}

	public void setHasName(boolean hasName) {
		this.hasName = hasName;
	}

	public boolean hasPurpose() {
		return hasPurpose;
	}

	public void setHasPurpose(boolean hasPurpose) {
		this.hasPurpose = hasPurpose;
	}

	public boolean hasObjective() {
		return hasObjective;
	}

	public void setHasObjective(boolean hasObjective) {
		this.hasObjective = hasObjective;
	}

	public boolean hasMethodology() {
		return hasMethodology;
	}

	public void setHasMethodology(boolean hasMethodology) {
		this.hasMethodology = hasMethodology;
	}

	public boolean hasListofAbbreviation() {
		return hasListofAbbreviation;
	}

	public void setHasListofAbbreviation(boolean hasListofAbbreviation) {
		this.hasListofAbbreviation = hasListofAbbreviation;
	}

	public boolean hasOtherResources() {
		return hasOtherResources;
	}

	public void setHasOtherResources(boolean hasOtherResources) {
		this.hasOtherResources = hasOtherResources;
	}

	public boolean hasAuditPlanActivity() {
		return hasAuditPlanActivity;
	}

	public void setHasAuditPlanActivity(boolean hasAuditPlanActivity) {
		this.hasAuditPlanActivity = hasAuditPlanActivity;
	}

	public boolean hasAuditPlanActivityInput() {
		return hasAuditPlanActivityInput;
	}

	public void setHasAuditPlanActivityInput(boolean hasAuditPlanActivityInput) {
		this.hasAuditPlanActivityInput = hasAuditPlanActivityInput;
	}

	public boolean hasAuditPlanActivityQuarter() {
		return hasAuditPlanActivityQuarter;
	}

	public void setHasAuditPlanActivityQuarter(boolean hasAuditPlanActivityQuarter) {
		this.hasAuditPlanActivityQuarter = hasAuditPlanActivityQuarter;
	}

}
