package gov.giamis.service.auditplan.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.web.rest.auditplan.AuditPlanActivityResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link AuditPlanActivity} entity. This class is used
 * in {@link AuditPlanActivityResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-plan-schedules?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditPlanActivityCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter auditPlanActivityQuarterId;
    
    private LongFilter auditPlanActivityInputId;

    private LongFilter auditPlanId;

    private LongFilter riskId;

    private LongFilter auditableAreaId;

    private LongFilter organisationUnitId;

    public AuditPlanActivityCriteria(){
    }

    public AuditPlanActivityCriteria(AuditPlanActivityCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.auditPlanActivityQuarterId = other.auditPlanActivityQuarterId == null ? null : other.auditPlanActivityQuarterId.copy();
        this.auditPlanActivityInputId = other.auditPlanActivityInputId == null ? null : other.auditPlanActivityInputId.copy();
        this.auditPlanId = other.auditPlanId == null ? null : other.auditPlanId.copy();
        this.riskId = other.riskId == null ? null : other.riskId.copy();
        this.auditableAreaId = other.auditableAreaId == null ? null : other.auditableAreaId.copy();
        this.organisationUnitId = other.organisationUnitId == null ? null : other.organisationUnitId.copy();
    }

    @Override
    public AuditPlanActivityCriteria copy() {
        return new AuditPlanActivityCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAuditPlanActivityQuarterId() {
        return auditPlanActivityQuarterId;
    }

    public void setAuditPlanActivityQuarterId(LongFilter auditPlanActivityQuarterId) {
        this.auditPlanActivityQuarterId = auditPlanActivityQuarterId;
    }
    
    public LongFilter getAuditPlanActivityInputId() {
        return auditPlanActivityInputId;
    }

    public void setAuditPlanActivityInputId(LongFilter auditPlanActivityInputId) {
        this.auditPlanActivityInputId = auditPlanActivityInputId;
    }

    public LongFilter getAuditPlanId() {
        return auditPlanId;
    }

    public void setAuditPlanId(LongFilter auditPlanId) {
        this.auditPlanId = auditPlanId;
    }

    public LongFilter getRiskId() {
        return riskId;
    }

    public void setRiskId(LongFilter riskId) {
        this.riskId = riskId;
    }

    public LongFilter getAuditableAreaId() {
        return auditableAreaId;
    }

    public void setAuditableAreaId(LongFilter auditableAreaId) {
        this.auditableAreaId = auditableAreaId;
    }

    public LongFilter getOrganisationUnitId() {
        return organisationUnitId;
    }

    public void setOrganisationUnitId(LongFilter organisationUnitId) {
        this.organisationUnitId = organisationUnitId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditPlanActivityCriteria that = (AuditPlanActivityCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(auditPlanActivityQuarterId, that.auditPlanActivityQuarterId) &&
            Objects.equals(auditPlanActivityInputId, that.auditPlanActivityInputId) &&
            Objects.equals(auditPlanId, that.auditPlanId) &&
            Objects.equals(riskId, that.riskId) &&
            Objects.equals(auditableAreaId, that.auditableAreaId) &&
            Objects.equals(organisationUnitId, that.organisationUnitId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        auditPlanActivityQuarterId,
        auditPlanActivityInputId,
        auditPlanId,
        riskId,
        auditableAreaId,
        organisationUnitId
        );
    }

    @Override
    public String toString() {
        return "AuditPlanActivityCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (auditPlanActivityQuarterId != null ? "auditPlanActivityQuarterId=" + auditPlanActivityQuarterId + ", " : "") +
                (auditPlanActivityInputId != null ? "auditPlanActivityInputId=" + auditPlanActivityInputId + ", " : "") +
                (auditPlanId != null ? "auditPlanId=" + auditPlanId + ", " : "") +
                (riskId != null ? "riskId=" + riskId + ", " : "") +
                (auditableAreaId != null ? "auditableAreaId=" + auditableAreaId + ", " : "") +
                (organisationUnitId != null ? "organisationUnitId=" + organisationUnitId + ", " : "") +
            "}";
    }

}
