package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlan;

import gov.giamis.dto.ApproveDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link StrategicAuditPlan}.
 */
public interface StrategicAuditPlanService {

    /**
     * Save a strategicAuditPlan.
     *
     * @param strategicAuditPlan the entity to save.
     * @return the persisted entity.
     */
    StrategicAuditPlan save(StrategicAuditPlan strategicAuditPlan);

    /**
     * Get all the strategicPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StrategicAuditPlan> findAll(Pageable pageable);


    /**
     * Get the "id" strategicPlan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StrategicAuditPlan> findOne(Long id);

    /**
     * Delete the "id" strategicPlan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    StrategicAuditPlan findByName(String name);

    void approve(ApproveDTO approveDTO);

}
