package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.domain.auditplan.AuditPlanActivityInput_;
import gov.giamis.domain.auditplan.AuditPlanActivityQuarter_;
import gov.giamis.domain.auditplan.AuditPlanActivity_;
import gov.giamis.domain.auditplan.AuditPlan_;
import gov.giamis.domain.risk.Risk_;
import gov.giamis.domain.setup.AuditableArea_;
import gov.giamis.domain.setup.OrganisationUnit_;
import gov.giamis.service.auditplan.dto.AuditPlanActivityCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.AuditPlanActivityRepository;

/**
 * Service for executing complex queries for {@link AuditPlanActivity} entities in the database.
 * The main input is a {@link AuditPlanActivityCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditPlanActivity} or a {@link Page} of {@link AuditPlanActivity} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditPlanActivityQueryService extends QueryService<AuditPlanActivity> {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityQueryService.class);

    private final AuditPlanActivityRepository auditPlanActivityRepository;

    public AuditPlanActivityQueryService(AuditPlanActivityRepository auditPlanActivityRepository) {
        this.auditPlanActivityRepository = auditPlanActivityRepository;
    }

    /**
     * Return a {@link List} of {@link AuditPlanActivity} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditPlanActivity> findByCriteria(AuditPlanActivityCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditPlanActivity> specification = createSpecification(criteria);
        return auditPlanActivityRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditPlanActivity} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditPlanActivity> findByCriteria(AuditPlanActivityCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditPlanActivity> specification = createSpecification(criteria);
        return auditPlanActivityRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditPlanActivityCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditPlanActivity> specification = createSpecification(criteria);
        return auditPlanActivityRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AuditPlanActivity> createSpecification(AuditPlanActivityCriteria criteria) {
        Specification<AuditPlanActivity> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), AuditPlanActivity_.id));
            }
            if (criteria.getAuditPlanActivityQuarterId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditPlanActivityQuarterId(),
                    root -> root.join(AuditPlanActivity_.quarters, JoinType.LEFT)
                        .get(AuditPlanActivityQuarter_.id)));
            }
            if (criteria.getAuditPlanActivityInputId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditPlanActivityInputId(),
                    root -> root.join(AuditPlanActivity_.auditPlanActivityInputs, JoinType.LEFT)
                        .get(AuditPlanActivityInput_.id)));
            }
            if (criteria.getAuditPlanId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditPlanId(),
                    root -> root.join(AuditPlanActivity_.auditPlan, JoinType.LEFT)
                        .get(AuditPlan_.id)));
            }
            if (criteria.getRiskId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getRiskId(),
                    root -> root.join(AuditPlanActivity_.risk, JoinType.LEFT).get(Risk_.id)));
            }
            if (criteria.getAuditableAreaId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditableAreaId(),
                    root -> root.join(AuditPlanActivity_.auditableArea, JoinType.LEFT)
                        .get(AuditableArea_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitId(),
                    root -> root.join(AuditPlanActivity_.organisationUnit, JoinType.LEFT)
                        .get(OrganisationUnit_.id)));
            }
        }
        return specification;
    }
}
