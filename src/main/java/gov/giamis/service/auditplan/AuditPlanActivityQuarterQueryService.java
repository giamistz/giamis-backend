package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;
import gov.giamis.domain.auditplan.AuditPlanActivityQuarter_;
import gov.giamis.domain.auditplan.AuditPlanActivity_;
import gov.giamis.domain.setup.Quarter_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.AuditPlanActivityQuarterRepository;
import gov.giamis.service.auditplan.dto.AuditPlanActivityQuarterCriteria;

/**
 * Service for executing complex queries for {@link AuditPlanActivityQuarter} entities in the database.
 * The main input is a {@link AuditPlanActivityQuarterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditPlanActivityQuarter} or a {@link Page} of {@link AuditPlanActivityQuarter} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditPlanActivityQuarterQueryService extends QueryService<AuditPlanActivityQuarter> {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityQuarterQueryService.class);

    private final AuditPlanActivityQuarterRepository auditPlanActivityQuarterRepository;

    public AuditPlanActivityQuarterQueryService(AuditPlanActivityQuarterRepository auditPlanActivityQuarterRepository) {
        this.auditPlanActivityQuarterRepository = auditPlanActivityQuarterRepository;
    }

    /**
     * Return a {@link List} of {@link AuditPlanActivityQuarter} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditPlanActivityQuarter> findByCriteria(AuditPlanActivityQuarterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditPlanActivityQuarter> specification = createSpecification(criteria);
        return auditPlanActivityQuarterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditPlanActivityQuarter} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditPlanActivityQuarter> findByCriteria(AuditPlanActivityQuarterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditPlanActivityQuarter> specification = createSpecification(criteria);
        return auditPlanActivityQuarterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditPlanActivityQuarterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditPlanActivityQuarter> specification = createSpecification(criteria);
        return auditPlanActivityQuarterRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditPlanActivityQuarter> createSpecification(
        AuditPlanActivityQuarterCriteria criteria) {

        Specification<AuditPlanActivityQuarter> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), AuditPlanActivityQuarter_.id));
            }
            if (criteria.getAuditPlanScheduleId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditPlanScheduleId(),
                    root -> root.join(AuditPlanActivityQuarter_.auditPlanActivity, JoinType.LEFT)
                        .get(AuditPlanActivity_.id)));
            }
            if (criteria.getQuarterId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getQuarterId(),
                    root -> root.join(AuditPlanActivityQuarter_.quarter, JoinType.LEFT)
                        .get(Quarter_.id)));
            }
        }
        return specification;
    }
}
