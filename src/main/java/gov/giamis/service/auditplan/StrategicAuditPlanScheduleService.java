package gov.giamis.service.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link StrategicAuditPlanSchedule}.
 */
public interface StrategicAuditPlanScheduleService {

    /**
     * Save a strategicAuditPlanSchedule.
     *
     * @param strategicAuditPlanSchedule the entity to save.
     * @return the persisted entity.
     */
    StrategicAuditPlanSchedule save(StrategicAuditPlanSchedule strategicAuditPlanSchedule);

    /**
     * Get all the strategicPlanSchedules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StrategicAuditPlanSchedule> findAll(Pageable pageable);


    /**
     * Get the "id" strategicPlanSchedule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StrategicAuditPlanSchedule> findOne(Long id);

    /**
     * Delete the "id" strategicPlanSchedule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
