package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.*;
import gov.giamis.service.auditplan.dto.AuditPlanActivityInputCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.AuditPlanActivityInputRepository;

/**
 * Service for executing complex queries for {@link AuditPlanActivityInput} entities in the database.
 * The main input is a {@link AuditPlanActivityInputCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditPlanActivityInput} or a {@link Page} of {@link AuditPlanActivityInput} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditPlanActivityInputQueryService extends QueryService<AuditPlanActivityInput> {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityInputQueryService.class);

    private final AuditPlanActivityInputRepository auditPlanActivityInputRepository;

    public AuditPlanActivityInputQueryService(AuditPlanActivityInputRepository auditPlanActivityInputRepository) {
        this.auditPlanActivityInputRepository = auditPlanActivityInputRepository;
    }

    /**
     * Return a {@link List} of {@link AuditPlanActivityInput} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditPlanActivityInput> findByCriteria(AuditPlanActivityInputCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditPlanActivityInput> specification = createSpecification(criteria);
        return auditPlanActivityInputRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditPlanActivityInput} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditPlanActivityInput> findByCriteria(AuditPlanActivityInputCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditPlanActivityInput> specification = createSpecification(criteria);
        return auditPlanActivityInputRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditPlanActivityInputCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditPlanActivityInput> specification = createSpecification(criteria);
        return auditPlanActivityInputRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditPlanActivityInput> createSpecification(AuditPlanActivityInputCriteria criteria) {
        Specification<AuditPlanActivityInput> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), AuditPlanActivityInput_.id));
            }
            if (criteria.getAuditPlanActivityId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditPlanActivityId(),
                    root -> root.join(AuditPlanActivityInput_.auditPlanActivity, JoinType.LEFT)
                        .get(AuditPlanActivity_.id)));
            }
        }
        return specification;
    }
}
