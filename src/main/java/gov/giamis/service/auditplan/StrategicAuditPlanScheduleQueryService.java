package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;
import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule_;
import gov.giamis.domain.auditplan.StrategicAuditPlan_;
import gov.giamis.domain.risk.Risk_;
import gov.giamis.domain.setup.AuditableArea_;
import gov.giamis.domain.setup.OrganisationUnit_;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanScheduleCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.StrategicAuditPlanScheduleRepository;

/**
 * Service for executing complex queries for {@link StrategicAuditPlanSchedule} entities in the database.
 * The main input is a {@link StrategicAuditPlanScheduleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StrategicAuditPlanSchedule} or a {@link Page} of {@link StrategicAuditPlanSchedule} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StrategicAuditPlanScheduleQueryService extends QueryService<StrategicAuditPlanSchedule> {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleQueryService.class);

    private final StrategicAuditPlanScheduleRepository strategicAuditPlanScheduleRepository;

    public StrategicAuditPlanScheduleQueryService(StrategicAuditPlanScheduleRepository strategicAuditPlanScheduleRepository) {
        this.strategicAuditPlanScheduleRepository = strategicAuditPlanScheduleRepository;
    }

    /**
     * Return a {@link List} of {@link StrategicAuditPlanSchedule} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StrategicAuditPlanSchedule> findByCriteria(StrategicAuditPlanScheduleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<StrategicAuditPlanSchedule> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link StrategicAuditPlanSchedule} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlanSchedule> findByCriteria(StrategicAuditPlanScheduleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<StrategicAuditPlanSchedule> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StrategicAuditPlanScheduleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<StrategicAuditPlanSchedule> specification = createSpecification(criteria);
        return strategicAuditPlanScheduleRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<StrategicAuditPlanSchedule> createSpecification(StrategicAuditPlanScheduleCriteria criteria) {
        Specification<StrategicAuditPlanSchedule> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), StrategicAuditPlanSchedule_.id));
            }
            if (criteria.getStrategicPlanId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getStrategicPlanId(),
                    root -> root.join(StrategicAuditPlanSchedule_.strategicAuditPlan, JoinType.LEFT)
                        .get(StrategicAuditPlan_.id)));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitId(),
                    root -> root.join(StrategicAuditPlanSchedule_.organisationUnit, JoinType.LEFT)
                        .get(OrganisationUnit_.id)));
            }
            if (criteria.getAuditableAreaId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getAuditableAreaId(),
                    root -> root.join(StrategicAuditPlanSchedule_.auditableArea, JoinType.LEFT)
                        .get(AuditableArea_.id)));
            }
            if (criteria.getRiskId() != null) {
                specification = specification.and(buildSpecification(criteria.getRiskId(),
                    root -> root.join(StrategicAuditPlanSchedule_.risk, JoinType.LEFT)
                        .get(Risk_.id)));
            }
        }
        return specification;
    }
}
