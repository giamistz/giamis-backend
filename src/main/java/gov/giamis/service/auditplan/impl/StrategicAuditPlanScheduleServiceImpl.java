package gov.giamis.service.auditplan.impl;

import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleService;
import gov.giamis.repository.auditplan.StrategicAuditPlanScheduleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link StrategicAuditPlanSchedule}.
 */
@Service
@Transactional
public class StrategicAuditPlanScheduleServiceImpl implements StrategicAuditPlanScheduleService {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleServiceImpl.class);

    private final StrategicAuditPlanScheduleRepository strategicAuditPlanScheduleRepository;

    public StrategicAuditPlanScheduleServiceImpl(StrategicAuditPlanScheduleRepository strategicAuditPlanScheduleRepository) {
        this.strategicAuditPlanScheduleRepository = strategicAuditPlanScheduleRepository;
    }

    /**
     * Save a strategicAuditPlanSchedule.
     *
     * @param strategicAuditPlanSchedule the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StrategicAuditPlanSchedule save(StrategicAuditPlanSchedule strategicAuditPlanSchedule) {
        log.debug("Request to save StrategicAuditPlanSchedule : {}", strategicAuditPlanSchedule);
        return strategicAuditPlanScheduleRepository.save(strategicAuditPlanSchedule);
    }

    /**
     * Get all the strategicPlanSchedules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlanSchedule> findAll(Pageable pageable) {
        log.debug("Request to get all StrategicPlanSchedules");
        return strategicAuditPlanScheduleRepository.findAll(pageable);
    }


    /**
     * Get one strategicPlanSchedule by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StrategicAuditPlanSchedule> findOne(Long id) {
        log.debug("Request to get StrategicAuditPlanSchedule : {}", id);
        return strategicAuditPlanScheduleRepository.findById(id);
    }

    /**
     * Delete the strategicPlanSchedule by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StrategicAuditPlanSchedule : {}", id);
        strategicAuditPlanScheduleRepository.deleteById(id);
    }
}
