package gov.giamis.service.auditplan.impl;

import gov.giamis.domain.auditplan.AuditPlanActivityInput;
import gov.giamis.service.auditplan.AuditPlanActivityInputService;
import gov.giamis.repository.auditplan.AuditPlanActivityInputRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing {@link AuditPlanActivityInput}.
 */
@Service
@Transactional
public class AuditPlanActivityInputServiceImpl implements AuditPlanActivityInputService {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityInputServiceImpl.class);

    private final AuditPlanActivityInputRepository auditPlanActivityInputRepository;

    public AuditPlanActivityInputServiceImpl(AuditPlanActivityInputRepository auditPlanActivityInputRepository) {
        this.auditPlanActivityInputRepository = auditPlanActivityInputRepository;
    }

    /**
     * Save a auditPlanActivityInput.
     *
     * @param auditPlanActivityInput the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditPlanActivityInput save(AuditPlanActivityInput auditPlanActivityInput) {
        log.debug("Request to save AuditPlanActivityInput : {}", auditPlanActivityInput);
        return auditPlanActivityInputRepository.save(auditPlanActivityInput);
    }

    /**
     * Get all the auditPlanScheduleCosts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditPlanActivityInput> findAll(Pageable pageable) {
        log.debug("Request to get all AuditPlanActivityInput");
        return auditPlanActivityInputRepository.findAll(pageable);
    }


    /**
     * Get one auditPlanScheduleCost by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditPlanActivityInput> findOne(Long id) {
        log.debug("Request to get AuditPlanActivityInput : {}", id);
        return auditPlanActivityInputRepository.findById(id);
    }

    /**
     * Delete the auditPlanScheduleCost by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditPlanActivityInput : {}", id);
        auditPlanActivityInputRepository.deleteById(id);
    }

	@Override
	public List<AuditPlanActivityInput> findAuditPlanActivityInputByAuditPlanActivityId(Long id) {
		 log.debug("Request to get AuditPlanActivityInput by AuditPlanActivity Id : {}", id);
		return auditPlanActivityInputRepository.findAuditPlanActivityInputByAuditPlanActivity_Id(id);
	}
}
