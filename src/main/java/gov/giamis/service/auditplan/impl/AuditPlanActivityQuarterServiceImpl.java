package gov.giamis.service.auditplan.impl;

import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;
import gov.giamis.service.auditplan.AuditPlanActivityQuarterService;
import gov.giamis.repository.auditplan.AuditPlanActivityQuarterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditPlanActivityQuarter}.
 */
@Service
@Transactional
public class AuditPlanActivityQuarterServiceImpl implements AuditPlanActivityQuarterService {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityQuarterServiceImpl.class);

    private final AuditPlanActivityQuarterRepository auditPlanActivityQuarterRepository;

    public AuditPlanActivityQuarterServiceImpl(AuditPlanActivityQuarterRepository auditPlanActivityQuarterRepository) {
        this.auditPlanActivityQuarterRepository = auditPlanActivityQuarterRepository;
    }

    /**
     * Save a auditPlanActivityQuarter.
     *
     * @param auditPlanActivityQuarter the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditPlanActivityQuarter save(AuditPlanActivityQuarter auditPlanActivityQuarter) {
        log.debug("Request to save AuditPlanActivityQuarter : {}", auditPlanActivityQuarter);
        return auditPlanActivityQuarterRepository.save(auditPlanActivityQuarter);
    }

    /**
     * Get all the auditPlanScheduleQuarters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditPlanActivityQuarter> findAll(Pageable pageable) {
        log.debug("Request to get all AuditPlanActivityQuarter");
        return auditPlanActivityQuarterRepository.findAll(pageable);
    }


    /**
     * Get one auditPlanScheduleQuarter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditPlanActivityQuarter> findOne(Long id) {
        log.debug("Request to get AuditPlanActivityQuarter : {}", id);
        return auditPlanActivityQuarterRepository.findById(id);
    }

    /**
     * Delete the auditPlanScheduleQuarter by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditPlanActivityQuarter : {}", id);
        auditPlanActivityQuarterRepository.deleteById(id);
    }

	@Override
	public List<AuditPlanActivityQuarter> findAuditPlanActivityQuarterByAuditPlanActivityId(Long id) {
		log.debug("Request to get AuditPlanActivityQuarter by AuditPlanActivity Id : {}", id);
		return auditPlanActivityQuarterRepository.findAuditPlanActivityQuarterByAuditPlanActivity_Id(id);
	}
}
