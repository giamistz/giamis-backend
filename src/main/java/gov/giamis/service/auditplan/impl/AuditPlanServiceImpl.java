package gov.giamis.service.auditplan.impl;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.User;
import gov.giamis.dto.ApproveDTO;
import gov.giamis.repository.setup.FileResourceRepository;
import gov.giamis.service.auditplan.AuditPlanService;
import gov.giamis.domain.auditplan.AuditPlan;
import gov.giamis.repository.auditplan.AuditPlanRepository;
import gov.giamis.service.setup.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditPlan}.
 */
@Service
@Transactional
public class AuditPlanServiceImpl implements AuditPlanService {

    private final Logger log = LoggerFactory.getLogger(AuditPlanServiceImpl.class);

    private final AuditPlanRepository auditPlanRepository;

    private final UserService userService;

    private final FileResourceRepository fileResourceRepository;

    public AuditPlanServiceImpl(AuditPlanRepository auditPlanRepository,
                                UserService userService,
                                FileResourceRepository fileResourceRepository) {
        this.auditPlanRepository = auditPlanRepository;
        this.userService = userService;
        this.fileResourceRepository = fileResourceRepository;
    }

    /**
     * Save a auditPlan.
     *
     * @param auditPlan the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditPlan save(AuditPlan auditPlan) {
        log.debug("Request to save AuditPlan : {}", auditPlan);
        return auditPlanRepository.save(auditPlan);
    }

    /**
     * Get all the auditPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditPlan> findAll(Pageable pageable) {
        log.debug("Request to get all AuditPlans");
        return auditPlanRepository.findAll(pageable);
    }


    /**
     * Get one auditPlan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditPlan> findOne(Long id) {
        log.debug("Request to get AuditPlan : {}", id);
        return auditPlanRepository.findById(id);
    }

    /**
     * Delete the auditPlan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditPlan : {}", id);
        auditPlanRepository.deleteById(id);
    }

	@Override
	public void updateAuditPlanById(Long id) {
		 log.debug("Request to update AuditPlan by ID : {}", id);
		auditPlanRepository.updateAuditPlanById(id);

	}

	@Override
	public AuditPlan findAuditPlanById(Long id) {
		 log.debug("Request to get AuditPlan by ID : {}", id);
		return auditPlanRepository.findAuditPlanById(id);
	}

    @Override
    public AuditPlan findByOrganisationUnit(Long id) {
        log.debug("Request to get AuditPlan by organisation unit ID : {}", id);
        return (AuditPlan) auditPlanRepository.findByOrganisationUnit(id);
    }

    @Override
    @Transactional
    public void approve(ApproveDTO approveDTO) {
        AuditPlan plan = auditPlanRepository.getOne(approveDTO.getId());
        plan.setApproved(true);
        plan.setApprovedDate(LocalDate.now());
        User user = userService.getCurrent().get();
        plan.setApprovedBy(user.getFirstName().concat(" ").concat(user.getLastName()));
        FileResource fileResource = fileResourceRepository.getOne(approveDTO.getFileResourceId());
        plan.setFileResource(fileResource);
        auditPlanRepository.save(plan);
        fileResourceRepository.setAssigned(approveDTO.getFileResourceId(), true);
    }
}
