package gov.giamis.service.auditplan.impl;

import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;
import gov.giamis.service.auditplan.StrategicAuditPlanScheduleDayService;
import gov.giamis.repository.auditplan.StrategicAuditPlanScheduleDayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link StrategicAuditPlanScheduleDay}.
 */
@Service
@Transactional
public class StrategicAuditPlanScheduleDayServiceImpl implements StrategicAuditPlanScheduleDayService {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanScheduleDayServiceImpl.class);

    private final StrategicAuditPlanScheduleDayRepository strategicAuditPlanScheduleDayRepository;

    public StrategicAuditPlanScheduleDayServiceImpl(StrategicAuditPlanScheduleDayRepository strategicAuditPlanScheduleDayRepository) {
        this.strategicAuditPlanScheduleDayRepository = strategicAuditPlanScheduleDayRepository;
    }

    /**
     * Save a strategicAuditPlanScheduleDay.
     *
     * @param strategicAuditPlanScheduleDay the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StrategicAuditPlanScheduleDay save(StrategicAuditPlanScheduleDay strategicAuditPlanScheduleDay) {
        log.debug("Request to save StrategicAuditPlanScheduleDay : {}", strategicAuditPlanScheduleDay);
        return strategicAuditPlanScheduleDayRepository.save(strategicAuditPlanScheduleDay);
    }

    /**
     * Get all the strategicPlanScheduleDays.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlanScheduleDay> findAll(Pageable pageable) {
        log.debug("Request to get all StrategicPlanScheduleDays");
        return strategicAuditPlanScheduleDayRepository.findAll(pageable);
    }


    /**
     * Get one strategicPlanScheduleDay by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StrategicAuditPlanScheduleDay> findOne(Long id) {
        log.debug("Request to get StrategicAuditPlanScheduleDay : {}", id);
        return strategicAuditPlanScheduleDayRepository.findById(id);
    }

    /**
     * Delete the strategicPlanScheduleDay by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StrategicAuditPlanScheduleDay : {}", id);
        strategicAuditPlanScheduleDayRepository.deleteById(id);
    }
}
