package gov.giamis.service.auditplan.impl;

import gov.giamis.service.auditplan.AuditPlanActivityService;
import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.repository.auditplan.AuditPlanActivityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditPlanActivity}.
 */
@Service
@Transactional
public class AuditPlanActivityServiceImpl implements AuditPlanActivityService {

    private final Logger log = LoggerFactory.getLogger(AuditPlanActivityServiceImpl.class);

    private final AuditPlanActivityRepository auditPlanActivityRepository;

    public AuditPlanActivityServiceImpl(AuditPlanActivityRepository auditPlanActivityRepository) {
        this.auditPlanActivityRepository = auditPlanActivityRepository;
    }

    /**
     * Save a auditPlanActivity.
     *
     * @param auditPlanActivity the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditPlanActivity save(AuditPlanActivity auditPlanActivity) {
        log.debug("Request to save AuditPlanActivity : {}", auditPlanActivity);
        return auditPlanActivityRepository.save(auditPlanActivity);
    }

    /**
     * Get all the auditPlanSchedules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditPlanActivity> findAll(Pageable pageable) {
        log.debug("Request to get all AuditPlanActivities");
        return auditPlanActivityRepository.findAll(pageable);
    }


    /**
     * Get one auditPlanSchedule by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditPlanActivity> findOne(Long id) {
        log.debug("Request to get AuditPlanActivity : {}", id);
        return auditPlanActivityRepository.findById(id);
    }

    /**
     * Delete the auditPlanSchedule by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditPlanActivity : {}", id);
        auditPlanActivityRepository.deleteById(id);
    }

	@Override
	public AuditPlanActivity findAuditPlanActivityByAuditPlanId(Long id) {
		log.debug("Request to get AuditPlanActivity by AuditPlan Id : {}", id);
		return auditPlanActivityRepository.findFirstAuditPlanActivityByAuditPlan_IdOrderByIdDesc(id);
	}
}
