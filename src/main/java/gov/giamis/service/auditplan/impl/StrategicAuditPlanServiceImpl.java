package gov.giamis.service.auditplan.impl;

import java.time.LocalDate;
import java.util.Optional;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.User;
import gov.giamis.dto.ApproveDTO;
import gov.giamis.repository.setup.FileResourceRepository;
import gov.giamis.service.setup.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.auditplan.StrategicAuditPlan;
import gov.giamis.repository.auditplan.StrategicAuditPlanRepository;
import gov.giamis.service.auditplan.StrategicAuditPlanService;
import gov.giamis.service.setup.UserOrganisationUnitService;

//import javax.jws.soap.SOAPBinding;

/**
 * Service Implementation for managing {@link StrategicAuditPlan}.
 */
@Service
@Transactional
public class StrategicAuditPlanServiceImpl implements StrategicAuditPlanService {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanServiceImpl.class);

    private final StrategicAuditPlanRepository strategicAuditPlanRepository;

    private final UserOrganisationUnitService userOrganisationUnitService;

    private final UserService userService;

    private final FileResourceRepository fileResourceRepository;

    public StrategicAuditPlanServiceImpl(
        StrategicAuditPlanRepository strategicAuditPlanRepository,
        UserOrganisationUnitService userOrganisationUnitService,
        UserService userService, FileResourceRepository fileResourceRepository) {
        this.strategicAuditPlanRepository = strategicAuditPlanRepository;
        this.userOrganisationUnitService = userOrganisationUnitService;
        this.userService = userService;
        this.fileResourceRepository = fileResourceRepository;
    }

    /**
     * Save a strategicAuditPlan.
     *
     * @param strategicAuditPlan the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StrategicAuditPlan save(StrategicAuditPlan strategicAuditPlan) {
        log.debug("Request to save StrategicAuditPlan : {}", strategicAuditPlan);
            strategicAuditPlan.setOrganisationUnit(userOrganisationUnitService.getCurrentOrganisationUnit());
        return strategicAuditPlanRepository.save(strategicAuditPlan);
    }

    /**
     * Get all the strategicPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlan> findAll(Pageable pageable) {
        log.debug("Request to get all StrategicPlans");
        return strategicAuditPlanRepository.findAll(pageable);
    }


    /**
     * Get one strategicPlan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StrategicAuditPlan> findOne(Long id) {
        log.debug("Request to get StrategicAuditPlan : {}", id);
        return strategicAuditPlanRepository.findById(id);
    }

    /**
     * Delete the strategicPlan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StrategicAuditPlan : {}", id);
        strategicAuditPlanRepository.deleteById(id);
    }

	@Override
	public StrategicAuditPlan findByName(String name) {
		 log.debug("Request to get StrategicAuditPlan by name : {}", name);
		return strategicAuditPlanRepository.findByNameIgnoreCase(name);
	}

    @Override
    @Transactional
    public void approve(ApproveDTO approveDTO) {
        StrategicAuditPlan plan = strategicAuditPlanRepository.getOne(approveDTO.getId());
        plan.setApproved(true);
        plan.setApprovedDate(LocalDate.now());
        User user = userService.getCurrent().get();
        plan.setApprovedBy(user.getFirstName().concat(" ").concat(user.getLastName()));
        FileResource fileResource = fileResourceRepository.getOne(approveDTO.getFileResourceId());
        plan.setFileResource(fileResource);
        strategicAuditPlanRepository.save(plan);
        fileResourceRepository.setAssigned(approveDTO.getFileResourceId(), true);
    }

}
