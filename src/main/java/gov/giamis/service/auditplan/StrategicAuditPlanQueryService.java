package gov.giamis.service.auditplan;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.auditplan.StrategicAuditPlan;
import gov.giamis.domain.auditplan.StrategicAuditPlan_;
import gov.giamis.domain.setup.FinancialYear_;
import gov.giamis.domain.setup.OrganisationUnit_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.auditplan.StrategicAuditPlanRepository;
import gov.giamis.service.auditplan.dto.StrategicAuditPlanCriteria;

/**
 * Service for executing complex queries for {@link StrategicAuditPlan} entities in the database.
 * The main input is a {@link StrategicAuditPlanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StrategicAuditPlan} or a {@link Page} of {@link StrategicAuditPlan} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StrategicAuditPlanQueryService extends QueryService<StrategicAuditPlan> {

    private final Logger log = LoggerFactory.getLogger(StrategicAuditPlanQueryService.class);

    private final StrategicAuditPlanRepository strategicAuditPlanRepository;

    public StrategicAuditPlanQueryService(StrategicAuditPlanRepository strategicAuditPlanRepository) {
        this.strategicAuditPlanRepository = strategicAuditPlanRepository;
    }

    /**
     * Return a {@link List} of {@link StrategicAuditPlan} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StrategicAuditPlan> findByCriteria(StrategicAuditPlanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<StrategicAuditPlan> specification = createSpecification(criteria);
        return strategicAuditPlanRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link StrategicAuditPlan} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StrategicAuditPlan> findByCriteria(StrategicAuditPlanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<StrategicAuditPlan> specification = createSpecification(criteria);
        return strategicAuditPlanRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StrategicAuditPlanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<StrategicAuditPlan> specification = createSpecification(criteria);
        return strategicAuditPlanRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<StrategicAuditPlan> createSpecification(StrategicAuditPlanCriteria criteria) {
        Specification<StrategicAuditPlan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), StrategicAuditPlan_.id));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitId(),
                    root -> root.join(StrategicAuditPlan_.organisationUnit, JoinType.LEFT)
                        .get(OrganisationUnit_.id)));
            }
            if (criteria.getStartFinancialYearId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getStartFinancialYearId(),
                    root -> root.join(StrategicAuditPlan_.startFinancialYear, JoinType.LEFT)
                        .get(FinancialYear_.id)));
            }
            if (criteria.getEndFinancialYearId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getEndFinancialYearId(),
                    root -> root.join(StrategicAuditPlan_.endFinancialYear, JoinType.LEFT)
                        .get(FinancialYear_.id)));
            }
        }
        return specification;
    }
}
