package gov.giamis.service.impl;

import gov.giamis.service.ProfessionalQualifcationService;
import gov.giamis.domain.ProfessionalQualifcation;
import gov.giamis.repository.ProfessionalQualifcationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProfessionalQualifcation}.
 */
@Service
@Transactional
public class ProfessionalQualifcationServiceImpl implements ProfessionalQualifcationService {

    private final Logger log = LoggerFactory.getLogger(ProfessionalQualifcationServiceImpl.class);

    private final ProfessionalQualifcationRepository professionalQualifcationRepository;

    public ProfessionalQualifcationServiceImpl(ProfessionalQualifcationRepository professionalQualifcationRepository) {
        this.professionalQualifcationRepository = professionalQualifcationRepository;
    }

    /**
     * Save a professionalQualifcation.
     *
     * @param professionalQualifcation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProfessionalQualifcation save(ProfessionalQualifcation professionalQualifcation) {
        log.debug("Request to save ProfessionalQualifcation : {}", professionalQualifcation);
        return professionalQualifcationRepository.save(professionalQualifcation);
    }

    /**
     * Get all the professionalQualifcations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProfessionalQualifcation> findAll(Pageable pageable) {
        log.debug("Request to get all ProfessionalQualifcations");
        return professionalQualifcationRepository.findAll(pageable);
    }


    /**
     * Get one professionalQualifcation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProfessionalQualifcation> findOne(Long id) {
        log.debug("Request to get ProfessionalQualifcation : {}", id);
        return professionalQualifcationRepository.findById(id);
    }

    /**
     * Delete the professionalQualifcation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProfessionalQualifcation : {}", id);
        professionalQualifcationRepository.deleteById(id);
    }
}
