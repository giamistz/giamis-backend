package gov.giamis.service.impl;

import gov.giamis.service.EngagementSamplingService;
import gov.giamis.domain.EngagementSampling;
import gov.giamis.repository.EngagementSamplingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngagementSampling}.
 */
@Service
@Transactional
public class EngagementSamplingServiceImpl implements EngagementSamplingService {

    private final Logger log = LoggerFactory.getLogger(EngagementSamplingServiceImpl.class);

    private final EngagementSamplingRepository engagementSamplingRepository;

    public EngagementSamplingServiceImpl(EngagementSamplingRepository engagementSamplingRepository) {
        this.engagementSamplingRepository = engagementSamplingRepository;
    }

    /**
     * Save a engagementSampling.
     *
     * @param engagementSampling the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngagementSampling save(EngagementSampling engagementSampling) {
        log.debug("Request to save EngagementSampling : {}", engagementSampling);
        return engagementSamplingRepository.save(engagementSampling);
    }

    /**
     * Get all the engagementSamplings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngagementSampling> findAll(Pageable pageable) {
        log.debug("Request to get all EngagementSamplings");
        return engagementSamplingRepository.findAll(pageable);
    }


    /**
     * Get one engagementSampling by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngagementSampling> findOne(Long id) {
        log.debug("Request to get EngagementSampling : {}", id);
        return engagementSamplingRepository.findById(id);
    }

    /**
     * Delete the engagementSampling by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngagementSampling : {}", id);
        engagementSamplingRepository.deleteById(id);
    }
}
