package gov.giamis.service.impl;

import gov.giamis.service.engagement.EngFindingRecommendationService;
import gov.giamis.domain.engagement.EngFindingRecommendation;
import gov.giamis.repository.engagement.EngFindingReccomendationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EngFindingRecommendation}.
 */
@Service
@Transactional
public class EngFindingRecommendationServiceImpl implements EngFindingRecommendationService {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationServiceImpl.class);

    private final EngFindingReccomendationRepository engFindingReccomendationRepository;

    public EngFindingRecommendationServiceImpl(EngFindingReccomendationRepository engFindingReccomendationRepository) {
        this.engFindingReccomendationRepository = engFindingReccomendationRepository;
    }

    /**
     * Save a engFindingReccomendation.
     *
     * @param engFindingRecommendation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngFindingRecommendation save(EngFindingRecommendation engFindingRecommendation) {
        log.debug("Request to save EngFindingReccomendation : {}", engFindingRecommendation);
        return engFindingReccomendationRepository.save(engFindingRecommendation);
    }

    /**
     * Get all the engFindingReccomendations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngFindingRecommendation> findAll(Pageable pageable) {
        log.debug("Request to get all EngFindingReccomendations");
        return engFindingReccomendationRepository.findAll(pageable);
    }


    /**
     * Get one engFindingReccomendation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngFindingRecommendation> findOne(Long id) {
        log.debug("Request to get EngFindingReccomendation : {}", id);
        return engFindingReccomendationRepository.findById(id);
    }

    /**
     * Delete the engFindingReccomendation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngFindingReccomendation : {}", id);
        engFindingReccomendationRepository.deleteById(id);
    }
}
