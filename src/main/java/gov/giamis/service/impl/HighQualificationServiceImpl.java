package gov.giamis.service.impl;

import gov.giamis.service.HighQualificationService;
import gov.giamis.domain.HighQualification;
import gov.giamis.repository.HighQualificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link HighQualification}.
 */
@Service
@Transactional
public class HighQualificationServiceImpl implements HighQualificationService {

    private final Logger log = LoggerFactory.getLogger(HighQualificationServiceImpl.class);

    private final HighQualificationRepository highQualificationRepository;

    public HighQualificationServiceImpl(HighQualificationRepository highQualificationRepository) {
        this.highQualificationRepository = highQualificationRepository;
    }

    /**
     * Save a highQualification.
     *
     * @param highQualification the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HighQualification save(HighQualification highQualification) {
        log.debug("Request to save HighQualification : {}", highQualification);
        return highQualificationRepository.save(highQualification);
    }

    /**
     * Get all the highQualifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HighQualification> findAll(Pageable pageable) {
        log.debug("Request to get all HighQualifications");
        return highQualificationRepository.findAll(pageable);
    }


    /**
     * Get one highQualification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HighQualification> findOne(Long id) {
        log.debug("Request to get HighQualification : {}", id);
        return highQualificationRepository.findById(id);
    }

    /**
     * Delete the highQualification by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HighQualification : {}", id);
        highQualificationRepository.deleteById(id);
    }
}
