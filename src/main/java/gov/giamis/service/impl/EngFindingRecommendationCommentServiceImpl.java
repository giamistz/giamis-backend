package gov.giamis.service.impl;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import gov.giamis.service.engagement.EngFindingRecommendationCommentService;
import gov.giamis.repository.engagement.EngFindingReccomendCommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EngFindingRecommendationComment}.
 */
@Service
@Transactional
public class EngFindingRecommendationCommentServiceImpl implements EngFindingRecommendationCommentService {

    private final Logger log = LoggerFactory.getLogger(EngFindingRecommendationCommentServiceImpl.class);

    private final EngFindingReccomendCommentRepository engFindingReccomendCommentRepository;

    public EngFindingRecommendationCommentServiceImpl(EngFindingReccomendCommentRepository engFindingReccomendCommentRepository) {
        this.engFindingReccomendCommentRepository = engFindingReccomendCommentRepository;
    }

    /**
     * Save a engFindingReccomendComment.
     *
     * @param engFindingRecommendationComment the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EngFindingRecommendationComment save(EngFindingRecommendationComment engFindingRecommendationComment) {
        log.debug("Request to save EngFindingReccomendComment : {}", engFindingRecommendationComment);
        return engFindingReccomendCommentRepository.save(engFindingRecommendationComment);
    }

    /**
     * Get all the engFindingReccomendComments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EngFindingRecommendationComment> findAll(Pageable pageable) {
        log.debug("Request to get all EngFindingReccomendComments");
        return engFindingReccomendCommentRepository.findAll(pageable);
    }


    /**
     * Get one engFindingReccomendComment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EngFindingRecommendationComment> findOne(Long id) {
        log.debug("Request to get EngFindingReccomendComment : {}", id);
        return engFindingReccomendCommentRepository.findById(id);
    }

    /**
     * Delete the engFindingReccomendComment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EngFindingReccomendComment : {}", id);
        engFindingReccomendCommentRepository.deleteById(id);
    }

    @Override
    public List<EngFindingRecommendationComment> findByRecommendationId(Long id) {
        log.debug("Request to get Engagement Finding by Engagement Procedure ID : {}", id);
        return engFindingReccomendCommentRepository.findByEngFindingRecommendation_Id(id);
    }
}
