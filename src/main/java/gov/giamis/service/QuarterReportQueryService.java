package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.OrganisationUnit_;
import gov.giamis.domain.setup.Quarter_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.QuarterReport;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.QuarterReportRepository;
import gov.giamis.service.dto.QuarterReportCriteria;

/**
 * Service for executing complex queries for {@link QuarterReport} entities in the database.
 * The main input is a {@link QuarterReportCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuarterReport} or a {@link Page} of {@link QuarterReport} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuarterReportQueryService extends QueryService<QuarterReport> {

    private final Logger log = LoggerFactory.getLogger(QuarterReportQueryService.class);

    private final QuarterReportRepository quarterReportRepository;

    public QuarterReportQueryService(QuarterReportRepository quarterReportRepository) {
        this.quarterReportRepository = quarterReportRepository;
    }

    /**
     * Return a {@link List} of {@link QuarterReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuarterReport> findByCriteria(QuarterReportCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<QuarterReport> specification = createSpecification(criteria);
        return quarterReportRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link QuarterReport} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuarterReport> findByCriteria(QuarterReportCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<QuarterReport> specification = createSpecification(criteria);
        return quarterReportRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuarterReportCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<QuarterReport> specification = createSpecification(criteria);
        return quarterReportRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<QuarterReport> createSpecification(QuarterReportCriteria criteria) {
        Specification<QuarterReport> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), QuarterReport_.id));
            }
            if (criteria.getOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitId(),
                    root -> root.join(QuarterReport_.organisationUnit, JoinType.LEFT).get(OrganisationUnit_.id)));
            }
            if (criteria.getQuarterId() != null) {
                specification = specification.and(buildSpecification(criteria.getQuarterId(),
                    root -> root.join(QuarterReport_.quarter, JoinType.LEFT).get(Quarter_.id)));
            }
        }
        return specification;
    }
}
