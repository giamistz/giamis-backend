package gov.giamis.service;

import gov.giamis.domain.ProfessionalQualifcation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ProfessionalQualifcation}.
 */
public interface ProfessionalQualifcationService {

    /**
     * Save a professionalQualifcation.
     *
     * @param professionalQualifcation the entity to save.
     * @return the persisted entity.
     */
    ProfessionalQualifcation save(ProfessionalQualifcation professionalQualifcation);

    /**
     * Get all the professionalQualifcations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProfessionalQualifcation> findAll(Pageable pageable);


    /**
     * Get the "id" professionalQualifcation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProfessionalQualifcation> findOne(Long id);

    /**
     * Delete the "id" professionalQualifcation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
