package gov.giamis.service.report;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.report.ReportParam;

public interface ReportParamService {
    /**
     * Save a reportParam.
     *
     * @param reportParam the entity to save.
     * @return the persisted entity.
     */
    public ReportParam save(ReportParam reportParam);

    /**
     * Get all the reportParams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<ReportParam> findAll(Pageable pageable);


    /**
     * Get one reportParam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ReportParam> findOne(Long id);

    /**
     * Delete the reportParam by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id);
}
