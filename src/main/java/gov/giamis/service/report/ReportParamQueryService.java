package gov.giamis.service.report;
import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportContent_;
import gov.giamis.domain.report.ReportParam;
import gov.giamis.domain.report.ReportParam_;
import gov.giamis.repository.report.ReportParamRepository;
import gov.giamis.service.report.dto.ReportParamCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ReportParam} entities in the database.
 * The main input is a {@link ReportParamCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReportParam} or a {@link Page} of {@link ReportParam} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReportParamQueryService extends QueryService<ReportParam> {

    private final Logger log = LoggerFactory.getLogger(ReportParamQueryService.class);

    private final ReportParamRepository reportParamRepository;

    public ReportParamQueryService(ReportParamRepository reportParamRepository) {
        this.reportParamRepository = reportParamRepository;
    }

    /**
     * Return a {@link List} of {@link ReportParam} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReportParam> findByCriteria(ReportParamCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ReportParam> specification = createSpecification(criteria);
        return reportParamRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ReportParam} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReportParam> findByCriteria(ReportParamCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ReportParam> specification = createSpecification(criteria);
        return reportParamRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReportParamCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ReportParam> specification = createSpecification(criteria);
        return reportParamRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<ReportParam> createSpecification(ReportParamCriteria criteria) {
        Specification<ReportParam> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ReportParam_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ReportParam_.name));
            }
            if (criteria.getOptions() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOptions(), ReportParam_.options));
            }
            if (criteria.getOptionQuery() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOptionQuery(), ReportParam_.optionQuery));
            }
            if (criteria.getReportContentId() != null) {
                specification = specification.and(buildSpecification(criteria.getReportContentId(),
                    root -> root.join(ReportParam_.reportContent, JoinType.LEFT).get(ReportContent_.id)));
            }
        }
        return specification;
    }
}
