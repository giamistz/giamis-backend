package gov.giamis.service.report;

import gov.giamis.config.ApplicationProperties;
import gov.giamis.config.Constants;
import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.enumeration.EngagementRole;
import gov.giamis.domain.enumeration.LetterType;
import gov.giamis.repository.engagement.EngagementMemberRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class EngagementTemplateService {

    private final ReportExportService reportService;


    private final ApplicationProperties applicationProperties;

    private final SpringTemplateEngine templateEngine;

    private final EngagementMemberRepository memberRepository;


    public EngagementTemplateService(ReportExportService reportService,
                                     ApplicationProperties applicationProperties,
                                     SpringTemplateEngine templateEngine,
                                     EngagementMemberRepository memberRepository) {
        this.reportService = reportService;
        this.applicationProperties = applicationProperties;
        this.templateEngine = templateEngine;
        this.memberRepository = memberRepository;
    }

    public File createEngagementMemberLetter(Long memberId)
        throws IOException, JRException, SQLException {
        JasperReport jasperReport= reportService.getTemplate("invitation_letter");
        Map<String, Object> parameters = new HashMap<>();

        File file = ResourceUtils.getFile(applicationProperties.getFileStorageBaseDir().concat("/report/logo.png"));

        if (file.exists()) {
            parameters.put("logo", file.getAbsolutePath());
        }
        parameters.put("engagementMemberId", memberId);
        return reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
    }

    public File createClientLetter(CommunicationLetter letter)
        throws IOException, JRException, SQLException {
        String type = "E1_engagement_letter";
        if (letter.getType() == LetterType.TRANSMITTAL_LETTER) {
            type = "trasmittal_letter";
        }
        JasperReport jasperReport= reportService.getTemplate(type);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("engagementId", letter.getEngagement().getId());
        parameters.put("subject", letter.getSubject());
        parameters.put("body", letter.getBody());
        return reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
    }

    public String getEngInvitationLetterBody(EngagementMember member) {
        EngagementMember teamLead = memberRepository.findByEngagement_IdAndRole(member.getEngagement().getId(), EngagementRole.TEAMLEAD);
        String teamLeader = null;
        if (teamLead != null) {
            teamLeader = teamLead.getUser().getFirstName().concat(" ").concat(teamLead.getUser().getLastName());
        }
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable(Constants.BASE_URL, applicationProperties.getBaseUrl());
        context.setVariable(Constants.API_VERSION, Constants.API_V1);
        context.setVariable(Constants.USER, member.getUser());
        context.setVariable(Constants.ENGAGEMENT, member.getEngagement());
        context.setVariable(Constants.ENGAGEMENT_MEMBER, member);
        context.setVariable("objective", "Objective");
        context.setVariable("teamLeader", teamLeader);
        return  templateEngine.process(Constants.ENG_MEMBER_LETTER_TEMP, context);
    }

    public String getEngagementLetterBody(Engagement engagement, List<EngagementMember> members) {

        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable(Constants.BASE_URL, applicationProperties.getBaseUrl());
        context.setVariable(Constants.API_VERSION, Constants.API_V1);
        context.setVariable(Constants.ENGAGEMENT_MEMBERS, members);
        context.setVariable(Constants.ENGAGEMENT, engagement);
        return  templateEngine.process(Constants.ENG_LETTER_TEMP, context);
    }

    public String getTransmittalLetterBody(Engagement engagement) {

        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable(Constants.BASE_URL, applicationProperties.getBaseUrl());
        context.setVariable(Constants.API_VERSION, Constants.API_V1);
        context.setVariable(Constants.ENGAGEMENT, engagement);
        return  templateEngine.process(Constants.TRAN_LETTER_TEMP, context);
    }

    public String getEngagementAuditeeLetterBody(Engagement eng) {
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable("auditee", eng.getOrganisationUnit().getName());
        context.setVariable("auditee", eng.getOrganisationUnit().getName());

        return templateEngine.process("mail/client-email", context);
    }

    public File createNda(Long memberId)
        throws IOException, JRException, SQLException {
        JasperReport jasperReport= reportService.getTemplate("nda");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("engagementMemberId", memberId);
        return reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
    }

    public String getNdaForm( EngagementMember member) {

        String fullName = member.getUser().getFirstName().concat(" ").concat(member.getUser().getLastName());
        String auditee = member.getEngagement().getOrganisationUnit().getName();
        String agreeUrl = Constants.API_V1.concat("/engagement-members/nda/agree/").concat(member.getId().toString());
        String declineUrl = Constants.API_V1.concat("/engagement-members/nda/decline/").concat(member.getId().toString());
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable("fullName", fullName);
        context.setVariable("auditee", auditee);
        context.setVariable("agreeUrl", agreeUrl);
        context.setVariable("declineUrl", declineUrl);

        return templateEngine.process("mail/nda", context);
    }
    public File getByReport(String type, Map<String, Object> params)
        throws IOException, JRException, SQLException {
        params.put("logo", "logo.png");
        JasperReport jasperReport= reportService.getTemplate(type);
        Map<String, Object> parameters = new HashMap<>();
        return reportService.setParams(parameters).setJasperReport(jasperReport).exportToPdf();
    }


}
