package gov.giamis.service.report;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.report.ReportContentValue;


public interface ReportContentValueService {

    public ReportContentValue save(ReportContentValue reportContentValue);

    /**
     * Get all the reportContentValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
 
    public Page<ReportContentValue> findAll(Pageable pageable);


    /**
     * Get one reportContentValue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ReportContentValue> findOne(Long id);

    /**
     * Delete the reportContentValue by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id);
}
