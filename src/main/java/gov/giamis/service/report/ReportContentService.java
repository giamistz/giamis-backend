package gov.giamis.service.report;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.report.ReportContent;

/**
 * Service Implementation for managing {@link ReportContent}.
 */

public interface ReportContentService {

    public ReportContent save(ReportContent reportContent);

    /**
     * Get all the reportContents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<ReportContent> findAll(Pageable pageable);


    /**
     * Get one reportContent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ReportContent> findOne(Long id);

    /**
     * Delete the reportContent by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id);
}
