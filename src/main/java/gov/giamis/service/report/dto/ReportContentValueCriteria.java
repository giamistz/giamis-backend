package gov.giamis.service.report.dto;

import java.io.Serializable;
import java.util.Objects;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.ReportContentValue} entity. This class is used
 * in {@link gov.giamis.web.rest.ReportContentValueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /report-content-values?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReportContentValueCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter startPage;

    private IntegerFilter endPage;

    private IntegerFilter numberOfPages;

    private LongFilter reportContentId;

    public ReportContentValueCriteria(){
    }

    public ReportContentValueCriteria(ReportContentValueCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.startPage = other.startPage == null ? null : other.startPage.copy();
        this.endPage = other.endPage == null ? null : other.endPage.copy();
        this.numberOfPages = other.numberOfPages == null ? null : other.numberOfPages.copy();
        this.reportContentId = other.reportContentId == null ? null : other.reportContentId.copy();
    }

    @Override
    public ReportContentValueCriteria copy() {
        return new ReportContentValueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getStartPage() {
        return startPage;
    }

    public void setStartPage(IntegerFilter startPage) {
        this.startPage = startPage;
    }

    public IntegerFilter getEndPage() {
        return endPage;
    }

    public void setEndPage(IntegerFilter endPage) {
        this.endPage = endPage;
    }

    public IntegerFilter getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(IntegerFilter numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public LongFilter getReportContentId() {
        return reportContentId;
    }

    public void setReportContentId(LongFilter reportContentId) {
        this.reportContentId = reportContentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReportContentValueCriteria that = (ReportContentValueCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(startPage, that.startPage) &&
            Objects.equals(endPage, that.endPage) &&
            Objects.equals(numberOfPages, that.numberOfPages) &&
            Objects.equals(reportContentId, that.reportContentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        startPage,
        endPage,
        numberOfPages,
        reportContentId
        );
    }

    @Override
    public String toString() {
        return "ReportContentValueCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (startPage != null ? "startPage=" + startPage + ", " : "") +
                (endPage != null ? "endPage=" + endPage + ", " : "") +
                (numberOfPages != null ? "numberOfPages=" + numberOfPages + ", " : "") +
                (reportContentId != null ? "reportContentId=" + reportContentId + ", " : "") +
            "}";
    }

}
