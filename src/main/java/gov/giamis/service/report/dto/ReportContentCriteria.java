package gov.giamis.service.report.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.enumeration.ContentType;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.ReportContent} entity. This class is used
 * in {@link gov.giamis.web.rest.ReportContentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /report-contents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReportContentCriteria implements Serializable, Criteria {
    /**
     * Class for filtering ContentType
     */
    public static class ContentTypeFilter extends Filter<ContentType> {

        public ContentTypeFilter() {
        }

        public ContentTypeFilter(ContentTypeFilter filter) {
            super(filter);
        }

        @Override
        public ContentTypeFilter copy() {
            return new ContentTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private ContentTypeFilter contentType;

    private StringFilter templateUri;

    private IntegerFilter displayOrder;

    private LongFilter reportId;

    private LongFilter parentContentId;

    public ReportContentCriteria(){
    }

    public ReportContentCriteria(ReportContentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.contentType = other.contentType == null ? null : other.contentType.copy();
        this.templateUri = other.templateUri == null ? null : other.templateUri.copy();
        this.displayOrder = other.displayOrder == null ? null : other.displayOrder.copy();
        this.reportId = other.reportId == null ? null : other.reportId.copy();
        this.parentContentId = other.parentContentId == null ? null : other.parentContentId.copy();
    }

    @Override
    public ReportContentCriteria copy() {
        return new ReportContentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public ContentTypeFilter getContentType() {
        return contentType;
    }

    public void setContentType(ContentTypeFilter contentType) {
        this.contentType = contentType;
    }

    public StringFilter getTemplateUri() {
        return templateUri;
    }

    public void setTemplateUri(StringFilter templateUri) {
        this.templateUri = templateUri;
    }

    public IntegerFilter getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(IntegerFilter displayOrder) {
        this.displayOrder = displayOrder;
    }

    public LongFilter getReportId() {
        return reportId;
    }

    public void setReportId(LongFilter reportId) {
        this.reportId = reportId;
    }

    public LongFilter getParentContentId() {
        return parentContentId;
    }

    public void setParentContentId(LongFilter parentContentId) {
        this.parentContentId = parentContentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReportContentCriteria that = (ReportContentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(contentType, that.contentType) &&
            Objects.equals(templateUri, that.templateUri) &&
            Objects.equals(displayOrder, that.displayOrder) &&
            Objects.equals(reportId, that.reportId) &&
            Objects.equals(parentContentId, that.parentContentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        contentType,
        templateUri,
        displayOrder,
        reportId,
        parentContentId
        );
    }

    @Override
    public String toString() {
        return "ReportContentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (contentType != null ? "contentType=" + contentType + ", " : "") +
                (templateUri != null ? "templateUri=" + templateUri + ", " : "") +
                (displayOrder != null ? "displayOrder=" + displayOrder + ", " : "") +
                (reportId != null ? "reportId=" + reportId + ", " : "") +
                (parentContentId != null ? "parentContentId=" + parentContentId + ", " : "") +
            "}";
    }

}
