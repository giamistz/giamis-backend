package gov.giamis.service.report.dto;

import java.io.Serializable;
import java.util.Objects;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link gov.giamis.domain.ReportParam} entity. This class is used
 * in {@link gov.giamis.web.rest.ReportParamResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /report-params?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReportParamCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter options;

    private StringFilter optionQuery;

    private LongFilter reportContentId;

    public ReportParamCriteria(){
    }

    public ReportParamCriteria(ReportParamCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.options = other.options == null ? null : other.options.copy();
        this.optionQuery = other.optionQuery == null ? null : other.optionQuery.copy();
        this.reportContentId = other.reportContentId == null ? null : other.reportContentId.copy();
    }

    @Override
    public ReportParamCriteria copy() {
        return new ReportParamCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getOptions() {
        return options;
    }

    public void setOptions(StringFilter options) {
        this.options = options;
    }

    public StringFilter getOptionQuery() {
        return optionQuery;
    }

    public void setOptionQuery(StringFilter optionQuery) {
        this.optionQuery = optionQuery;
    }

    public LongFilter getReportContentId() {
        return reportContentId;
    }

    public void setReportContentId(LongFilter reportContentId) {
        this.reportContentId = reportContentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReportParamCriteria that = (ReportParamCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(options, that.options) &&
            Objects.equals(optionQuery, that.optionQuery) &&
            Objects.equals(reportContentId, that.reportContentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        options,
        optionQuery,
        reportContentId
        );
    }

    @Override
    public String toString() {
        return "ReportParamCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (options != null ? "options=" + options + ", " : "") +
                (optionQuery != null ? "optionQuery=" + optionQuery + ", " : "") +
                (reportContentId != null ? "reportContentId=" + reportContentId + ", " : "") +
            "}";
    }

}
