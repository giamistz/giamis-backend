package gov.giamis.service.report;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.report.Report;

/**
 * Service Implementation for managing {@link Report}.
 */

public interface ReportService {

    /**
     * Save a report.
     *
     * @param report the entity to save.
     * @return the persisted entity.
     */
    public Report save(Report report);

    /**
     * Get all the reports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */

    public Page<Report> findAll(Pageable pageable);


    /**
     * Get one report by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Report> findOne(Long id);

    /**
     * Delete the report by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id);
}
