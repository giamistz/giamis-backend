package gov.giamis.service.report;
import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportContentValue;
import gov.giamis.domain.report.ReportContentValue_;
import gov.giamis.domain.report.ReportContent_;
import gov.giamis.repository.report.ReportContentValueRepository;
import gov.giamis.service.report.dto.ReportContentValueCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ReportContentValue} entities in the database.
 * The main input is a {@link ReportContentValueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReportContentValue} or a {@link Page} of {@link ReportContentValue} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReportContentValueQueryService extends QueryService<ReportContentValue> {

    private final Logger log = LoggerFactory.getLogger(ReportContentValueQueryService.class);

    private final ReportContentValueRepository reportContentValueRepository;

    public ReportContentValueQueryService(ReportContentValueRepository reportContentValueRepository) {
        this.reportContentValueRepository = reportContentValueRepository;
    }

    /**
     * Return a {@link List} of {@link ReportContentValue} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReportContentValue> findByCriteria(ReportContentValueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ReportContentValue> specification = createSpecification(criteria);
        return reportContentValueRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ReportContentValue} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReportContentValue> findByCriteria(ReportContentValueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ReportContentValue> specification = createSpecification(criteria);
        return reportContentValueRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReportContentValueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ReportContentValue> specification = createSpecification(criteria);
        return reportContentValueRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<ReportContentValue> createSpecification(ReportContentValueCriteria criteria) {
        Specification<ReportContentValue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ReportContentValue_.id));
            }
            if (criteria.getStartPage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartPage(), ReportContentValue_.startPage));
            }
            if (criteria.getEndPage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndPage(), ReportContentValue_.endPage));
            }
            if (criteria.getNumberOfPages() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfPages(), ReportContentValue_.numberOfPages));
            }
            if (criteria.getReportContentId() != null) {
                specification = specification.and(buildSpecification(criteria.getReportContentId(),
                    root -> root.join(ReportContentValue_.reportContent, JoinType.LEFT).get(ReportContent_.id)));
            }
        }
        return specification;
    }
}
