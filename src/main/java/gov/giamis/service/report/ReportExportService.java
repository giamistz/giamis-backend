package gov.giamis.service.report;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.sql.DataSource;
import java.io.*;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

@Service
public class ReportExportService {

    private final DataSource dataSource;

    private Map<String, Object> params;

    private JasperReport jasperReport;

    @Autowired
    public ReportExportService(
        DataSource dataSource) {

        this.dataSource = dataSource;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public JasperReport getJasperReport() {
        return jasperReport;
    }

    public JasperReport getTemplate(String type) throws IOException, JRException {
        /**
         * Get Template for a report
         */
      //  File file = ResourceUtils.getFile("classpath:templates/report/"+type+".jrxml");
        InputStream reportStream = getClass().getResourceAsStream("/templates/report/".concat(type).concat(".jrxml"));

      //  InputStream reportStream = new FileInputStream(file);

        /**
         * Compile/get compiles file
         */
        JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
        JRSaver.saveObject(jasperReport, Files.createTempFile(type,".jasper").toFile());
        reportStream.close();
        return jasperReport;
    }

    public File exportToPdf() throws SQLException, IOException, JRException {

        File tmpFile = Files.createTempFile("userReport",".pdf").toFile();
        Connection connection = dataSource.getConnection();
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(tmpFile));

        SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);

        SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor("chris");
        exportConfig.setEncrypted(true);
        exportConfig.setAllowedPermissionsHint("PRINTING");

        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);

        exporter.exportReport();
        connection.close();
        return  tmpFile;
    }

    public File exportXls() throws IOException, JRException, SQLException {

        File tmpFile = Files.createTempFile("userReport",".xls").toFile();

        Connection connection = dataSource.getConnection();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(tmpFile));

        SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
        reportConfig.setSheetNames(new String[] { "Employee Data" });

        exporter.setConfiguration(reportConfig);
        exporter.exportReport();
        connection.close();

        return tmpFile;
    }

    public File exportDoc() throws IOException, JRException, SQLException {

        File tmpFile = Files.createTempFile("userReport",".doc").toFile();

        Connection connection = dataSource.getConnection();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        JRDocxExporter exporter = new JRDocxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(tmpFile));

        SimpleDocxExporterConfiguration reportConfig = new SimpleDocxExporterConfiguration();
        reportConfig.setMetadataTitle("Doc baby");

        exporter.setConfiguration(reportConfig);
        exporter.exportReport();
        connection.close();
        return tmpFile;
    }

    public ReportExportService setParams(Map<String, Object> parameters) {
        this.params = parameters;
        return this;
    }

    public ReportExportService setJasperReport(JasperReport jasperReport) {
        this.jasperReport = jasperReport;
        return this;
    }
}
