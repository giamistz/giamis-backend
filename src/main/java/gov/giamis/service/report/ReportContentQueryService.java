package gov.giamis.service.report;
import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportContent;
import gov.giamis.domain.report.ReportContent_;
import gov.giamis.domain.report.Report_;
import gov.giamis.repository.report.ReportContentRepository;
import gov.giamis.service.report.dto.ReportContentCriteria;
import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ReportContent} entities in the database.
 * The main input is a {@link ReportContentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReportContent} or a {@link Page} of {@link ReportContent} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReportContentQueryService extends QueryService<ReportContent> {

    private final Logger log = LoggerFactory.getLogger(ReportContentQueryService.class);

    private final ReportContentRepository reportContentRepository;

    public ReportContentQueryService(ReportContentRepository reportContentRepository) {
        this.reportContentRepository = reportContentRepository;
    }

    /**
     * Return a {@link List} of {@link ReportContent} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReportContent> findByCriteria(ReportContentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ReportContent> specification = createSpecification(criteria);
        return reportContentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ReportContent} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReportContent> findByCriteria(ReportContentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ReportContent> specification = createSpecification(criteria);
        return reportContentRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReportContentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ReportContent> specification = createSpecification(criteria);
        return reportContentRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<ReportContent> createSpecification(ReportContentCriteria criteria) {
        Specification<ReportContent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ReportContent_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ReportContent_.name));
            }
            if (criteria.getContentType() != null) {
                specification = specification.and(buildSpecification(criteria.getContentType(), ReportContent_.contentType));
            }
            if (criteria.getTemplateUri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTemplateUri(), ReportContent_.templateUri));
            }
            if (criteria.getDisplayOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDisplayOrder(), ReportContent_.displayOrder));
            }
            if (criteria.getReportId() != null) {
                specification = specification.and(buildSpecification(criteria.getReportId(),
                    root -> root.join(ReportContent_.report, JoinType.LEFT).get(Report_.id)));
            }
            if (criteria.getParentContentId() != null) {
                specification = specification.and(buildSpecification(criteria.getParentContentId(),
                    root -> root.join(ReportContent_.parentContent, JoinType.LEFT).get(ReportContent_.id)));
            }
        }
        return specification;
    }
}
