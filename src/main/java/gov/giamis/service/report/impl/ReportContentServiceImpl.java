package gov.giamis.service.report.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportContent;
import gov.giamis.repository.report.ReportContentRepository;
import gov.giamis.service.report.ReportContentService;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ReportContent}.
 */
@Service
@Transactional
public class ReportContentServiceImpl implements ReportContentService {

    private final Logger log = LoggerFactory.getLogger(ReportContentServiceImpl.class);

    private final ReportContentRepository reportContentRepository;

    public ReportContentServiceImpl(ReportContentRepository reportContentRepository) {
        this.reportContentRepository = reportContentRepository;
    }

    /**
     * Save a reportContent.
     *
     * @param reportContent the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ReportContent save(ReportContent reportContent) {
        log.debug("Request to save ReportContent : {}", reportContent);
        return reportContentRepository.save(reportContent);
    }

    /**
     * Get all the reportContents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReportContent> findAll(Pageable pageable) {
        log.debug("Request to get all ReportContents");
        return reportContentRepository.findAll(pageable);
    }


    /**
     * Get one reportContent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReportContent> findOne(Long id) {
        log.debug("Request to get ReportContent : {}", id);
        return reportContentRepository.findById(id);
    }

    /**
     * Delete the reportContent by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReportContent : {}", id);
        reportContentRepository.deleteById(id);
    }
}
