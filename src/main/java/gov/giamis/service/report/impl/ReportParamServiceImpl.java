package gov.giamis.service.report.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportParam;
import gov.giamis.repository.report.ReportParamRepository;
import gov.giamis.service.report.ReportParamService;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ReportParam}.
 */
@Service
@Transactional
public class ReportParamServiceImpl  implements ReportParamService{

    private final Logger log = LoggerFactory.getLogger(ReportParamServiceImpl.class);

    private final ReportParamRepository reportParamRepository;

    public ReportParamServiceImpl(ReportParamRepository reportParamRepository) {
        this.reportParamRepository = reportParamRepository;
    }

    /**
     * Save a reportParam.
     *
     * @param reportParam the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ReportParam save(ReportParam reportParam) {
        log.debug("Request to save ReportParam : {}", reportParam);
        return reportParamRepository.save(reportParam);
    }

    /**
     * Get all the reportParams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReportParam> findAll(Pageable pageable) {
        log.debug("Request to get all ReportParams");
        return reportParamRepository.findAll(pageable);
    }


    /**
     * Get one reportParam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReportParam> findOne(Long id) {
        log.debug("Request to get ReportParam : {}", id);
        return reportParamRepository.findById(id);
    }

    /**
     * Delete the reportParam by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReportParam : {}", id);
        reportParamRepository.deleteById(id);
    }
}
