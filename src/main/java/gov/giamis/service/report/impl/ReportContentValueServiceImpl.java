package gov.giamis.service.report.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.report.ReportContentValue;
import gov.giamis.repository.report.ReportContentValueRepository;
import gov.giamis.service.report.ReportContentValueService;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ReportContentValue}.
 */
@Service
@Transactional
public class ReportContentValueServiceImpl  implements ReportContentValueService{

    private final Logger log = LoggerFactory.getLogger(ReportContentValueServiceImpl.class);

    private final ReportContentValueRepository reportContentValueRepository;

    public ReportContentValueServiceImpl(ReportContentValueRepository reportContentValueRepository) {
        this.reportContentValueRepository = reportContentValueRepository;
    }

    /**
     * Save a reportContentValue.
     *
     * @param reportContentValue the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ReportContentValue save(ReportContentValue reportContentValue) {
        log.debug("Request to save ReportContentValue : {}", reportContentValue);
        return reportContentValueRepository.save(reportContentValue);
    }

    /**
     * Get all the reportContentValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReportContentValue> findAll(Pageable pageable) {
        log.debug("Request to get all ReportContentValues");
        return reportContentValueRepository.findAll(pageable);
    }


    /**
     * Get one reportContentValue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ReportContentValue> findOne(Long id) {
        log.debug("Request to get ReportContentValue : {}", id);
        return reportContentValueRepository.findById(id);
    }

    /**
     * Delete the reportContentValue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReportContentValue : {}", id);
        reportContentValueRepository.deleteById(id);
    }
}
