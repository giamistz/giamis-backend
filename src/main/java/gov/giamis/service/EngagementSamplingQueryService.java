package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.EngagementSampling;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.EngagementSamplingRepository;
import gov.giamis.service.dto.EngagementSamplingCriteria;

/**
 * Service for executing complex queries for {@link EngagementSampling} entities in the database.
 * The main input is a {@link EngagementSamplingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EngagementSampling} or a {@link Page} of {@link EngagementSampling} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EngagementSamplingQueryService extends QueryService<EngagementSampling> {

    private final Logger log = LoggerFactory.getLogger(EngagementSamplingQueryService.class);

    private final EngagementSamplingRepository engagementSamplingRepository;

    public EngagementSamplingQueryService(EngagementSamplingRepository engagementSamplingRepository) {
        this.engagementSamplingRepository = engagementSamplingRepository;
    }

    /**
     * Return a {@link List} of {@link EngagementSampling} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EngagementSampling> findByCriteria(EngagementSamplingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EngagementSampling> specification = createSpecification(criteria);
        return engagementSamplingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link EngagementSampling} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EngagementSampling> findByCriteria(EngagementSamplingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EngagementSampling> specification = createSpecification(criteria);
        return engagementSamplingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EngagementSamplingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EngagementSampling> specification = createSpecification(criteria);
        return engagementSamplingRepository.count(specification);
    }

    /**
     * Function to convert {@link EngagementSamplingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EngagementSampling> createSpecification(EngagementSamplingCriteria criteria) {
        Specification<EngagementSampling> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), EngagementSampling_.id));
            }
            if (criteria.getPopulationSize() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPopulationSize(), EngagementSampling_.populationSize));
            }
            if (criteria.getSampleSize() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSampleSize(), EngagementSampling_.sampleSize));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), EngagementSampling_.remarks));
            }
            if (criteria.getSamplingType() != null) {
                specification = specification.and(buildSpecification(criteria.getSamplingType(), EngagementSampling_.samplingType));
            }
        }
        return specification;
    }
}
