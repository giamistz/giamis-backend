package gov.giamis.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.HighQualification;
import gov.giamis.domain.*; // for static metamodels
import gov.giamis.repository.HighQualificationRepository;
import gov.giamis.service.dto.HighQualificationCriteria;

/**
 * Service for executing complex queries for {@link HighQualification} entities in the database.
 * The main input is a {@link HighQualificationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HighQualification} or a {@link Page} of {@link HighQualification} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HighQualificationQueryService extends QueryService<HighQualification> {

    private final Logger log = LoggerFactory.getLogger(HighQualificationQueryService.class);

    private final HighQualificationRepository highQualificationRepository;

    public HighQualificationQueryService(HighQualificationRepository highQualificationRepository) {
        this.highQualificationRepository = highQualificationRepository;
    }

    /**
     * Return a {@link List} of {@link HighQualification} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<HighQualification> findByCriteria(HighQualificationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<HighQualification> specification = createSpecification(criteria);
        return highQualificationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link HighQualification} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HighQualification> findByCriteria(HighQualificationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<HighQualification> specification = createSpecification(criteria);
        return highQualificationRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HighQualificationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<HighQualification> specification = createSpecification(criteria);
        return highQualificationRepository.count(specification);
    }

    /**
     * Function to convert {@link HighQualificationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<HighQualification> createSpecification(HighQualificationCriteria criteria) {
        Specification<HighQualification> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), HighQualification_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), HighQualification_.name));
            }
        }
        return specification;
    }
}
