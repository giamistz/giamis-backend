package gov.giamis.service.program;

import gov.giamis.domain.program.AuditProgramControl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditProgramControl}.
 */
public interface AuditProgramControlService {

    /**
     * Save a auditProgramControl.
     *
     * @param auditProgramControl the entity to save.
     * @return the persisted entity.
     */
    AuditProgramControl save(AuditProgramControl auditProgramControl);

    /**
     * Get all the auditProgramControls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditProgramControl> findAll(Pageable pageable);


    /**
     * Get the "id" auditProgramControl.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditProgramControl> findOne(Long id);

    /**
     * Delete the "id" auditProgramControl.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<AuditProgramControl> findControlsByAuditProgramEngagementId(Long id);
}
