package gov.giamis.service.program;

import gov.giamis.domain.program.AuditProgramEngagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link AuditProgramEngagement}.
 */
public interface AuditProgramEngagementService {

    /**
     * Save a auditProgramEngagement.
     *
     * @param auditProgramEngagement the entity to save.
     * @return the persisted entity.
     */
    AuditProgramEngagement save(AuditProgramEngagement auditProgramEngagement);

    /**
     * Get all the auditProgramEngagements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditProgramEngagement> findAll(Pageable pageable);


    /**
     * Get the "id" auditProgramEngagement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditProgramEngagement> findOne(Long id);

    /**
     * Delete the "id" auditProgramEngagement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
