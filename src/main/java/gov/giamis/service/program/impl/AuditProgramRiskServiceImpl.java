package gov.giamis.service.program.impl;

import gov.giamis.service.program.AuditProgramRiskService;
import gov.giamis.domain.program.AuditProgramRisk;
import gov.giamis.repository.program.AuditProgramRiskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditProgramRisk}.
 */
@Service
@Transactional
public class AuditProgramRiskServiceImpl implements AuditProgramRiskService {

    private final Logger log = LoggerFactory.getLogger(AuditProgramRiskServiceImpl.class);

    private final AuditProgramRiskRepository auditProgramRiskRepository;

    public AuditProgramRiskServiceImpl(AuditProgramRiskRepository auditProgramRiskRepository) {
        this.auditProgramRiskRepository = auditProgramRiskRepository;
    }

    /**
     * Save a auditProgramRisk.
     *
     * @param auditProgramRisk the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditProgramRisk save(AuditProgramRisk auditProgramRisk) {
        log.debug("Request to save AuditProgramRisk : {}", auditProgramRisk);
        return auditProgramRiskRepository.save(auditProgramRisk);
    }

    /**
     * Get all the auditProgramRisks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditProgramRisk> findAll(Pageable pageable) {
        log.debug("Request to get all AuditProgramRisks");
        return auditProgramRiskRepository.findAll(pageable);
    }


    /**
     * Get one auditProgramRisk by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditProgramRisk> findOne(Long id) {
        log.debug("Request to get AuditProgramRisk : {}", id);
        return auditProgramRiskRepository.findById(id);
    }

    /**
     * Delete the auditProgramRisk by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditProgramRisk : {}", id);
        auditProgramRiskRepository.deleteById(id);
    }
}
