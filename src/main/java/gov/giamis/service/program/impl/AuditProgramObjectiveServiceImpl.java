package gov.giamis.service.program.impl;

import gov.giamis.service.program.AuditProgramObjectiveService;
import gov.giamis.domain.program.AuditProgramObjective;
import gov.giamis.repository.program.AuditProgramObjectiveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditProgramObjective}.
 */
@Service
@Transactional
public class AuditProgramObjectiveServiceImpl implements AuditProgramObjectiveService {

    private final Logger log = LoggerFactory.getLogger(AuditProgramObjectiveServiceImpl.class);

    private final AuditProgramObjectiveRepository auditProgramObjectiveRepository;

    public AuditProgramObjectiveServiceImpl(AuditProgramObjectiveRepository auditProgramObjectiveRepository) {
        this.auditProgramObjectiveRepository = auditProgramObjectiveRepository;
    }

    /**
     * Save a auditProgramObjective.
     *
     * @param auditProgramObjective the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditProgramObjective save(AuditProgramObjective auditProgramObjective) {
        log.debug("Request to save AuditProgramObjective : {}", auditProgramObjective);
        return auditProgramObjectiveRepository.save(auditProgramObjective);
    }

    /**
     * Get all the auditProgramObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditProgramObjective> findAll(Pageable pageable) {
        log.debug("Request to get all AuditProgramObjectives");
        return auditProgramObjectiveRepository.findAll(pageable);
    }


    /**
     * Get one auditProgramObjective by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditProgramObjective> findOne(Long id) {
        log.debug("Request to get AuditProgramObjective : {}", id);
        return auditProgramObjectiveRepository.findById(id);
    }

    /**
     * Delete the auditProgramObjective by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditProgramObjective : {}", id);
        auditProgramObjectiveRepository.deleteById(id);
    }

    @Override
    public List<AuditProgramObjective> findObjectivesByAuditProgramEngagementId(Long id) {
        log.debug("Request to get audit program risk by audit program ID: {}", id);
        return auditProgramObjectiveRepository.findAuditProgramObjectiveByAuditProgramEngagement_Id(id);
    }
}
