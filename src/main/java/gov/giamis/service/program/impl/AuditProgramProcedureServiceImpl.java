package gov.giamis.service.program.impl;

import gov.giamis.service.program.AuditProgramProcedureService;
import gov.giamis.domain.program.AuditProgramProcedure;
import gov.giamis.repository.program.AuditProgramProcedureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditProgramProcedure}.
 */
@Service
@Transactional
public class AuditProgramProcedureServiceImpl implements AuditProgramProcedureService {

    private final Logger log = LoggerFactory.getLogger(AuditProgramProcedureServiceImpl.class);

    private final AuditProgramProcedureRepository auditProgramProcedureRepository;

    public AuditProgramProcedureServiceImpl(AuditProgramProcedureRepository auditProgramProcedureRepository) {
        this.auditProgramProcedureRepository = auditProgramProcedureRepository;
    }

    /**
     * Save a auditProgramProcedure.
     *
     * @param auditProgramProcedure the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditProgramProcedure save(AuditProgramProcedure auditProgramProcedure) {
        log.debug("Request to save AuditProgramProcedure : {}", auditProgramProcedure);
        return auditProgramProcedureRepository.save(auditProgramProcedure);
    }

    /**
     * Get all the auditProgramProcedures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditProgramProcedure> findAll(Pageable pageable) {
        log.debug("Request to get all AuditProgramProcedures");
        return auditProgramProcedureRepository.findAll(pageable);
    }


    /**
     * Get one auditProgramProcedure by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditProgramProcedure> findOne(Long id) {
        log.debug("Request to get AuditProgramProcedure : {}", id);
        return auditProgramProcedureRepository.findById(id);
    }

    /**
     * Delete the auditProgramProcedure by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditProgramProcedure : {}", id);
        auditProgramProcedureRepository.deleteById(id);
    }
}
