package gov.giamis.service.program.impl;

import gov.giamis.service.program.AuditProgramEngagementService;
import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.repository.program.AuditProgramEngagementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditProgramEngagement}.
 */
@Service
@Transactional
public class AuditProgramEngagementServiceImpl implements AuditProgramEngagementService {

    private final Logger log = LoggerFactory.getLogger(AuditProgramEngagementServiceImpl.class);

    private final AuditProgramEngagementRepository auditProgramEngagementRepository;

    public AuditProgramEngagementServiceImpl(AuditProgramEngagementRepository auditProgramEngagementRepository) {
        this.auditProgramEngagementRepository = auditProgramEngagementRepository;
    }

    /**
     * Save a auditProgramEngagement.
     *
     * @param auditProgramEngagement the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditProgramEngagement save(AuditProgramEngagement auditProgramEngagement) {
        log.debug("Request to save AuditProgramEngagement : {}", auditProgramEngagement);
        return auditProgramEngagementRepository.save(auditProgramEngagement);
    }

    /**
     * Get all the auditProgramEngagements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditProgramEngagement> findAll(Pageable pageable) {
        log.debug("Request to get all AuditProgramEngagements");
        return auditProgramEngagementRepository.findAll(pageable);
    }


    /**
     * Get one auditProgramEngagement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditProgramEngagement> findOne(Long id) {
        log.debug("Request to get AuditProgramEngagement : {}", id);
        return auditProgramEngagementRepository.findById(id);
    }

    /**
     * Delete the auditProgramEngagement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditProgramEngagement : {}", id);
        auditProgramEngagementRepository.deleteById(id);
    }
}
