package gov.giamis.service.program.impl;

import gov.giamis.service.program.AuditProgramControlService;
import gov.giamis.domain.program.AuditProgramControl;
import gov.giamis.repository.program.AuditProgramControlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditProgramControl}.
 */
@Service
@Transactional
public class AuditProgramControlServiceImpl implements AuditProgramControlService {

    private final Logger log = LoggerFactory.getLogger(AuditProgramControlServiceImpl.class);

    private final AuditProgramControlRepository auditProgramControlRepository;

    public AuditProgramControlServiceImpl(AuditProgramControlRepository auditProgramControlRepository) {
        this.auditProgramControlRepository = auditProgramControlRepository;
    }

    /**
     * Save a auditProgramControl.
     *
     * @param auditProgramControl the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditProgramControl save(AuditProgramControl auditProgramControl) {
        log.debug("Request to save AuditProgramControl : {}", auditProgramControl);
        return auditProgramControlRepository.save(auditProgramControl);
    }

    /**
     * Get all the auditProgramControls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditProgramControl> findAll(Pageable pageable) {
        log.debug("Request to get all AuditProgramControls");
        return auditProgramControlRepository.findAll(pageable);
    }


    /**
     * Get one auditProgramControl by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditProgramControl> findOne(Long id) {
        log.debug("Request to get AuditProgramControl : {}", id);
        return auditProgramControlRepository.findById(id);
    }

    /**
     * Delete the auditProgramControl by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditProgramControl : {}", id);
        auditProgramControlRepository.deleteById(id);
    }

    @Override
    public List<AuditProgramControl> findControlsByAuditProgramEngagementId(Long id) {
        log.debug("Request to get audit program procedure by audit program ID: {}", id);
        return auditProgramControlRepository.findAuditProgramControlsByAuditProgramEngagement_Id(id);
    }
}
