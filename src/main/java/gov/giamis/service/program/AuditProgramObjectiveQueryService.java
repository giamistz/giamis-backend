package gov.giamis.service.program;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.program.AuditProgramEngagement_;
import gov.giamis.domain.program.AuditProgramObjective_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.program.AuditProgramObjective;
import gov.giamis.repository.program.AuditProgramObjectiveRepository;
import gov.giamis.service.program.dto.AuditProgramObjectiveCriteria;

/**
 * Service for executing complex queries for {@link AuditProgramObjective} entities in the database.
 * The main input is a {@link AuditProgramObjectiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditProgramObjective} or a {@link Page} of {@link AuditProgramObjective} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditProgramObjectiveQueryService extends QueryService<AuditProgramObjective> {

    private final Logger log = LoggerFactory.getLogger(AuditProgramObjectiveQueryService.class);

    private final AuditProgramObjectiveRepository auditProgramObjectiveRepository;

    public AuditProgramObjectiveQueryService(AuditProgramObjectiveRepository auditProgramObjectiveRepository) {
        this.auditProgramObjectiveRepository = auditProgramObjectiveRepository;
    }

    /**
     * Return a {@link List} of {@link AuditProgramObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditProgramObjective> findByCriteria(AuditProgramObjectiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditProgramObjective> specification = createSpecification(criteria);
        return auditProgramObjectiveRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditProgramObjective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditProgramObjective> findByCriteria(AuditProgramObjectiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditProgramObjective> specification = createSpecification(criteria);
        return auditProgramObjectiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditProgramObjectiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditProgramObjective> specification = createSpecification(criteria);
        return auditProgramObjectiveRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditProgramObjective> createSpecification(AuditProgramObjectiveCriteria criteria) {
        Specification<AuditProgramObjective> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditProgramObjective_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AuditProgramObjective_.name));
            }
            if (criteria.getAuditProgramEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditProgramEngagementId(),
                    root -> root.join(AuditProgramObjective_.auditProgramEngagement, JoinType.LEFT).get(AuditProgramEngagement_.id)));
            }
        }
        return specification;
    }
}
