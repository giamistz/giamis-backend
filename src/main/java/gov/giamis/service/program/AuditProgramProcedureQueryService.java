package gov.giamis.service.program;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.program.AuditProgramControl_;
import gov.giamis.domain.program.AuditProgramProcedure_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.program.AuditProgramProcedure;
import gov.giamis.repository.program.AuditProgramProcedureRepository;
import gov.giamis.service.program.dto.AuditProgramProcedureCriteria;

/**
 * Service for executing complex queries for {@link AuditProgramProcedure} entities in the database.
 * The main input is a {@link AuditProgramProcedureCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditProgramProcedure} or a {@link Page} of {@link AuditProgramProcedure} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditProgramProcedureQueryService extends QueryService<AuditProgramProcedure> {

    private final Logger log = LoggerFactory.getLogger(AuditProgramProcedureQueryService.class);

    private final AuditProgramProcedureRepository auditProgramProcedureRepository;

    public AuditProgramProcedureQueryService(AuditProgramProcedureRepository auditProgramProcedureRepository) {
        this.auditProgramProcedureRepository = auditProgramProcedureRepository;
    }

    /**
     * Return a {@link List} of {@link AuditProgramProcedure} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditProgramProcedure> findByCriteria(AuditProgramProcedureCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditProgramProcedure> specification = createSpecification(criteria);
        return auditProgramProcedureRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditProgramProcedure} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditProgramProcedure> findByCriteria(AuditProgramProcedureCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditProgramProcedure> specification = createSpecification(criteria);
        return auditProgramProcedureRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditProgramProcedureCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditProgramProcedure> specification = createSpecification(criteria);
        return auditProgramProcedureRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditProgramProcedure> createSpecification(AuditProgramProcedureCriteria criteria) {
        Specification<AuditProgramProcedure> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditProgramProcedure_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AuditProgramProcedure_.name));
            }
            if (criteria.getAuditProgramControlId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditProgramControlId(),
                    root -> root.join(AuditProgramProcedure_.auditProgramControl, JoinType.LEFT).get(AuditProgramControl_.id)));
            }
        }
        return specification;
    }
}
