package gov.giamis.service.program;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.program.AuditProgramObjective_;
import gov.giamis.domain.program.AuditProgramRisk_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.program.AuditProgramRisk;
import gov.giamis.repository.program.AuditProgramRiskRepository;
import gov.giamis.service.program.dto.AuditProgramRiskCriteria;

/**
 * Service for executing complex queries for {@link AuditProgramRisk} entities in the database.
 * The main input is a {@link AuditProgramRiskCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditProgramRisk} or a {@link Page} of {@link AuditProgramRisk} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditProgramRiskQueryService extends QueryService<AuditProgramRisk> {

    private final Logger log = LoggerFactory.getLogger(AuditProgramRiskQueryService.class);

    private final AuditProgramRiskRepository auditProgramRiskRepository;

    public AuditProgramRiskQueryService(AuditProgramRiskRepository auditProgramRiskRepository) {
        this.auditProgramRiskRepository = auditProgramRiskRepository;
    }

    /**
     * Return a {@link List} of {@link AuditProgramRisk} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditProgramRisk> findByCriteria(AuditProgramRiskCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditProgramRisk> specification = createSpecification(criteria);
        return auditProgramRiskRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditProgramRisk} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditProgramRisk> findByCriteria(AuditProgramRiskCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditProgramRisk> specification = createSpecification(criteria);
        return auditProgramRiskRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditProgramRiskCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditProgramRisk> specification = createSpecification(criteria);
        return auditProgramRiskRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AuditProgramRisk> createSpecification(AuditProgramRiskCriteria criteria) {
        Specification<AuditProgramRisk> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditProgramRisk_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AuditProgramRisk_.name));
            }
            if (criteria.getAuditProgramObjectiveId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditProgramObjectiveId(),
                    root -> root.join(AuditProgramRisk_.auditProgramObjective, JoinType.LEFT).get(AuditProgramObjective_.id)));
            }
        }
        return specification;
    }

}
