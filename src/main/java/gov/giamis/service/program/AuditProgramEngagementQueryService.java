package gov.giamis.service.program;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.program.AuditProgramEngagement_;
import gov.giamis.domain.setup.AuditableArea_;
import gov.giamis.domain.setup.OrganisationUnitLevel_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.repository.program.AuditProgramEngagementRepository;
import gov.giamis.service.program.dto.AuditProgramEngagementCriteria;

/**
 * Service for executing complex queries for {@link AuditProgramEngagement} entities in the database.
 * The main input is a {@link AuditProgramEngagementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditProgramEngagement} or a {@link Page} of {@link AuditProgramEngagement} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditProgramEngagementQueryService extends QueryService<AuditProgramEngagement> {

    private final Logger log = LoggerFactory.getLogger(AuditProgramEngagementQueryService.class);

    private final AuditProgramEngagementRepository auditProgramEngagementRepository;

    public AuditProgramEngagementQueryService(AuditProgramEngagementRepository auditProgramEngagementRepository) {
        this.auditProgramEngagementRepository = auditProgramEngagementRepository;
    }

    /**
     * Return a {@link List} of {@link AuditProgramEngagement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditProgramEngagement> findByCriteria(AuditProgramEngagementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditProgramEngagement> specification = createSpecification(criteria);
        return auditProgramEngagementRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditProgramEngagement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditProgramEngagement> findByCriteria(AuditProgramEngagementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditProgramEngagement> specification = createSpecification(criteria);
        return auditProgramEngagementRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditProgramEngagementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditProgramEngagement> specification = createSpecification(criteria);
        return auditProgramEngagementRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AuditProgramEngagement> createSpecification(AuditProgramEngagementCriteria criteria) {
        Specification<AuditProgramEngagement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditProgramEngagement_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AuditProgramEngagement_.process));
            }
            if (criteria.getAuditableAreaId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditableAreaId(),
                    root -> root.join(AuditProgramEngagement_.auditableArea, JoinType.INNER).get(AuditableArea_.id)));
            }
            if (criteria.getOrganisationUnitLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganisationUnitLevelId(),
                    root -> root.join(AuditProgramEngagement_.organisationUnitLevels, JoinType.INNER).get(OrganisationUnitLevel_.id)));
            }
        }
        return specification;
    }
}
