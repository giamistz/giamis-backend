package gov.giamis.service.program.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.web.rest.program.AuditProgramEngagementResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditProgramEngagement} entity. This class is used
 * in {@link AuditProgramEngagementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-program-engagements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditProgramEngagementCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter auditableAreaId;

    private LongFilter organisationUnitLevelId;

    public AuditProgramEngagementCriteria(){
    }

    public AuditProgramEngagementCriteria(AuditProgramEngagementCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.auditableAreaId = other.auditableAreaId == null ? null : other.auditableAreaId.copy();
        this.organisationUnitLevelId = other.organisationUnitLevelId == null ? null : other.organisationUnitLevelId.copy();
    }

    @Override
    public AuditProgramEngagementCriteria copy() {
        return new AuditProgramEngagementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getAuditableAreaId() {
        return auditableAreaId;
    }

    public void setAuditableAreaId(LongFilter auditableAreaId) {
        this.auditableAreaId = auditableAreaId;
    }

    public LongFilter getOrganisationUnitLevelId() {
        return organisationUnitLevelId;
    }

    public void setOrganisationUnitLevelId(LongFilter organisationUnitLevelId) {
        this.organisationUnitLevelId = organisationUnitLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditProgramEngagementCriteria that = (AuditProgramEngagementCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(auditableAreaId, that.auditableAreaId) &&
            Objects.equals(organisationUnitLevelId, that.organisationUnitLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        auditableAreaId,
        organisationUnitLevelId
        );
    }

    @Override
    public String toString() {
        return "AuditProgramEngagementCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (auditableAreaId != null ? "auditableAreaId=" + auditableAreaId + ", " : "") +
                (organisationUnitLevelId != null ? "organisationUnitLevelId=" + organisationUnitLevelId + ", " : "") +
            "}";
    }

}
