package gov.giamis.service.program.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.program.AuditProgramProcedure;
import gov.giamis.web.rest.program.AuditProgramProcedureResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditProgramProcedure} entity. This class is used
 * in {@link AuditProgramProcedureResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-program-procedures?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditProgramProcedureCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter auditProgramControlId;

    public AuditProgramProcedureCriteria(){
    }

    public AuditProgramProcedureCriteria(AuditProgramProcedureCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.auditProgramControlId = other.auditProgramControlId == null ? null : other.auditProgramControlId.copy();
    }

    @Override
    public AuditProgramProcedureCriteria copy() {
        return new AuditProgramProcedureCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getAuditProgramControlId() {
        return auditProgramControlId;
    }

    public void setAuditProgramControlId(LongFilter auditProgramControlId) {
        this.auditProgramControlId = auditProgramControlId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditProgramProcedureCriteria that = (AuditProgramProcedureCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(auditProgramControlId, that.auditProgramControlId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        auditProgramControlId
        );
    }

    @Override
    public String toString() {
        return "AuditProgramProcedureCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (auditProgramControlId != null ? "auditProgramControlId=" + auditProgramControlId + ", " : "") +
            "}";
    }

}
