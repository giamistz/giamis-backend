package gov.giamis.service.program.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.program.AuditProgramObjective;
import gov.giamis.web.rest.program.AuditProgramObjectiveResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditProgramObjective} entity. This class is used
 * in {@link AuditProgramObjectiveResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-program-objectives?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditProgramObjectiveCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter auditProgramEngagementId;

    public AuditProgramObjectiveCriteria(){
    }

    public AuditProgramObjectiveCriteria(AuditProgramObjectiveCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.auditProgramEngagementId = other.auditProgramEngagementId == null ? null : other.auditProgramEngagementId.copy();
    }

    @Override
    public AuditProgramObjectiveCriteria copy() {
        return new AuditProgramObjectiveCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getAuditProgramEngagementId() {
        return auditProgramEngagementId;
    }

    public void setAuditProgramEngagementId(LongFilter auditProgramEngagementId) {
        this.auditProgramEngagementId = auditProgramEngagementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditProgramObjectiveCriteria that = (AuditProgramObjectiveCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(auditProgramEngagementId, that.auditProgramEngagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        auditProgramEngagementId
        );
    }

    @Override
    public String toString() {
        return "AuditProgramObjectiveCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (auditProgramEngagementId != null ? "auditProgramEngagementId=" + auditProgramEngagementId + ", " : "") +
            "}";
    }

}
