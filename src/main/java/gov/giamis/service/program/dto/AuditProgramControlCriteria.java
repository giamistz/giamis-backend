package gov.giamis.service.program.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.program.AuditProgramControl;
import gov.giamis.web.rest.program.AuditProgramControlResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditProgramControl} entity. This class is used
 * in {@link AuditProgramControlResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-program-controls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditProgramControlCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private IntegerFilter stepNumber;

    private StringFilter areaOfResponsibility;

    private StringFilter criteria;

    private LongFilter auditProgramEngagementId;

    public AuditProgramControlCriteria(){
    }

    public AuditProgramControlCriteria(AuditProgramControlCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.stepNumber = other.stepNumber == null ? null : other.stepNumber.copy();
        this.areaOfResponsibility = other.areaOfResponsibility == null ? null : other.areaOfResponsibility.copy();
        this.criteria = other.criteria == null ? null : other.criteria.copy();
        this.auditProgramEngagementId = other.auditProgramEngagementId == null ? null : other.auditProgramEngagementId.copy();
    }

    @Override
    public AuditProgramControlCriteria copy() {
        return new AuditProgramControlCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(IntegerFilter stepNumber) {
        this.stepNumber = stepNumber;
    }

    public StringFilter getAreaOfResponsibility() {
        return areaOfResponsibility;
    }

    public void setAreaOfResponsibility(StringFilter areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
    }

    public StringFilter getCriteria() {
        return criteria;
    }

    public void setCriteria(StringFilter criteria) {
        this.criteria = criteria;
    }

    public LongFilter getAuditProgramEngagementId() {
        return auditProgramEngagementId;
    }

    public void setAuditProgramEngagementId(LongFilter auditProgramEngagementId) {
        this.auditProgramEngagementId = auditProgramEngagementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditProgramControlCriteria that = (AuditProgramControlCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(stepNumber, that.stepNumber) &&
            Objects.equals(areaOfResponsibility, that.areaOfResponsibility) &&
            Objects.equals(criteria, that.criteria) &&
            Objects.equals(auditProgramEngagementId, that.auditProgramEngagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        stepNumber,
        areaOfResponsibility,
        criteria,
            auditProgramEngagementId
        );
    }

    @Override
    public String toString() {
        return "AuditProgramControlCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (stepNumber != null ? "stepNumber=" + stepNumber + ", " : "") +
                (areaOfResponsibility != null ? "areaOfResponsibility=" + areaOfResponsibility + ", " : "") +
                (criteria != null ? "criteria=" + criteria + ", " : "") +
                (auditProgramEngagementId != null ? "auditProgramEngagementId=" + auditProgramEngagementId + ", " : "") +
            "}";
    }

}
