package gov.giamis.service.program.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.program.AuditProgramRisk;
import gov.giamis.web.rest.program.AuditProgramRiskResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditProgramRisk} entity. This class is used
 * in {@link AuditProgramRiskResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /audit-program-risks?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditProgramRiskCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter auditProgramObjectiveId;

    public AuditProgramRiskCriteria(){
    }

    public AuditProgramRiskCriteria(AuditProgramRiskCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.auditProgramObjectiveId = other.auditProgramObjectiveId == null ? null : other.auditProgramObjectiveId.copy();
    }

    @Override
    public AuditProgramRiskCriteria copy() {
        return new AuditProgramRiskCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getAuditProgramObjectiveId() {
        return auditProgramObjectiveId;
    }

    public void setAuditProgramObjectiveId(LongFilter auditProgramObjectiveId) {
        this.auditProgramObjectiveId = auditProgramObjectiveId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditProgramRiskCriteria that = (AuditProgramRiskCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(auditProgramObjectiveId, that.auditProgramObjectiveId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        auditProgramObjectiveId
        );
    }

    @Override
    public String toString() {
        return "AuditProgramRiskCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (auditProgramObjectiveId != null ? "auditProgramObjectiveId=" + auditProgramObjectiveId + ", " : "") +
            "}";
    }

}
