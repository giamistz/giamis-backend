package gov.giamis.service.program;

import gov.giamis.domain.program.AuditProgramRisk;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditProgramRisk}.
 */
public interface AuditProgramRiskService {

    /**
     * Save a auditProgramRisk.
     *
     * @param auditProgramRisk the entity to save.
     * @return the persisted entity.
     */
    AuditProgramRisk save(AuditProgramRisk auditProgramRisk);

    /**
     * Get all the auditProgramRisks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditProgramRisk> findAll(Pageable pageable);


    /**
     * Get the "id" auditProgramRisk.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditProgramRisk> findOne(Long id);

    /**
     * Delete the "id" auditProgramRisk.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
