package gov.giamis.service.program;

import gov.giamis.domain.program.AuditProgramProcedure;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditProgramProcedure}.
 */
public interface AuditProgramProcedureService {

    /**
     * Save a auditProgramProcedure.
     *
     * @param auditProgramProcedure the entity to save.
     * @return the persisted entity.
     */
    AuditProgramProcedure save(AuditProgramProcedure auditProgramProcedure);

    /**
     * Get all the auditProgramProcedures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditProgramProcedure> findAll(Pageable pageable);


    /**
     * Get the "id" auditProgramProcedure.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditProgramProcedure> findOne(Long id);

    /**
     * Delete the "id" auditProgramProcedure.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

}
