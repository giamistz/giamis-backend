package gov.giamis.service.program;

import gov.giamis.domain.program.AuditProgramObjective;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AuditProgramObjective}.
 */
public interface AuditProgramObjectiveService {

    /**
     * Save a auditProgramObjective.
     *
     * @param auditProgramObjective the entity to save.
     * @return the persisted entity.
     */
    AuditProgramObjective save(AuditProgramObjective auditProgramObjective);

    /**
     * Get all the auditProgramObjectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditProgramObjective> findAll(Pageable pageable);


    /**
     * Get the "id" auditProgramObjective.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditProgramObjective> findOne(Long id);

    /**
     * Delete the "id" auditProgramObjective.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<AuditProgramObjective> findObjectivesByAuditProgramEngagementId(Long id);
}
