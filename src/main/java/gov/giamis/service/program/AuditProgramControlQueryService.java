package gov.giamis.service.program;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.program.AuditProgramControl_;
import gov.giamis.domain.program.AuditProgramEngagement_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.program.AuditProgramControl;
import gov.giamis.repository.program.AuditProgramControlRepository;
import gov.giamis.service.program.dto.AuditProgramControlCriteria;

/**
 * Service for executing complex queries for {@link AuditProgramControl} entities in the database.
 * The main input is a {@link AuditProgramControlCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditProgramControl} or a {@link Page} of {@link AuditProgramControl} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditProgramControlQueryService extends QueryService<AuditProgramControl> {

    private final Logger log = LoggerFactory.getLogger(AuditProgramControlQueryService.class);

    private final AuditProgramControlRepository auditProgramControlRepository;

    public AuditProgramControlQueryService(AuditProgramControlRepository auditProgramControlRepository) {
        this.auditProgramControlRepository = auditProgramControlRepository;
    }

    /**
     * Return a {@link List} of {@link AuditProgramControl} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditProgramControl> findByCriteria(AuditProgramControlCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditProgramControl> specification = createSpecification(criteria);
        return auditProgramControlRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditProgramControl} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditProgramControl> findByCriteria(AuditProgramControlCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditProgramControl> specification = createSpecification(criteria);
        return auditProgramControlRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditProgramControlCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditProgramControl> specification = createSpecification(criteria);
        return auditProgramControlRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditProgramControl> createSpecification(AuditProgramControlCriteria criteria) {
        Specification<AuditProgramControl> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditProgramControl_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AuditProgramControl_.name));
            }
            if (criteria.getStepNumber() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStepNumber(), AuditProgramControl_.stepNumber));
            }
            if (criteria.getAreaOfResponsibility() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAreaOfResponsibility(), AuditProgramControl_.areaOfResponsibility));
            }
            if (criteria.getCriteria() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCriteria(), AuditProgramControl_.criteria));
            }
            if (criteria.getAuditProgramEngagementId() != null) {
                specification = specification.and(buildSpecification(criteria.getAuditProgramEngagementId(),
                    root -> root.join(AuditProgramControl_.auditProgramEngagement, JoinType.LEFT).get(AuditProgramEngagement_.id)));
            }
        }
        return specification;
    }
}
