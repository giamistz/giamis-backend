package gov.giamis.service.setup.impl;

import gov.giamis.domain.engagement.EngagementFinding;
import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.domain.engagement.WorkProgram;
import gov.giamis.domain.enumeration.ControlScore;
import gov.giamis.dto.WalkThroughDTO;
import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.repository.engagement.InternalControlRepository;
import gov.giamis.service.engagement.EngagementFindingService;
import gov.giamis.service.engagement.EngagementProcedureService;
import gov.giamis.service.engagement.InternalControlService;

import gov.giamis.service.engagement.WorkProgramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link InternalControl}.
 */
@Service
@Transactional
public class InternalControlServiceImpl implements InternalControlService {

    private final Logger log = LoggerFactory.getLogger(InternalControlServiceImpl.class);

    private final InternalControlRepository internalControlRepository;

    private final EngagementRepository engagementRepository;

    private final EngagementProcedureService procedureService;

    private final WorkProgramService workDoneService;

    private final EngagementFindingService findingService;

    public InternalControlServiceImpl(InternalControlRepository internalControlRepository,
                                      EngagementRepository engagementRepository,
                                      EngagementProcedureService procedureService,
                                      WorkProgramService workDoneService,
                                      EngagementFindingService findingService) {
        this.internalControlRepository = internalControlRepository;
        this.engagementRepository = engagementRepository;
        this.procedureService = procedureService;
        this.workDoneService = workDoneService;
        this.findingService = findingService;
    }

    /**
     * Save a internalControl.
     *
     * @param internalControl the entity to save.
     * @return the persisted entity.
     */
    @Override
    public InternalControl save(InternalControl internalControl) {
        log.debug("Request to save InternalControl : {}", internalControl);
        return internalControlRepository.save(internalControl);
    }

    /**
     * Get all the internalControls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<InternalControl> findAll(Pageable pageable) {
        log.debug("Request to get all InternalControls");
        return internalControlRepository.findAll(pageable);
    }


    /**
     * Get one internalControl by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InternalControl> findOne(Long id) {
        log.debug("Request to get InternalControl : {}", id);
        return internalControlRepository.findById(id);
    }

    /**
     * Delete the internalControl by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete InternalControl : {}", id);
        internalControlRepository.deleteById(id);
    }

	@Override
	public Optional<InternalControl> findLastRecord(Long id) {
		log.debug("Request to get Last Record of InternalControl By Engagement ID");
        return internalControlRepository.findFirstByEngagement_IdOrderByIdDesc(id);
	}

	@Transactional
    @Override
    public void createWalkThrough(WalkThroughDTO walkThrough) {
        List<EngagementFinding> findings = new ArrayList<>();
        walkThrough.getIntControls().forEach(c -> {
            InternalControl control = internalControlRepository.getOne(c.getId());
            control.setScore(c.getScore());
            control.setWtRef(c.getWtRef());
            internalControlRepository.save(control);
            if (c.getScore().equals(ControlScore.NOT_AS_NARRATED)) {
                EngagementProcedure procedure = procedureService.getOrCreate(c);
                WorkProgram workDone = workDoneService.getOrCreate("Walk through ".concat(c.getWtRef()), procedure);
                findings.add(new EngagementFinding(procedure, workDone));
                findingService.initiateFinding(findings);
            } else {

            }
        });

        engagementRepository.setAdequacy(walkThrough.getEngagementId(), walkThrough.getControlAdequacy());
    }
}
