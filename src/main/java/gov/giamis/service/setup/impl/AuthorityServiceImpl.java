package gov.giamis.service.setup.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import gov.giamis.domain.setup.User;
import gov.giamis.service.setup.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.Authority;
import gov.giamis.repository.setup.AuthorityDao;
import gov.giamis.repository.setup.AuthorityMenuDao;
import gov.giamis.repository.setup.AuthorityRepository;
import gov.giamis.service.setup.AuthorityService;

/**
 * Service Implementation for managing {@link Authority}.
 */
@Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

    private final Logger log = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    private final AuthorityRepository authorityRepository;
    
    private final AuthorityDao authorityDao;
    
    private final AuthorityMenuDao authorityMenuDao;

    @Autowired
    private UserService userService;

    public AuthorityServiceImpl(AuthorityRepository authorityRepository,
    		AuthorityDao authorityDao,
    		AuthorityMenuDao authorityMenuDao) {
        this.authorityRepository = authorityRepository;
        this.authorityDao = authorityDao;
        this.authorityMenuDao = authorityMenuDao;
    }

    /**
     * Save a authority.
     *
     * @param authority the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Authority save(Authority authority) {
        log.debug("Request to save Authority : {}", authority);
        return authorityRepository.save(authority);
    }

    /**
     * Get all the authoritys.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Authority> findAll(Pageable pageable) {
        log.debug("Request to get all Authorities");
        return authorityRepository.findAll(pageable);
    }


    /**
     * Get one authority by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Authority> findOne(Long id) {
        log.debug("Request to get Authority : {}", id);
        return authorityRepository.findById(id);
    }

    /**
     * Delete the authority by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Authority : {}", id);
        authorityRepository.deleteById(id);
    }

	@Override
	public List<Authority> findAll() {
		// TODO Auto-generated method stub
		return authorityRepository.findAll();
	}

	@Override
	public List<Authority> findAuthorityOrderByName() {
		log.debug("Request to get Authorities order by name");
		return authorityRepository.findAuthorityOrderByName();
	}

	@Override
	public List<Authority> findDistinctAuthority() {
		log.debug("Request to get Unique Authorities by resource");
		return authorityRepository.findDistinctAuthority();
	}

	@Override
	public List<Authority> getAllAuthorities(String resource) {
		log.debug("Request to get Authorities by resource for Role");
		return authorityRepository.findDistinctAuthorityByResourceOrderByNameDesc(resource);
	}

	@Override
	public List<Authority> getAllMenuAuthorities(String getResource) {
		log.debug("Request to get Authorities by resource for Menu Item");
		return authorityMenuDao.getAllMenuAuthorities(getResource);
	}

	@Override
	public Set<Authority> getAllAuthorities(Long id) {
		log.debug("Request to get Authorities by USER ID");
		return authorityRepository.getAllAuthorities(id);
	}

    @Override
    public Set<Authority> getAllAuthoritiesByEmail(String email) {
        return authorityRepository.getAllAuthoritiesByEmail(email);
    }

}
