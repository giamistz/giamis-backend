package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.MenuGroupService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.repository.setup.MenuGroupRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MenuGroup}.
 */
@Service
@Transactional
public class MenuGroupServiceImpl implements MenuGroupService {

    private final Logger log = LoggerFactory.getLogger(MenuGroupServiceImpl.class);

    private final MenuGroupRepository menuGroupRepository;

    public MenuGroupServiceImpl(MenuGroupRepository menuGroupRepository) {
        this.menuGroupRepository = menuGroupRepository;
    }

    /**
     * Save a menuGroup.
     *
     * @param menuGroup the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MenuGroup save(MenuGroup menuGroup) {
        log.debug("Request to save MenuGroup : {}", menuGroup);
        menuGroup.setName(Utils.capitalizeWords(Jsoup.parse(menuGroup.getName().toLowerCase()).text()));
        return menuGroupRepository.save(menuGroup);
    }

    /**
     * Get all the riskCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MenuGroup> findAll(Pageable pageable) {
        log.debug("Request to get all Menu Groups");
        return menuGroupRepository.findAll(pageable);
    }

    /**
     * Get one menuGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MenuGroup> findOne(Long id) {
        log.debug("Request to get MenuGroup : {}", id);
        return menuGroupRepository.findById(id);
    }

    /**
     * Delete the menuGroup by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MenuGroup : {}", id);
        menuGroupRepository.deleteById(id);
    }

    @Override
    public MenuGroup findByName(String name) {
        log.debug("Request to get MenuGroup by name : {}", name);
        return menuGroupRepository.findByNameIgnoreCase(name);
    }
    
    @Override
	public List<MenuGroup> getMenuGroups(Long id) {
		log.debug("Request to get Menu Items by USER ID");
		return menuGroupRepository.getAllMenuGroups(id);
	}
    
    @Override
   	public List<MenuGroup> getMenuGroupsNoParent(Long id) {
   		log.debug("Request to get Menu Items by USER ID");
   		return menuGroupRepository.getAllMenuGroupsNoParent(id);
   	}

	@Override
	public List<MenuGroup> findByParentMenuGroup(Long id) {
		 log.debug("Request to get MenuGroup by parent menu group : {}", id);
	        return menuGroupRepository.findByParentMenuGroup(id);
	}
	
	@Override
	public List<MenuGroup> findMenuGroupById(Long id) {
		 log.debug("Request to get MenuGroup by ID: {}", id);
	        return menuGroupRepository.findMenuGroupById(id);
	}

	@Override
	public List<MenuGroup> getAllMenuItems(Long id) {
		 log.debug("Request to get MenuItems by menu Group ID: {}", id);
	        return menuGroupRepository.getAllMenuItems(id);
	}

}
