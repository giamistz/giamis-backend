package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.FileResourceTypeService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.FileResourceType;
import gov.giamis.repository.setup.FileResourceTypeRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link FileResourceType}.
 */
@Service
@Transactional
public class FileResourceTypeServiceImpl implements FileResourceTypeService {

    private final Logger log = LoggerFactory.getLogger(FileResourceTypeServiceImpl.class);

    private final FileResourceTypeRepository fileResourceTypeRepository;

    public FileResourceTypeServiceImpl(FileResourceTypeRepository fileResourceTypeRepository) {
        this.fileResourceTypeRepository = fileResourceTypeRepository;
    }

    /**
     * Save a fileResourceType.
     *
     * @param fileResourceType the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FileResourceType save(FileResourceType fileResourceType) {
        log.debug("Request to save FileResourceType : {}", fileResourceType);
        fileResourceType.setName(Utils.capitalizeWords(Jsoup.parse(fileResourceType.getName().toLowerCase()).text()));
        return fileResourceTypeRepository.save(fileResourceType);
    }

    /**
     * Get all the fileResourceTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FileResourceType> findAll(Pageable pageable) {
        log.debug("Request to get all FileResourceTypes");
        return fileResourceTypeRepository.findAll(pageable);
    }


    /**
     * Get one fileResourceType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FileResourceType> findOne(Long id) {
        log.debug("Request to get FileResourceType : {}", id);
        return fileResourceTypeRepository.findById(id);
    }

    /**
     * Delete the fileResourceType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FileResourceType : {}", id);
        fileResourceTypeRepository.deleteById(id);
    }
    
    @Override
    public FileResourceType findByName(String name) {
    	log.debug("Request to get FileResourceType by name : {}", name);
        return fileResourceTypeRepository.findByNameAllIgnoreCase(name);
    }
}
