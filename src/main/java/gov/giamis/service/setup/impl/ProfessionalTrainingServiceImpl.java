package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.ProfessionalTrainingService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.ProfessionalTraining;
import gov.giamis.repository.setup.ProfessionalTrainingRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProfessionalTraining}.
 */
@Service
@Transactional
public class ProfessionalTrainingServiceImpl implements ProfessionalTrainingService {

    private final Logger log = LoggerFactory.getLogger(ProfessionalTrainingServiceImpl.class);

    private final ProfessionalTrainingRepository professionalTraningRepository;

    public ProfessionalTrainingServiceImpl(ProfessionalTrainingRepository professionalTraningRepository) {
        this.professionalTraningRepository = professionalTraningRepository;
    }

    /**
     * Save a professionalTraining.
     *
     * @param professionalTraining the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProfessionalTraining save(ProfessionalTraining professionalTraining) {
        log.debug("Request to save ProfessionalTraining : {}", professionalTraining);
        professionalTraining.setTitle(Utils.capitalizeWords(Jsoup.parse(professionalTraining.getTitle().toLowerCase()).text()));
        return professionalTraningRepository.save(professionalTraining);
    }

    /**
     * Get all the professionalTrainings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProfessionalTraining> findAll(Pageable pageable) {
        log.debug("Request to get all ProfessionalTrainings");
        return professionalTraningRepository.findAll(pageable);
    }

    /**
     * Get one professionalTraining by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProfessionalTraining> findOne(Long id) {
        log.debug("Request to get ProfessionalTraining : {}", id);
        return professionalTraningRepository.findById(id);
    }

    /**
     * Delete the professionalTraining by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProfessionalTraining : {}", id);
        professionalTraningRepository.deleteById(id);
    }

    @Override
    public ProfessionalTraining findByUser(Long id) {
         log.debug("Request to get ProfessionalTraining by USERID : {}",id);
        return professionalTraningRepository.findByUser_Id(id);
    }

}
