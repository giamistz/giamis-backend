package gov.giamis.service.setup.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuGroup;
import gov.giamis.security.SecurityUtils;
import gov.giamis.service.setup.AuthorityService;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.MenuItem;
import gov.giamis.repository.setup.MenuItemRepository;
import gov.giamis.service.setup.MenuItemService;
import gov.giamis.service.util.Utils;

import javax.persistence.EntityManager;

/**
 * Service Implementation for managing {@link MenuItem}.
 */
@Service
@Transactional
public class MenuItemServiceImpl implements MenuItemService {

    private final Logger log = LoggerFactory.getLogger(MenuItemServiceImpl.class);

    private final MenuItemRepository menuItemRepository;

    private final EntityManager em;

    private final AuthorityService authorityService;

    public MenuItemServiceImpl(
        MenuItemRepository menuItemRepository,
        EntityManager em,
        AuthorityService authorityService) {
            this.menuItemRepository = menuItemRepository;
            this.em = em;
            this.authorityService = authorityService;
    }

    /**
     * Save a menuItem.
     *
     * @param menuItem the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MenuItem save(MenuItem menuItem) {
        log.debug("Request to save MenuItem : {}", menuItem);
        menuItem.setName(Utils.capitalizeWords(Jsoup.parse(menuItem.getName().toLowerCase()).text()));
        return menuItemRepository.save(menuItem);
    }
    
    /**
     * Get all the menuItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MenuItem> findAll(Pageable pageable) {
        log.debug("Request to get all Menu Items");
        return menuItemRepository.findAll(pageable);
    }

    /**
     * Get all the menuItems with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<MenuItem> findAllWithEagerRelationships(Pageable pageable) {
        return menuItemRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one menuItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MenuItem> findOne(Long id) {
        log.debug("Request to get MenuItem : {}", id);
        return menuItemRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the menuItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MenuItem : {}", id);
        menuItemRepository.deleteById(id);
    }

    
    @Override
    public MenuItem findByName(String name) {
        log.debug("Request to find menuItem by auditor name {}");
        return menuItemRepository.findByNameIgnoreCase(name);
    }
    
    @Override
    public MenuItem findMenuItemById(Long id) {
        log.debug("Request to find menuItem by ID {}"+id);
        return menuItemRepository.findMenuItemById(id);
    }

    @Override
    public List<MenuItem> findAllMenuItems() {
        log.debug("Request to get List of  menuItems{}");
        return menuItemRepository.findAll();
    }

	@Override
	public List<MenuItem> getAllMenuItemsByGroup(Long menuGroupId) {
		 log.debug("Request to get List of  menuItems by MENU GROUP ID");
		return menuItemRepository.getAllMenuItemsByGroup(menuGroupId);
	}

	@Override
    public List<MenuGroup> getWithItems(Map<String, List<Long>> ids) {
        Session session = em.unwrap(Session.class);
        Filter filter = session.enableFilter("inId");
        filter.setParameterList("ids", ids.get("groupIds"));
        filter.setParameterList("itemIds", ids.get("itemIds"));
        return (List<MenuGroup>) session.createQuery("from MenuGroup g where g.parentMenuGroup = null ").list();
    }

    @Override
    public List<MenuItem> getByAuthorities(List<Long> authorities) {
        return menuItemRepository.findByAuthorities(authorities);
    }

    @Override
    public List<MenuItem> getWithNoGroupByAuthorities(List<Long> authorities) {
        return menuItemRepository.findWithGroupByAuthorities(authorities);
    }

}
