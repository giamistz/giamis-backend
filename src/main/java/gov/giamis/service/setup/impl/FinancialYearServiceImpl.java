package gov.giamis.service.setup.impl;

import java.time.LocalDate;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.repository.setup.FinancialYearRepository;
import gov.giamis.service.setup.FinancialYearService;
import gov.giamis.service.util.Utils;
/**
 * Service Implementation for managing {@link FinancialYear}.
 */
@Service
@Transactional
public class FinancialYearServiceImpl implements FinancialYearService {

	private final Logger log = LoggerFactory.getLogger(FinancialYearServiceImpl.class);
	Integer status=Constants.INACTIVE;

	private final FinancialYearRepository financialYearRepository;

	public FinancialYearServiceImpl(FinancialYearRepository financialYearRepository) {
		this.financialYearRepository = financialYearRepository;
	}

	/**
	 * Save a period.
	 *
	 * @param financialYear the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public FinancialYear save(FinancialYear financialYear) {
		log.debug("Request to save Period : {}", financialYear);
		financialYear.setName(Utils.capitalizeWords(Jsoup.parse(financialYear.getName().toLowerCase()).text()));
		financialYearRepository.save(financialYear);
		changeOtherStatus(status,financialYear);
		return financialYearRepository.save(financialYear);
	}

	/**
	 * Get all the periods.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<FinancialYear> findAll(Pageable pageable) {
		log.debug("Request to get all Periods");
		return financialYearRepository.findAllByOrderByIdDesc(pageable);
	}

	/**
	 * Get one period by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<FinancialYear> findOne(Long id) {
		log.debug("Request to get Period : {}", id);
		return financialYearRepository.findById(id);
	}

	/**
	 * Delete the period by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Period : {}", id);
		financialYearRepository.deleteById(id);
	}

	@Override
	public FinancialYear findByName(String name) {
		log.debug("Request to get Financial Year by name : {}", name);
		return financialYearRepository.findByNameIgnoreCase(name);
	}

	@Override
	public void changeStatus(Integer status,FinancialYear financialYear) {
		log.debug("Request to change status of Financial Year");
		if( status == Constants.INACTIVE) {
			status = Constants.ACTIVE;
			financialYearRepository.updateOtherFinancialYearStatus(Constants.INACTIVE, financialYear.getId());
		}
		else {
			status = Constants.INACTIVE;
		}
	financialYearRepository.updateFinancialYearStatus(status, financialYear.getId());
	}

	@Override
	public void changeOtherStatus(Integer status,FinancialYear financialYear) {
		log.debug("Request to activate status of current Financial Year");
	financialYearRepository.updateOtherFinancialYearStatus(status, financialYear.getId());
	}

	@Override
	public FinancialYear findStatusById(Long id) {
		log.debug("Request to get status of Financial Year by ID");
	return financialYearRepository.findStatusById(id);
	}

	@Override
	public FinancialYear findByStatus(Integer status) {
		log.debug("Request to get Financial Year by status");
		return financialYearRepository.findByStatus(status);
	}

    @Override
    public FinancialYear findCurrent() {
        LocalDate currentDate = LocalDate.now();
        return financialYearRepository.findCurrent(currentDate);
    }

}
