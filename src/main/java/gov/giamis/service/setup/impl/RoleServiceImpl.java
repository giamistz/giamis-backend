package gov.giamis.service.setup.impl;

import java.util.List;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Role;
import gov.giamis.repository.setup.RoleRepository;
import gov.giamis.service.setup.RoleService;
import gov.giamis.service.util.Utils;

/**
 * Service Implementation for managing {@link Role}.
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	private final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);

	private final RoleRepository roleRepository;

	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	/**
	 * Save a role.
	 *
	 * @param role the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Role save(Role role) {
		log.debug("Request to save Role : {}", role);
		role.setName(Utils.capitalizeWords(Jsoup.parse(role.getName().toLowerCase()).text()));
		String currentCode = null;
		long currentCodeNumber = 0;
		List<Role> roleDetails = roleRepository.findAllRoles();
		if(!roleDetails.isEmpty()) {
			currentCode = roleDetails.get(0).getCode().substring(2);
			currentCodeNumber = Long.parseLong(currentCode)+1;
			role.setCode(Constants.ROLE_CODE+currentCodeNumber);
		}else {
			currentCodeNumber = 1;
			role.setCode(Constants.ROLE_CODE+currentCodeNumber);
		}
		return roleRepository.save(role);
	}
	
	/**
	 * Get all the roles.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Role> findAll(Pageable pageable) {
		log.debug("Request to get all Roles");
		return roleRepository.findAll(pageable);
	}

	/**
	 * Get all the roles with eager load of many-to-many relationships.
	 *
	 * @return the list of entities.
	 */
	public Page<Role> findAllWithEagerRelationships(Pageable pageable) {
		return roleRepository.findAllWithEagerRelationships(pageable);
	}

	/**
	 * Get one role by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Role> findOne(Long id) {
		log.debug("Request to get Role : {}", id);
		return roleRepository.findOneWithEagerRelationships(id);
	}

	/**
	 * Delete the role by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Role : {}", id);
		roleRepository.deleteById(id);
	}

	
	@Override
	public Role findByName(String name) {
		log.debug("Request to find role by auditor name {}");
		return roleRepository.findByNameIgnoreCase(name);
	}
	
	@Override
	public Role findRoleById(Long id) {
		log.debug("Request to find role by ID {}"+id);
		return roleRepository.findRoleById(id);
	}

	@Override
	public List<Role> findAllRoles() {
		log.debug("Request to get List of  roles{}");
		return roleRepository.findAll();
	}
	
}
