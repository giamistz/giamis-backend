package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.RiskRankService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.RiskRank;
import gov.giamis.repository.setup.RiskRankRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RiskRank}.
 */
@Service
@Transactional
public class RiskRankServiceImpl implements RiskRankService {

	private final Logger log = LoggerFactory.getLogger(RiskRankServiceImpl.class);

	private final RiskRankRepository riskRankRepository;

	public RiskRankServiceImpl(RiskRankRepository riskRankRepository) {
		this.riskRankRepository = riskRankRepository;
	}

	/**
	 * Save a riskRank.
	 *
	 * @param riskRank the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public RiskRank save(RiskRank riskRank) {
		log.debug("Request to save RiskRank : {}", riskRank);
		riskRank.setName(Utils.capitalizeWords(Jsoup.parse(riskRank.getName().toLowerCase()).text()));
		return riskRankRepository.save(riskRank);
	}

	/**
	 * Get all the riskRanks.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<RiskRank> findAll(Pageable pageable) {
		log.debug("Request to get all RiskRanks");
		return riskRankRepository.findAll(pageable);
	}

	/**
	 * Get one riskRank by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<RiskRank> findOne(Long id) {
		log.debug("Request to get RiskRank : {}", id);
		return riskRankRepository.findById(id);
	}

	/**
	 * Delete the riskRank by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete RiskRank : {}", id);
		riskRankRepository.deleteById(id);
	}

	@Override
	public RiskRank findByName(String name) {
		log.debug("Request to find RiskRank by name : {}", name);
		return riskRankRepository.findByNameAllIgnoreCase(name);
	}

	@Override
	public List<RiskRank> findRiskRankByColor(Integer minValue, Integer maxValue) {
		log.debug("Request to get color from RiskRank using two values : {}", minValue+" , "+maxValue);
		int result = 0;
		result = minValue * maxValue;
		return riskRankRepository.getColorFromRiskRank(result);
	}
}
