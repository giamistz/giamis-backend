package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.ObjectiveService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.Objective;
import gov.giamis.repository.setup.ObjectiveRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Objective}.
 */
@Service
@Transactional
public class ObjectiveServiceImpl implements ObjectiveService {

	private final Logger log = LoggerFactory.getLogger(ObjectiveServiceImpl.class);

	private final ObjectiveRepository objectiveRepository;

	public ObjectiveServiceImpl(ObjectiveRepository objectiveRepository) {
		this.objectiveRepository = objectiveRepository;
	}

	/**
	 * Save a objective.
	 *
	 * @param objective the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Objective save(Objective objective) {
		log.debug("Request to save Objective : {}", objective);
		objective.setDescription(Utils.capitalizeWords(Jsoup.parse(objective.getDescription().toLowerCase()).text()));
		return objectiveRepository.save(objective);
	}

	/**
	 * Get all the objectives.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Objective> findAll(Pageable pageable) {
		log.debug("Request to get all Objectives");
		return objectiveRepository.findAll(pageable);
	}

	/**
	 * Get one objective by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Objective> findOne(Long id) {
		log.debug("Request to get Objective : {}", id);
		return objectiveRepository.findById(id);
	}

	/**
	 * Delete the objective by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Objective : {}", id);
		objectiveRepository.deleteById(id);
	}

	@Override
	public Objective findByDescription(String description) {
		log.debug("Request to get Objective by description : {}", description);
		return objectiveRepository.findByDescriptionIgnoreCase(description);
	}
}
