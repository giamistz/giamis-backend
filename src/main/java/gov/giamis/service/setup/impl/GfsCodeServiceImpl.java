package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.GfsCodeService;
import gov.giamis.domain.setup.GfsCode;
import gov.giamis.repository.setup.GfsCodeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link GfsCode}.
 */
@Service
@Transactional
public class GfsCodeServiceImpl implements GfsCodeService {

    private final Logger log = LoggerFactory.getLogger(GfsCodeServiceImpl.class);

    private final GfsCodeRepository gfsCodeRepository;

    public GfsCodeServiceImpl(GfsCodeRepository gfsCodeRepository) {
        this.gfsCodeRepository = gfsCodeRepository;
    }

    /**
     * Save a gfsCode.
     *
     * @param gfsCode the entity to save.
     * @return the persisted entity.
     */
    @Override
    public GfsCode save(GfsCode gfsCode) {
        log.debug("Request to save GfsCode : {}", gfsCode);
        return gfsCodeRepository.save(gfsCode);
    }

    /**
     * Get all the gfsCodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GfsCode> findAll(Pageable pageable) {
        log.debug("Request to get all GfsCodes");
        return gfsCodeRepository.findAll(pageable);
    }


    /**
     * Get one gfsCode by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GfsCode> findOne(Long id) {
        log.debug("Request to get GfsCode : {}", id);
        return gfsCodeRepository.findById(id);
    }

    /**
     * Delete the gfsCode by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete GfsCode : {}", id);
        gfsCodeRepository.deleteById(id);
    }
    
    @Override
    public GfsCode findByCode(String code) {
        log.debug("Request to get GfsCode by name : {}", code);
        return gfsCodeRepository.findByCodeIgnoreCase(code);
    }
}
