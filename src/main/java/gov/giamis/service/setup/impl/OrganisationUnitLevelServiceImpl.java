package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.OrganisationUnitLevelService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.OrganisationUnitLevel;
import gov.giamis.repository.setup.OrganisationUnitLevelRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrganisationUnitLevel}.
 */
@Service
@Transactional
public class OrganisationUnitLevelServiceImpl implements OrganisationUnitLevelService {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitLevelServiceImpl.class);

    private final OrganisationUnitLevelRepository organisationUnitLevelRepository;

    public OrganisationUnitLevelServiceImpl(OrganisationUnitLevelRepository organisationUnitLevelRepository) {
        this.organisationUnitLevelRepository = organisationUnitLevelRepository;
    }

    /**
     * Save a organisationUnitLevel.
     *
     * @param organisationUnitLevel the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationUnitLevel save(OrganisationUnitLevel organisationUnitLevel) {
        log.debug("Request to save OrganisationUnitLevel : {}", organisationUnitLevel);
        organisationUnitLevel.setName(Utils.capitalizeWords(Jsoup.parse(organisationUnitLevel.getName().toLowerCase()).text()));
        return organisationUnitLevelRepository.save(organisationUnitLevel);
    }

    /**
     * Get all the organisationUnitLevels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationUnitLevel> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationUnitLevels");
        return organisationUnitLevelRepository.findAll(pageable);
    }


    /**
     * Get one organisationUnitLevel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationUnitLevel> findOne(Long id) {
        log.debug("Request to get OrganisationUnitLevel : {}", id);
        return organisationUnitLevelRepository.findById(id);
    }

    /**
     * Delete the organisationUnitLevel by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationUnitLevel : {}", id);
        organisationUnitLevelRepository.deleteById(id);
    }
    
    @Override
    public OrganisationUnitLevel findByName(String name) {
    	 log.debug("Request to get OrganisationUnitLevel by name: {}", name);
        return organisationUnitLevelRepository.findByNameIgnoreCase(name);
    }
}
