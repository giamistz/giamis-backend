package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.QuarterService;
import gov.giamis.service.util.Utils;
import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Quarter;
import gov.giamis.repository.setup.QuarterRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Quarter}.
 */
@Service
@Transactional
public class QuarterServiceImpl implements QuarterService {
	Integer status=Constants.INACTIVE;
    private final Logger log = LoggerFactory.getLogger(QuarterServiceImpl.class);

    private final QuarterRepository quarterRepository;

    public QuarterServiceImpl(QuarterRepository quarterRepository) {
        this.quarterRepository = quarterRepository;
    }

    /**
     * Save a quarter.
     *
     * @param quarter the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Quarter save(Quarter quarter) {
        log.debug("Request to save Quarter : {}", quarter);
        quarter.setName(Utils.capitalizeWords(Jsoup.parse(quarter.getName().toLowerCase()).text()));
        quarterRepository.save(quarter);
		changeOtherStatus(status,quarter);
		return quarterRepository.save(quarter);
    }

    /**
     * Get all the quarters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Quarter> findAll(Pageable pageable) {
        log.debug("Request to get all Quarters");
        return quarterRepository.findAll(pageable);
    }


    /**
     * Get one quarter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Quarter> findOne(Long id) {
        log.debug("Request to get Quarter : {}", id);
        return quarterRepository.findById(id);
    }

    /**
     * Delete the quarter by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Quarter : {}", id);
        quarterRepository.deleteById(id);
    }
    
    @Override
    public Quarter findByName(String name) {
    	 log.debug("Request to get Quarter by name : {}", name);
        return quarterRepository.findByNameAllIgnoreCase(name);
    }
    
	@Override
	public void changeStatus(Integer status,Quarter quarter) {
		 log.debug("Request to change status of Quarter");
		if( status == Constants.INACTIVE) {
			status = Constants.ACTIVE;
			quarterRepository.updateOtherQuarterStatus(Constants.INACTIVE, quarter.getId());
		}
		else {
			status = Constants.INACTIVE;
		}
		quarterRepository.updateQuarterStatus(status, quarter.getId());
	}
	
	@Override
	public void changeOtherStatus(Integer status,Quarter quarter) {
		log.debug("Request to activate status of current Quarter");
		quarterRepository.updateOtherQuarterStatus(status, quarter.getId());
	}
	
	@Override
	public Quarter findStatusById(Long id) {
		log.debug("Request to get status of Quarter by ID");
	return quarterRepository.findStatusById(id);
	}
}
