package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.OrganisationUnitGroupSetService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.OrganisationUnitGroupSet;
import gov.giamis.repository.setup.OrganisationUnitGroupSetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import org.jsoup.Jsoup;

/**
 * Service Implementation for managing {@link OrganisationUnitGroupSet}.
 */
@Service
@Transactional
public class OrganisationUnitGroupSetServiceImpl implements OrganisationUnitGroupSetService {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupSetServiceImpl.class);

    private final OrganisationUnitGroupSetRepository organisationUnitGroupSetRepository;

    public OrganisationUnitGroupSetServiceImpl(OrganisationUnitGroupSetRepository organisationUnitGroupSetRepository) {
        this.organisationUnitGroupSetRepository = organisationUnitGroupSetRepository;
    }

    /**
     * Save a organisationUnitGroupSet.
     *
     * @param organisationUnitGroupSet the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationUnitGroupSet save(OrganisationUnitGroupSet organisationUnitGroupSet) {
        log.debug("Request to save OrganisationUnitGroupSet : {}", organisationUnitGroupSet);
        organisationUnitGroupSet.setName(Utils.capitalizeWords(Jsoup.parse(organisationUnitGroupSet.getName().toLowerCase()).text()));
        return organisationUnitGroupSetRepository.save(organisationUnitGroupSet);
    }

    /**
     * Get all the organisationUnitGroupSets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationUnitGroupSet> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationUnitGroupSets");
        return organisationUnitGroupSetRepository.findAll(pageable);
    }


    /**
     * Get one organisationUnitGroupSet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationUnitGroupSet> findOne(Long id) {
        log.debug("Request to get OrganisationUnitGroupSet : {}", id);
        return organisationUnitGroupSetRepository.findById(id);
    }

    /**
     * Delete the organisationUnitGroupSet by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationUnitGroupSet : {}", id);
        organisationUnitGroupSetRepository.deleteById(id);
    }

	@Override
	public OrganisationUnitGroupSet findByName(String name) {
		 log.debug("Request to get OrganisationUnitGroupSet by name: {}", name);
		return organisationUnitGroupSetRepository.findByNameIgnoreCase(name);
	}
    
    
}
