package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.OrganisationUnitGroupService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.OrganisationUnitGroup;
import gov.giamis.repository.setup.OrganisationUnitGroupRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrganisationUnitGroup}.
 */
@Service
@Transactional
public class OrganisationUnitGroupServiceImpl implements OrganisationUnitGroupService {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupServiceImpl.class);

    private final OrganisationUnitGroupRepository organisationUnitGroupRepository;

    public OrganisationUnitGroupServiceImpl(OrganisationUnitGroupRepository organisationUnitGroupRepository) {
        this.organisationUnitGroupRepository = organisationUnitGroupRepository;
    }

    /**
     * Save a organisationUnitGroup.
     *
     * @param organisationUnitGroup the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationUnitGroup save(OrganisationUnitGroup organisationUnitGroup) {
        log.debug("Request to save OrganisationUnitGroup : {}", organisationUnitGroup);
        organisationUnitGroup.setName(Utils.capitalizeWords(Jsoup.parse(organisationUnitGroup.getName().toLowerCase()).text()));

        return organisationUnitGroupRepository.save(organisationUnitGroup);
    }

    /**
     * Get all the organisationUnitGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationUnitGroup> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationUnitGroups");
        return organisationUnitGroupRepository.findAll(pageable);
    }


    /**
     * Get one organisationUnitGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationUnitGroup> findOne(Long id) {
        log.debug("Request to get OrganisationUnitGroup : {}", id);
        return organisationUnitGroupRepository.findById(id);
    }

    /**
     * Delete the organisationUnitGroup by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationUnitGroup : {}", id);
        organisationUnitGroupRepository.deleteById(id);
    }
    
    @Override
    public OrganisationUnitGroup findByName(String name) {
    	 log.debug("Request to get OrganisationUnitGroup by name: {}", name);
        return organisationUnitGroupRepository.findByNameIgnoreCase(name);
    }

}
