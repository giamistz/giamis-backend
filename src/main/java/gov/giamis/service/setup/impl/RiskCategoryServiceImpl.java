package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.RiskCategoryService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.RiskCategory;
import gov.giamis.repository.setup.RiskCategoryRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RiskCategory}.
 */
@Service
@Transactional
public class RiskCategoryServiceImpl implements RiskCategoryService {

	private final Logger log = LoggerFactory.getLogger(RiskCategoryServiceImpl.class);

	private final RiskCategoryRepository riskCategoryRepository;

	public RiskCategoryServiceImpl(RiskCategoryRepository riskCategoryRepository) {
		this.riskCategoryRepository = riskCategoryRepository;
	}

	/**
	 * Save a riskCategory.
	 *
	 * @param riskCategory the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public RiskCategory save(RiskCategory riskCategory) {
		log.debug("Request to save RiskCategory : {}", riskCategory);
		riskCategory.setName(Utils.capitalizeWords(Jsoup.parse(riskCategory.getName().toLowerCase()).text()));
		return riskCategoryRepository.save(riskCategory);
	}

	/**
	 * Get all the riskCategories.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<RiskCategory> findAll(Pageable pageable) {
		log.debug("Request to get all RiskCategories");
		return riskCategoryRepository.findAll(pageable);
	}

	/**
	 * Get one riskCategory by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<RiskCategory> findOne(Long id) {
		log.debug("Request to get RiskCategory : {}", id);
		return riskCategoryRepository.findById(id);
	}

	/**
	 * Delete the riskCategory by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete RiskCategory : {}", id);
		riskCategoryRepository.deleteById(id);
	}

	@Override
	public RiskCategory findByName(String name) {
		log.debug("Request to get RiskCategory by name : {}", name);
		return riskCategoryRepository.findByNameIgnoreCase(name);
	}
}
