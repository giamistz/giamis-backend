package gov.giamis.service.setup.impl;

import java.util.Optional;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.repository.setup.OrganisationUnitRepository;
import gov.giamis.service.setup.OrganisationUnitService;
import gov.giamis.service.util.Utils;

/**
 * Service Implementation for managing {@link OrganisationUnit}.
 */
@Service
@Transactional
public class OrganisationUnitServiceImpl implements OrganisationUnitService {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitServiceImpl.class);

    private final OrganisationUnitRepository organisationUnitRepository;
    
    public OrganisationUnitServiceImpl(OrganisationUnitRepository organisationUnitRepository) {
        this.organisationUnitRepository = organisationUnitRepository;
    }

    /**
     * Save a organisationUnit.
     *
     * @param organisationUnit the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationUnit save(OrganisationUnit organisationUnit) {
        log.debug("Request to save OrganisationUnit : {}", organisationUnit);
        organisationUnit.setName(Utils.capitalizeWords(Jsoup.parse(organisationUnit.getName().toLowerCase()).text()));

        return organisationUnitRepository.save(organisationUnit);
    }

    /**
     * Get all the organisationUnits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationUnit> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationUnits");
        return organisationUnitRepository.findAll(pageable);
    }

    /**
     * Get all the organisationUnits with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<OrganisationUnit> findAllWithEagerRelationships(Pageable pageable) {
        return organisationUnitRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one organisationUnit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationUnit> findOne(Long id) {
        log.debug("Request to get OrganisationUnit : {}", id);
        return organisationUnitRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the organisationUnit by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationUnit : {}", id);
        organisationUnitRepository.deleteById(id);
    }

	@Override
	public OrganisationUnit findByName(String name) {
		log.debug("Request to find OrganisationUnit by name : {}", name);
		return organisationUnitRepository.findByName(name);
	}

	@Override
	public void updateUserByOrganisationUnit(Long userId, Long id) {
		log.debug("Request to update User by  OrganisationUnit Id : {}", id);
		 organisationUnitRepository.updateUserOfOrganisationUnitById(userId, id);
	}

	
}
