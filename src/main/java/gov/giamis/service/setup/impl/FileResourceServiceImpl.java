package gov.giamis.service.setup.impl;

import gov.giamis.config.ApplicationProperties;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.FileResourceType;
import gov.giamis.repository.setup.FileResourceRepository;
import gov.giamis.repository.setup.FileResourceTypeRepository;
import gov.giamis.service.setup.FileResourceService;
import gov.giamis.service.util.Utils;
import gov.giamis.web.rest.errors.ResourceNotFoundException;
import org.apache.commons.io.FilenameUtils;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FileResource}.
 */
@Service
@Transactional
public class FileResourceServiceImpl implements FileResourceService {

	private final Logger log = LoggerFactory.getLogger(FileResourceServiceImpl.class);

	private final FileResourceRepository fileResourceRepository;

	private final FileResourceTypeRepository fileResourceTypeRepository;

	private final ApplicationProperties applicationProperties;

	private String fileStorageBaseDir;

	public FileResourceServiceImpl(FileResourceRepository fileResourceRepository,
			FileResourceTypeRepository fileResourceTypeRepository, ApplicationProperties applicationProperties) {
		this.fileResourceRepository = fileResourceRepository;
		this.fileResourceTypeRepository = fileResourceTypeRepository;
		this.applicationProperties = applicationProperties;
	}

	@PostConstruct
	public void init() {
		this.fileStorageBaseDir = applicationProperties.getFileStorageBaseDir();
	}

	/**
	 * Save a fileResource.
	 *
	 * @param fileResource the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public FileResource save(FileResource fileResource) {
		log.debug("Request to save FileResource : {}", fileResource);
		fileResource.setName(Utils.capitalizeWords(Jsoup.parse(fileResource.getName().toLowerCase()).text()));
		return fileResourceRepository.save(fileResource);
	}

	/**
	 * Get all the fileResources.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<FileResource> findAll(Pageable pageable) {
		log.debug("Request to get all FileResources");
		return fileResourceRepository.findAll(pageable);
	}

	/**
	 * Get one fileResource by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<FileResource> findOne(Long id) {
		log.debug("Request to get FileResource : {}", id);
		return fileResourceRepository.findById(id);
	}

	/**
	 * Delete the fileResource by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete FileResource : {}", id);
		FileResource fileResource = fileResourceRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Specified File resource to be deleted not found"));
		File file = new File(fileResource.getPath());
		boolean deleted = file.delete();
		fileResourceRepository.deleteById(id);
	}

	/**
	 * Method to upload file resource to file system and create corresponding file
	 * resource entity
	 *
	 * @param file
	 * @param name
	 * @param fileType
	 * @return
	 * @throws IOException
	 */
	@Override
	public FileResource upload(@NotNull MultipartFile file, String name, String fileType) throws IOException {

	    // get size
		long size = file.getSize();
		//Get content type
		String contentType = file.getContentType();
		//Get extension
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		// Get md5
		String md5 = DigestUtils.md5DigestAsHex(file.getBytes());

        FileResourceType fileResourceType;

		Optional<FileResourceType> mayExist = fileResourceTypeRepository.findByNameIgnoreCase(fileType);
        fileResourceType = mayExist.orElseGet(() -> fileResourceTypeRepository.save(new FileResourceType(
            fileType.toUpperCase(),
            fileType)));

		File directory = getBaseDir(fileResourceType.getName());

		FileResource fileResource = fileResourceRepository.findByNameAndContentMd5AllIgnoreCase(name, md5);
		if (fileResource == null) {
			fileResource = new FileResource(name, contentType, md5, size, null, fileResourceType);
		}

		String filePath = directory.getAbsolutePath().concat("/").concat(name).concat(md5).concat(".")
				.concat(extension);

		Path path = Paths.get(filePath);
		file.transferTo(path);
		fileResource.setPath(filePath);

		return fileResourceRepository.save(fileResource);
	}

	/**
	 * Method to upload file resource to file system and create corresponding file
	 * resource entity
	 *
	 * @param file
	 * @param name
	 * @param fileType
	 * @return
	 * @throws IOException
	 */
	@Override
	public FileResource upload(@NotNull File file, String name, String fileType) throws IOException {

	    // get size
		long size = file.length();
		//Get content type
		String contentType = Files.probeContentType(file.toPath());
		//Get extension
		String extension = FilenameUtils.getExtension(file.getPath());
		// Get md5
		String md5 = DigestUtils.md5DigestAsHex(Files.readAllBytes(file.toPath()));

        FileResourceType fileResourceType;

		Optional<FileResourceType> mayExist = fileResourceTypeRepository.findByNameIgnoreCase(fileType);
        fileResourceType = mayExist.orElseGet(() -> fileResourceTypeRepository.save(new FileResourceType(
            fileType.toUpperCase(),
            fileType)));

		File directory = getBaseDir(fileResourceType.getName());

		FileResource fileResource = fileResourceRepository.findByNameAndContentMd5AllIgnoreCase(name, md5);
		if (fileResource == null) {
			fileResource = new FileResource(name, contentType, md5, size, null, fileResourceType);
		}

		String filePath = directory.getAbsolutePath().concat("/").concat(name).concat(md5).concat(".")
				.concat(extension);

		Path path = Paths.get(filePath);

        Files.copy(file.toPath(), path);
		fileResource.setPath(filePath);

		return fileResourceRepository.save(fileResource);
	}

	private File getBaseDir(String folder) {

		if (fileStorageBaseDir == null) {
			throw new ResourceNotFoundException(
					"No file storage baseDir specified; " + "Please add to application config before upload");
		}

		boolean mkBase = true;

		File baseDir = new File(fileStorageBaseDir);
		if (!baseDir.exists()) {
			mkBase = baseDir.mkdir();
		}
		if (!mkBase) {
			throw new ResourceNotFoundException("Cannot create Storage directory, "
					+ "invalid storage directory specified; " + "Correct baseDir in application config");
		}

		File container = new File(baseDir + "/" + folder + "/");
		if (!container.exists()) {
			container.mkdir();
		}
		return container;
	}

	@Override
	public FileResource findByName(String name) {
		log.debug("Request to get FileResource by name : {}", name);
		return fileResourceRepository.findByNameAllIgnoreCase(name);
	}

    @Override
    public int setAssigned(Long id, Boolean isAssigned) {
        return fileResourceRepository.setAssigned(id, isAssigned);
    }

}
