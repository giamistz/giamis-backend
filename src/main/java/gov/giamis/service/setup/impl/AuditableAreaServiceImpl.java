package gov.giamis.service.setup.impl;

import gov.giamis.service.setup.AuditableAreaService;
import gov.giamis.service.util.Utils;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.repository.setup.AuditableAreaRepository;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AuditableArea}.
 */
@Service
@Transactional
public class AuditableAreaServiceImpl implements AuditableAreaService {

    private final Logger log = LoggerFactory.getLogger(AuditableAreaServiceImpl.class);

    private final AuditableAreaRepository auditableAreaRepository;

    public AuditableAreaServiceImpl(AuditableAreaRepository auditableAreaRepository) {
        this.auditableAreaRepository = auditableAreaRepository;
    }

    /**
     * Save a auditableArea.
     *
     * @param auditableArea the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AuditableArea save(AuditableArea auditableArea) {
        log.debug("Request to save AuditableArea : {}", auditableArea);
        auditableArea.setName(Utils.capitalizeWords(Jsoup.parse(auditableArea.getName().toLowerCase()).text()));
        return auditableAreaRepository.save(auditableArea);
    }

    /**
     * Get all the auditableAreas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AuditableArea> findAll(Pageable pageable) {
        log.debug("Request to get all AuditableAreas");
        return auditableAreaRepository.findAll(pageable);
    }

    /**
     * Get all the auditableAreas with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<AuditableArea> findAllWithEagerRelationships(Pageable pageable) {
        return auditableAreaRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one auditableArea by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AuditableArea> findOne(Long id) {
        log.debug("Request to get AuditableArea : {}", id);
        return auditableAreaRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the auditableArea by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuditableArea : {}", id);
        auditableAreaRepository.deleteById(id);
    }
    
    @Override
    public AuditableArea findByName(String name) {
    	  log.debug("Request to get AuditableArea by name: {}", name);
        return auditableAreaRepository.findByNameAllIgnoreCase(name);
    }
}
