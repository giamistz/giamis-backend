package gov.giamis.service.setup;

import gov.giamis.domain.setup.GfsCode;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link GfsCode}.
 */
public interface GfsCodeService {

    /**
     * Save a gfsCode.
     *
     * @param gfsCode the entity to save.
     * @return the persisted entity.
     */
    GfsCode save(GfsCode gfsCode);

    /**
     * Get all the gfsCodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GfsCode> findAll(Pageable pageable);


    /**
     * Get the "id" gfsCode.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GfsCode> findOne(Long id);

    /**
     * Delete the "id" gfsCode.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    GfsCode findByCode(String code);
}
