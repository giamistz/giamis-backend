package gov.giamis.service.setup;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.ProfessionalTraining_;
import gov.giamis.domain.setup.User_;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import gov.giamis.domain.setup.ProfessionalTraining;
import gov.giamis.repository.setup.ProfessionalTrainingRepository;
import gov.giamis.service.setup.dto.ProfessionalTrainingCriteria;

/**
 * Service for executing complex queries for {@link ProfessionalTraining} entities in the database.
 * The main input is a {@link ProfessionalTrainingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProfessionalTraining} or a {@link Page} of {@link ProfessionalTraining} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProfessionalTrainingQueryService extends QueryService<ProfessionalTraining> {

    private final Logger log = LoggerFactory.getLogger(ProfessionalTrainingQueryService.class);

    private final ProfessionalTrainingRepository professionalTrainingRepository;

    public ProfessionalTrainingQueryService(ProfessionalTrainingRepository professionalTrainingRepository) {
        this.professionalTrainingRepository = professionalTrainingRepository;
    }

    /**
     * Return a {@link List} of {@link ProfessionalTraining} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProfessionalTraining> findByCriteria(ProfessionalTrainingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProfessionalTraining> specification = createSpecification(criteria);
        return professionalTrainingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ProfessionalTraining} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProfessionalTraining> findByCriteria(ProfessionalTrainingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProfessionalTraining> specification = createSpecification(criteria);
        return professionalTrainingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProfessionalTrainingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProfessionalTraining> specification = createSpecification(criteria);
        return professionalTrainingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<ProfessionalTraining> createSpecification(ProfessionalTrainingCriteria criteria) {
        Specification<ProfessionalTraining> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), ProfessionalTraining_.id));
            }
            
            if (criteria.getTitle() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getTitle(), ProfessionalTraining_.title));
            }
            
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getStartDate(), ProfessionalTraining_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getEndDate(), ProfessionalTraining_.endDate));
            }
            
            
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(ProfessionalTraining_.user, JoinType.LEFT).get(User_.id)));
            }
            
        }
        return specification;
    }
}
