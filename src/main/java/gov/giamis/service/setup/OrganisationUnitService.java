package gov.giamis.service.setup;

import gov.giamis.domain.setup.OrganisationUnit;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * Service Interface for managing {@link OrganisationUnit}.
 */
public interface OrganisationUnitService {

    /**
     * Save a organisationUnit.
     *
     * @param organisationUnit the entity to save.
     * @return the persisted entity.
     */
    OrganisationUnit save(OrganisationUnit organisationUnit);

    /**
     * Get all the organisationUnits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationUnit> findAll(Pageable pageable);

    /**
     * Get all the organisationUnits with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<OrganisationUnit> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" organisationUnit.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationUnit> findOne(Long id);

    /**
     * Delete the "id" organisationUnit.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    OrganisationUnit findByName(String name);
    
    void updateUserByOrganisationUnit(Long userId,Long id);

}
