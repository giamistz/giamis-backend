package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.GfsCode_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.GfsCode;
import gov.giamis.repository.setup.GfsCodeRepository;
import gov.giamis.service.setup.dto.GfsCodeCriteria;

/**
 * Service for executing complex queries for {@link GfsCode} entities in the database.
 * The main input is a {@link GfsCodeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GfsCode} or a {@link Page} of {@link GfsCode} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GfsCodeQueryService extends QueryService<GfsCode> {

    private final Logger log = LoggerFactory.getLogger(GfsCodeQueryService.class);

    private final GfsCodeRepository gfsCodeRepository;

    public GfsCodeQueryService(GfsCodeRepository gfsCodeRepository) {
        this.gfsCodeRepository = gfsCodeRepository;
    }

    /**
     * Return a {@link List} of {@link GfsCode} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GfsCode> findByCriteria(GfsCodeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<GfsCode> specification = createSpecification(criteria);
        return gfsCodeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link GfsCode} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GfsCode> findByCriteria(GfsCodeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<GfsCode> specification = createSpecification(criteria);
        return gfsCodeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GfsCodeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<GfsCode> specification = createSpecification(criteria);
        return gfsCodeRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<GfsCode> createSpecification(GfsCodeCriteria criteria) {
        Specification<GfsCode> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), GfsCode_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), GfsCode_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), GfsCode_.description));
            }
        }
        return specification;
    }
}
