package gov.giamis.service.setup;

import gov.giamis.domain.setup.Quarter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Quarter}.
 */
public interface QuarterService {

	/**
	 * Save a quarter.
	 *
	 * @param quarter the entity to save.
	 * @return the persisted entity.
	 */
	Quarter save(Quarter quarter);

	/**
	 * Get all the quarters.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Quarter> findAll(Pageable pageable);

	/**
	 * Get the "id" quarter.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Quarter> findOne(Long id);

	/**
	 * Delete the "id" quarter.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	Quarter findByName(String name);
	
    void changeStatus(Integer status,Quarter quarter);
	
	void changeOtherStatus(Integer status,Quarter quarter);
	
	Quarter findStatusById(Long id);
}
