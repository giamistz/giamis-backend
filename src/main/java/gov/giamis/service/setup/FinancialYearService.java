package gov.giamis.service.setup;

import gov.giamis.domain.setup.FinancialYear;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link FinancialYear}.
 */
public interface FinancialYearService {

	/**
	 * Save a financial year service.
	 *
	 * @param financialYear the entity to save.
	 * @return the persisted entity.
	 */
	FinancialYear save(FinancialYear financialYear);

	/**
	 * Get all the inancial year service.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<FinancialYear> findAll(Pageable pageable);

	/**
	 * Get the "id" inancial year.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<FinancialYear> findOne(Long id);

	/**
	 * Delete the "id" financial year.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	FinancialYear findByName(String name);

	void changeStatus(Integer status,FinancialYear financialYear);

	void changeOtherStatus(Integer status,FinancialYear financialYear);

	FinancialYear findStatusById(Long id);

	FinancialYear findByStatus(Integer status);

	FinancialYear findCurrent();
}
