package gov.giamis.service.setup;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import gov.giamis.domain.setup.UserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.User;
import gov.giamis.repository.setup.AuthorityRepository;
import gov.giamis.repository.setup.UserProfileRepository;
import gov.giamis.repository.setup.UserRepository;
import gov.giamis.security.SecurityUtils;
import gov.giamis.service.setup.dto.UserDTO;
import gov.giamis.service.util.RandomUtil;
import gov.giamis.web.rest.errors.EmailAlreadyUsedException;
import gov.giamis.web.rest.errors.InvalidPasswordException;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final String defaultPassword = "1234"; //Or use RandomUtil.generatePassword()

    private final UserRepository userRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private RoleService roleService;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    public UserService(UserRepository userRepository,
                       PasswordEncoder passwordEncoder,
                       AuthorityRepository authorityRepository,
                       CacheManager cacheManager) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                this.clearUserCaches(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail().toLowerCase());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        userRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser){
        if (existingUser.getActivated()) {
             return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLastName(userDTO.getLastName());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setMiddleName(userDTO.getMiddleName());
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setImageUrl(userDTO.getImageUrl());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setAuditor(userDTO.getAuditor());
        if (userDTO.getAuditorProfile() != null) {
            userDTO.getAuditorProfile().setUser(user);
        }
        user.setAuditorProfile(userDTO.getAuditorProfile());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(defaultPassword);
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        user.setAuthorities(userDTO.getAuthorities());
        user.setRoles(userDTO.getRoles());
        user.setOrganisationUnit(userDTO.getOrganisationUnit());
        userRepository.save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}");
        return user;
    }

    /**
     * User Profile update; Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName  last name of user.
     * @param email     email id of user.
     * @param langKey   language key.
     * @param imageUrl  image URL of user.
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByEmailIgnoreCase)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email.toLowerCase());
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
    		return Optional.of(userRepository
    	            .findById(userDTO.getId()))
    	            .filter(Optional::isPresent)
    	            .map(Optional::get)
    	            .map(user -> {
    	                this.clearUserCaches(user);
    	                user.setEmail(userDTO.getEmail());
    	                user.setFirstName(userDTO.getFirstName());
    	                user.setLastName(userDTO.getLastName());
    	                user.setMiddleName(userDTO.getMiddleName());
    	                user.setPhoneNumber(userDTO.getPhoneNumber());
    	                user.setImageUrl(userDTO.getImageUrl());
    	                user.setActivated(userDTO.isActivated());
    	                user.setLangKey(userDTO.getLangKey());
    	                user.setOrganisationUnit(userDTO.getOrganisationUnit());
    	                user.setRoles(userDTO.getRoles());
    	                user.setAuditor(userDTO.getAuditor());
    	                if (userDTO.getAuditorProfile() != null) {
    	                    userDTO.getAuditorProfile().setUser(user);
                        }
    	                user.setAuditorProfile(userDTO.getAuditorProfile());
    	                this.clearUserCaches(user);
    	                log.debug("Changed Information for User: {}", user);
    	                return userRepository.save(user);
    	            })
    	            .map(UserDTO::new);


    }

    public void deleteUser(Long id) {
        userRepository.findById(id).ifPresent(user -> {
            userRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByEmailIgnoreCase)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<User> getAllManagedUsers(Pageable pageable) {
        User user = getUserWithAuthorities().get();
        if (user.getOrganisationUnit() == null) {
            return userRepository.findAllByEmailIgnoreCase(user.getEmail(), pageable);
        }
        return userRepository.findAllByOrganisationUnit_IdIn(
            userOrgUnits(user.getOrganisationUnit().getId()), pageable);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByEmailIgnoreCase(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesById(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByEmailIgnoreCase);
    }

    @Transactional(readOnly = true)
    public Optional<User> getCurrent() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByEmailIgnoreCase);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getEmail());
                userRepository.delete(user);
                this.clearUserCaches(user);
            });
    }

    /**
     * Gets a list of all the authorities.
     * @return a list of all the authorities.
     */
    public List<String> getAuthorities() {
    	log.debug("Request to get all Authorities : {}");
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }


    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }

    public User findByEmail(String email) {
    	log.debug("Request to find User by Email : {}", email);
    return userRepository.findByEmail(email);
    }

    public void updateUserOrganisationUnitByEmail(OrganisationUnit OrganisationUnit,String email) {
    	log.debug("Request to update Organisation Unit by Email : {}", email);
    	  userRepository.updateUserOrganisationUnitByEmail(OrganisationUnit,email);
    }

	public User save(User user) {
		log.debug("Request to save User : {}", user);
		return userRepository.save(user);
	}

	public Optional<User> findById(Long id){
		log.debug("Request to find User by ID : {}", id);
		return userRepository.findById(id);
	}

	public User findUserById(Long id){
		log.debug("Request to find User by ID : {}", id);
		return userRepository.findUserById(id);
	}

	public List<User> findUsersByRole(String name) {
		log.debug("Request to find User by role : {}", name);
		return userRepository.findByRoles_NameIgnoreCase(name);
	}

	public List<User> getUserByEngagementPlannedAuditee(){
		log.debug("Request to Get all Users by Using Engagement Auditee Planned : {}");
		return userRepository.getUserByEngagementPlannedAuditee();
	}

	public List<User> findAll(){
		log.debug("Request to Get all Users : {}");
		return userRepository.findAll();
	}

	public List<Long> userOrgUnits(Long organisationUnitId) {
        Cache.ValueWrapper cache = cacheManager
            .getCache(UserRepository.ORGUNIT_BY_PARENTS)
            .get(organisationUnitId);

        if (cache != null) {
            return  (List<Long>) cache.get();
        } else {
            List<Long> orgUnits = userRepository.getUserOrganisations(organisationUnitId);
            cacheManager.getCache(UserRepository.ORGUNIT_BY_PARENTS)
                .put(organisationUnitId, orgUnits);
            return orgUnits;
        }
    }
}
