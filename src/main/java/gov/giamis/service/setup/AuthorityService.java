package gov.giamis.service.setup;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.setup.Authority;
/**
 * Service Interface for managing {@link Authority}.
 */
public interface AuthorityService {

    /**
     * Save a authority.
     *
     * @param authority the entity to save.
     * @return the persisted entity.
     */
    Authority save(Authority authority);

    /**
     * Get all the authoritys.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Authority> findAll(Pageable pageable);
    
    /**
     * Get the "id" authority.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Authority> findOne(Long id);

    /**
     * Delete the "id" authority.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

	List<Authority> findAll();
	
	List<Authority> findAuthorityOrderByName();
	
	List<Authority> findDistinctAuthority();
	
	List<Authority> getAllAuthorities(String getResource);
	
	List<Authority> getAllMenuAuthorities(String getResource);
	
	Set<Authority> getAllAuthorities(Long id);

	Set<Authority> getAllAuthoritiesByEmail(String email);
}
