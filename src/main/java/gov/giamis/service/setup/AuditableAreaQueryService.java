package gov.giamis.service.setup;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.AuditableArea_;
import gov.giamis.domain.setup.OrganisationUnitGroup_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.repository.setup.AuditableAreaRepository;
import gov.giamis.service.setup.dto.AuditableAreaCriteria;

/**
 * Service for executing complex queries for {@link AuditableArea} entities in the database.
 * The main input is a {@link AuditableAreaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditableArea} or a {@link Page} of {@link AuditableArea} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditableAreaQueryService extends QueryService<AuditableArea> {

    private final Logger log = LoggerFactory.getLogger(AuditableAreaQueryService.class);

    private final AuditableAreaRepository auditableAreaRepository;

    public AuditableAreaQueryService(AuditableAreaRepository auditableAreaRepository) {
        this.auditableAreaRepository = auditableAreaRepository;
    }

    /**
     * Return a {@link List} of {@link AuditableArea} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditableArea> findByCriteria(AuditableAreaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditableArea> specification = createSpecification(criteria);
        return auditableAreaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AuditableArea} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditableArea> findByCriteria(AuditableAreaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditableArea> specification = createSpecification(criteria);
        return auditableAreaRepository.findAllWithEagerRelationships(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditableAreaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditableArea> specification = createSpecification(criteria);
        return auditableAreaRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<AuditableArea> createSpecification(AuditableAreaCriteria criteria) {
        Specification<AuditableArea> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), AuditableArea_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), AuditableArea_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), AuditableArea_.name));
            }
            if (criteria.getOrganisationUnitGroupId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitGroupId(),
                    root -> root.join(AuditableArea_.organisationUnitGroups, JoinType.LEFT)
                        .get(OrganisationUnitGroup_.id)));
            }
        }
        return specification;
    }
}
