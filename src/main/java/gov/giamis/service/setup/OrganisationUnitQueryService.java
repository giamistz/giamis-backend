package gov.giamis.service.setup;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.repository.setup.OrganisationUnitRepository;
import gov.giamis.service.setup.dto.OrganisationUnitCriteria;

/**
 * Service for executing complex queries for {@link OrganisationUnit} entities in the database.
 * The main input is a {@link OrganisationUnitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrganisationUnit} or
 * a {@link Page} of {@link OrganisationUnit} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrganisationUnitQueryService extends QueryService<OrganisationUnit> {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitQueryService.class);

    private final OrganisationUnitRepository organisationUnitRepository;

    public OrganisationUnitQueryService(OrganisationUnitRepository organisationUnitRepository) {
        this.organisationUnitRepository = organisationUnitRepository;
    }

    /**
     * Return a {@link List} of {@link OrganisationUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrganisationUnit> findByCriteria(OrganisationUnitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrganisationUnit> specification = createSpecification(criteria);
        return organisationUnitRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OrganisationUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrganisationUnit> findByCriteria(
        OrganisationUnitCriteria criteria,
        Pageable page) {

        log.debug("find by criteria : {}, page: {}", criteria, page);
        if (criteria.getParentOrganisationUnitId() != null) {
           Long id = criteria.getParentOrganisationUnitId().getEquals();
           return organisationUnitRepository.findByParentOrUnit(id, page);
        }
        final Specification<OrganisationUnit> specification = createSpecification(criteria);
        return organisationUnitRepository.findAllWithEagerRelationships(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrganisationUnitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrganisationUnit> specification = createSpecification(criteria);
        return organisationUnitRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OrganisationUnit> createSpecification(OrganisationUnitCriteria criteria) {
        Specification<OrganisationUnit> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), OrganisationUnit_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), OrganisationUnit_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), OrganisationUnit_.name));
            }
            if (criteria.getParentOrganisationUnitId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getParentOrganisationUnitId(),
                    root -> root.join(OrganisationUnit_.parentOrganisationUnit, JoinType.LEFT)
                        .get(OrganisationUnit_.id)));
            }
            if (criteria.getOrganisationUnitGroupId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitGroupId(),
                    root -> root.join(OrganisationUnit_.organisationUnitGroups, JoinType.LEFT)
                        .get(OrganisationUnitGroup_.id)));
            }
            if (criteria.getOrganisationUnitGroupSetId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitGroupSetId(),
                    root -> root.join(OrganisationUnit_.organisationUnitGroups, JoinType.LEFT)
                        .join(OrganisationUnitGroup_.organisationUnitGroupSet, JoinType.LEFT)
                        .get(OrganisationUnitGroupSet_.id)));
            }
            if (criteria.getOrganisationUnitLevelId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitLevelId(),
                    root -> root.join(OrganisationUnit_.organisationUnitLevel, JoinType.LEFT)
                        .get(OrganisationUnitLevel_.id)));
            }
        }
        return specification;
    }
}
