package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.FileResourceType_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.FileResourceType;
import gov.giamis.repository.setup.FileResourceTypeRepository;
import gov.giamis.service.setup.dto.FileResourceTypeCriteria;

/**
 * Service for executing complex queries for {@link FileResourceType} entities in the database.
 * The main input is a {@link FileResourceTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FileResourceType} or a {@link Page} of {@link FileResourceType} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FileResourceTypeQueryService extends QueryService<FileResourceType> {

    private final Logger log = LoggerFactory.getLogger(FileResourceTypeQueryService.class);

    private final FileResourceTypeRepository fileResourceTypeRepository;

    public FileResourceTypeQueryService(FileResourceTypeRepository fileResourceTypeRepository) {
        this.fileResourceTypeRepository = fileResourceTypeRepository;
    }

    /**
     * Return a {@link List} of {@link FileResourceType} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FileResourceType> findByCriteria(FileResourceTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FileResourceType> specification = createSpecification(criteria);
        return fileResourceTypeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FileResourceType} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FileResourceType> findByCriteria(FileResourceTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FileResourceType> specification = createSpecification(criteria);
        return fileResourceTypeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FileResourceTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FileResourceType> specification = createSpecification(criteria);
        return fileResourceTypeRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<FileResourceType> createSpecification(FileResourceTypeCriteria criteria) {
        Specification<FileResourceType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), FileResourceType_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), FileResourceType_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), FileResourceType_.code));
            }
        }
        return specification;
    }
}
