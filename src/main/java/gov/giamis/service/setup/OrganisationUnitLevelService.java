package gov.giamis.service.setup;

import gov.giamis.domain.setup.OrganisationUnitLevel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link OrganisationUnitLevel}.
 */
public interface OrganisationUnitLevelService {

    /**
     * Save a organisationUnitLevel.
     *
     * @param organisationUnitLevel the entity to save.
     * @return the persisted entity.
     */
    OrganisationUnitLevel save(OrganisationUnitLevel organisationUnitLevel);

    /**
     * Get all the organisationUnitLevels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationUnitLevel> findAll(Pageable pageable);


    /**
     * Get the "id" organisationUnitLevel.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationUnitLevel> findOne(Long id);

    /**
     * Delete the "id" organisationUnitLevel.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    OrganisationUnitLevel findByName(String name);
}
