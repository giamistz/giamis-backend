package gov.giamis.service.setup;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.setup.ControlType;

import java.util.Optional;

/**
 * Service Interface for managing {@link ControlType}.
 */
public interface ControlTypeService {

    /**
     * Save a controlType.
     *
     * @param controlType the entity to save.
     * @return the persisted entity.
     */
    ControlType save(ControlType controlType);

    /**
     * Get all the controlTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ControlType> findAll(Pageable pageable);


    /**
     * Get the "id" controlType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ControlType> findOne(Long id);

    /**
     * Delete the "id" controlType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    ControlType findByName(String name);
}
