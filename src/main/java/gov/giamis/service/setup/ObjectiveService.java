package gov.giamis.service.setup;

import gov.giamis.domain.setup.Objective;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Objective}.
 */
public interface ObjectiveService {

	/**
	 * Save a objective.
	 *
	 * @param objective the entity to save.
	 * @return the persisted entity.
	 */
	Objective save(Objective objective);

	/**
	 * Get all the objectives.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Objective> findAll(Pageable pageable);

	/**
	 * Get the "id" objective.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Objective> findOne(Long id);

	/**
	 * Delete the "id" objective.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	Objective findByDescription(String description);
}
