package gov.giamis.service.setup;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import gov.giamis.domain.setup.MenuItem;

/**
 * Service Interface for managing {@link MenuItem}.
 */
public interface MenuItemService {

    /**
     * Save a menuItem.
     *
     * @param menuItem the entity to save.
     * @return the persisted entity.
     */
    MenuItem save(MenuItem menuItem);

    /**
     * Get all the menuItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MenuItem> findAll(Pageable pageable);

    /**
     * Get all the menuItems with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<MenuItem> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" menuItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MenuItem> findOne(Long id);

    /**
     * Delete the "id" menuItem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    MenuItem findByName(String name);
    
    MenuItem findMenuItemById(Long id);

    List<MenuItem> findAllMenuItems();
  
    List<MenuItem> getAllMenuItemsByGroup(Long menuGroupId);

    List<MenuGroup> getWithItems(Map<String, List<Long>> groupItemIds);

    List<MenuItem> getByAuthorities(List<Long> authorities);

    List<MenuItem> getWithNoGroupByAuthorities(List<Long> authorities);
}
