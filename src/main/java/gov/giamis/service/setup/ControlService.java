package gov.giamis.service.setup;

import gov.giamis.domain.setup.Control;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Control}.
 */
public interface ControlService {

    /**
     * Save a control.
     *
     * @param control the entity to save.
     * @return the persisted entity.
     */
    Control save(Control control);

    /**
     * Get all the controls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Control> findAll(Pageable pageable);


    /**
     * Get the "id" control.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Control> findOne(Long id);

    /**
     * Delete the "id" control.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
