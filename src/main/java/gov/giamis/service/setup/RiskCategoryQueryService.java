package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.RiskCategory_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.RiskCategory;
import gov.giamis.repository.setup.RiskCategoryRepository;
import gov.giamis.service.setup.dto.RiskCategoryCriteria;

/**
 * Service for executing complex queries for {@link RiskCategory} entities in the database.
 * The main input is a {@link RiskCategoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskCategory} or a {@link Page} of {@link RiskCategory} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskCategoryQueryService extends QueryService<RiskCategory> {

    private final Logger log = LoggerFactory.getLogger(RiskCategoryQueryService.class);

    private final RiskCategoryRepository riskCategoryRepository;

    public RiskCategoryQueryService(RiskCategoryRepository riskCategoryRepository) {
        this.riskCategoryRepository = riskCategoryRepository;
    }

    /**
     * Return a {@link List} of {@link RiskCategory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskCategory> findByCriteria(RiskCategoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskCategory> specification = createSpecification(criteria);
        return riskCategoryRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskCategory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskCategory> findByCriteria(RiskCategoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskCategory> specification = createSpecification(criteria);
        return riskCategoryRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskCategoryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskCategory> specification = createSpecification(criteria);
        return riskCategoryRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskCategory> createSpecification(RiskCategoryCriteria criteria) {
        Specification<RiskCategory> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), RiskCategory_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), RiskCategory_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), RiskCategory_.name));
            }
        }
        return specification;
    }
}
