package gov.giamis.service.setup;

import gov.giamis.domain.setup.MenuGroup;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link MenuGroup}.
 */
public interface MenuGroupService {

    /**
     * Save a menuGroup.
     *
     * @param menuGroup the entity to save.
     * @return the persisted entity.
     */
    MenuGroup save(MenuGroup menuGroup);

    /**
     * Get all the menuGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MenuGroup> findAll(Pageable pageable);


    /**
     * Get the "id" menuGroup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MenuGroup> findOne(Long id);

    /**
     * Delete the "id" menuGroup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    MenuGroup findByName(String name);
    
    List<MenuGroup> getMenuGroups(Long id);
    
    List<MenuGroup> getMenuGroupsNoParent(Long id);
    
    List<MenuGroup> findByParentMenuGroup(Long id);
    
    List<MenuGroup> findMenuGroupById(Long id);
    
    List<MenuGroup> getAllMenuItems(Long id);
}
