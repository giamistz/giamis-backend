package gov.giamis.service.setup;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.User;
import gov.giamis.repository.setup.OrganisationUnitRepository;
import gov.giamis.repository.setup.UserRepository;
import gov.giamis.security.SecurityUtils;
import gov.giamis.service.setup.dto.UserOrganisationUnit;
import gov.giamis.web.rest.setup.AccountResource;


@Service
public class UserOrganisationUnitService {

    private final OrganisationUnitRepository organisationUnitRepository;

    private final UserRepository userRepository;

    @Autowired
    public UserOrganisationUnitService(
        OrganisationUnitRepository organisationUnitRepository,
        UserRepository userRepository) {
        this.organisationUnitRepository = organisationUnitRepository;
        this.userRepository = userRepository;
    }

    public UserOrganisationUnit getCurrentUserOrgUnit() {
        String userEmail = SecurityUtils.getCurrentUserLogin().orElseThrow(()
            -> new AccountResource.AccountResourceException("Current user login not found"));

        User user = userRepository.findOneByEmailIgnoreCase(userEmail)
            .orElseThrow(() -> new AccountResource.AccountResourceException("User not found"));

        UserOrganisationUnit userOrganisationUnit;

        if (user.getOrganisationUnit() == null) {
            return null;
        }
        userOrganisationUnit = new UserOrganisationUnit(user.getOrganisationUnit());
        userOrganisationUnit.setChildren(getOrgUnitChildren(user.getOrganisationUnit().getId()));

        return  userOrganisationUnit;
    }

    public List<UserOrganisationUnit> getOrgUnitChildren(Long parentId) {
        return organisationUnitRepository.findByParentOrganisationUnitId(parentId)
            .stream()
            .map(UserOrganisationUnit::new)
            .collect(Collectors.toList());
    }
    
    public OrganisationUnit getCurrentOrganisationUnit(){
    	String userEmail = SecurityUtils.getCurrentUserLogin().orElseThrow(()
                -> new AccountResource.AccountResourceException("Current user login not found"));

            User user = userRepository.findOneByEmailIgnoreCase(userEmail)
                .orElseThrow(() -> new AccountResource.AccountResourceException("User not found"));
            OrganisationUnit organisationUnit=user.getOrganisationUnit();
            
            return organisationUnit;
    }
}
