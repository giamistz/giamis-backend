package gov.giamis.service.setup;

import gov.giamis.domain.setup.FileResourceType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link FileResourceType}.
 */
public interface FileResourceTypeService {

    /**
     * Save a fileResourceType.
     *
     * @param fileResourceType the entity to save.
     * @return the persisted entity.
     */
    FileResourceType save(FileResourceType fileResourceType);

    /**
     * Get all the fileResourceTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FileResourceType> findAll(Pageable pageable);


    /**
     * Get the "id" fileResourceType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FileResourceType> findOne(Long id);

    /**
     * Delete the "id" fileResourceType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    FileResourceType findByName(String name);
}
