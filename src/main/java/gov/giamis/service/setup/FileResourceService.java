package gov.giamis.service.setup;

import gov.giamis.domain.setup.FileResource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Service Interface for managing {@link FileResource}.
 */
public interface FileResourceService {

	/**
	 * Save a fileResource.
	 *
	 * @param fileResource the entity to save.
	 * @return the persisted entity.
	 */
	FileResource save(FileResource fileResource);

	/**
	 * Get all the fileResources.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<FileResource> findAll(Pageable pageable);

	/**
	 * Get the "id" fileResource.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<FileResource> findOne(Long id);

	/**
	 * Delete the "id" fileResource.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	FileResource upload(MultipartFile file, String name, String fileType) throws IOException;

	FileResource upload(File file, String name, String fileType) throws IOException;

	FileResource findByName(String name);

	int setAssigned(Long id, Boolean isAssigned);

}
