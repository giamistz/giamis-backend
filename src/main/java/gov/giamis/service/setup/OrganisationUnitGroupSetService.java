package gov.giamis.service.setup;

import gov.giamis.domain.setup.OrganisationUnitGroupSet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link OrganisationUnitGroupSet}.
 */
public interface OrganisationUnitGroupSetService {

    /**
     * Save a organisationUnitGroupSet.
     *
     * @param organisationUnitGroupSet the entity to save.
     * @return the persisted entity.
     */
    OrganisationUnitGroupSet save(OrganisationUnitGroupSet organisationUnitGroupSet);

    /**
     * Get all the organisationUnitGroupSets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationUnitGroupSet> findAll(Pageable pageable);


    /**
     * Get the "id" organisationUnitGroupSet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationUnitGroupSet> findOne(Long id);

    /**
     * Delete the "id" organisationUnitGroupSet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    OrganisationUnitGroupSet findByName(String name);
}
