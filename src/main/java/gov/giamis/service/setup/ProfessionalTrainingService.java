package gov.giamis.service.setup;
import gov.giamis.domain.setup.ProfessionalTraining;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ProfessionalTraining}.
 */
public interface ProfessionalTrainingService {

    /**
     * Save a professionalTraining.
     *
     * @param professionalTraining the entity to save.
     * @return the persisted entity.
     */
    ProfessionalTraining save(ProfessionalTraining professionalTraining);

    /**
     * Get all the professionalTrainings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProfessionalTraining> findAll(Pageable pageable);


    /**
     * Get the "id" professionalTraining.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProfessionalTraining> findOne(Long id);

    /**
     * Delete the "id" professionalTraining.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    ProfessionalTraining findByUser(Long id);
}
