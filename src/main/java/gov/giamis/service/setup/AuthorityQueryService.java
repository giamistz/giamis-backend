package gov.giamis.service.setup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.Authority_;
import gov.giamis.repository.setup.AuthorityRepository;
import gov.giamis.service.setup.dto.AuthorityCriteria;
import io.github.jhipster.service.QueryService;

@Service
@Transactional
public class AuthorityQueryService extends QueryService<Authority> {

    private final Logger log = LoggerFactory.getLogger(AuthorityQueryService.class);

    private final AuthorityRepository authorityRepository;

    public AuthorityQueryService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public Page<Authority> findByCriteria(AuthorityCriteria criteria, Pageable pageable) {
        log.debug("Get authorities by criteria : {}" , criteria);
        final Specification<Authority> specification = createSpecification(criteria);
        return authorityRepository.findAll(specification, pageable);
    }

    protected Specification<Authority> createSpecification(AuthorityCriteria criteria) {
        Specification<Authority> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Authority_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildSpecification(criteria.getName(), Authority_.name));
            }
            
            if (criteria.getResource() != null) {
                specification = specification.and(buildSpecification(criteria.getResource(), Authority_.resource));
            }
            
            if (criteria.getAction() != null) {
                specification = specification.and(buildSpecification(criteria.getAction(), Authority_.action));
            }
        }

        return specification;
    }

}
