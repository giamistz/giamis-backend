package gov.giamis.service.setup;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gov.giamis.domain.engagement.Engagement;
import gov.giamis.repository.engagement.EngagementRepository;
import gov.giamis.repository.setup.OrganisationUnitRepository;
import gov.giamis.service.setup.dto.EngagementOrganisationUnit;
import gov.giamis.web.rest.errors.BadRequestAlertException;


@Service
public class EngagementOrganisationUnitService {

    private final OrganisationUnitRepository organisationUnitRepository;

    private final EngagementRepository engagementRepository;

    @Autowired
    public EngagementOrganisationUnitService(
        OrganisationUnitRepository organisationUnitRepository,
        EngagementRepository engagementRepository) {
        this.organisationUnitRepository = organisationUnitRepository;
        this.engagementRepository = engagementRepository;
    }

    public EngagementOrganisationUnit getCurrentEngagementOrgUnit(Long id) {
        Engagement engagement = engagementRepository.findByOrganisationUnit_Id(id);
		if (engagement.getOrganisationUnit() == null) {
			throw new BadRequestAlertException("Engagement with Organisation Unit with ID: "
                + id + " not found",
					"", "id not exists");
		}

        EngagementOrganisationUnit userOrganisationUnit;

        if (engagement.getOrganisationUnit() == null) {
            return null;
        }
        userOrganisationUnit = new EngagementOrganisationUnit(engagement.getOrganisationUnit());
        userOrganisationUnit.setChildren(getOrgUnitChildren(engagement.getOrganisationUnit().getId()));

        return  userOrganisationUnit;
    }

    public List<EngagementOrganisationUnit> getOrgUnitChildren(Long parentId) {
        return organisationUnitRepository.findByParentOrganisationUnitId(parentId)
            .stream()
            .map(EngagementOrganisationUnit::new)
            .collect(Collectors.toList());
    }
}
