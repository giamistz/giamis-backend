package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.OrganisationUnitLevel_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.OrganisationUnitLevel;
import gov.giamis.repository.setup.OrganisationUnitLevelRepository;
import gov.giamis.service.setup.dto.OrganisationUnitLevelCriteria;

/**
 * Service for executing complex queries for {@link OrganisationUnitLevel} entities in the database.
 * The main input is a {@link OrganisationUnitLevelCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrganisationUnitLevel} or
 * a {@link Page} of {@link OrganisationUnitLevel} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrganisationUnitLevelQueryService extends QueryService<OrganisationUnitLevel> {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitLevelQueryService.class);

    private final OrganisationUnitLevelRepository organisationUnitLevelRepository;

    public OrganisationUnitLevelQueryService(OrganisationUnitLevelRepository organisationUnitLevelRepository) {
        this.organisationUnitLevelRepository = organisationUnitLevelRepository;
    }

    /**
     * Return a {@link List} of {@link OrganisationUnitLevel} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrganisationUnitLevel> findByCriteria(OrganisationUnitLevelCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrganisationUnitLevel> specification = createSpecification(criteria);
        return organisationUnitLevelRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OrganisationUnitLevel} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrganisationUnitLevel> findByCriteria(OrganisationUnitLevelCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrganisationUnitLevel> specification = createSpecification(criteria);
        return organisationUnitLevelRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrganisationUnitLevelCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrganisationUnitLevel> specification = createSpecification(criteria);
        return organisationUnitLevelRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<OrganisationUnitLevel> createSpecification(OrganisationUnitLevelCriteria criteria) {
        Specification<OrganisationUnitLevel> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OrganisationUnitLevel_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), OrganisationUnitLevel_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), OrganisationUnitLevel_.name));
            }
        }
        return specification;
    }
}
