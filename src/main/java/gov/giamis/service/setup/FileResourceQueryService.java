package gov.giamis.service.setup;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.FileResourceType_;
import gov.giamis.domain.setup.FileResource_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.repository.setup.FileResourceRepository;
import gov.giamis.service.setup.dto.FileResourceCriteria;

/**
 * Service for executing complex queries for {@link FileResource} entities in the database.
 * The main input is a {@link FileResourceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FileResource} or a {@link Page} of {@link FileResource} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FileResourceQueryService extends QueryService<FileResource> {

    private final Logger log = LoggerFactory.getLogger(FileResourceQueryService.class);

    private final FileResourceRepository fileResourceRepository;

    public FileResourceQueryService(FileResourceRepository fileResourceRepository) {
        this.fileResourceRepository = fileResourceRepository;
    }

    /**
     * Return a {@link List} of {@link FileResource} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FileResource> findByCriteria(FileResourceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FileResource> specification = createSpecification(criteria);
        return fileResourceRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FileResource} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FileResource> findByCriteria(FileResourceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FileResource> specification = createSpecification(criteria);
        return fileResourceRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FileResourceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FileResource> specification = createSpecification(criteria);
        return fileResourceRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<FileResource> createSpecification(FileResourceCriteria criteria) {
        Specification<FileResource> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), FileResource_.id));
            }
            if (criteria.getUid() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getUid(), FileResource_.uid));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), FileResource_.name));
            }
            if (criteria.getPath() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getPath(), FileResource_.path));
            }
            if (criteria.getContentType() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getContentType(), FileResource_.contentType));
            }
            if (criteria.getSize() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getSize(), FileResource_.size));
            }
            if (criteria.getIsAssigned() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getIsAssigned(), FileResource_.isAssigned));
            }
            if (criteria.getTags() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getTags(), FileResource_.tags));
            }
            if (criteria.getFileResourceTypeId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getFileResourceTypeId(),
                    root -> root.join(FileResource_.fileResourceType, JoinType.LEFT)
                        .get(FileResourceType_.id)));
            }
        }
        return specification;
    }
}
