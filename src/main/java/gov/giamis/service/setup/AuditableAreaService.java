package gov.giamis.service.setup;

import gov.giamis.domain.setup.AuditableArea;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link AuditableArea}.
 */
public interface AuditableAreaService {

    /**
     * Save a auditableArea.
     *
     * @param auditableArea the entity to save.
     * @return the persisted entity.
     */
    AuditableArea save(AuditableArea auditableArea);

    /**
     * Get all the auditableAreas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuditableArea> findAll(Pageable pageable);

    /**
     * Get all the auditableAreas with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<AuditableArea> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" auditableArea.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuditableArea> findOne(Long id);

    /**
     * Delete the "id" auditableArea.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    AuditableArea findByName(String name);
}
