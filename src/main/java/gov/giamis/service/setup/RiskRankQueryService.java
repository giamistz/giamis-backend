package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.RiskRank_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.RiskRank;
import gov.giamis.repository.setup.RiskRankRepository;
import gov.giamis.service.setup.dto.RiskRankCriteria;

/**
 * Service for executing complex queries for {@link RiskRank} entities in the database.
 * The main input is a {@link RiskRankCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RiskRank} or a {@link Page} of {@link RiskRank} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RiskRankQueryService extends QueryService<RiskRank> {

    private final Logger log = LoggerFactory.getLogger(RiskRankQueryService.class);

    private final RiskRankRepository riskRankRepository;

    public RiskRankQueryService(RiskRankRepository riskRankRepository) {
        this.riskRankRepository = riskRankRepository;
    }

    /**
     * Return a {@link List} of {@link RiskRank} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RiskRank> findByCriteria(RiskRankCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RiskRank> specification = createSpecification(criteria);
        return riskRankRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link RiskRank} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RiskRank> findByCriteria(RiskRankCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RiskRank> specification = createSpecification(criteria);
        return riskRankRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RiskRankCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<RiskRank> specification = createSpecification(criteria);
        return riskRankRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<RiskRank> createSpecification(RiskRankCriteria criteria) {
        Specification<RiskRank> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), RiskRank_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), RiskRank_.name));
            }
            if (criteria.getMinValue() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getMinValue(), RiskRank_.minValue));
            }
            if (criteria.getMaxValue() != null) {
                specification = specification.and(buildRangeSpecification(
                    criteria.getMaxValue(), RiskRank_.maxValue));
            }
            if (criteria.getColor() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getColor(), RiskRank_.color));
            }
        }
        return specification;
    }
}
