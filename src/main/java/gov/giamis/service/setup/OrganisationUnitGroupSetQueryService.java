package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.OrganisationUnitGroupSet_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.OrganisationUnitGroupSet;
import gov.giamis.repository.setup.OrganisationUnitGroupSetRepository;
import gov.giamis.service.setup.dto.OrganisationUnitGroupSetCriteria;

/**
 * Service for executing complex queries for {@link OrganisationUnitGroupSet} entities in the database.
 * The main input is a {@link OrganisationUnitGroupSetCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrganisationUnitGroupSet} or
 * a {@link Page} of {@link OrganisationUnitGroupSet} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrganisationUnitGroupSetQueryService extends QueryService<OrganisationUnitGroupSet> {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupSetQueryService.class);

    private final OrganisationUnitGroupSetRepository organisationUnitGroupSetRepository;

    public OrganisationUnitGroupSetQueryService(
        OrganisationUnitGroupSetRepository organisationUnitGroupSetRepository) {

        this.organisationUnitGroupSetRepository = organisationUnitGroupSetRepository;
    }

    /**
     * Return a {@link List} of {@link OrganisationUnitGroupSet} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrganisationUnitGroupSet> findByCriteria(OrganisationUnitGroupSetCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrganisationUnitGroupSet> specification = createSpecification(criteria);
        return organisationUnitGroupSetRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OrganisationUnitGroupSet} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrganisationUnitGroupSet> findByCriteria(OrganisationUnitGroupSetCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrganisationUnitGroupSet> specification = createSpecification(criteria);
        return organisationUnitGroupSetRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrganisationUnitGroupSetCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrganisationUnitGroupSet> specification = createSpecification(criteria);
        return organisationUnitGroupSetRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<OrganisationUnitGroupSet> createSpecification(OrganisationUnitGroupSetCriteria criteria) {
        Specification<OrganisationUnitGroupSet> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), OrganisationUnitGroupSet_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), OrganisationUnitGroupSet_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), OrganisationUnitGroupSet_.name));
            }
        }
        return specification;
    }
}
