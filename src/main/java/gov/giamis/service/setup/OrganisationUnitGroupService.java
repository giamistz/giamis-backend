package gov.giamis.service.setup;

import gov.giamis.domain.setup.OrganisationUnitGroup;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link OrganisationUnitGroup}.
 */
public interface OrganisationUnitGroupService {

    /**
     * Save a organisationUnitGroup.
     *
     * @param organisationUnitGroup the entity to save.
     * @return the persisted entity.
     */
    OrganisationUnitGroup save(OrganisationUnitGroup organisationUnitGroup);

    /**
     * Get all the organisationUnitGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationUnitGroup> findAll(Pageable pageable);


    /**
     * Get the "id" organisationUnitGroup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationUnitGroup> findOne(Long id);

    /**
     * Delete the "id" organisationUnitGroup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    
    OrganisationUnitGroup findByName(String name);
}
