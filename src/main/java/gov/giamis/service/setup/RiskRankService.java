package gov.giamis.service.setup;

import gov.giamis.domain.setup.RiskRank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link RiskRank}.
 */
public interface RiskRankService {

	/**
	 * Save a riskRank.
	 *
	 * @param riskRank the entity to save.
	 * @return the persisted entity.
	 */
	RiskRank save(RiskRank riskRank);

	/**
	 * Get all the riskRanks.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<RiskRank> findAll(Pageable pageable);

	/**
	 * Get the "id" riskRank.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<RiskRank> findOne(Long id);

	/**
	 * Delete the "id" riskRank.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	RiskRank findByName(String name);
	
	List<RiskRank> findRiskRankByColor(Integer minValue,Integer maxValue);
}
