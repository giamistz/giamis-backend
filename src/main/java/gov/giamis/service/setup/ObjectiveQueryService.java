package gov.giamis.service.setup;

import java.util.List;

import gov.giamis.domain.setup.Objective_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.Objective;
import gov.giamis.repository.setup.ObjectiveRepository;
import gov.giamis.service.setup.dto.ObjectiveCriteria;

/**
 * Service for executing complex queries for {@link Objective} entities in the database.
 * The main input is a {@link ObjectiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Objective} or
 * a {@link Page} of {@link Objective} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ObjectiveQueryService extends QueryService<Objective> {

    private final Logger log = LoggerFactory.getLogger(ObjectiveQueryService.class);

    private final ObjectiveRepository objectiveRepository;

    public ObjectiveQueryService(ObjectiveRepository objectiveRepository) {
        this.objectiveRepository = objectiveRepository;
    }

    /**
     * Return a {@link List} of {@link Objective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Objective> findByCriteria(ObjectiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Objective> specification = createSpecification(criteria);
        return objectiveRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Objective} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Objective> findByCriteria(ObjectiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Objective> specification = createSpecification(criteria);
        return objectiveRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ObjectiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Objective> specification = createSpecification(criteria);
        return objectiveRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Objective> createSpecification(ObjectiveCriteria criteria) {
        Specification<Objective> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), Objective_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), Objective_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getDescription(), Objective_.description));
            }
        }
        return specification;
    }
}
