package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.UserProfile;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link UserProfile} entity. This class is used
 * in {@link UserProfileResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /organisation-units?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserProfileCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter voteCode;

    private StringFilter email;

    private StringFilter checkNumber;

    private LongFilter userId;
   
    public UserProfileCriteria(){
    }

    public UserProfileCriteria(UserProfileCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.voteCode = other.voteCode == null ? null : other.voteCode.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.checkNumber = other.checkNumber == null ? null : other.checkNumber.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public UserProfileCriteria copy() {
        return new UserProfileCriteria(this);
    }

   
    public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getVoteCode() {
		return voteCode;
	}

	public void setVoteCode(StringFilter voteCode) {
		this.voteCode = voteCode;
	}

	public StringFilter getEmail() {
		return email;
	}

	public void setEmail(StringFilter email) {
		this.email = email;
	}

	public StringFilter getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(StringFilter checkNumber) {
		this.checkNumber = checkNumber;
	}

	public LongFilter getUserId() {
		return userId;
	}

	public void setUserId(LongFilter userId) {
		this.userId = userId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserProfileCriteria that = (UserProfileCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(voteCode, that.voteCode) &&
            Objects.equals(email, that.email) &&
            Objects.equals(checkNumber, that.checkNumber) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        voteCode,
        email,
        checkNumber,
        userId
        );
    }

	@Override
	public String toString() {
		return "UserProfileCriteria [id=" + id + ", voteCode=" + voteCode + ", email=" + email + ", checkNumber="
				+ checkNumber + ", userId=" + userId + "]";
	}
}
