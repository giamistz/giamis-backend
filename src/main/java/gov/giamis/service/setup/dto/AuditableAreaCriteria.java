package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.web.rest.setup.AuditableAreaResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link AuditableArea} entity. This class is used
 * in {@link AuditableAreaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /auditable-areas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditableAreaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter organisationUnitGroupId;

    public AuditableAreaCriteria(){
    }

    public AuditableAreaCriteria(AuditableAreaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.organisationUnitGroupId = other.organisationUnitGroupId == null ? null : other.organisationUnitGroupId.copy();
    }

    @Override
    public AuditableAreaCriteria copy() {
        return new AuditableAreaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getOrganisationUnitGroupId() {
        return organisationUnitGroupId;
    }

    public void setOrganisationUnitGroupId(LongFilter organisationUnitGroupId) {
        this.organisationUnitGroupId = organisationUnitGroupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditableAreaCriteria that = (AuditableAreaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(organisationUnitGroupId, that.organisationUnitGroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
            organisationUnitGroupId
        );
    }

    @Override
    public String toString() {
        return "AuditableAreaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (organisationUnitGroupId != null ? "organisationUnitGroupId=" + organisationUnitGroupId + ", " : "") +
            "}";
    }

}
