package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.web.rest.setup.OrganisationUnitResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link OrganisationUnit} entity. This class is used
 * in {@link OrganisationUnitResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /organisation-units?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrganisationUnitCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter organisationUnitLevelId;

    private LongFilter parentOrganisationUnitId;

    private LongFilter organisationUnitGroupId;

    private LongFilter organisationUnitGroupSetId;
   
    public OrganisationUnitCriteria(){
    }

    public OrganisationUnitCriteria(OrganisationUnitCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.organisationUnitLevelId = other.organisationUnitLevelId == null ? null : other.organisationUnitLevelId.copy();
        this.parentOrganisationUnitId = other.parentOrganisationUnitId == null ? null : other.parentOrganisationUnitId.copy();
        this.organisationUnitGroupId = other.organisationUnitGroupId == null ? null : other.organisationUnitGroupId.copy();
        this.organisationUnitGroupSetId = other.organisationUnitGroupSetId == null ? null : other.organisationUnitGroupSetId.copy();
    }

    @Override
    public OrganisationUnitCriteria copy() {
        return new OrganisationUnitCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getOrganisationUnitLevelId() {
        return organisationUnitLevelId;
    }

    public void setOrganisationUnitLevelId(LongFilter organisationUnitLevelId) {
        this.organisationUnitLevelId = organisationUnitLevelId;
    }

    public LongFilter getParentOrganisationUnitId() {
        return parentOrganisationUnitId;
    }

    public void setParentOrganisationUnitId(LongFilter parentOrganisationUnitId) {
        this.parentOrganisationUnitId = parentOrganisationUnitId;
    }

    public LongFilter getOrganisationUnitGroupId() {
        return organisationUnitGroupId;
    }

    public void setOrganisationUnitGroupId(LongFilter organisationUnitGroupId) {
        this.organisationUnitGroupId = organisationUnitGroupId;
    }

    public LongFilter getOrganisationUnitGroupSetId() {
        return organisationUnitGroupSetId;
    }

    public void setOrganisationUnitGroupSetId(LongFilter organisationUnitGroupSetId) {
        this.organisationUnitGroupSetId = organisationUnitGroupSetId;
    }

   
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrganisationUnitCriteria that = (OrganisationUnitCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(organisationUnitLevelId, that.organisationUnitLevelId) &&
            Objects.equals(parentOrganisationUnitId, that.parentOrganisationUnitId) &&
            Objects.equals(organisationUnitGroupId, that.organisationUnitGroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
            organisationUnitLevelId,
            parentOrganisationUnitId,
            organisationUnitGroupId
        );
    }

    @Override
    public String toString() {
        return "OrganisationUnitCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (organisationUnitLevelId != null ? "organisationUnitLevelId=" + organisationUnitLevelId + ", " : "") +
                (parentOrganisationUnitId != null ? "parentOrganisationUnitId=" + parentOrganisationUnitId + ", " : "") +
                (organisationUnitGroupId != null ? "orgUnitGroupId=" + organisationUnitGroupId + ", " : "") +
            "}";
    }

}
