package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.web.rest.setup.FileResourceResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link FileResource} entity. This class is used
 * in {@link FileResourceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /file-resources?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FileResourceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter uid;

    private StringFilter name;

    private StringFilter path;

    private StringFilter contentType;

    private StringFilter contentMd5;

    private LongFilter size;

    private BooleanFilter isAssigned;

    private StringFilter tags;

    private LongFilter fileResourceTypeId;

    public FileResourceCriteria(){
    }

    public FileResourceCriteria(FileResourceCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.uid = other.uid == null ? null : other.uid.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.path = other.path == null ? null : other.path.copy();
        this.contentType = other.contentType == null ? null : other.contentType.copy();
        this.contentMd5 = other.contentMd5 == null ? null : other.contentMd5.copy();
        this.size = other.size == null ? null : other.size.copy();
        this.isAssigned = other.isAssigned == null ? null : other.isAssigned.copy();
        this.tags = other.tags == null ? null : other.tags.copy();
        this.fileResourceTypeId = other.fileResourceTypeId == null ? null : other.fileResourceTypeId.copy();
    }

    @Override
    public FileResourceCriteria copy() {
        return new FileResourceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUid() {
        return uid;
    }

    public void setUid(StringFilter uid) {
        this.uid = uid;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPath() {
        return path;
    }

    public void setPath(StringFilter path) {
        this.path = path;
    }

    public StringFilter getContentType() {
        return contentType;
    }

    public void setContentType(StringFilter contentType) {
        this.contentType = contentType;
    }

    public StringFilter getContentMd5() {
        return contentMd5;
    }

    public void setContentMd5(StringFilter contentMd5) {
        this.contentMd5 = contentMd5;
    }

    public LongFilter getSize() {
        return size;
    }

    public void setSize(LongFilter size) {
        this.size = size;
    }

    public BooleanFilter getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(BooleanFilter isAssigned) {
        this.isAssigned = isAssigned;
    }

    public StringFilter getTags() {
        return tags;
    }

    public void setTags(StringFilter tags) {
        this.tags = tags;
    }

    public LongFilter getFileResourceTypeId() {
        return fileResourceTypeId;
    }

    public void setFileResourceTypeId(LongFilter fileResourceTypeId) {
        this.fileResourceTypeId = fileResourceTypeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FileResourceCriteria that = (FileResourceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(uid, that.uid) &&
            Objects.equals(name, that.name) &&
            Objects.equals(path, that.path) &&
            Objects.equals(contentType, that.contentType) &&
            Objects.equals(contentMd5, that.contentMd5) &&
            Objects.equals(size, that.size) &&
            Objects.equals(isAssigned, that.isAssigned) &&
            Objects.equals(tags, that.tags) &&
            Objects.equals(fileResourceTypeId, that.fileResourceTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        uid,
        name,
        path,
        contentType,
        contentMd5,
        size,
        isAssigned,
        tags,
        fileResourceTypeId
        );
    }

    @Override
    public String toString() {
        return "FileResourceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (uid != null ? "uid=" + uid + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (path != null ? "path=" + path + ", " : "") +
                (contentType != null ? "contentType=" + contentType + ", " : "") +
                (contentMd5 != null ? "contentMd5=" + contentMd5 + ", " : "") +
                (size != null ? "size=" + size + ", " : "") +
                (isAssigned != null ? "isAssigned=" + isAssigned + ", " : "") +
                (tags != null ? "tags=" + tags + ", " : "") +
                (fileResourceTypeId != null ? "fileResourceTypeId=" + fileResourceTypeId + ", " : "") +
            "}";
    }

}
