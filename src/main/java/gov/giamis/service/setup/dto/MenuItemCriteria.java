package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.MenuItem;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link MenuItem} entity. This class is used
 * in {@link MenuItemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /roles?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MenuItemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter authorityId;

    private LongFilter menuGroupId;

    private LongFilter userId;

    public MenuItemCriteria(){
    }

    public MenuItemCriteria(MenuItemCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.authorityId = other.authorityId == null ? null : other.authorityId.copy();
        this.menuGroupId = other.menuGroupId == null ? null : other.menuGroupId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public MenuItemCriteria copy() {
        return new MenuItemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(LongFilter authorityId) {
        this.authorityId = authorityId;
    }

    public LongFilter getMenuGroupId() {
        return menuGroupId;
    }

    public void setMenuGroupId(LongFilter menuGroupId) {
        this.menuGroupId = menuGroupId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MenuItemCriteria that = (MenuItemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(authorityId, that.authorityId) &&
            Objects.equals(menuGroupId, that.menuGroupId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        authorityId,
        menuGroupId,
        userId
        );
    }

    @Override
    public String toString() {
        return "MenuItemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (authorityId != null ? "authorityId=" + authorityId + ", " : "") +
                (menuGroupId != null ? "menuGroupId=" + menuGroupId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
