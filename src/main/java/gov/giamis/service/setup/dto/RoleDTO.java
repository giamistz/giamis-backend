package gov.giamis.service.setup.dto;

import java.util.List;
import gov.giamis.domain.setup.Authority;

public class RoleDTO{
    
    public RoleDTO(){

    }
    private String name;

    List<Authority> authorities;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}
   
}