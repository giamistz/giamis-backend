package gov.giamis.service.setup.dto;
import java.util.List;

import gov.giamis.domain.setup.MenuGroup;

public class MenuDTO{
    
    public MenuDTO(){

    }
    private Long id;
    
    private String name;
    
    private String path;
    
    private String icon;

    List<MenuGroup> menuItem;

	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<MenuGroup> getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(List<MenuGroup> menuItem) {
		this.menuItem = menuItem;
	}

}