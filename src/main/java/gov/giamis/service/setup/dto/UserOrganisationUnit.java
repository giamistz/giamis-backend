package gov.giamis.service.setup.dto;

import java.util.ArrayList;
import java.util.List;

import gov.giamis.domain.setup.OrganisationUnit;

public class UserOrganisationUnit {

    private Long id;
    private String code;
    private String name;
    private Boolean isRiskRegisterLevel;
    private Boolean isRiskOwnerLevel;
    private Boolean isAuditableLevel;

    public UserOrganisationUnit() {

    }

    public UserOrganisationUnit(OrganisationUnit organisationUnit) {
        this.id = organisationUnit.getId();
        this.name = organisationUnit.getName();
        this.code = organisationUnit.getCode();
        this.isRiskRegisterLevel = organisationUnit.getOrganisationUnitLevel().getRiskRegisterLevel();
        this.isRiskOwnerLevel = organisationUnit.getOrganisationUnitLevel().getRiskOwnerLevel();
        this.isAuditableLevel = organisationUnit.getOrganisationUnitLevel().getAuditableLevel();
    }

    private List<UserOrganisationUnit> children = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserOrganisationUnit> getChildren() {
        return children;
    }

    public void setChildren(List<UserOrganisationUnit> children) {
        this.children = children;
    }

    public Boolean getRiskRegisterLevel() {
        return isRiskRegisterLevel;
    }

    public void setRiskRegisterLevel(Boolean riskRegisterLevel) {
        isRiskRegisterLevel = riskRegisterLevel;
    }

    public Boolean getRiskOwnerLevel() {
        return isRiskOwnerLevel;
    }

    public void setRiskOwnerLevel(Boolean riskOwnerLevel) {
        isRiskOwnerLevel = riskOwnerLevel;
    }

    public Boolean getAuditableLevel() {
        return isAuditableLevel;
    }

    public void setAuditableLevel(Boolean auditableLevel) {
        isAuditableLevel = auditableLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserOrganisationUnit)) return false;
        UserOrganisationUnit that = (UserOrganisationUnit) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserOrganisationUnit{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", name='" + name + '\'' +
            ", isRiskRegisterLevel=" + isRiskRegisterLevel +
            ", isRiskOwnerLevel=" + isRiskOwnerLevel +
            ", isAuditableLevel=" + isAuditableLevel +
            ", children=" + children +
            '}';
    }
}
