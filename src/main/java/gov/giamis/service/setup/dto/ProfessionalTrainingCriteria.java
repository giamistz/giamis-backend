package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.ProfessionalTraining;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link ProfessionalTraining} entity. This class is used
 * in {@link ProfessionalTrainingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /organisation-units?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProfessionalTrainingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private LocalDateFilter startDate;

    private LocalDateFilter endDate;

    private LongFilter userId;
   
    public ProfessionalTrainingCriteria(){
    }

    public ProfessionalTrainingCriteria(ProfessionalTrainingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public ProfessionalTrainingCriteria copy() {
        return new ProfessionalTrainingCriteria(this);
    }

    public LongFilter getId() {
		return id;
	}

	public void setId(LongFilter id) {
		this.id = id;
	}

	public StringFilter getTitle() {
		return title;
	}

	public void setTitle(StringFilter title) {
		this.title = title;
	}

	public LocalDateFilter getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateFilter startDate) {
		this.startDate = startDate;
	}

	public LocalDateFilter getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateFilter endDate) {
		this.endDate = endDate;
	}

	public LongFilter getUserId() {
		return userId;
	}

	public void setUserId(LongFilter userId) {
		this.userId = userId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProfessionalTrainingCriteria that = (ProfessionalTrainingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        title,
        startDate,
        endDate,
        userId
        );
    }

	@Override
	public String toString() {
		return "ProfessionalTrainingCriteria [id=" + id + ", title=" + title + ", startDate=" + startDate + ", endDate="
				+ endDate + ", userId=" + userId + "]";
	} 
}
