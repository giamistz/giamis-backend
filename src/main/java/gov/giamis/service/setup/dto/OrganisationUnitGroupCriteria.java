package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.setup.OrganisationUnitGroup;
import gov.giamis.web.rest.setup.OrganisationUnitGroupResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link OrganisationUnitGroup} entity. This class is used
 * in {@link OrganisationUnitGroupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /organisation-unit-groups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrganisationUnitGroupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private LongFilter organisationUnitGroupSetId;

    public OrganisationUnitGroupCriteria(){
    }

    public OrganisationUnitGroupCriteria(OrganisationUnitGroupCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.organisationUnitGroupSetId = other.organisationUnitGroupSetId == null ? null : other.organisationUnitGroupSetId.copy();
    }

    @Override
    public OrganisationUnitGroupCriteria copy() {
        return new OrganisationUnitGroupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getOrganisationUnitGroupSetId() {
        return organisationUnitGroupSetId;
    }

    public void setOrganisationUnitGroupSetId(LongFilter organisationUnitGroupSetId) {
        this.organisationUnitGroupSetId = organisationUnitGroupSetId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrganisationUnitGroupCriteria that = (OrganisationUnitGroupCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(organisationUnitGroupSetId, that.organisationUnitGroupSetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        name,
        organisationUnitGroupSetId
        );
    }

    @Override
    public String toString() {
        return "OrganisationUnitGroupCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (organisationUnitGroupSetId != null ? "organisationUnitGroupSetId=" + organisationUnitGroupSetId + ", " : "") +
            "}";
    }

}
