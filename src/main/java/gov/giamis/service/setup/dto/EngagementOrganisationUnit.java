package gov.giamis.service.setup.dto;

import java.util.ArrayList;
import java.util.List;

import gov.giamis.domain.setup.OrganisationUnit;

public class EngagementOrganisationUnit {

    private Long id;
    private String code;
    private String name;

    public EngagementOrganisationUnit() {

    }

    public EngagementOrganisationUnit(OrganisationUnit organisationUnit) {
        this.id = organisationUnit.getId();
        this.name = organisationUnit.getName();
        this.code = organisationUnit.getCode();
    }

    private List<EngagementOrganisationUnit> children = new ArrayList<>();

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EngagementOrganisationUnit> getChildren() {
		return children;
	}

	public void setChildren(List<EngagementOrganisationUnit> children) {
		this.children = children;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EngagementOrganisationUnit)) return false;
        EngagementOrganisationUnit that = (EngagementOrganisationUnit) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "EngagementOrganisationUnit [id=" + id + ", code=" + code + ", name=" + name + ", children=" + children
				+ "]";
	}
   
}
