package gov.giamis.service.setup.dto;

import java.io.Serializable;
import java.util.Objects;

import gov.giamis.domain.engagement.InternalControl;
import gov.giamis.domain.enumeration.ControlScore;
import gov.giamis.web.rest.engagement.InternalControlResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link InternalControl} entity. This class is used
 * in {@link InternalControlResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /internal-controls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InternalControlCriteria implements Serializable, Criteria {
    /**
     * Class for filtering ControlScore
     */
    public static class ControlScoreFilter extends Filter<ControlScore> {

        public ControlScoreFilter() {
        }

        public ControlScoreFilter(ControlScoreFilter filter) {
            super(filter);
        }

        @Override
        public ControlScoreFilter copy() {
            return new ControlScoreFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private IntegerFilter stepNumber;

    private StringFilter criteria;

    private ControlScoreFilter score;

    private LongFilter internalControlTypeId;

    private StringFilter areaOfResponsibility;

    private LongFilter engagementId;

    public InternalControlCriteria(){
    }

    public InternalControlCriteria(InternalControlCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.stepNumber = other.stepNumber == null ? null : other.stepNumber.copy();
        this.criteria = other.criteria == null ? null : other.criteria.copy();
        this.score = other.score == null ? null : other.score.copy();
        this.internalControlTypeId = other.internalControlTypeId == null ? null : other.internalControlTypeId.copy();
        this.areaOfResponsibility = other.areaOfResponsibility == null ? null : other.areaOfResponsibility.copy();
        this.engagementId = other.engagementId == null ? null : other.engagementId.copy();
    }

    @Override
    public InternalControlCriteria copy() {
        return new InternalControlCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(IntegerFilter stepNumber) {
        this.stepNumber = stepNumber;
    }

    public StringFilter getCriteria() {
        return criteria;
    }

    public void setCriteria(StringFilter criteria) {
        this.criteria = criteria;
    }

    public ControlScoreFilter getScore() {
        return score;
    }

    public void setScore(ControlScoreFilter score) {
        this.score = score;
    }

    public LongFilter getInternalControlTypeId() {
        return internalControlTypeId;
    }

    public void setInternalControlTypeId(LongFilter internalControlTypeId) {
        this.internalControlTypeId = internalControlTypeId;
    }

    public StringFilter getAreaOfResponsibility() {
        return areaOfResponsibility;
    }

    public void setAreaOfResponsibility(StringFilter areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
    }

    public LongFilter getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(LongFilter engagementId) {
        this.engagementId = engagementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InternalControlCriteria that = (InternalControlCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(stepNumber, that.stepNumber) &&
            Objects.equals(criteria, that.criteria) &&
            Objects.equals(score, that.score) &&
            Objects.equals(internalControlTypeId, that.internalControlTypeId) &&
            Objects.equals(areaOfResponsibility, that.areaOfResponsibility) &&
            Objects.equals(engagementId, that.engagementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        stepNumber,
        criteria,
        score,
        internalControlTypeId,
            areaOfResponsibility,
        engagementId
        );
    }

    @Override
    public String toString() {
        return "InternalControlCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (stepNumber != null ? "stepNumber=" + stepNumber + ", " : "") +
                (criteria != null ? "criteria=" + criteria + ", " : "") +
                (score != null ? "score=" + score + ", " : "") +
                (internalControlTypeId != null ? "internalControlTypeId=" + internalControlTypeId + ", " : "") +
                (areaOfResponsibility != null ? "areaOfResponsibility=" + areaOfResponsibility + ", " : "") +
                (engagementId != null ? "engagementId=" + engagementId + ", " : "") +
            "}";
    }

}
