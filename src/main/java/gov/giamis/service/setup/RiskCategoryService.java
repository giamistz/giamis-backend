package gov.giamis.service.setup;

import gov.giamis.domain.setup.RiskCategory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link RiskCategory}.
 */
public interface RiskCategoryService {

	/**
	 * Save a riskCategory.
	 *
	 * @param riskCategory the entity to save.
	 * @return the persisted entity.
	 */
	RiskCategory save(RiskCategory riskCategory);

	/**
	 * Get all the riskCategories.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<RiskCategory> findAll(Pageable pageable);

	/**
	 * Get the "id" riskCategory.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<RiskCategory> findOne(Long id);

	/**
	 * Delete the "id" riskCategory.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	RiskCategory findByName(String name);
}
