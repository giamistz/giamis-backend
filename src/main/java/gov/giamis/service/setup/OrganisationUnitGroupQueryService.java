package gov.giamis.service.setup;

import java.util.List;

import javax.persistence.criteria.JoinType;

import gov.giamis.domain.setup.OrganisationUnitGroupSet_;
import gov.giamis.domain.setup.OrganisationUnitGroup_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import gov.giamis.domain.setup.OrganisationUnitGroup;
import gov.giamis.repository.setup.OrganisationUnitGroupRepository;
import gov.giamis.service.setup.dto.OrganisationUnitGroupCriteria;

/**
 * Service for executing complex queries for {@link OrganisationUnitGroup} entities in the database.
 * The main input is a {@link OrganisationUnitGroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrganisationUnitGroup} or
 * a {@link Page} of {@link OrganisationUnitGroup} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrganisationUnitGroupQueryService extends QueryService<OrganisationUnitGroup> {

    private final Logger log = LoggerFactory.getLogger(OrganisationUnitGroupQueryService.class);

    private final OrganisationUnitGroupRepository organisationUnitGroupRepository;

    public OrganisationUnitGroupQueryService(
        OrganisationUnitGroupRepository organisationUnitGroupRepository) {

        this.organisationUnitGroupRepository = organisationUnitGroupRepository;
    }

    /**
     * Return a {@link List} of {@link OrganisationUnitGroup} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrganisationUnitGroup> findByCriteria(OrganisationUnitGroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrganisationUnitGroup> specification = createSpecification(criteria);
        return organisationUnitGroupRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OrganisationUnitGroup} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrganisationUnitGroup> findByCriteria(OrganisationUnitGroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrganisationUnitGroup> specification = createSpecification(criteria);
        return organisationUnitGroupRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrganisationUnitGroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrganisationUnitGroup> specification = createSpecification(criteria);
        return organisationUnitGroupRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<OrganisationUnitGroup> createSpecification(OrganisationUnitGroupCriteria criteria) {
        Specification<OrganisationUnitGroup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getId(), OrganisationUnitGroup_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getCode(), OrganisationUnitGroup_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(
                    criteria.getName(), OrganisationUnitGroup_.name));
            }
            if (criteria.getOrganisationUnitGroupSetId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getOrganisationUnitGroupSetId(),
                    root -> root.join(OrganisationUnitGroup_.organisationUnitGroupSet, JoinType.LEFT)
                        .get(OrganisationUnitGroupSet_.id)));
            }
        }
        return specification;
    }
}
