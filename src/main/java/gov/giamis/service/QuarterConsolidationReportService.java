package gov.giamis.service;

import gov.giamis.domain.QuarterConsolidationReport;
import gov.giamis.repository.QuarterConsolidationReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link QuarterConsolidationReport}.
 */
@Service
@Transactional
public class QuarterConsolidationReportService {

    private final Logger log = LoggerFactory.getLogger(QuarterConsolidationReportService.class);

    private final QuarterConsolidationReportRepository quarterConsolidationReportRepository;

    public QuarterConsolidationReportService(QuarterConsolidationReportRepository quarterConsolidationReportRepository) {
        this.quarterConsolidationReportRepository = quarterConsolidationReportRepository;
    }

    /**
     * Save a quarterConsolidationReport.
     *
     * @param quarterConsolidationReport the entity to save.
     * @return the persisted entity.
     */
    public QuarterConsolidationReport save(QuarterConsolidationReport quarterConsolidationReport) {
        log.debug("Request to save QuarterConsolidationReport : {}", quarterConsolidationReport);
        return quarterConsolidationReportRepository.save(quarterConsolidationReport);
    }

    /**
     * Get all the quarterConsolidationReports.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public QuarterConsolidationReport findByOuAndQtr(Long organisationUnitId, Long quarterId) {
        log.debug("Request to get one QuarterConsolidationReports");
        return quarterConsolidationReportRepository.findFirstByOrganisationUnit_IdAndQuarter_Id(organisationUnitId, quarterId);
    }


    /**
     * Get one quarterConsolidationReport by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<QuarterConsolidationReport> findOne(Long id) {
        log.debug("Request to get QuarterConsolidationReport : {}", id);
        return quarterConsolidationReportRepository.findById(id);
    }

    /**
     * Delete the quarterConsolidationReport by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete QuarterConsolidationReport : {}", id);
        quarterConsolidationReportRepository.deleteById(id);
    }
}
