package gov.giamis.domain.setup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A OrganisationUnitGroup.
 */
@Entity
@Table(name = "organisation_unit_groups")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrganisationUnitGroup extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
	@SafeHtml
	@Size(max = 50)
    @Column(name = "code", length = 50)
    private String code;

    @NotBlank
	@SafeHtml
	@Size(max = 50)
    @Column(name = "name", length = 50, nullable = false, unique = true)
    private String name;

    @ManyToOne
    @NotNull
    @JsonIgnoreProperties({"organisationUnitGroups","code"})
    private OrganisationUnitGroupSet organisationUnitGroupSet;

    
    public OrganisationUnitGroup() {
		super();
	}

	public OrganisationUnitGroup(@NotBlank @SafeHtml @Size(max = 50) String code,
			@NotBlank @SafeHtml @Size(max = 50) String name,
			@NotNull OrganisationUnitGroupSet organisationUnitGroupSet) {
		super();
		this.code = code;
		this.name = name;
		this.organisationUnitGroupSet = organisationUnitGroupSet;
	}
	
	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public OrganisationUnitGroup code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public OrganisationUnitGroup name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganisationUnitGroupSet getOrganisationUnitGroupSet() {
        return organisationUnitGroupSet;
    }

    public OrganisationUnitGroup organisationUnitGroupSet(OrganisationUnitGroupSet organisationUnitGroupSet) {
        this.organisationUnitGroupSet = organisationUnitGroupSet;
        return this;
    }

    public void setOrganisationUnitGroupSet(OrganisationUnitGroupSet organisationUnitGroupSet) {
        this.organisationUnitGroupSet = organisationUnitGroupSet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationUnitGroup)) {
            return false;
        }
        return id != null && id.equals(((OrganisationUnitGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationUnitGroup{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
