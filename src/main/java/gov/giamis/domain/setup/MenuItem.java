package gov.giamis.domain.setup;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A MenuItem.
 */
@Entity
@Table(name = "menu_items")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIgnoreProperties(value ={"authorities"},allowGetters = true,allowSetters= true)
public class MenuItem extends AbstractAuditingEntity implements Serializable {

    public MenuItem(String name, String path, String icon, MenuGroup menuGroup, Long order) {
        this.name = name;
        this.path = path;
        this.icon = icon;
        this.menuGroup = menuGroup;
        this.sortOrder = order;
    }

    public MenuItem() {}

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @Column(name = "sort_order")
    private Long sortOrder;

    @ManyToOne(optional = true,fetch =FetchType.EAGER)
    @JsonIgnoreProperties({
        "menuItem",
        "children",
        "parentMenuGroup"
    })
    private MenuGroup menuGroup;

    @ManyToMany(fetch =FetchType.EAGER
        , cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}
        )
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "menu_items_authorities",
               joinColumns = @JoinColumn(name = "menu_item_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
    @JsonIgnoreProperties({
        "role", "resource", "action"
    })
    private Set<Authority> authorities = new HashSet<>();

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Size(max = 255)
    @Column(name = "path", length = 255, nullable = false)
    private String path;

    @Size(max = 255)
    @Column(name = "icon", length = 255, nullable = false)
    private String icon;

    public MenuGroup getMenuGroup() {
		return menuGroup;
	}

	public void setMenuGroup(MenuGroup menuGroup) {
		this.menuGroup = menuGroup;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MenuItem)) {
            return false;
        }
        return id != null && id.equals(((MenuItem) o).id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return 31;
    }

//    @Override
//    public String toString() {
//        return "MenuItem{" +
//            "id=" + id +
//            ", name='" + name + '\'' +
//            ", menuGroup=" + menuGroup +
//            ", authorities=" + authorities +
//            ", path='" + path + '\'' +
//            ", icon='" + icon + '\'' +
//            '}';
//    }
}
