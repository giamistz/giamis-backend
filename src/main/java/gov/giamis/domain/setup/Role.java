package gov.giamis.domain.setup;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A Role.
 */
@Entity
@Table(name = "roles")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Role extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
	@SafeHtml
    @Size(min = 3, max = 50)
    @Column(name = "name", length = 50, nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;
    
    @Size(max = 50)
    @SafeHtml
    @Column(name = "code", length = 50)
    private String code;
 
    @ManyToMany(fetch =FetchType.LAZY)
    @JoinTable(name = "role_authorities",
               joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
    @JsonIgnoreProperties({"roles"})
    private Set<Authority> authorities = new HashSet<>();

    // TODO DELETE
    @JsonIgnore
    @ManyToMany(mappedBy = "roles",fetch=FetchType.LAZY)
    private Set<User> users = new HashSet<>();
 
    public Role() {
		super();
	}
    
	public Role(Long id, @NotBlank @SafeHtml @Size(min = 3, max = 50) String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Role(@NotBlank @SafeHtml @Size(min = 3, max = 50) String name, String description,
			@Size(max = 50) @SafeHtml String code) {
		super();
		this.name = name;
		this.description = description;
		this.code = code;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Role name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Role description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<Authority> getAuthorities() {
        return authorities;
    }

    public Role authorities(Set<Authority> authorities) {
        this.authorities = authorities;
        return this;
    }

    public Role addAuthority(Authority authority) {
        this.authorities.add(authority);
        return this;
    }

    public Role removeAuthority(Authority authority) {
        this.authorities.remove(authority);
        return this;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }
        return id != null && id.equals(((Role) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", description=" + description + ", code=" + code
				+ ", authorities=" + authorities + ", users=" + users + "]";
	}
}
