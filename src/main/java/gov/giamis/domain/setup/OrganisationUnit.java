package gov.giamis.domain.setup;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A OrganisationUnit.
 */
@Entity
@Table(name = "organisation_units")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIgnoreProperties(value ={"user"},allowGetters = true,allowSetters= true)
public class OrganisationUnit extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

	@SafeHtml
    @Size(max = 50)
    @Column(name = "code", length = 50)
    private String code;

	@NotBlank
	@SafeHtml
    @Size(max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "address")
    private String address;

    @Size(max = 20)
    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Size(max = 150)
    @Email
    @Column(name = "email", length = 150)
    private String email;

    @Size(max = 2000)
    @Column(name = "background", length = 2000)
    private String background;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organisation_unit_id")
    @JsonIgnoreProperties({
        "parentOrganisationUnit",
        "organisationUnitGroups",
        "contactPerson",
        "address",
        "code",
        "phoneNumber",
        "email",
        "background"})
    private OrganisationUnit parentOrganisationUnit;

    @ManyToMany(fetch =FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties({
        "parentOrganisationUnit",
        "organisationUnitGroups",
        "organisationUnitGroupSet",
        "code"
    })
    @JoinTable(name = "organisation_unit_organisation_unit_groups",
               joinColumns = @JoinColumn(name = "organisation_unit_id",
                   referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(
                   name = "organisation_unit_group_id",
                   referencedColumnName = "id"))
    private Set<OrganisationUnitGroup> organisationUnitGroups = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({
        "organisationUnits",
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    @OneToMany(mappedBy="organisationUnit")
    @JsonIgnore
    private List<User> users;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationUnits")
    private OrganisationUnitLevel organisationUnitLevel;

	@OneToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="contact_person_id",referencedColumnName = "id")
    @JsonIgnoreProperties({
        "authorities",
        "menus",
        "roles",
        "activated",
        "langKey",
        "imageUrl",
        "resetDate",
        "organisationUnit",
        "organisationUnitContactPerson",
        "auditorProfile",
        "currentFinancialYear"
    })
	private SimpleUser user;


    public OrganisationUnitLevel getOrganisationUnitLevel() {
        return organisationUnitLevel;
    }

    public void setOrganisationUnitLevel(OrganisationUnitLevel organisationUnitLevel) {
        this.organisationUnitLevel = organisationUnitLevel;
    }


    public OrganisationUnit() {
		super();
	}

	public OrganisationUnit(@SafeHtml @Size(max = 50) String code, @NotBlank @SafeHtml @Size(max = 100) String name,
			OrganisationUnitLevel organisationUnitLevel) {
		super();
		this.code = code;
		this.name = name;
		this.organisationUnitLevel = organisationUnitLevel;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public OrganisationUnit code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public OrganisationUnit name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public OrganisationUnit address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public OrganisationUnit phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public OrganisationUnit email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBackground() {
        return background;
    }

    public OrganisationUnit background(String background) {
        this.background = background;
        return this;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public OrganisationUnit getParentOrganisationUnit() {
        return parentOrganisationUnit;
    }

    public OrganisationUnit organisationUnit(OrganisationUnit organisationUnit) {
        this.parentOrganisationUnit = organisationUnit;
        return this;
    }

    public void setParentOrganisationUnit(OrganisationUnit parentOrganisationUnit) {
        this.parentOrganisationUnit = parentOrganisationUnit;
    }

    public Set<OrganisationUnitGroup> getOrganisationUnitGroups() {
        return organisationUnitGroups;
    }

    public OrganisationUnit organisationUnitGroups(Set<OrganisationUnitGroup> organisationUnitGroups) {
        this.organisationUnitGroups = organisationUnitGroups;
        return this;
    }

    public OrganisationUnit addOrganisationUnitGroup(OrganisationUnitGroup organisationUnitGroup) {
        this.organisationUnitGroups.add(organisationUnitGroup);
        return this;
    }

    public OrganisationUnit removeOrganisationUnitGroup(OrganisationUnitGroup organisationUnitGroup) {
        this.organisationUnitGroups.remove(organisationUnitGroup);
        return this;
    }

    public void setOrganisationUnitGroups(Set<OrganisationUnitGroup> organisationUnitGroups) {
        this.organisationUnitGroups = organisationUnitGroups;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public OrganisationUnit fileResource(FileResource fileResource) {
        this.fileResource = fileResource;
        return this;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public SimpleUser getUser() {
		return user;
	}

	public void setUser(SimpleUser user) {
		this.user = user;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationUnit)) {
            return false;
        }
        return id != null && id.equals(((OrganisationUnit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationUnit{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", email='" + email + '\'' +
            ", background='" + background + '\'' +
            ", parentOrganisationUnit=" + parentOrganisationUnit +
            ", organisationUnitGroups=" + organisationUnitGroups +
            ", fileResource=" + fileResource +
            ", users=" + users +
            ", organisationUnitLevel=" + organisationUnitLevel +
            ", user=" + user +
            '}';
    }

}
