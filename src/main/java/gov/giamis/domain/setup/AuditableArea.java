package gov.giamis.domain.setup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A AuditableArea.
 */
@Entity
@Table(name = "auditable_areas")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditableArea  extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "code", length = 50, nullable = false, unique = true)
	private String code;

    @NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "name", length = 50, nullable = false, unique = true)
	private String name;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties({"organisationUnitGroupSet","code"})
    @NotNull
    @JoinTable(name = "auditable_area_organisation_unit_groups",
               joinColumns = @JoinColumn(
                   name = "auditable_area_id",
                   referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(
                   name = "organisation_unit_group_id",
                   referencedColumnName = "id"))
    private Set<OrganisationUnitGroup> organisationUnitGroups = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public AuditableArea code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public AuditableArea name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<OrganisationUnitGroup> getOrganisationUnitGroups() {
        return organisationUnitGroups;
    }

    public AuditableArea organisationUnitGroups(Set<OrganisationUnitGroup> organisationUnitGroups) {
        this.organisationUnitGroups = organisationUnitGroups;
        return this;
    }

    public AuditableArea addOrganisationUnitGroups(OrganisationUnitGroup organisationUnitGroup) {
        this.organisationUnitGroups.add(organisationUnitGroup);
        return this;
    }

    public AuditableArea removeOrganisationUnitGroups(OrganisationUnitGroup organisationUnitGroup) {
        this.organisationUnitGroups.remove(organisationUnitGroup);
        return this;
    }

    public void setOrganisationUnitGroups(Set<OrganisationUnitGroup> organisationUnitGroups) {
        this.organisationUnitGroups = organisationUnitGroups;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditableArea)) {
            return false;
        }
        return id != null && id.equals(((AuditableArea) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditableArea{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
