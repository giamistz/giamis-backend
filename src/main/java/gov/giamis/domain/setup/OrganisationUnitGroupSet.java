package gov.giamis.domain.setup;

import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.SafeHtml;
import java.io.Serializable;

/**
 * A OrganisationUnitGroupSet.
 */
@Entity
@Table(name = "organisation_unit_group_sets")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrganisationUnitGroupSet extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "code", length = 50, nullable = false, unique = true)
	private String code;

	@NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "name", length = 50, nullable = false, unique = true)
	private String name;
	
	public OrganisationUnitGroupSet() {
		super();
	}

	public OrganisationUnitGroupSet(@NotBlank @SafeHtml @Size(max = 50) String code,
			@NotBlank @SafeHtml @Size(max = 50) String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public OrganisationUnitGroupSet code(String code) {
		this.code = code;
		return this;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public OrganisationUnitGroupSet name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof OrganisationUnitGroupSet)) {
			return false;
		}
		return id != null && id.equals(((OrganisationUnitGroupSet) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "OrganisationUnitGroupSet{" + "id=" + getId() + ", code='" + getCode() + "'" + ", name='" + getName()
				+ "'" + "}";
	}
}
