package gov.giamis.domain.setup;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.risk.Risk;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Set;

/**
 * A Control.
 */
@Entity
@Table(name = "controls")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Control extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 50)
    @Column(name = "code", length = 50, unique = true)
    private String code;

    @NotBlank
    @SafeHtml
    @Size(min = 3, max = 2000)
    @Column(name = "name", length = 2000, nullable = false, unique = true)
    private String name;
    
    @ManyToMany(mappedBy = "controls")
     private Set<Risk> risk;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Control code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public Control name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Set<Risk> getRisk() {
		return risk;
	}

	public void setRisk(Set<Risk> risk) {
		this.risk = risk;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Control)) {
            return false;
        }
        return id != null && id.equals(((Control) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Control{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
