package gov.giamis.domain.setup;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An authority (a security role) used by Spring Security.
 */
@Entity
@Table(name = "authorities")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(
				name = "getauthorities", 
				procedureName = "getauthorities", 
				resultClasses = {
				Authority.class }, 
				parameters = {
			    @StoredProcedureParameter(mode = ParameterMode.IN, name = "getResource", type = String.class) }),
		@NamedStoredProcedureQuery(
				name = "getmenuauthorities", 
				procedureName = "getmenuauthorities", 
				resultClasses = {
				Authority.class }, 
				parameters = {
			    @StoredProcedureParameter(mode = ParameterMode.IN, name = "getResource", type = String.class) })})
public class Authority implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotBlank
	@SafeHtml
	@Size(max = 200)
	@Column(length = 200)
	private String name;

	@NotBlank
	@SafeHtml
	@Size(max = 255)
	@Column(name = "display_name", length = 255)
	private String displayName;

	@NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(length = 50)
	private String resource;

	@NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(length = 50)
	private String action;

	@JsonIgnore
	@ManyToMany(mappedBy = "authorities", fetch = FetchType.EAGER)
	private Set<Role> roles;

	public Authority() {
		super();
	}

	public Authority(Long id, @NotBlank @SafeHtml @Size(max = 200) String name,
			@NotBlank @SafeHtml @Size(max = 255) String displayName,
			@NotBlank @SafeHtml @Size(max = 50) String resource, @NotBlank @SafeHtml @Size(max = 50) String action) {
		super();
		this.id = id;
		this.name = name;
		this.displayName = displayName;
		this.resource = resource;
		this.action = action;
	}

	public Authority(@NotBlank @SafeHtml @Size(max = 50) String resource) {
		super();
		this.resource = resource;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Authority)) {
			return false;
		}
		return id != null && id.equals(((Authority) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Authority{" + "id='" + id + '\'' + "name='" + name + '\'' + "displayName='" + displayName + '\''
				+ "action='" + action + '\'' + "resource='" + resource + '\'' + "}";
	}
}
