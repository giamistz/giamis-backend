package gov.giamis.domain.setup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A FileResource.
 */
@Entity
@Table(name = "file_resources")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FileResource extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public FileResource() {}

    public FileResource(String name,
                        String contentType,
                        String contentMd5,
                        Long size,
                        String tags,
                        FileResourceType fileResourceType) {
        this.name = name;
        this.size = size;
        this.tags = tags;
        this.isAssigned = false;
        this.contentMd5 = contentMd5;
        this.contentType = contentType;
        this.fileResourceType = fileResourceType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
	@SafeHtml
    @Column(name = "uid", nullable = false, unique = true)
    private String uid = String.valueOf(System.currentTimeMillis());

    @NotBlank
	@SafeHtml
    @Size(max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @NotBlank
	@SafeHtml
    @Column(name = "path", nullable = false)
    private String path;

    @NotBlank
	@SafeHtml
    @Size(max = 200)
    @Column(name = "content_type", length = 200, nullable = false)
    private String contentType;

    @NotBlank
	@SafeHtml
    @Size(max = 32)
    @Column(name = "content_md_5", length = 32, nullable = false)
    private String contentMd5;

    @NotNull
    @Column(name = "size", nullable = false)
    private Long size;

    @Column(name = "is_assigned")
    private Boolean isAssigned;

    @Column(name = "tags")
    private String tags;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("fileResources")
    private FileResourceType fileResourceType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public FileResource uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public FileResource name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public FileResource path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContentType() {
        return contentType;
    }

    public FileResource contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentMd5() {
        return contentMd5;
    }

    public FileResource contentMd5(String contentMd5) {
        this.contentMd5 = contentMd5;
        return this;
    }

    public void setContentMd5(String contentMd5) {
        this.contentMd5 = contentMd5;
    }

    public Long getSize() {
        return size;
    }

    public FileResource size(Long size) {
        this.size = size;
        return this;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Boolean isIsAssigned() {
        return isAssigned;
    }

    public FileResource isAssigned(Boolean isAssigned) {
        this.isAssigned = isAssigned;
        return this;
    }

    public void setIsAssigned(Boolean isAssigned) {
        this.isAssigned = isAssigned;
    }

    public String getTags() {
        return tags;
    }

    public FileResource tags(String tags) {
        this.tags = tags;
        return this;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public FileResourceType getFileResourceType() {
        return fileResourceType;
    }

    public FileResource fileResourceType(FileResourceType fileResourceType) {
        this.fileResourceType = fileResourceType;
        return this;
    }

    public void setFileResourceType(FileResourceType fileResourceType) {
        this.fileResourceType = fileResourceType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileResource)) {
            return false;
        }
        return id != null && id.equals(((FileResource) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FileResource{" +
            "id=" + getId() +
            ", uid='" + getUid() + "'" +
            ", name='" + getName() + "'" +
            ", path='" + getPath() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", contentMd5='" + getContentMd5() + "'" +
            ", size=" + getSize() +
            ", isAssigned='" + isIsAssigned() + "'" +
            ", tags='" + getTags() + "'" +
            "}";
    }
}
