package gov.giamis.domain.setup;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.program.AuditProgramEngagement;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A OrganisationUnitLevel.
 */
@Entity
@Table(name = "organisation_unit_levels")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrganisationUnitLevel extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

	@SafeHtml
	@Size(max = 50)
	@Column(name = "code", length = 50, nullable = false, unique = true)
	private String code;

    @NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "name", length = 50, nullable = false, unique = true)
	private String name;

    @Column(name = "is_risk_register_level")
    private Boolean isRiskRegisterLevel = false;

    @Column(name = "is_risk_owner_level")
    private Boolean isRiskOwnerLevel = false;

    @Column(name = "is_auditable_level")
    private Boolean isAuditableLevel = false;

    
    public OrganisationUnitLevel() {
		super();
	}

	public OrganisationUnitLevel(@NotBlank @SafeHtml @Size(max = 50) String name) {
		super();
		this.name = name;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public OrganisationUnitLevel code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public OrganisationUnitLevel name(String name) {
        this.name = name;
        return this;
    }

    public Boolean getRiskRegisterLevel() {
        return isRiskRegisterLevel;
    }

    public void setRiskRegisterLevel(Boolean riskRegisterLevel) {
        isRiskRegisterLevel = riskRegisterLevel;
    }

    public Boolean getRiskOwnerLevel() {
        return isRiskOwnerLevel;
    }

    public void setRiskOwnerLevel(Boolean riskOwnerLevel) {
        isRiskOwnerLevel = riskOwnerLevel;
    }

    public Boolean getAuditableLevel() {
        return isAuditableLevel;
    }

    public void setAuditableLevel(Boolean auditableLevel) {
        isAuditableLevel = auditableLevel;
    }

    public void setName(String name) {
        this.name = name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationUnitLevel)) {
            return false;
        }
        return id != null && id.equals(((OrganisationUnitLevel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationUnitLevel{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
