package gov.giamis.domain.setup;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import gov.giamis.domain.HighQualification;
import gov.giamis.domain.Position;
import gov.giamis.domain.ProfessionalQualifcation;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * A user.
 */
@Entity
@Table(name = "user_profiles")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIgnoreProperties(value ={"user"},allowGetters = true,allowSetters= true)
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 150)
    @Column(name = "vote_code", length = 150)
    private String voteCode;

    @SafeHtml
    @Size(max = 10)
    @NotBlank
    @Column(name = "gender", length = 10)
    private String gender;

    @SafeHtml
    @Size(max = 250)
    @Column(name = "title", length = 250)
    private String title;

    @SafeHtml
    @Size(max = 100)
    @Column(name = "check_number", length = 100,unique = true)
    private String checkNumber;


    @Column(name = "date_of_birth")
    @NotNull
    private LocalDate dateOfBirth;

    @NotNull
    @Column(name = "appointment_date")
    private LocalDate appointmentDate;

    @SafeHtml
    @Size(max = 250)
    @Column(name = "work_experience", length = 250)
    private String workExperience;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties("positions")
    @JoinColumn(name = "position_id")
    private Position position;

    @ManyToMany(
        fetch = FetchType.EAGER)
    @JsonIgnoreProperties({
        "userProfiles"
    })
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_profile_high_qualification",
        joinColumns = @JoinColumn(
            name = "user_profile_id",
            referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "high_qualification_id",
            referencedColumnName = "id"))
    private Set<HighQualification> highQualifications = new HashSet<>();

    @ManyToMany(
        fetch = FetchType.EAGER)
    @JsonIgnoreProperties({
        "userProfiles"
    })
    @JoinTable(name = "user_profile_professional_qualification",
        joinColumns = @JoinColumn(
            name = "user_profile_id",
            referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "professional_qualification_id",
            referencedColumnName = "id"))
    private Set<ProfessionalQualifcation> professionalQualifications = new HashSet<>();


    @OneToOne(optional = false)
    @JoinColumn(name = "user_id")
    @JsonProperty(access = Access.WRITE_ONLY)
    private User user;

    public UserProfile() {
		super();
	}

	public UserProfile(@SafeHtml @Size(max = 150) @NotBlank String voteCode,
			@SafeHtml @Size(max = 10) @NotBlank String gender, @SafeHtml @Size(max = 250) @NotBlank String title,
			@SafeHtml @Size(max = 100) @NotBlank String checkNumber, @NotNull LocalDate dateOfBirth,
			@NotNull LocalDate appointmentDate, @Size(max = 150) String highQualification,
			@SafeHtml @Size(max = 250) String workExperience,
			@SafeHtml @Size(max = 250) String professionalQualification, User user) {
		super();
		this.voteCode = voteCode;
		this.gender = gender;
		this.title = title;
		this.checkNumber = checkNumber;
		this.dateOfBirth = dateOfBirth;
		this.appointmentDate = appointmentDate;
		this.workExperience = workExperience;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVoteCode() {
		return voteCode;
	}

	public void setVoteCode(String voteCode) {
		this.voteCode = voteCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public LocalDate getAppointmentDate() {
		return appointmentDate;
	}

    public void setAppointmentDate(LocalDate appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getWorkExperience() {
		return workExperience;
	}
	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

    public Set<HighQualification> getHighQualifications() {
        return highQualifications;
    }

    public Set<ProfessionalQualifcation> getProfessionalQualifications() {
        return professionalQualifications;
    }

    public UserProfile highQualification(Set<HighQualification> highQualifications) {
        this.highQualifications = highQualifications;
        return this;
    }

    public UserProfile addHighQualifications(HighQualification highQualification) {
        this.highQualifications.add(highQualification);
        return this;
    }

    public UserProfile removeHighQualification(HighQualification highQualification) {
        this.highQualifications.remove(highQualification);
        return this;
    }

    public void setHighQualifications(Set<HighQualification> highQualifications) {
        this.highQualifications = highQualifications;
    }

    public UserProfile professionalQualification(Set<ProfessionalQualifcation> professionalQualifications) {
        this.professionalQualifications = professionalQualifications;
        return this;
    }

    public UserProfile professionalQualification(ProfessionalQualifcation professionalQualifcation) {
        this.professionalQualifications.add(professionalQualifcation);
        return this;
    }

    public UserProfile removeProfessionalQualification(ProfessionalQualifcation professionalQualifcation) {
        this.professionalQualifications.remove(professionalQualifcation);
        return this;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setProfessionalQualifications(Set<ProfessionalQualifcation> professionalQualifications) {
        this.professionalQualifications = professionalQualifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserProfile)) {
            return false;
        }
        return id != null && id.equals(((UserProfile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
            "id=" + id +
            ", voteCode='" + voteCode + '\'' +
            ", gender='" + gender + '\'' +
            ", title='" + title + '\'' +
            ", checkNumber='" + checkNumber + '\'' +
            ", dateOfBirth=" + dateOfBirth +
            ", appointmentDate=" + appointmentDate +
            ", workExperience='" + workExperience + '\'' +
            ", highQualifications=" + highQualifications +
            ", professionalQualifications=" + professionalQualifications +
            ", user=" + user +
            ", position=" + position +
            '}';
    }
}
