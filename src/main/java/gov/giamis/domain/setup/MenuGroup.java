package gov.giamis.domain.setup;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A MenuGroup.
 */
@Entity
@Table(name = "menu_groups")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@FilterDef(name="inId", parameters={
    @ParamDef( name = "itemIds", type = "long"),
    @ParamDef( name="ids", type = "long"),
})
@Filters( {
    @Filter(name="inId", condition="id IN (:ids)")
} )
public class MenuGroup extends AbstractAuditingEntity implements Serializable {

    public MenuGroup(String name, String icon, MenuGroup menuGroup, Long order) {
        this.name = name;
        this.icon = icon;
        this.parentMenuGroup = menuGroup;
        this.sortOrder = order;
    }

    public MenuGroup(Long id, String name, String icon, String path, Long order) {
      this.id = id;
      this.name = name;
      this.icon = icon;
      this.path = path;
      this.parentMenuGroup = null;
      this.sortOrder = order;
    }

    public MenuGroup(){}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotBlank
	@Size(max = 200)
	@Column(name = "name", length = 200, nullable = false)
	private String name;

    @Size(max = 255)
	@Column(name = "path", length = 255, nullable = false)
	private String path;

	@Size(max = 255)
	@Column(name = "icon", length = 255, nullable = false)
	private String icon;

	@Column(name = "sort_order")
	private Long sortOrder;

    @OneToMany(
        mappedBy = "menuGroup",
        cascade = CascadeType.ALL,
        orphanRemoval = true,
        fetch = FetchType.EAGER)
    @JsonIgnoreProperties({ "menuGroup" })
    @Filters( {
        @Filter(name="inId", condition="id IN (:itemIds)")
    } )
    @OrderBy("sort_order ASC")
    private Set<MenuItem> menuItem = new HashSet<>();

    @OneToMany(mappedBy = "parentMenuGroup",
        fetch = FetchType.EAGER,
        cascade = CascadeType.ALL
    )
    @JsonIgnoreProperties({"parentMenuGroup"})
    @Filters( {
        @Filter(name="inId", condition="id IN (:ids)")
    } )
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @OrderBy("sort_order ASC")
    private Set<MenuGroup> children = new HashSet<>();

	@ManyToOne()
	@JoinColumn(name = "menu_group_id")
	@JsonIgnoreProperties({ "children","authorities" })
	private MenuGroup parentMenuGroup;

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Set<MenuItem> getMenuItem() {
		return menuItem;
	}

    public void setChildren(Set<MenuGroup> children) {
        this.children = children;
    }

    public Set<MenuGroup> getChildren() {
        return children;
    }

	public void setMenuItem(Set<MenuItem> menuItem) {
		this.menuItem = menuItem;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof MenuGroup)) {
			return false;
		}
		return id != null && id.equals(((MenuGroup) o).id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public MenuGroup getParentMenuGroup() {
		return parentMenuGroup;
	}

	public void setParentMenuGroup(MenuGroup parentMenuGroup) {
		this.parentMenuGroup = parentMenuGroup;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "MenuGroup [id=" + id + ", name=" + name + ", menuItem=" + menuItem + "]";
	}

}
