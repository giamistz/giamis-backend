package gov.giamis.domain.setup;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A user.
 */
@Entity
@Table(name = "professional_trainings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//@JsonIgnoreProperties(value ={"user"},allowGetters = true,allowSetters= true)
public class ProfessionalTraining extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @NotBlank
    @Size(max = 250)
    @Column(name = "title", length = 250)
    private String title; 
    
    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description", nullable = true)
    private String description;

    @NotNull
    @Column(name = "start_date")
    private LocalDate startDate; 

    @NotNull
    @Column(name = "end_date")
    private LocalDate endDate;

    @ManyToOne
   // @JsonProperty(access = Access.WRITE_ONLY)
    @JsonIgnoreProperties(value ={
    		"user","authorities","roles","menus","organisationUnit",
    		"auditorProfile","professionalTraining","organisationUnitContactPerson"})
    private User user;
 
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfessionalTraining)) {
            return false;
        }
        return id != null && id.equals(((ProfessionalTraining) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "ProfessionalTraining [id=" + id + ", title=" + title + ", startDate=" + startDate + ", endDate="
				+ endDate + ", description=" + description + ", user=" + user + "]";
	}
}
