package gov.giamis.domain.report;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ReportParam.
 */
@Entity
@Table(name = "report_params")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReportParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @SafeHtml
    @Column(name = "options")
    private String options;

    @SafeHtml
    @Column(name = "option_query")
    private String optionQuery;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reportParams")
    private ReportContent reportContent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ReportParam name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOptions() {
        return options;
    }

    public ReportParam options(String options) {
        this.options = options;
        return this;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getOptionQuery() {
        return optionQuery;
    }

    public ReportParam optionQuery(String optionQuery) {
        this.optionQuery = optionQuery;
        return this;
    }

    public void setOptionQuery(String optionQuery) {
        this.optionQuery = optionQuery;
    }

    public ReportContent getReportContent() {
        return reportContent;
    }

    public ReportParam reportContent(ReportContent reportContent) {
        this.reportContent = reportContent;
        return this;
    }

    public void setReportContent(ReportContent reportContent) {
        this.reportContent = reportContent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReportParam)) {
            return false;
        }
        return id != null && id.equals(((ReportParam) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ReportParam{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", options='" + getOptions() + "'" +
            ", optionQuery='" + getOptionQuery() + "'" +
            "}";
    }
}
