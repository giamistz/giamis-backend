package gov.giamis.domain.report;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ReportContentValue.
 */
@Entity
@Table(name = "report_content_values")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReportContentValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "value")
    private String value;

    @Column(name = "start_page")
    private Integer startPage;

    @Column(name = "end_page")
    private Integer endPage;

    @Column(name = "number_of_pages")
    private Integer numberOfPages;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reportContentValues")
    private ReportContent reportContent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public ReportContentValue value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getStartPage() {
        return startPage;
    }

    public ReportContentValue startPage(Integer startPage) {
        this.startPage = startPage;
        return this;
    }

    public void setStartPage(Integer startPage) {
        this.startPage = startPage;
    }

    public Integer getEndPage() {
        return endPage;
    }

    public ReportContentValue endPage(Integer endPage) {
        this.endPage = endPage;
        return this;
    }

    public void setEndPage(Integer endPage) {
        this.endPage = endPage;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public ReportContentValue numberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
        return this;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public ReportContent getReportContent() {
        return reportContent;
    }

    public ReportContentValue reportContent(ReportContent reportContent) {
        this.reportContent = reportContent;
        return this;
    }

    public void setReportContent(ReportContent reportContent) {
        this.reportContent = reportContent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReportContentValue)) {
            return false;
        }
        return id != null && id.equals(((ReportContentValue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ReportContentValue{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            ", startPage=" + getStartPage() +
            ", endPage=" + getEndPage() +
            ", numberOfPages=" + getNumberOfPages() +
            "}";
    }
}