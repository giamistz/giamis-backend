package gov.giamis.domain.report;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import gov.giamis.domain.enumeration.ContentType;

/**
 * A ReportContent.
 */
@Entity
@Table(name = "report_contents")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReportContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "content_type")
    private ContentType contentType;

    @SafeHtml
    @Column(name = "template_uri")
    private String templateUri;

    @Column(name = "display_order")
    private Integer displayOrder;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reportContents")
    private Report report;

    @ManyToOne
    @JsonIgnoreProperties("reportContents")
    private ReportContent parentContent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ReportContent name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public ReportContent contentType(ContentType contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getTemplateUri() {
        return templateUri;
    }

    public ReportContent templateUri(String templateUri) {
        this.templateUri = templateUri;
        return this;
    }

    public void setTemplateUri(String templateUri) {
        this.templateUri = templateUri;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public ReportContent displayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
        return this;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Report getReport() {
        return report;
    }

    public ReportContent report(Report report) {
        this.report = report;
        return this;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public ReportContent getParentContent() {
        return parentContent;
    }

    public ReportContent parentContent(ReportContent reportContent) {
        this.parentContent = reportContent;
        return this;
    }

    public void setParentContent(ReportContent reportContent) {
        this.parentContent = reportContent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReportContent)) {
            return false;
        }
        return id != null && id.equals(((ReportContent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ReportContent{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", templateUri='" + getTemplateUri() + "'" +
            ", displayOrder=" + getDisplayOrder() +
            "}";
    }
}