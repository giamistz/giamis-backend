package gov.giamis.domain.risk;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.engagement.EngagementObjectiveRisk;
import gov.giamis.domain.engagement.EngagementProcedure;
import gov.giamis.domain.engagement.InternalControl;

import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.List;

/**
 * A RiskControlMatrix.
 */
@Entity
@Table(name = "risk_control_matrices")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RiskControlMatrix implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "is_key_control")
    private Boolean isKeyControl;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"riskControlMatrices"})
    @NotFound(action = NotFoundAction.IGNORE)
    private EngagementObjectiveRisk engagementObjectiveRisk;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"riskControlMatrices","engagement"})
    private InternalControl internalControl;

    @OneToMany(mappedBy = "riskControlMatrix")
    @JsonIgnoreProperties({
        "riskControlMatrix"
    })
    @Where(clause = "is_walk_through = false")
    private List<EngagementProcedure> engagementProcedures;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsKeyControl() {
        return isKeyControl;
    }

    public RiskControlMatrix isKeyControl(Boolean isKeyControl) {
        this.isKeyControl = isKeyControl;
        return this;
    }

    public void setIsKeyControl(Boolean isKeyControl) {
        this.isKeyControl = isKeyControl;
    }

    public EngagementObjectiveRisk getEngagementObjectiveRisk() {
        return engagementObjectiveRisk;
    }

    public RiskControlMatrix engagementObjectiveRisk(EngagementObjectiveRisk engagementObjectiveRisk) {
        this.engagementObjectiveRisk = engagementObjectiveRisk;
        return this;
    }

    public Boolean getKeyControl() {
        return isKeyControl;
    }

    public void setKeyControl(Boolean keyControl) {
        isKeyControl = keyControl;
    }

    public List<EngagementProcedure> getEngagementProcedures() {
        return engagementProcedures;
    }

    public void setEngagementProcedures(List<EngagementProcedure> engagementProcedures) {
        this.engagementProcedures = engagementProcedures;
    }

    public void setEngagementObjectiveRisk(EngagementObjectiveRisk engagementObjectiveRisk) {
        this.engagementObjectiveRisk = engagementObjectiveRisk;
    }

    public InternalControl getInternalControl() {
        return internalControl;
    }

    public RiskControlMatrix internalControl(InternalControl internalControl) {
        this.internalControl = internalControl;
        return this;
    }

    public void setInternalControl(InternalControl internalControl) {
        this.internalControl = internalControl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RiskControlMatrix)) {
            return false;
        }
        return id != null && id.equals(((RiskControlMatrix) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RiskControlMatrix{" +
            "id=" + getId() +
            ", isKeyControl='" + isIsKeyControl() + "'" +
            "}";
    }
}
