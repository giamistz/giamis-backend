package gov.giamis.domain.risk;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.Objective;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Target.
 */
@Entity
@Table(name = "targets")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Target extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;


    @NotBlank
	@SafeHtml
    @Size(max = 10)
    @Column(name = "code", length = 10, nullable = false, unique = true)
    private String code;

    @NotBlank
	@SafeHtml
    @Column(name = "description",nullable = false)
    private String description;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("targets")
    private Objective objective;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("targets")
    private FinancialYear startFinancialYear;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("targets")
    private FinancialYear endFinancialYear;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("targets")
    private OrganisationUnit organisationUnit;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Target code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Target description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Objective getObjective() {
        return objective;
    }

    public Target objective(Objective objective) {
        this.objective = objective;
        return this;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    public FinancialYear getStartFinancialYear() {
        return startFinancialYear;
    }

    public Target startFinancialYear(FinancialYear financialYear) {
        this.startFinancialYear = financialYear;
        return this;
    }

    public void setStartFinancialYear(FinancialYear financialYear) {
        this.startFinancialYear = financialYear;
    }

    public FinancialYear getEndFinancialYear() {
        return endFinancialYear;
    }

    public Target endFinancialYear(FinancialYear financialYear) {
        this.endFinancialYear = financialYear;
        return this;
    }

    public void setEndFinancialYear(FinancialYear financialYear) {
        this.endFinancialYear = financialYear;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public Target organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Target)) {
            return false;
        }
        return id != null && id.equals(((Target) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Target{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
