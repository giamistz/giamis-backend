package gov.giamis.domain.risk;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.enumeration.RiskRatingType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import gov.giamis.domain.enumeration.RiskSource;

/**
 * A RiskRating.
 */
@Entity
@Table(name = "risk_ratings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RiskRating extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "impact", nullable = false)
    private Integer impact;

    @NotNull
    @Column(name = "likelihood", nullable = false)
    private Integer likelihood;

    @Size(max = 1000)
    @Column(name = "comments", length = 1000)
    private String comments;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "source", nullable = false)
    private RiskSource source;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private RiskRatingType type;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("riskRatings")
    private Risk risk;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public Long getId() {
        return id;
    }
    
	public void setId(Long id) {
        this.id = id;
    }

    public Integer getImpact() {
        return impact;
    }

    public RiskRating impact(Integer impact) {
        this.impact = impact;
        return this;
    }

    public RiskRatingType getType() {
        return type;
    }

    public void setType(RiskRatingType type) {
        this.type = type;
    }

    public void setImpact(Integer impact) {
        this.impact = impact;
    }

    public Integer getLikelihood() {
        return likelihood;
    }

    public RiskRating likelihood(Integer likelihood) {
        this.likelihood = likelihood;
        return this;
    }

    public void setLikelihood(Integer likelihood) {
        this.likelihood = likelihood;
    }

    public String getComments() {
        return comments;
    }

    public RiskRating comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public RiskSource getSource() {
        return source;
    }

    public RiskRating source(RiskSource source) {
        this.source = source;
        return this;
    }

    public void setSource(RiskSource source) {
        this.source = source;
    }

    public Risk getRisk() {
        return risk;
    }

    public RiskRating risk(Risk risk) {
        this.risk = risk;
        return this;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RiskRating)) {
            return false;
        }
        return id != null && id.equals(((RiskRating) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RiskRating{" +
            "id=" + getId() +
            ", impact=" + getImpact() +
            ", likelihood=" + getLikelihood() +
            ", comments='" + getComments() + "'" +
            ", source='" + getSource() + "'" +
            "}";
    }
}
