package gov.giamis.domain.risk;
import gov.giamis.domain.engagement.Engagement;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RiskControlReport.
 */
@Entity
@Table(name = "risk_control_reports")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RiskControlReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;


    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "work_performed", nullable = false)
    private String workPerformed;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "result", nullable = false)
    private String result;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion", nullable = false)
    private String conclusion;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "reviewer_comment")
    private String reviewerComment;

    private Boolean isApproved = false;

    private LocalDate approvedDate;

    private String approvedBy;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "reason_for_suspension", nullable = false)
    private String reasonForSuspension;

    @OneToOne(optional = false)
    @NotNull
    @MapsId
    @JoinColumn(name = "id")
    private Engagement engagement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkPerformed() {
        return workPerformed;
    }

    public RiskControlReport workPerfomed(String workPerfomed) {
        this.workPerformed = workPerfomed;
        return this;
    }

    public void setWorkPerformed(String workPerformed) {
        this.workPerformed = workPerformed;
    }

    public String getResult() {
        return result;
    }

    public RiskControlReport result(String result) {
        this.result = result;
        return this;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getConclusion() {
        return conclusion;
    }

    public RiskControlReport conclusion(String conclusion) {
        this.conclusion = conclusion;
        return this;
    }

    public String getReviewerComment() {
        return reviewerComment;
    }

    public void setReviewerComment(String reviewerComment) {
        this.reviewerComment = reviewerComment;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getReasonForSuspension() {
        return reasonForSuspension;
    }

    public RiskControlReport reasonForSuspension(String reasonForSuspension) {
        this.reasonForSuspension = reasonForSuspension;
        return this;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public void setReasonForSuspension(String reasonForSuspension) {
        this.reasonForSuspension = reasonForSuspension;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public RiskControlReport engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RiskControlReport)) {
            return false;
        }
        return id != null && id.equals(((RiskControlReport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RiskControlReport{" +
            "id=" + getId() +
            ", workPerformed='" + getWorkPerformed() + "'" +
            ", result='" + getResult() + "'" +
            ", conclusion='" + getConclusion() + "'" +
            ", reasonForSuspension='" + getReasonForSuspension() + "'" +
            "}";
    }

}
