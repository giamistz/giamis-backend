package gov.giamis.domain.risk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.Control;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.RiskCategory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import gov.giamis.domain.enumeration.RiskSource;

/**
 * A Risk.
 */
@Entity
@Table(name = "risks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Risk extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 50)
    @Column(name = "code", length = 50, nullable = false, unique = true)
    private String code;

    @NotBlank
	@SafeHtml
    @Column(name = "name", nullable = false)
    private String name;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "source", nullable = true)
    @JsonSerialize
    @JsonDeserialize
    private RiskSource source;


    @Column(name = "causes")
    private String causes;


    @Column(name = "consequences")
    private String consequences;


    @Column(name = "treatments")
    private String treatments;


    @Column(name = "resources")
    private String resources;

    @NotNull
    @Column(name = "completed", nullable = false)
    private Boolean completed = false;

    @Column(name = "is_approved")
    private Boolean isApproved = false;

    @Column(name = "comments")
    private String comments;

    @OneToMany(mappedBy = "risk",
        cascade = CascadeType.ALL,
        orphanRemoval = true,
        fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("risk")
    private Set<RiskRating> riskRatings = new HashSet<>();

    @ManyToMany(fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "risk_controls",
        joinColumns = @JoinColumn(name = "risk_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "control_id", referencedColumnName = "id"))
    private Set<Control> controls = new HashSet<>();

    @ManyToOne()
    @JsonIgnoreProperties("risks")
    private RiskRegister riskRegister;

    @ManyToOne()
    @JsonIgnoreProperties({
        "risks",
        "startFinancialYear",
        "endFinancialYear",
        "organisationUnit",
    })
    private Target target;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("risks")
    private OrganisationUnit organisationUnit;

    @ManyToOne()
    @JsonIgnoreProperties("risks")
    private RiskCategory riskCategory;

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Risk code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public Risk name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Risk description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RiskSource getSource() {
        return source;
    }

    public Risk source(RiskSource source) {
        this.source = source;
        return this;
    }

    public void setSource(RiskSource source) {
        this.source = source;
    }

    public String getCauses() {
        return causes;
    }

    public Risk causes(String causes) {
        this.causes = causes;
        return this;
    }

    public void setCauses(String causes) {
        this.causes = causes;
    }

    public String getConsequences() {
        return consequences;
    }
    public Risk consequences(String consequences) {
        this.consequences = consequences;
        return this;
    }

    public void setConsequences(String consequences) {
        this.consequences = consequences;
    }

    public String getTreatments() {
        return treatments;
    }

    public Risk treatments(String treatments) {
        this.treatments = treatments;
        return this;
    }

    public void setTreatments(String treatments) {
        this.treatments = treatments;
    }

    public String getResources() {
        return resources;
    }

    public Risk resources(String resources) {
        this.resources = resources;
        return this;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    public Boolean isCompleted() {
        return completed;
    }

    public Risk completed(Boolean completed) {
        this.completed = completed;
        return this;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Set<RiskRating> getRiskRatings() {
        return riskRatings;
    }

    public Risk riskRatings(Set<RiskRating> riskRatings) {
        this.riskRatings = riskRatings;
        return this;
    }

    public Risk addRiskRatings(RiskRating riskRating) {
        this.riskRatings.add(riskRating);
        riskRating.setRisk(this);
        return this;
    }

    public Risk removeRiskRatings(RiskRating riskRating) {
        this.riskRatings.remove(riskRating);
        riskRating.setRisk(null);
        return this;
    }

    public void setRiskRatings(Set<RiskRating> riskRatings) {
        this.riskRatings = riskRatings;
    }

    public Set<Control> getControls() {
        return controls;
    }

    public Risk controls(Set<Control> riskControls) {
        this.controls = riskControls;
        return this;
    }

    public Risk addControls(Control control) {
        this.controls.add(control);
        return this;
    }

    public Risk removeControls(Control control) {
        this.controls.remove(control);
        return this;
    }

    public void setControls(Set<Control> controls) {
        this.controls = controls;
    }

    public RiskRegister getRiskRegister() {
        return riskRegister;
    }

    public Risk riskRegister(RiskRegister riskRegister) {
        this.riskRegister = riskRegister;
        return this;
    }

    public void setRiskRegister(RiskRegister riskRegister) {
        this.riskRegister = riskRegister;
    }

    public Target getTarget() {
        return target;
    }

    public Risk target(Target target) {
        this.target = target;
        return this;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public Risk organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public RiskCategory getRiskCategory() {
        return riskCategory;
    }

    public Risk riskCategory(RiskCategory riskCategory) {
        this.riskCategory = riskCategory;
        return this;
    }

    public void setRiskCategory(RiskCategory riskCategory) {
        this.riskCategory = riskCategory;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove
	public Boolean getCompleted() {
		return completed;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Risk)) {
            return false;
        }
        return id != null && id.equals(((Risk) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Risk{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", source='" + getSource() + "'" +
            ", causes='" + getCauses() + "'" +
            ", consequences='" + getConsequences() + "'" +
            ", treatments='" + getTreatments() + "'" +
            ", resources='" + getResources() + "'" +
            ", completed='" + isCompleted() + "'" +
            "}";
    }
}
