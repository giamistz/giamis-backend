package gov.giamis.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.Quarter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A QuarterReport.
 */
@Entity
@Table(name = "quarter_reports")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QuarterReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "acknowledgement")
    private String acknowledgement;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "list_of_abbreviation")
    private String listOfAbbreviation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "list_of_figure_and_table")
    private String listOfFigureAndTable;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "executive_summary")
    private String executiveSummary;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "background")
    private String background;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "objective")
    private String objective;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "methodology")
    private String methodology;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "scope")
    private String scope;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "challenge")
    private String challenge;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "cag_recommendation")
    private String cagRecommendation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "internal_auditor_recommendation")
    private String internalAuditorRecommendation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "ppra_auditor_recommendation")
    private String ppraAuditorRecommendation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "laac_auditor_recommendation")
    private String laacAuditorRecommendation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "risk_training")
    private String riskTraining;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "facility_training")
    private String facilityTraining;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "other_training")
    private String otherTraining;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "non_audit_activity")
    private String nonAuditActivity;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion")
    private String conclusion;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("quarterReports")
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("quarterReports")
    private Quarter quarter;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    public String getListOfAbbreviation() {
        return listOfAbbreviation;
    }

    public void setListOfAbbreviation(String listOfAbbreviation) {
        this.listOfAbbreviation = listOfAbbreviation;
    }

    public String getListOfFigureAndTable() {
        return listOfFigureAndTable;
    }

    public void setListOfFigureAndTable(String listOfFigureAndTable) {
        this.listOfFigureAndTable = listOfFigureAndTable;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getInternalAuditorRecommendation() {
        return internalAuditorRecommendation;
    }

    public void setInternalAuditorRecommendation(String internalAuditorRecommendation) {
        this.internalAuditorRecommendation = internalAuditorRecommendation;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getMethodology() {
        return methodology;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getPpraAuditorRecommendation() {
        return ppraAuditorRecommendation;
    }


    public String getCagRecommendation() {
        return cagRecommendation;
    }

    public void setCagRecommendation(String cagRecommendation) {
        this.cagRecommendation = cagRecommendation;
    }

    public void setPpraAuditorRecommendation(String ppraAuditorRecommendation) {
        this.ppraAuditorRecommendation = ppraAuditorRecommendation;
    }

    public String getLaacAuditorRecommendation() {
        return laacAuditorRecommendation;
    }

    public void setLaacAuditorRecommendation(String laacAuditorRecommendation) {
        this.laacAuditorRecommendation = laacAuditorRecommendation;
    }

    public String getRiskTraining() {
        return riskTraining;
    }

    public void setRiskTraining(String riskTraining) {
        this.riskTraining = riskTraining;
    }

    public String getFacilityTraining() {
        return facilityTraining;
    }

    public void setFacilityTraining(String facilityTraining) {
        this.facilityTraining = facilityTraining;
    }

    public String getOtherTraining() {
        return otherTraining;
    }

    public void setOtherTraining(String otherTraining) {
        this.otherTraining = otherTraining;
    }

    public String getNonAuditActivity() {
        return nonAuditActivity;
    }

    public void setNonAuditActivity(String nonAuditActivity) {
        this.nonAuditActivity = nonAuditActivity;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public Quarter getQuarter() {
        return quarter;
    }

    public QuarterReport quarter(Quarter quarter) {
        this.quarter = quarter;
        return this;
    }

    public void setQuarter(Quarter quarter) {
        this.quarter = quarter;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuarterReport)) {
            return false;
        }
        return id != null && id.equals(((QuarterReport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "QuarterReport{" +
            "id=" + getId() +
            ", acknowledgement='" + getAcknowledgement() + "'" +
            ", listOfAbbreviation='" + getListOfAbbreviation() + "'" +
            ", listOfFigureAndTable='" + getListOfFigureAndTable() + "'" +
            ", executiveSummary='" + getExecutiveSummary() + "'" +
            ", objective='" + getObjective() + "'" +
            ", methodology='" + getMethodology() + "'" +
            ", scope='" + getScope() + "'" +
            ", challenge='" + getChallenge() + "'" +
            ", ppraAuditorRecommendation='" + getPpraAuditorRecommendation() + "'" +
            ", laacAuditorRecommendation='" + getLaacAuditorRecommendation() + "'" +
            ", riskTraining='" + getRiskTraining() + "'" +
            ", facilityTraining='" + getFacilityTraining() + "'" +
            ", otherTraining='" + getOtherTraining() + "'" +
            ", nonAuditActivity='" + getNonAuditActivity() + "'" +
            ", conclusion='" + getConclusion() + "'" +
            "}";
    }
}
