package gov.giamis.domain.enumeration;

/**
 * The MeetingType enumeration.
 */
public enum MeetingType {
    TEAM_MEETING, ENTRANCE_MEETING, INTERIM_MEETING, EXIT_MEATING
}
