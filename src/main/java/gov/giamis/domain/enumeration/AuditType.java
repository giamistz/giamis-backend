package gov.giamis.domain.enumeration;

/**
 * The AuditType enumeration.
 */
public enum AuditType {
    PLANNED, SPECIAL
}
