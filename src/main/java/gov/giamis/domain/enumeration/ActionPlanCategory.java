package gov.giamis.domain.enumeration;

/**
 * The ActionPlanCategory enumeration.
 */
public enum ActionPlanCategory {
    HIGH, MEDIUM, LOW
}
