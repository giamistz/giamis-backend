package gov.giamis.domain.enumeration;
public enum ContentType {
    TEMPLATE, TEXT, UPLOAD, SECTION
}
