package gov.giamis.domain.enumeration;

/**
 * The EngagementPhaseStatus enumeration.
 */
public enum EngagementPhaseStatus {
    OPENED, REVIEWED, APPROVED, LOCKED
}
