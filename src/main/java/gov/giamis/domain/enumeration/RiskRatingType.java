package gov.giamis.domain.enumeration;

/**
 * The RiskSource enumeration.
 */
public enum RiskRatingType {
    INHERENT, RESIDUAL
}
