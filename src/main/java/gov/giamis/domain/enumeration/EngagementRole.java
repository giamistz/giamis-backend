package gov.giamis.domain.enumeration;

/**
 * The EngagementRole enumeration.
 */
public enum EngagementRole {
    MEMBER, TEAMLEAD
}
