package gov.giamis.domain.enumeration;

/**
 * The LetterType enumeration.
 */
public enum LetterType {
    ENGAGEMENT_LETTER, TRANSMITTAL_LETTER
}
