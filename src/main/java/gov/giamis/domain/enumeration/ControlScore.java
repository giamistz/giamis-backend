package gov.giamis.domain.enumeration;

/**
 * The ControlScore enumeration.
 */
public enum ControlScore {
    SAME_AS_NARRATED, NOT_AS_NARRATED
}
