package gov.giamis.domain.enumeration;

/**
 * The RiskSource enumeration.
 */
public enum RiskSource {
    MANAGEMENT, AUDITOR, ENGAGEMENT
}
