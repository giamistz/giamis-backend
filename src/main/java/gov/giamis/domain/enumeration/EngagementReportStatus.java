package gov.giamis.domain.enumeration;

/**
 * The EngagementReportStatus enumeration.
 */
public enum EngagementReportStatus {
    PREPARATION, DRAFT, FINAL
}
