package gov.giamis.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.engagement.EngagementProcedure;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A EngagementSampling.
 */
@Entity
@Table(name = "engagement_samplings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementSampling extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "population_size", nullable = false)
    private Integer populationSize;

    @NotNull
    @Column(name = "sample_size", nullable = false)
    private Integer sampleSize;

    @Size(max = 250)
    @Column(name = "remarks", length = 250)
    private String remarks;

    @Column(name = "sampling_type")
    private Boolean samplingType;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("engFindingRecommendations")
    private EngagementProcedure engagementProcedure;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPopulationSize() {
        return populationSize;
    }

    public EngagementSampling populationSize(Integer populationSize) {
        this.populationSize = populationSize;
        return this;
    }

    public void setPopulationSize(Integer populationSize) {
        this.populationSize = populationSize;
    }

    public Integer getSampleSize() {
        return sampleSize;
    }

    public EngagementSampling sampleSize(Integer sampleSize) {
        this.sampleSize = sampleSize;
        return this;
    }

    public void setSampleSize(Integer sampleSize) {
        this.sampleSize = sampleSize;
    }

    public String getRemarks() {
        return remarks;
    }

    public EngagementSampling remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isSamplingType() {
        return samplingType;
    }

    public EngagementSampling samplingType(Boolean samplingType) {
        this.samplingType = samplingType;
        return this;
    }

    public void setSamplingType(Boolean samplingType) {
        this.samplingType = samplingType;
    }

    public EngagementProcedure getEngagementProcedure() {
        return engagementProcedure;
    }

    public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
        this.engagementProcedure = engagementProcedure;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementSampling)) {
            return false;
        }
        return id != null && id.equals(((EngagementSampling) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementSampling{" +
            "id=" + getId() +
            ", populationSize=" + getPopulationSize() +
            ", sampleSize=" + getSampleSize() +
            ", remarks='" + getRemarks() + "'" +
            ", samplingType='" + isSamplingType() + "'" +
            "}";
    }
}
