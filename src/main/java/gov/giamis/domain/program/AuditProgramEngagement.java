package gov.giamis.domain.program;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.domain.setup.OrganisationUnitLevel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A AuditProgramEngagement.
 */
@Entity
@Table(name = "audit_program_engagements")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditProgramEngagement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "process", nullable = false)
    private String process;

    @SafeHtml
    @Size(max = 200)
    @Column(name = "sub_process", length = 200, nullable = false)
    private String subProcess;

    @SafeHtml
    @Size(max = 200)
    @Column(name = "sub_sub_process", length = 200, nullable = false)
    private String subSubProcess;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditProgramEngagements")
    private AuditableArea auditableArea;

    @OneToMany(
        fetch = FetchType.EAGER, orphanRemoval = true)
    @JsonIgnoreProperties({
        "auditProgramEngagements",
        "isRiskRegisterLevel",
        "isRiskOwnerLevel",
        "isAuditableLevel",
        "code"
    })
    @JoinTable(name = "audit_program_engagement_org_unit_levels",
        joinColumns = @JoinColumn(
            name = "audit_program_engagement_id",
            referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "organisation_unit_level_id",
            referencedColumnName = "id"))
    private Set<OrganisationUnitLevel> organisationUnitLevels = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcess() {
        return process;
    }

    public AuditProgramEngagement name(String name) {
        this.process = name;
        return this;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public AuditableArea getAuditableArea() {
        return auditableArea;
    }

    public AuditProgramEngagement auditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
        return this;
    }

    public String getSubProcess() {
        return subProcess;
    }

    public void setSubProcess(String subProcess) {
        this.subProcess = subProcess;
    }

    public String getSubSubProcess() {
        return subSubProcess;
    }

    public void setSubSubProcess(String subSubProcess) {
        this.subSubProcess = subSubProcess;
    }

    public void setAuditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
    }

    public Set<OrganisationUnitLevel> getOrganisationUnitLevels() {
        return organisationUnitLevels;
    }

    public AuditProgramEngagement organisationUnitLevel(Set<OrganisationUnitLevel> organisationUnitLevel) {
        this.organisationUnitLevels = organisationUnitLevel;
        return this;
    }

    public void setOrganisationUnitLevels(Set<OrganisationUnitLevel> organisationUnitLevels) {
        this.organisationUnitLevels = organisationUnitLevels;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditProgramEngagement)) {
            return false;
        }
        return id != null && id.equals(((AuditProgramEngagement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditProgramEngagement{" +
            "id=" + getId() +
            ", name='" + getProcess() + "'" +
            " subProcess=" + subProcess + "," +
            " subSubProcess=" + subSubProcess + "," +
            "}";
    }
}
