package gov.giamis.domain.program;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AuditProgramProcedure.
 */
@Entity
@Table(name = "audit_program_procedures")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditProgramProcedure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditProgramProcedures")
    private AuditProgramControl auditProgramControl;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AuditProgramProcedure name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuditProgramControl getAuditProgramControl() {
        return auditProgramControl;
    }

    public AuditProgramProcedure auditProgramControl(AuditProgramControl auditProgramControl) {
        this.auditProgramControl = auditProgramControl;
        return this;
    }

    public void setAuditProgramControl(AuditProgramControl auditProgramControl) {
        this.auditProgramControl = auditProgramControl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditProgramProcedure)) {
            return false;
        }
        return id != null && id.equals(((AuditProgramProcedure) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditProgramProcedure{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
