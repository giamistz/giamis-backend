package gov.giamis.domain.program;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AuditProgramControl.
 */
@Entity
@Table(name = "audit_program_controls")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditProgramControl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "step_number")
    private Integer stepNumber;

    @Column(name = "area_of_responsibility")
    private String areaOfResponsibility;

    @Column(name = "criteria")
    private String criteria;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditProgramControls")
    private AuditProgramEngagement auditProgramEngagement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AuditProgramControl name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public AuditProgramControl stepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
        return this;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getAreaOfResponsibility() {
        return areaOfResponsibility;
    }

    public AuditProgramControl areaOfResponsibility(String areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
        return this;
    }

    public void setAreaOfResponsibility(String areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
    }

    public String getCriteria() {
        return criteria;
    }

    public AuditProgramControl criteria(String criteria) {
        this.criteria = criteria;
        return this;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public AuditProgramEngagement getAuditProgramEngagement() {
        return auditProgramEngagement;
    }

    public AuditProgramControl auditProgramRisk(AuditProgramEngagement auditProgramEngagement) {
        this.auditProgramEngagement = auditProgramEngagement;
        return this;
    }

    public void setAuditProgramEngagement(AuditProgramEngagement auditProgramEngagement) {
        this.auditProgramEngagement = auditProgramEngagement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditProgramControl)) {
            return false;
        }
        return id != null && id.equals(((AuditProgramControl) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditProgramControl{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stepNumber=" + getStepNumber() +
            ", areaOfResponsibility='" + getAreaOfResponsibility() + "'" +
            ", criteria='" + getCriteria() + "'" +
            "}";
    }
}
