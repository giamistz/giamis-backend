package gov.giamis.domain.program;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AuditProgramRisk.
 */
@Entity
@Table(name = "audit_program_risks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditProgramRisk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditProgramRisks")
    private AuditProgramEngagement auditProgramEngagement;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditProgramRisks")
    private AuditProgramObjective auditProgramObjective;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AuditProgramRisk name(String name) {
        this.name = name;
        return this;
    }

    public AuditProgramEngagement getAuditProgramEngagement() {
        return auditProgramEngagement;
    }

    public void setAuditProgramEngagement(AuditProgramEngagement auditProgramEngagement) {
        this.auditProgramEngagement = auditProgramEngagement;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuditProgramObjective getAuditProgramObjective() {
        return auditProgramObjective;
    }

    public AuditProgramRisk auditProgramObjective(AuditProgramObjective auditProgramObjective) {
        this.auditProgramObjective = auditProgramObjective;
        return this;
    }

    public void setAuditProgramObjective(AuditProgramObjective auditProgramObjective) {
        this.auditProgramObjective = auditProgramObjective;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditProgramRisk)) {
            return false;
        }
        return id != null && id.equals(((AuditProgramRisk) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditProgramRisk{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", auditProgramObjective='" + auditProgramObjective + "'" +
            ", auditProgramEngagement='" + auditProgramEngagement + "'" +
            "}";
    }
}
