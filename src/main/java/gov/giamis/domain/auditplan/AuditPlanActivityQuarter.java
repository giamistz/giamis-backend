package gov.giamis.domain.auditplan;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.Quarter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AuditPlanActivityQuarter.
 */
@Entity
@Table(name = "audit_plan_activity_quarters")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditPlanActivityQuarter
    extends AbstractAuditingEntity
    implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "total_days", nullable = false)
    private Integer totalDays;

    @ManyToOne
    @JoinColumn(name = "audit_plan_activity_id")
    @JsonIgnoreProperties("auditPlanActivityQuarters")
    private AuditPlanActivity auditPlanActivity;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"auditPlanActivityQuarters","financialYear"})
    private Quarter quarter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalDays() {
        return totalDays;
    }

    public AuditPlanActivityQuarter totalDays(Integer totalDays) {
        this.totalDays = totalDays;
        return this;
    }

    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }

    public AuditPlanActivity getAuditPlanActivity() {
        return auditPlanActivity;
    }

    public AuditPlanActivityQuarter auditPlanSchedule(AuditPlanActivity auditPlanActivity) {
        this.auditPlanActivity = auditPlanActivity;
        return this;
    }

    public void setAuditPlanActivity(AuditPlanActivity auditPlanActivity) {
        this.auditPlanActivity = auditPlanActivity;
    }

    public Quarter getQuarter() {
        return quarter;
    }

    public AuditPlanActivityQuarter quarter(Quarter quarter) {
        this.quarter = quarter;
        return this;
    }

    public void setQuarter(Quarter quarter) {
        this.quarter = quarter;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditPlanActivityQuarter)) {
            return false;
        }
        return id != null && id.equals(((AuditPlanActivityQuarter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditPlanActivityQuarter{" +
            "id=" + getId() +
            ", totalDays=" + getTotalDays() +
            "}";
    }
}
