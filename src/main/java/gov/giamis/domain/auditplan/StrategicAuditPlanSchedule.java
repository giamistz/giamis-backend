package gov.giamis.domain.auditplan;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.risk.Risk;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Set;

/**
 * A StrategicAuditPlanSchedule.
 */
@Entity
@Table(name = "strategic_audit_plan_schedules")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StrategicAuditPlanSchedule
    extends AbstractAuditingEntity
    implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties({
        "strategicPlanSchedules",
        "purpose",
        "objective",
        "methodology",
        "financialResources",
        "organisationUnit",
        "startFinancialYear",
        "endFinancialYear"
    })
    private StrategicAuditPlan strategicAuditPlan;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "strategicPlanSchedules",
        "engagements",
        "address",
        "background",
        "code",
        "contactPerson",
        "email",
        "fileResource",
        "organisationUnitGroups",
        "organisationUnitLevel",
        "parentOrganisationUnit",
        "phoneNumber"
    })
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "code",
        "strategicPlanSchedules",
        "organisationUnitGroups"
    })
    private AuditableArea auditableArea;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "strategicPlanSchedules",
        "source",
        "causes",
        "consequences",
        "treatments",
        "resources",
        "completed",
        "controls",
        "riskRegister",
        "organisationUnit",
        "riskCategory"
    })
    private Risk risk;

    @OneToMany( mappedBy = "strategicAuditPlanSchedule",
        cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonIgnoreProperties({
        "strategicAuditPlanSchedule",

    })
    private Set<StrategicAuditPlanScheduleDay> scheduleDays;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StrategicAuditPlan getStrategicAuditPlan() {
        return strategicAuditPlan;
    }

    public StrategicAuditPlanSchedule strategicPlan(StrategicAuditPlan strategicAuditPlan) {
        this.strategicAuditPlan = strategicAuditPlan;
        return this;
    }

    public void setStrategicAuditPlan(StrategicAuditPlan strategicAuditPlan) {
        this.strategicAuditPlan = strategicAuditPlan;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public StrategicAuditPlanSchedule organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public AuditableArea getAuditableArea() {
        return auditableArea;
    }

    public StrategicAuditPlanSchedule auditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
        return this;
    }

    public void setAuditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
    }

    public Risk getRisk() {
        return risk;
    }

    public StrategicAuditPlanSchedule risk(Risk risk) {
        this.risk = risk;
        return this;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }

    public Set<StrategicAuditPlanScheduleDay> getScheduleDays() {
        return scheduleDays;
    }

    public void setScheduleDays(Set<StrategicAuditPlanScheduleDay> scheduleDays) {
        this.scheduleDays = scheduleDays;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StrategicAuditPlanSchedule)) {
            return false;
        }
        return id != null && id.equals(((StrategicAuditPlanSchedule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "StrategicAuditPlanSchedule{" +
            "id=" + getId() +
            "}";
    }
}
