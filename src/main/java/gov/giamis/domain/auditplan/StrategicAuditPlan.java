package gov.giamis.domain.auditplan;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;
import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A StrategicAuditPlan.
 */
@Entity
@Table(name = "strategic_audit_plans")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StrategicAuditPlan extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
    @SafeHtml
    private String name;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "purpose", nullable = true)
    private String purpose;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "objective", nullable = true)
    private String objective;


    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "methodology", nullable = true)
    private String methodology;


    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "financial_resources", nullable = true)
    private String financialResources;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "background", nullable = true)
    private String background;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("strategicPlans")
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("strategicPlans")
    private FinancialYear startFinancialYear;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("strategicPlans")
    private FinancialYear endFinancialYear;

    private Boolean approved = false;

    private String approvedBy;

    private LocalDate approvedDate;

    @ManyToOne
    @JoinColumn(name = "file_resource_id", referencedColumnName = "id")
    @JsonIgnoreProperties({
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPurpose() {
        return purpose;
    }

    public StrategicAuditPlan purpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getObjective() {
        return objective;
    }

    public StrategicAuditPlan objective(String objective) {
        this.objective = objective;
        return this;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getMethodology() {
        return methodology;
    }

    public StrategicAuditPlan methodology(String methodology) {
        this.methodology = methodology;
        return this;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getFinancialResources() {
        return financialResources;
    }

    public StrategicAuditPlan financialResources(String financialResources) {
        this.financialResources = financialResources;
        return this;
    }

    public void setFinancialResources(String financialResources) {
        this.financialResources = financialResources;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public StrategicAuditPlan organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public FinancialYear getStartFinancialYear() {
        return startFinancialYear;
    }

    public StrategicAuditPlan startFinancialYear(FinancialYear financialYear) {
        this.startFinancialYear = financialYear;
        return this;
    }

    public void setStartFinancialYear(FinancialYear financialYear) {
        this.startFinancialYear = financialYear;
    }

    public FinancialYear getEndFinancialYear() {
        return endFinancialYear;
    }

    public StrategicAuditPlan endFinancialYear(FinancialYear financialYear) {
        this.endFinancialYear = financialYear;
        return this;
    }

    public void setEndFinancialYear(FinancialYear financialYear) {
        this.endFinancialYear = financialYear;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StrategicAuditPlan)) {
            return false;
        }
        return id != null && id.equals(((StrategicAuditPlan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "StrategicAuditPlan{" +
            "id=" + getId() +
            ", purpose='" + getPurpose() + "'" +
            ", objective='" + getObjective() + "'" +
            ", methodology='" + getMethodology() + "'" +
            ", financialResources='" + getFinancialResources() + "'" +
            ", background='" + background + "'" +
            "}";
    }
}
