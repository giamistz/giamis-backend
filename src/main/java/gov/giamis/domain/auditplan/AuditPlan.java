package gov.giamis.domain.auditplan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A AuditPlan.
 */
@Entity
@Table(name = "audit_plans")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditPlan
    extends AbstractAuditingEntity
    implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
    @SafeHtml
    private String name;

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
    @Column(name = "purpose", nullable = false)
    private String purpose;

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
    @Column(name = "objective", nullable = false)
    private String objective;

    private Boolean approved = false;

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
    @Column(name = "methodology", nullable = false)
    private String methodology;

    @SafeHtml
	@Size(max = 2000)
    @Column(name = "list_of_abbreviation",length = 2000)
    private String listOfAbbreviation;

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
    @Column(name = "other_resources")
    private String otherResources;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "background")
    private String background;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties({
        "auditPlans",
        "engagements",
        "address",
        "background",
        "code",
        "contactPerson",
        "email",
        "fileResource",
        "organisationUnitGroups",
        "organisationUnitLevel",
        "parentOrganisationUnit",
        "phoneNumber"
    })
    private OrganisationUnit organisationUnit;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties({
        "auditPlans",
        "endDate",
        "startDate",
        "status"
    })
    private FinancialYear financialYear;

    private String approvedBy;

    private LocalDate approvedDate;

    @ManyToOne
    @JoinColumn(name = "file_resource_id", referencedColumnName = "id")
    @JsonIgnoreProperties({
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AuditPlan name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurpose() {
        return purpose;
    }

    public AuditPlan purpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getObjective() {
        return objective;
    }

    public AuditPlan objective(String objective) {
        this.objective = objective;
        return this;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getMethodology() {
        return methodology;
    }

    public AuditPlan methodology(String methodology) {
        this.methodology = methodology;
        return this;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getListOfAbbreviation() {
        return listOfAbbreviation;
    }

    public AuditPlan listOfAbbreviation(String listOfAbbreviation) {
        this.listOfAbbreviation = listOfAbbreviation;
        return this;
    }

    public void setListOfAbbreviation(String listOfAbbreviation) {
        this.listOfAbbreviation = listOfAbbreviation;
    }

    public String getOtherResources() {
        return otherResources;
    }

    public AuditPlan otherResources(String otherResources) {
        this.otherResources = otherResources;
        return this;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public void setOtherResources(String otherResources) {
        this.otherResources = otherResources;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public AuditPlan organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public AuditPlan financialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
        return this;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditPlan)) {
            return false;
        }
        return id != null && id.equals(((AuditPlan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditPlan{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", purpose='" + getPurpose() + "'" +
            ", objective='" + getObjective() + "'" +
            ", methodology='" + getMethodology() + "'" +
            ", listOfAbbreviation='" + getListOfAbbreviation() + "'" +
            ", otherResources='" + getOtherResources() + "'" +
            ", background='" + background + "'" +
            "}";
    }
}
