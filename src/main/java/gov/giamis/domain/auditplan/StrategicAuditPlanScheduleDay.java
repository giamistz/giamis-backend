package gov.giamis.domain.auditplan;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FinancialYear;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A StrategicAuditPlanScheduleDay.
 */
@Entity
@Table(name = "strategic_audit_plan_schedule_days")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StrategicAuditPlanScheduleDay
    extends AbstractAuditingEntity
    implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "total_days", nullable = false)
    private Integer totalDays;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("strategicPlanScheduleDays")
    private StrategicAuditPlanSchedule strategicAuditPlanSchedule;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "name" ,
        "strategicPlanScheduleDays",
        "startDate",
        "endDate",
        "status"
    })
    private FinancialYear financialYear;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalDays() {
        return totalDays;
    }

    public StrategicAuditPlanScheduleDay totalDays(Integer totalDays) {
        this.totalDays = totalDays;
        return this;
    }

    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }

    public StrategicAuditPlanSchedule getStrategicAuditPlanSchedule() {
        return strategicAuditPlanSchedule;
    }

    public StrategicAuditPlanScheduleDay strategicPlanSchedule(StrategicAuditPlanSchedule strategicAuditPlanSchedule) {
        this.strategicAuditPlanSchedule = strategicAuditPlanSchedule;
        return this;
    }

    public void setStrategicAuditPlanSchedule(StrategicAuditPlanSchedule strategicAuditPlanSchedule) {
        this.strategicAuditPlanSchedule = strategicAuditPlanSchedule;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public StrategicAuditPlanScheduleDay financialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
        return this;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StrategicAuditPlanScheduleDay)) {
            return false;
        }
        return id != null && id.equals(((StrategicAuditPlanScheduleDay) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "StrategicAuditPlanScheduleDay{" +
            "id=" + getId() +
            ", totalDays=" + getTotalDays() +
            "}";
    }
}
