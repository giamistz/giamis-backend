package gov.giamis.domain.auditplan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.GfsCode;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A AuditPlanActivityInput.
 */
@Entity
@Table(name = "audit_plan_activity_inputs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditPlanActivityInput extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotNull
	@Column(name = "quantity", nullable = false)
	private Float quantity;

	@NotNull
	@Column(name = "frequency", nullable = false)
	private Float frequency;

	@NotNull
	@Column(name = "unit_price", nullable = false)
	private Double unitPrice;
	
	@ManyToOne
	@JoinColumn(name = "audit_plan_activity_id")
	@JsonIgnoreProperties("auditPlanActivityInputs")
	private AuditPlanActivity auditPlanActivity;

    public GfsCode getGfsCode() {
        return gfsCode;
    }

    public void setGfsCode(GfsCode gfsCode) {
        this.gfsCode = gfsCode;
    }

    @ManyToOne(optional = false)
    @NotNull
    private GfsCode gfsCode;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getQuantity() {
		return quantity;
	}

	public AuditPlanActivityInput quantity(Float quantity) {
		this.quantity = quantity;
		return this;
	}

	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}

	public Float getFrequency() {
		return frequency;
	}

	public AuditPlanActivityInput frequency(Float frequency) {
		this.frequency = frequency;
		return this;
	}

	public void setFrequency(Float frequency) {
		this.frequency = frequency;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public AuditPlanActivityInput unitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
		return this;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public AuditPlanActivity getAuditPlanActivity() {
		return auditPlanActivity;
	}

	public AuditPlanActivityInput quarter(AuditPlanActivity auditPlanActivity) {
		this.auditPlanActivity = auditPlanActivity;
		return this;
	}

	public void setAuditPlanActivity(AuditPlanActivity auditPlanActivity) {
		this.auditPlanActivity = auditPlanActivity;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AuditPlanActivityInput)) {
			return false;
		}
		return id != null && id.equals(((AuditPlanActivityInput) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "AuditPlanActivityInput{" + "id=" + getId() + ", quantity=" + getQuantity() + ", frequency="
				+ getFrequency() + ", unitPrice=" + getUnitPrice() + "}";
	}
}
