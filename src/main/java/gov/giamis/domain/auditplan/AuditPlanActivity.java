package gov.giamis.domain.auditplan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.domain.risk.Risk;
import gov.giamis.domain.risk.Target;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.domain.setup.Objective;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A AuditPlanActivity.
 */
@Entity
@Table(name = "audit_plan_activities")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditPlanActivity
    extends AbstractAuditingEntity
    implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
    @SafeHtml
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "total_days", nullable = false)
    private Integer totalDays;

    @OneToMany(mappedBy = "auditPlanActivity",
        cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"auditPlanActivity", "startDate", "endDate"})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AuditPlanActivityQuarter> quarters = new HashSet<>();

    @OneToMany(mappedBy = "auditPlanActivity",
            cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"auditPlanActivity", "startDate", "endDate"})
   //  @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AuditPlanActivityInput> auditPlanActivityInputs = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private AuditPlan auditPlan;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditPlanActivities")
    private Risk risk;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditPlanActivities")
    private AuditableArea auditableArea;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("auditPlanActivities")
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = true)
    private Target target;

    @ManyToOne(optional = true)
    private Objective objective;

    @ManyToOne()
    @JoinColumn(name ="audit_program_engagement_id")
    @JsonIgnoreProperties({
        "auditableArea",
        "organisationUnitLevels"
    })
    private AuditProgramEngagement auditProgramEngagement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public AuditPlanActivity description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotalDays() {
        return totalDays;
    }

    public AuditPlanActivity totalDays(Integer totalDays) {
        this.totalDays = totalDays;
        return this;
    }

    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }

    public Set<AuditPlanActivityQuarter> getQuarters() {
        return quarters;
    }

    public AuditPlanActivity auditPlanActivityQuarters(
        Set<AuditPlanActivityQuarter> quarters) {

        this.quarters = quarters;
        return this;
    }

    public AuditPlanActivity addAuditPlanActivityQuarters(
        AuditPlanActivityQuarter quarters) {

        this.quarters.add(quarters);
        quarters.setAuditPlanActivity(this);
        return this;
    }

    public AuditPlanActivity removeAuditPlanActivityQuarters(
        AuditPlanActivityQuarter quarters) {

        this.quarters.remove(quarters);
        quarters.setAuditPlanActivity(null);
        return this;
    }

    public void setAuditPlanActivityQuarters(
        Set<AuditPlanActivityQuarter> quarters) {

        this.quarters = quarters;
    }

    public Set<AuditPlanActivityInput> getAuditPlanActivityInputs() {
        return auditPlanActivityInputs;
    }

    public AuditPlanActivity auditPlanActivityInputs(
        Set<AuditPlanActivityInput> auditPlanActivityInputs) {

        this.auditPlanActivityInputs = auditPlanActivityInputs;
        return this;
    }

    public AuditPlanActivity addAuditPlanActivityInputs(
        AuditPlanActivityInput auditPlanActivityInput) {

        this.auditPlanActivityInputs.add(auditPlanActivityInput);
        auditPlanActivityInput.setAuditPlanActivity(this);
        return this;
    }

    public AuditPlanActivity removeAuditPlanActivityInputs(
        AuditPlanActivityInput auditPlanActivityInput) {

        this.auditPlanActivityInputs.remove(auditPlanActivityInput);
        auditPlanActivityInput.setAuditPlanActivity(null);
        return this;
    }

    public void setAuditPlanActivityInputs(
        Set<AuditPlanActivityInput> auditPlanActivityInputs) {

        this.auditPlanActivityInputs = auditPlanActivityInputs;
    }

    public AuditPlan getAuditPlan() {
        return auditPlan;
    }

    public AuditPlanActivity auditPlan(AuditPlan auditPlan) {
        this.auditPlan = auditPlan;
        return this;
    }

    public void setAuditPlan(AuditPlan auditPlan) {
        this.auditPlan = auditPlan;
    }

    public Risk getRisk() {
        return risk;
    }

    public AuditPlanActivity risk(Risk risk) {
        this.risk = risk;
        return this;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }

    public AuditableArea getAuditableArea() {
        return auditableArea;
    }

    public AuditPlanActivity auditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
        return this;
    }

    public void setAuditableArea(AuditableArea auditableArea) {
        this.auditableArea = auditableArea;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public AuditPlanActivity organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public Objective getObjective() {
		return objective;
	}

	public void setObjective(Objective objective) {
		this.objective = objective;
	}

    public AuditProgramEngagement getAuditProgramEngagement() {
        return auditProgramEngagement;
    }

    public void setAuditProgramEngagement(AuditProgramEngagement auditProgramEngagement) {
        this.auditProgramEngagement = auditProgramEngagement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuditPlanActivity)) {
            return false;
        }
        return id != null && id.equals(((AuditPlanActivity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AuditPlanActivity{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", totalDays=" + getTotalDays() +
            "}";
    }
}
