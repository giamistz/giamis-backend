package gov.giamis.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Notification.
 */
@Entity
@Table(name = "notifications")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    public Notification(){}

    public Notification(String email, String body, String subject, FileResource fileResource) {

        this.email = email;
        this.body = body;
        this.subject = subject;
        this.fileResource = fileResource;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "body")
    private String body;

    @NotNull
    @Column(name = "subject", nullable = false)
    private String subject;

    @Column(name = "is_sent")
    private Boolean isSent = false;

    @Column(name = "is_read")
    private Boolean isRead = false;

    @ManyToOne
    @JsonIgnoreProperties("notifications")
    private FileResource fileResource;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Notification email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public Notification body(String body) {
        this.body = body;
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public Notification subject(String subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean isIsSent() {
        return isSent;
    }

    public Notification isSent(Boolean isSent) {
        this.isSent = isSent;
        return this;
    }

    public void setIsSent(Boolean isSent) {
        this.isSent = isSent;
    }

    public Boolean isIsRead() {
        return isRead;
    }

    public Notification isRead(Boolean isRead) {
        this.isRead = isRead;
        return this;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public Notification fileResource(FileResource fileResource) {
        this.fileResource = fileResource;
        return this;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", body='" + getBody() + "'" +
            ", subject='" + getSubject() + "'" +
            ", isSent='" + isIsSent() + "'" +
            ", isRead='" + isIsRead() + "'" +
            "}";
    }
}
