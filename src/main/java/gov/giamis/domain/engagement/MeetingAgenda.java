package gov.giamis.domain.engagement;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Table(name = "meeting_agendas")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MeetingAgenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "agenda", nullable = true)
    private String agenda;

    @ManyToOne(optional = true)
    private EngagementTeamMeeting engagementTeamMeeting;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public EngagementTeamMeeting getEngagementTeamMeeting() {
        return engagementTeamMeeting;
    }

    public void setEngagementTeamMeeting(EngagementTeamMeeting engagementTeamMeeting) {
        this.engagementTeamMeeting = engagementTeamMeeting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeetingAgenda)) {
            return false;
        }
        return id != null && id.equals(((MeetingAgenda) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MeetingAgenda [id=" + id + ", agenda=" + agenda + ", engagementTeamMeeting="
                + engagementTeamMeeting + "]";
    }

}
