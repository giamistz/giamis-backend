package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import gov.giamis.domain.AbstractAuditingEntity;
@Entity
@Table(name = "engagement_exit_meetings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementExitMeeting extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "meeting_date")
    private LocalDate meetingDate;

    @SafeHtml
    @Size(max = 50)
    @Column(name = "venue", length = 50)
    private String venue;

    @ManyToOne(fetch = FetchType.LAZY)
    private Engagement engagement;

    @ManyToMany(fetch =FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "engagement_exit_meeting_members",
            joinColumns = @JoinColumn(name = "engagement_exit_meeting_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "engagement_member_id", referencedColumnName = "id"))
     private Set<EngagementExitMember> engagementExitMember = new HashSet<>();


    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "remarks")
    private String remarks;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "comments")
    private String comments;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion")
    private String conclusion;

    @Column(name = "reviewed_by", length = 50)
    private String reviewedBy;

    @Column(name = "reviewed_date", length = 50)
    private LocalDate reviewedDate;

    @Column(name = "approved_by", length = 50)
    private String approvedBy;

    @Column(name = "approved_date", length = 50)
    private LocalDate approvedDate;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties({
        "organisationUnits",
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(LocalDate meetingDate) {
		this.meetingDate = meetingDate;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Engagement getEngagement() {
		return engagement;
	}

	public void setEngagement(Engagement engagement) {
		this.engagement = engagement;
	}

	public Set<EngagementExitMember> getEngagementExitMember() {
		return engagementExitMember;
	}

	public void setEngagementExitMember(Set<EngagementExitMember> engagementExitMember) {
		this.engagementExitMember = engagementExitMember;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

    public FileResource getFileResource() {
        return fileResource;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public LocalDate getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(LocalDate reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public LocalDate getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDate approvedDate) {
		this.approvedDate = approvedDate;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementMeeting)) {
            return false;
        }
        return id != null && id.equals(((EngagementExitMeeting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "EngagementExitMeeting [id=" + id + ", meetingDate=" + meetingDate + ", venue=" + venue + ", engagement="
				+ engagement + ", engagementExitMember=" + engagementExitMember + ", remarks=" + remarks + ", comments="
				+ comments + ", conclusion=" + conclusion + ", reviewedBy=" + reviewedBy + ", reviewedDate="
				+ reviewedDate + ", approvedBy=" + approvedBy + ", approvedDate=" + approvedDate + "]";
	}
}
