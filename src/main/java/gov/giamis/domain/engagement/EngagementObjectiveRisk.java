package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.risk.Risk;
import gov.giamis.domain.risk.RiskControlMatrix;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A EngagementObjectiveRisk.
 */
@Entity
@Table(name = "engagement_objective_risks")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementObjectiveRisk extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String code;

    private String description;

    private Integer impact;

    private Integer likelihood;

    @ManyToOne(optional = false)
    @NotNull
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnoreProperties({
        "engagementObjectiveRisks"
    })
    private SpecificObjective specificObjective;

    @OneToMany(mappedBy = "engagementObjectiveRisk")
    @JsonIgnoreProperties({"engagementObjectiveRisk"})
    private List<RiskControlMatrix> riskControlMatrices = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getImpact() {
        return impact;
    }

    public void setImpact(Integer impact) {
        this.impact = impact;
    }

    public Integer getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(Integer likelihood) {
        this.likelihood = likelihood;
    }

    public List<RiskControlMatrix> getRiskControlMatrices() {
        return riskControlMatrices;
    }

    public void setRiskControlMatrices(List<RiskControlMatrix> riskControlMatrices) {
        this.riskControlMatrices = riskControlMatrices;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SpecificObjective getSpecificObjective() {
        return specificObjective;
    }

    public EngagementObjectiveRisk engagementObjective(SpecificObjective specificObjective) {
        this.specificObjective = specificObjective;
        return this;
    }

    public void setSpecificObjective(SpecificObjective specificObjective) {
        this.specificObjective = specificObjective;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementObjectiveRisk)) {
            return false;
        }
        return id != null && id.equals(((EngagementObjectiveRisk) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "EngagementObjectiveRisk [id=" + id + "," +
            " specificObjective=" + specificObjective
				+ "]";
	}

}
