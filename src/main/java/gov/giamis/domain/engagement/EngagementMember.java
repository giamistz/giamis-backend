package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.SimpleUser;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.enumeration.EngagementRole;

/**
 * A EngagementMember.
 */
@Entity
@Table(name = "engagement_members")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementMember extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private EngagementRole role;

    @Column(name = "accepted_nda")
    private Boolean acceptedNda = false;

    @Column(name = "acceptance_date")
    private LocalDate acceptanceDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "code",
        "description",
        "auditType",
        "scope",
        "startDate",
        "endDate",
        "isGo",
        "financialYear",
        "auditableArea",
        "engagementPhase",
        "engagementResources",
        "organisationUnit",
        "parentEngagement",
        "engagementTeamMeeting",
        "engagementExitMeeting",
        "engagementMembers"
    })
    private Engagement engagement;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "engagementMembers",
        "authorities",
        "roles",
        "menus",
        "organisationUnit",
        "organisationUnitContactPerson",
        "auditorProfile",
        "langKey",
        "imageUrl",
        "resetDate"
    })
    private SimpleUser user;

    @ManyToOne()
    @JoinColumn(name = "letter_file_resource_id")
    @JsonIgnoreProperties({"name","uuid","path","tags","contentMd5","contentType","fileResourceType"})
    private FileResource letterFileResource;

    @ManyToOne()
    @JsonIgnoreProperties({"name","uuid","path","tags","contentMd5","contentType","fileResourceType"})
    @JoinColumn(name = "nda_file_resource_id")
    private FileResource ndaFileResource;
    
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EngagementRole getRole() {
        return role;
    }

    public EngagementMember role(EngagementRole role) {
        this.role = role;
        return this;
    }

    public void setRole(EngagementRole role) {
        this.role = role;
    }

    public Boolean isAcceptedNda() {
        return acceptedNda;
    }

    public EngagementMember acceptedNda(Boolean acceptedNda) {
        this.acceptedNda = acceptedNda;
        return this;
    }

    public void setAcceptedNda(Boolean acceptedNda) {
        this.acceptedNda = acceptedNda;
    }

    public LocalDate getAcceptanceDate() {
        return acceptanceDate;
    }

    public EngagementMember acceptanceDate(LocalDate acceptanceDate) {
        this.acceptanceDate = acceptanceDate;
        return this;
    }

    public void setAcceptanceDate(LocalDate acceptanceDate) {
        this.acceptanceDate = acceptanceDate;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public EngagementMember engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    public SimpleUser getUser() {
        return user;
    }

    public EngagementMember user(SimpleUser user) {
        this.user = user;
        return this;
    }

    public void setUser(SimpleUser user) {
        this.user = user;
    }

    public FileResource getLetterFileResource() {
        return letterFileResource;
    }

    public void setLetterFileResource(FileResource letterFileResource) {
        this.letterFileResource = letterFileResource;
    }

    public FileResource getNdaFileResource() {
        return ndaFileResource;
    }

    public void setNdaFileResource(FileResource ndaFileResource) {
        this.ndaFileResource = ndaFileResource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementMember)) {
            return false;
        }
        return id != null && id.equals(((EngagementMember) o).id);
    }


	@Override
    public int hashCode() {
        return 31;
    }

   
}
