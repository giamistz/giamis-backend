package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.program.AuditProgramControl;
import gov.giamis.domain.setup.ControlType;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.List;

import gov.giamis.domain.enumeration.ControlScore;
import org.hibernate.annotations.Where;
import org.springframework.stereotype.Component;

/**
 * A InternalControl.
 */
@Entity
@Table(name = "internal_controls")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InternalControl implements Serializable {

    private static final long serialVersionUID = 1L;

    public InternalControl(Long id) {
        this.id = id;
    }

    public InternalControl() {}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 1000)
    @Column(name = "name", length = 1000, nullable = false)
    private String name;

    @NotNull
    @Size(max = 100)
    @Column(name = "area_of_responsibility", length = 100, nullable = false)
    private String areaOfResponsibility;

    @NotNull
    @Column(name = "step_number", nullable = false)
    private Integer stepNumber;

    @Column(name = "criteria")
    private String criteria;

    @Enumerated(EnumType.STRING)
    @Column(name = "score")
    private ControlScore score;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("internalControls")
    private ControlType internalControlType;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("internalControls")
    private Engagement engagement;

    @Column(name = "is_control")
    private Boolean isControl;

    @Column(name = "document_name")
    private String documentName;

    @Column(name = "wt_ref")
    private String wtRef;

    @ManyToOne
    @JoinColumn(name = "audit_program_control_id")
    private AuditProgramControl auditProgramControl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public InternalControl name(String name) {
        this.name = name;
        return this;
    }
    public InternalControl areaOfResponsibility(String areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAreaOfResponsibility() {
        return areaOfResponsibility;
    }

    public void setAreaOfResponsibility(String areaOfResponsibility) {
        this.areaOfResponsibility = areaOfResponsibility;
    }


    public Integer getStepNumber() {
        return stepNumber;
    }

    public InternalControl stepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
        return this;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getCriteria() {
        return criteria;
    }

    public InternalControl criteria(String criteria) {
        this.criteria = criteria;
        return this;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public ControlScore getScore() {
        return score;
    }

    public InternalControl score(ControlScore score) {
        this.score = score;
        return this;
    }

    public void setScore(ControlScore score) {
        this.score = score;
    }

    public ControlType getInternalControlType() {
        return internalControlType;
    }

    public InternalControl internalControlType(ControlType controlType) {
        this.internalControlType = controlType;
        return this;
    }

    public void setInternalControlType(ControlType controlType) {
        this.internalControlType = controlType;
    }

    public InternalControl organisationUnit(OrganisationUnit organisationUnit) {
        return this;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public InternalControl engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public Boolean getControl() {
        return isControl;
    }

    public void setControl(Boolean control) {
        isControl = control;
    }
    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getWtRef() {
        return wtRef;
    }

    public void setWtRef(String wtRef) {
        this.wtRef = wtRef;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    public AuditProgramControl getAuditProgramControl() {
        return auditProgramControl;
    }

    public void setAuditProgramControl(AuditProgramControl auditProgramControl) {
        this.auditProgramControl = auditProgramControl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InternalControl)) {
            return false;
        }
        return id != null && id.equals(((InternalControl) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "InternalControl{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stepNumber=" + getStepNumber() +
            ", criteria='" + getCriteria() + "'" +
            ", score='" + getScore() + "'" +
            "}";
    }
}
