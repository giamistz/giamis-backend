package gov.giamis.domain.engagement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "engagement_team_meeting_members")
public class EngagementTeamMeetingMember {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "engagement_meeting_id")
    private Long engagementMeetingId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "engagement_member_id")
    @JsonIgnoreProperties({
        "engagement",
        "letterFileResource",
        "ndaFileResource",
        "acceptedNda",
        "acceptanceDate",
        "role"
    })
    private EngagementMember engagementMember;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEngagementMeetingId() {
        return engagementMeetingId;
    }

    public void setEngagementMeetingId(Long engagementMeetingId) {
        this.engagementMeetingId = engagementMeetingId;
    }

    public EngagementMember getEngagementMember() {
        return engagementMember;
    }

    public void setEngagementMember(EngagementMember engagementMember) {
        this.engagementMember = engagementMember;
    }
}
