package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import gov.giamis.domain.risk.RiskControlMatrix;
import gov.giamis.domain.setup.SimpleUser;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A EngagementProcedure.
 */
@Entity
@Table(name = "engagement_procedures")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementProcedure extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public EngagementProcedure(InternalControl control, String name, Boolean isWalkThrough) {
        this.internalControl = control;
        this.name = name;
        this.isWalkThrough = isWalkThrough;
    }

    public EngagementProcedure() {}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false)
    private String name;
    
    @SafeHtml
    @Size(max = 100)
    @Column(name = "code", length = 100, nullable = true)
    private String code;

    @Column(name = "date_planned", nullable = false)
    private LocalDate datePlanned;

    @Column(name = "date_completed", nullable = true)
    private LocalDate dateCompleted;

    @Column(name = "is_walk_through")
    private Boolean isWalkThrough = false;

    @ManyToOne()
    @JsonIgnoreProperties({
        "authorities",
        "menus",
        "roles",
        "activated",
        "langKey",
        "imageUrl",
        "resetDate",
        "organisationUnit",
        "organisationUnitContactPerson",
        "auditorProfile",
        "currentFinancialYear"
    })
    private SimpleUser user;

    @OneToMany(mappedBy = "engagementProcedure", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("engagementProcedure")
    private Set<EngagementItem> engagementItem = new HashSet<>();

    @OneToMany(mappedBy = "engagementProcedure", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("engagementProcedure")
    private Set<EngagementTestResult> engagementTestResult = new HashSet<>();

    @SafeHtml
    @Size(max = 50)
    @Column(name = "wprf", length = 50, nullable = true)
    private String wprf;

    @ManyToOne()
    @JoinColumn(name = "risk_control_matrix_id")
    private RiskControlMatrix riskControlMatrix;

    @ManyToOne()
    @JoinColumn(name = "internal_control_id")
    private InternalControl internalControl;

    public InternalControl getInternalControl() {
        return internalControl;
    }

    public LocalDate getDatePlanned() {
        return datePlanned;
    }

    public void setDatePlanned(LocalDate datePlanned) {
        this.datePlanned = datePlanned;
    }

    public LocalDate getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDate dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public SimpleUser getUser() {
        return user;
    }

    public void setUser(SimpleUser user) {
        this.user = user;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public EngagementProcedure name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

   
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

    public Boolean getWalkThrough() {
        return isWalkThrough;
    }

    public void setWalkThrough(Boolean walkThrough) {
        isWalkThrough = walkThrough;
    }

    public RiskControlMatrix getRiskControlMatrix() {
        return riskControlMatrix;
    }

    public void setRiskControlMatrix(RiskControlMatrix riskControlMatrix) {
        this.riskControlMatrix = riskControlMatrix;
    }

    public void setInternalControl(InternalControl internalControl) {
        this.internalControl = internalControl;
    }

    public EngagementProcedure specificObjective(SpecificObjective specificObjective) {
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Set<EngagementTestResult> getEngagementTestResult() {
		return engagementTestResult;
	}

	public void setEngagementTestResult(Set<EngagementTestResult> engagementTestResult) {
		this.engagementTestResult = engagementTestResult;
	}

	public Set<EngagementItem> getEngagementItem() {
		return engagementItem;
	}

	public void setEngagementItem(Set<EngagementItem> engagementItem) {
		this.engagementItem = engagementItem;
	}

	public String getWprf() {
		return wprf;
	}

	public void setWprf(String wprf) {
		this.wprf = wprf;
	}

    @Override
    public String toString() {
        return "EngagementProcedure{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", code='" + code + '\'' +
            ", datePlanned=" + datePlanned +
            ", dateCompleted=" + dateCompleted +
            ", user=" + user +
            ", engagementItem=" + engagementItem +
            ", engagementTestResult=" + engagementTestResult +
            ", wprf='" + wprf + '\'' +
            '}';
    }
}
