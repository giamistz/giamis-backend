package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import gov.giamis.domain.enumeration.LetterType;

/**
 * A CommunicationLetter.
 */
@Entity
@Table(name = "communication_letters")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CommunicationLetter implements Serializable {

    private static final long serialVersionUID = 1L;

    public CommunicationLetter() {}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "subject", nullable = false)
    private String subject;

    
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "body", nullable = false)
    private String body;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private LetterType type;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "engagement_id")
    @JsonIgnoreProperties({
        "process",
        "subProcess",
        "subSubProcess",
        "controlAdequacy",
        "engagementPhases",
        "auditProgramEngagement",
        "code",
        "description",
        "auditType",
        "scope",
        "startDate",
        "endDate",
        "isGo",
        "financialYear",
        "auditableArea",
        "engagementPhase",
        "engagementResources",
        "organisationUnit",
        "parentEngagement",
        "engagementTeamMeeting",
        "engagementExitMeeting",
        "engagementMembers"
    })
    private Engagement engagement;

    @ManyToOne
    @JoinColumn(name = "file_resource_id")
    @JsonIgnoreProperties({
        "fileResourceType",
        "uid",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    public CommunicationLetter(LetterType type, String subject, String body, Engagement e) {
        this.type = type;
        this.subject = subject;
        this.body = body;
        this.engagement = e;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public CommunicationLetter subject(String subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public CommunicationLetter body(String body) {
        this.body = body;
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LetterType getType() {
        return type;
    }

    public CommunicationLetter type(LetterType type) {
        this.type = type;
        return this;
    }

    public void setType(LetterType type) {
        this.type = type;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public CommunicationLetter engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public CommunicationLetter fileResource(FileResource fileResource) {
        this.fileResource = fileResource;
        return this;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommunicationLetter)) {
            return false;
        }
        return id != null && id.equals(((CommunicationLetter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CommunicationLetter{" +
            "id=" + getId() +
            ", subject='" + getSubject() + "'" +
            ", body='" + getBody() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
