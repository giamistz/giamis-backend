package gov.giamis.domain.engagement;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.AbstractAuditingEntity;

@Entity
@Table(name = "engagement_items")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementItem extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@SafeHtml
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "amount", nullable = true)
	private Double amount;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties({
        "organisationUnits",
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

	@ManyToOne(optional = true)
	@JsonIgnoreProperties("engagementItem")
	private EngagementProcedure engagementProcedure;

	@OneToMany(mappedBy = "engagementItem", cascade = CascadeType.ALL, orphanRemoval = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@JsonIgnoreProperties({"engagementItem"})
	private Set<EngagementTestResult> engagementTestResult = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

    public FileResource getFileResource() {
        return fileResource;
    }

    public EngagementItem fileResource(FileResource fileResource) {
        this.fileResource = fileResource;
        return this;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public EngagementProcedure getEngagementProcedure() {
		return engagementProcedure;
	}

	public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}

	public Set<EngagementTestResult> getEngagementTestResult() {
		return engagementTestResult;
	}

	public void setEngagementTestResult(Set<EngagementTestResult> engagementTestResult) {
		this.engagementTestResult = engagementTestResult;
	}
}
