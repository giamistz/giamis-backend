package gov.giamis.domain.engagement;

import gov.giamis.domain.AbstractAuditingEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
@Entity
@Table(name = "engagement_team_meetings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementTeamMeeting extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Column(name = "meeting_date", nullable = true)
	private LocalDate meetingDate;

	@NotBlank
	@SafeHtml
	@Size(max = 50)
	@Column(name = "venue", length = 50, nullable = false)
	private String venue;

	private String scope;

	private  String methodology;

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	private Engagement engagement;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "summary", nullable = false)
	private String summary;

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "any_other_issue", nullable = false)
	private String anyOtherIssue;

	@Column(name = "reviewed_by", nullable = true, length = 50)
	private String reviewedBy;

	@Column(name = "reviewed_date")
	private LocalDate reviewedDate;

	@ManyToOne(optional = true)
    @JoinColumn(name = "engagement_objective_id")
	private EngagementObjective engagementObjective;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(LocalDate meetingDate) {
		this.meetingDate = meetingDate;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

    public Engagement getEngagement() {
		return engagement;
	}

	public void setEngagement(Engagement engagement) {
		this.engagement = engagement;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

    public String getAnyOtherIssue() {
		return anyOtherIssue;
	}

	public void setAnyOtherIssue(String anyOtherIssue) {
		this.anyOtherIssue = anyOtherIssue;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public LocalDate getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(LocalDate reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

    public EngagementObjective getEngagementObjective() {
        return engagementObjective;
    }

    public void setEngagementObjective(EngagementObjective engagementObjective) {
        this.engagementObjective = engagementObjective;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getMethodology() {
        return methodology;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }
}
