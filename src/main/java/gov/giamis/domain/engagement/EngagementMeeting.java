package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import gov.giamis.domain.enumeration.MeetingType;

/**
 * A EngagementMeeting.
 */
@Entity
@Table(name = "engagement_meetings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementMeeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "meeting_type", nullable = false)
    private MeetingType meetingType;

    
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "agenda", nullable = false)
    private String agenda;

    @NotNull
    @Column(name = "meeting_date", nullable = false)
    private LocalDate meetingDate;

    @NotBlank
    @SafeHtml
    @Size(max = 100)
    @Column(name = "venue", length = 100, nullable = false)
    private String venue;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "code",
        "description",
        "auditType",
        "scope",
        "startDate",
        "endDate",
        "isGo",
        "financialYear",
        "auditableArea",
        "engagementPhase",
        "engagementResources",
        "organisationUnit",
        "parentEngagement",
        "engagementTeamMeeting",
        "engagementExitMeeting",
        "engagementMembers"
    })
    private Engagement engagement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MeetingType getMeetingType() {
        return meetingType;
    }

    public EngagementMeeting meetingType(MeetingType meetingType) {
        this.meetingType = meetingType;
        return this;
    }

    public void setMeetingType(MeetingType meetingType) {
        this.meetingType = meetingType;
    }

    public String getAgenda() {
        return agenda;
    }

    public EngagementMeeting agenda(String agenda) {
        this.agenda = agenda;
        return this;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public LocalDate getMeetingDate() {
        return meetingDate;
    }

    public EngagementMeeting meetingDate(LocalDate meetingDate) {
        this.meetingDate = meetingDate;
        return this;
    }

    public void setMeetingDate(LocalDate meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getVenue() {
        return venue;
    }

    public EngagementMeeting venue(String venue) {
        this.venue = venue;
        return this;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public EngagementMeeting engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementMeeting)) {
            return false;
        }
        return id != null && id.equals(((EngagementMeeting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementMeeting{" +
            "id=" + getId() +
            ", meetingType='" + getMeetingType() + "'" +
            ", agenda='" + getAgenda() + "'" +
            ", meetingDate='" + getMeetingDate() + "'" +
            ", venue='" + getVenue() + "'" +
            "}";
    }
}
