package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FileResource;

@Entity
@Table(name = "engagement_entrance_meetings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementEntranceMeeting extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;


    @Column(name = "meeting_date", nullable = false)
    private LocalDate meetingDate;

    @NotBlank
    @SafeHtml
    @Size(max = 50)
    @Column(name = "venue", length = 50, nullable = false)
    private String venue;

    @ManyToOne(optional = false)
    private Engagement engagement;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "remarks", nullable = false)
    private String remarks;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "overview", nullable = false)
    private String overview;


    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "client_other_issues", nullable = false)
    private String clientOtherIssue;


    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion", nullable = false)
    private String conclusion;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "any_other_issue", nullable = false)
    private String anyOtherIssue;


    @Column(name = "reviewed_by", nullable = true, length = 50)
    private String reviewedBy;

    @Column(name = "reviewed_date")
    private LocalDate reviewedDate;

    @ManyToOne
    @JsonIgnoreProperties({
        "organisationUnits","fileResourceType", "uid", "name",
        "contentType","contentMd5","size", "isAssigned","tags"
    })
    private FileResource fileResource;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(LocalDate meetingDate) {
		this.meetingDate = meetingDate;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}


	public Engagement getEngagement() {
		return engagement;
	}

	public void setEngagement(Engagement engagement) {
		this.engagement = engagement;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getClientOtherIssue() {
		return clientOtherIssue;
	}

	public void setClientOtherIssue(String clientOtherIssue) {
		this.clientOtherIssue = clientOtherIssue;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public String getAnyOtherIssue() {
		return anyOtherIssue;
	}

	public void setAnyOtherIssue(String anyOtherIssue) {
		this.anyOtherIssue = anyOtherIssue;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public LocalDate getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(LocalDate reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

	public FileResource getFileResource() {
		return fileResource;
	}

	public void setFileResource(FileResource fileResource) {
		this.fileResource = fileResource;
	}

}
