package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gov.giamis.domain.AbstractAuditingEntity;

/**
 * A EngagementWorkProgram.
 */
@Entity
@Table(name = "engagement_work_programs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementWorkProgram extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_planned", nullable = false)
    private LocalDate datePlanned;

    @Column(name = "date_completed")
    private LocalDate dateCompleted;

    @ManyToOne(optional = false)
    @NotNull
    private SpecificObjective  specificObjective;

    @ManyToOne(optional = false)
    @NotNull
    private EngagementProcedure engagementProcedure;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDatePlanned() {
		return datePlanned;
	}

	public void setDatePlanned(LocalDate datePlanned) {
		this.datePlanned = datePlanned;
	}

	public LocalDate getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(LocalDate dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public SpecificObjective getSpecificObjective() {
		return specificObjective;
	}

	public void setSpecificObjective(SpecificObjective specificObjective) {
		this.specificObjective = specificObjective;
	}

	public EngagementProcedure getEngagementProcedure() {
		return engagementProcedure;
	}

	public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}
}
