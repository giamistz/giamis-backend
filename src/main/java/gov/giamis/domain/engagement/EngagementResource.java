package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.config.Constants;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A EngagementResource.
 */
@Entity
@Table(name = "engagement_resources")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description")
    private String description;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "code",
        "description",
        "auditType",
        "scope",
        "startDate",
        "endDate",
        "isGo",
        "financialYear",
        "auditableArea",
        "engagementPhase",
        "engagementResources",
        "organisationUnit",
        "parentEngagement",
        "engagementTeamMeeting",
        "engagementExitMeeting",
        "engagementMembers"})
    private Engagement engagement;

    @ManyToOne
    @JsonIgnoreProperties({
        "engagementResources",
        "contentMd5",
        "isAssigned",
        "tags",
        "uid",
        "fileResourceType"
    })
    private FileResource fileResource;

    private Boolean appendix;

    public Boolean getAppendix() {
        return appendix;
    }

    public void setAppendix(Boolean appendix) {
        this.appendix = appendix;
    }


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public EngagementResource name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public EngagementResource description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public EngagementResource engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public EngagementResource fileResource(FileResource fileResource) {
        this.fileResource = fileResource;
        return this;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementResource)) {
            return false;
        }
        return id != null && id.equals(((EngagementResource) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementResource{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
