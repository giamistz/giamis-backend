package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A WorkProgram.
 */
@Entity
@Table(name = "work_programs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WorkProgram implements Serializable {

    private static final long serialVersionUID = 1L;

    public WorkProgram(EngagementProcedure procedure, String name) {
        this.engagementProcedure = procedure;
        this.name = name;
    }

    public WorkProgram() {}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotBlank
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false)
    private String name;
    
    @SafeHtml
    @Size(max = 50)
    @Column(name = "code", length = 50, unique = true)
    private String code;

    @OneToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "user",
        "datePlanned",
        "code"
    })
    private EngagementProcedure engagementProcedure;

    
    @OneToMany(mappedBy = "workProgram", cascade = CascadeType.ALL, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("workProgram")
    private Set<EngagementTestResult> engagementTestResult = new HashSet<>();


        @OneToMany(fetch = FetchType.EAGER, mappedBy = "workProgram")
    @JsonIgnoreProperties({
        "category",
        "subCategory",
        "condition",
        "disclosed",
        "cause",
        "action_plan_categorisation",
        "recommendation",
        "auditor_comment",
        "engagementProcedure",
        "implication",
        "findingComments",
        "responseAndAction",
        "workProgram"})
    private Set<EngagementFinding> engagementFindings = new HashSet<>();

    public Set<EngagementFinding> getEngagementFindings() {
        return engagementFindings;
    }

    public void setEngagementFindings(Set<EngagementFinding> engagementFindings) {
        this.engagementFindings = engagementFindings;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public EngagementProcedure getEngagementProcedure() {
		return engagementProcedure;
	}

	public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}
 
	public Set<EngagementTestResult> getEngagementTestResult() {
		return engagementTestResult;
	}

	public void setEngagementTestResult(Set<EngagementTestResult> engagementTestResult) {
		this.engagementTestResult = engagementTestResult;
	}
}
