package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.OrganisationUnit;

@Entity
@Table(name = "engagement_interm_meetings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementIntermMeeting extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "meeting_date", nullable = false)
    private LocalDate meetingDate;

    @NotBlank
    @SafeHtml
    @Size(max = 50)
    @Column(name = "venue", length = 50, nullable = false)
    private String venue;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("engagementMeetings")
    private Engagement engagement;

    @OneToMany(mappedBy = "engagementIntermMeeting", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
   	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
   	@JsonIgnoreProperties("engagementIntermMeeting")
   	private Set<EngagementIntermMember> engagementIntermMember = new HashSet<>();

    @ManyToOne(optional = false)
    @JsonIgnoreProperties("strategicPlans")
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = false)
    private FinancialYear financialYear;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "remarks", nullable = false)
    private String remarks;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "findings", nullable = false)
    private String findings;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "comments",nullable = false)
    private String comments;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion", nullable = false)
    private String conclusion;

    @Column(name = "reviewed_by", nullable = true, length = 50)
    private String reviewedBy;

    @Column(name = "reviewed_date")
    private LocalDate reviewedDate;

    @Column(name = "approved_by", nullable = true, length = 50)
    private String approvedBy;

    @Column(name = "approved_date")
    private LocalDate approvedDate;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties({
        "organisationUnits",
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(LocalDate meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

	public Set<EngagementIntermMember> getEngagementIntermMember() {
		return engagementIntermMember;
	}

	public void setEngagementIntermMember(Set<EngagementIntermMember> engagementIntermMember) {
		this.engagementIntermMember = engagementIntermMember;
	}

	public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getConclusion() {
        return conclusion;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getReviewedBy() {
        return reviewedBy;
    }

    public void setReviewedBy(String reviewedBy) {
        this.reviewedBy = reviewedBy;
    }

    public LocalDate getReviewedDate() {
        return reviewedDate;
    }

    public void setReviewedDate(LocalDate reviewedDate) {
        this.reviewedDate = reviewedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementMeeting)) {
            return false;
        }
        return id != null && id.equals(((EngagementIntermMeeting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "EngagementIntermMeeting [id=" + id + ", meetingDate=" + meetingDate + ", venue=" + venue
				+ ", engagement=" + engagement + ", engagementIntermMember=" + engagementIntermMember
				+ ", organisationUnit=" + organisationUnit + ", financialYear=" + financialYear + ", remarks=" + remarks
				+ ", findings=" + findings + ", comments=" + comments + ", conclusion=" + conclusion + ", reviewedBy="
				+ reviewedBy + ", reviewedDate=" + reviewedDate + ", approvedBy=" + approvedBy + ", approvedDate="
				+ approvedDate + "]";
	}
}
