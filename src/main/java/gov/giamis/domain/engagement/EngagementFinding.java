package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.SimpleUser;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.User;
@Entity
@Table(name = "engagement_findings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementFinding extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;


	public EngagementFinding() {}

	public EngagementFinding(EngagementProcedure engagementProcedure,
                             WorkProgram workProgram) {
	    this.engagementProcedure = engagementProcedure;
	    this.workProgram = workProgram;
    }

	@NotBlank
	@SafeHtml
	@Size(max = 20)
	@Column(name = "five_attribute", length = 20, nullable = false)
	private String fiveAttribute;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "description", nullable = true)
	private String description;

    @SafeHtml
	@Column(name = "category", nullable = true)
	private String category;

	@SafeHtml
	@Column(name = "sub_category", nullable = true)
	private String subCategory;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "condition", nullable = true)
	private String condition;

	@SafeHtml
	@Size(max = 6)
	@Column(name = "disclosed_inlast_audit", length = 6, nullable = true)
	private String disclosedInLastAudit;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "cause", nullable = true)
	private String cause;

	@SafeHtml
	@Column(name = "action_plan_categorisation", nullable = true)
	private String actionPlanCategorization;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "recommendation", nullable = true)
	private String recommendation;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "auditor_comment", nullable = true)
	private String auditorComment;

	@SafeHtml
	@Column(name = "implement_status", nullable = true)
	private String implementStatus;

	// @Column(name = "status", nullable = true)
	// private boolean status;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "response_and_action", nullable = true)
	private String responseAndAction;

	@Column(name = "amount",nullable = false)
	private Double amount;

	@NotNull
	@OneToOne(optional = false)
    @JsonIgnoreProperties("engagementFindings")
    private WorkProgram workProgram;

    @ManyToOne(optional = false)
    @NotNull
    private EngagementProcedure engagementProcedure;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "engagementFinding")
    @JsonIgnoreProperties({
        "engagementFinding",
        "user"})
    private List<EngagementFindingComment> findingComments;

    public List<EngagementFindingComment> getFindingComments() {
        return findingComments;
    }

    public void setFindingComments(List<EngagementFindingComment> findingComments) {
        this.findingComments = findingComments;
    }

    @SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "implication", nullable = true)
	private String implication;

    @SafeHtml
   	@Type(type = "org.hibernate.type.TextType")
   	@Column(name = "comments", nullable = true)
   	private String comments;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "engagementFinding")
    @JsonIgnoreProperties({
        "engagementFinding"
    })
    private List<EngFindingRecommendation> engFindingRecommendations = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFiveAttribute() {
		return fiveAttribute;
	}

	public void setFiveAttribute(String fiveAttribute) {
		this.fiveAttribute = fiveAttribute;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getCondition() {
		return condition;
	}

    public List<EngFindingRecommendation> getEngFindingRecommendations() {
        return engFindingRecommendations;
    }

    public void setEngFindingRecommendations(List<EngFindingRecommendation> engFindingRecommendations) {
        this.engFindingRecommendations = engFindingRecommendations;
    }

    public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getDisclosedInLastAudit() {
		return disclosedInLastAudit;
	}

	public void setDisclosedInLastAudit(String disclosedInLastAudit) {
		this.disclosedInLastAudit = disclosedInLastAudit;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getActionPlanCategorization() {
		return actionPlanCategorization;
	}

	public void setActionPlanCategorization(String actionPlanCategorization) {
		this.actionPlanCategorization = actionPlanCategorization;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getAuditorComment() {
		return auditorComment;
	}

	public void setAuditorComment(String auditorComment) {
		this.auditorComment = auditorComment;
	}

	public String getImplementStatus() {
		return implementStatus;
	}

	public void setImplementStatus(String implementStatus) {
		this.implementStatus = implementStatus;
	}

	public String getResponseAndAction() {
		return responseAndAction;
	}

	public void setResponseAndAction(String responseAndAction) {
		this.responseAndAction = responseAndAction;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public WorkProgram getWorkProgram() {
		return workProgram;
	}

	public void setWorkProgram(WorkProgram workProgram) {
		this.workProgram = workProgram;
	}
	public EngagementProcedure getEngagementProcedure() {
		return engagementProcedure;
	}

	public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}

	public String getImplication() {
		return implication;
	}

	public void setImplication(String implication) {
		this.implication = implication;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}


}
