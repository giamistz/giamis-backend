package gov.giamis.domain.engagement;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gov.giamis.domain.AbstractAuditingEntity;

@Entity
@Table(name = "engagement_exit_meeting_members")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementExitMember extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    
    @ManyToOne(optional = false)
    @JoinColumn(name="engagement_exit_meeting_id",referencedColumnName="id")
    private EngagementExitMeeting engagementExitMeeting;
    
   
    @ManyToOne(optional = false)
    @JoinColumn(name = "engagement_member_id")
    @JsonIgnoreProperties({
        "role",
        "acceptedNda",
        "acceptanceDate",
        "engagement",
        "letterFileResource",
        "ndaFileResource"
    })
    private EngagementMember engagementMember;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EngagementExitMeeting getEngagementExitMeeting() {
        return engagementExitMeeting;
    }

    public void setEngagementExitMeeting(EngagementExitMeeting engagementExitMeeting) {
        this.engagementExitMeeting = engagementExitMeeting;
    }

    public EngagementMember getEngagementMember() {
        return engagementMember;
    }

    public void setEngagementMember(EngagementMember engagementMember) {
        this.engagementMember = engagementMember;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementExitMember)) {
            return false;
        }
        return id != null && id.equals(((EngagementExitMember) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementExitMember [id=" + id + ", engagementExitMeeting=" + engagementExitMeeting
                + ", engagementMember=" + engagementMember + "]";
    }
}
