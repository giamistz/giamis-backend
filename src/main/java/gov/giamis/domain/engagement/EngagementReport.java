package gov.giamis.domain.engagement;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import gov.giamis.domain.enumeration.EngagementReportStatus;

/**
 * A EngagementReport.
 */
@Entity
@Table(name = "engagement_reports")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "introduction")
    private String introduction;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "acknowledgement")
    private String acknowledgement;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "executive_summary")
    private String executiveSummary;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion")
    private String conclusion;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "general_remarks")
    private String generalRemarks;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private EngagementReportStatus status;

    @OneToOne(optional = false)    @NotNull

    @MapsId
    @JoinColumn(name = "id")
    private Engagement engagement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntroduction() {
        return introduction;
    }

    public EngagementReport introduction(String introduction) {
        this.introduction = introduction;
        return this;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public EngagementReport acknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
        return this;
    }

    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public EngagementReport executiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
        return this;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getConclusion() {
        return conclusion;
    }

    public EngagementReport conclusion(String conclusion) {
        this.conclusion = conclusion;
        return this;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getGeneralRemarks() {
        return generalRemarks;
    }

    public EngagementReport generalRemarks(String generalRemarks) {
        this.generalRemarks = generalRemarks;
        return this;
    }

    public void setGeneralRemarks(String generalRemarks) {
        this.generalRemarks = generalRemarks;
    }

    public EngagementReportStatus getStatus() {
        return status;
    }

    public EngagementReport status(EngagementReportStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(EngagementReportStatus status) {
        this.status = status;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public EngagementReport engagement(Engagement engagement) {
        this.engagement = engagement;
        return this;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementReport)) {
            return false;
        }
        return id != null && id.equals(((EngagementReport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementReport{" +
            "id=" + getId() +
            ", introduction='" + getIntroduction() + "'" +
            ", acknowledgement='" + getAcknowledgement() + "'" +
            ", executiveSummary='" + getExecutiveSummary() + "'" +
            ", conclusion='" + getConclusion() + "'" +
            ", generalRemarks='" + getGeneralRemarks() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
