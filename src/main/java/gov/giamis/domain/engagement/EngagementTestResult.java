package gov.giamis.domain.engagement;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@Entity
@Table(name = "engagement_test_results")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementTestResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    public Boolean getOk() {
        return isOk;
    }

    public void setOk(Boolean ok) {
        isOk = ok;
    }

    @Column(name = "is_ok")
    private Boolean isOk;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties({"engagementProcedure","engagementTestResult"})
    private EngagementItem engagementItem;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties(value = {"engagementResult","engagementProcedure"}, allowSetters = true)
    private WorkProgram workProgram;

    @ManyToOne(optional = true)
    @JsonIgnoreProperties("engagementResult")
    private EngagementProcedure engagementProcedure;
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public EngagementItem getEngagementItem() {
		return engagementItem;
	}

	public void setEngagementItem(EngagementItem engagementItem) {
		this.engagementItem = engagementItem;
	}

	public WorkProgram getWorkProgram() {
		return workProgram;
	}

	public void setWorkProgram(WorkProgram workProgram) {
		this.workProgram = workProgram;
	}

	public EngagementProcedure getEngagementProcedure() {
		return engagementProcedure;
	}

	public void setEngagementProcedure(EngagementProcedure engagementProcedure) {
		this.engagementProcedure = engagementProcedure;
	}

//	public List<EngagementScore> getScoreList() {
//		return scoreList;
//	}
//
//	public void setScoreList(List<EngagementScore> scoreList) {
//		this.scoreList = scoreList;
//	}
	
    }
