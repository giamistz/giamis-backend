package gov.giamis.domain.engagement;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gov.giamis.domain.AbstractAuditingEntity;

@Entity
@Table(name = "engagement_interm_members")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementIntermMember extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    
    @ManyToOne(optional = false)
    @JoinColumn(name="engagement_meeting_id",referencedColumnName="id")
    private EngagementIntermMeeting engagementIntermMeeting;
    
   
    @ManyToOne(optional = false)
    @JoinColumn(name = "engagement_member_id")
    private EngagementMember engagementMember;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EngagementIntermMeeting getEngagementIntermMeeting() {
        return engagementIntermMeeting;
    }

    public void setEngagementIntermMeeting(EngagementIntermMeeting engagementIntermMeeting) {
        this.engagementIntermMeeting = engagementIntermMeeting;
    }

    public EngagementMember getEngagementMember() {
        return engagementMember;
    }

    public void setEngagementMember(EngagementMember engagementMember) {
        this.engagementMember = engagementMember;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementIntermMember)) {
            return false;
        }
        return id != null && id.equals(((EngagementIntermMember) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngagementIntermMember [id=" + id + ", engagementIntermMeeting=" + engagementIntermMeeting
                + ", engagementMember=" + engagementMember + "]";
    }
}
