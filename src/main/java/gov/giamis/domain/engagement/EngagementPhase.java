package gov.giamis.domain.engagement;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

import org.checkerframework.checker.units.qual.C;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.SafeHtml;

import gov.giamis.domain.AbstractAuditingEntity;

@Entity
@Table(name = "engagement_phases")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementPhase  extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public EngagementPhase(String name, Boolean active) {
        super();
        this.name = name;
        this.active = active;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @Column(name = "name", nullable = true)
    private String name;
    
    @Column(name = "active")
    private Boolean active = false;

    @Column(name = "prepared_date")
    private LocalDate preparedDate;

    @Column(name = "prepared_by")
    private String preparedBy;

    @Column(name = "reviewed")
    private Boolean reviewed = false;

    @Column(name = "reviewed_date")
    private LocalDate reviewedDate;

    @Column(name = "reviewed_by")
    private String reviewedBy;

    @Column(name = "approved")
    private Boolean approved = false;

    @Column(name = "approved_date")
    private LocalDate approvedDate;

    @Column(name = "approved_by")
    private String approvedBy;

    @ManyToOne
    @JoinColumn(name = "engagement_id")
    private Engagement engagement;


    public EngagementPhase() {
		super();
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isStatus() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

    public Boolean getActive() {
        return active;
    }

    public LocalDate getPreparedDate() {
        return preparedDate;
    }

    public void setPreparedDate(LocalDate preparedDate) {
        this.preparedDate = preparedDate;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public Boolean getReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public LocalDate getReviewedDate() {
        return reviewedDate;
    }

    public void setReviewedDate(LocalDate reviewedDate) {
        this.reviewedDate = reviewedDate;
    }

    public String getReviewedBy() {
        return reviewedBy;
    }

    public void setReviewedBy(String reviewedBy) {
        this.reviewedBy = reviewedBy;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Engagement getEngagement() {
        return engagement;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngagementPhase)) {
            return false;
        }
        return id != null && id.equals(((EngagementPhase) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "EngagementPhase [id=" + id + ", name=" + name + ", + , active=" + active + "]";
	}
}
