package gov.giamis.domain.engagement;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import gov.giamis.domain.AbstractAuditingEntity;

@Entity
@Table(name = "meeting_deliverables")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MeetingDeliverable extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "deliverable", nullable = true)
    private String deliverable;

    @Column(name = "date",nullable = true)
    private LocalDate date;

    @ManyToOne(optional = true)
    private EngagementTeamMeeting engagementTeamMeeting;
   
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeliverable() {
		return deliverable;
	}

	public void setDeliverable(String deliverable) {
		this.deliverable = deliverable;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public EngagementTeamMeeting getEngagementTeamMeeting() {
		return engagementTeamMeeting;
	}

	public void setEngagementTeamMeeting(EngagementTeamMeeting engagementTeamMeeting) {
		this.engagementTeamMeeting = engagementTeamMeeting;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeetingDeliverable)) {
            return false;
        }
        return id != null && id.equals(((MeetingDeliverable) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "MeetingDeliverable [id=" + id + ", deliverable=" + deliverable + ", date=" + date
				+ ", engagementTeamMeeting=" + engagementTeamMeeting + ", hashCode()=" + hashCode()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedBy()=" + getLastModifiedBy() + ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
	}
    
    

}
