package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A EngFindingRecommendation.
 */
@Entity
@Table(name = "eng_finding_recommendations")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngFindingRecommendation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public EngFindingRecommendation() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;


    @Size(max = 250)
    @Column(name = "description")
    private String description;

    @Size(max = 100)
    @Column(name = "status", length = 100)
    private String status;

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "completion_date")
    private LocalDate completionDate;

    @Column(name = "compliance_comments")
    private String complianceComments;

    @ManyToOne
    @JsonIgnoreProperties({
        "organisationUnits",
        "fileResourceType",
        "uid",
        "name",
        "contentType",
        "contentMd5",
        "size",
        "isAssigned",
        "tags"
    })
    private FileResource fileResource;

    private Instant createdDate;

    @ManyToOne
    @JsonIgnoreProperties("engFindingRecommendations")
    private EngagementFinding engagementFinding;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "engFindingRecommendation")
    @JsonIgnoreProperties({
        "engFindingRecommendation"
    })
    private List<EngFindingRecommendationComment> engFindingRecommendationComments = new ArrayList<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public EngFindingRecommendation description(String description) {
        this.description = description;
        return this;
    }

    public EngagementFinding getEngagementFinding() {
        return engagementFinding;
    }

    public void setEngagementFinding(EngagementFinding engagementFinding) {
        this.engagementFinding = engagementFinding;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public EngFindingRecommendation status(String status) {
        this.status = status;
        return this;
    }


    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComplianceComments() {
        return complianceComments;
    }

    public void setComplianceComments(String complianceComments) {
        this.complianceComments = complianceComments;
    }

    public FileResource getFileResource() {
        return fileResource;
    }

    public void setFileResource(FileResource fileResource) {
        this.fileResource = fileResource;
    }

    public List<EngFindingRecommendationComment> getEngFindingRecommendationComments() {
        return engFindingRecommendationComments;
    }

    public void setEngFindingRecommendationComments(List<EngFindingRecommendationComment> engFindingRecommendationComments) {
        this.engFindingRecommendationComments = engFindingRecommendationComments;
    }

    @Override
    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EngFindingRecommendation)) {
            return false;
        }
        return id != null && id.equals(((EngFindingRecommendation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EngFindingRecommendation{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
