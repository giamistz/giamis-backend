package gov.giamis.domain.engagement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import gov.giamis.domain.enumeration.ActionPlanCategory;

/**
 * A Finding.
 */
@Entity
@Table(name = "findings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Finding implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "condition", nullable = false)
    private String condition;

    
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "cause", nullable = false)
    private String cause;

    @Column(name = "number_of_error")
    private Integer numberOfError;

    @Column(name = "total_value_amount")
    private Double totalValueAmount;

    @Column(name = "impact_on_assurance")
    private String impactOnAssurance;

    
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "recommendation", nullable = false)
    private String recommendation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "action_plan")
    private String actionPlan;

    @Enumerated(EnumType.STRING)
    @Column(name = "action_plan_category")
    private ActionPlanCategory actionPlanCategory;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("findings")
    private EngagementProcedure procedure;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public Finding condition(String condition) {
        this.condition = condition;
        return this;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCause() {
        return cause;
    }

    public Finding cause(String cause) {
        this.cause = cause;
        return this;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Integer getNumberOfError() {
        return numberOfError;
    }

    public Finding numberOfError(Integer numberOfError) {
        this.numberOfError = numberOfError;
        return this;
    }

    public void setNumberOfError(Integer numberOfError) {
        this.numberOfError = numberOfError;
    }

    public Double getTotalValueAmount() {
        return totalValueAmount;
    }

    public Finding totalValueAmount(Double totalValueAmount) {
        this.totalValueAmount = totalValueAmount;
        return this;
    }

    public void setTotalValueAmount(Double totalValueAmount) {
        this.totalValueAmount = totalValueAmount;
    }

    public String getImpactOnAssurance() {
        return impactOnAssurance;
    }

    public Finding impactOnAssurance(String impactOnAssurance) {
        this.impactOnAssurance = impactOnAssurance;
        return this;
    }

    public void setImpactOnAssurance(String impactOnAssurance) {
        this.impactOnAssurance = impactOnAssurance;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public Finding recommendation(String recommendation) {
        this.recommendation = recommendation;
        return this;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getActionPlan() {
        return actionPlan;
    }

    public Finding actionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
        return this;
    }

    public void setActionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
    }

    public ActionPlanCategory getActionPlanCategory() {
        return actionPlanCategory;
    }

    public Finding actionPlanCategory(ActionPlanCategory actionPlanCategory) {
        this.actionPlanCategory = actionPlanCategory;
        return this;
    }

    public void setActionPlanCategory(ActionPlanCategory actionPlanCategory) {
        this.actionPlanCategory = actionPlanCategory;
    }

    public EngagementProcedure getProcedure() {
        return procedure;
    }

    public Finding procedure(EngagementProcedure engagementProcedure) {
        this.procedure = engagementProcedure;
        return this;
    }

    public void setProcedure(EngagementProcedure engagementProcedure) {
        this.procedure = engagementProcedure;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Finding)) {
            return false;
        }
        return id != null && id.equals(((Finding) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Finding{" +
            "id=" + getId() +
            ", condition='" + getCondition() + "'" +
            ", cause='" + getCause() + "'" +
            ", numberOfError=" + getNumberOfError() +
            ", totalValueAmount=" + getTotalValueAmount() +
            ", impactOnAssurance='" + getImpactOnAssurance() + "'" +
            ", recommendation='" + getRecommendation() + "'" +
            ", actionPlan='" + getActionPlan() + "'" +
            ", actionPlanCategory='" + getActionPlanCategory() + "'" +
            "}";
    }
}
