package gov.giamis.domain.engagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import gov.giamis.domain.program.AuditProgramObjective;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "specific_objectives")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpecificObjective implements Serializable {

	private static final long serialVersionUID = 1L;

	public SpecificObjective() {}

    public SpecificObjective(Engagement eng, String refinedDescription, String code) {
	    this.engagement = eng;
        this.description = refinedDescription;
        this.refinedDescription = refinedDescription;
        this.code = code;
        this.isRefined = true;
    }

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	private String code;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "description", nullable = true)
	private String description;

	@SafeHtml
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "refined_description", nullable = true)
	private String refinedDescription;

	@Column(name = "is_refined", nullable = true)
	private Boolean isRefined = false;

	@ManyToOne()
    @JsonIgnoreProperties({
        "description",
        "code","name",
        "startDate",
        "endDate",
        "auditType",
        "auditableArea",
        "engagementPhase",
        "engagementTeamMeeting",
        "engagementMembers",
        "parentEngagement"
    })
    @NotNull
    private Engagement engagement;


    @OneToMany(mappedBy = "specificObjective", fetch = FetchType.LAZY)
    @JsonIgnoreProperties({
        "specificObjective"
    })
    private List<EngagementObjectiveRisk> engagementObjectiveRisks = new ArrayList<>();

    @ManyToOne
    @JsonIgnoreProperties({
        "auditProgramEngagement"
    })
    @JoinColumn(name = "audit_program_objective_id")
    private AuditProgramObjective auditProgramObjective;

    public Engagement getEngagement() {
        return engagement;
    }

    public void setEngagement(Engagement engagement) {
        this.engagement = engagement;
    }

    public List<EngagementObjectiveRisk> getEngagementObjectiveRisks() {
        return engagementObjectiveRisks;
    }

    public void setEngagementObjectiveRisks(List<EngagementObjectiveRisk> engagementObjectiveRisks) {
        this.engagementObjectiveRisks = engagementObjectiveRisks;
    }

	public SpecificObjective(@SafeHtml String description) {
		super();
		this.description = description;
	}


	public String getRefinedDescription() {
		return refinedDescription;
	}

	public void setRefinedDescription(String refinedDescription) {
		this.refinedDescription = refinedDescription;
	}

	public Boolean getIsRefined() {
		return isRefined;
	}

	public void setIsRefined(Boolean isRefined) {
		this.isRefined = isRefined;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AuditProgramObjective getAuditProgramObjective() {
        return auditProgramObjective;
    }

    public void setAuditProgramObjective(AuditProgramObjective auditProgramObjective) {
        this.auditProgramObjective = auditProgramObjective;
    }

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SpecificObjective)) {
			return false;
		}
		return id != null && id.equals(((SpecificObjective) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "SpecificObjective [id=" + id + ", description=" + description + ", engagementTeamMeeting="
				 + "]";
	}

}
