package gov.giamis.domain.engagement;
import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import gov.giamis.domain.setup.SimpleUser;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.setup.FileResource;
import gov.giamis.domain.setup.User;
@Entity
@Table(name = "engagement_finding_comments")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementFindingComment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@ManyToOne(optional = false)
    private EngagementFinding engagementFinding;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({
        "authorities",
        "menus",
        "roles",
        "activated",
        "langKey",
        "imageUrl",
        "resetDate",
        "organisationUnit",
        "organisationUnitContactPerson",
        "auditorProfile",
        "currentFinancialYear"
    })
    private SimpleUser user;

	@SafeHtml
   	@Type(type = "org.hibernate.type.TextType")
   	@Column(name = "auditor_comment", nullable = true)
   	private String auditorComment;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "client_response", nullable = true)
    private String clientResponse;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "implementation_status", nullable = true)
    private String implementationStatus;

	@NotNull
    @Column(name = "attachment", nullable = false)
    private String attachment;

	@NotNull
    @Column(name = "initial_response", nullable = false)
	private Boolean initialResponse;

   	 @ManyToOne
    @JsonIgnoreProperties({
        "organisationUnits","fileResourceType", "uid", "name",
        "contentType","contentMd5","size", "isAssigned","tags"
    })
    private FileResource fileResource;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EngagementFinding getEngagementFinding() {
		return engagementFinding;
	}

	public void setEngagementFinding(EngagementFinding engagementFinding) {
		this.engagementFinding = engagementFinding;
	}

	public SimpleUser getUser() {
		return user;
	}

	public void setUser(SimpleUser user) {
		this.user = user;
	}

	public String getAuditorComment() {
		return auditorComment;
	}

	public void setAuditorComment(String auditorComment) {
		this.auditorComment = auditorComment;
	}

	public FileResource getFileResource() {
		return fileResource;
	}

    public String getClientResponse() {
        return clientResponse;
    }

    public void setClientResponse(String clientResponse) {
        this.clientResponse = clientResponse;
    }

    public String getImplementationStatus() {
        return implementationStatus;
    }

    public void setImplementationStatus(String implementationStatus) {
        this.implementationStatus = implementationStatus;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public void setFileResource(FileResource fileResource) {
		this.fileResource = fileResource;
	}

    public Boolean getInitialResponse() {
        return initialResponse;
    }

    public void setInitialResponse(Boolean initialResponse) {
        this.initialResponse = initialResponse;
    }

    @Override
    public String toString() {
        return "EngagementFindingComment{" +
            "id=" + id +
            ", engagementFinding=" + engagementFinding +
            ", user=" + user +
            ", auditorComment='" + auditorComment + '\'' +
            ", clientResponse='" + clientResponse + '\'' +
            ", implementationStatus='" + implementationStatus + '\'' +
            ", attachment='" + attachment + '\'' +
            ", fileResource=" + fileResource +
            ", initialResponse=" + initialResponse +
            '}';
    }

}
