package gov.giamis.domain.engagement;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import gov.giamis.domain.AbstractAuditingEntity;
import gov.giamis.domain.setup.GfsCode;
@Entity
@Table(name = "engagement_planning_costs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EngagementPlanningCost extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "frequency",nullable = false)
    private Integer frequency; 

    @NotNull
    @Column(name = "quantity",nullable = false)
    private Integer quantity;

    @NotNull
    @Column(name = "unit_price",nullable = false)
    private Double unitPrice;

    @NotNull
    @Column(name = "grand_total",nullable = false)
    private Double grandTotal;

    @ManyToOne(optional = false)
    @NotNull
    private Engagement engagement; 

    @ManyToOne(optional = false)
    @NotNull
    private GfsCode gfsCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Engagement getEngagement() {
		return engagement;
	}

	public void setEngagement(Engagement engagement) {
		this.engagement = engagement;
	}

	public GfsCode getGfsCode() {
		return gfsCode;
	}

	public void setGfsCode(GfsCode gfsCode) {
		this.gfsCode = gfsCode;
	}
    
}
