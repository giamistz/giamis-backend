package gov.giamis.domain.engagement;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import gov.giamis.domain.program.AuditProgramEngagement;
import gov.giamis.domain.setup.FileResource;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.hibernate.validator.constraints.SafeHtml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import gov.giamis.domain.enumeration.AuditType;
import gov.giamis.domain.setup.AuditableArea;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.OrganisationUnit;

/**
 * A Engagement.
 */
@Entity
@Table(name = "engagements")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Engagement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @SafeHtml
    @Size(max = 200)
    @Column(name = "process", length = 200, nullable = false)
    private String process;

    @SafeHtml
    @Size(max = 200)
    @Column(name = "sub_process", length = 200, nullable = true)
    private String subProcess;

    @SafeHtml
    @Size(max = 200)
    @Column(name = "sub_sub_process", length = 200, nullable = true)
    private String subSubProcess;

    @SafeHtml
    @Size(max = 10)
    @Column(name = "code", length = 10, nullable = true)
    private String code;

    @SafeHtml
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "description")
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "audit_type", nullable = false)
    private AuditType auditType;

    @NotBlank
    @SafeHtml
    @Column(name = "scope", nullable = false)
    private String scope;

    @NotNull
    @Column(name = "start_date")
    private LocalDate startDate;

    @NotNull
    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "control_adequacy")
    private Boolean controlAdequacy;

    @Column(name= "is_go")
    private Boolean isGo;

    public Boolean getGo() {
        return isGo;
    }

    public void setGo(Boolean go) {
        isGo = go;
    }

    @ManyToOne(optional = false)
    @JsonIgnoreProperties({
        "engagements",
        "endDate",
        "startDate",
        "status"
    })
    private FinancialYear financialYear;

    /**
     * The Auditee (Organisation unit to be audited)
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({
        "engagements",
        "address",
        "background",
        "code",
        "contactPerson",
        "email",
        "fileResource",
        "organisationUnitGroups",
        "organisationUnitLevel",
        "parentOrganisationUnit",
        "phoneNumber",
        "user",
        "users"
    })
    private OrganisationUnit organisationUnit;

    @OneToOne
    @JoinColumn(name="auditable_area_id")
    @JsonIgnoreProperties({
        "code",
        "organisationUnitGroups"})
    private AuditableArea auditableArea;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "engagement")
    @JsonIgnoreProperties({
        "engagement"
    })
    private Set<EngagementPhase> engagementPhases = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = "auditee")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnoreProperties({
        "users",
        "engagements",
        "background",
        "code",
        "contactPerson",
        "fileResource",
        "organisationUnitGroups",
        "organisationUnitLevel",
        "parentOrganisationUnit",
    })
    private OrganisationUnit parentEngagement;

    @OneToMany(mappedBy = "engagement")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("engagementTeamMeeting")
    private Set<EngagementTeamMeeting> engagementTeamMeeting = new HashSet<>();


    @OneToMany(mappedBy = "engagement")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnoreProperties("engagementExitMeeting")
    private Set<EngagementExitMeeting> engagementExitMeeting = new HashSet<>();

    @OneToMany(mappedBy = "engagement" , fetch = FetchType.LAZY)
    private Set<EngagementMember> engagementMembers = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name ="audit_program_engagement_id")
    @JsonIgnoreProperties({
        "auditableArea",
        "organisationUnitLevels"
    })
    private AuditProgramEngagement auditProgramEngagement;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcess() {
        return process;
    }

    public Engagement name(String name) {
        this.process = name;
        return this;
    }

    public String getSubProcess() {
        return subProcess;
    }

    public void setSubProcess(String subProcess) {
        this.subProcess = subProcess;
    }

    public String getSubSubProcess() {
        return subSubProcess;
    }

    public void setSubSubProcess(String subSubProcess) {
        this.subSubProcess = subSubProcess;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
        return description;
    }

    public Engagement description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AuditType getAuditType() {
        return auditType;
    }

    public Engagement auditType(AuditType auditType) {
        this.auditType = auditType;
        return this;
    }

    public void setAuditType(AuditType auditType) {
        this.auditType = auditType;
    }

    public String getScope() {
        return scope;
    }

    public Engagement scope(String scope) {
        this.scope = scope;
        return this;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Engagement startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Engagement endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public Engagement financialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
        return this;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public Engagement organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public Set<EngagementMember> getEngagementMembers() {
        return engagementMembers;
    }

    public void setEngagementMembers(Set<EngagementMember> engagementMembers) {
        this.engagementMembers = engagementMembers;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public AuditableArea getAuditableArea() {
		return auditableArea;
	}

	public void setAuditableArea(AuditableArea auditableArea) {
		this.auditableArea = auditableArea;
	}

	public Set<EngagementPhase> getEngagementPhase() {
		return engagementPhases;
	}

	public void setEngagementPhase(Set<EngagementPhase> engagementPhases) {
		this.engagementPhases = engagementPhases;
	}


	public OrganisationUnit getParentEngagement() {
		return parentEngagement;
	}

	public void setParentEngagement(OrganisationUnit parentEngagement) {
		this.parentEngagement = parentEngagement;
	}

	public Set<EngagementTeamMeeting> getEngagementTeamMeeting() {
		return engagementTeamMeeting;
	}

	public void setEngagementTeamMeeting(Set<EngagementTeamMeeting> engagementTeamMeeting) {
		this.engagementTeamMeeting = engagementTeamMeeting;
	}

	public Set<EngagementExitMeeting> getEngagementExitMeeting() {
		return engagementExitMeeting;
	}

	public void setEngagementExitMeeting(Set<EngagementExitMeeting> engagementExitMeeting) {
		this.engagementExitMeeting = engagementExitMeeting;
	}

    public AuditProgramEngagement getAuditProgramEngagement() {
        return auditProgramEngagement;
    }

    public void setAuditProgramEngagement(AuditProgramEngagement auditProgramEngagement) {
        this.auditProgramEngagement = auditProgramEngagement;
    }

    public Boolean getControlAdequacy() {
        return controlAdequacy;
    }

    public void setControlAdequacy(Boolean controlAdequacy) {
        this.controlAdequacy = controlAdequacy;
    }

    public Set<EngagementPhase> getEngagementPhases() {
        return engagementPhases;
    }

    public void setEngagementPhases(Set<EngagementPhase> engagementPhases) {
        this.engagementPhases = engagementPhases;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Engagement)) {
            return false;
        }
        return id != null && id.equals(((Engagement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "Engagement [id=" + id + "," +
            " process=" + process + "," +
            " subProcess=" + subProcess + "," +
            " subSubProcess=" + subSubProcess + "," +
            " code=" + code + ", " +
            "description=" + description + "," +
            " auditType=" + auditType + "," +
            " scope=" + scope + ", " +
            "startDate=" + startDate + ", " +
            "endDate=" + endDate + ", " +
            "financialYear=" + financialYear + ", " +
            "organisationUnit=" + organisationUnit + ", " +
            "auditableArea=" + auditableArea + ", " +
            "parentEngagement=" + parentEngagement + "]";
	}


}
