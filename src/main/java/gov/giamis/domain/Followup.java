package gov.giamis.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.setup.FinancialYear;
import gov.giamis.domain.setup.OrganisationUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Followup.
 */
@Entity
@Table(name = "followups")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Followup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "introduction")
    private String introduction;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "objective")
    private String objective;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "scope")
    private String scope;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "methodology")
    private String methodology;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "statement_of_assurance")
    private String statementOfAssurance;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "result")
    private String result;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion")
    private String conclusion;

    @Column(name = "is_open")
    private Boolean isOpen;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("followups")
    private FinancialYear financialYear;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("followups")
    private OrganisationUnit organisationUnit;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "followup_engagements",
               joinColumns = @JoinColumn(name = "followup_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "engagement_id", referencedColumnName = "id"))
    private Set<Engagement> engagements = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public Followup introduction(String introduction) {
        this.introduction = introduction;
        return this;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getObjective() {
        return objective;
    }

    public Followup objective(String objective) {
        this.objective = objective;
        return this;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getScope() {
        return scope;
    }

    public Followup scope(String scope) {
        this.scope = scope;
        return this;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getMethodology() {
        return methodology;
    }

    public Followup methodology(String methodology) {
        this.methodology = methodology;
        return this;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getStatementOfAssurance() {
        return statementOfAssurance;
    }

    public Followup statementOfAssurance(String statementOfAssurance) {
        this.statementOfAssurance = statementOfAssurance;
        return this;
    }

    public void setStatementOfAssurance(String statementOfAssurance) {
        this.statementOfAssurance = statementOfAssurance;
    }

    public String getResult() {
        return result;
    }

    public Followup result(String result) {
        this.result = result;
        return this;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getConclusion() {
        return conclusion;
    }

    public Followup conclusion(String conclusion) {
        this.conclusion = conclusion;
        return this;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Followup startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Followup endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public Followup financialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
        return this;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public Followup organisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(OrganisationUnit organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public Set<Engagement> getEngagements() {
        return engagements;
    }

    public Followup engagements(Set<Engagement> engagements) {
        this.engagements = engagements;
        return this;
    }

    public void setEngagements(Set<Engagement> engagements) {
        this.engagements = engagements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Followup)) {
            return false;
        }
        return id != null && id.equals(((Followup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Followup{" +
            "id=" + getId() +
            ", introduction='" + getIntroduction() + "'" +
            ", objective='" + getObjective() + "'" +
            ", scope='" + getScope() + "'" +
            ", methodology='" + getMethodology() + "'" +
            ", statementOfAssurance='" + getStatementOfAssurance() + "'" +
            ", result='" + getResult() + "'" +
            ", conclusion='" + getConclusion() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}
