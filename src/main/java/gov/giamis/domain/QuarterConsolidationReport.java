package gov.giamis.domain;
import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.Quarter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A QuarterConsolidationReport.
 */
@Entity
@Table(name = "quarter_consolidation_reports")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QuarterConsolidationReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "acknowledgement")
    private String acknowledgement;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "list_of_abbreviation")
    private String listOfAbbreviation;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "list_of_figure_and_table")
    private String listOfFigureAndTable;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "executive_summary")
    private String executiveSummary;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "introduction")
    private String introduction;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "analysis_summary")
    private String analysisSummary;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "general_summary")
    private String generalSummary;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "achievement")
    private String achievement;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "challenge")
    private String challenge;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "conclusion")
    private String conclusion;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "recommendation_to_higher_level")
    private String recommendationToHigherLevel;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "recommendation_from_higher_level")
    private String recommendationFromHigherLevel;

    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "appendix")
    private String appendix;

    @ManyToOne(optional = false)
    @JoinColumn(name = "organisation_unit_id")
    @NotNull
    private OrganisationUnit organisationUnit;

    @ManyToOne(optional = false)
    @JoinColumn(name = "quarter_id")
    @NotNull
    private Quarter quarter;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    public String getListOfAbbreviation() {
        return listOfAbbreviation;
    }

    public void setListOfAbbreviation(String listOfAbbreviation) {
        this.listOfAbbreviation = listOfAbbreviation;
    }

    public String getListOfFigureAndTable() {
        return listOfFigureAndTable;
    }

    public void setListOfFigureAndTable(String listOfFigureAndTable) {
        this.listOfFigureAndTable = listOfFigureAndTable;
    }

    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getAnalysisSummary() {
        return analysisSummary;
    }

    public void setAnalysisSummary(String analysisSummary) {
        this.analysisSummary = analysisSummary;
    }

    public String getGeneralSummary() {
        return generalSummary;
    }

    public void setGeneralSummary(String generalSummary) {
        this.generalSummary = generalSummary;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getRecommendationToHigherLevel() {
        return recommendationToHigherLevel;
    }

    public void setRecommendationToHigherLevel(String recommendationToHigherLevel) {
        this.recommendationToHigherLevel = recommendationToHigherLevel;
    }

    public String getRecommendationFromHigherLevel() {
        return recommendationFromHigherLevel;
    }


    public void setRecommendationFromHigherLevel(String recommendationFromHigherLevel) {
        this.recommendationFromHigherLevel = recommendationFromHigherLevel;
    }

    public String getAppendix() {
        return appendix;
    }


    public void setAppendix(String appendix) {
        this.appendix = appendix;
    }

    public OrganisationUnit getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(OrganisationUnit organisation) {
        this.organisationUnit = organisation;
    }

    public Quarter getQuarter() {
        return quarter;
    }

    public void setQuarter(Quarter quarter) {
        this.quarter = quarter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuarterConsolidationReport)) {
            return false;
        }
        return id != null && id.equals(((QuarterConsolidationReport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "QuarterConsolidationReport{" +
            "id=" + getId() +
            ", acknowledgement='" + getAcknowledgement() + "'" +
            ", listOfAbbreviation='" + getListOfAbbreviation() + "'" +
            ", listOfFigureAndTable='" + getListOfFigureAndTable() + "'" +
            ", executiveSummary='" + getExecutiveSummary() + "'" +
            ", introduction='" + getIntroduction() + "'" +
            ", analysisSummary='" + getAnalysisSummary() + "'" +
            ", generalSummary='" + getGeneralSummary() + "'" +
            ", achievement='" + getAchievement() + "'" +
            ", challenge='" + getChallenge() + "'" +
            ", conclusion='" + getConclusion() + "'" +
            ", recommendationToHigherLevel='" + getRecommendationToHigherLevel() + "'" +
            ", recommendationFromHigherLevel='" + getRecommendationFromHigherLevel() + "'" +
            ", appendix='" + getAppendix() + "'" +
            "}";
    }
}
