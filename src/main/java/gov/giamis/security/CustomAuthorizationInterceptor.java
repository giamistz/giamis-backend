package gov.giamis.security;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import com.google.gson.Gson;

import gov.giamis.domain.setup.Authority;
import gov.giamis.repository.setup.AuthorityRepository;
import gov.giamis.repository.setup.UserRepository;
import gov.giamis.web.rest.response.CustomApiResponse;

@Component
public class CustomAuthorizationInterceptor implements HandlerInterceptor {

   private final CacheManager cacheManager;

   private final AuthorityRepository authorityRepository;

    @Autowired
    public CustomAuthorizationInterceptor(
        CacheManager cacheManager,
        AuthorityRepository authorityRepository) {

        this.cacheManager = cacheManager;
        this.authorityRepository = authorityRepository;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        if (handler.getClass().getSimpleName().equals("ResourceHttpRequestHandler")) {
            return true;
        }

       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        HandlerMethod method = (HandlerMethod) handler;
        String requestType = request.getMethod();
        String action = "READ";

        if (requestType.equals("POST") ||
            requestType.equals("PUT") ||
            requestType.equals("PATCH")) {

            action = "WRITE";
        } else if (requestType.equals("DELETE")) {
            action ="DELETE";
        }

        NoAthorization noAthorization = method.getMethodAnnotation(NoAthorization.class);

        if( noAthorization == null) {

            String actionAuthority = action
                .concat("_")
                .concat(method.getBeanType()
                    .getSimpleName()
                    .toUpperCase()
                    .replace("RESOURCE",""));

            List<String> userAuth;
            Cache.ValueWrapper cache = cacheManager
                .getCache(UserRepository.AUTHORITIES_BY_EMAIL_CACHE)
                .get(authentication.getName());

            //If cache hit
            if (cache != null) {
                userAuth = (List<String>)cache.get();
            } else {
                // Cache mis get; get from db and add to cache
                Set<Authority> authorities = authorityRepository.getAllAuthoritiesByEmail(authentication.getName());
                userAuth = authorities.stream()
                    .map(Authority::getName)
                    .collect(Collectors.toList());
                cacheManager.getCache(UserRepository.AUTHORITIES_BY_EMAIL_CACHE)
                    .put(authentication.getName(), userAuth);
            }
            if (userAuth.contains(actionAuthority)) {
                return true;
            }
            Gson gson= new Gson();
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType("application/json");
            response.getWriter().write(gson.toJson(CustomApiResponse.accessDenied("Access Denied")));
           return false;

        } else {
            return true;
        }
    }
}
