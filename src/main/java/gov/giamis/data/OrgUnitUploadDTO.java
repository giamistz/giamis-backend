package gov.giamis.data;

import javax.validation.constraints.NotNull;

public class OrgUnitUploadDTO {

    @NotNull
    private String name;
    private String code;
    @NotNull
    private String level;
    private String parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "OrgUnitUploadDTO{" +
            "name='" + name + '\'' +
            ", code='" + code + '\'' +
            ", level='" + level + '\'' +
            ", parent='" + parent + '\'' +
            '}';
    }
}
