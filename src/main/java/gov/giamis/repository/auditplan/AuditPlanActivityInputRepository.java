package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivityInput;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditPlanActivityInput entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditPlanActivityInputRepository
    extends JpaRepository<AuditPlanActivityInput, Long>,
    JpaSpecificationExecutor<AuditPlanActivityInput> {

	List<AuditPlanActivityInput> findAuditPlanActivityInputByAuditPlanActivity_Id(Long id);
}
