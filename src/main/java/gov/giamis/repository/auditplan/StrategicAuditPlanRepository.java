package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlan;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the StrategicAuditPlan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StrategicAuditPlanRepository
    extends JpaRepository<StrategicAuditPlan, Long>,
    JpaSpecificationExecutor<StrategicAuditPlan> {

	public StrategicAuditPlan findByNameIgnoreCase(String name);

}
