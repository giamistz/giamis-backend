package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlanScheduleDay;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the StrategicAuditPlanScheduleDay entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StrategicAuditPlanScheduleDayRepository
    extends JpaRepository<StrategicAuditPlanScheduleDay, Long>,
    JpaSpecificationExecutor<StrategicAuditPlanScheduleDay> {

}
