package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.StrategicAuditPlanSchedule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the StrategicAuditPlanSchedule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StrategicAuditPlanScheduleRepository
    extends JpaRepository<StrategicAuditPlanSchedule, Long>,
    JpaSpecificationExecutor<StrategicAuditPlanSchedule> {

}
