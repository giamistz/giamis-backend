package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivity;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AuditPlanActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditPlanActivityRepository
    extends JpaRepository<AuditPlanActivity, Long>,
    JpaSpecificationExecutor<AuditPlanActivity> {
	
	AuditPlanActivity findFirstAuditPlanActivityByAuditPlan_IdOrderByIdDesc(Long id);
}
