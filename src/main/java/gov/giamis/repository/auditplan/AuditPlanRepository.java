package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.AuditPlan;
import gov.giamis.domain.auditplan.AuditPlanActivity;
import gov.giamis.domain.auditplan.AuditPlanActivityInput;
import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;

import gov.giamis.domain.setup.OrganisationUnit;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Spring Data  repository for the AuditPlan entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface AuditPlanRepository
    extends JpaRepository<AuditPlan, Long>,
    JpaSpecificationExecutor<AuditPlan> {

	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE AuditPlan ap SET ap.approved =true WHERE ap.id =:id")
	void updateAuditPlanById(@Param(value = "id") Long id);

	AuditPlan findAuditPlanById(Long id);


	List<AuditPlan> findByOrganisationUnit(Long organisationUnit);
}
