package gov.giamis.repository.auditplan;

import gov.giamis.domain.auditplan.AuditPlanActivityQuarter;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditPlanActivityQuarter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditPlanActivityQuarterRepository
    extends JpaRepository<AuditPlanActivityQuarter, Long>,
    JpaSpecificationExecutor<AuditPlanActivityQuarter> {

	List<AuditPlanActivityQuarter> findAuditPlanActivityQuarterByAuditPlanActivity_Id(Long id);
}
