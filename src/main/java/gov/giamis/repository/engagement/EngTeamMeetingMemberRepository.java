package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementTeamMeetingMember;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EngTeamMeetingMemberRepository extends
    JpaRepository<EngagementTeamMeetingMember, Long> {

    List<EngagementTeamMeetingMember> findByEngagementMeetingId(Long engagementMeetingId);
}
