package gov.giamis.repository.engagement;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.EngagementItem;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the EngagementItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementItemRepository extends JpaRepository<EngagementItem, Long>, JpaSpecificationExecutor<EngagementItem> {

	List<EngagementItem> findByEngagementProcedure_Id(@Param(value="id") Long id);
	
	@Query(value="FROM EngagementItem ei WHERE ei.id =:id")
	Optional<EngagementItem> findById(@Param(value="id") Long id);
}
