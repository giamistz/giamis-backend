package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementTeamMeeting;
import gov.giamis.domain.engagement.MeetingDeliverable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Engagement Team Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface TeamMeetingDeliverableRepository extends
    JpaRepository<MeetingDeliverable, Long>{
}
