package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.MeetingAttachment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MeetingAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetingAttachmentRepository extends JpaRepository<MeetingAttachment, Long>, JpaSpecificationExecutor<MeetingAttachment> {

}
