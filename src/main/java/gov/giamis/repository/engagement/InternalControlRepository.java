package gov.giamis.repository.engagement;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.InternalControl;


/**
 * Spring Data  repository for the InternalControl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InternalControlRepository extends JpaRepository<InternalControl, Long>, JpaSpecificationExecutor<InternalControl> {
	Optional<InternalControl> findFirstByEngagement_IdOrderByIdDesc(@Param(value="id") Long id);


    List<InternalControl> findByEngagement_Id(Long engagementId);
//
//    @Query("select distinct co " +
//        " from InternalControl  co " +
//        " left join fetch co.engagementProcedures p " +
//        " where co.engagement.id =:engagementId and co.isControl = true " )
//    List<InternalControl> findAllWithProcedures(@Param("engagementId") Long engagementId);
}
