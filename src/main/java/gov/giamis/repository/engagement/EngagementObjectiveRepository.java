package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementObjective;
import gov.giamis.domain.setup.OrganisationUnit;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the EngagementObjective entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementObjectiveRepository extends
    JpaRepository<EngagementObjective, Long>,
    JpaSpecificationExecutor<EngagementObjective> {

    @Query("select distinct engagementObjective " +
        "from EngagementObjective engagementObjective " )
    List<EngagementObjective> findAllWithRisks(Specification specification);
}
