package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngFindingRecommendation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngFindingReccomendation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngFindingReccomendationRepository extends JpaRepository<EngFindingRecommendation, Long>, JpaSpecificationExecutor<EngFindingRecommendation> {

}
