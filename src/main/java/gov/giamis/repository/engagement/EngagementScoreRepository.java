package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementScore;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementScoreRepository extends
    JpaRepository<EngagementScore, Long>,
    JpaSpecificationExecutor<EngagementScore> {
    
  EngagementScore findEngagementScoreById(@Param(value="id") Long id);
}
