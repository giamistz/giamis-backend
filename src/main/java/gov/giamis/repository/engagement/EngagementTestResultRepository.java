package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementProcedure;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.EngagementTestResult;

import java.util.List;

import javax.persistence.QueryHint;
import javax.transaction.Transactional;

/**
 * Spring Data  repository for the EngagementTestResult entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementTestResultRepository extends JpaRepository<EngagementTestResult, Long>, JpaSpecificationExecutor<EngagementTestResult> {

	List<EngagementTestResult> findByEngagementProcedure_Id(@Param(value = "id") Long id);
	
//	@Query(value="SELECT ets FROM EngagementTestResult ets  WHERE ets.ok = :ok AND ets.engagementProcedure.id =:id")
//	List<EngagementTestResult> findByOkAndEngagementProcedureId(@Param(value = "ok") Boolean ok,@Param(value = "id") Long id);
//
	List<EngagementTestResult> findByIsOkAndEngagementProcedure_Id(Boolean ok, Long id);

//	@Query(value="SELECT COUNT(ets.score) FROM EngagementTestResult ets  WHERE ets.ok != :score AND ets.workProgram.id =:id")
//	Long countByScoreNotAndWorkProgram(@Param(value = "score") String score,@Param(value = "id") Long id);
	
	EngagementTestResult findFirstByWorkProgram_IdOrderByIdDesc(@Param(value="id") Long id);
	
//	@Query(value="SELECT ets FROM EngagementTestResult ets  WHERE ets.score != :score AND ets.workProgram.id = :id ORDER BY ets.id DESC")
//	List<EngagementTestResult> findByScoreNotAndWorkProgram(@Param(value = "score") String score,@Param(value = "id") Long id);
//
//	@Query(value="SELECT ets FROM EngagementTestResult ets  WHERE ets.score != :score AND ets.workProgram.id != :id ORDER BY ets.id DESC")
//	List<EngagementTestResult> findByWorkProgramNotAndOrderByIdDesc(@Param(value = "score") String score,@Param(value="id") Long id);

	EngagementTestResult findByEngagementItem_Id(@Param(value = "id") Long id);
	
	@Query(value="FROM EngagementTestResult etr INNER JOIN EngagementItem ei ON etr.engagementItem.id = ei.id  WHERE etr.engagementItem.id =:id ORDER BY ei.id DESC")
	List<EngagementTestResult> findByEngagementItemId(@Param(value = "id") Long id);
	

	@Modifying(clearAutomatically =true, flushAutomatically =true)
	@Query(value="UPDATE EngagementTestResult ets SET ets.engagementItem.id =:item, ets.workProgram.id =:workProgram, ets.engagementProcedure.id =:procedure WHERE ets.id =:id")
	void updateEngagementTestResult(@Param(value = "item") Long item,@Param(value = "workProgram") Long workProgram,@Param(value = "procedure") Long procedure,@Param(value = "id") Long id);
}
