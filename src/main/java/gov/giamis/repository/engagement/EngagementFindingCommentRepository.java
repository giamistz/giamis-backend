package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementFindingComment;

import java.util.Collection;
import java.util.List;

import gov.giamis.domain.engagement.EngagementProcedure;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementFindingCommentRepository extends
    JpaRepository<EngagementFindingComment, Long>,
    JpaSpecificationExecutor<EngagementFindingComment> {
    
}
