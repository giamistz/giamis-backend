package gov.giamis.repository.engagement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.EngagementResource;


/**
 * Spring Data  repository for the EngagementResource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementResourceRepository extends JpaRepository<EngagementResource, Long>, JpaSpecificationExecutor<EngagementResource> {

}
