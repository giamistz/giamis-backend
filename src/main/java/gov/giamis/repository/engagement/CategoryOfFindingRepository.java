package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.CategoryOfFinding;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CategoryOfFinding entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryOfFindingRepository extends JpaRepository<CategoryOfFinding, Long>, JpaSpecificationExecutor<CategoryOfFinding> {

    CategoryOfFinding findByNameIgnoreCase(String name);
}
