package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.MeetingDeliverable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Engagement Team Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetingDeliverableRepository extends
    JpaRepository<MeetingDeliverable, Long>,
    JpaSpecificationExecutor<MeetingDeliverable> {

    List<MeetingDeliverable> findByEngagementTeamMeeting_Id(Long id);
}
