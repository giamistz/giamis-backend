package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementWorkProgram;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementWorkProgram entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementWorkProgramRepository extends JpaRepository<EngagementWorkProgram, Long>, JpaSpecificationExecutor<EngagementWorkProgram> {

}
