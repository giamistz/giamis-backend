package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.WorkProgram;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WorkProgram entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface WorkProgramRepository extends JpaRepository<WorkProgram, Long>, JpaSpecificationExecutor<WorkProgram> {

	WorkProgram findByCodeIgnoreCase(String code);
	
	List<WorkProgram> findByEngagementProcedure_Id(@Param(value="id") Long id);
	
    @Query(value="SELECT COUNT(ep.engagementProcedure) FROM WorkProgram ep WHERE ep.engagementProcedure.id = :id")
	Long countWorkProgramByEngagementProcedure(@Param(value="id") Long id);
    
    WorkProgram findFirstByEngagementProcedure_IdOrderByIdDesc(@Param(value="id") Long id);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(value="UPDATE WorkProgram wp SET wp.name =:name WHERE wp.id =:id")
	void updateWorkProgram(@Param(value="name") String name,@Param(value="id") Long id);

    WorkProgram findFirstByEngagementProcedure_IdAndName(@Param("id")Long id, @Param("name") String name);
}
