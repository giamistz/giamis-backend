package gov.giamis.repository.engagement;

import javax.transaction.Transactional;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import gov.giamis.domain.engagement.EngagementObjectiveRisk;

import java.util.List;

/**
 * Spring Data  repository for the EngagementObjectiveRisk entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementObjectiveRiskRepository extends JpaRepository<EngagementObjectiveRisk, Long>, JpaSpecificationExecutor<EngagementObjectiveRisk> {


    @Query("select distinct engObRisk " +
        "from EngagementObjectiveRisk engObRisk" +
        " join engObRisk.specificObjective obj " +
        "left join fetch engObRisk.riskControlMatrices where obj.engagement.id =:engagementId " )
	List<EngagementObjectiveRisk> findAllWithControl(@Param("engagementId") Long engagementId);

    @Query("select distinct engObRisk " +
        "from EngagementObjectiveRisk engObRisk" +
        " join engObRisk.specificObjective obj " +
        " where obj.engagement.id =:engagementId " )
    List<EngagementObjectiveRisk> findByEngagement(Long engagementId);
}
