package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementPlanningCost;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementPlanningCostRepository extends
    JpaRepository<EngagementPlanningCost, Long>,
    JpaSpecificationExecutor<EngagementPlanningCost> {
    
	EngagementPlanningCost findByEngagement_id(@Param(value="id") Long id);
	
	EngagementPlanningCost findByGfsCode_id(@Param(value="id") Long id);
}
