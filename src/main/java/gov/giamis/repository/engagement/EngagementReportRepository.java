package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementReport;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementReportRepository extends JpaRepository<EngagementReport, Long>, JpaSpecificationExecutor<EngagementReport> {

}
