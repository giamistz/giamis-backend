package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementTeamMeeting;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Engagement Team Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementTeamMeetingRepository extends
    JpaRepository<EngagementTeamMeeting, Long>,
    JpaSpecificationExecutor<EngagementTeamMeeting> {
    
	EngagementTeamMeeting findByEngagement_Id(@Param(value = "id") Long id);
	
	@Query(value="SELECT COUNT(etm.engagement.id) FROM EngagementTeamMeeting etm WHERE etm.engagement.id = :id")
    Long countEngagementTeamMeetingByEngagementId(@Param(value="id") Long id);
}
