package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementPhase;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementPhase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementPhaseRepository extends JpaRepository<EngagementPhase, Long>, JpaSpecificationExecutor<EngagementPhase> {

    EngagementPhase findByNameIgnoreCase(String name);

}
