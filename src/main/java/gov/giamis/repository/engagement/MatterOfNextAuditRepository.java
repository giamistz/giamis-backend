package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.MatterOfNextAudit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MatterOfNextAudit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MatterOfNextAuditRepository extends JpaRepository<MatterOfNextAudit, Long>, JpaSpecificationExecutor<MatterOfNextAudit> {

}
