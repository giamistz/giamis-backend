package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.SubCategoryOfFinding;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SubCategoryOfFinding entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubCategoryOfFindingRepository extends JpaRepository<SubCategoryOfFinding, Long>, JpaSpecificationExecutor<SubCategoryOfFinding> {

    SubCategoryOfFinding findByNameIgnoreCase(String name);
}
