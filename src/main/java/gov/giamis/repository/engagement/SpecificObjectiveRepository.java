package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.SpecificObjective;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


/**
 * Spring Data  repository for the SpecificObjective entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface SpecificObjectiveRepository extends JpaRepository<SpecificObjective, Long>, JpaSpecificationExecutor<SpecificObjective> {
	@Query(value="SELECT spo FROM SpecificObjective spo")
	List<SpecificObjective> findAllSpecificObjective();
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE SpecificObjective sob SET " +
        "sob.code =:code, " +
        "sob.refinedDescription =:refinedDescription, " +
        "sob.isRefined = true " +
        "WHERE sob.id =:id")
	void refine(@Param(value="refinedDescription") String refinedDescription,
                @Param("code") String code,
                @Param(value="id") Long id);

	long countByEngagement_IdAndIsRefinedTrue(Long engagement_id);

	List<SpecificObjective> findByIsRefinedTrue();

    @Query("select distinct obj " +
        "from SpecificObjective obj " +
        "left join fetch obj.engagementObjectiveRisks risk " +
        "where obj.engagement.id =:engagementId and obj.isRefined = true " )
    List<SpecificObjective> findAllWithRisks(@Param("engagementId") Long engagementId);

    List<SpecificObjective> findByEngagement_Id(Long id);

    Page<SpecificObjective> findByEngagement_Id(Long id, Pageable pageable);
}
