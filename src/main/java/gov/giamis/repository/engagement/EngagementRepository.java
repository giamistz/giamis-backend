package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementFinding;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Engagement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementRepository extends JpaRepository<Engagement, Long>, JpaSpecificationExecutor<Engagement> {

	Engagement findByOrganisationUnit_Id(Long id);

	List<Engagement> findByFinancialYear_IdAndOrganisationUnit_Id(Long financialYearId, Long organisationUnitId);


    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query("UPDATE Engagement e set e.controlAdequacy=:controlAdequacy where e.id=:engagementId")
    void setAdequacy(@Param("engagementId")Long engagementId, @Param("controlAdequacy")Boolean controlAdequacy);
}
