package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementMeeting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementMeeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementMeetingRepository extends
    JpaRepository<EngagementMeeting, Long>,
    JpaSpecificationExecutor<EngagementMeeting> {

}
