package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.Engagement;
import gov.giamis.domain.engagement.EngagementFinding;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import gov.giamis.domain.engagement.EngagementProcedure;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementFindingRepository extends
    JpaRepository<EngagementFinding, Long>,
    JpaSpecificationExecutor<EngagementFinding> {

	List<EngagementFinding> findByEngagementProcedure_Id(@Param(value = "id") Long id);

	EngagementFinding findByWorkProgram_Id(@Param(value = "id") Long id);

	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE EngagementFinding ef SET ef.amount =:amount WHERE ef.workProgram.id =:workProgramId")
	void updateEngagementFindingByWorkProgramId(@Param(value = "amount") double amount,@Param(value = "workProgramId") Long workProgramId);

	Long countByEngagementProcedureIn(Collection<@NotNull EngagementProcedure> engagementProcedure);

	@Query(value="FROM EngagementFinding engf " +
        " INNER JOIN EngagementProcedure engp ON engf.engagementProcedure.id = engp.id " +
        " inner join engp.riskControlMatrix as rcm " +
        " inner join rcm.engagementObjectiveRisk as eor" +
        " inner  join eor.specificObjective as so " +
        " WHERE so.engagement.id =:id ")
	List<EngagementFinding> findEngagementFindingByEngagementId(@Param(value="id") Long id);

    @Query(value="FROM EngagementFinding engf " +
        "INNER JOIN EngagementProcedure engp ON engf.engagementProcedure.id = engp.id " +
        "INNER JOIN InternalControl ic ON ic.id = engp.internalControl.id " +
        "INNER JOIN Engagement eng ON ic.engagement.id = eng.id " +
        "WHERE eng.financialYear.id =:financialYear AND eng.organisationUnit.id =:organisationUnit ")
    List<EngagementFinding> findByFinancialYear_IdAndOrganisationUnit_Id(@Param(value = "financialYear") Long financialYear, @Param(value = "organisationUnit") Long organisationUnit);

    @Query(value="FROM EngagementFinding engf " +
        "INNER JOIN EngagementProcedure engp ON engf.engagementProcedure.id = engp.id " +
        "INNER JOIN InternalControl ic ON ic.id = engp.internalControl.id " +
        "INNER JOIN Engagement eng ON ic.engagement.id = eng.id " +
        "WHERE eng.endDate < :date " +
        "AND engf.implementStatus <> 'FullImplemented'" +
        "AND eng.organisationUnit.id =:organisationUnitId " +
        "AND eng.auditableArea.id =:auditableAreaId ")
    List<EngagementFinding> getPreviousFinding(
        @Param("date")LocalDate date,
        @Param("organisationUnitId") Long organisationUnitId,
        @Param("auditableAreaId") Long auditableAreaId);

    @Query("select count(engf.id) FROM EngagementFinding engf " +
        " INNER JOIN EngagementProcedure engp ON engf.engagementProcedure.id = engp.id " +
        " INNER JOIN InternalControl ic ON ic.id = engp.internalControl.id " +
        " where ic.engagement.id = :engagementId")
    Long countByImplementStatusAndEngagement(@Param("engagementId") Long engagementId);

    @Query(value="FROM EngagementFinding engf " +
        " INNER JOIN EngagementProcedure engp ON engf.engagementProcedure.id = engp.id " +
        " inner join engp.riskControlMatrix as rcm " +
        " inner join rcm.engagementObjectiveRisk as eor" +
        " inner  join eor.specificObjective as so " +
        " WHERE so.engagement.organisationUnit.id =:id ")
    List<EngagementFinding> findByOrganisationUnitId(@Param(value = "id") Long id);


}
