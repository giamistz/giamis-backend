package gov.giamis.repository.engagement;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.EngagementProcedure;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Spring Data  repository for the EngagementProcedure entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementProcedureRepository extends
    JpaRepository<EngagementProcedure, Long>,
    JpaSpecificationExecutor<EngagementProcedure> {

    @Query(value = " select count(ep.id) from engagement_procedures as ep" +
        " join  risk_control_matrices as rcm on rcm.id = ep.risk_control_matrix_id " +
        " join engagement_objective_risks as eor on eor.id = rcm.engagement_objective_risk_id " +
        " where eor.specific_objective_id =:id ", nativeQuery = true)
    Long countBySpecificObjective(@Param(value="id") Long id);
    
    List<EngagementProcedure> findByInternalControl_Id(@Param(value="id") Long id);
    List<EngagementProcedure> findByRiskControlMatrix_Id(@Param(value="id") Long id);

    Optional<EngagementProcedure> findById(@Param(value="id") Long id);
    
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(value="UPDATE EngagementProcedure ep SET ep.name =:name WHERE ep.id =:id")
    void updateEngagementProcedure(@Param(value="name") String name,@Param(value="id")Long id);

    EngagementProcedure findFirstByOrderByIdDesc();
    
    EngagementProcedure findFirstById(@Param(value="id") Long id);

    @Query(value="SELECT COUNT(ep.id) FROM EngagementProcedure ep")
    Long countEngagementProcedure();

    EngagementProcedure findFirstByInternalControl_IdAndName(@Param("id")Long id, @Param("name")String name);

    @Query(" select distinct p from EngagementProcedure p " +
        "join p.riskControlMatrix rm " +
        "join rm.engagementObjectiveRisk eor " +
        "join eor.specificObjective sp where sp.id=:id ")
    List<EngagementProcedure> findBySpecificObjective(@Param("id")Long id);
}
