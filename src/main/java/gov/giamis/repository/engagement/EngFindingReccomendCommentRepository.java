package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngFindingRecommendationComment;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the EngFindingReccomendComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngFindingReccomendCommentRepository extends JpaRepository<EngFindingRecommendationComment, Long>, JpaSpecificationExecutor<EngFindingRecommendationComment> {
    List<EngFindingRecommendationComment> findByEngFindingRecommendation_Id(@Param(value = "id") Long id);
}
