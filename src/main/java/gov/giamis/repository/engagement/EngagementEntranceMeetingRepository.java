package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementEntranceMeeting;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Engagement Entrance Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementEntranceMeetingRepository extends
    JpaRepository<EngagementEntranceMeeting, Long>,
    JpaSpecificationExecutor<EngagementEntranceMeeting> {

	EngagementEntranceMeeting findByEngagement_Id(@Param(value = "id") Long id);
	
	@Query(value="SELECT COUNT(etm.engagement.id) FROM EngagementEntranceMeeting etm WHERE etm.engagement.id = :id")
    Long countEngagementEntranceMeetingByEngagementId(@Param(value="id") Long id);
}
