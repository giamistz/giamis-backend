package gov.giamis.repository.engagement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.engagement.Finding;


/**
 * Spring Data  repository for the Finding entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FindingRepository extends JpaRepository<Finding, Long>, JpaSpecificationExecutor<Finding> {

}
