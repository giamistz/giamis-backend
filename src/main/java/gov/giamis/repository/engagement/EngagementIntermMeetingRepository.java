package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementIntermMeeting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementMeeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementIntermMeetingRepository extends
    JpaRepository<EngagementIntermMeeting, Long>,
    JpaSpecificationExecutor<EngagementIntermMeeting> {

}
