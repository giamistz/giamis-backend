package gov.giamis.repository.engagement;
import gov.giamis.domain.engagement.EngagementExitMeeting;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Engagement Exit Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementExitMeetingRepository extends
    JpaRepository<EngagementExitMeeting, Long>,
    JpaSpecificationExecutor<EngagementExitMeeting> {

    EngagementExitMeeting findByEngagement_Id(@Param(value = "id") Long id);
    
    @Query(value="SELECT COUNT(etm.engagement.id) FROM EngagementExitMeeting etm WHERE etm.engagement.id = :id")
    Long countEngagementExitMeetingByEngagementId(@Param(value="id") Long id);
}
