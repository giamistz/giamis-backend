package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.MeetingAgenda;
import gov.giamis.domain.engagement.MeetingDeliverable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data  repository for the Engagement Team Meeting entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface TeamMeetingAgendaRepository extends
    JpaRepository<MeetingAgenda, Long>{

    List<MeetingAgenda> findByEngagementTeamMeeting_Id(Long id);
}
