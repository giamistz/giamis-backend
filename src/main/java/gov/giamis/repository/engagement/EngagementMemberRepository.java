package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.EngagementMember;
import gov.giamis.domain.enumeration.EngagementRole;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

/**
 * Spring Data  repository for the EngagementMember entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface EngagementMemberRepository extends
    JpaRepository<EngagementMember, Long>,
    JpaSpecificationExecutor<EngagementMember> {
	
	@Modifying(clearAutomatically =true, flushAutomatically =true)
	@Query(value="UPDATE EngagementMember em SET em.acceptanceDate =:acceptanceDate WHERE em.id =:id")
	void approveEngagementMember(@Param(value = "acceptanceDate") LocalDate acceptanceDate,@Param(value = "id") Long id);

    List<EngagementMember> findByEngagement_Id(Long id);

    int countByEngagement_IdAndRole(Long engagementId, EngagementRole role);

    EngagementMember findByEngagement_IdAndRole(Long engagementId, EngagementRole role);
}
