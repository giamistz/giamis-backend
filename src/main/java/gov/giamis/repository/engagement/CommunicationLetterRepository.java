package gov.giamis.repository.engagement;

import gov.giamis.domain.engagement.CommunicationLetter;
import gov.giamis.domain.enumeration.LetterType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;


/**
 * Spring Data  repository for the CommunicationLetter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommunicationLetterRepository
    extends JpaRepository<CommunicationLetter, Long>,
    JpaSpecificationExecutor<CommunicationLetter> {

    Optional<CommunicationLetter> findByTypeAndEngagement_Id(@NotNull LetterType type, Long engagement_id);

}
