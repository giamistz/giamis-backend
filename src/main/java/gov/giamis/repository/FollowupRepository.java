package gov.giamis.repository;

import gov.giamis.domain.Followup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Followup entity.
 */
@Repository
public interface FollowupRepository extends JpaRepository<Followup, Long>, JpaSpecificationExecutor<Followup> {

    @Query(value = "select distinct followup from Followup followup left join fetch followup.engagements",
        countQuery = "select count(distinct followup) from Followup followup")
    Page<Followup> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct followup from Followup followup left join fetch followup.engagements")
    List<Followup> findAllWithEagerRelationships();

    @Query("select followup from Followup followup left join fetch followup.engagements where followup.id =:id")
    Optional<Followup> findOneWithEagerRelationships(@Param("id") Long id);

}
