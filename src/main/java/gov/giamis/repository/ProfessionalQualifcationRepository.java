package gov.giamis.repository;

import gov.giamis.domain.ProfessionalQualifcation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProfessionalQualifcation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfessionalQualifcationRepository extends JpaRepository<ProfessionalQualifcation, Long>, JpaSpecificationExecutor<ProfessionalQualifcation> {

}
