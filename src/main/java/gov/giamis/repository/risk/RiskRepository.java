package gov.giamis.repository.risk;

import gov.giamis.domain.risk.Risk;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data  repository for the Risk entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface RiskRepository extends JpaRepository<Risk, Long>, JpaSpecificationExecutor<Risk> {

	Risk findByCodeIgnoreCase(String code);
	
	List<Risk> findByOrganisationUnit_Id(@Param(value = "orgUnitId") Long orgUnitId);
	
	List<Risk> findByIdAndOrganisationUnit_Id(@Param(value = "id") Long id,@Param(value = "orgUnitId") Long orgUnitId);
	
	List<Risk> findByRiskRegister_Id(@Param(value = "riskRegisterId") Long riskRegisterId);
	
	List<Risk> findByIdAndRiskRegister_Id(@Param(value = "id") Long id,@Param(value = "riskRegisterId") Long riskRegisterId);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE Risk r SET r.isApproved =true WHERE r.organisationUnit.id =:orgUnitId")
	void updateRiskByOrganisationUnitId(@Param(value = "orgUnitId") Long orgUnitId);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE Risk r SET r.isApproved =true WHERE r.id=:id AND r.organisationUnit.id =:orgUnitId")
	void updateRiskByIdAndOrganisationUnitId(@Param(value = "id") Long id,@Param(value = "orgUnitId") Long orgUnitId);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE Risk r SET r.isApproved =true WHERE r.riskRegister.id =:riskRegisterId")
	void updateRiskByRiskRegisterId(@Param(value = "riskRegisterId") Long riskRegisterId);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value="UPDATE Risk r SET r.isApproved =true WHERE r.id=:id AND r.riskRegister.id =:riskRegisterId")
	void updateRiskByIdAndRiskRegisterId(@Param(value = "id") Long id,@Param(value = "riskRegisterId") Long riskRegisterId);

	@Query(" SELECT MAX(code) from Risk r where r.riskRegister.id =:riskRegisterId and r.target.id =:targetId ")
	String maxByRegisterId(@Param("riskRegisterId") Long riskRegisterId, @Param("targetId") Long targetId);
}
