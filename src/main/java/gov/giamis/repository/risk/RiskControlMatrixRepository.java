package gov.giamis.repository.risk;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.risk.RiskControlMatrix;

import java.util.List;


/**
 * Spring Data  repository for the RiskControlMatrix entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RiskControlMatrixRepository extends JpaRepository<RiskControlMatrix, Long>, JpaSpecificationExecutor<RiskControlMatrix> {

	
	@Query(value="SELECT COUNT(rcm.id) FROM RiskControlMatrix rcm")
	Long countRiskControlMatrix();

    @Query("select distinct cm " +
        " from RiskControlMatrix  cm " +
        " join cm.internalControl as co " +
        " left join fetch cm.engagementProcedures p " +
        " where co.engagement.id =:engagementId and cm.isKeyControl = true " )
    List<RiskControlMatrix> findAllWithProcedures(@Param("engagementId") Long engagementId);
}
