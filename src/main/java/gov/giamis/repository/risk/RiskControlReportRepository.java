package gov.giamis.repository.risk;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.risk.RiskControlReport;


/**
 * Spring Data  repository for the RiskControlReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RiskControlReportRepository extends JpaRepository<RiskControlReport, Long>, JpaSpecificationExecutor<RiskControlReport> {

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(value="UPDATE RiskControlReport r SET r.isApproved =true WHERE r.id =:Id")
    void updateRiskControlReportById(@Param(value = "Id") Long Id);
}
