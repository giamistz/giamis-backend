package gov.giamis.repository.risk;

import gov.giamis.domain.risk.Target;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Target entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TargetRepository extends JpaRepository<Target, Long>, JpaSpecificationExecutor<Target> {

    Target findByDescriptionIgnoreCase(String name);
}
