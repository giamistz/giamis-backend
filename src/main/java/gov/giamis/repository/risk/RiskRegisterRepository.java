package gov.giamis.repository.risk;

import gov.giamis.domain.risk.RiskRegister;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RiskRegister entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RiskRegisterRepository extends JpaRepository<RiskRegister, Long>, JpaSpecificationExecutor<RiskRegister> {

    RiskRegister findByNameIgnoreCase(String name);
}
