package gov.giamis.repository.risk;

import gov.giamis.domain.risk.RiskRating;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RiskRating entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RiskRatingRepository extends JpaRepository<RiskRating, Long>, JpaSpecificationExecutor<RiskRating> {

}
