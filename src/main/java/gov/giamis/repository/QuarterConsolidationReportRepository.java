package gov.giamis.repository;

import gov.giamis.domain.QuarterConsolidationReport;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the QuarterConsolidationReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuarterConsolidationReportRepository extends JpaRepository<QuarterConsolidationReport, Long>, JpaSpecificationExecutor<QuarterConsolidationReport> {

    QuarterConsolidationReport findFirstByOrganisationUnit_IdAndQuarter_Id(Long organisationUnit_id, Long quarter_id);
}
