package gov.giamis.repository;

import gov.giamis.domain.HighQualification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HighQualification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HighQualificationRepository extends JpaRepository<HighQualification, Long>, JpaSpecificationExecutor<HighQualification> {

}
