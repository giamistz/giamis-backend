package gov.giamis.repository.report;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.report.ReportContentValue;


/**
 * Spring Data  repository for the ReportContentValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportContentValueRepository extends JpaRepository<ReportContentValue, Long>, JpaSpecificationExecutor<ReportContentValue> {

}
