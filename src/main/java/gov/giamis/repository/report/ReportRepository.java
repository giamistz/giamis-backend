package gov.giamis.repository.report;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.report.Report;


/**
 * Spring Data  repository for the Report entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportRepository extends JpaRepository<Report, Long>, JpaSpecificationExecutor<Report> {

}
