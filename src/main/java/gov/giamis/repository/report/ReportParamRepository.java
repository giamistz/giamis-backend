package gov.giamis.repository.report;


import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.report.ReportParam;


/**
 * Spring Data  repository for the ReportParam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportParamRepository extends JpaRepository<ReportParam, Long>, JpaSpecificationExecutor<ReportParam> {

}
