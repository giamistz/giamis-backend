package gov.giamis.repository.report;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.report.ReportContent;


/**
 * Spring Data  repository for the ReportContent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportContentRepository extends JpaRepository<ReportContent, Long>, JpaSpecificationExecutor<ReportContent> {

}
