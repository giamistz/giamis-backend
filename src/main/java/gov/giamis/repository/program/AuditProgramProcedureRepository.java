package gov.giamis.repository.program;

import gov.giamis.domain.program.AuditProgramProcedure;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditProgramProcedure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditProgramProcedureRepository extends JpaRepository<AuditProgramProcedure, Long>, JpaSpecificationExecutor<AuditProgramProcedure> {
    }
