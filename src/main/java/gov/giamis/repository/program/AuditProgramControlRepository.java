package gov.giamis.repository.program;

import gov.giamis.domain.program.AuditProgramControl;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditProgramControl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditProgramControlRepository extends JpaRepository<AuditProgramControl, Long>, JpaSpecificationExecutor<AuditProgramControl> {

    @Query(value="FROM AuditProgramControl apc " +
        "INNER JOIN AuditProgramEngagement ape ON apc.auditProgramEngagement.id = ape.id " +
        "WHERE ape.id =:engagementId ")
    List<AuditProgramControl> findAuditProgramControlsByAuditProgramEngagement_Id(@Param(value = "engagementId") Long engagementId);

}
