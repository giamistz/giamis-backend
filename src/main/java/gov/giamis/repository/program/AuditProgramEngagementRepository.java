package gov.giamis.repository.program;

import gov.giamis.domain.program.AuditProgramEngagement;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AuditProgramEngagement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditProgramEngagementRepository extends JpaRepository<AuditProgramEngagement, Long>, JpaSpecificationExecutor<AuditProgramEngagement> {

}
