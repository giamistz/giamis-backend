package gov.giamis.repository.program;

import gov.giamis.domain.program.AuditProgramRisk;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditProgramRisk entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditProgramRiskRepository extends JpaRepository<AuditProgramRisk, Long>, JpaSpecificationExecutor<AuditProgramRisk> {
}
