package gov.giamis.repository.program;

import gov.giamis.domain.program.AuditProgramObjective;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the AuditProgramObjective entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditProgramObjectiveRepository extends JpaRepository<AuditProgramObjective, Long>, JpaSpecificationExecutor<AuditProgramObjective> {

    @Query(value="FROM AuditProgramObjective apo " +
        "INNER JOIN AuditProgramEngagement ape ON apo.auditProgramEngagement.id = ape.id " +
        "WHERE ape.id =:engagementId ")
    List<AuditProgramObjective> findAuditProgramObjectiveByAuditProgramEngagement_Id(@Param(value = "engagementId") Long engagementId);
}
