package gov.giamis.repository;

import gov.giamis.domain.EngagementSampling;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EngagementSampling entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EngagementSamplingRepository extends JpaRepository<EngagementSampling, Long>, JpaSpecificationExecutor<EngagementSampling> {

}
