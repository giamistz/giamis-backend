package gov.giamis.repository;

import gov.giamis.domain.QuarterReport;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the QuarterReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuarterReportRepository extends JpaRepository<QuarterReport, Long>, JpaSpecificationExecutor<QuarterReport> {

    QuarterReport findFirstByQuarter_Id(Long quarter_id);
}
