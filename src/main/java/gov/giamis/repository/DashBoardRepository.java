package gov.giamis.repository;

import gov.giamis.dto.AuditableAreaQuarterValueDTO;
import gov.giamis.dto.OrgUnitAuditorDTO;
import gov.giamis.dto.QuarterValueDTO;
import gov.giamis.dto.RecommendationStatusDTO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class DashBoardRepository {

    @PersistenceContext
    public EntityManager entityManager;


    public List<QuarterValueDTO> engagementByQuarter(
        @Param("financialYearId") Long financialYearId,
        @Param("orgInits")List<Long> orgUnits
        ) {

       return entityManager.createNativeQuery("select " +
           "  q.name as quarter, " +
           "  count(e.id) as value " +
           " from engagements e" +
           " join financial_years fy on e.financial_year_id = fy.id " +
           " join quarters q on q.start_date <= e.start_date and q.end_date >= e.end_date " +
           " where e.organisation_unit_id in :orgUnits and e.financial_year_id =:financialYearId  " +
           " group by q.id ", QuarterValueDTO.class)
           .setParameter("financialYearId", financialYearId)
           .setParameter("orgUnits", orgUnits)
           .getResultList();

    }

    public List<AuditableAreaQuarterValueDTO> getByAreaByQrt( @Param("financialYearId") Long financialYearId,
                                                              @Param("orgInits")List<Long> orgUnits) {

        return  entityManager.createNativeQuery("select " +
            "  count(e.id) as value, " +
            "  q.name as quarter, " +
            "  a.name as auditable_area " +
            "  from engagements e " +
            "       join financial_years fy on e.financial_year_id = fy.id\n" +
            "       join quarters q on q.start_date <= e.start_date and q.end_date >= e.end_date " +
            "  join auditable_areas as a on a.id = e.auditable_area_id " +
            " where e.financial_year_id =:financialYearId and e.organisation_unit_id in :orgUnits " +
            " group by q.id, a.id", AuditableAreaQuarterValueDTO.class)
            .setParameter("financialYearId", financialYearId)
            .setParameter("orgUnits", orgUnits)
            .getResultList();

    }

    public List<RecommendationStatusDTO> recommByStatus( @Param("financialYearId") Long financialYearId,
                                                              @Param("orgInits")List<Long> orgUnits) {

        return  entityManager.createNativeQuery("select " +
            "  count(efr.id) as value, " +
            "  efr.status as status " +
            " from eng_finding_recommendations as efr " +
            " join engagement_findings ef on efr.engagement_finding_id = ef.id " +
            " join engagement_procedures ep on ef.engagement_procedure_id = ep.id " +
            " join risk_control_matrices rcm on ep.risk_control_matrix_id = rcm.id " +
            " join engagement_objective_risks eor on rcm.engagement_objective_risk_id = eor.id " +
            " join specific_objectives so on eor.specific_objective_id = so.id " +
            " join engagements e on so.engagement_id = e.id " +
            " where e.financial_year_id =:financialYearId and e.organisation_unit_id in :orgUnits " +
            " group by efr.status", RecommendationStatusDTO.class)
            .setParameter("financialYearId", financialYearId)
            .setParameter("orgUnits", orgUnits)
            .getResultList();

    }

    public List<OrgUnitAuditorDTO> auditors(@Param("orgInits")List<Long> orgUnits) {

        return  entityManager.createNativeQuery("select " +
            "       count(u.id) as value," +
            "       ou.name as organisation_unit " +
            " from organisation_units  as ou " +
            " left join users u on u.organisation_unit_id = ou.id and u.is_auditor = true " +
            " where ou.id in :orgUnits " +
            " group by ou.id", OrgUnitAuditorDTO.class)
            .setParameter("orgUnits", orgUnits)
            .getResultList();

    }


}
