package gov.giamis.repository;

import gov.giamis.domain.Notification;
import gov.giamis.domain.setup.FileResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Spring Data  repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {
   long deleteByFileResource(FileResource fileResource);
   List<Notification> findByIsSent(Boolean isSent);

    Page<Notification> findByEmailOrderByIdDesc(String email, Pageable pageable);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(" update Notification as n SET n.isRead= true  where n.id =:id")
    void read(@Param("id") Long id);
}
