package gov.giamis.repository.setup;

import gov.giamis.domain.setup.OrganisationUnit;
import gov.giamis.domain.setup.User;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


import java.time.Instant;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    String AUTHORITIES_BY_EMAIL_CACHE = "authoritiesByEmail";

    String ORGUNIT_BY_PARENTS = "orgUnitByParent";

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    @EntityGraph(attributePaths = {
        "organisationUnit", "organisationUnit.organisationUnitLevel"
    })
    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByEmailIgnoreCaseAndIdNot(String email, Long id);

    @EntityGraph(attributePaths = {"authorities","roles"})
    Optional<User> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = {"roles"})
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
    Optional<User> findOneWithAuthoritiesByEmailIgnoreCase(String email);

    @EntityGraph(attributePaths = {
        "organisationUnit", "roles","auditorProfile"
    })
    Page<User> findAllByOrganisationUnit_IdIn(Collection<Long> organisationUnit_id, Pageable pageable);

    @EntityGraph(attributePaths = {
        "organisationUnit", "roles","auditorProfile"
    })
    Page<User> findAllByEmailIgnoreCase(String email, Pageable pageable);
    
    User findByEmail(String email);
    
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query(value="UPDATE User u SET u.organisationUnit = :organisationUnit WHERE u.email =:email")
    void updateUserOrganisationUnitByEmail(@Param(value="organisationUnit") OrganisationUnit organisationUnit,@Param(value="email") String email);
    
    Optional<User> findById(Long id);
    
    @Query("FROM User u WHERE u.id =:id")
    User findUserById(@Param(value = "id") Long id);
    
    List<User> findByRoles_NameIgnoreCase(@Param(value = "name")String name);
    
    @Query(value="FROM User u\r\n" + 
    	      "INNER JOIN EngagementMember em ON em.user.id = u.id\r\n" + 
    	      "INNER JOIN Engagement en ON en.organisationUnit.id = u.organisationUnit.id\r\n" + 
    	      "WHERE en.auditType = gov.giamis.domain.enumeration.AuditType.PLANNED")
   List<User> getUserByEngagementPlannedAuditee();
   
    @Query(value="FROM User u INNER JOIN EngagementMember em ON em.user.id = u.id INNER JOIN Engagement en ON en.id = em.engagement.id")
     List<User> findAll();

    @Query(value = " WITH RECURSIVE children AS ( " +
        "  SELECT id, organisation_unit_id AS parent_id, name " +
        "  FROM organisation_units " +
        "  WHERE id =:organisationUnitId " +
        "  UNION " +
        "  SELECT o.id, o.organisation_unit_id, o.name " +
        "  FROM organisation_units o " +
        "         INNER JOIN children c ON c.id = o.organisation_unit_id " +
        ") SELECT id FROM children ", nativeQuery = true)
    List<Long> getUserOrganisations(@Param("organisationUnitId") Long organisationUnitId);
    
}
