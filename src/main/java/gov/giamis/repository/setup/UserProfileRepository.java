package gov.giamis.repository.setup;

import java.time.LocalDate;
import java.util.Set;

import gov.giamis.domain.ProfessionalQualifcation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.UserProfile;

/**
 * Spring Data JPA repository for the {@link UserProfile} entity.
 */
@Repository
@Transactional
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>,JpaSpecificationExecutor<UserProfile> {

	UserProfile findByCheckNumberAllIgnoreCase(String checkNumber);

	@Modifying(clearAutomatically =true,flushAutomatically =true)
	@Query(value = "UPDATE UserProfile SET voteCode =:voteCode,gender =:gender,title =:title,checkNumber =:checkNumber,dateOfBirth =:dateOfBirth,\r\n" +
			"appointmentDate =:appointmentDate,workExperience =:workExperience WHERE user.id =:userId")
	public void updateUserProfileByUserId(@Param(value = "voteCode") String voteCode,@Param(value = "gender") String gender,@Param(value = "title") String title,
			@Param(value = "checkNumber") String checkNumber,@Param(value = "dateOfBirth") LocalDate dateOfBirth,@Param(value = "appointmentDate") LocalDate appointmentDate,
			@Param(value = "workExperience") String workExperience,
			@Param(value = "userId") Long userId);

    // void updateUserProfileByUserId(String voteCode, String gender, String title, String checkNumber, LocalDate dateOfBirth, LocalDate appointmentDate, String workExperience, Long id);
}
