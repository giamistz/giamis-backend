package gov.giamis.repository.setup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import gov.giamis.domain.setup.ProfessionalTraining;

/**
 * Spring Data JPA repository for the {@link ProfessionalTraining} entity.
 */
@Repository
@Transactional
public interface ProfessionalTrainingRepository extends JpaRepository<ProfessionalTraining, Long>,JpaSpecificationExecutor<ProfessionalTraining> {

    ProfessionalTraining findByUser_Id(Long id);
    
}
