package gov.giamis.repository.setup;

import gov.giamis.domain.setup.AuditableArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the AuditableArea entity.
 */
@Repository
public interface AuditableAreaRepository extends
    JpaRepository<AuditableArea, Long>,
    JpaSpecificationExecutor<AuditableArea> {

    @Query(value = "select distinct auditableArea " +
        "from AuditableArea auditableArea " +
        "left join fetch auditableArea.organisationUnitGroups",
        countQuery = "select count(distinct auditableArea) " +
            "from AuditableArea auditableArea")
    Page<AuditableArea> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct auditableArea " +
        "from AuditableArea auditableArea " +
        "left join fetch auditableArea.organisationUnitGroups",
        countQuery = "select count(distinct auditableArea) " +
            "from AuditableArea auditableArea")
    Page<AuditableArea> findAllWithEagerRelationships(
        Specification specification,
        Pageable pageable);

    @Query("select distinct auditableArea " +
        "from AuditableArea auditableArea " +
        "left join fetch auditableArea.organisationUnitGroups")
    List<AuditableArea> findAllWithEagerRelationships();

    @Query("select auditableArea from AuditableArea auditableArea left " +
        "join fetch auditableArea.organisationUnitGroups " +
        "where auditableArea.id =:id")
    Optional<AuditableArea> findOneWithEagerRelationships(@Param("id") Long id);
     
    public AuditableArea findByNameAllIgnoreCase(String name);
}
