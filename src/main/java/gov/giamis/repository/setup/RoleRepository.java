package gov.giamis.repository.setup;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.setup.Role;

/**
 * Spring Data  repository for the Role entity.
 */
@Repository
public interface RoleRepository extends
    JpaRepository<Role, Long>,
    JpaSpecificationExecutor<Role> {

    @Query(value = "select distinct role from Role role " +
        "left join fetch role.authorities",
        countQuery = "select count(distinct role) " +
            "from Role role")
    Page<Role> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct role " +
        "from Role role " +
        "left join fetch role.authorities")
    List<Role> findAllWithEagerRelationships();

    @Query("select role " +
        "from Role role " +
        "left join fetch role.authorities " +
        "where role.id =:id")
    Optional<Role> findOneWithEagerRelationships(@Param("id") Long id);
    
    Role findByNameIgnoreCase(String name);
   
   
    @Query(value="FROM Role r WHERE r.id =:id")
    Role findRoleById(@Param(value="id") Long id);
    
    @Query(value="select new Role(r.id, r.name) from Role r")
    List<Role> findAll();
    
    @Query(value="SELECT r FROM Role r ORDER BY r.id DESC")
    List<Role> findAllRoles();
    
    @Query(value="SELECT COUNT(r.id) FROM Role r")
   	Long countRoleById();

    Role findByCode(String code);
}
