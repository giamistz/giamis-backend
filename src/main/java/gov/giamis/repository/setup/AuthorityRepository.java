package gov.giamis.repository.setup;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.giamis.domain.setup.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends
    JpaRepository<Authority, Long> ,
    JpaSpecificationExecutor<Authority> {

    Optional<Authority> findByName(String s);
    
    @Query(value="FROM Authority GROUP BY id ORDER BY name")
    List<Authority> findAuthorityOrderByName();
    
    List<Authority> findDistinctAuthorityByResourceOrderByNameDesc(String resource);
    
    @Query(value="SELECT DISTINCT new Authority(au.resource) FROM Authority au order by au.resource")
    List<Authority> findDistinctAuthority();

    @Query(value="SELECT au.id,au.name,au.display_name,au.resource,au.action FROM authorities au " +
    		"INNER JOIN role_authorities ra ON au.id = ra.authority_id " +
    		"INNER JOIN roles rs ON rs.id = ra.role_id " +
    		"INNER JOIN user_roles urs ON urs.role_id = rs.id " +
    		"WHERE urs.user_id =:userId " +
            "UNION " +
            "SELECT au.id,au.name,au.display_name,au.resource,au.action from user_authorities ua " +
            "INNER JOIN authorities au on ua.authority_id = au.id where ua.user_id=:userId",nativeQuery = true)
    Set<Authority> getAllAuthorities(@Param(value="userId") Long userId);

    @Query(value="SELECT au.id,au.name,au.display_name,au.resource,au.action FROM authorities au " +
    		"INNER JOIN role_authorities ra ON au.id = ra.authority_id " +
    		"INNER JOIN roles rs ON rs.id = ra.role_id " +
    		"INNER JOIN user_roles urs ON urs.role_id = rs.id " +
    		"INNER JOIN users u ON urs.user_id = u.id " +
    		"WHERE u.email =:email " +
            "UNION " +
            "SELECT au.id,au.name,au.display_name,au.resource,au.action from user_authorities ua " +
            "INNER JOIN authorities au on ua.authority_id = au.id " +
            "INNER JOIN users u on u.id = ua.user_id " +
            "WHERE u.email=:email",nativeQuery = true)
    Set<Authority> getAllAuthoritiesByEmail(@Param(value="email") String email);

    @Query(value="FROM Authority")
    Set<Authority> findAllAuthorities();

}
