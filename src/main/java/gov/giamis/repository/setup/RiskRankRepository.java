package gov.giamis.repository.setup;

import gov.giamis.domain.setup.RiskRank;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface RiskRankRepository extends
    JpaRepository<RiskRank, Long>,
    JpaSpecificationExecutor<RiskRank> {

    RiskRank findByNameAllIgnoreCase(String name);
    
    @Query(value="SELECT r FROM RiskRank r WHERE r.minValue <= :result AND r.maxValue >= :result ")
    List<RiskRank> getColorFromRiskRank(@Param("result") Integer result);
    
}
