package gov.giamis.repository.setup;

import gov.giamis.domain.setup.FileResourceType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the FileResourceType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileResourceTypeRepository extends
    JpaRepository<FileResourceType, Long>,
    JpaSpecificationExecutor<FileResourceType> {

    Optional<FileResourceType> findByNameIgnoreCase(String name);
	public FileResourceType findByNameAllIgnoreCase(String name);
}
