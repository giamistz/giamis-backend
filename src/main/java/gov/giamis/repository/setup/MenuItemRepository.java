package gov.giamis.repository.setup;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import gov.giamis.domain.setup.Authority;
import gov.giamis.domain.setup.MenuGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.setup.MenuItem;

/**
 * Spring Data  repository for the MenuItem entity.
 */
@Repository
public interface MenuItemRepository extends
    JpaRepository<MenuItem, Long>,
    JpaSpecificationExecutor<MenuItem> {

    @Query(value = "select distinct menuitem from MenuItem menuitem " +
        "left join fetch menuitem.authorities",
        countQuery = "select count(distinct menuitem) " +
            "from MenuItem menuitem")
    Page<MenuItem> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct menuitem " +
        "from MenuItem menuitem " +
        "left join fetch menuitem.authorities")
    List<MenuItem> findAllWithEagerRelationships();

    @Query("select menuitem " +
        "from MenuItem menuitem " +
        "left join fetch menuitem.authorities " +
        "where menuitem.id =:id")
    Optional<MenuItem> findOneWithEagerRelationships(@Param("id") Long id);
    
    MenuItem findByNameIgnoreCase(String name);
   
   
    @Query(value="FROM MenuItem mn WHERE mn.id =:id")
    MenuItem findMenuItemById(@Param(value="id") Long id);
    
    @Query(value="SELECT DISTINCT mgp.id,mgp.name,mgp.path,mgp.icon,mgp.created_by,mgp.created_date,mgp.last_modified_by,"
    		+ "mgp.last_modified_date,mgp.menu_group_id FROM menu_items mi \r\n" + 
            "INNER JOIN menu_groups mgp on mgp.id = mi.menu_group_id \r\n" + 
            "INNER JOIN menu_items_authorities mia on mia.menu_item_id = mi.id\r\n" + 
            "INNER JOIN authorities au on au.id = mia.authority_id\r\n" + 
            "INNER JOIN role_authorities ra on ra.authority_id = au.id\r\n" + 
            "INNER JOIN roles rs on rs.id = ra.role_id\r\n" + 
            "INNER JOIN user_roles urs on urs.role_id = rs.id\r\n" + 
            "WHERE mi.menu_group_id =:menuGroupId ",nativeQuery = true)
    List<MenuItem> getAllMenuItemsByGroup(@Param(value="menuGroupId") Long menuGroupId);

    @Query(value = "select mi.* from menu_items mi join menu_items_authorities ma " +
        " on ma.menu_item_id = mi.id where ma.authority_id in (:authorities) and mi.menu_group_id is not null ", nativeQuery = true)
    List<MenuItem> findByAuthorities(@Param("authorities") List<Long> authorities);

    @Query(value = "select mi.* from menu_items mi join menu_items_authorities ma " +
        " on ma.menu_item_id = mi.id where ma.authority_id in (:authorities) and mi.menu_group_id is null", nativeQuery = true)
    List<MenuItem> findWithGroupByAuthorities(@Param("authorities") List<Long> authorities);

}
