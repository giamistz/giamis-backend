package gov.giamis.repository.setup;

import gov.giamis.domain.setup.OrganisationUnitGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationUnitGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationUnitGroupRepository extends
    JpaRepository<OrganisationUnitGroup, Long>,
    JpaSpecificationExecutor<OrganisationUnitGroup> {

    OrganisationUnitGroup findByNameIgnoreCase(String name);
}
