package gov.giamis.repository.setup;

import gov.giamis.domain.setup.Objective;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface ObjectiveRepository extends
    JpaRepository<Objective, Long>,
    JpaSpecificationExecutor<Objective> {

    Objective findByDescriptionIgnoreCase(String description);
}
