package gov.giamis.repository.setup;

import gov.giamis.domain.setup.FileResource;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the FileResource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileResourceRepository extends
    JpaRepository<FileResource, Long>,
    JpaSpecificationExecutor<FileResource> {

    FileResource findByNameAndContentMd5AllIgnoreCase(String name, String contentMd5);

    @Modifying
    @Query("update FileResource f set f.isAssigned = :isAssigned where f.id = :id")
    int setAssigned(@Param("id") Long id, @Param("isAssigned") Boolean isAssigned);

    List<FileResource> findByIsAssigned(Boolean isAssigned);
    
    FileResource findByNameAllIgnoreCase(String name);
}
