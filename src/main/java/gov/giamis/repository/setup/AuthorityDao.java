package gov.giamis.repository.setup;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.setup.Authority;

@Repository
public class AuthorityDao {
	@PersistenceContext
	public EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Authority> getAllAuthorities(
			@Param("getResource") String getResource) {
		return (entityManager
				.createNamedStoredProcedureQuery("getauthorities")
				.setParameter("getResource", getResource)
				.getResultList());
	}
}
