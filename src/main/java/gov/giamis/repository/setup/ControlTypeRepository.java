package gov.giamis.repository.setup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.setup.ControlType;


/**
 * Spring Data  repository for the ControlType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ControlTypeRepository extends JpaRepository<ControlType, Long>, JpaSpecificationExecutor<ControlType> {

	ControlType findByNameIgnoreCase(String name);
	
}
