package gov.giamis.repository.setup;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import gov.giamis.domain.setup.OrganisationUnit;


/**
 * Spring Data  repository for the OrganisationUnit entity.
 */
@Repository
@Transactional
public interface OrganisationUnitRepository extends
    JpaRepository<OrganisationUnit, Long>,
    JpaSpecificationExecutor<OrganisationUnit> {

	
    @Query(value = "select distinct organisationUnit " +
        "from OrganisationUnit organisationUnit " +
        "left join fetch organisationUnit.organisationUnitGroups",
        countQuery = "select count(distinct organisationUnit) " +
            "from OrganisationUnit organisationUnit")
    Page<OrganisationUnit> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct organisationUnit " +
        "from OrganisationUnit organisationUnit " +
        "left join fetch organisationUnit.organisationUnitGroups",
        countQuery = "select count(distinct organisationUnit) " +
            "from OrganisationUnit organisationUnit")
    @EntityGraph(attributePaths = {"fileResource","parentOrganisationUnit", "organisationUnitLevel", "user"})
    Page<OrganisationUnit> findAllWithEagerRelationships(Specification specification,
                                                         Pageable pageable);

    @Query(value = "select distinct organisationUnit " +
        "from OrganisationUnit organisationUnit " +
        "left join fetch organisationUnit.organisationUnitGroups " +
        "where organisationUnit.parentOrganisationUnit.id =:parentOrgUnitId ",
         countQuery = "select count(distinct organisationUnit) " +
            "from OrganisationUnit organisationUnit")
    @EntityGraph(attributePaths = {"fileResource","parentOrganisationUnit", "organisationUnitLevel", "user"})
    Page<OrganisationUnit> findByParentOrUnit(@Param("parentOrgUnitId") Long parentOrgUnitId,
                                              Pageable pageable);

    @Query("select distinct organisationUnit " +
        "from OrganisationUnit organisationUnit " +
        "left join fetch organisationUnit.organisationUnitGroups")
    @EntityGraph(attributePaths = {"organisationUnitLevel", "user", "parentOrganisationUnit"})
    List<OrganisationUnit> findAllWithEagerRelationships();

    @Query("select organisationUnit " +
        "from OrganisationUnit organisationUnit " +
        "left join fetch organisationUnit.organisationUnitGroups " +
        "where organisationUnit.id =:id")
    @EntityGraph(attributePaths = {"organisationUnitLevel", "user", "parentOrganisationUnit"})
    Optional<OrganisationUnit> findOneWithEagerRelationships(@Param("id") Long id);

    @EntityGraph(attributePaths = {"organisationUnitLevel", "user", "parentOrganisationUnit"})
    List<OrganisationUnit> findByParentOrganisationUnitId(@Param("parentId") Long parentId);
    
    OrganisationUnit findByName(String name);
    
    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Query("UPDATE OrganisationUnit ou SET  ou.user.id =:userId WHERE ou.id =:id")
    void updateUserOfOrganisationUnitById(@Param(value="userId") Long userId,@Param(value="id") Long id);

    OrganisationUnit findByCode(String code);
}
