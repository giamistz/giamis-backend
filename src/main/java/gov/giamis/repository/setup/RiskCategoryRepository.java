package gov.giamis.repository.setup;

import gov.giamis.domain.setup.RiskCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface RiskCategoryRepository extends
    JpaRepository<RiskCategory, Long>,
    JpaSpecificationExecutor<RiskCategory> {

    RiskCategory findByNameIgnoreCase(String name);
}
