package gov.giamis.repository.setup;

import gov.giamis.domain.setup.OrganisationUnitGroupSet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationUnitGroupSet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationUnitGroupSetRepository extends
    JpaRepository<OrganisationUnitGroupSet, Long>,
    JpaSpecificationExecutor<OrganisationUnitGroupSet> {

    OrganisationUnitGroupSet findByNameIgnoreCase(String name);
	
}
