package gov.giamis.repository.setup;

import gov.giamis.domain.setup.OrganisationUnitLevel;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationUnitLevel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationUnitLevelRepository extends
    JpaRepository<OrganisationUnitLevel, Long>,
    JpaSpecificationExecutor<OrganisationUnitLevel> {

    OrganisationUnitLevel findByNameIgnoreCase(String name);

    OrganisationUnitLevel findByCode(String code);
}
