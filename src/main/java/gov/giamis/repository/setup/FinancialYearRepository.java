package gov.giamis.repository.setup;

import gov.giamis.domain.setup.FinancialYear;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data repository for the Period entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface FinancialYearRepository
		extends JpaRepository<FinancialYear, Long>, 
		JpaSpecificationExecutor<FinancialYear> {

    FinancialYear findByNameIgnoreCase(String name);
	
    FinancialYear findStatusById(Long id);

	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value = "UPDATE FinancialYear  set status=:status where id = :id")
	void updateFinancialYearStatus(@Param("status") Integer status, @Param("id") Long id);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value = "UPDATE FinancialYear  set status=:status where id != :id")
	void updateOtherFinancialYearStatus(@Param("status") Integer status, @Param("id") Long id);
	
	FinancialYear findByStatus(Integer status);
	
	@Query(value="SELECT fy FROM FinancialYear fy " +
        "WHERE fy.status =:status AND fy.startDate >=:startDate " +
        "AND fy.endDate >=:endDate")
    FinancialYear queryCurrentFinancialYearRange(
			@Param(value="status") Integer status,
			@Param(value="startDate") LocalDate startDate,
			@Param(value="endDate") LocalDate endDate);
	
	Page<FinancialYear> findAllByOrderByIdDesc(Pageable pageable);

	@Query(" SELECT fy from FinancialYear fy where " +
        "fy.startDate <=:currentDate and  fy.endDate >=:currentDate ")
    FinancialYear findCurrent(@Param("currentDate") LocalDate currentDate);
}
