package gov.giamis.repository.setup;

import gov.giamis.domain.setup.Quarter;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unused")
@Repository
@Transactional
public interface QuarterRepository extends
    JpaRepository<Quarter, Long>,
    JpaSpecificationExecutor<Quarter> {

    Quarter findByNameAllIgnoreCase(String name);
    
	public Quarter findStatusById(Long id);

	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value = "UPDATE Quarter  set status=:status where id = :id")
	void updateQuarterStatus(@Param("status") Integer status, @Param("id") Long id);
	
	@Modifying(clearAutomatically = true,flushAutomatically = true)
	@Query(value = "UPDATE Quarter  set status=:status where id != :id")
	void updateOtherQuarterStatus(@Param("status") Integer status, @Param("id") Long id);

}
