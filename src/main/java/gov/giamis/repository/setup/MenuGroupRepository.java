

package gov.giamis.repository.setup;

import gov.giamis.domain.setup.MenuGroup;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MenuGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuGroupRepository extends JpaRepository<MenuGroup, Long>, JpaSpecificationExecutor<MenuGroup> {
	  MenuGroup findByNameIgnoreCase(String name);
	  
	  @Query(value="SELECT DISTINCT mi.id,mi.name,mi.path,mi.icon,mi.created_by,mi.created_date," +
	  		    "mi.last_modified_by,mi.last_modified_date,mi.menu_group_id FROM menu_items mi \r\n" + 
	            "INNER JOIN menu_groups mgp on mi.menu_group_id = mgp.id \r\n" + 
	            "INNER JOIN menu_items_authorities mia on mia.menu_item_id = mi.id\r\n" + 
	            "INNER JOIN authorities au on au.id = mia.authority_id\r\n" + 
	            "INNER JOIN role_authorities ra on ra.authority_id = au.id\r\n" + 
	            "INNER JOIN roles rs on rs.id = ra.role_id\r\n" + 
	            "INNER JOIN user_roles urs on urs.role_id = rs.id\r\n" + 
	            "WHERE mi.menu_group_id =:id",nativeQuery = true)
	    List<MenuGroup> getAllMenuItems(@Param(value="id") Long id);
	  
	  
	  @Query(value="SELECT DISTINCT mgp.id,mgp.name,mgp.path,mgp.icon,mgp.created_by,mgp.created_date,"
	  		+ "mgp.last_modified_by,mgp.last_modified_date,mgp.menu_group_id FROM menu_groups mgp \r\n" + 
	  		"INNER JOIN menu_groups mmg ON mgp.id = mmg.menu_group_id \r\n" + 
	  		"INNER JOIN menu_items mi ON mi.menu_group_id = mmg.id \r\n" + 
	  		"INNER JOIN menu_items_authorities mia ON mia.menu_item_id = mi.id \r\n" + 
	  		"INNER JOIN authorities au ON au.id = mia.authority_id \r\n" + 
	  		"INNER JOIN role_authorities ra ON ra.authority_id = au.id \r\n" + 
	  		"INNER JOIN roles rs ON rs.id = ra.role_id \r\n" + 
	  		"INNER JOIN user_roles urs ON urs.role_id = rs.id \r\n" + 
	  		"INNER JOIN users usrs ON usrs.id = urs.role_id \r\n" + 
	  		"WHERE usrs.id =:id \r\n" + 
	  		"GROUP BY mmg.menu_group_id,mgp.id  ORDER BY mgp.menu_group_id DESC",nativeQuery = true)
	    List<MenuGroup> getAllMenuGroups(@Param(value="id") Long id);
	  
	  @Query(value="select DISTINCT mgp.id,mgp.name,mgp.path,mgp.icon,mgp.created_by,mgp.created_date,"
		  		+ "mgp.last_modified_by,mgp.last_modified_date,mgp.menu_group_id FROM menu_groups mgp\r\n" + 
		  		"INNER JOIN menu_items mi ON mi.menu_group_id = mgp.id\r\n" + 
		  		"INNER JOIN menu_items_authorities mia ON mia.menu_item_id = mi.id \r\n" + 
		  		"INNER JOIN authorities au ON au.id = mia.authority_id \r\n" + 
		  		"INNER JOIN role_authorities ra ON ra.authority_id = au.id \r\n" + 
		  		"INNER JOIN roles rs ON rs.id = ra.role_id \r\n" + 
		  		"INNER JOIN user_roles urs ON urs.role_id = rs.id \r\n" + 
		  		"WHERE urs.user_id =:id AND mgp.menu_group_id IS NULL\r\n" + 
		  		"ORDER BY mgp.menu_group_id DESC",nativeQuery = true)
		    List<MenuGroup> getAllMenuGroupsNoParent(@Param(value="id") Long id);
	  
	  @Query(value="FROM MenuGroup mg  WHERE mg.parentMenuGroup.id =:id")
	  List<MenuGroup> findByParentMenuGroup(@Param(value="id") Long id);
	  
	  @Query(value="FROM MenuGroup mg  WHERE mg.id =:id")
	  List<MenuGroup> findMenuGroupById(@Param(value="id") Long id);
}
