package gov.giamis.repository.setup;

import gov.giamis.domain.setup.GfsCode;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the GfsCode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GfsCodeRepository extends JpaRepository<GfsCode, Long>, JpaSpecificationExecutor<GfsCode> {

	GfsCode findByCodeIgnoreCase(String code);
}
