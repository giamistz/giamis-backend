
CREATE OR REPLACE FUNCTION getAllAuthorities(getResource varchar)
RETURNS SETOF public.authorities AS
$BODY$
BEGIN
return query SELECT * from authorities where resource=getResource;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;