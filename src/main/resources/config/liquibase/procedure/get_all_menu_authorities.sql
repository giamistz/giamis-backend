
CREATE OR REPLACE FUNCTION getAllMenuAuthorities(getResource varchar)
RETURNS SETOF public.authorities AS
$BODY$
BEGIN
return query SELECT * from authorities where resource=getResource and action like '%getAll%';
END;
$BODY$
LANGUAGE plpgsql VOLATILE;